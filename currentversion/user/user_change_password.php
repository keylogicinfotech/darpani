<?php 
/*
File Name  :- user_personal_details.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#--------------------------------------------------------------------------------------------------------
#Include files
session_start();
include("./user_validate_session.php");
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_xml.php";
include "./../includes/count_site_unique_view.php";
//include "./../includes/http_to_https.php";
include "./user_config.php";
#-------------------------------------------------------------------------------------------------------

$str_title_page = "Change Password";
$str_db_table_name = "t_user";
$INT_USER_SESSION_PKID = $_SESSION["userpkid"];
$STR_USER_SESSION_LOGINID = $_SESSION["userpkid"];
//----------------------------------------------------------------------------------------------------
# Select Query to Get Details
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$str_db_table_name. " WHERE pkid=".$INT_USER_SESSION_PKID;
$rs_list = GetRecordSet($str_query_select);
$str_old_password = $rs_list->fields("password");
//----------------------------------------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print $STR_TITLE_MODEL; ?> - <?php print $str_title_page; ?></title>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />   
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> 
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
<div class="container center-bg">
    <div class="row padding-10">
    <a name="ptop" id="ptop"></a>
        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
            <h1 align="right"><?php print $str_title_page; ?></h1>           
        </div>
    </div>
    <hr/>
    <div class="row padding-10">
    <div class="col-md-3 col-xs-12 col-sm-4">
            <?php include($STR_USER_PANEL_PATH); ?>
    </div>
    <div class="col-md-9 col-xs-12 col-sm-8">
        
        <div class="row padding-10">
            <div class="col-md-12">
                <form name="frm_change_password" id="frm_change_password" novalidate>
                    <?php /*?><div><label>Model Login ID:</label>&nbsp;&nbsp;<span class="heading05"><?php print($STR_USER_SESSION_LOGINID);?></span></div><?php */?>
                    <div class="control-group form-group">
                        <div class="controls"><label>Existing Password<span class="text-help-form"> *</span></label></label>
                                <input type="password" id ="txt_old_password" name="txt_old_password" class="form-control" required data-validation-required-message="Please enter your existing password." placeholder="<?php print $STR_PLACEHOLDER_PASSWORD; ?>" minlength="6" maxlength="20" >
                                <!--<p class="help-block text-help-form">NOTE: Enter your Existing Password here.</p>-->
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls"><label>New Password<span class="text-help-form"> *</span></label></label>
                            <input type="password" name="txt_new_password" id="txt_new_password" class="form-control" required data-validation-required-message="Please enter your new password." placeholder="<?php print $STR_PLACEHOLDER_PASSWORD; ?>" minlength="6" maxlength="20">
                                <!--<p class="help-block text-help-form">NOTE: Keep Password length minimum 6 characters.</p>-->
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls"><label>Confirm New Password<span class="text-help-form"> *</span></label>
                            <input type="password" name="txt_confirm_password" id="txt_confirm_password" class="form-control" required data-validation-matches-message="Confirm Password must be match with new password" required data-validation-required-message="Please enter confirm password."  data-validation-matches-match="txt_new_password" placeholder="<?php print $STR_PLACEHOLDER_PASSWORD; ?>" minlength="6" maxlength="20">
                                <!--<p class="help-block text-help-form">NOTE: New password and Confirm New Password must be same.</p>-->
                        </div>
                    </div>
                    <input type="hidden" name="hdn_old_password" id="hdn_old_password" value="<?php print($str_old_password);?>">

                    <input type="hidden" name="hdn_mid" id="hdn_mid" value="<?php print($INT_USER_SESSION_PKID);?>">
                    <div id="success"></div>
                    
                    <?php print DisplayFormButton("CHANGEPASSWORD",0); ?>&nbsp;&nbsp;<?php print DisplayFormButton("RESET",0); ?>
                
                </form><br/>
            </div>
        </div>
            <div class="row padding-10"><div class="col-md-12"><div class="alert alert-warning"><h3 class="nopadding">Help Section</h3><p class="text-help">• 	Fields marked with '*' are mandatory.</p></div></div></div>
        </div>
    </div>
</div>  
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_change_password.js"></script>
</body>
</html>
