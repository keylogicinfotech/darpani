<?php 
if($APP_RUN_MODE == "LOCAL")
{ 
    $STR_BACKPROCESS_URL = "./user/store_item_purchase_payment_local.php";
}
else
{ 
    $STR_BACKPROCESS_URL = "./user/store_item_purchase_payment_local.php";
    //$STR_BACKPROCESS_URL = "https://bill.ccbill.com/jpost/signup.cgi";     
}
#------------------- CCBILL Payment Gateway - Online Store------------------------------------
$STR_CCBILL_CA = "948624-0006";				//e.g. 000000-0000
$STR_CCBILL_CLIENT_ACC_NUM = "948624";		//e.g. 000000
$STR_CCBILL_CLIENT_SUB_ACC_NUM = "0006";	//e.g. 0000
$STR_CCBILL_FORM_NAME_CREDIT_CARD = "211cc";	//e.g. 9cc
//$STR_CCBILL_FORM_NAME_PHONE_STORE="31t9";			//e.g. 9cp
//$INT_DEFAULT_CCBILL_BILLING_PERIOD_STORE = 30;
$STR_CCBILL_CHECK_CURRENCIES = "840";
$STR_SALT_VALUE = "0PfTTHG/r7u08mSxmZGjpcHP";
$STR_CCBILL_FORM_CURRENCY = 840;
$INT_PRODUCT_DOWNLOAD_LIMIT_DAY = 3;
#------------------------------------------ Page titles----------------------------------------
$STR_TITLE_PAGE = "Brag List"; 

$STR_TITLE_PAGE_DETAILS = "Create Brag - Upload Image";
$STR_TITLE_PAGE_DETAILS02 = "Create Brag - Set Color & Text";
$STR_TITLE_PAGE_DETAILS03= "Create Brag - Select Frame";
$STR_TITLE_PAGE_DETAILS04 = "Create Brag - Select Senders";

$STR_TITLE_PAGE_COLOR = "Color List";
$STR_TITLE_PAGE_PROMOCODE = "Promocode List";
$STR_TITLE_PAGE_ORDER_STATUS = "Order Status List";

$STR_TITLE_PAGE_UNIQUE_VIEW_REPORT_MONTHLY = "Monthly Unique View Report ";
$STR_TITLE_PAGE_UNIQUE_VIEW_REPORT_YEARLY = "Yearly Unique View Report ";

$STR_TITLE_PAGE_REPORT_MONTHLY = "Monthly Transaction Report";
$STR_TITLE_PAGE_REPORT_YEARLY = "Yearly Transaction Report";

$STR_TITLE_PAGE_MAILERS_LIST = "Mailers List";
$STR_TITLE_PAGE_PHOTO_LIST = "Photo List";
$STR_TITLE_PAGE_CART = "Cart";
$STR_TITLE_PAGE_PURCHASE_PAYMENT_LOCAL = "Local Purchase";
$STR_TITLE_PAGE_CHECKOUT_MESSAGE = "Checkout Message";

#-------------------------------------- Panel Steps Heading--------------------------------------------
## For Panel Titles
$STR_TITLE_STEP1 = "<i class='fa fa-upload'></i>&nbsp;Upload Image";
$STR_TITLE_STEP2 = "<i class='fa fa-check-circle'></i>&nbsp;Set Color & Text";
$STR_TITLE_STEP3 = "<i class='fa fa-check-square'></i>&nbsp;Select Frame";
$STR_TITLE_STEP4 = "<i class='fa fa-envelope'></i>&nbsp;Select Senders";

#-------------------------------- Image Paths & Validation Variables-----------------------------------
$UPLOAD_IMG_PATH = "./../mdm/store/";
$UPLOAD_IMG_PATH_PHOTO_FRAME = "./../mdm/storephotoframe/";
$INT_IMG_WIDTH_LARGE = 900;	
$INT_IMG_WIDTH_THUMB = 300; 
$INT_RECORD_PER_PAGE = 20;


$STR_IMG_FILE_TYPE = "File Type: .jpeg, .jpg, .png, .gif";

#-------------------------------- Table Related Variables-----------------------------------
# For Main List
global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "t_store"; 
$STR_DB_TABLE_NAME_ORDER_BY = "";

# for user
global $STR_DB_TABLE_NAME_USER;
global $STR_DB_TABLE_NAME_USER_ORDER_BY;
$STR_DB_TABLE_NAME_USER = "t_freemember"; 
$STR_DB_TABLE_NAME_USER_ORDER_BY = "";

#For Blog
global $STR_DB_TABLE_NAME_BLOG;
$STR_DB_TABLE_NAME_BLOG = "tr_blog"; 
# For Category
global $STR_DB_TABLE_NAME_CAT;
global $STR_DB_TABLE_NAME_ORDER_BY_CAT;

$STR_DB_TABLE_NAME_CAT = "t_store_cat"; 
$STR_DB_TABLE_NAME_ORDER_BY_CAT = " ORDER BY title ASC";


global $STR_DB_TABLE_NAME_SUBCAT;
global $STR_DB_TABLE_NAME_ORDER_BY_SUBCAT;

$STR_DB_TABLE_NAME_SUBCAT = "t_store_subcat"; 
$STR_DB_TABLE_NAME_ORDER_BY_SUBCAT = " ORDER BY subcattitle ASC";

# For Color
global $STR_DB_TABLE_NAME_COLOR;
global $STR_DB_TABLE_NAME_ORDER_BY_COLOR;
$STR_DB_TABLE_NAME_COLOR = "t_store_color"; 
$STR_DB_TABLE_NAME_ORDER_BY_COLOR = " ORDER BY title ASC ";

# For Promocode
global $STR_DB_TABLE_NAME_PROMOCODE;
global $STR_DB_TABLE_NAME_ORDER_BY_PROMOCODE;
$STR_DB_TABLE_NAME_PROMOCODE = "t_store_promocode"; 
$STR_DB_TABLE_NAME_ORDER_BY_PROMOCODE = " ORDER BY displayorder DESC, title ASC ";

# For Photo Frame
global $STR_DB_TABLE_NAME_FRAME;
global $STR_DB_TABLE_NAME_ORDER_BY_FRAME;
$STR_DB_TABLE_NAME_FRAME = "t_store_photoframe"; 
$STR_DB_TABLE_NAME_ORDER_BY_FRAME = " ORDER BY title ASC ";

/*global $STR_DB_TABLE_NAME_PURCHASE_ADDRESSBOOK;
global $STR_DB_TABLE_NAME_ORDER_BY_PURCHASE_ADDRESSBOOK;
$STR_DB_TABLE_NAME_PURCHASE_ADDRESSBOOK = "t_store_purchase_addressbook"; 
$STR_DB_TABLE_NAME_ORDER_BY_PURCHASE_ADDRESSBOOK = "";


global $STR_DB_TABLE_NAME_ADDRESSBOOK;
global $STR_DB_TABLE_NAME_ORDER_BY_ADDRESSBOOK;
$STR_DB_TABLE_NAME_ADDRESSBOOK = "t_addressbook"; 
$STR_DB_TABLE_NAME_ORDER_BY_ADDRESSBOOK = ""; */


global $STR_DB_TABLE_NAME_PURCHASE;
global $STR_DB_TABLE_NAME_ORDER_BY_PURCHASE;
$STR_DB_TABLE_NAME_PURCHASE = "t_store_purchase"; 
$STR_DB_TABLE_NAME_ORDER_BY_PURCHASE = "";

global $STR_DB_TABLE_NAME_PHOTO;
global $STR_DB_TABLE_NAME_ORDER_BY_PHOTO;
$STR_DB_TABLE_NAME_PHOTO = "tr_store_photo"; 
$STR_DB_TABLE_NAME_ORDER_BY_PHOTO= " ORDER BY displayorder DESC";

global $STR_DB_TABLE_NAME_STORE_VIEW;
$STR_DB_TABLE_NAME_STORE_VIEW = "t_store_item_view";

global $STR_DB_TABLE_NAME_SESSION_CART;
$STR_DB_TABLE_NAME_SESSION_CART = "t_store_sessioncart";

global $STR_DB_TABLE_NAME_CCBILL_PRICE;
$STR_DB_TABLE_NAME_CCBILL_PRICE = "t_ccbill_dynamic_price";


#for store report view
$INT_BAR_CHART_WIDTH = 100;
$VIEW_BAR_CHART_COLOR="#A8DBFF";

#Error / Success Messages
$STR_MSG_ACTION_DISCOUNT_POSITIVE = "Error... You can only enter positive numeric value for Discount Percentage and Discount Amount.";
$STR_MSG_ACTION_DISCOUNT_BLANK = "Error... Both Discount Percentage and Discount Amount can not be blank. Please enter any one.";



# Common Messages / Notes / Help displaying on Form
$STR_MSG_COLOR = "Separate multiple colors with pipe | (pipe) e.g. White | Black";
$STR_MSG_SIZE = "Separate multiple sizes with pipe | (pipe) e.g. S | M | L | XL";
$STR_MSG_TYPE = "Separate multiple types with pipe | (pipe) e.g. Type1 | Type2";
$STR_MSG_DOMESTIC_SHIPPING = "Enter 0 (zero) for no shipping charge.";
$STR_MSG_INTERNATIONAL_SHIPPING = "Enter 0 (zero) for no shipping charge.";
$STR_DISPLAY_AS_PURCHASE = "If set YES, item purchase link will be dipaly at user side.";	

global $XML_FILE_PATH;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/store/";

$STR_TITLE_DROPDOWN = "-- ALL USERS --";

## Brag Related Variables
# For Landscape 
$INT_IMAGE_WIDTH_L = 2100; 
$INT_IMAGE_HEIGHT_L = 1500;

$INT_IMAGE_WIDTH_PRINTABLE_L = 2100; 
$INT_IMAGE_HEIGHT_PRINTABLE_L = 1500;

# For Portrait
$INT_IMAGE_WIDTH_P = 1500;
$INT_IMAGE_HEIGHT_P = 2100;

$INT_IMAGE_WIDTH_PRINTABLE_P = 1500;
$INT_IMAGE_HEIGHT_PRINTABLE_P = 2100;

$INT_IMAGE_RATIO = 3;
$INT_DPI = 300;
$INT_MULTIPLE = 4;

$INT_QR_CODE_SIZE = "300x300";

$INT_IMAGE_FRAME_WIDTH_ONE_SIDE = 100;

$STR_CBO_OPTION1 = "PROCESSING";
$STR_CBO_OPTION2 = "SHIPPED";
$STR_CBO_OPTION3 = "DELIVERED";

$STR_PROMO_OPTION1 = "ENABLE";
$STR_PROMO_OPTION2 = "DISABLE";

$STR_MSG_ACTION_INVALID_DIMENSION_L = "Error... Image must be of minimum ".$INT_IMAGE_WIDTH_L." w X ".$INT_IMAGE_HEIGHT_L." h";
$STR_MSG_ACTION_INVALID_DIMENSION_P = "Error... Image must be of minimum ".$INT_IMAGE_WIDTH_P." w X ".$INT_IMAGE_HEIGHT_P." h";
?>