<?php 
/*
File Name  :- user_home.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#---------------------------------------------------------------------
#Include files
session_start();
include "./user_validate_session.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
//include "./../includes/http_to_https.php";
include "./user_config.php";
//print_r($_SESSION);exit;
#----------------------------------------------------------------------
//$str_title_page = "";
$str_title_page = "My Dashboard";
$str_db_table_name = "t_user";
$str_db_table_name_news = "t_news";
$str_db_table_name_metatage = " t_page_metatag ";

$int_userpkid = 0;
$terms_usage = "";
$fp=OpenXMLFile($STR_XML_FILE_PATH_CMS."user_cms.xml");
//$str_desc_cms = getTagValue("ITEMKEYVALUE_LOGIN",$fp);
$terms_usage = getTagValue("ITEMKEYVALUE_USAGETERMS",$fp);
$str_visible = getTagValue("ITEMKEYVALUE_VISIBLE2",$fp);
//print "terms of usage:" .$terms_usage;
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table
/*$str_title_page_metatag = "PG_MT_HOME";
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatage. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."'";
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");*/
#----------------------------------------------------------------------
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$str_db_table_name. " WHERE pkid=".$_SESSION["userpkid"]."";
//print $str_query_select; 
$rs_list = GetRecordset($str_query_select);
//$int_userpkid = $_SESSION["userpkid"]; 
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print $STR_TITLE_MODEL; ?> - <?php print $str_title_page; ?></title>
    <title><?php // print($STR_SITE_TITLE);?> : <?php //print($rs_list_mt->fields("titletag")) ;?></title>
    <?php //print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />   
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> 
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <a name="ptop" id="ptop"></a>
            <div class="col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1>           
            </div>
        </div><hr/>
        <div class="row padding-10">
            <div class="col-md-3 col-xs-12 col-sm-12">
                <?php include($STR_USER_PANEL_PATH); ?>
            </div>
            <div class="col-md-9 col-xs-12 col-sm-12">
                <?php if($rs_list->fields("approved") == "NO") {
                    $str_user_type = "";
                    
                    ?>
                    <?php if(trim($rs_list->fields("shortenurlkey"))!="" && trim($rs_list->fields("name"))!="" && trim($rs_list->fields("emailid"))!="" && trim($rs_list->fields("countrypkid"))!="" && trim($rs_list->fields("statepkid"))!="" && trim($rs_list->fields("city"))!="" && trim($rs_list->fields("zipcode"))!="" && trim($rs_list->fields("address"))!="" && trim($rs_list->fields("description"))!=""  && (trim($rs_list->fields("imagefilenamenew"))!="" || trim($rs_list->fields("imagefilenamelarge"))!="") && trim($rs_list->fields("uscproof1")) && trim($rs_list->fields("uscproof2"))!="" && $rs_list->fields("dateofbirth")!="" ) { ?> 
                    <div class="row padding-10">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                            Thank you for your submission! Please wait while the site administrator reviews your application. You will be notified by email once your profile is approved or if anything else is required from you. Thank you.
                            </div>
                        </div><br />
                    </div>
                    <?php } else { ?>
                    <div class="row padding-10">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                            <?php /* ?>You are <b>NOT APPROVED YET</b> by the site administrator.<br />
                            Please complete the 3 required sections at the Control Panel to get approved:<br/><br/>
                            &bull; Age & Identity Verification - Upload 2 Forms of ID<br/> &bull; Agreement - Review & E-Sign Agreement<br/>&bull; Personal Details - Complete your Profile Details<br /><br/>
                            If you have completed the 3 required sections to get approved then please wait while site administrator reviews your submission.  You will be notified by email once your profile has been approved.<?php */ ?>
			    You are <b>NOT APPROVED YET</b> by the site administrator.<br />
                            Please complete your Profile Details at the Control Panel to get approved:<br/><br/>
                           
                            If you have completed above section to get approved then please wait while site administrator reviews your submission.  You will be notified by email once your profile has been approved.
                            </div>
                        </div><br />
                    </div>
                    <?php } ?>
                <?php } ?>
                <?php if($rs_list->fields("approved") == "YES") { 
                    if($rs_list->fields("isusertype") == "REGULAR")
                    {
                        $str_user_type = "REGULAR";
                    }
                    else {
                        $str_user_type = "WHOLESALER";
                    } ?>
                <?php 
		$str_query_select="";
		$str_query_select="SELECT * FROM " .$str_db_table_name_news. " WHERE typefornews = '".$str_user_type."' AND visible='YES' AND archive='NO' ORDER BY displayorder DESC, date DESC,pkid,heading" ;
                //print $str_query_select;
		$rs_list_news=GetRecordSet($str_query_select);
            ?>
            <?php if(!$rs_list_news->eof()) { ?>
            <div class="row padding-10">      
                <div class="col-md-1 text-center"></div> 
                <div class="col-md-5 text-center"> 
                    <a href="./user_home.php#ptop" title="Click to view latest announcements" type="button" class="btn btn-warning btn-block disabled"><i class="fa fa-bullhorn" aria-hidden="true"></i>&nbsp;<b>View Latest Announcements</b></a>
                </div>
                <div class="col-md-5 text-center"> 
                    <a href="./user_home_archived_news.php#ptop" title="Click to view past announcements" type="button" class="btn btn-default btn-block"><i class="fa fa-bullhorn" aria-hidden="true"></i>&nbsp;<b>View Past Announcements</b></a> 
                </div>
                <div class="col-md-1 text-center"></div>
            </div>
            <br/>
        
        <?php 
            $int_cnt=1;
            while(!$rs_list_news->eof())
            { ?>

            <?php if($rs_list_news->fields("imagefilename")!="") { ?>
                <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list_news->fields("pkid"));?>" rel="thumbnail"><img class="img-responsive" src="<?php print($STR_UPLOAD_NEWS_PATH.$rs_list_news->fields("imagefilename"));?>"  border="0" align="absmiddle" alt="<?php print $rs_list_news->fields("imagefilename")?>" title="<?php print $rs_list_news->fields("imagefilename")?>"></a>
                <div class="modal fade f-pop-up-<?php print($rs_list_news->fields("pkid")); ?> " align="center" role="dialog" aria-labelledby="" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                <img src="<?php print($STR_UPLOAD_NEWS_PATH.$rs_list_news->fields("imagefilenamelarge"));?>" class="img-responsive img-rounded" title="<?php print $str_title_page; ?> image" alt="<?php print $str_title_page; ?> image">
                            </div>
                        </div>
                    </div>
                </div>															
            <?php } ?>
            <div class="row">
                <div class="col-md-12">
                    <h4><b><?php print $rs_list_news->Fields("heading"); ?></b></h4>
                    <p align="justify"><?php print $rs_list_news->Fields("description"); ?></p><hr/>                             
                </div>
            </div> 
            <?php 
            $int_cnt=$int_cnt+1;
            $rs_list_news->MoveNext();
            } ?>
            <?php  }?>
                <?php if($terms_usage != "" && $str_visible == "YES") { ?>
                <div class="row padding-10">
                    <div class="col-md-12">
                        <div class="alert alert-warning">
                            <h3 class="nopadding">Terms Of Use</h3>
                            <p class="text-help"><?php  print($terms_usage);?></p>
                        </div>
                    </div>
                </div>
                <?php } ?>
    <?php   } ?> 
            </div>
        </div>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_personal_details.js"></script>
</body>
</html>
