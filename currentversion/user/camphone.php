<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#-------------------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/http_to_https.php";	
include "./../includes/lib_image.php";
include "./../includes/lib_xml.php";

#-------------------------------------------------------------------------------
$str_page_title = "Cam Phone";
//$str_img_path = "./mdm/about/";
$str_xml_file_name = "niteflirt.xml";
$str_xml_file_name_cms = "about_cms.xml";

$str_title_page_metatag = "PG_NITEFLIRT";
$str_db_table_name_metatag = "t_page_metatag";
$str_db_table_name = "t_about";

$str_desc_nightflirt = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_MODULE.$str_xml_file_name);
$str_desc_nightflirt = getTagValue("DESCRIPTION", $fp);
$str_title_nightflirt = getTagValue("TITLE", $fp);
//print_r($str_title_nightflirt);
//$str_image_about = getTagValue("IMAGENAME", $fp);

#getting datas from module xmlfile.
$arr_xml_list="";
$arr_xml_list=readXml($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");

#open cms xml file
/*$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp); */

#----------------------------------------------------------------------
# Select Query to get blog details
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$str_db_table_name." WHERE visible = 'YES' ORDER BY title ASC";
//print $str_query_select;exit;
$rs_list_niteflirt = GetRecordSet($str_query_select);

$str_about_title = "";
$str_about_title = $rs_list_niteflirt->Fields("title");
//print $str_about_title;exit;
#----------------------------------------------------------------------
# Get metatag page title
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatag. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = "";
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag("PG_NITEFLIRT")); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>

<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_page_title; ?></h1>
                <hr/>
            </div>
        </div> 
        <?php 
           $arr_test=array_keys($arr_xml_list); 
        if($arr_test[0]!= "ROOT_ITEM"){ ?>
        
             <?php  $int_cnt = 0; ?>
            <?php

            if($arr_test[0]!= "ROOT_ITEM")
            { ?>
                           <?php 
// print_r($arr_test);                  
            while(list($key,$val) = each($arr_xml_list)) 
            {
                if(is_array($val))
                { 
            
            ?><div class="well">
                <div class="row padding-10">
                    
                        <div class="col-lg-3 col-md-3 ">
                            <h3 align="left" class="nopadding"><?php print(($arr_xml_list[$key]["TITLE"])) ?></h3>
                        </div>
                        <div class="col-lg-9 col-md-9 ">
                            <p class="nopadding"><?php print(($arr_xml_list[$key]["DESCRIPTION"])) ?></p>
                        </div>
                    </div>
                </div>

             <?php   
            }} }?>

          <?Php /* ?>  <div class="col-md-12">
                <?php if($str_desc_cms != "" && $str_desc_cms != "<br>" && $str_visible_cms == "YES") { ?>
                    <div  class="breadcrumb"><p align="justify" class="nopadding"><?php print($str_desc_cms);?></p></div>
                <?php } ?>
            </div> <?Php */ ?>
       
        <?php } ?>
      
    </div> 
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    
    
</body>
    <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
    </script>
<div class="scrollup" style="display: block;"></div>
<script type="text/javascript">
            $(document).ready(function(){ 

            $(window).scroll(function(){
                if ($(this).scrollTop() > 100) {
                    $('.scrollup').fadeIn();
                } else {
                    $('.scrollup').fadeOut();
                }
            }); 
            $('.scrollup').click(function(){
                $("html, body").animate({ scrollTop: 0 }, 600);
                return false;
            });
        });
    </script>
    
</html>
