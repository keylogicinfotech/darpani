<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_MEMBERVIP_SIGNIN_SIGNUP";
$str_img_path = "";
$str_db_table_name = "t_page_metatag";
$str_xml_file_name = "";
$str_xml_file_name_cms = "member_cms.xml";
$int_records_per_page = 0;
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
$str_desc = "";
$str_desc2 = "";
$str_desc3 = "";
$str_desc4 = "";
$str_desc5 = "";
$str_desc6 = "";
$str_desc7 = "";
$str_desc8 = "";

$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_desc2 = getTagValue("ITEMKEYVALUE_DESCRIPTION2", $fp);;
$str_desc3 = getTagValue("ITEMKEYVALUE_DESCRIPTION3", $fp);;
$str_desc4 = getTagValue("ITEMKEYVALUE_DESCRIPTION4", $fp);;
$str_desc5 = getTagValue("ITEMKEYVALUE_DESCRIPTION5", $fp);;
$str_desc6 = getTagValue("ITEMKEYVALUE_DESCRIPTION6", $fp);;
$str_desc7 = getTagValue("ITEMKEYVALUE_DESCRIPTION7", $fp);;
$str_desc8 = getTagValue("ITEMKEYVALUE_DESCRIPTION8", $fp);;

$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet">    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1>
                <hr/>
            </div>
        </div>
        <?php if($str_visible_cms == "YES") { ?>
            <?php if($str_desc!= "" && $str_desc!= "<br>") { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div  class="breadcrumb"><p align="justify"><?php print($str_desc);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 text-center">
                <b><?php if($str_visible_cms == "YES") { ?><?php if($str_desc2!= "" && $str_desc2!= "<br>") { print $str_desc2; ?><?php } ?><?php } ?></b>
                <form action="https://bill.ccbill.com/jpost/signup.cgi" method="POST">
                    <input type="hidden" name="clientAccnum" value="<?php print($STR_CCBILL_MEMBER_CLIENT_ACCOUNT_NUMBER);?>"/>
                    <input type="hidden" name="clientSubacc" value="<?php print($STR_CCBILL_MEMBER_CLIENT_SUB_ACCOUNT_NUMBER);?>"/>
                    <input type="hidden" name="formName" value="<?php print($STR_CCBILL_MEMBER_FORM_NAME_CREDIT_CARD);?>"/>
                    <input type="hidden" name="language" value="<?php print($STR_CCBILL_MEMBER_LANGUAGE);?>"/>
                    <input type="hidden" name="allowedTypes" value="<?php print($STR_CCBILL_MEMBER_EU_DEBIT_ALLOWEDTYPES);?>"/>
                    <input type="hidden" name="subscriptionTypeId" value="<?php print($STR_CCBILL_MEMBER_EU_DEBIT_SUBSCRIPTIONTYPEID);?>"/>
                    <button class="btn btn-primary"><b>Instant Credit Card Access</b></button>
                </form>
                <br/><br/>
                <?php if($str_visible_cms == "YES") { ?><?php if($str_desc5!= "" && $str_desc5!= "<br>") { print $str_desc5; ?><?php } ?><?php } ?>
                <form action="https://support.ccbill.com">
                    <button class="btn btn-primary"><b>I Forgot My Password / Customer Support</b></button>
                </form>
            </div>
        </div>
        <br/><br/>
        <div class="row padding-10">
            <div class="col-lg-4 col-md-4"></div>
            <div class="col-lg-4 col-md-4">
                <?php if($str_visible_cms == "YES") { ?><?php if($str_desc6!= "" && $str_desc6!= "<br>") { print $str_desc6; ?><?php } ?><?php } ?>
                <form name="frm_login" id="frm_login" novalidate>
                    <div class="control-group form-group">
                        <div class="controls">
                            <input type="text" class="form-control" id="txt_loginid" name="txt_loginid" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_LOGINID; ?>" placeholder="<?php print $STR_PLACEHOLDER_LOGINID; ?>" >
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <input type="password" class="form-control" id="txt_password" name="txt_password" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_PASSWORD; ?>" placeholder="<?php print $STR_PLACEHOLDER_PASSWORD; ?>" minlength="6" maxlength="20" />
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls input-group">
                            <input type="text" class="form-control" id="txt_secretcode" name="txt_secretcode" required data-validation-required-message="<?php print $STR_PLACEHOLDER_SECRET_CODE; ?>" placeholder="<?php print $STR_PLACEHOLDER_SECRET_CODE; ?>" minlength="7" maxlength="7">
                            <span class="input-group-addon input-group-addon-no-padding nopadding">
                                <img src="./includes/get_unique_image.php" border="0" alt="Captcha" width="130" height="29">
                            </span>
                        </div>
                        <p class="help-block"></p>                            
                    </div>
                    <div id="successL"></div>
                    <p><button type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i>&nbsp;<b>LOGIN</b></button></p>
                </form>   
            </div>
            <div class="col-lg-4 col-md-4"></div>
        </div>
        <?php if($str_visible_cms == "YES") { ?>
            <?php if($str_desc7!= "" && $str_desc7!= "<br>") { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12" align="left">
                    <p class="text-help"><?php print $str_desc7; ?></p>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/vipmember_joinnow.js"></script>
</body>
</html>
