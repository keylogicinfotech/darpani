<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_BOOKING";
$str_img_path = $STR_SITENAME_WITH_PROTOCOL."/mdm//";
$str_db_table_name = "t_page_metatag";
$str_xml_file_name = "";
$str_xml_file_name_cms = "bookme_cms.xml";
$int_records_per_page = 0;
#----------------------------------------------------------------------
#read main xml file
$str_xml_list = "";
$str_xml_list = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");

$int_total_records = 0;
$int_total_records = (count(array_keys($str_xml_list)));	
//print $int_total_records;
#----------------------------------------------------------------------
#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
            </div>
        </div>
        <?php if($str_visible_cms == "YES") { ?>
            <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?><br/>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="frm_add" name="frm_add" novalidate>
                    <div class="row padding-10">
                        <div class="col-md-6  col-lg-6">
                            <div class="form-group control-group">
                                <div class="controls">
                                    <label>Type of Assignment</label><span class="text-help-form">&nbsp;*</span>
                                    <input type="text" id="txt_title" name="txt_title" class="form-control" required data-validation-required-message="<?php print $STR_PLACEHOLDER_TEXT; ?>" placeholder="<?php print $STR_PLACEHOLDER_TEXT; ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group control-group">
                                <label>Budget (US $)</label><span class="text-help-form">&nbsp;*</span>
                                <div class="controls">
                                    <input type="text" id="txt_price" name="txt_price" class="form-control" required data-validation-required-message="<?php print $STR_PLACEHOLDER_INT_PRICE; ?>" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-4  col-lg-4">
                            <div class="form-group control-group">
                                <div class="controls">
                                    <label>Event Date</label><span class="text-help-form">&nbsp;*</span><br/>
                                    <?php print DisplayDate("cbo_day", "cbo_month", "cbo_year",date("d"), date("m"), date("Y")); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4  col-lg-4">
                            <div class="form-group control-group">
                                <div class="controls">
                                    <label>Event Time</label><span class="text-help-form">&nbsp;*</span><br/>
                                    <?php print DisplayTime12("cbo_time_hh", "cbo_time_mm", "cbo_time_ampm",date("h"), date("i"), strtoupper(date("a"))); ?>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            <div class="form-group control-group">
                                <label>Event Time Zone</label><span class="text-help-form">&nbsp;*</span><br/>
                                <?php print(DisplayTimeZone("cbo_timezone")); ?>
                                
                            </div>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-12  col-lg-12">
                            <div class="form-group control-group">
                                <div class="controls">
                                    <label>Event Venue</label><span class="text-help-form">&nbsp;*</span>
                                    <input type="text" id="txt_location" name="txt_location" class="form-control" placeholder="<?php print $STR_PLACEHOLDER_TEXT; ?>" required data-validation-required-message="<?php print $STR_PLACEHOLDER_TEXT; ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-12  col-lg-12">
                            <div class="form-group control-group">
                                <label>Event Description</label><span class="text-help-form">&nbsp;*</span>
                                <div class="controls">
                                    <textarea id="ta_desc" name="ta_desc" class="form-control" rows="4" required data-validation-required-message="<?php print $STR_PLACEHOLDER_MESSAGE; ?>" placeholder="<?php print $STR_PLACEHOLDER_MESSAGE; ?>"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row padding-10">
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group control-group">
                                <div class="controls">
                                    <label>Contact Name</label><span class="text-help-form">&nbsp;*</span>
                                    <input type="text" id="txt_name" name="txt_name" class="form-control" required data-validation-required-message="<?php print $STR_PLACEHOLDER_NAME; ?>" placeholder="<?php print $STR_PLACEHOLDER_NAME; ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group control-group">
                                <label>Contact Email ID</label><span class="text-help-form">&nbsp;*</span>
                                <div class="controls">
                                    <input type="email" id="txt_emailid" name="txt_emailid" class="form-control" required data-validation-required-message="<?php print $STR_PLACEHOLDER_EMAIL; ?>" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group control-group">
                                <div class="controls">
                                    <label>Contact Phone</label><span class="text-help-form">&nbsp;</span>
                                    <input type="text" id="txt_phone" name="txt_phone" class="form-control" placeholder="<?php print $STR_PLACEHOLDER_INT; ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group control-group">
                                <div class="controls">
                                    <label>Contact Fax</label><span class="text-help-form">&nbsp;</span>
                                    <input type="text" id="txt_fax" name="txt_fax" class="form-control" placeholder="<?php print $STR_PLACEHOLDER_INT; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-12  col-lg-12">
                            <div class="control-group form-group">
                                <label>Secret Code</label><span class="text-help-form">&nbsp;*</span>
                                <div class="controls input-group">
                                    <input type="text" class="form-control" id="txt_secretcode" name="txt_secretcode" required data-validation-required-message="<?php print $STR_PLACEHOLDER_SECRET_CODE; ?>" placeholder="<?php print $STR_PLACEHOLDER_SECRET_CODE; ?>" minlength="7" maxlength="7">
                                    <span class="input-group-addon input-group-addon-no-padding nopadding">
                                        <img src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/includes/get_unique_image.php" border="0" alt="Captcha" width="130" height="29">
                                    </span>
                                </div>
                                <p class="help-block"></p>                            
                            </div>
                        </div>
                    </div>
                    <?php //print_r($_GET);?>
                    <div id="success"></div>
                    <input type="hidden" id="hdn_blogpkid" name="hdn_blogpkid" value="<?php print $int_pkid; ?>" />
                    <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i>&nbsp;Submit</button>
                </form>
            </div>
        </div><br/>
        <?php include($STR_USER_BANNER_PATH); ?>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/bookme.js"></script>
</body>
</html>
