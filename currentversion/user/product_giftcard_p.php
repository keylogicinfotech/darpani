<?php /*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();
set_time_limit(150);
include "./../user/product_validatesession.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_xml.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/lib_datetimeyear.php";
include "./../user/product_config.php";
//include "./../includes/http_to_https.php";	
//print_r($_POST);exit;
#----------------------------------------------------------------------
# posted variables...
$int_giftcard_amount = 0;
if(isset($_POST["txt_giftcard"]) && $_POST["txt_giftcard"] != "")
{
    $int_giftcard_amount = $_POST["txt_giftcard"];
}

if($int_giftcard_amount == "" || $int_giftcard_amount == 0 || !is_numeric($int_giftcard_amount)) 
{
    $response['status']='ERR';
    $response['message']="Error... Please Enter Amount.";
    echo json_encode($response);
    return;
}

$int_userpkid = 0;
if(isset($_POST["hdn_userpkid"]) && $_POST["hdn_userpkid"] != "")
{
    $int_userpkid = $_POST["hdn_userpkid"];
}

if($int_userpkid == 0) 
{
    $response['status']='ERR';
    $response['message']="Error... Please Login to Apply Gift card.";
    echo json_encode($response);
    return;
}

$int_extprice = 0;
if(isset($_POST["hdn_extprice"]) && $_POST["hdn_extprice"] != "")
{
    $int_extprice = $_POST["hdn_extprice"];
}


$item_sessionid="";
if(isset($_SESSION['sessionid']))
{ 
    $item_sessionid = $_SESSION['sessionid']; 
}
$item_uniqueid = "";
if(isset($_SESSION['uniqueid']))
{ 
    $item_uniqueid = $_SESSION['uniqueid']; 
}


if($int_giftcard_amount > $int_extprice)
{
    $response['status'] = 'ERR';
    $response['message'] = "Gift Card Amount should be lesser than your order amount.";
    echo json_encode($response);
    return;
}


$str_where = "";
if(isset($_COOKIE["sessionid"]) && isset($_COOKIE["uniqueid"]) && $_COOKIE["sessionid"] != "" && $_COOKIE["uniqueid"] != "") 
{
    $str_where = " WHERE sessionid='".$_COOKIE["sessionid"]."' AND uniqueid='".$_COOKIE["uniqueid"]."' ";
}
else
{
    $str_where = " WHERE sessionid='".$item_sessionid."' AND uniqueid='".$item_uniqueid."' ";
}



$str_query_select = "";
$str_query_select = "SELECT giftcard, countrypkid FROM ".$STR_DB_TABLE_NAME_USER." WHERE pkid= ".$int_userpkid."";
$rs_list = GetRecordSet($str_query_select);
if($rs_list->Fields("giftcard") == 0)
{
    $response['status'] = 'ERR';
    $response['message'] = "Gift Card Not available.";
    echo json_encode($response);
    return;
}
else 
{
    if($rs_list->Fields("giftcard") >= $int_giftcard_amount)
    {
        $str_query_update = "";
        $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_SESSION_CART." SET giftcard = giftcard + ".$int_giftcard_amount.$str_where ;        
        //print $str_query_update; exit; 
        ExecuteQuery($str_query_update);
        
        $str_query_select = "";
        $str_query_select = "SELECT conversionrate FROM ".$STR_DB_TABLE_NAME_COUNTRY." WHERE pkid=".$_SESSION["usr_cntry"];
        $rs_list_conversionrate = GetRecordSet($str_query_select);
        
        //print $rs_list_conversionrate->Fields("conversionrate"); exit;
        $int_giftcard_amount = $int_giftcard_amount * $rs_list_conversionrate->Fields("conversionrate");
        
        $str_query_update = "";
        $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_USER." SET giftcard = giftcard-".$int_giftcard_amount." WHERE pkid=".$int_userpkid ;        
        //print $str_query_update; exit; 
        ExecuteQuery($str_query_update);
        
    }
    else
    {
        $response['status']='ERR';
        $response['message']="Gift Card Amount must be lesser then your order amount.";
        echo json_encode($response);
        return;
    }
}

$response['status']='SUC';
$response['message']="Gift Card Applied Successfully.";
echo json_encode($response);
return;

?>