<?php
$STR_TITLE_PAGE = "Free Member List";
$STR_TITLE_HISTORY = "IP Address History";
$STR_TITLE_USER = "Free Member";
$UPLOAD_IMG_PATH = "../../mdm/freemember/";
$INT_IMG_WIDTH = 200;
$INT_IMG_HEIGHT = 200;
$INT_IMG_LARGE_WIDTH = 1200;

$INT_RECORD_PER_PAGE = 5;

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "t_freemember"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY registrationdate DESC, loginid ASC "; 

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "cms_cammember_home";

global $STR_DB_TABLE_NAME_COUNTRY;
$STR_DB_TABLE_NAME_COUNTRY = "t_country";

global $STR_DB_TABLE_NAME_STATE;
$STR_DB_TABLE_NAME_STATE = "t_state";

global $STR_DB_TABLE_NAME_HISTORY;
global $STR_DB_TABLE_NAME_HISTORY_ORDER_BY;
$STR_DB_TABLE_NAME_HISTORY = "t_ccbill_member_validation"; 
$STR_DB_TABLE_NAME_HISTORY_ORDER_BY = " ORDER BY registrationdatetime DESC, loginid ASC "; 

$STR_TITLE_DROPDOWN = "ALL RECIPIENTS";

global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/user.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/user_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";	
?>
