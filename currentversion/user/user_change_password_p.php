<?php 
/*
File Name  :- user_personal_details.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#--------------------------------------------------------------------------------------------------------
#Include files
session_start();
//include("./../includes/validate_mdl_session.php");
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_xml.php";
#-------------------------------------------------------------------------------------------------------
//print_r($_POST);exit;
$str_db_table_name = "t_user";
$INT_MODEL_SESSION_PKID=$_SESSION["userpkid"];
//$STR_MODEL_SESSION_LOGINID=$_SESSION["modelid"];
//echo trim($_POST["hdm_mid"]);
//Initializing Variables.
$int_mdlpkid="";
if(isset($_POST["hdn_mid"])) {	$int_mdlpkid=trim($_POST["hdn_mid"]);	}
//echo $int_mdlpkid;
$str_old_password="";
if(isset($_POST["txt_old_password"])){ $str_old_password=trim($_POST["txt_old_password"]); }
$str_new_password="";
if(isset($_POST["txt_new_password"])) { $str_new_password=trim($_POST["txt_new_password"]); }
$str_confirm_password="";
if(isset($_POST["txt_confirm_password"])) { $str_confirm_password=trim($_POST["txt_confirm_password"]); }
//Get Post Datas.
//print($int_mdlpkid); 
if($int_mdlpkid == "" || $int_mdlpkid<=0 || !is_numeric($int_mdlpkid))
{
	
    $response['status']='ERR';
    $response['message']= "SESSION VARIABLE NOT FOUND!";
    echo json_encode($response); 
    return;
    //CloseConnection();
    //Redirect("./mdl_change_password.php?msg=F&type=E&#ptop");
    //exit();
}

if($int_mdlpkid != $INT_MODEL_SESSION_PKID)
{
	
    $response['status']='ERR';
    $response['message']= "Invalid SESSION ID!";
    echo json_encode($response); 
    return;
    //CloseConnection();
    //Redirect("./mdl_change_password.php?msg=F&type=E&#ptop");
    //exit();
}
//----------------------------------------------------------------------------------------------------------------------
if( empty($str_new_password) || is_valid_userpassword($str_new_password)==false )
{
    //$_SESSION['image_secret_code']="";
    $response['status']='ERR';
    $response['message']= "Invalid Password, Please Enter One Capital & One Numeric Character!";
    echo json_encode($response); 
    return;
}
//To Check whether Values are passed properly or not.
if(empty($str_old_password))
{
    $response['status']='ERR';
    $response['message']= "Please Enter Old Password!";
    echo json_encode($response); 
    return;
}
if(empty($str_new_password))
{
    $response['status']='ERR';
    $response['message']= "Please Enter New Password!";
    echo json_encode($response); 
    return;
}

/*if($str_confirm_password != $str_new_password)
{
    CloseConnection();
    Redirect("./mdl_change_password.php?msg=FU&type=E&#ptop");
    exit();
}*/
if(strtolower($str_new_password)!=strtolower($str_confirm_password))
{
    //$_SESSION['image_secret_code']="";
    $response['status']='ERR';
    $response['message']= "Password and Confirm Password must be same!";
    echo json_encode($response); 
    return;
}
//----------------------------------------------------------------------------------------------------------------------
//Select Query
$str_query_select="";
$str_query_select="SELECT * FROM " .$str_db_table_name. " WHERE pkid=".$int_mdlpkid;
//print($str_query_select);
//exit();
$rs_check_password=GetRecordSet($str_query_select);

if($rs_check_password->fields("password") != $str_old_password)
{
	$response['status']='ERR';
    $response['message']= "Invalid Existing Password. Please Enter Correct One!";
    echo json_encode($response); 
    return;
}
//----------------------------------------------------------------------------------------------------------------------
//Update Query
$str_query_update="";
$str_query_update="UPDATE " .$str_db_table_name. " SET password='".ReplaceQuote($str_new_password)."' WHERE pkid=".$int_mdlpkid;
//print($str_query_update);
//exit();
ExecuteQuery($str_query_update);
$response['status']='SUC';
$response['message']="Password Changed Successfully.";
echo json_encode($response);
return;
?>
