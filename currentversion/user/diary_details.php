<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../user/store_config.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";

//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_MT_DIARY";
$str_img_path = $STR_SITENAME_WITH_PROTOCOL."/mdm/journal/";
$str_db_table_name_metatag = "t_page_metatag";
$str_db_table_name_photo = "tr_journal_detail";
$str_xml_file_name = "journal.xml";
$str_btn_title_view_more = "Read Full Story&nbsp;<i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i>";
#----------------------------------------------------------------------
#read main xml file
$str_xml_list = "";
$str_xml_list = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");

$str_xml_list_main = "";
$str_xml_list_main = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");

$int_total_records = 0;
$int_total_records = (count(array_keys($str_xml_list_main)));	
//print $int_total_records;

#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid = 0;
if(isset($_GET["pid"]) && trim($_GET["pid"]) != "" )
{   
    $int_pkid = trim(GetDecryptId($_GET["pid"]));
}

//print $int_pkid; exit;


$str_member_flag = false;
$int_memberpkid = 0;
$str_memberid = "";
if((isset($_SESSION['vipmemberid']) && $_SESSION['vipmemberid'] != ""))
{
    $str_member_flag = true;
    $int_memberpkid = $_SESSION['vipmemberpkid'];
    $str_memberid = $_SESSION['vipmemberid'];
}
/*else
{
    CloseConnection();
    Redirect("./../vipmember-joinnow#ptop"); 
    exit();
}*/
$int_journal_pkid = 0;
$str_jnl_title="";
$str_jnl_desc="";
$str_jnl_date="";
$str_jnl_thumbfile="";
$str_jnl_largefile="";
$str_jnl_preview="";
$str_jnl_access="";	
$str_jnl_displayasnew="";	
$arr_photo="";
if($int_pkid!="")
{
        while(list($key, $val) = each($str_xml_list_main)) 
        {
                if($str_xml_list_main[$key]["PKID"] == $int_pkid)
                {
                        $int_journal_pkid = $str_xml_list_main[$key]["PKID"];				
                        $str_jnl_title = $str_xml_list_main[$key]["TITLE"];
                        $str_jnl_desc = $str_xml_list_main[$key]["DESCRIPTION"];
                        $str_jnl_date = $str_xml_list_main[$key]["DATE"];
                        $str_jnl_thumbfile = $str_xml_list_main[$key]["THUMBIMAGEFILENAME"];
                        $str_jnl_largefile = $str_xml_list_main[$key]["LARGEIMAGEFILENAME"];
                        $str_jnl_preview = $str_xml_list_main[$key]["PREVIEWTOALL"];
                        $str_jnl_access = $str_xml_list_main[$key]["ACCESSTOALL"];
                        $str_jnl_displayasnew = $str_xml_list_main[$key]["DISPLAYASNEW"];
                        /*if($str_xml_list_main[$key]["THUMBIMAGES"]!="")
                        {
                                $arr_photo=explode("|",$str_xml_list_main[$key]["THUMBIMAGES"]);
                        }*/	
                        break;
                }		
        }	
        //print $int_pkid;exit;
        if($int_pkid!=$int_journal_pkid)
        {
                CloseConnection();
                redirect("../../diary");
                exit();
        }
}
//print $str_jnl_title;

#----------------------------------------------------------------------
#get metatag page title from table

$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatag. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_similar_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_similar_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_similar_mt->fields("titletag")) ;?><?php if($str_jnl_title != "") { print " [".$str_jnl_title."]"; }?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet">
</head>
<body>
<?php include("../includes/header.php"); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
                <ol class="breadcrumb thumbnail-margin">
                    <?php /*if($rs_list->Fields("subcattitle") != ""){ ?>
                        <li class="active"><?php print $rs_list->Fields("subcattitle"); ?></li>
                    <?php } */?>
                    <?php if($str_jnl_title != ""){ ?>
                        <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL."/diary"; ?>"><?php print $str_jnl_title; ?></a></li>
                    <?php } ?>
                </ol>
            </div>
        </div>
        <div class="row padding-10">
            <div class="col-lg-4 col-md-4 col-xs-12 col-sm-5">
                <?php  
                $str_select_images = "";
                $str_select_images = "SELECT * FROM ".$str_db_table_name_photo." WHERE visible='YES' AND pkid= ".$int_pkid." ";
                $rs_list_image = GetRecordSet($str_select_images); 
                
                ?>
                <ul id="glasscase" class="gc-start">
                    <?php                
                    while($rs_list_image->eof() != true) 
                    { ?>
                        <li>
                            <img src="<?php print $str_img_path.$int_pkid."/".$rs_list_image->Fields("thumbimagefilename"); ?>" alt="<?php print($str_jnl_title); ?>" data-gc-caption="<?php print($str_jnl_title); ?>" />

                        </li><?php
                        $rs_list_image->MoveNext();
                    } ?>
                </ul> 
            </div>
            <div class="col-lg-8 col-md-8 col-xs-12 col-sm-7">
                <div class="row padding-10">
                    <div class="col-md-12">
                        <?php if($str_jnl_title != "") { ?>
                        <h3 class=""><b><?php print $str_jnl_title; ?><?php if($str_jnl_displayasnew == "YES") { print "&nbsp;".$STR_ICON_PATH_NEW; } ?><?php if($str_jnl_preview == "YES" && $str_jnl_access== "YES") { print "&nbsp;".$STR_ICON_PATH_FREE; } ?></b></h3><hr/>
                        <?php } ?>
                        <?php if($str_jnl_title != "") { ?>
                        <p><?php print $str_jnl_desc; ?></p>
                        <?php } ?>
                        
                    </div>
                </div>
            </div>
        </div>
        <?php 
        $arr_test = array_keys($str_xml_list);
        $int_cnt=0;
        if(trim($arr_test[0])!="ROOT_ITEM")
        { 
            ?>
        <br/><br/>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h3 align="left">More Diary<?php //print $str_title_page; ?></h3><hr/>
            </div>
        </div>
        <div class="row padding-10">
            <?php
            $int_arr_lim=0;
            //print_r($str_xml_list);
            foreach($str_xml_list as $key => $val)         
            {
                    if($int_pkid !=  $str_xml_list[$key]["PKID"])
                    {
                        $int_arr_lim++;
			if(is_array($val))
			{
                            $str_video_access_flag=false;
                            $str_preview_flag=false;
                            if($str_member_flag)
                            {
                                $str_video_access_flag=true;
				$str_preview_flag=true;
                            }
                            else
                            {
                                if($str_xml_list[$key]["ACCESSTOALL"] == "YES")
				{
                                    if($str_xml_list[$key]["PREVIEWTOALL"] == "YES")
                                    {
                                            $str_video_access_flag = true;
                                            $str_preview_flag = true;
                                    }										
				}
				else
				{
                                    if($str_xml_list[$key]["PREVIEWTOALL"] == "YES")
                                    {
                                        $str_preview_flag = true;
                                    }
				}									
                            } ?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="well">
                                    <div class="row padding-10">
                                        <?php if($str_xml_list[$key]["THUMBIMAGEFILENAME"] != "") { ?>
                                        <div class="col-md-6">
                                            <?php if($str_preview_flag == true || $str_member_flag) { ?>
                                                <image src="<?php print $str_img_path.$str_xml_list[$key]["PKID"]."/".$str_xml_list[$key]["THUMBIMAGEFILENAME"]; ?>" alt="<?php print $str_xml_list[$key]["TITLE"]; ?>" title="<?php print $str_xml_list[$key]["TITLE"]; ?>" class="img-responsive" />
                                            <?php } else { print("".MakeBlankTab(0,0,$STR_PREVIEW_NOT_AVAILABLE,"preview-danger")); }?>
                                            <div class="text-top-right">
                                                <?php if($str_xml_list[$key]["DISPLAYASNEW"] == "YES") { print $STR_ICON_PATH_NEW; } ?>
                                                <?php if($str_xml_list[$key]["PREVIEWTOALL"] == "YES" && $str_xml_list[$key]["ACCESSTOALL"] == "YES") { print $STR_ICON_PATH_FREE; } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <?php if($str_xml_list[$key]["TITLE"] != "") { ?>
                                                <h3 class="nopadding"><?php print $str_xml_list[$key]["TITLE"]; ?></a></h3>
                                            <?php } ?>
                                                
                                            <?php if($str_xml_list[$key]["DESCRIPTION"] != "") { ?>     
                                                <p><?php 
                                                    $str_desc_string = "";
                                                    $str_desc_string = $str_xml_list[$key]["DESCRIPTION"]; 
                    //                                $str_desc_string = "";
                                                    $str_desc_string = strip_tags($str_desc_string);
                                                    if (strlen($str_desc_string) > 64) {

                                                    $str_desc_stringCut = substr($str_desc_string, 0, 64);
                                                    $endPoint = strrpos($str_desc_stringCut, ' ');

                                                    //if the string doesn't contain any space then it will cut without word basis.
                                                    $str_desc_string = $endPoint? substr($str_desc_stringCut, 0, $endPoint) : substr($str_desc_stringCut, 0);
                                                    $str_desc_string .= '...';
                                                    }?>

                                                    <?php print $str_desc_string; ?></p>
                                            <?php } ?>
                                            <?php
                                            //print $str_member_flag;exit;
                                            if($str_member_flag == true) { ?>    
                                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/diary/<?php print(GetEncryptId($str_xml_list[$key]["PKID"]));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $str_xml_list[$key]["TITLE"])))); ?>" title="Click to view" class="btn btn-primary">
                                                    <b><?php print $str_btn_title_view_more; ?></b>
                                                </a>
                                            <?php } else if($str_video_access_flag == true && $str_preview_flag == true) { ?>    
                                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/diary/<?php print(GetEncryptId($str_xml_list[$key]["PKID"]));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $str_xml_list[$key]["TITLE"])))); ?>" title="Click to view" class="btn btn-primary">
                                                    <b><?php print $str_btn_title_view_more;?></b>
                                                    
                                                </a>
                                            <?php } else { ?>
                                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view" class="btn btn-primary"><b><b><?php print $str_btn_title_view_more;?></b></a>
                                            <?php } ?>      
                                        </div>
                                        <?php } else { ?>
                                        <div class="col-md-12">
                                            <?php if($str_xml_list[$key]["TITLE"] != "") { ?>
                                                <h3 class=""><?php print $str_xml_list[$key]["TITLE"]; ?></a></h3>
                                            <?php } ?>
                                            
                                            <?php if($str_xml_list[$key]["DESCRIPTION"] != "") { ?>    
                                                <p><?php 
                                                    $str_desc_string = "";
                                                    $str_desc_string = $str_xml_list[$key]["DESCRIPTION"]; 
                    //                                $str_desc_string = "";
                                                    $str_desc_string = strip_tags($str_desc_string);
                                                    if (strlen($str_desc_string) > 64) {

                                                    $str_desc_stringCut = substr($str_desc_string, 0, 64);
                                                    $endPoint = strrpos($str_desc_stringCut, ' ');

                                                    //if the string doesn't contain any space then it will cut without word basis.
                                                    $str_desc_string = $endPoint? substr($str_desc_stringCut, 0, $endPoint) : substr($str_desc_stringCut, 0);
                                                    $str_desc_string .= '...';
                                                    }?>

                                                    <?php print $str_desc_string; ?></p>
                                            <?php } ?>
                                            
                                            <?php
                                            //print $str_member_flag;exit;
                                            if($str_member_flag == true) { ?>    
                                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/diary/<?php  print(GetEncryptId($str_xml_list[$key]["PKID"]));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $str_xml_list[$key]["TITLE"])))); ?>" title="Click to view" class="btn btn-primary">
                                                    <b><?php print $str_btn_title_view_more; ?></b>
                                                </a>
                                            <?php } else if($str_video_access_flag == true && $str_preview_flag == true) { ?>    
                                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/diary/<?php  print(GetEncryptId($str_xml_list[$key]["PKID"]));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $str_xml_list[$key]["TITLE"])))); ?>" title="Click to view" class="btn btn-primary">
                                                    <b><?php print $str_btn_title_view_more;?></b>
                                                    
                                                </a>
                                            <?php } else { ?>
                                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view" class="btn btn-primary"><b><b><?php print $str_btn_title_view_more;?></b></a>
                                            <?php } ?>    
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } if($int_arr_lim % 2 == 0) { print "</div><div class='row padding-10'>"; }
                    }
                
            }?>
        </div>
        <?php } ?>
        
        <br/>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include("../includes/footer.php");CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/glass.js"></script>
    
    <script type="text/javascript">
        $(document).ready( function () {
            $('#glasscase').glassCase({ 'thumbsPosition': 'bottom', 'widthDisplay' : 410});
        });
    </script>
</body>
</html>
