<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
include "./../user/store_config.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_BLOG";
$str_db_table_name_metatag = "t_page_metatag";

$str_img_path = "./mdm/blog/";

$str_db_table_name = "tr_blog";

$str_xml_file_name = "";
$str_xml_file_name_cat = "";

$str_xml_file_name_cms = "blog_cms.xml";
$int_records_per_page = 1;
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid = 0;
if(isset($_GET["bid"]) && $_GET["bid"] != "") 
{
    $int_pkid = $_GET["bid"];
}
//print($int_pkid); exit;
/*$int_cat_pkid = "";
if(isset($_POST["catid"]) && trim($_POST["catid"]) != "" )
{   
    $int_cat_pkid = trim($_POST["catid"]);    
}

$str_name_key = "";
if(isset($_POST["key"]) && trim($_POST["key"]) != "" )
{ $str_name_key = trim($_POST["key"]); }


# get data for paging
$int_page = "";
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"]) != "" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"]) > 0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page = 1; }
//print_r($_POST);
$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";


if($int_cat_pkid > 0)
{
    $str_where = " AND a.subcatpkid=".$int_cat_pkid." ";
}*/
//$str_where = "";    
#----------------------------------------------------------------------
# Select Query to get blog details
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$str_db_table_name." WHERE approved = 'YES' AND visible = 'YES' AND blogtype = 'B' AND blogpkid = ".$int_pkid." ORDER BY submitdatetime DESC, title ASC";
//print $str_query_select;exit;
$rs_list_blog = GetRecordSet($str_query_select);

$str_blog_title = "";
$str_blog_title = $rs_list_blog->Fields("title");
//print $str_blog_title;exit;
#----------------------------------------------------------------------
# Select Query to get blog comments
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$str_db_table_name." WHERE approved = 'YES' AND visible = 'YES' AND blogtype = 'C' AND blogpkid = ".$int_pkid." ORDER BY submitdatetime DESC, title ASC";
$rs_list = GetRecordSet($str_query_select);
//print $rs_list->Count();
#----------------------------------------------------------------------
# Get metatag page title
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatag. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = "";
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?><?php if($str_blog_title != ""){ print " : ".$str_blog_title.""; } ?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?><?php if($str_blog_title != ""){ print " : ".$str_blog_title.""; } ?></h1><hr/>
                <ol class="breadcrumb thumbnail-margin">
                    <?php if($str_blog_title != ""){ ?>
                        <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL."/blog"; ?>"><?php print $str_blog_title; ?></a></li>
                    <?php } ?>
                </ol>
            </div>
        </div>
        <?php 
        $int_cnt = 0;
        //print $rs_list->Count();
        while($rs_list_blog->eof() == false)
        { ?>
            <div class="row padding-10">
                
                <div class="col-md-12  col-lg-12">
                    <h4><i class="fa fa-file-text"></i>&nbsp;&nbsp;<?php print date("M d, Y H:i:s", strtotime($rs_list_blog->Fields("submitdatetime"))); ?> EST</h4>
                    <?php if($rs_list_blog->Fields("largeimagefilename") != "") { ?>
                    <p><img class="img-responsive" src="<?php print $str_img_path.$rs_list_blog->Fields("largeimagefilename"); ?>" alt="<?php print $rs_list_blog->Fields("title"); ?>" title="<?php print $rs_list_blog->Fields("title"); ?>"></p>
                    <?php } ?>
                    <?php if($rs_list_blog->Fields("title") != "") { ?>
                        <h3 class=""><?php print $rs_list_blog->Fields("title"); ?></h3>
                    <?php } ?>
                    <?php if($rs_list_blog->fields("description") != "<br>" && $rs_list_blog->fields("description") != "") {?>
                    <p class=""><?php print $rs_list_blog->fields("description"); ?></p>
                    <?php } ?>
                    <p>
                            <?php
                            $str_img_alt = "";
                            if($rs_list->fields("url") != "")
                            {
                                if($rs_list->fields("url") != "") 
                                    { $str_img_alt = $rs_list->fields("urltitle"); }
                                else
                                    { $str_img_alt = $rs_list->fields("url"); } 
                            } 

                            if($rs_list->fields("url") != "") {
                            ?>    
                            &nbsp;&nbsp;<i class="fa fa-globe"></i>&nbsp;<?php print(DisplayWebsiteURL($rs_list_blog->fields("url"),$rs_list_blog->fields("urltitle"),$rs_list_blog->fields("openinnewwindow"), $str_img_alt, '', '', ''));?>
                            <?php } ?>    
                    </p>            
                </div>
            </div><hr/>
        <?php 
        $int_cnt++;
        $rs_list_blog->MoveNext();
        } ?>
    <br/>
    <a name="blogleavecomment" id="blogleavecomment"></a>
    <div class="row padding-10">
        <div class="col-md-12  col-lg-12">
            <div class="well">
                <h4 class="nopadding">Leave a Comment</h4><br/>
                <form id="frm_add" name="frm_add" novalidate>
                    <div class="row padding-10">
                        <div class="col-md-4  col-lg-4">
                            <div class="form-group control-group">
                                <div class="controls">
                                    <label>Name</label><span class="text-help-form">&nbsp;*</span>
                                    <input type="text" id="txt_name" name="txt_name" class="form-control" required data-validation-required-message="<?php print $STR_PLACEHOLDER_NAME; ?>" placeholder="<?php print $STR_PLACEHOLDER_NAME; ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4  col-lg-4">
                            <div class="form-group control-group">
                                <div class="controls">
                                    <label>Location</label><span class="text-help-form">&nbsp;</span>
                                    <input type="text" id="txt_location" name="txt_location" class="form-control" placeholder="<?php print $STR_PLACEHOLDER_TEXT; ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            <div class="form-group control-group">
                                <label>Email ID</label><span class="text-help-form">&nbsp;</span>
                                <div class="controls">
                                    <input type="email" id="txt_emailid" name="txt_emailid" class="form-control" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-12  col-lg-12">
                            <div class="form-group control-group">
                                <label>Message</label><span class="text-help-form">&nbsp;*</span>
                                <div class="controls">
                                    <textarea id="ta_desc" name="ta_desc" class="form-control" rows="2" required data-validation-required-message="<?php print $STR_PLACEHOLDER_MESSAGE; ?>" placeholder="<?php print $STR_PLACEHOLDER_MESSAGE; ?>"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-12  col-lg-12">
                            <div class="control-group form-group">
                                <label>Secret Code</label><span class="text-help-form">&nbsp;*</span>
                                <div class="controls input-group">
                                    <input type="text" class="form-control" id="txt_secretcode" name="txt_secretcode" required data-validation-required-message="<?php print $STR_PLACEHOLDER_SECRET_CODE; ?>" placeholder="<?php print $STR_PLACEHOLDER_SECRET_CODE; ?>" minlength="7" maxlength="7">
                                    <span class="input-group-addon input-group-addon-no-padding nopadding">
                                        <img src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/includes/get_unique_image.php" border="0" alt="Captcha" width="130" height="29">
                                    </span>
                                </div>
                                <p class="help-block"></p>                            
                            </div>
                        </div>
                    </div>
                    
                    
                    <?php //print_r($_GET);?>
                    <div id="success"></div>
                    <input type="hidden" id="hdn_blogpkid" name="hdn_blogpkid" value="<?php print $int_pkid; ?>" />
                    <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i>&nbsp;Submit</button>
                </form>
            </div>
            <br/>
                <?php if($rs_list->Count() > 0) { ?>
                <div class="row padding-10">
                    <div class="col-md-12  col-lg-12">
                        <h3><i class="fa fa-commenting" aria-hidden="true"></i> Comments (<?php print $rs_list->Count();?>)</h3>
                    </div>
                </div><hr/>
                <?php } ?>
                <?php 
                $int_cnt = 0;
                //print $rs_list->Count();
                while($rs_list->eof() == false)
                {
                ?>
                    
                    <?php if($rs_list->fields("description") != "<br>" && $rs_list->fields("description") != "") {?><p class=""><?php print $rs_list->fields("description"); ?></p><?php } ?>
                    <p class="text-help">
                        <?php if($rs_list->fields("personname") != "") { ?><i class="fa fa-user" aria-hidden="true"></i>&nbsp;<?php print $rs_list->Fields("personname"); ?><?php } ?>
                        <?php if($rs_list->fields("location") != "") { ?>&nbsp;&nbsp;<span><i class="fa fa-map-marker"></i>&nbsp;<?php print $rs_list->Fields("location"); ?></span><?php } ?>
                        <?php if($rs_list->fields("submitdatetime") != "") { ?>&nbsp;&nbsp;<span><i class="fa fa-calendar"></i>&nbsp;<?php print date("M d, Y h:i:s", strtotime($rs_list->Fields("submitdatetime"))); ?> EST</span><?php } ?>
                    </p><hr/>
                <?php 
                $int_cnt++;
                $rs_list->MoveNext();
                } ?>
            </div>
        </div>    
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/blog_details.js"></script>
</body>
</html>
