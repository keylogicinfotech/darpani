//console.log("in product_listing file");
$(function() {
    $("form").submit(function(e){
        e.preventDefault();
		var form = $(this);
		var id = form.attr('id');
		//console.log("in form submit");
		$.ajax({
            url: "../user/product_addtocart_p.php", // For Product Listing Page
            type: "POST",
            data: form.serialize(),
            cache: false,        
            success: function(data) 
            { 
                console.log(data);
				alert(data);
				var responseText=JSON.parse(data);
                show_popup(responseText.message);
			},	
			error: function (jqXHR, exception) {
				console.log(jqXHR);
			},		
        });
    }); // End of $(function())

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });

 }); // End of $(function()) 
function show_popup(msg)
{
	$("#show_popup").text(msg);
	$("#show_popup").show();
	setTimeout(function() { $("#show_popup").fadeOut(800); }, 5000);
}

$("#show_popup").click(function(){
	$("#show_popup").hide();
});
