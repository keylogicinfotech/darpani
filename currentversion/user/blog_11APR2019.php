<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
//Include files
/*
if(!isset($_SESSION["freememberid"]) || $_SESSION["freememberid"] == "")
{
    header('Location: ../signin?blgmdl=false');
    exit();
}
*/
$STR_TITLE_BLOG = "Blog";
$UPLOAD_IMG_PATH = "../mdm/blog/";
$sel_blog_list = "";
$sel_blog_list = "SELECT * FROM tr_blog WHERE approved = 'YES' AND visible = 'YES' AND blogtype = 'B' ORDER BY submitdatetime DESC, title ASC";
//print $sel_blog_list;exit;
$rs_list = GetRecordSet($sel_blog_list);

///--------------------------------------------------------------------
# Get Page Title for SEO
$str_select_query="";
$str_select_query="SELECT titletag FROM t_page_metatag WHERE visible='YES' AND pagekey='PG_MT_BLOG'";
$rs_title_list=GetRecordset($str_select_query);
///--------------------------------------------------------------------
$str_member_flag=false;
if(isset($_SESSION["freememberpkid"]) && $_SESSION["freememberpkid"]!="")
{
    $str_user_type = "MEMBER";
    $str_member_flag = true;
}
?>
<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_title_list->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag("PG_MT_BLOG")); ?>
    <link href="./../css/bootstrap.min.css" rel="stylesheet">
    <link href="./../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="./../css/user.css" rel="stylesheet">    
    <link href="./../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="./../css/user.css" rel="stylesheet">    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
<div class="container center_bg"><br/>
    <div class="row padding-10">
        <div class="col-md-12">
            <h1 align="center"><b><?php print $STR_TITLE_BLOG; ?></b></h1>
            <hr class="star-primary">
        </div>
    </div>
    <?php 
    $int_cnt = 0;
    //print $rs_list->Count();
    while($rs_list->eof() == false)
    {
    ?>
    <div class="row padding-10">
        <div class="col-md-1 text-center">
            <p class="text-primary"><i class="fa fa-file-text fa-4x"></i></p>
            <p><?php print date("M d, Y H:i:s", strtotime($rs_list->Fields("submitdatetime"))); ?> EST</p>
        </div>
        <?php if($rs_list->Fields("largeimagefilename") != "") { 
		$str_div_first = "col-md-3";
		$str_div_second = "col-md-8";
        } else { 
		$str_div_first = "col-md-0";
		$str_div_second = "col-md-11";	
	} ?>
        <div class="<?php print($str_div_first); ?>">
            <?php if($rs_list->Fields("largeimagefilename") != "") { ?>
            <p><img class="img-responsive img-hover img-thumbnail" src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("largeimagefilename"); ?>" alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>"></p>
            <?php } ?>
        </div>
	<div class="<?php print($str_div_second); ?>">
            <?php /* ?><?php if($rs_list->Fields("largeimagefilename") != "") { ?>
            <p><img class="img-responsive img-hover img-thumbnail" src="<?php print $BLOG_IMAGE_PATH_ABSOLUTE.$rs_list->Fields("largeimagefilename"); ?>" alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>"></p>
            <?php } ?><?php */ ?>
            <?php if($rs_list->Fields("title") != "") { ?>
                <h3 class="heading"><?php print $rs_list->Fields("title"); ?></h3>
            <?php } ?>
                
            
            <?php if($rs_list->fields("description") != "<br>" && $rs_list->fields("description") != "") {?>
            <p class=""><?php print $rs_list->fields("description"); ?></p>
            <?php } ?>
            <?php $sel_mdl_details = "";
            $sel_mdl_details = "SELECT shortenurlkey, shortenurlvalue, countrypkid FROM t_user WHERE pkid = ".$rs_list->Fields("masterpkid");
//            print $sel_mdl_details;
            $rs_mdl_details = GetRecordSet($sel_mdl_details);
            ?>
            <p>
                <?php if(isset($_SESSION["freememberid"]) && $_SESSION["freememberid"] != "") { ?>
                    <i class="fa fa-user"></i>&nbsp;<a href="<?php print $rs_list->Fields("title"); ?>" title="Click to view this profile" target="_self"><?php print $rs_list->Fields("title"); ?></a> <b class="text-help"></b>&nbsp;&nbsp;
                <?php } else { ?>
                    <i class="fa fa-user"></i>&nbsp;<?php print $rs_mdl_details->Fields("shortenurlkey"); ?>&nbsp;&nbsp;
                <?php } ?>
                
                <?php
                $str_img_alt = "";
                if($rs_list->fields("url") != "")
                {
                    if($rs_list->fields("url") != "") 
                        { $str_img_alt = $rs_list->fields("urltitle"); }
                    else
                        { $str_img_alt = $rs_list->fields("url"); } 
                } 

                if($rs_list->fields("url") != "") {
                ?>    
                <i class="fa fa-globe"></i>&nbsp;<?php print(DisplayWebsiteURL($rs_list->fields("url"),$rs_list->fields("urltitle"),$rs_list->fields("openinnewwindow"), $str_img_alt, '', '', ''));?>
                <?php } ?>    


                   
                        <?php if($rs_mdl_details->Fields("countrypkid") > 0) { ?>
                        &nbsp;&nbsp;<i class="fa fa-map-marker"></i>&nbsp;
                                        <?php 
                                        $sel_cntry_details = "";
                                        $sel_cntry_details = "SELECT countryname, photofilename FROM t_site_country WHERE countrypkid=".$rs_mdl_details->Fields("countrypkid");
                                        $rs_cntry_details = GetRecordSet($sel_cntry_details);
                                        ?>
                                        <img src="<?php print $UPLOAD_SITE_COUNTRY_PATH.$rs_cntry_details->Fields("photofilename"); ?>" title="<?php print $rs_cntry_details->Fields("countryname"); ?>" alt="<?php print $rs_cntry_details->Fields("countryname"); ?>" />
                                    <?php } ?> 
                        <?php //print $rs_list->Fields("location"); ?>

                
            </p>
            <?php
            $sel_qry_cnt_comments = "";
            $sel_qry_cnt_comments = "SELECT count(*) AS noofcomments FROM tr_blog WHERE blogpkid = ".$rs_list->Fields("blogpkid")." AND blogtype='C' AND visible='YES' AND approved='YES'";
            $rs_cnt_comments=GetRecordSet($sel_qry_cnt_comments);
            
            ?>
            <?php /* ?><a class="btn btn-success" href="./blog-<?php print $rs_list->Fields("blogpkid");?>" title="Click to view blog details">View Comments (<?php print $rs_cnt_comments->Fields("noofcomments"); ?>)&nbsp;&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></a><?php */ ?>
	<?php //if($str_member_flag == true || $rs_list->Fields("masterpkid") == $_SESSION["modelpkid"]) { ?><a class="btn btn-warning" href="./blog_detail.php" title="Click to leave a comment">Leave a Comment&nbsp;&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></a><?php //} ?>
        </div>
    </div>
    <hr>
    <?php 
    $int_cnt++;
    $rs_list->MoveNext();
    } ?>
    
</div>    
<script src="<?php print(GetCurrentPathRespectToUser());?>includes/jquery.min.js"></script>
<?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
<script language="JavaScript" src="./../js/functions.js" type="text/JavaScript"></script>
 <script language="JavaScript" src="./../js/bootstrap.min.js"></script>
<script language="JavaScript" src="./../js/jqBootstrapValidation.js"></script>
<script language="JavaScript" src="./../js/stoprightclick.js"></script>
</body>
</html>
