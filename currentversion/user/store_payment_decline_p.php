<?php
/*
	Module Name	:-  product payment Decline
	File Name	:- product_payment_decline_p.php
	Create Date	:- 14-07-2006
	Initially Created by :- 0014
*/
#------------------------------------------------------------------------------------------
#	include files
//	session_start();
	include "../includes/validatesession.php";
        include "../includes/validate_session_user.php";
	include "../includes/configuration.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
        include "./store_item_config.php";
//	include "./includes/http_to_https.php";		

#------------------------------------------------------------------------------------------
	$item_sessionid="";
	$item_uniqueid="";
	//print_r($_POST);exit;
	# Use below code only when doing testing on LOCAL server - START
	/*if(isset($_SESSION['sessionid']))
	{
		$item_sessionid=$_SESSION['sessionid'];
	}
	if(isset($_SESSION['uniqueid']))
	{
		$item_uniqueid=$_SESSION['uniqueid'];
	}*/
	# Use below code only when doing testing on LOCAL server - END

	# Use below code only when doing testing on ACTUAL server - START
	if(isset($_POST["itemsessionid"]) && trim($_POST["itemsessionid"])!="")
	{
		$item_sessionid=trim($_POST["itemsessionid"]);
	}

	if(isset($_POST["itemuniqueid"]) && trim($_POST["itemuniqueid"])!="")
	{
		$item_uniqueid=trim($_POST["itemuniqueid"]);
	}
	# Use below code only when doing testing on ACTUAL server - END

	$str_userid="";
	if(isset($_POST["userid"]) && trim($_POST["userid"])!="")
	{
		$str_userid=trim($_POST["userid"]);
	}

	$int_userpkid="";
	if(isset($_POST["userpkid"]) && trim($_POST["userpkid"])!="")
	{
		$int_userpkid=trim($_POST["userpkid"]);
	}
	
	$str_login_id="";
	$str_login_password="";

	$str_fname="";
	$str_lname="";
	$str_address="";
	$str_city="";
	$str_state="";
	$str_country="";
	$str_zipcode="";
	$str_phone="";	
	$str_email_id="";
	$str_ipaddress="";

	$str_denial_id="";
	$str_cancel_reason="";

	$str_response_digest="";
	$str_salt="";
	$int_trans_status=""; // 1 means pament is process SUCCESSFULLY, 0 means payment is DENIED

	
	if(isset($_POST["denialId"]) && trim($_POST["denialId"])!="")
	{
		$str_denial_id=RemoveQuote(trim($_POST["denialId"]));
	}
	
	/*if(isset($_POST["salt"]) && trim($_POST["salt"])!="")
	{
		$str_salt=RemoveQuote(trim($_POST["salt"]));
	}*/	
	

	if(isset($_POST["username"]) && trim($_POST["username"])!="")
	{
		$str_login_id=RemoveQuote(trim($_POST["username"]));
	}
	if(isset($_POST["password"]) && trim($_POST["password"])!="")
	{
		$str_login_password=RemoveQuote(trim($_POST["password"]));
	}	
	if(isset($_POST["email"]) && trim($_POST["email"])!="")
	{
		$str_email_id=RemoveQuote(trim($_POST["email"]));
	}
	if(isset($_POST["reasonForDecline"]) && trim($_POST["reasonForDecline"])!="")
	{
		$str_cancel_reason=RemoveQuote(trim($_POST["reasonForDecline"]));
	}	
	/*if(isset($_POST["price"]) && trim($_POST["price"])!="")
	{
		$str_price=RemoveQuote(trim($_POST["price"]));
	}*/
	if(isset($_POST["customer_fname"]) && trim($_POST["customer_fname"])!="")
	{
		$str_fname=RemoveQuote(trim($_POST["customer_fname"]));
	}
	if(isset($_POST["customer_lname"]) && trim($_POST["customer_lname"])!="")
	{
		$str_lname=RemoveQuote(trim($_POST["customer_lname"]));
	}
	if(isset($_POST["address1"]) && trim($_POST["address1"])!="")
	{
		$str_address=RemoveQuote(trim($_POST["address1"]));
	}
	if(isset($_POST["state"]) && trim($_POST["state"])!="")
	{
		$str_state=RemoveQuote(trim($_POST["state"]));
	}
	if(isset($_POST["city"]) && trim($_POST["city"])!="")
	{
		$str_city=RemoveQuote(trim($_POST["city"]));
	}
	if(isset($_POST["country"]) && trim($_POST["country"])!="")
	{
		$str_country=RemoveQuote(trim($_POST["country"]));
	}
	if(isset($_POST["zipcode"]) && trim($_POST["zipcode"])!="")
	{
		$str_zipcode=RemoveQuote(trim($_POST["zipcode"]));
	}
	/*if(isset($_POST["phone_number"]) && trim($_POST["phone_number"])!="")
	{
		$str_phone=RemoveQuote(trim($_POST["phone_number"]));
	}*/
	if(isset($_POST["ip_address"]) && trim($_POST["ip_address"])!="")
	{
		$str_ipaddress=RemoveQuote(trim($_POST["ip_address"]));
	}
	
	$str_ship_add="";
	if(isset($_POST["ta_ship_add"]) && trim($_POST["ta_ship_add"])!="")
	{
		$str_ship_add=RemoveQuote(trim($_POST["ta_ship_add"]));
	}
	
	/*#########	CHECK product Exist Or Not And Get Price Value and model name from the table
	$str_price_value="";
	$str_photoset_title="";
	$str_model_name="";
	
	$str_select="SELECT a.photosettitle,b.modelname,c.pricevalue FROM tr_mdl_photoset a ";
	$str_select.=" LEFT JOIN t_model b ON a.modelpkid=b.modelpkid ";
	$str_select.=" LEFT JOIN t_ccbill_price c ON c.pricepkid=a.pricepkid ";
	$str_select.=" WHERE a.allowpurchase='YES' AND a.photosetpkid=".$int_photosetpkid." AND a.modelpkid=".$int_modelpkid." AND a.pricepkid=".$int_pricepkid;
	$rs_list=GetRecordset($str_select);
	if($rs_list->eof()==false)
	{
		$str_price_value=$rs_list->fields("pricevalue");
		$str_model_name=$rs_list->fields("modelname");
		$str_photosettitle=$rs_list->fields("photosettitle");
	}
	else
	{
		exit;
	}*/

	
	#########	CHECK Member Exist Or Not And member id from the table
	/*$str_membername_name="";	
	$str_select="SELECT loginid FROM t_freemember WHERE memberpkid=".$int_userpkid;
	$rs_member_list=GetRecordset($str_select);
	if($rs_member_list->eof()==false)
	{
		$str_membername_name=$rs_member_list->fields("loginid");
	}
	else
	{
		exit;
	}*/
	
	$str_purchase_date=date("Y-m-d H:i:s");
	$expire_date=date("Y-m-d H:i:s",mktime(23, 59, 59, date("m")  , date("d")+$INT_PRODUCT_DOWNLOAD_LIMIT_DAY, date("Y")));

	$str_purchase_day="";
	$str_purchase_month="";
	$str_purchase_year="";
	
	/// find the day, month and year from the transaction date
	$str_purchase_day=date("d",strtotime($str_purchase_date));
	$str_purchase_month=date("m",strtotime($str_purchase_date));
	$str_purchase_year=date("Y",strtotime($str_purchase_date));
	
	$str_download="YES";
	$str_addedby="YES";		


	#########	Get individual product data from t_sessioncart table
	$str_price_value="";
	$str_product_title="";
	$str_model_name="";
	
	$str_select="SELECT * FROM t_store_sessioncart WHERE sessionid='".$item_sessionid. "' AND uniqueid='".$item_uniqueid."'";
//	$str_select="SELECT * FROM t_sessioncart WHERE sessionid='".$item_sessionid. "' AND uniqueid='".$item_uniqueid."' AND freememberpkid=".$int_userpkid;
        //print $str_select; exit;
	$rs_cart_list=GetRecordset($str_select);
	
	$int_ps_quantity=$rs_cart_list->Count(); // No. of records fetched in the recordset
	
	if($rs_cart_list->eof()==false)
	{
            while(!$rs_cart_list->eof())
            {	

                $str_insert="INSERT INTO t_store_cancel_purchase(userpkid, username, memberpkid, membername, productpkid, producttitle, catpkid, cattitle, subcatpkid, subcattitle, pricevalue, price, extendedprice,color,size,typemodel,quantity, shippingvalue,promocodepkid,finaldiscountedamount, emailid, denialid, reasonlog,";
                $str_insert.="firstname, lastname, address, city, state, country, zipcode, shippingaddress, ipaddress,";
                $str_insert.="purchasedatetime, inday, inmonth, inyear, phoneno, expiredate";
                $str_insert.=") VALUES (";
                $str_insert.=$rs_cart_list->fields("userpkid").",";
                $str_insert.="'".$rs_cart_list->fields("username")."',";
                $str_insert.=$rs_cart_list->fields("memberpkid").",";
                $str_insert.="'".$rs_cart_list->fields("membername")."',";
                $str_insert.=$rs_cart_list->fields("productpkid").",";
                $str_insert.="'".$rs_cart_list->fields("producttitle")."',";
                $str_insert.=$rs_cart_list->fields("catpkid").",";
                $str_insert.="'".$rs_cart_list->fields("cattitle")."',";
                $str_insert.=$rs_cart_list->fields("subcatpkid").",";
                $str_insert.="'".$rs_cart_list->fields("subcattitle")."',";
                $str_insert.=$rs_cart_list->fields("price").",";
                $str_insert.=$rs_cart_list->fields("price").",";
                $str_insert.=$rs_cart_list->fields("extendedprice").",";
                $str_insert.="'".$rs_cart_list->fields("color")."',";
                $str_insert.="'".$rs_cart_list->fields("size")."',";
                $str_insert.="'".$rs_cart_list->fields("typemodel")."',";
                $str_insert.="".$rs_cart_list->fields("quantity").",";
                $str_insert.=$rs_cart_list->fields("dshippingvalue").",";
                $str_insert.=$rs_cart_list->fields("promocodepkid").",";
                $str_insert.=$rs_cart_list->fields("finaldiscountedamount").",";
                $str_insert.="'".ReplaceQuote($str_email_id)."',";
                $str_insert.="'".ReplaceQuote($str_denial_id)."',";
                $str_insert.="'".ReplaceQuote($str_cancel_reason)."',";
                $str_insert.="'".ReplaceQuote($str_fname)."',";
                $str_insert.="'".ReplaceQuote($str_lname)."',";
                $str_insert.="'".ReplaceQuote($str_address)."',";
                $str_insert.="'".ReplaceQuote($str_city)."',";
                $str_insert.="'".ReplaceQuote($str_state)."',";
                $str_insert.="'".ReplaceQuote($str_country)."',";
                $str_insert.="'".ReplaceQuote($str_zipcode)."',";
                $str_insert.="'".ReplaceQuote($str_ship_add)."',";
                $str_insert.="'".ReplaceQuote($str_ipaddress)."',";
                $str_insert.="'".ReplaceQuote($str_purchase_date)."',";
                $str_insert.=$str_purchase_day.",";
                $str_insert.=$str_purchase_month.",";
                $str_insert.=$str_purchase_year.",";
                $str_insert.="'".ReplaceQuote($str_phone)."',";
                $str_insert.="'".ReplaceQuote($expire_date)."'";
                $str_insert.=")";

                //print $str_insert; exit;
                ExecuteQuery($str_insert);

                $rs_cart_list->MoveNext();
            }
            
	/*else
	{
		exit;
	}*/

	//$str_query_delete="DELETE FROM t_sessioncart WHERE sessionid='".$item_sessionid. "' AND uniqueid='".$item_uniqueid."' AND freememberpkid=".$int_userpkid;

	#########	Once product purchase date stored in t_product_purchase table, DELETE those data from t_sessioncart table
	//$str_query_delete="DELETE FROM t_sessioncart WHERE sessionid='".$item_sessionid. "' AND uniqueid='".$item_uniqueid."'";
	//ExecuteQuery($str_query_delete);

	}
        
        ## Below code to delete senders which are selected before while add to cart during cancellation
        $str_query_select = "";
        $str_query_select="SELECT * ";
        $str_query_select.=" FROM  t_store_cancel_purchase ";
        //$str_query_select.=" LEFT JOIN t_store a ON a.pkid=m.productpkid AND a.visible='YES'";
        //$str_query_select.=" LEFT JOIN tr_store_photo b ON a.pkid=b.masterpkid AND b.setasfront='YES' AND b.visible='YES'";
        //$str_query_select.=" LEFT JOIN t_store_price c ON a.pricepkid=c.pricepkid ";
        //$str_query_select.=" LEFT JOIN t_store_subcat d ON a.subcatpkid=d.subcatpkid ";
        $str_query_select.=" WHERE userpkid=".$int_userpkid." AND purchasedatetime='".ReplaceQuote($str_purchase_date)."'";
        $str_query_select.=" ORDER BY purchasedatetime desc";
        //print $str_query_select; exit;
        $rs_list = GetRecordSet($str_query_select);
            //print $rs_list->Fields("purchasepkid");exit;
        if($rs_list->Count() > 0)
        {
            $str_query_delete = "";
            $str_query_delete = "DELETE FROM t_store_purchase_addressbook WHERE masterpkid=".$rs_list->Fields("productpkid")." AND userpkid=".$rs_list->Fields("userpkid");
            ExecuteQuery($str_query_delete);
            
        }
        
        
	//exit;
#------------------------------------------------------------------------------------------
CloseConnection();
Redirect("./store_item_checkout_msg.php?st=d");
//Redirect("member_home.php");
exit();
?>