<?php 
/*
File Name  :- user_unfavorite_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../user/product_config.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";
//include "./../includes/lib_file_upload.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
//$str_title_page = "User Favorite List";
//$STR_TITLE_USER_FAVORITE = "User Favorite List";
$INT_USER_SESSION_PKID=$_SESSION["userpkid"];
$STR_USER_SESSION_LOGINID=$_SESSION["userpkid"];

$str_db_table_name = "tr_user_favorite_item";
#----------------------------------------------------------------------
//Initializing Variables.
$int_itempkid=0;
if(isset($_GET["itempkid"]) && trim($_GET["itempkid"])!="")
{
    $int_itempkid = trim($_GET["itempkid"]);
}
#---------------------------------------------------------------------
//Select Query to Get records
$str_query_delete="";
$str_query_delete="DELETE FROM " .$str_db_table_name. " WHERE userpkid=".$INT_USER_SESSION_PKID." AND itempkid= ".$int_itempkid;
//print($str_query_delete);exit;
ExecuteQuery($str_query_delete);
CloseConnection();
Redirect("./user_favorite.php");

?>
