<?php
$STR_TITLE_PAGE = "Webmaster List";
$STR_TITLE_HOME = "Webmaster Home";
$STR_TITLE_TOOLS = "Affiliate Tools";
$STR_TITLE_GALLERY = "Hosted Gallery";
$STR_TITLE_GALLERY_VIEW = "Gallery View";

$UPLOAD_IMG_PATH = "../../mdm/webmaster/";
$INT_IMG_WIDTH = 300;
$INT_IMG_HEIGHT = 250;

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "cms_webmaster"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY displayorder DESC, title"; 

global $STR_DB_TABLE_NAME_BANNER;
$STR_DB_TABLE_NAME_BANNER = "t_banner"; 

global $STR_DB_TABLE_NAME_GALLERY;
$STR_DB_TABLE_NAME_GALLERY = "t_promogallery"; 

global $STR_DB_TABLE_NAME_PHOTO;
$STR_DB_TABLE_NAME_PHOTO = "tr_promogallery_photo";

global $STR_DB_TABLE_NAME_METATAGE;
$STR_DB_TABLE_NAME_METATAGE = "t_page_metatag";

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "cms_webmaster_home";

global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/webmaster.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/webmaster_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";	
?>
