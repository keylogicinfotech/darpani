<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/lib_xml.php";
include "./../includes/http_to_https.php";	
include "./../includes/unique_site_view_count.php";
include "./webmaster_config.php";
#----------------------------------------------------------------------
$str_title_page_metatag = 'PG_AFFILIATE';
#getting header image from xmlfile.
/*$fp=openXMLfile($UPLOAD_XML_PATH."header.xml");
$HEADER_FILE_NAME=getTagValue($HEADER_WEBMASTER,$fp);
closeXMLfile($fp);*/
$INT_BANNER = 300;
#------------------------------------------------------------------------------
if(!isset($_SERVER['HTTP_REFERER']))
{
    Redirect("./webmaster.php");
    exit();
}	
#------------------------------------------------------------------------------	
##get Query String Data
$str_aid="";
$str_pt="";
//if(isset($_POST['txt_login']))
//{
//	$str_aid=trim($_POST['txt_login']);
//}
if(isset($_POST['pt']))
{
	//$str_pt=trim($_POST['pt']);
    if(isset($_POST['txt_login']))
    {
        $str_aid=trim($_POST['txt_login']);
        // Set Affiliate ID cookies so it can be accessed from all pages
        setcookie('affid',$str_aid,0,'/');
    }
}
else 
{
    if(isset($_POST["aid"])==true)
    {
        $str_aid=trim($_POST["aid"]);
    }	
}

//print $str_pt."<br/>";
//print $str_aid;
//exit;
#------------------------------------------------------------------------------	
# Get Page Title for SEO
$str_query_select="";
$str_query_select="SELECT titletag FROM " .$STR_DB_TABLE_NAME_METATAGE. " WHERE visible='YES' AND pagekey='" .$str_title_page_metatag. "'";
//print $str_query_select;
$rs_list_mt=GetRecordset($str_query_select);
#------------------------------------------------------------------------------	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>

<?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
<!-- Bootstrap Core CSS -->
<link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />     
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?php print(GetCurrentPathRespectToUser());?>includes/stoprightclick.js"></script>
<script type="text/javascript">
function HighlightAll(theField) 
{
	var tempval=eval("document."+theField)
	tempval.focus()
	tempval.select()
	var copytoclip=1
	/*
	if (copytoclip==1)
	{		
		therange=tempval.createTextRange()
		therange.execCommand("Copy")
		window.status="Contents highlighted and copied to clipboard!"
		setTimeout("window.status=''",2400);
		copytoclip=0;
	}*/
}
</script>
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
<!-- Page Content -->
<div class="container center-bg">
    <div class="row padding-10">
        <div class="col-lg-12 col-md-12">
            <h1 align="right"><?php print $STR_TITLE_AFFILIATE_TOOL; ?></h1>
            <hr/>
        </div>
    </div>
    <div class="row padding-10">
        <div class="col-md-12">
            <?php include("./webmaster_panel.php");?>
        </div>
    </div>
    <div class="row padding-10">
        <div class="col-lg-12 col-md-12">
            <ul id="myTab" class="nav nav-tabs nav-justified">
                <li class="active" id="hometab"><a href="#home" data-toggle="tab"  title="Click to view Affiliate Home">Home</a></li>
                <li class="" id="linkingcodetab"><a href="#linking-code" data-toggle="tab"  title="Click to view Linking Codes">Linking Code</a></li>
                <li class="" id="gallerytab"><a href="#hosted-galleries" data-toggle="tab" title="Click to view Hosted Galleries">Hosted Galleries</a></li>
                <li class="" id="bannertab"><a href="#banners" data-toggle="tab"  title="Click to view Banners">Banners</a></li>
                <li class="" id="statetab"><a href="http://affiliateadmin.ccbill.com"  target="_blank"  title="Click to view Affiliate Stats">Stats</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="home">
                <br/>
                <div class="row padding-10">
                    <div class="col-lg-12 col-md-12">
                        <h4 class="">Welcome to Affiliate Program! To begin, go to any of tab listed above:</h4>
                        <a href="#linking-code" data-toggle="tab"  name="linkinkg" onClick="return calllinkingcode();">Get Linking Codes</a>
                        <script>function calllinkingcode(){document.getElementById('linkingcodetab').className="active";document.getElementById('hometab').className="";}</script><br/>
                        <a href="#hosted-galleries" data-toggle="tab"   onClick="return callgallery();">Get Hosted Galleries</a>
                        <script>function callgallery(){document.getElementById('gallerytab').className="active";document.getElementById('hometab').className="";}</script><br/> 
                        <a href="#banners" data-toggle="tab"   onClick="return callbanner();">Get Banners</a>
                        <script>function callbanner(){document.getElementById('bannertab').className="active";document.getElementById('hometab').className="";}</script><br/>
                        <a href="http://affiliateadmin.ccbill.com" target="_blank" >View Stats</a>
                        <?php /*?>&bull; Get Linking Codes<br/>
                        &bull; Get Hosted Galleries<br/>
                        &bull; Get Banners<br/>
                        &bull; Click to view Affiliate Program Statistics<br/><?php */?>
                        <h4 class="">Did you know?</h4>
                    <p class="">My affiliate program is part of the <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>" target="_blank"  title="Click to visit <?php print $STR_SITENAME_WITH_PROTOCOL; ?>"><?php print $STR_SITENAME_WITH_PROTOCOL; ?></a> network family.</p>
                    </div>
                </div>
                    </div>
                    <div class="tab-pane fade" id="linking-code">
                        <br/>
                        <div class="row padding-10">
                            <div class="col-lg-12 col-md-12"> 
                                <p class="text-help nopadding">Here is the linking code of my site with your affiliate ID inserted in code:</p>
                                <form name="frmlink">
                                    <textarea  name="ta_link1" wrap="on" rows="3" class="form-control">http://refer.ccbill.com/cgi-bin/clicks.cgi?CA=<?php print($STR_CCBILL_MEMBER_WEBMASTER_CLIENT_ACCOUNT);?>&PA=<?php print($str_aid);?>&HTML=<?php print($STR_SITENAME_WITH_PROTOCOL);?> </textarea><a href="javascript: void(0)" onClick="return HighlightAll('frmlink.ta_link1');" >Select text</a>
                                    <br/><br/><p class="text-help nopadding">You may also link to a specific page on my site. For example, if you want to link directly to my photo gallery page, your code would look like this:</p>
                                    <textarea  name="ta_link2" wrap="on" rows="3" class="form-control">http://refer.ccbill.com/cgi-bin/clicks.cgi?CA=<?php print($STR_CCBILL_MEMBER_CLIENT_ACCOUNT);?>&PA=<?php print($str_aid);?>&HTML=<?php print($STR_SITENAME_WITH_PROTOCOL);?>/user/photo_album.php</textarea> <a href="javascript: void(0)" onClick="return HighlightAll('frmlink.ta_link2');" >Select text</a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="hosted-galleries">
                        <div class="row padding-10">
                            <div class="col-lg-12 col-md-12">
                                <br/>
                                <p class="text-help nopadding">You can link directly to my hosted galleries by copying the referral codes from text box below. You can also download zip file of the galleries to use on your website for promotional purpose.</p>
                            </div>
                        </div>
                        <?php
                        $str_query_select = "";
                        $str_query_select = "SELECT pkid,title,createdate,zipfilename,noofvisiblephoto,zipfilesize"; 
                        $str_query_select=$str_query_select." FROM " .$STR_DB_TABLE_NAME_GALLERY. "";
                        $str_query_select=$str_query_select." WHERE visible='YES' AND noofvisiblephoto > 0 ORDER BY displayorder,createdate desc";
                        $rs_list_details=GetRecordset($str_query_select);
                        if($rs_list_details->EOF())
                        {	  
                            print ("<div class='row padding-10'><div class='col-lg-12 col-md-12 alert alert-danger' align='center'>Gallery not available at present.</div></div>");
                        }					
                        else
                        {
                        ?>
                        <form name="frmhostedgallery">
                            <?php
                            $int_cnt=1;
                            while(!$rs_list_details->EOF())
                            {
                            ?>
                            <div class="row padding-10">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <h4 class="nopadding"><?php print(MyhtmlEncode($rs_list_details->fields("title"))); ?> <span class="text-help">(<?php print(DDMMMYYYYFormat($rs_list_details->fields("createdate"))); ?>)</span></h4>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" align="right"><h4 class="nopadding"></h4>
                                    <?php $enc = GetEncryptId($rs_list_details->fields("pkid")); ?><a href="./user/webmaster_galleryview.php?pkid=<?php print($enc); ?>&aid=<?php print($str_aid);?>" class="btn btn-primary btn-xs no-outline" target="_blank" title="Click to view Promo Gallery"><i class="fa fa-eye" aria-hidden="true"></i></a>&nbsp;<i class="text-help">(<?php print($rs_list_details->fields("noofvisiblephoto"));?> images) </i>&nbsp;&nbsp;
                                    <?php 
                                    if($rs_list_details->fields("zipfilename")!="")
                                    {
                                        $enczip = GetEncryptId($rs_list_details->fields("pkid"));
                                    ?>
            <a href="./user/webmaster_zipdownload_p.php?pkid=<?php print($enczip);?>" title="click to Downlaod Promo Gallery Zip File"  class="btn btn-success btn-xs no-outline"><i class="fa fa-download" aria-hidden="true"></i></a>&nbsp;<i class="text-help">(<?php print(sizeoffile($rs_list_details->fields("zipfilesize")));?>)</i>
                                <?php 
                                    } 					
                                    ?>
                                </div>
                            </div>
                                    <?php
                                    $str_url=$_SERVER['HTTP_HOST'];
                                    $str_url=$str_url.$_SERVER['PHP_SELF'];
                                    $str_url=substr($str_url,0,strrpos($str_url,"/")+1);
                                    ?>

</p>                                <textarea  wrap="on" name="ta_link<?php print($int_cnt);?>" rows="3" class="form-control">http://refer.ccbill.com/cgi-bin/clicks.cgi?CA=<?php print($STR_CCBILL_MEMBER_WEBMASTER_CLIENT_ACCOUNT);?>&PA=<?php print($str_aid);?>&HTML=http://<?php print($str_url); ?>webmaster_galleryview.php?pkid=<?php print($enc); ?></textarea> <a href="javascript: void(0)" onClick="return HighlightAll('frmhostedgallery.ta_link<?php print($int_cnt);?>')" >Select text</a> 
                                    <br/><br/>

                                    <?php		
                                    $rs_list_details->MoveNext();				
                                    $int_cnt++;	
                                    }
                                    ?>
                            </form>
                                <?php
                                } //end of else
                                ?>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="banners">
                                <br/>
                                <div class="row padding-10">
                                    <div class="col-lg-12 col-md-12">
                                <?php
                                $str_query_select ="";
                                $str_query_select = "SELECT imagefilename,height,width,linkurl,linktext,displayasnew FROM " .$STR_DB_TABLE_NAME_BANNER. " ";	
                                $str_query_select = $str_query_select ." WHERE visible = 'YES'";
                                $str_query_select = $str_query_select ." ORDER BY displayorder ";		
                                $rs_list = GetRecordSet($str_query_select);	
                                if($rs_list->EOF())
                                {	
                                    print ("Banner Not Available");
                                }
                                else
                                {
                                ?>
                                <form name="frmbanner">
                                <?php 
                                $int_cnt=1;
                                $image_embed_tag_textarea="";
                                $image_tag_textarea="";
                                while(!$rs_list->EOF())
                                {									

                                    $str_ccbill_url="http://refer.ccbill.com/cgi-bin/clicks.cgi?CA=".$STR_CCBILL_MEMBER_WEBMASTER_CLIENT_ACCOUNT."&PA=".$str_aid;
                                    if($rs_list->fields("linkurl")=="")
                                    {
                                        $str_href = "http://" . $_SERVER['HTTP_HOST'];
                                    }
                                    else
                                    {
                                        $str_href = $rs_list->fields("linkurl");
                                    }
                                    
                                    if(trim($rs_list->fields('imagefilename'))!="") 
                                    { 
                                        $str_banner_ext = "";
                                        $path="";
                                        
                                        $str_banner_ext = GetExtension($rs_list->fields('imagefilename'));
                                        $path="./mdm/banner/".$rs_list->fields('imagefilename');
                                        //print $UPLOAD_BANNER_PATH."<br/>";
                                            $title=MyHtmlEncode($rs_list->fields("linktext"));
                                        
                                        if(strtoupper($str_banner_ext)!="SWF")
                                        {											
                                            $alt="Banner ".$int_cnt;
                                            $image_tag=ResizeImageTag($path,$rs_list->fields("height"),$rs_list->fields("width"),$INT_BANNER,$alt,$title,0);
                                            ?>
                                            <?php /*?><a href="<?php print($rs_list->fields("linkurl"));?>" target="_blank"><?php print($image_tag);?></a><?php */?>
                                            
                                          
                                              
                                            <a href="<?php print($rs_list->fields("linkurl"));?>" target="_blank"><img src="<?php  print($path); ?>" alt="<?php print($alt); ?>" border="0" class="img-responsive"/></a>
                                          <?php   if($rs_list->fields('displayasnew')=="YES") { ?>
                                              <div class="text-top-left">
                                             <?php print $STR_ICON_PATH_NEW; ?>                  
                                            </div>
                                          <?php } ?>
                                            <br/>
                                                <?php
                                                $image_tag=ResizeImageTag($path,$rs_list->fields("height"),$rs_list->fields("width"),$INT_BANNER,$alt,$title,1);
                                                $image_tag_textarea="<a href='".$str_ccbill_url."&HTML=".$str_href."' target='_blank'>".$image_tag."</a>";
                                                    }
                                                    else
                                                    {

                                                        $swf_object_tag=DisplaySWFFile($path,MyHtmlEncode($rs_list->fields("linkurl")),$rs_list->fields("width"),$rs_list->fields("height"),"_blank",$title,"#FFFFFF",0);
                                                        print($swf_object_tag);
                                                        $image_tag_textarea = DisplaySWFFile($path,$str_ccbill_url."&HTML=".$str_href,$rs_list->fields("width"),$rs_list->fields("height"),"_blank",$title,"#FFFFFF",1);
                                                        $image_embed_tag_textarea = DisplaySWFFileWithEmbedTag($path,$str_ccbill_url."&HTML=".$str_href,$rs_list->fields("width"),$rs_list->fields("height"),"_blank",$title,"#FFFFFF",1);
                                                }					
                                                        
                                                    }
                                                    ?>							
                                                    <?php 
                                                    $str_objecttag_msg="";
                                                    if($image_embed_tag_textarea!="")
                                                    {
                                                    $str_objecttag_msg="Copy & Paste Below code for site other than MySpace.";
                                                    ?>
                                                    <span class="">Copy & Paste this Code in your MySpace Profile.</span>
                                                    <textarea  name="ta_link<?php print($int_cnt);?>" wrap="on" rows="3" class="form-control"><?php print($image_embed_tag_textarea);?></textarea> <a  href="javascript: void(0)" onClick="return HighlightAll('frmbanner.ta_link<?php print($int_cnt);?>')" >Select text</a>
                                                <?php 
                                                }
                                                ?>
                                                <span class=""><?php print($str_objecttag_msg); ?></span>
                                                <textarea  name="ta_link1_<?php print($int_cnt);?>" wrap="on" rows="3" class="form-control"><?php print($image_tag_textarea);?></textarea> <a  href="javascript: void(0)" onClick="return HighlightAll('frmbanner.ta_link1_<?php print($int_cnt);?>')" >Select text</a><br/><br/>
                                        <?php
                                        $rs_list->MoveNext();
                                        $int_cnt++;
                                        }		
                                        ?>
                                        </form>
                                <?php			
                                }		
                                ?>
                                </div>  
                                </div>
                            </div>
                        <div class="tab-pane fade" id="stats"><br/>
                            <div class="row padding-10">
                                <div class="col-lg-12 col-md-12">
                                    <h4 class="text-info"><a href="http://affiliateadmin.ccbill.com" target="_blank"  title="Click here to view Affiliate Program Statistics">Click here to view Affiliate Program Statistics</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <br>
    </div>
<?php include($STR_USER_FOOTER_PATH); ?>
<div class="scrollup"></div>
<script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script> 
    <script type="text/javascript">
        $(document).ready(function(){ 

        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        }); 

        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });

        });
    </script>
</body>
</html>
