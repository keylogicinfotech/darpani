/*
  Jquery Validation using jqBootstrapValidation
   example is taken from jqBootstrapValidation docs 
  */
$(function() {

    $("#frm_giftcard input, #frm_giftcard select").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // something to have when submit produces an error ?
            // Not decided if I need it yet
        },
        
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var txt_giftcard = $("input#txt_giftcard").val();
            var hdn_userpkid = $("input#hdn_userpkid").val();
            var hdn_extprice = $("input#hdn_extprice").val();
            
            $.ajax({
                url: "./../user/product_giftcard_p.php",
                type: "POST",
                data: 
                {
                    txt_giftcard: txt_giftcard,
                    hdn_userpkid: hdn_userpkid,
                    hdn_extprice: hdn_extprice
                },
                cache: false,
                
                success: function(data) 
                { 
                    alert(data);
                    var $responseText=JSON.parse(data);
                    if($responseText.status == 'SUC')
                    {
                        // Success message
                        $('#successg').html("<div class='alert alert-success'>");
                        $('#successg > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                        $('#successg > .alert-success').append("<strong> " + $responseText.message + " </strong>");
                        $('#successg > .alert-success').append('</div>');

                        //clear all fields
                        setTimeout(function() { location.reload(); }, 2000);
                    }
                    else if($responseText.status == 'ERR')
                    {
                        // Fail message
                        $('#successg').html("<div class='alert alert-danger'>");
                        $('#successg > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                .append("</button>");
                        $('#successg > .alert-danger').append("<strong> " + $responseText.message + " ");
                        $('#successg > .alert-danger').append('</div>');
                        setTimeout(function() { location.reload(); }, 2000);
                    }
                },
				
		/*success: function() {
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Your contact inquiry has been submitted, we will contact you Soon. Thank you for communication.</strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + " it seems that my mail server is not responding...</strong> Could you please email me directly to <a href='mailto:info@websitename.com?Subject=Message_Me from websitename.com;>info@websitename.com</a> ? Sorry for the inconvenience!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },*/
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#successg').html('');
});
