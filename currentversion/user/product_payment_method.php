<?php 
/*
Create Date:- JUN-2019
Intially Create By :- 0015
Update History:
*/
//print "HELLO"; exit;
#----------------------------------------------------------------------
#Include files
//session_start();
include "./product_validatesession.php";
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_xml.php";
//include "../includes/lib_file_upload.php";
//include "../includes/lib_image.php";
include "../includes/lib_datetimeyear.php";
include "./product_config.php";

//include "./../includes/http_to_https.php";

#----------------------------------------------------------------------
$str_title_page = "";
$str_title_page = $STR_TITLE_PAGE_PAYMENT_METHOD;
//print $str_title_page; exit;

$str_xml_file_name_cms = "";
$str_xml_file_name_cms = "store_cms.xml";
#------------------------------------------------------------------------------------------------
# Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
	case("E"): $str_type = "E"; break;
	case("W"): $str_type = "W"; break;
    }
}
#	Get message text.

if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;        
    }
} 

$item_sessionid = "";
$item_uniqueid = "";
if(isset($_SESSION['sessionid']))
{
        $item_sessionid = $_SESSION['sessionid'];
}
if(isset($_SESSION['uniqueid']))
{
        $item_uniqueid = $_SESSION['uniqueid'];
}
if($item_sessionid == "" || $item_uniqueid == "")
{
    CloseConnection();
    Redirect($STR_SITENAME_WITH_PROTOCOL);
    exit();
}

$str_member_flag = false;
$int_userpkid = 0;
$str_username = "";
$int_giftcard_amount = 0;
if(isset($_SESSION['userpkid']) && $_SESSION['userpkid'] != "")
{
    $str_member_flag = true;
    $int_userpkid = $_SESSION['userpkid'];
    $str_username = $_SESSION['username'];
}

$str_allow_wholesaler_discount = "NO";
$str_allow_promocode = "NO";
if($int_userpkid > 0)
{
    $str_query_select = "";
    $str_query_select = "SELECT isusertype, giftcard FROM ".$STR_DB_TABLE_NAME_USER." WHERE pkid=".$int_userpkid;
    $rs_list_user = GetRecordSet($str_query_select);
    if(!$rs_list_user->EOF())
    {
        if($rs_list_user->Fields("isusertype") == "REGULAR")
        {
            $int_giftcard_amount = $rs_list_user->Fields("giftcard");
            $str_allow_promocode = "YES";
            $str_allow_wholesaler_discount = "NO";
        }
        
        if($rs_list_user->Fields("isusertype") == "WHOLESALER")
        {
            $int_giftcard_amount = $rs_list_user->Fields("giftcard");
            $str_allow_promocode = "NO";
            $str_allow_wholesaler_discount = "YES";
        }
        
    }
}
//print $str_allow_promocode;
#----------------------------------------------------------------------
#read main xml file
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = "";
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION3",$fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE3", $fp); 

#Select Query
$str_query_select="";
$str_query_select="SELECT * FROM  " .$STR_DB_TABLE_NAME_STATE." ORDER BY title ASC" ;
$rs_list_state=GetRecordSet($str_query_select);
$str_statevalues="";
while(!$rs_list_state->EOF()==true)
{
    $str_statevalues=$str_statevalues.$rs_list_state->fields("masterpkid").",".$rs_list_state->fields("pkid").",'".addslashes($rs_list_state->fields("title"))."',";
    $rs_list_state->MoveNext();
}
$str_statevalues=substr($str_statevalues,0,strlen($str_statevalues)-1);


$var_gift_sku = "FALSE";
$var_promo_sku = "FALSE";
$var_extprice = 0;
$hid_options = "";
$hid_serialno = "";

# Below recordset fetches all purchased related data from table for particular table.
$str_where = "";
$int_send_sessionid = "";
$int_send_uniqueid = "";

if(isset($_COOKIE["sessionid"]) && isset($_COOKIE["uniqueid"]) && $_COOKIE["sessionid"] != "" && $_COOKIE["uniqueid"] != "") 
{
    $str_where = " WHERE sessionid='".$_COOKIE["sessionid"]."' AND uniqueid='".$_COOKIE["uniqueid"]."' ";
    $int_send_sessionid = $_COOKIE["sessionid"];
    $int_send_uniqueid = $_COOKIE["uniqueid"];
}
else
{
    $str_where = " WHERE sessionid='".$item_sessionid."' AND uniqueid='".$item_uniqueid."' ";
    $int_send_sessionid = $item_sessionid;
    $int_send_uniqueid = $item_uniqueid;
}

$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_SESSION_CART.$str_where." ORDER BY serialno,sku,producttitle";
//print $strsql; exit;
$rs_list = GetRecordSet($str_query_select);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($str_title_page) ;?></title>
    <?php //print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />   
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/paypal_custom.css" rel="stylesheet" />   
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    
</head>
<body>
<?php include $STR_USER_HEADER_PATH2; ?>
    <div class="container center-bg" style="margin-top:25px;">
        <div class="row padding-10">
            <div class="col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>           
            </div>
        </div>
        <?php if($str_visible_cms == "YES") { ?>
            <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div  class="well"><p align="justify"><?php print($str_desc_cms);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        <?php 
        $int_subtotal = 0.00;
        $int_giftcard_discount = 0.00;
        $int_promocode_discount_amount = 0.00;
        $int_pincode = "";    
        $int_total_shipping_value = 0.00; 
        $int_wholesaler_discount = 0.00;
        $int_final_subtotal = 0;
        $int_discount = 0;
                            
        if($rs_list->Count() > 0)
        {
            
            while(!$rs_list->EOF()) 
            { 
                
                //$int_shipping_price = 0.00;
                //$int_shipping_price = ($rs_list->Fields("shippingvalue") * $rs_list->Fields("weight")) * $rs_list->Fields("quantity");
                //print $int_shipping_price."<br/>";
                $int_tailoring_price = 0.00;
                if($rs_list->Fields("tailoringprice") > 0) { 
                    $int_tailoring_price = ceil($rs_list->Fields("tailoringprice") / $int_conversionrate);
                } else { $int_tailoring_price = 0.00; }     

                $int_extended_total = 0; 
                $int_extended_total = ceil(($rs_list->Fields("price") + $int_tailoring_price) / $int_conversionrate) * $rs_list->Fields("quantity");  
//print $int_extended_total."<br/>";
                $int_subtotal = $int_subtotal + $int_extended_total;
                $int_giftcard_discount = $rs_list->Fields("giftcard") / $int_conversionrate;
                $int_promocode_discount_amount = $rs_list->Fields("finaldiscountedamount") / $int_conversionrate;
                $int_pincode = $rs_list->Fields("zipcode");
                $int_total_shipping_value = $int_total_shipping_value + ($rs_list->Fields("shippingvalue") / $int_conversionrate); 
                $int_wholesaler_discount = $rs_list->Fields("wholesalerdiscount") / $int_conversionrate;
                
                $rs_list->MoveNext();
            } $rs_list->MoveFirst();
        } 
        
        //print $int_total_shipping_value;
        
        $str_cod_available = "NO";
        $str_query_select = "";
        $str_query_select = "SELECT pincode FROM ".$STR_DB_TABLE_NAME_PINCODE." WHERE pincode='".$int_pincode."'";
        $rs_list_pincode = GetRecordSet($str_query_select);
        if($rs_list_pincode->Count() > 0)
        {
            $str_cod_available = "YES";
        }
        
        if($int_wholesaler_discount > 0 && $str_allow_wholesaler_discount == "YES") 
        { 
            $int_discount = ($int_subtotal * $int_wholesaler_discount) / 100;
        }
            
        
        $int_final_subtotal = $int_subtotal + $int_total_shipping_value - $int_giftcard_discount - $int_promocode_discount_amount - $int_discount;
        //print $str_cod_available;
        //$int_subtotal = $int_subtotal - $int_giftcard_discount;
        //$int_subtotal = $int_subtotal - $int_promocode_discount_amount;
        //print $int_subtotal;
        //print $int_giftcard_discount;   
            ## Have to check properly - Important
        ?>
        <form name="frm_shipping_address" id="frm_shipping_address" novalidate></form>
        <div class="row">
            <div class="col-md-4">
                <?php if($str_member_flag == true && $str_allow_promocode == "YES") { ?>
                <h4 class="nopadding"><b>Apply Promocode</b></h4><hr/>
                <form id="frm_promocode" name="frm_promocode" novalidate>
                    <div class="control-group nopadding">
                        <div class="controls ">
                            <div class="form-group input-group">
                            <input type="text" id="txt_promocode" name="txt_promocode" class="form-control text-left" placeholder="Apply Promocode Here" required data-validation-required-message="Please Enter Promocode." /> 
                            <input type="hidden" id="hdn_userpkid" name="hdn_userpkid" value="<?php print $int_userpkid; ?>" />
                            <input type="hidden" id="hdn_extprice" name="hdn_extprice" value="<?php print $int_subtotal; ?>" />
                            <span class="input-group-btn"><button class="btn btn-primary" type="submit">Apply</button></span>  

                            </div>
                        </div>
                    </div>
                    <div id="success"></div>
                </form><hr/><br/>
                <?php } ?>
                <?php if($int_giftcard_amount > 0) { ?>
                <h4 class="nopadding"><b>Apply Gift Card</b>&nbsp;<small><span class="text-primary ">(<i class="fa fa-gift"></i>&nbsp;<?php print "Available Balance: <b>".$str_currency_symbol."&nbsp;".number_format($int_giftcard_amount/$int_conversionrate, 2)."</b>"; ?>)</span></small></h4><hr/>
                <form id="frm_giftcard" name="frm_giftcard" novalidate>
                    <div class="control-group nopadding">
                        <div class="controls ">
                            <div class="form-group input-group">
                                <?php //print $int_final_subtotal; ?>
                                <input type="text" id="txt_giftcard" name="txt_giftcard" class="form-control text-left" placeholder="Enter Amount Here" required data-validation-required-message="Please Enter Amount Here." /> 
                                <input type="hidden" id="hdn_userpkid" name="hdn_userpkid" value="<?php print $int_userpkid; ?>" />
                                <input type="hidden" id="hdn_extprice" name="hdn_extprice" value="<?php print $int_subtotal; ?>" />
                                <span class="input-group-btn"><button class="btn btn-primary" type="submit">Apply</button></span>  
                            </div>
                        </div>
                    </div>
                    <div id="successg"></div>
                </form><br/>
                <?php } ?>
            </div> 
            <div class="col-md-4">
                <?php
                $str_query_select = "";
                if($str_cod_available == "YES") {
                    $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_PAYMENT_OPTION." WHERE visible='YES' ORDER BY displayorder DESC";
                } 
                else if($str_cod_available == "NO")
                {
                    $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_PAYMENT_OPTION." WHERE visible='YES' ORDER BY displayorder DESC";
                }
                $rs_list_paymentoption = GetRecordSet($str_query_select);
                if($rs_list_paymentoption->Count() > 0) { ?>
                <h4 class="nopadding"><b>Select Payment Option</b></h4><hr/>
                <?php $str_cod_style ="";
                $int_cnt = 1;
                while(!$rs_list_paymentoption->EOF()) { ?>    
                    <div class="radio">
                        <label>
                            <input type="radio" name="rdo_payment_option" id="rdo_payment_option" value="<?php print $rs_list_paymentoption->Fields("pkid"); ?>" <?php if($int_cnt == 1) { print "checked"; } ?>>
                                <?php if($rs_list_paymentoption->Fields("imagefilename") != "") { ?>&nbsp;<img src="<?php print $UPLOAD_IMG_PATH_PAYMENT_OPTIONS.$rs_list_paymentoption->Fields("imagefilename"); ?>" width="100"/><?php } else { print "<h4 class='nopadding text-primary'>".$rs_list_paymentoption->Fields("title")."</h4>"; } ?>
                            <?php if($rs_list_paymentoption->Fields("description") != "") { ?>
                                <p><?php print $rs_list_paymentoption->Fields("description"); ?></p>
                            <?php } ?>
                        </label>
                    </div><hr/>
                <?php $int_cnt++;
                $rs_list_paymentoption->MoveNext();
                } ?>
                <br/>
                <?php } ?>
            </div>
            <div class="col-md-4">
                <?php
                
                if($rs_list->Count() > 0) { ?>
                <h4 class="nopadding"><b>Order Review</b> <a href="./product_cart.php" class="pull-right">Edit</a></h4><hr/>
                
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="12%">Product</th>
                                <th>Details</th>
                                <th width="1%">Qty</th>
                                <th width="20%">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            //$int_subtotal = 0.00;
                            $str_payu_product_info = "";
                            $str_payu_name = "";
                            $str_payu_email = "";
                            $str_payu_phone = "";
                            
                            $str_paytm_email = "";
                            $str_paytm_phone = "";
                            while(!$rs_list->EOF()) { ?>
                            <tr>
                                <td>
                                    <?php
                                    $str_query_select = "";
                                    $str_query_select = "SELECT a.thumbphotofilename, a.imageurl, b.listprice FROM ".$STR_DB_TABLE_NAME_PHOTO." a LEFT JOIN ".$STR_DB_TABLE_NAME." b ON a.masterpkid=b.pkid AND b.approved='YES' AND b.visible='YES' WHERE a.masterpkid=".$rs_list->Fields("productpkid")." AND a.visible='YES' AND a.setasfront='YES'";
                                    $rs_list_image = GetRecordSet($str_query_select); ?>
                                    <?php if($rs_list_image->Fields("thumbphotofilename") != "") { ?>
                                        <img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("productpkid")."/".$rs_list_image->Fields("thumbphotofilename"); ?>" class="img-responsive" alt="<?php print $rs_list->Fields("producttitle"); ?>" title="<?php print $rs_list->Fields("producttitle"); ?>" width="100%"/>
                                    <?php } if($rs_list_image->Fields("imageurl") != "") {?>
                                        <img src="<?php print $rs_list_image->Fields("imageurl"); ?>" class="img-responsive" alt="<?php print $rs_list->Fields("producttitle"); ?>" title="<?php print $rs_list->Fields("producttitle"); ?>" width="100%"/>
                                    <?php } ?>
                                </td>
                                <td>
                                    <p><?php print MakeStringShort($rs_list->Fields("producttitle"), 30); ?></p><hr/>
                                    
                                    <?php if($rs_list->Fields("color") != "") { ?>
                                        <p><?php print $rs_list->Fields("color"); ?></p>
                                    <?php } ?>
                                    <?php if($rs_list->Fields("size") != "") { ?>    
                                        <p><?php print $rs_list->Fields("size"); ?></p>
                                    <?php } ?>
                                    
                                </td>
                                <td class="text-center"><?php print $rs_list->Fields("quantity"); ?></td>
                                <td align="right">
                                    
                                    <?php 
                                    $int_extended_total2 = 0; 
                                    $int_tailoring_price = 0.00;
                                    if($rs_list->Fields("tailoringprice") > 0) { 
                                        $int_tailoring_price = ceil($rs_list->Fields("tailoringprice") / $int_conversionrate);
                                    } else { $int_tailoring_price = 0.00; }  
                                    
                                        //$int_extended_total2 = ($rs_list->Fields("price") + $int_tailoring_price) * $rs_list->Fields("quantity");   
                                        
                                        $int_price = 0;
                                        $int_price = ceil($rs_list->Fields("price") / $int_conversionrate);
                                        $int_extended_total2 = 0; 
                                        $int_extended_total2 = ($int_price + $int_tailoring_price) * $rs_list->Fields("quantity");    
                                    ?>
                                    <?php print $str_currency_symbol." ".number_format($int_extended_total2,2); ?>
                                </td>
                            </tr>
                            
                            <?php 
                            //$int_subtotal = $int_subtotal + $int_extended_total;
                            $str_payu_product_info .= $rs_list->Fields("producttitle");
                            $str_payu_name = $rs_list->Fields("username");
                            $str_payu_email = $rs_list->Fields("emailid");
                            $str_payu_phone = $rs_list->Fields("phone");
                            
                            $str_paytm_email = $rs_list->Fields("emailid");
                            $str_paytm_phone = $rs_list->Fields("phone");
                            
                            $rs_list->MoveNext();
                            } ?>
                            <tr>
                                <td colspan="3" align="right"><b>Sub Total</b></td>
                                <td align="right"><b><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_subtotal,2); ?></b></td>
                            </tr>
                            <?php if($int_total_shipping_value > 0) { ?>
                            <tr>
                                <td colspan="3" align="right"><b>+ Shipping Charges</b></td>
                                    <?php //$int_subtotal = $int_subtotal + $int_total_shipping_value; ?>
                                <td align="right"><b><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_total_shipping_value,2); ?></b></td>
                            </tr>
                            <?php } ?>
                            <?php
                            if($int_discount > 0) 
                            { 
                                //$int_discount = ($int_subtotal * $int_wholesaler_discount) / 100; ?>
                            <tr>
                                <td colspan="3" align="right"><b>- Wholesaler Discount</b></td>
                                <td align="right"><b><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_discount,2); ?></b></td>
                            </tr>
                            <?php } ?>
                            <?php if($int_giftcard_discount > 0) { ?>
                            <tr>
                                <td colspan="3" align="right">
                                    <span class='pull-left'>
                                    <form id='frm_giftcard_remove' name='frm_giftcard_remove' novalidate>
                                        <input type="hidden" id='txt_giftcard_remove_amt' name='txt_giftcard_remove_amt' value="<?php print $int_giftcard_discount; ?>" />
                                        <input type="hidden" id='hdn_userpkid' name='hdn_userpkid' value="<?php print $int_userpkid;?>" />
                                        <input type="hidden" id="hdn_sessionid" name="hdn_sessionid" value="<?php print $int_send_sessionid; ?>" />
                                        <input type="hidden" id="hdn_uniqueid" name="hdn_uniqueid" value="<?php print $int_send_uniqueid; ?>" />
                                        <button type='submit' class="btn btn-danger btn-xs"><i class='fa fa-times'></i>&nbsp;Remove</button>
                                    </form>
                                    </span>
                                    <b>- Gift Card Discount</b>
                                </td>
                                <td align="right"><b><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_giftcard_discount,2); ?></b></td>
                            </tr>
                            <?php } ?>
                            <?php if($int_promocode_discount_amount > 0) { ?>
                            <tr>
                                <td colspan="3" align="right"><b>- Promocode Discount</b></td>
                                <td align="right"><b><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_promocode_discount_amount,2); ?></b></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="3" align="right"><b class="text-success">= Order Total </b></td>
                                <?php $int_final_subtotal = $int_subtotal + $int_total_shipping_value - $int_giftcard_discount - $int_promocode_discount_amount - $int_discount;?>
                                <td align="right"><b class="text-success"><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_final_subtotal,2); ?></b></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div id='successgr'></div>
                <?php } ?>  
                
                <div id="option1" style="display:none;">
                    <form id="frm_cod" name="frm_cod" action="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_payment_success_cod_p.php" method="POST">
                        <input type="hidden" id="hdn_sessionid" name="hdn_sessionid" value="<?php print $int_send_sessionid; ?>" />
                        <input type="hidden" id="hdn_uniqueid" name="hdn_uniqueid" value="<?php print $int_send_uniqueid; ?>" />
                        <input type="hidden" id="hdn_paymentoptionpkid" name="hdn_paymentoptionpkid" value="1" />
                        <input type="hidden" id="hdn_int_order_total" name="hdn_int_order_total" value="<?php print $int_final_subtotal; ?>" />
                        
                        <button id='btn_option1' class="btn btn-primary btn-lg btn-block"><b>Place Order</b></button>
                    </form>
                </div>
                
                <div id="option2" style="display:none;">
               
			<!-- <script src="https://www.paypal.com/sdk/js?client-id=AeM1sjCwyXkuOGCtUKLv5H5XzJda-0Q6BObrEovetwHVTvF1-tBSW4SnfTheJfAmr-jrcqDRImk9CDOj"> // Required. Replace SB_CLIENT_ID with your sandbox client ID. -->
			<script src="https://www.paypal.com/sdk/js?client-id=<?php echo $PAYPAL_CLIENT_KEY;?>&currency=<?php echo $GLOBALS['currency'];?>"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.

  			</script>

  			<div id="paypal-button-container"></div>
            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
			<script>
			paypal.Buttons({
			    	createOrder: function(data, actions) {
			      		// This function sets up the details of the transaction, including the amount and line item details.
			      		return actions.order.create({
						purchase_units: [{
				  			amount: {
				    			value: "<?php echo $int_final_subtotal; ?>"
				  			}
						}]
			      		});
			    	},
                     // Finalize the transaction
                    onApprove: function(data, actions) {
                                return actions.order.capture().then(function(details) {
                                    // Show a success message to the buyer
                                    swal("Transaction completed by", details.payer.name.given_name, "success");
                                    var base_url = window.location.origin;
                                    window.location.href = base_url+'/user/product_payment_success_paypal_p.php?transId='+details.id;
                                });
                    }
			}).render('#paypal-button-container');
			</script>



  			<?php /* ?><script>
				paypal.Buttons().render('#paypal-button-container');
    				// This function displays Smart Payment Buttons on your web page.
  			</script><?php */ ?>


                    <form id="frm_paypal" name="frm_paypal" action="" method="POST">
                        <button id='btn_option2' class="btn btn-primary btn-lg btn-block"><b>Save & Continue With PayPal</b></button>
                    </form>
                </div>
                
                <div id="option3" style="display:none;">
                    <?php
                    $str_payu_txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
                    
                    // Hash Sequence
                    // In case you haven't posted any UDFs, the hash sequence should look like this - 
                    // hashSequence= key|txnid|amount|productinfo|firstname|email|||||||||||salt.
                    //$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
                    //$hashSequence = $STR_PAYU_MERCHANT_KEY."|".$str_payu_txnid."|".$int_final_subtotal."|".$str_payu_product_info."|".$str_payu_name."|".$str_payu_email."|||||||||||".$STR_PAYU_MERCHANT_SALT; 

                    $hashSequence = $STR_PAYU_MERCHANT_KEY."|".$str_payu_txnid."|".$int_final_subtotal."|".$str_payu_product_info."|".$str_payu_name."|".$str_payu_email."|".$_SESSION['sessionid']."|".$_SESSION['uniqueid']."|||||||||".$STR_PAYU_MERCHANT_SALT; 

                    //print "hashSequence : ".$hashSequence."<br/>";                    
                    $str_payu_form_action = $STR_PAYU_BASE_URL . '/_payment';

                    $str_payu_hash = strtolower(hash('sha512', $hashSequence));

// hashSequence : YYI9So8b|67ec1e460f48bf8fb5f9|570|Preety Blue Western Wear Skirt For Women|My name is HDK|katariahitesh2020@gmail.com|76856a3fed67d76ad9c128e59cb3ce51|20200830094110|||||||||l5d35I98ys
                    ?>
                    
                    <form id="payuForm" name="payuForm" action="<?php echo $str_payu_form_action; ?>" method="POST">
                        <input type="hidden" name="key" value="<?php echo $STR_PAYU_MERCHANT_KEY ?>" />
                        <input type="hidden" name="txnid" value="<?php echo $str_payu_txnid ?>" />
                        <input type="hidden" name="amount" value="<?php echo $int_final_subtotal ?>" />  
                        <input type="hidden" name="productinfo" value="<?php echo $str_payu_product_info ?>" />
                        <input type="hidden" name="firstname" value="<?php echo $str_payu_name ?>" />
                        <input type="hidden" name="email" value="<?php echo $str_payu_email ?>" />  
                        <input type="hidden" name="phone" value="<?php echo $str_payu_phone ?>" />
                        <input type="hidden" name="surl" value="<?php echo $STR_PAYU_URL_SUCCESS ?>" /> 
                        <input type="hidden" name="furl" value="<?php echo $STR_PAYU_URL_FAILURE ?>" /> 
                        <input type="hidden" name="hash" value="<?php echo $str_payu_hash ?>" />
                        <input type="hidden" name="udf1" value="<?php echo $_SESSION['sessionid'] ?>" />
                        <input type="hidden" name="udf2" value="<?php echo $_SESSION['uniqueid'] ?>" />
                        <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
                        <input type="submit" id='btn_option3' class="btn btn-primary btn-lg btn-block" value="Save & Continue With PayUMoney" /> 
                    </form>
                </div>
                
                <div id="option4" style="display:none;">
                    <?php 
                    require_once("../PaytmKit/lib/config_paytm.php");
                    require_once("../PaytmKit/lib/encdec_paytm.php");
                    
                    $checkSum = "";
                    //$int_paytm_order_id = rand(10000,99999999);
                    $int_paytm_order_id = date("YmdHis");

                    $paramList = array();

                    //$ORDER_ID = $_POS$STR_DB_TABLE_NAME_PAYMENT_OPTIONT["ORDER_ID"];
                    //$CUST_ID = $_POST["CUST_ID"];
                    //$INDUSTRY_TYPE_ID = $_POST["INDUSTRY_TYPE_ID"];
                    //$CHANNEL_ID = $_POST["CHANNEL_ID"];
                    //$TXN_AMOUNT = $_POST["TXN_AMOUNT"];

                    // Create an array having all required parameters for creating checksum.
                    $paramList["MID"] = PAYTM_MERCHANT_MID;
                    $paramList["ORDER_ID"] = $int_paytm_order_id;
                    $paramList["CUST_ID"] = $str_paytm_email;
                    $paramList["INDUSTRY_TYPE_ID"] = "Retailer"; //Retailer For Testing // Will be provided by paytm For Production
                    $paramList["CHANNEL_ID"] = "WEB";
                    $paramList["TXN_AMOUNT"] = $int_final_subtotal;
                    $paramList["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;
                    $paramList["EMAIL"] = $str_paytm_email;
                    $paramList["MOBILE_NO"] = $str_paytm_phone;
                    $paramList["CALLBACK_URL"] = "http://www.darpani.com/user/product_payment_success_paytm_p.php";

                    /*
                    $paramList["CALLBACK_URL"] = "http://localhost/PaytmKit/pgResponse.php";
                    $paramList["MSISDN"] = $MSISDN; //Mobile number of customer
                    $paramList["EMAIL"] = $EMAIL; //Email ID of customer
                    $paramList["VERIFIED_BY"] = "EMAIL"; //
                    $paramList["IS_USER_VERIFIED"] = "YES"; //
                    */
                    
                    //Here checksum string will return by getChecksumFromArray() function.
                    $checkSum = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);

                    ?>

                    <form method="post" action="<?php echo PAYTM_TXN_URL ?>" name="f1">
                        <?php
                        foreach($paramList as $name => $value) 
                        {
                            echo '<input type="hidden" name="' . $name .'" value="' . $value . '">';
                            //echo "" . $name ." - ". $value."<br/>";
                        }
                        ?>
                        <input type="hidden" name="CHECKSUMHASH" value="<?php echo $checkSum ?>">
                        
                        <?php /* ?><script type="text/javascript">
                                document.f1.submit();
                        </script><?php */ ?>
                        <button id='btn_option4' class="btn btn-primary btn-lg btn-block"><b>Save & Continue With Paytm</b></button>
                    </form>
                    
                    <?php /* ?><form id="frm_paytm" name="frm_paytm" action="" method="POST">
                    <button id='btn_option4' class="btn btn-primary btn-lg btn-block"><b>Save and Continue&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i><i class="fa fa-chevron-right" aria-hidden="true"></i></b></button>
                    </form><?php */ ?>
                </div>






                
                
            </div>
        </div><br/>
    </div>
    <?php 
    # This query is used for Paytm payment integration. this validation field will be checked on success page
    $str_query_update="UPDATE ".$STR_DB_TABLE_NAME_SESSION_CART." SET validation01='".$int_paytm_order_id."' WHERE sessionid='".$item_sessionid."' AND uniqueid='".$item_uniqueid."'" ;
    //print "Query : ".$str_query_update."<br/>";
    ExecuteQuery($str_query_update);
    ?>
    <div class="footer-bottom-margin">
        <?php include $STR_USER_FOOTER_PATH2; CloseConnection();?>
    </div>
    
    
    
    <?php ## FOR STICKY BUTTON
        # Have to manage button of forms of various payment methods on click of <a> as given below
    ?>
    <div class="container-fluid">
        <div class="row padding-10">
            <div class="price-and-checkout-btn-panel hidden-lg hidden-md">
                <div class="col-xs-12 text-center nopadding">
                    <div id="opt1" style="display:none;">
                        <a onclick="$('#btn_option1').click();">
                            <div class="buy-now-btn">COD
                             <?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_final_subtotal, 2)." ".$str_currency_shortform; ?> / PLACE ORDER
                            </div>
                        </a>
                    </div>
                    <div id="opt2" style="display:none;">
                        <a onclick="$('#btn_option2').click();">
                            <div class="buy-now-btn">PAYPAL
                             <?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_final_subtotal, 2)." ".$str_currency_shortform; ?> / PLACE ORDER
                            </div>
                        </a>
                    </div>
                    <div id="opt3" style="display:none;">
                        <a onclick="$('#btn_option3').click();">
                            <div class="buy-now-btn">PAYUMONEY
                             <?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_final_subtotal, 2)." ".$str_currency_shortform; ?> / PLACE ORDER
                            </div>
                        </a>
                    </div>
                    <div id="opt4" style="display:none;">
                        <a onclick="$('#btn_option4').click();">
                            <div class="buy-now-btn">PAYTM
                             <?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_final_subtotal, 2)." ".$str_currency_shortform; ?> / PLACE ORDER
                            </div>
                        </a>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_promocode.js"></script>    
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_giftcard.js"></script>    
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_giftcard_remove.js"></script>    
    <script>
        $(document).ready(function(){
            var defaultoption = $('input[name="rdo_payment_option"]:checked').val()
            //alert(defaultoption);
            if(defaultoption == 1)
            {
                $("#option1").show();
                $("#option2").hide();
                $("#option3").hide();
                $("#option4").hide();
             
             
                $("#opt1").show();
                $("#opt2").hide();
                $("#opt3").hide();
                $("#opt4").hide();
                
            }
            else if(defaultoption == 2)
            {
                $("#option1").hide();
                $("#option2").show();
                $("#option3").hide();
                $("#option4").hide();
                
                $("#opt1").hide();
                $("#opt2").show();
                $("#opt3").hide();
                $("#opt4").hide();
            }
            else if(defaultoption == 3)
            {
                $("#option1").hide();
                $("#option2").hide();
                $("#option3").show();
                $("#option4").hide();
                
                $("#opt1").hide();
                $("#opt2").hide();
                $("#opt3").show();
                $("#opt4").hide();
            }
            else if(defaultoption == 4)
            {
                $("#option1").hide();
                $("#option2").hide();
                $("#option3").hide();
                $("#option4").show();
                
                $("#opt1").hide();
                $("#opt2").hide();
                $("#opt3").hide();
                $("#opt4").show();
            }
        });
        
        
        
        
        $('input[type="radio"]').click(function()
        {
            if($(this).attr("name")=="rdo_payment_option")
            {
                var val= $(this).val();
                //alert(val);
                //var quantity = $('#txt_quantity').val();
                  //alert(quantity);

                //var rd_optionTWO = document.getElementById("rd_option2");
                if(val == 1)
                {
                    $("#option1").show();
                    $("#option2").hide();
                    $("#option3").hide();
                    $("#option4").hide();
                    
                    
                    $("#opt1").show();
                    $("#opt2").hide();
                    $("#opt3").hide();
                    $("#opt4").hide();

                }
                else if(val == 2)
                {
                    $("#option1").hide();
                    $("#option2").show();
                    $("#option3").hide();
                    $("#option4").hide();
                    
                    $("#opt1").hide();
                    $("#opt2").show();
                    $("#opt3").hide();
                    $("#opt4").hide();
                }
                else if(val == 3)
                {
                    $("#option1").hide();
                    $("#option2").hide();
                    $("#option3").show();
                    $("#option4").hide();
                    
                    $("#opt1").hide();
                    $("#opt2").hide();
                    $("#opt3").show();
                    $("#opt4").hide();
                }
                else if(val == 4)
                {
                    $("#option1").hide();
                    $("#option2").hide();
                    $("#option3").hide();
                    $("#option4").show();
                    
                    $("#opt1").hide();
                    $("#opt2").hide();
                    $("#opt3").hide();
                    $("#opt4").show();
                }
                //document.getElementById("output").innerHTML = val;
                //document.getElementById("output1").innerHTML = val;

                //document.getElementById("rawclr").addClass("alert alert-warning");
                //temp=document.getElementById("temp");
                //temp.className="alert alert-warning";
                /*$('input[type="radio"]').click(function() {
                //alert("hi");
                        var selected = $(this).hasClass("alert alert-warning");
                        $("#data tr").removeClass("alert alert-warning");
                        if(!selected)
                                        $("#data tr").addClass("alert alert-warning");
                });*/
            }
        });
    </script>
</body>
</html>
