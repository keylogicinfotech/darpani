<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";

//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_MT_VIDEO";
$str_img_path = "./mdm/video/";
$str_clip_path = "../../mdm/ftpupload/video/";
$str_db_table_name = "t_page_metatag";
$str_db_table_name_video = "t_video";
$str_db_table_name_video_cat = "t_video_cat";
$str_xml_file_name = "video.xml";
$str_xml_file_name_cat = "video_cat.xml";
$str_xml_file_name_cms = "video_cms.xml";
$int_records_per_page = 10;
//print_r($_GET);exit;
#----------------------------------------------------------------------
#read main xml file
$str_xml_list_cat = "";
$str_xml_list_cat = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name_cat,"ROOT_ITEM_CAT");

$str_xml_list = "";
$str_xml_list = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");

$int_total_records = 0;
$int_total_records = (count(array_keys($str_xml_list)));	
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_cat_pkid = 0;
if(isset($_GET["cid"]) && trim($_GET["cid"]) != "" )
{   
    $int_cat_pkid = trim(GetDecryptId($_GET["cid"]));    
}

$int_pkid = 0;
if(isset($_GET["pid"]) && trim($_GET["pid"]) != "" )
{   
    $int_pkid = trim(GetDecryptId($_GET["pid"]));
}


$str_query_select = "";
$str_query_select = "SELECT title, previewtoall, accesstoall, description FROM ".$str_db_table_name_video." WHERE pkid = ".$int_pkid;
$rs_list_video = GetRecordSet($str_query_select);

$str_title = "";
$str_title = $rs_list_video->Fields("title");

$str_desc = "";
$str_desc = $rs_list_video->Fields("description");

$str_query_select = "";
$str_query_select = "SELECT title FROM ".$str_db_table_name_video_cat." WHERE catpkid = ".$int_cat_pkid;
$rs_list_video_cat = GetRecordSet($str_query_select);

$str_cat_title = "";
$str_cat_title = $rs_list_video_cat->Fields("title");
//print $str_title;exit;

/*$str_name_key = "";
if(isset($_POST["key"]) && trim($_POST["key"]) != "" )
{ $str_name_key = trim($_POST["key"]); }

# get data for paging
$int_page = "";
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"]) != "" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"]) > 0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page = 1; }
//print_r($_POST);
$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";
*/



/*$int_page = 1;
if($int_total_records>0)
{
    $int_total_pages = ceil($int_total_records / $int_records_per_page); 
    //print $int_total_pages;
    if ($int_page > $int_total_pages)
    {
        $int_page = $int_total_pages;
    }
    if($int_page < 1)
    {
        $int_page=1;
    }
}*/

$str_member_flag = false;
if((isset($_SESSION['vipmemberid']) && $_SESSION['vipmemberid'] != "") || $rs_list_video->Fields("accesstoall") != "NO")
{
    $str_member_flag = true;
}
else
{
    CloseConnection();
    Redirect("./../vipmember-joinnow#ptop"); 
    exit();
}

//print $int_total_records; exit;
#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table

$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?><?php if($str_title != "") { print " [".$str_title."]"; }?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet">    
</head>
<body>
<?php include("../includes/header.php"); ?>
    <div class="container center-bg">
        <div class="row padding-10">
           <?php /* <div class="col-lg-4 col-md-4">
                <h4 class="">CAT 001 / <a href="" title="">Product Name</a></h4>
                <?php print $str_title;?>
            </div>*/?>
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
                <ol class="breadcrumb thumbnail-margin">
                    <?php if($str_cat_title != ""){ ?>
                        <li class="active"><?php print $str_cat_title; ?></li>
                    <?php } ?>
                    <?php if($str_title != ""){ ?>    
                        <li><a href="<?php print $_SERVER['HTTP_REFERER']; ?>"><?php print $str_title; ?></a></li>
                    <?php } ?>
                </ol>
            </div>
        </div>
        <?php if($str_desc != "" && $str_desc != "<br>") { ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <p align="justify"><?php print($str_desc);?></p>
            </div>
        </div>
        <?php } ?>
        <?php 
        $arr_test = array_keys($str_xml_list);
	$int_cnt=0;
        if(trim($arr_test[0])!="ROOT_ITEM")
        { 
            /*if($int_page != 1)
            {
                for($int_cnt;$int_cnt<($int_page-1)*($int_records_per_page);$int_cnt++)
		{
                    array_shift($str_xml_list);
		}	
            } */ ?>
            <div class="row padding-10">
                <?php
                $int_arr_lim=0;
                foreach($str_xml_list as $key => $val)         
                {
                    if($str_xml_list[$key]["CATPKID"] == $int_cat_pkid && $str_xml_list[$key]["PKID"] == $int_pkid)
                    {
                        //if($int_arr_lim < $int_records_per_page)
                        //{
                            $int_arr_lim++;
                            if(is_array($val))
                            {
                                $str_video_access_flag=false;
                                $str_preview_flag=false;
                                if($str_member_flag)
                                {
                                    $str_video_access_flag=true;
                                    $str_preview_flag=true;
                                }
                                else
                                {
                                    if($str_xml_list[$key]["ACCESSTOALL"] == "YES")
                                    {
                                        if($str_xml_list[$key]["PREVIEWTOALL"] == "YES")
                                        {
                                                $str_video_access_flag = true;
                                                $str_preview_flag = true;
                                        }										
                                    }
                                    else
                                    {
                                        if($str_xml_list[$key]["PREVIEWTOALL"] == "YES")
                                        {
                                            $str_preview_flag = true;
                                        }
                                    }									
                                } ?>
                                <?php 
                                $arr_main = array();
                                if(trim($str_xml_list[$key]["CLIP"])!="")
                                {
                                    $arr_main = explode("||;||",$str_xml_list[$key]["CLIP"]);
                                    for($i=0; $i < count($arr_main)-1; $i++)
                                    {
                                        $arr_sub = explode("|;|",$arr_main[$i]);
                                        $str_clip_pkid1 = GetEncryptId($arr_sub[0]);
                                        $str_clip_pkid2 = GetEncryptId($arr_sub[0]);
                                        $str_clip_title = $arr_sub[1];
                                        $str_clip_name = $str_clip_path.$arr_sub[2];
                                        $str_clip_name_for_extension = $arr_sub[2];
                                        $str_clip_extension = GetExtension($str_clip_name_for_extension);
                                        $str_clip_download = $arr_sub[3];
                                        //print $str_clip_extension;
                                        //print $str_clip_name."<br/>";
                                        if(file_exists("../mdm/ftpupload/video/".$arr_sub[2]))
                                        {  ?>
                                        <div class="row padding-10">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="thumbnail" >
                                                    <p class="">
                                                    <?php if(strtoupper($str_clip_extension) == "MP4" || strtoupper($str_clip_extension) == "WEBM" || strtoupper($str_clip_extension) == "OGV" || strtoupper($str_clip_extension) == "OGG") { ?>
                                                    <video class="" controls="" width="100%">
                                                        <source src="<?php print $str_clip_name; ?>" type="video/mp4">
                                                        <source src="<?php print $str_clip_name; ?>" type="video/ogg">
                                                        <source src="<?php print $str_clip_name; ?>" type="video/webm">
                                                        Your browser does not support HTML5 video.
                                                    </video>
                                                    <?php } else if(strtoupper($str_clip_extension)=="RAM" || strtoupper($str_clip_extension)=="RM" || strtoupper($str_clip_extension)=="RMVB" || strtoupper($str_clip_extension)=="RPM") { ?>
                                                            <img src="./../images/ico_rm.png" class="img-responsive" />
                                                        <?php
                                                        } elseif (strtoupper($str_clip_extension)=="3GP") { ?>
                                                            <img src="./../images/ico_3gp.png" class="img-responsive" />
                                                        <?php } elseif (strtoupper($str_clip_extension)=="FLV") { ?>
                                                            <img src="./../images/ico_flv.png" class="img-responsive" />
                                                        <?php } elseif (strtoupper($str_clip_extension)=="MKV") { ?>
                                                            <img src="./../images/ico_mkv.png" class="img-responsive" />
                                                        <?php } elseif (strtoupper($str_clip_extension)=="AVI") { ?>
                                                            <img src="./../images/ico_avi.png" class="img-responsive" />
                                                        <?php } elseif (strtoupper($str_clip_extension)=="MPEG") { ?>
                                                            <img src="./../images/ico_mpeg.png" class="img-responsive" />
                                                        <?php } elseif (strtoupper($str_clip_extension)=="MPG") { ?>
                                                            <img src="./../images/ico_mpg.png" class="img-responsive" />
                                                        <?php } elseif (strtoupper($str_clip_extension)=="avi") { ?>
                                                            <img src="./../images/ico_avi.png" class="img-responsive" />
                                                        <?php } elseif (strtoupper($str_clip_extension)=="WMV") { ?>
                                                            <img src="./../images/ico_wmv.png" class="img-responsive" />
                                                        <?php } ?>
                                                    </p>
                                                    <h3 class="nopadding"><a href="" title=""><?php print $str_clip_title; ?></a></h3>
                                                    <?php   
                                                    ///////////////////////////////////////////////////////////////////
                                                    /// getID3() by James Heinrich <info@getid3.org>               //
                                                    //  available at http://getid3.sourceforge.net                 //
                                                    //            or http://www.getid3.org                         //
                                                    /////////////////////////////////////////////////////////////////
                                                    //                                                             //
                                                    // /demo/demo.basic.php - part of getID3()                     //
                                                    // Sample script showing most basic use of getID3()            //
                                                    // See readme.txt for more details                             //
                                                    //                                                            ///
                                                    /////////////////////////////////////////////////////////////////

                                                    //die('Due to a security issue, this demo has been disabled. It can be enabled by removing line '.__LINE__.' in '.$_SERVER['PHP_SELF']);

                                                    // include getID3() library (can be in a different directory if full path is specified)
                                                    require_once('./getID3_1912/getid3/getid3.php');

                                                    // Initialize getID3 engine
                                                    $getID3 = new getID3;

                                                    // Analyze file and store returned data in $ThisFileInfo
                                                    $ThisFileInfo = $getID3->analyze("../mdm/ftpupload/video/".$arr_sub[2]);

                                                    /*
                                                     Optional: copies data from all subarrays of [tags] into [comments] so
                                                     metadata is all available in one location for all tag formats
                                                     metainformation is always available under [tags] even if this is not called
                                                    */
                                                    getid3_lib::CopyTagsToComments($ThisFileInfo);
                                                    //echo 'Video Resolution = '.$ThisFileInfo['video']['resolution_x'].' X '.$ThisFileInfo['video']['resolution_y'];

                                                    /*
                                                     Output desired information in whatever format you want
                                                     Note: all entries in [comments] or [tags] are arrays of strings
                                                     See structure.txt for information on what information is available where
                                                     or check out the output of /demos/demo.browse.php for a particular file
                                                     to see the full detail of what information is returned where in the array
                                                     Note: all array keys may not always exist, you may want to check with isset()
                                                     or empty() before deciding what to output
                                                    */
                                                    //echo $ThisFileInfo['comments_html']['artist'][0]; // artist from any/all available tag formats
                                                    //echo $ThisFileInfo['tags']['id3v2']['title'][0];  // title from ID3v2
                                                    //echo $ThisFileInfo['audio']['bitrate'];           // audio bitrate
                                                    //echo $ThisFileInfo['playtime_string'];            // playtime in minutes:seconds, formatted string

                                                    /*
                                                     if you want to see ALL the output, uncomment this line:
                                                    */
                                                    //echo '<pre>'.htmlentities(print_r($ThisFileInfo, true)).'</pre>';
                                                    ?>	
                                                    <p class=""><i class="fa fa-video-camera"></i>&nbsp;<?php print sizeoffile(filesize("../mdm/ftpupload/video/".$arr_sub[2])); ?> | <i class="fa fa-clock-o"></i>&nbsp;<?php print($ThisFileInfo['playtime_string']); ?> min:sec | <i class="fa fa-file"></i>&nbsp;<?php print($ThisFileInfo['video']['resolution_x'].'x'.$ThisFileInfo['video']['resolution_y'].' px');?> | <a href="../../user/video_details_download_p.php?pkid=<?php print $str_clip_pkid1; ?>" title="Click to Download" class="btn btn-success btn-xs"><i class="fa fa-download"></i></a></p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                        }
                                    }
                                } ?>
                            <?php
                            } 
                        //}
                    } 
                }?>
            </div>
        <?php
        }?>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include("../includes/footer.php");CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
</body>
</html>
