<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/lib_xml.php";
include "./../includes/http_to_https.php";	
include "./../includes/unique_site_view_count.php";
include "./webmaster_config.php";
#----------------------------------------------------------------------
    $int_promopkid ="";	
    if(isset($_GET["pkid"]))
    {
        $enc_id = trim($_GET["pkid"]);
        $int_promopkid = GetDecryptId($enc_id);
    }	

    if($int_promopkid==trim("") || !is_numeric($int_promopkid) || $int_promopkid<0)
    {	
        CloseConnection();
        exit();
    }		
    $str_query_select = "SELECT title,zipfilename,createdate FROM " .$STR_DB_TABLE_NAME_GALLERY. " WHERE pkid =" .$int_promopkid;		
    $rs_list = GetRecordSet($str_query_select);

    if($rs_list->EOF()==true)
    {
        CloseConnection();
        exit();
    }
    $str_file=$rs_list->fields("zipfilename");
    $str_title=$rs_list->fields("title");
    $str_title=str_replace(" ","_",$str_title);
    $str_filename = $str_title."_".DDMMMYYYYFormat($rs_list->fields("createdate")).".".GetExtension($str_file);

    // Set name of file with header
    ob_end_clean();
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename=" . $str_filename);

    $fname = $UPLOAD_IMG_PATH .$int_promopkid."/". $str_file;	
    $fp = fopen($fname, 'rb');
    header("Content-Length: " . filesize($fname));

    //If an error occurs, fpassthru() returns FALSE. 
    //Otherwise, fpassthru() returns the number of characters read from handle and passed through to the output. 

    fpassthru($fp);	
    CloseConnection();	
    exit();
?>
