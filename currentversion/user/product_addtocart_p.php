<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();
include "./../user/product_validatesession.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
//include "./../includes/http_to_https.php";	
include "./../includes/lib_xml.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/lib_datetimeyear.php";
include "./../user/product_config.php";
//print_r($_POST); 
//exit;
#----------------------------------------------------------------------.

if(isset($_SESSION["usr_cntry"])) 
    {
        $sel_qry_price = "";
        $sel_qry_price = "SELECT conversionrate, currency_title, currency_symbol, currency_shortform, courierpriceperkg FROM t_site_country WHERE pkid = ".$_SESSION["usr_cntry"];
        $rs_list_currency = GetRecordSet($sel_qry_price);
        if($rs_list_currency->Count() >0)
        {
            $str_currency_symbol = $rs_list_currency->Fields("currency_title");            
	    $str_currency_symbol = $rs_list_currency->Fields("currency_symbol");
            $str_currency_shortform = $rs_list_currency->Fields("currency_shortform");
            $int_conversionrate = $rs_list_currency->Fields("conversionrate");
	    $int_courier_price_per_kg = $rs_list_currency->fields("courierpriceperkg");

        }
    }

#----------------------------------------------------------------------
# posted variables...

$int_pkid = 0;
if(isset($_POST["hdn_prodpkid"]) && $_POST["hdn_prodpkid"] != "")
{
    $int_pkid = $_POST["hdn_prodpkid"];
}

$str_title = "";
if(isset($_POST["hdn_prodtitle"]) && $_POST["hdn_prodtitle"] != "")
{
    $str_title = str_replace("'","\'",trim($_POST["hdn_prodtitle"]));
}

$str_itemcode = "";
if(isset($_POST["hdn_itemcode"]) && $_POST["hdn_itemcode"] != "")
{
    $str_itemcode = trim($_POST["hdn_itemcode"]);
}

$int_catpkid = 0;
if(isset($_POST["hdn_catpkid"]) && $_POST["hdn_catpkid"] != "")
{
    $int_catpkid = $_POST["hdn_catpkid"];
}

/*$int_memberpkid = 0;
if(isset($_POST["hdn_memberpkid"]) && $_POST["hdn_memberpkid"] != "")
{
    $int_memberpkid = $_POST["hdn_memberpkid"];
}
//print_r($_POST);exit;

$str_memberid = "";
if(isset($_POST["hdn_memberid"]) && $_POST["hdn_memberid"] != "")
{
    $str_memberid = $_POST["hdn_memberid"];
}*/
$int_userpkid = 0;
if(isset($_POST["hdn_userpkid"]) && $_POST["hdn_userpkid"] != "")
{
    $int_userpkid = $_POST["hdn_userpkid"];
}
//print_r($_POST);exit;

$str_username = "";
if(isset($_POST["hdn_username"]) && $_POST["hdn_username"] != "")
{
    $str_username = $_POST["hdn_username"];
}


$int_referredbypkid = 0;
if(isset($_POST["hdn_referredbypkid"]) && $_POST["hdn_referredbypkid"] != "")
{
    $int_referredbypkid = $_POST["hdn_referredbypkid"];
}

$int_commission = 0;
if(isset($_POST["hdn_commission"]) && $_POST["hdn_commission"] != "")
{
    $int_commission = $_POST["hdn_commission"];
}

$int_discount = 0;
if(isset($_POST["hdn_discount"]) && $_POST["hdn_discount"] != "")
{
    $int_discount = $_POST["hdn_discount"];
}


$str_prodcattitle = "";
if($int_catpkid > 0)
{
    $str_sel_cat = "";
    $str_sel_cat = "SELECT title FROM ".$STR_DB_TABLE_NAME_CAT." WHERE catpkid = ".$int_catpkid;
    $rs_cat_title = GetRecordSet($str_sel_cat);
    if($rs_cat_title->Count() > 0)
    {
        $str_prodcattitle = $rs_cat_title->Fields("title");
    }
}

$int_subcatpkid = 0;
if(isset($_POST["hdn_subcatpkid"]) && $_POST["hdn_subcatpkid"] != "")
{
    $int_subcatpkid = $_POST["hdn_subcatpkid"];
}
$str_prodsubcattitle = "";
if($int_subcatpkid > 0)
{
    $str_sel_subcat = "";
    $str_sel_subcat = "SELECT subcattitle FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid = ".$int_subcatpkid;
    $rs_subcat_title = GetRecordSet($str_sel_subcat);
    if($rs_subcat_title->Count() > 0)
    {
        $str_prodsubcattitle = $rs_subcat_title->Fields("subcattitle");
    }
    
}



//print $str_prodsubcattitle; exit;
//print $str_prodsubcattitle; exit;
/*$int_modelpkid = 0;
if(isset($_POST["hdn_mdlpkid"]) && $_POST["hdn_mdlpkid"] != "")
{
    $int_modelpkid = $_POST["hdn_mdlpkid"];
}

$str_modelname = "";
if(isset($_POST["hdn_modelname"]) && $_POST["hdn_modelname"] != "")
{
    $str_modelname = $_POST["hdn_modelname"];
}*/

$int_quantity = 0;
if(isset($_POST["txt_quantity"]) && $_POST["txt_quantity"] != "")
{
    $int_quantity = trim($_POST["txt_quantity"]);
}

$int_weight = 0;
if(isset($_POST["hdn_weight"]) && $_POST["hdn_weight"] != "")
{
    $int_weight = trim($_POST["hdn_weight"]);
}
//print $int_weight; exit;

//$int_colorpkid = 0;
$str_color = "";

$int_colorpkid = 0;
if(isset($_POST["rdo_color"]) && $_POST["rdo_color"] != "")
{
    $int_colorpkid  = $_POST["rdo_color"];
}

if($int_colorpkid > 0 ) // This will be checked when item added from product details page
{
    $str_query_select = "";
    $str_query_select = "SELECT title FROM ".$STR_DB_TABLE_NAME_COLOR." WHERE pkid = ".$int_colorpkid;
    $rs_color = GetRecordSet($str_query_select);
    if($rs_color->Count() > 0)
    {
        $str_color = $rs_color->Fields("title");
    }
}
else if($int_colorpkid != "" && is_string($int_colorpkid) ) // This will be checked when item added from products listing page
{
	$str_color = $_POST["rdo_color"];
}
/*else
{
    $response['status']='SUC';
    $response['message']="Please Select Color.";
    echo json_encode($response);
}

*/
//print $_POST["rdo_color"]." - ".$str_color."<br/>";

$str_size = "";
$int_sizepkid = 0;
if(isset($_POST["rdo_size"]) && $_POST["rdo_size"] != "")
{
    $int_sizepkid = $_POST["rdo_size"];
}
if($int_sizepkid > 0)
{
    $str_query_select = "";
    $str_query_select = "SELECT title FROM ".$STR_DB_TABLE_NAME_SIZE." WHERE pkid = ".$int_sizepkid;
    $rs_size = GetRecordSet($str_query_select);
    if($rs_size->Count() > 0)
    {
        $str_size = $rs_size->Fields("title");
    }
}
else if($int_sizepkid != "" && is_string($int_sizepkid) ) // This will be checked when item added from products listing page
{
	$str_size = $_POST["rdo_size"];
}
//print $_POST["rdo_size"]." - ".$str_size."<br/>";

$str_type = "";
if(isset($_POST["cbo_type"]) && $_POST["cbo_type"] != "")
{
    $str_type = $_POST["cbo_type"];
}

$int_tailoring_option = 0;
$int_tailoring_price = 0.00;
$str_tailoring_option = "";
$str_measurement_details = "";
if(isset($_POST["rdo_tailoringoption"]) && $_POST["rdo_tailoringoption"] != "")
{
    $int_tailoring_option = $_POST["rdo_tailoringoption"];
}

//print $int_tailoring_option; exit;
if($int_tailoring_option > 0)
{
    $str_query_select = "";
    $str_query_select = "SELECT a.price, a.masterpkid, b.title FROM ".$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE." a LEFT JOIN ".$STR_DB_TABLE_NAME_TAILORING_SERVICE." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE masterpkid=".$int_tailoring_option." AND catpkid=".$int_catpkid;
    $rs_list_tailoring_option = GetRecordSet($str_query_select);
    if($rs_list_tailoring_option->Count() > 0)
    {
        $int_tailoring_price = $rs_list_tailoring_option->Fields("price");
        $str_tailoring_option = $rs_list_tailoring_option->Fields("title");
    }
    
    if($int_tailoring_option == 3)
    {
        ## Blouse
        $int_blouse_length = 0;
        if(isset($_POST["txt_blouse_length"]) && $_POST["txt_blouse_length"] != "")
        {
            $int_blouse_length = $_POST["txt_blouse_length"];
        }
        if($int_blouse_length != "")
        {
            $str_measurement_details .= "Blouse Length:".$int_blouse_length."|~|";
        }
        
        
        $int_blouse_sleeve_length = 0;
        if(isset($_POST["txt_blouse_sleeve_length"]) && $_POST["txt_blouse_sleeve_length"] != "")
        {
            $int_blouse_sleeve_length = $_POST["txt_blouse_sleeve_length"];
        }
        if($int_blouse_sleeve_length != "")
        {
            $str_measurement_details .= "Blouse Sleeve Length:".$int_blouse_sleeve_length."|~|";
        }
        
        $int_blouse_around_bust = 0;
        if(isset($_POST["cbo_blouse_aroundbust"]) && $_POST["cbo_blouse_aroundbust"] != "")
        {
            $int_blouse_around_bust = $_POST["cbo_blouse_aroundbust"];
        }
        if($int_blouse_around_bust != "")
        {
            $str_measurement_details .= "Blouse Around Bust:".$int_blouse_around_bust."|~|";
        }
        
        $int_blouse_around_above_waist = 0;
        if(isset($_POST["txt_blouse_abovearoundwaist"]) && $_POST["txt_blouse_abovearoundwaist"] != "")
        {
            $int_blouse_around_above_waist = $_POST["txt_blouse_abovearoundwaist"];
        }
        if($int_blouse_around_above_waist != "")
        {
            $str_measurement_details .= "Blouse Above Around Bust:".$int_blouse_around_above_waist."|~|";
        }
        
        $int_blouse_front_neck_depth = 0;
        if(isset($_POST["txt_blouse_frontneckdepth"]) && $_POST["txt_blouse_frontneckdepth"] != "")
        {
            $int_blouse_front_neck_depth = $_POST["txt_blouse_frontneckdepth"];
        }
        if($int_blouse_front_neck_depth != "")
        {
            $str_measurement_details .= "Blouse Front Neck Depth:".$int_blouse_front_neck_depth."|~|";
        }
        
        $int_blouse_back_neck_depth = 0;
        if(isset($_POST["txt_blouse_backneckdepth"]) && $_POST["txt_blouse_backneckdepth"] != "")
        {
            $int_blouse_back_neck_depth = $_POST["txt_blouse_backneckdepth"];
        }
        if($int_blouse_back_neck_depth != "")
        {
            $str_measurement_details .= "Blouse Back Neck Depth:".$int_blouse_back_neck_depth."|~|";
        }
        
        /*if($str_measurement_details != "") 
        {
            $str_measurement_details.="||~~||";
        }*/
        
        ## Blouse Pad
	$str_blouse_pad = "";
        if(isset($_POST["cbo_blouse_pad"]) && $_POST["cbo_blouse_pad"] != "")
        {
            $str_blouse_pad = $_POST["cbo_blouse_pad"];
        }
        if($str_blouse_pad != "")
        {
            $str_measurement_details .= "Want Blouse Pad:".$str_blouse_pad."|~|";
        }

        ## Lehenga
        $int_lehenga_around_waist = 0;
        if(isset($_POST["txt_lehenga_aroundwaist"]) && $_POST["txt_lehenga_aroundwaist"] != "")
        {
            $int_lehenga_around_waist = $_POST["txt_lehenga_aroundwaist"];
        }
        if($int_lehenga_around_waist != "")
        {
            $str_measurement_details .= "Lehenga Around Waist:".$int_lehenga_around_waist."|~|";
        }
        
        $int_lehenga_around_hips = 0;
        if(isset($_POST["txt_lehenga_aroundhips"]) && $_POST["txt_lehenga_aroundhips"] != "")
        {
            $int_lehenga_around_hips = $_POST["txt_lehenga_aroundhips"];
        }
        if($int_lehenga_around_hips != "")
        {
            $str_measurement_details .= "Lehenga Around Hips:".$int_lehenga_around_hips."|~|";
        }
        
        $int_lehenga_length = 0;
        if(isset($_POST["txt_lehenga_length"]) && $_POST["txt_lehenga_length"] != "")
        {
            $int_lehenga_length = $_POST["txt_lehenga_length"];
        }
        if($int_lehenga_length != "")
        {
            $str_measurement_details .= "Lehenga Length:".$int_lehenga_length."|~|";
        }
        
        ## Salwar
        $int_salwar_yourheight = 0;
        if(isset($_POST["txt_salwar_yourheight"]) && $_POST["txt_salwar_yourheight"] != "")
        {
            $int_salwar_yourheight = $_POST["txt_salwar_yourheight"];
        }
        //print $int_salwar_yourheight;exit;
        /*if($int_salwar_yourheight == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Your Height";
            echo json_encode($response);
            return;
        }*/
        if($int_salwar_yourheight != "")
        {
            $str_measurement_details .= "Your Height:".$int_salwar_yourheight."|~|";
        }
        
        
        
        $int_salwar_frontneckdepth = 0;
        if(isset($_POST["txt_salwar_frontneckdepth"]) && $_POST["txt_salwar_frontneckdepth"] != "")
        {
            $int_salwar_frontneckdepth = $_POST["txt_salwar_frontneckdepth"];
        }
        /*if($int_salwar_frontneckdepth <= 0 || $int_salwar_frontneckdepth == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Front Neck Depth";
            echo json_encode($response);
            return;
        }*/
        if($int_salwar_frontneckdepth != "")
        {
            $str_measurement_details .= "Front Neck Depth:".$int_salwar_frontneckdepth."|~|";
        }
        
        $int_salwar_bottomlength = 0;
        if(isset($_POST["txt_salwar_bottomlength"]) && $_POST["txt_salwar_bottomlength"] != "")
        {
            $int_salwar_bottomlength = $_POST["txt_salwar_bottomlength"];
        }
        /*if($int_salwar_bottomlength <= 0 || $int_salwar_bottomlength == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Bottom Length";
            echo json_encode($response);
            return;
        }*/
        if($int_salwar_bottomlength != "")
        {
            $str_measurement_details .= "Bottom Length:".$int_salwar_bottomlength."|~|";
        }
        
        $int_salwar_kameezlength = 0;
        if(isset($_POST["txt_salwar_kameezlength"]) && $_POST["txt_salwar_kameezlength"] != "")
        {
            $int_salwar_kameezlength = $_POST["txt_salwar_kameezlength"];
        }
        /*if($int_salwar_kameezlength <= 0 || $int_salwar_kameezlength == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Kameez Length";
            echo json_encode($response);
            return;
        }*/
        if($int_salwar_kameezlength != "")
        {
            $str_measurement_details .= "Kameez Length:".$int_salwar_kameezlength."|~|";
        }
        
        $int_salwar_sleevelength = 0;
        if(isset($_POST["txt_salwar_sleevelength"]) && $_POST["txt_salwar_sleevelength"] != "")
        {
            $int_salwar_sleevelength = $_POST["txt_salwar_sleevelength"];
        }
        /*if($int_salwar_sleevelength <= 0 || $int_salwar_sleevelength == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Sleeve Length";
            echo json_encode($response);
            return;
        }*/
        
        if($int_salwar_sleevelength != "")
        {
            $str_measurement_details .= "Sleeve Length:".$int_salwar_sleevelength."|~|";
        }
        
        
        $str_salwar_sleevestyle = "";
        if(isset($_POST["cbo_salwar_sleevestyle"]) && $_POST["cbo_salwar_sleevestyle"] != "")
        {
            $str_salwar_sleevestyle = $_POST["cbo_salwar_sleevestyle"];
        }
        /*if($str_salwar_sleevestyle == "")
        {
            $response['status']='ERR';
            $response['message']="Select Sleeve Style";
            echo json_encode($response);
            return;
        }*/
        if($str_salwar_sleevestyle != "")
        {
            $str_measurement_details .= "Sleeve Style:".$str_salwar_sleevestyle."|~|";
        }
        
        
        
        
        $str_salwar_around_bust = 0;
        if(isset($_POST["cbo_salwar_aroundbust"]) && $_POST["cbo_salwar_aroundbust"] != "")
        {
            $str_salwar_around_bust = $_POST["cbo_salwar_aroundbust"];
        }
        /*if($str_salwar_around_bust == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Around Bust";
            echo json_encode($response);
            return;
        }*/
        if($str_salwar_around_bust != "")
        {
            $str_measurement_details .= "Around Bust:".$str_salwar_around_bust."|~|";
        }
        
        $int_salwar_around_above_waist = 0;
        if(isset($_POST["txt_salwar_abovearoundwaist"]) && $_POST["txt_salwar_abovearoundwaist"] != "")
        {
            $int_salwar_around_above_waist = $_POST["txt_salwar_abovearoundwaist"];
        }
        /*if($int_salwar_around_above_waist <= 0 || $int_salwar_around_above_waist == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Above Around Waist";
            echo json_encode($response);
            return;
        }*/
        if($int_salwar_around_above_waist != "")
        {
            $str_measurement_details .= "Around Above Waist:".$int_salwar_around_above_waist."|~|";
        }
        
        $int_salwar_around_waist = 0;
        if(isset($_POST["txt_salwar_aroundwaist"]) && $_POST["txt_salwar_aroundwaist"] != "")
        {
            $int_salwar_around_waist = $_POST["txt_salwar_aroundwaist"];
        }
        /*if($int_salwar_around_waist <= 0 || $int_salwar_around_waist == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Around Waist";
            echo json_encode($response);
            return;
        }*/
        if($int_salwar_around_waist != "")
        {
            $str_measurement_details .= "Around Waist:".$int_salwar_around_waist."|~|";
        }
        
        
        
        $int_salwar_around_hips = 0;
        if(isset($_POST["txt_salwar_aroundhips"]) && $_POST["txt_salwar_aroundhips"] != "")
        {
            $int_salwar_around_hips = $_POST["txt_salwar_aroundhips"];
        }
        /*if($int_salwar_around_hips <= 0 || $int_salwar_around_hips == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Around Hips";
            echo json_encode($response);
            return;
        }*/
        if($int_salwar_around_hips != "")
        {
            $str_measurement_details .= "Around Hips:".$int_salwar_around_hips."|~|";
        }
        
        $int_salwar_backneckdepth = 0;
        if(isset($_POST["txt_salwar_backneckdepth"]) && $_POST["txt_salwar_backneckdepth"] != "")
        {
            $int_salwar_backneckdepth = $_POST["txt_salwar_backneckdepth"];
        }
        /*if($int_salwar_backneckdepth <= 0 || $int_salwar_backneckdepth == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Back Neck Depth";
            echo json_encode($response);
            return;
        }*/
        if($int_salwar_backneckdepth != "")
        {
            $str_measurement_details .= "Back Neck Depth:".$int_salwar_backneckdepth."|~|";
        }
        
        $int_salwar_around_thigh = 0;
        if(isset($_POST["txt_salwar_aroundthigh"]) && $_POST["txt_salwar_aroundthigh"] != "")
        {
            $int_salwar_around_thigh = $_POST["txt_salwar_aroundthigh"];
        }
       /* if($int_salwar_around_thigh <= 0 || $int_salwar_around_thigh == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Around Thigh";
            echo json_encode($response);
            return;
        }*/
        if($int_salwar_around_thigh != "")
        {
            $str_measurement_details .= "Around Thigh:".$int_salwar_around_thigh."|~|";
        }
        
        $int_salwar_around_knee = 0;
        if(isset($_POST["txt_salwar_aroundknee"]) && $_POST["txt_salwar_aroundknee"] != "")
        {
            $int_salwar_around_knee = $_POST["txt_salwar_aroundknee"];
        }
        /*if($int_salwar_around_knee <= 0 || $int_salwar_around_knee == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Around Knee";
            echo json_encode($response);
            return;
        }*/
        if($int_salwar_around_knee != "")
        {
            $str_measurement_details .= "Around Knee:".$int_salwar_around_knee."|~|";
        }
        
        $int_salwar_around_calf = 0;
        if(isset($_POST["txt_salwar_aroundcalf"]) && $_POST["txt_salwar_aroundcalf"] != "")
        {
            $int_salwar_around_calf = $_POST["txt_salwar_aroundcalf"];
        }
        /*if($int_salwar_around_calf <= 0 || $int_salwar_around_calf == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Around Calf";
            echo json_encode($response);
            return;
        }*/
        if($int_salwar_around_calf != "")
        {
            $str_measurement_details .= "Around Calf:".$int_salwar_around_calf."|~|";
        }
        
        $int_salwar_around_ankle = 0;
        if(isset($_POST["txt_salwar_aroundankle"]) && $_POST["txt_salwar_aroundankle"] != "")
        {
            $int_salwar_around_ankle = $_POST["txt_salwar_aroundankle"];
        }
        /*if($int_salwar_around_ankle <= 0 || $int_salwar_around_ankle == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Around Ankle";
            echo json_encode($response);
            return;
        }*/
        if($int_salwar_around_ankle != "")
        {
            $str_measurement_details .= "Around Ankle:".$int_salwar_around_ankle."|~|";
        }
        
        
        //print $str_measurement_details; exit;
        
        
        
        
    }
    else if($int_tailoring_option == 2)
    {
        $int_semistitched_size = 0;
        if(isset($_POST["cbo_semistitched_stitchingsize"]) && $_POST["cbo_semistitched_stitchingsize"] != "")
        {
            $int_semistitched_size = $_POST["cbo_semistitched_stitchingsize"];
        }
        /*if($int_salwar_around_ankle <= 0 || $int_salwar_around_ankle == "")
        {
            $response['status']='ERR';
            $response['message']="Enter Around Ankle";
            echo json_encode($response);
            return;
        }*/
        if($int_semistitched_size != "")
        {
            $str_measurement_details .= "Semi Stitched Size:".$int_semistitched_size;
        }
        
    }
    /*else if($int_tailoring_option == 1)
    {
          $str_measurement_details .="";
    }*/
}
//print $int_tailoring_price;
//exit;


$int_shippingvalue = (($int_weight * $int_courier_price_per_kg) / $int_conversionrate) * $int_quantity; 

/*$int_shippingvalue = 0.00;
if(isset($_POST["rdo_shipping"]) && $_POST["rdo_shipping"] != "")
{
    $int_shippingvalue = $_POST["rdo_shipping"];
}*/
//print $int_shippingvalue; exit;


/*$str_desc = "";
if(isset($_POST["hdn_description"]) && $_POST["hdn_description"] != "")
{
    $str_desc = trim($_POST["hdn_description"]);
}

$str_shipping_info= "";
if(isset($_POST["hdn_shippinginfo"]) && $_POST["hdn_shippinginfo"] != "")
{
    $str_shipping_info = trim($_POST["hdn_shippinginfo"]);
}*/

$int_price = 0.00;
if(isset($_POST["hdn_price"]) && $_POST["hdn_price"] != "")
{
    $int_price = $_POST["hdn_price"];
}


//print $int_price;exit;


$item_sku = "";
$item_sessionid = "";
$item_uniqueid = "";

if(isset($_POST['specssku']))
{
    $item_sku = GetDecryptId($_POST['specssku']);
        //$item_sku=$_GET['specssku'];
}

//var_dump($item_sku); exit;
/*if($item_sku=="")
{
    CloseConnection();		
    Redirect("../online-store");
    exit();
}*/

//print($_SESSION["sessionid"]); exit;
if(isset($_SESSION['sessionid']))
{
    $item_sessionid = $_SESSION['sessionid'];
}

if(isset($_SESSION['uniqueid']))
{
    $item_uniqueid = $_SESSION['uniqueid'];
}
#----------------------------------------------------------------------
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_SESSION_CART." WHERE sku='" . $item_sku . "' AND sessionid='" . $item_sessionid . "'";
$str_query_select = $str_query_select." AND uniqueid='" . $item_uniqueid . "'";
	//print "SELECT: ".$str_query_select."<br/>"; exit;
$rs_list_cart = GetRecordSet($str_query_select);
	
if(!$rs_list_cart->EOF())
{
    /*$var_quantity = $rs_list_cart->fields("quantity");
    $var_price = $rs_list_cart->fields("price") + $int_tailoring_price;
    $var_quantity = $var_quantity + $int_quantity;
    $var_extendedprice = $var_price * $var_quantity;

    $strsql="UPDATE ".$STR_DB_TABLE_NAME_SESSION_CART." SET color='".$str_color."', size='".$str_size."', quantity=" . $var_quantity . ",extendedprice='" . $var_extendedprice . "' WHERE sku='" . $item_sku . "' AND sessionid='" . $item_sessionid . "' AND uniqueid='" . $item_uniqueid . "'";
    ExecuteQuery($strsql);
    //print "UPDATE: ".$strsql."<br>";  exit;
    
    $response['status']='SUC';
    $response['message']="Item updated to cart.";
    echo json_encode($response);
    return;*/
    $response['status']='SUC';
    $response['message']="Item already added to your cart.";
    echo json_encode($response);
}
else
{
    $var_cnt = 0;
    $str_where_seriolno = "";
    if(isset($_COOKIE["sessionid"]) && isset($_COOKIE["uniqueid"]) && $_COOKIE["sessionid"] != "" && $_COOKIE["uniqueid"] != "") 
    {
        $str_where_seriolno = " sessionid='".$_COOKIE["sessionid"]."' AND uniqueid='".$_COOKIE["uniqueid"]."' ";
    }
    else
    {
        $str_where_seriolno = " sessionid='".$item_sessionid."' AND uniqueid='".$item_uniqueid."' ";
    }
    
            
    #Below query get max serialno for t_sessioncart table.
    $str_query_select = "SELECT max(serialno) AS srno from ".$STR_DB_TABLE_NAME_SESSION_CART." WHERE".$str_where_seriolno;
    
    //print $str_query_select; exit;
    $rs_list_serialno = GetRecordSet($str_query_select);
    if($rs_list_serialno->EOF() == false)
    {
        if($rs_list_serialno->fields("srno") == "")
        {
            $var_cnt = 1;
        }
        else
        {				
            $var_cnt = $rs_list_serialno->fields("srno") + 1;
        }
    }

    $item_extprice = $int_price * $int_quantity;
    
    #----------------------------------------------------------------------
    $str_query_select = "";
    $str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_TYPE." a LEFT JOIN ".$STR_DB_TABLE_NAME_TYPE." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid=".$item_sku;
    $rs_list_type = GetRecordSet($str_query_select);
    if($rs_list_type->Count() > 0) 
    { 
        $str_additional_info = " | Type : ";
        while(!$rs_list_type->EOF()) 
        {
            $str_additional_info .= $rs_list_type->Fields("title");
            $rs_list_type->MoveNext(); 
        }
    } 
    
    $str_query_select = "";
    $str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_FABRIC." a LEFT JOIN ".$STR_DB_TABLE_NAME_FABRIC." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid=".$item_sku;
    $rs_list_fabric = GetRecordSet($str_query_select);
    if($rs_list_fabric->Count() > 0) 
    { 
        $str_additional_info .= " | Fabric  : ";
        while(!$rs_list_fabric->EOF()) 
        {
            $str_additional_info .= $rs_list_fabric->Fields("title");
            $rs_list_fabric->MoveNext(); 
        }
    }
    
    $str_query_select = "";
    $str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_WORK." a LEFT JOIN ".$STR_DB_TABLE_NAME_WORK." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid=".$item_sku;
    $rs_list_work = GetRecordSet($str_query_select);
    if($rs_list_work->Count() > 0) 
    { 
        $str_additional_info .= " | Work : ";
        while(!$rs_list_work->EOF()) 
        {
            $str_additional_info .= $rs_list_work->Fields("title");
            $rs_list_work->MoveNext(); 
        }
    }
    
    $str_query_select = "";
    $str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_LENGTH." a LEFT JOIN ".$STR_DB_TABLE_NAME_LENGTH." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid=".$item_sku;
    $rs_list_length = GetRecordSet($str_query_select);
    if($rs_list_length->Count() > 0) 
    { 
        $str_additional_info .= " | Length : ";
        while(!$rs_list_length->EOF()) 
        {
            $str_additional_info .= $rs_list_length->Fields("title");
            $rs_list_length->MoveNext(); 
        }
    }
    
    $str_query_select = "";
    $str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_OCCASION." a LEFT JOIN ".$STR_DB_TABLE_NAME_OCCASION." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid=".$item_sku;
    $rs_list_occasion = GetRecordSet($str_query_select);
    if($rs_list_occasion->Count() > 0) 
    { 
        $str_additional_info .= " | Occasion : ";
        while(!$rs_list_occasion->EOF()) 
        {
            $str_additional_info .= $rs_list_occasion->Fields("title");
            $rs_list_occasion->MoveNext(); 
        }
    }
                                                              
    #----------------------------------------------------------------------                                                          
                                                        
    #Below query inserts purchased item data into t_sessioncart table.
    $str_query_insert = "";
    $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_SESSION_CART."(sku,weight,serialno,sessionid,uniqueid,catpkid,subcatpkid,productpkid, userpkid, username,memberpkid, membername, referredbypkid, commission, wholesalerdiscount,producttitle, itemcode, cattitle, subcattitle,tailoringoptionpkid, tailoringoptiontitle, tailoringprice,tailoringmeasurements,quantity, price ,extendedprice, shippingvalue, color, size, type, sentdate, countrypkid, conversionrate) VALUES ("; 
    $str_query_insert = $str_query_insert."" . ReplaceQuote($item_sku) . ",".$int_weight.",". $var_cnt . ",'" . $item_sessionid . "','" . $item_uniqueid . "',". $int_catpkid.",".$int_subcatpkid."," . $int_pkid . ", ".$int_userpkid.", '".$str_username."', 0, '', ".$int_referredbypkid.", ".$int_commission.", ".$int_discount.",'" . $str_title ."','" . $str_itemcode . "','" . $str_prodcattitle . "', '".$str_prodsubcattitle."', " . $int_tailoring_option . ", '" . $str_tailoring_option . "' , " . $int_tailoring_price . ", '" . $str_measurement_details . "', " . $int_quantity . ", " . $int_price . "," . $item_extprice  . "," . $int_shippingvalue . ", '".$str_color."', '".$str_size."', '".$str_additional_info."', NOW() ,".$_SESSION["usr_cntry"].",".$int_conversionrate.")"; 


//INSERT INTO t_store_sessioncart(sku,weight,serialno,sessionid,uniqueid,catpkid,subcatpkid,productpkid, userpkid, username,memberpkid, membername, referredbypkid, commission, wholesalerdiscount,producttitle,cattitle, subcattitle,tailoringoptionpkid, tailoringoptiontitle, tailoringprice,tailoringmeasurements,quantity, price ,extendedprice, shippingvalue, color, size, type) VALUES (132,550.00,1,'a7b86b47a71a22960035ed2328c91b77','20200724043953',5,0,132, 0, '', 0, '', 0, 0, 0,'Opulent Cream Colored Casual Floral Printed Rayon Kurti-Palazzo','Kurtis', '', 0, '' , 0, '', 1, 570,570,0, '', '', '')


    //print "INSERT: ".$str_query_insert; exit;
    ExecuteQuery($str_query_insert);
    
    
    /*$int_cookie_sessionid = "";
    if(isset($_COOKIE['sessionid']) && $_COOKIE['sessionid'] != "")
    {
        $arr_sessionid = array();
        $arr_sessionid =  explode("|", $_COOKIE["sessionid"]);
        
        if(!in_array($item_sessionid, $arr_sessionid)) 
        {
            $int_cookie_sessionid = trim($_COOKIE['sessionid'])."|".trim($item_sessionid);
            setcookie("sessionid", $int_cookie_sessionid, time() + (10 * 365 * 24 * 60 * 60));
        }
    }
    else
    {
        $int_cookie_sessionid = $item_sessionid;
        setcookie("sessionid", $int_cookie_sessionid, time() + (10 * 365 * 24 * 60 * 60));
    }
    
    $int_cookie_uniqueid = "";
    if(isset($_COOKIE['uniqueid']) && $_COOKIE['uniqueid'] != "")
    {
        $arr_uniqueid = array();
        $arr_uniqueid =  explode("|", $_COOKIE["sessionid"]);
        
        if(!in_array($item_uniqueid, $arr_uniqueid)) 
        {
            $int_cookie_uniqueid = trim($_COOKIE['uniqueid'])."|".trim($item_uniqueid);
            setcookie("uniqueid", $int_cookie_uniqueid, time() + (10 * 365 * 24 * 60 * 60));
        }
    }
    else
    {
        $int_cookie_uniqueid = $item_uniqueid;
        setcookie("uniqueid", $int_cookie_uniqueid, time() + (10 * 365 * 24 * 60 * 60));
    }
    
    //setcookie("sessionid", $int_cookie_sessionid, time() + (10 * 365 * 24 * 60 * 60));
    //setcookie("uniqueid", $int_cookie_uniqueid, time() + (10 * 365 * 24 * 60 * 60));
    

    //setcookie("sessionid", $item_sessionid, time() + (10 * 365 * 24 * 60 * 60));
    //setcookie("uniqueid", $item_uniqueid, time() + (10 * 365 * 24 * 60 * 60));
     * 
     * 
    */
    
    
    $int_old_sessionid = "";
    if(isset($_COOKIE['sessionid']) && $_COOKIE['sessionid'] != "")
    {
        $int_old_sessionid = $_COOKIE['sessionid'];
    }
    
    $int_old_uniqueid = "";
    if(isset($_COOKIE['uniqueid']) && $_COOKIE['uniqueid'] != "")
    {
        $int_old_uniqueid = $_COOKIE['uniqueid'];
    }
    
    if($int_old_sessionid != "" && $int_old_uniqueid != "")
    {
    
        $str_query_update = "";
        $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_SESSION_CART." SET sessionid='".$item_sessionid."', uniqueid='".$item_uniqueid."' WHERE sessionid='".$int_old_sessionid."' AND uniqueid='".$int_old_uniqueid."'";
        //print $str_query_update; exit;
        ExecuteQuery($str_query_update);
        setcookie("sessionid", $item_sessionid, time() + (10 * 365 * 24 * 60 * 60));
        setcookie("uniqueid", $item_uniqueid, time() + (10 * 365 * 24 * 60 * 60));
    }
    else
    {
        setcookie("sessionid", $item_sessionid, time() + (10 * 365 * 24 * 60 * 60));
        setcookie("uniqueid", $item_uniqueid, time() + (10 * 365 * 24 * 60 * 60));
    }
    
    //exit;
    
    $response['status']='SUC';
    $response['message']="Item added to cart.";
    echo json_encode($response);
    return;
}
exit;
