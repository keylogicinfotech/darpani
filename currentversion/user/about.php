<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#-------------------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
//include "./../includes/http_to_https.php";	
include "./../includes/lib_image.php";
include "./../includes/lib_xml.php";

#-------------------------------------------------------------------------------
$str_page_title = "About Us";
$str_img_path = "./mdm/about/";
$str_xml_file_name = "about.xml";
$str_xml_file_name_cms = "about_cms.xml";

$str_title_page_metatag = "PG_ABOUT";
$str_db_table_name_metatag = "t_page_metatag";
$str_db_table_name = "t_about";

$str_desc_about = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_MODULE.$str_xml_file_name);
$str_desc_about = getTagValue("DESCRIPTION", $fp);
$str_title_about = getTagValue("TITLE", $fp);
$str_image_about = getTagValue("IMAGENAME", $fp);


#getting datas from module xmlfile.
$arr_xml_list="";
$arr_xml_list=readXml($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");


#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);

#----------------------------------------------------------------------
# Select Query to get blog details
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$str_db_table_name." WHERE visible = 'YES' ORDER BY title ASC";
//print $str_query_select;exit;
$rs_list_about = GetRecordSet($str_query_select);

$str_about_title = "";
$str_about_title = $rs_list_about->Fields("title");
//print $str_about_title;exit;
#----------------------------------------------------------------------
# Get metatag page title
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatag. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = "";
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />   
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
</head>

<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <?php if($str_page_title != ""){ ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_page_title; ?></h1>
                <hr/>
            </div>
        </div>  
        <?php } ?>
        
        <?php if($str_desc_cms != ""){ ?>
        <div class="row padding-10">
            <div class="col-md-12">
                <?php if($str_desc_cms != "" && $str_desc_cms != "<br>" && $str_visible_cms == "YES") { ?>
                    <div  class="well"><p align="justify" class="nopadding"><?php print($str_desc_cms);?></p></div>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
       
        
         <?php  $int_cnt = 0; ?>
            <?php

            $arr_test=array_keys($arr_xml_list);
            if($arr_test[0]!= "ROOT_ITEM")
            { ?>
                           <?php 
// print_r($arr_test);                  
            //while(list($key,$val) = each($arr_xml_list)) 
            foreach($arr_xml_list as $key => $val) 
                    
            {?>


             <?php   if(is_array($val))
                { ?>

                <?php if(($arr_xml_list[$key]["IMAGENAME"]!="") && ($arr_xml_list[$key]["POSITION"]=="LEFT"))  { ?>
                <div class="row padding-10">
                    <div class="col-lg-4 col-md-4">
                       <img class="img-responsive <?php /*?>img-hover<?php */?>" src="<?php print($str_img_path.$arr_xml_list[$key]["IMAGENAME"]); ?>" border="0" alt="<?php print $str_page_title;?> Image" title="<?php print $str_page_title;?> Image" align="middle"/>

                    </div>  
                        <div class="col-lg- col-md-">
                            <h3 align="left" class="nopadding"><?php print(($arr_xml_list[$key]["TITLE"])) ?></h3>
                            <p align="justify" class="nopadding"><?php print(($arr_xml_list[$key]["DESCRIPTION"])) ?></p>
                        </div>
                    </div><br/>
                    <?php } else if(($arr_xml_list[$key]["IMAGENAME"]=="")){ ?>
                 <div class="row padding-10">
                    <div class="col-lg-12 col-md-12">
                        <h3 align="left" class="nopadding"><?php print(($arr_xml_list[$key]["TITLE"])) ?></h3>
                           <p align="justify" class="nopadding"><?php print(($arr_xml_list[$key]["DESCRIPTION"])) ?></p>
                       </div>
                 </div><br/>
                   <?php } else{ ?>
                <div class="row padding-10">
                    <div class="col-lg- col-md-">
                        <h3 align="left" class="nopadding"><?php print(($arr_xml_list[$key]["TITLE"])) ?></h3>
                        <p align="justify" class="nopadding"><?php print(($arr_xml_list[$key]["DESCRIPTION"])) ?></p>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <p  data-toggle="modal" data-target=".f-pop-up-<?php print(MyHtmlEncode($arr_xml_list[$key]['PKID'])); ?>" rel="thumbnail"><img class="img-responsive <?php /*?>img-hover<?php */?>" src="<?php print($str_img_path.$arr_xml_list[$key]["IMAGENAME"]); ?>" border="0" alt="<?php print $str_page_title;?> Image" title="<?php print $str_page_title;?> Image" align="middle"/></p>
                    </div> 
                  </div><br/>
                  <?php  } 
                  } } } ?>
       
  
        <?php //include($STR_USER_BANNER_PATH); ?><br/>
        </div>

       
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
</body>
    <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
    </script>
<!--top top bottom arrow-->
<script>
$(document).ready(function(){ 
    $(window).scroll(function(){ 
        if ($(this).scrollTop() > 100) { 
            $('#scroll').fadeIn(); 
        } else { 
            $('#scroll').fadeOut(); 
        } 
    }); 
    $('#scroll').click(function(){ 
        $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }); 
});
</script>
<script type="text/javascript">
        $( document ).ready(function() {
            $('#load_cartitems').load('<?php print($STR_SITENAME_WITH_PROTOCOL);?>/user/product_cart_count_p.php');
            $('#mobile-cart-count').load('<?php print($STR_SITENAME_WITH_PROTOCOL);?>/user/product_cart_count_p.php');
        });
	var auto_refresh = setInterval(
	function ()
	{
            $('#load_cartitems').load('<?php print($STR_SITENAME_WITH_PROTOCOL);?>/user/product_cart_count_p.php').fadeIn("slow");
            $('#mobile-cart-count').load('<?php print($STR_SITENAME_WITH_PROTOCOL);?>/user/product_cart_count_p.php').fadeIn("slow");
	}, 1000); // refresh every 10000 milliseconds
        
    </script>
<!--end top top bottom arrow-->
    
</html>
