<?php 
/*
File Name  :- user_home.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#---------------------------------------------------------------------
#Include files
session_start();
include "./../includes/validate_session_user.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
//include "./../includes/http_to_https.php";	
include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
//print_r($_SESSION);exit;
#----------------------------------------------------------------------
//$str_title_page = "";
$str_title_page = "My Dashboard";
$str_db_table_name = " t_user";
$str_db_table_name_news = "t_news";
$str_db_table_name_metatage = " t_page_metatag ";

$int_userpkid = 0;
$terms_usage="";
$fp=OpenXMLFile($STR_XML_FILE_PATH_CMS."user_cms.xml");
//$str_desc_cms = getTagValue("ITEMKEYVALUE_LOGIN",$fp);
$terms_usage=getTagValue("ITEMKEYVALUE_USAGETERMS",$fp);
//print "terms of usage:" .$terms_usage;  exit;
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table
/*$str_title_page_metatag = "PG_MT_HOME";
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatage. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."'";
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");*/
#----------------------------------------------------------------------
$str_query_select="SELECT * FROM " .$str_db_table_name. " WHERE pkid=".$_SESSION["userpkid"]."";
//print $str_query_select; 
$rs_list=GetRecordset($str_query_select);
//$int_userpkid = $_SESSION["userpkid"]; 
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print $STR_TITLE_MODEL; ?> - <?php print $str_title_page; ?></title>
    <title><?php // print($STR_SITE_TITLE);?> : <?php //print($rs_list_mt->fields("titletag")) ;?></title>
    <?php //print(Display_Page_Metatag($str_title_page_metatag)); ?>
     <?php include "../includes/include_files_user.php"; ?>
</head>
<body>
            
<?php include("../includes/header.php"); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-md-3 col-xs-12 col-sm-12">
                <?php include("../includes/panel_user.php"); ?>
            </div>
            <div class="col-md-9 col-xs-12 col-sm-12">
                <div class="row">
                    <a name="ptop" id="ptop"></a>
                    <div class="col-md-12">
                        <h3 align="right"><?php print $str_title_page; ?></h3>           
                    </div>
                </div>
                <hr/>
            <?php if($rs_list->fields("approved") == "NO") { ?>

            <?php if(trim($rs_list->fields("shortenurlkey"))!="" && trim($rs_list->fields("name"))!="" && trim($rs_list->fields("emailid"))!="" && trim($rs_list->fields("countrypkid"))!="" && trim($rs_list->fields("statepkid"))!="" && trim($rs_list->fields("city"))!="" && trim($rs_list->fields("zipcode"))!="" && trim($rs_list->fields("address"))!="" && trim($rs_list->fields("description"))!=""  && (trim($rs_list->fields("imagefilenamenew"))!="" || trim($rs_list->fields("imagefilenamelarge"))!="") && trim($rs_list->fields("uscproof1")) && trim($rs_list->fields("uscproof2"))!="" && $rs_list->fields("dateofbirth")!="" ) { ?> 
            </div>
            <div class="row">
                <div class="col-md-12 alert alert-success">
                    Thank you for your submission! Please wait while the site administrator reviews your application. You will be notified by email once your profile is approved or if anything else is required from you. Thank you.
                </div><br />
            </div>
            <?php } else { ?>
            <div class="row">
                <div class="col-md-12 breadcrumb">
                    You are <b>NOT APPROVED YET</b> by the site administrator.<br />
                    Please complete the 3 required sections at the Control Panel to get approved:<br/><br/>
                    &bull; Age & Identity Verification - Upload 2 Forms of ID<br/> &bull; Agreement - Review & E-Sign Agreement<br/>&bull; Personal Details - Complete your Profile Details<br /><br/>
                    If you have completed the 3 required sections to get approved then please wait while site administrator reviews your submission.  You will be notified by email once your profile has been approved.
                </div><br />
            </div>
            <?php } ?>
            <?php } ?>
            <?php if($rs_list->fields("approved") == "YES") { ?>
            <?php 
		$str_query_select="";
		$str_query_select="SELECT * FROM " .$str_db_table_name_news. " WHERE typefornews = 'MODEL' AND visible='YES' AND archive='NO' ORDER BY displayorder DESC, date DESC,pkid,heading" ;
//                print $str_query_select;
		$rs_list_news=GetRecordSet($str_query_select);
            ?>
                <?php if(!$rs_list_news->eof()) { ?>
            <div class="row padding-10">      
                <div class="col-md-1 text-center"></div> 
                <div class="col-md-5 text-center"> 
                    <a href="./user_home.php#ptop" title="Click to view latest announcements" type="button" class="btn btn-warning btn-block disabled"><i class="fa fa-bullhorn" aria-hidden="true"></i>&nbsp;<b>View Latest Announcements</b></a>
                </div>
                <div class="col-md-5 text-center"> 
                    <a href="./user_home_archived_news.php#ptop" title="Click to view past announcements" type="button" class="btn btn-default btn-block"><i class="fa fa-bullhorn" aria-hidden="true"></i>&nbsp;<b>View Past Announcements</b></a> 
                </div>
                <div class="col-md-1 text-center"></div>
            </div>
            <br/>
        
        <?php 
            $int_cnt=1;
            while(!$rs_list_news->eof())
            { ?>

          <?php if($rs_list_news->fields("imagefilename")!="") { ?>
                    <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list_news->fields("pkid"));?>" rel="thumbnail"><img class="img-responsive img-thumbnail" src="<?php print($STR_UPLOAD_NEWS_PATH.$rs_list_news->fields("imagefilename"));?>"  border="0" align="absmiddle" alt="<?php print $rs_list_news->fields("imagefilename")?>" title="<?php print $rs_list_news->fields("imagefilename")?>"></a>
                    <div class="modal fade f-pop-up-<?php print($rs_list_news->fields("pkid")); ?> " align="center" role="dialog" aria-labelledby="" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                    <img src="<?php print($STR_UPLOAD_NEWS_PATH.$rs_list_news->fields("imagefilenamelarge"));?>" class="img-responsive img-rounded" title="<?php print $str_title_page; ?> image" alt="<?php print $str_title_page; ?> image">
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div>															
                
              <?php } ?>
                <div class="row">
                    <div class="col-md-12">
                        <h4><b><?php print $rs_list_news->Fields("heading"); ?></b></h4>
                        <p align="justify"><?php print $rs_list_news->Fields("description"); ?></p><hr/>                             
                    </div>
                </div> 
                <?php 
                $int_cnt=$int_cnt+1;
                $rs_list_news->MoveNext();
                } //while end
		?>
            <?php  }?>
            <?php if($terms_usage != "") { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="thumbnail bg-terms">
                                <h3>Terms Of Use</h3>
                                <p class="text-help"><?php  print($terms_usage);?></p>
                            </div>
                        </div>
                    </div>
                        <?php } ?>
            <?php } ?> 
        </div>
    </div>
</div>
    <?php include("../includes/footer.php");CloseConnection();?>
</body>
</html>
