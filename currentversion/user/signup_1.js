/*
  Jquery Validation using jqBootstrapValidation
   example is taken from jqBootstrapValidation docs 
  */
$(function() {
		   
    $("#FormPLogin input").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // something to have when submit produces an error ?
            // Not decided if I need it yet
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var rdo_usertype = $("input[name='rdo_usertype']:checked").val();
            var l_loginid = $("input#l_loginid").val();
            var l_password = $("input#l_password").val();
            var l_conf_password = $("input#l_conf_password").val();
            var l_email = $("input#l_email").val();
            //var l_secretcode = $("input#l_secretcode").val();
            $.ajax({
                url: "./user/signup_p.php",
                type: "POST",
		data: {
                    rdo_usertype: rdo_usertype,
                    l_loginid: l_loginid,
                    l_password: l_password,
                    l_conf_password: l_conf_password,
                    l_email: l_email
                    //l_secretcode: l_secretcode
                },
				//dataType: "json",
                cache: false,
				
				
				/*success: function(data) 
				{
					//alert(data);
					var $responseText=data;    
					if($responseText.staus=='SUC')
					{
						// Success message
						$('#success').html("<div class='alert alert-success'>");
						$('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
						.append("</button>");
						$('#success > .alert-success').append("<strong> " + $responseText.message + " </strong>");
						$('#success > .alert-success').append('</div>');

						//clear all fields
						//$('#FormPRegister').trigger("reset");
					}
					else if($responseText.status=='ERR')
					{
						// Fail message
						$('#success').html("<div class='alert alert-danger'>");
						$('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
						.append("</button>");
						$('#success > .alert-danger').append("<strong> " + $responseText.message + " </strong>");
						$('#success > .alert-danger').append('</div>');

						//$('#FormPRegister').trigger("reset");
					}
				},*/
				
				success: function(data) 
				{
					//alert(data);
					var $responseText=JSON.parse(data);
					if($responseText.status == 'SUC')
					{
						// Success message
						$('#success').html("<div class='alert alert-success'>");
						$('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
						.append("</button>");
						$('#success > .alert-success').append("<strong> " + $responseText.message + " </strong>");
						$('#success > .alert-success').append('</div>');
					
						//clear all fields
						$('#FormPRegister').trigger("reset");
					}

					else if($responseText.status == 'ERR')
					{
						// Fail message
						$('#success').html("<div class='alert alert-danger'>");
						$('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
							.append("</button>");
						$('#success > .alert-danger').append("<strong> " + $responseText.message + " ");
						$('#success > .alert-danger').append('</div>');
					}
				},
					
					
				/*success: function(res) 
				{
					alert(res.staus);

					if(res.staus=='OK')
					{
						$('#success').html("<div class='alert alert-success'>");
						$('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
						.append("</button>");
						$('#success > .alert-success')
						.append("<strong> " + res.message + " </strong>");
						$('#success > .alert-success')
						.append('</div>');
					}
					else if(res.status=='ERR')
					{
						// Fail message
						$('#success').html("<div class='alert alert-danger'>");
						$('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
						.append("</button>");
						$('#success > .alert-danger').append("<strong> " + res.message + " ");
						$('#success > .alert-danger').append('</div>');
					}
				}*/
					
					
				
                /*success: function() {
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Your contact inquiry has been submitted, we will contact you Soon. Thank you for communication.</strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#FormPRegister').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + " it seems that my mail server is not responding...</strong> Could you please email me directly to <a href='mailto:info@websitename.com?Subject=Message_Me from websitename.com;>info@websitename.com</a> ? Sorry for the inconvenience!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#FormPRegister').trigger("reset");
                },*/
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#l_loginid').focus(function() {
    $('#success').html('');
});
// JavaScript Document