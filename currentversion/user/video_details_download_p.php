<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";

//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_clip_path = "../mdm/ftpupload/video/";
$str_db_table_name = "tr_video_clip";
//print_r($_GET);exit;
#----------------------------------------------------------------------
$int_pkid = 0;
if(isset($_GET["pkid"]) && $_GET["pkid"] != "")
{
    $int_pkid = GetDecryptId($_GET["pkid"]);
}

if($int_pkid <= 0 || !is_numeric($int_pkid) || $int_pkid == "")
{
    ?>
    <script language="JavaScript">
        window.close();
    </script>
<?php
}
#----------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT title,filename FROM ".$str_db_table_name." WHERE pkid=".$int_pkid;
//print $str_query_select;exit;
$rs_list = GetRecordset($str_query_select);

$str_title = "";
$str_title = $rs_list->Fields("title");

if($rs_list->eof())
{
    ?>
    <script language="JavaScript">
        window.close();
    </script>
<?php
}

$str_query_update = "";
$str_query_update = "UPDATE ".$str_db_table_name." SET noofdownload = noofdownload + 1 WHERE pkid = ".$int_pkid;
ExecuteQuery($str_query_update);

$str_filename = "";
$str_filename = $rs_list->Fields("filename");
//print $str_filename;exit;
$str_download_filename = "";
$str_download_filename = "Video_".date("d_M_Y").".".GetExtension($str_filename);

$str_fname = 
$str_fname = $str_clip_path.$str_filename;
#-----------------------------------------------------------------------------------------------------
@ignore_user_abort();
@set_time_limit(0);
ob_end_clean();

    if (!is_file($str_fname) or connection_status()!=0) return(FALSE);
    
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header("Expires: ".gmdate("D, d M Y H:i:s", mktime(date("H")+2, date("i"), date("s"), date("m"), date("d"), date("Y")))." GMT");
    header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
    header("Content-Type: application/octet-stream");
    header("Content-Length: ".(string)(filesize($str_fname)));
    header("Content-Disposition: attachment; filename=".$str_download_filename);
    header("Content-Transfer-Encoding: binary\n");
    header("Accept-Ranges: bytes");
	

    if ($fp = fopen($fname, 'rb')) 
    {
        while(!feof($fp)) 
        {
            print(fread($fp, 1024*8));
            //echo(@fgets($fp, 8192));

        }
        fclose($fp);
        exit();
    }
CloseConnection();
#-----------------------------------------------------------------------------------------------------
?>