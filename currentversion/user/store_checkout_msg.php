<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
//print "HELLO"; exit;
#----------------------------------------------------------------------
#Include files
//session_start();
include "./../user/store_validatesession.php";
//include "./../includes/validate_session_user.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
//include "./../includes/http_to_https.php";	
include "./../includes/lib_xml.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/lib_datetimeyear.php";
include "./../user/store_config.php";

//print_r($_SESSION);exit;
#----------------------------------------------------------------------
$str_title_page = "";
$str_title_page = $STR_TITLE_PAGE_CHECKOUT_MESSAGE;

$resultCode="";
	if(isset($_GET["st"]))
	{
		$resultCode=RemoveQuote(trim($_GET["st"]));	
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($str_title_page) ;?></title>
    <?php //print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet">      
</head>
<body>
<?php include $STR_USER_HEADER_PATH; ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="row padding-10">
                    <div class="col-md-12">
                        <h1 align="right"><?php print $str_title_page; ?></h1><hr/>           
                    </div>
                </div>
                <div class="col-md-12" align="center">
			<?Php if($resultCode == "s"){ ?>
			<div class="alert alert-success" ><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Your payment has been done successfully! </div>
			<?php } else { ?>
			<div class="alert alert-danger" ><span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;Your payment has been declined! </div>
			<?php } ?>
		</div>
            </div>
        </div>
    </div>        
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include $STR_USER_FOOTER_PATH;CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>    
</body>
</html>
