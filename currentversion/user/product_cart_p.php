<?php
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
//print "HELLO"; exit;
#----------------------------------------------------------------------
#Include files
//session_start();
//print "HELLO"; exit;
include "./product_validatesession.php";
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_xml.php";
include "../includes/lib_file_upload.php";
include "../includes/lib_image.php";
include "../includes/lib_datetimeyear.php";
include "../user/product_config.php";
//include "./../includes/http_to_https.php";

//print_r($_SESSION); exit;
$int_cnt = 0;
$int_quantity = 0;
$int_price = 0.00;
$int_tailoring_price = 0.00;
$int_extended_price = 0.00;
$str_serialno = 0.00;
$str_filter = "";

$item_sessionid = "";
$item_uniqueid = "";
if (isset($_SESSION['sessionid'])) {
    $item_sessionid = $_SESSION['sessionid'];
}
if (isset($_SESSION['uniqueid'])) {
    $item_uniqueid = $_SESSION['uniqueid'];
}
if ($item_sessionid == "" || $item_uniqueid == "") {
    CloseConnection();
    Redirect("./store_cart.php?#ptop");
    exit();
}


$str_where = "";
if (isset($_COOKIE["sessionid"]) && isset($_COOKIE["uniqueid"]) && $_COOKIE["sessionid"] != "" && $_COOKIE["uniqueid"] != "") {
    $str_where = " sessionid='" . $_COOKIE["sessionid"] . "' AND uniqueid='" . $_COOKIE["uniqueid"] . "' ";
} else {
    $str_where = " sessionid='" . $item_sessionid . "' AND uniqueid='" . $item_uniqueid . "' ";
}

//print_r($_POST);

if (isset($_POST["hdn_cnt"]) && $_POST["hdn_cnt"] != "") {
    $int_cnt = $_POST["hdn_cnt"];
}


if (isset($_POST["hdn_serialno"]) && $_POST["hdn_serialno"] != "") {
    $str_serialno = $_POST["hdn_serialno"];
}
$str_filter = "#ret" . $str_serialno;

if (isset($_POST["txt_quantity" . $int_cnt]) && $_POST["txt_quantity" . $int_cnt] != "") {
    $int_quantity = $_POST["txt_quantity" . $int_cnt];
}

//print $int_quantity; exit;
if (isset($_POST["hdn_price"]) && $_POST["hdn_price"] != "") {
    $int_price = $_POST["hdn_price"];
}



$int_weight = 0.00;
if (isset($_POST["hdn_weight"]) && $_POST["hdn_weight"] != "") {
    $int_weight = $_POST["hdn_weight"];
}

$int_courier_price_per_kg = 0.00;
if (isset($_POST["hdn_courier_price_per_kg"]) && $_POST["hdn_courier_price_per_kg"] != "") {
    $int_courier_price_per_kg = $_POST["hdn_courier_price_per_kg"];
}

$int_conversionrate = 0.00;
if (isset($_POST["hdn_conversionrate"]) && $_POST["hdn_conversionrate"] != "") {
    $int_conversionrate = $_POST["hdn_conversionrate"];
}


if (isset($_POST["hdn_tailoring_price"]) && $_POST["hdn_tailoring_price"] != "") {
    $int_tailoring_price = $_POST["hdn_tailoring_price"];
}

$int_extended_price = ($int_price + $int_tailoring_price) * $int_quantity;
$int_shipping_price = (($int_weight * $int_courier_price_per_kg) / $int_conversionrate) * $int_quantity;

$str_query_update = "";
$str_query_update = "UPDATE " . $STR_DB_TABLE_NAME_SESSION_CART . " SET quantity='" . $int_quantity . "', shippingvalue='" . $int_shipping_price  . "', extendedprice='" . $int_extended_price . "' WHERE " . $str_where . " AND serialno='" . $str_serialno . "'";
//print $str_query_update; exit;
ExecuteQuery($str_query_update);

CloseConnection();
Redirect("product_cart.php?" . $str_filter);
exit();
//print $int_extended_price;exit;
//print_r($_POST);exit;
#----------------------------------------------------------------------
