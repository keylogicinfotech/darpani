<?php
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
//print "HELLO"; exit;
#----------------------------------------------------------------------
#Include files
//session_start();
include "../user/product_validatesession.php";
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_xml.php";
include "../includes/lib_file_upload.php";
include "../includes/lib_image.php";
include "../includes/lib_datetimeyear.php";
include "../user/product_config.php";

//include "./../includes/http_to_https.php";

//print_r($_SESSION);
#----------------------------------------------------------------------
$str_title_page = "";
$str_title_page = $STR_TITLE_PAGE_ADDRESS;
//print $str_title_page; exit;


$str_xml_file_name_cms = "";
$str_xml_file_name_cms = "store_cms.xml";

#------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------

# Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
# Get message type.
if (isset($_GET["type"])) {
    switch (trim($_GET["type"])) {
        case ("S"):
            $str_type = "S";
            break;
        case ("E"):
            $str_type = "E";
            break;
        case ("W"):
            $str_type = "W";
            break;
    }
}
#	Get message text.

if (isset($_GET["msg"])) {
    switch (trim($_GET["msg"])) {
        case ("F"):
            $str_message = $STR_MSG_ACTION_INFO_MISSING;
            break;
        case ("S"):
            $str_message = $STR_MSG_ACTION_ADD;
            break;
        case ("D"):
            $str_message = $STR_MSG_ACTION_DELETE;
            break;
        case ("U"):
            $str_message = $STR_MSG_ACTION_EDIT;
            break;
        case ("O"):
            $str_message = $STR_MSG_ACTION_DISPLAY_ORDER;
            break;
    }
}


$item_sessionid = "";
$item_uniqueid = "";

if (isset($_SESSION['sessionid'])) {
    $item_sessionid = $_SESSION['sessionid'];
}
if (isset($_SESSION['uniqueid'])) {
    $item_uniqueid = $_SESSION['uniqueid'];
}

if ($item_sessionid == "" || $item_uniqueid == "") {
    CloseConnection();
    Redirect($STR_SITENAME_WITH_PROTOCOL);
    exit();
}

$str_member_flag = false;
$int_userpkid = 0;
$str_username = "";
if (isset($_SESSION['userpkid']) && $_SESSION['userpkid'] != "") {
    $str_member_flag = true;
    $int_userpkid = $_SESSION['userpkid'];
    $str_username = $_SESSION['username'];
} else {
    CloseConnection();
    Redirect($STR_SITENAME_WITH_PROTOCOL . "/user-login");
    exit();
}

$str_allow_promocode = "NO";
$str_allow_wholesaler_discount = "NO";
$str_username = "";
$str_user_address = "";
$str_user_email = "";
$str_user_phone = "";
$int_user_countrypkid = "";
$int_user_statepkid = "";
$str_user_city = "";
$str_user_zipcode = "";

if ($int_userpkid > 0) {
    $str_query_select = "";
    $str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME_USER . " WHERE pkid=" . $int_userpkid;
    $rs_list_user = GetRecordSet($str_query_select);
    if (!$rs_list_user->EOF()) {
        $str_username = $rs_list_user->Fields("name");
        $str_home_address = $rs_list_user->Fields("address");
        $str_user_email = $rs_list_user->Fields("emailid");
        $str_user_phone = $rs_list_user->Fields("phoneno");
        $int_user_countrypkid = $rs_list_user->Fields("countrypkid");
        $int_user_statepkid = $rs_list_user->Fields("statepkid");;
        $str_user_city = $rs_list_user->Fields("city");
        $str_user_zipcode = $rs_list_user->Fields("zipcode");

        $str_ofc_username = $rs_list_user->Fields("ofc_fullname");
        $str_ofc_address = $rs_list_user->Fields("ofc_address");
        $str_ofc_email = $rs_list_user->Fields("ofc_email");
        $str_ofc_phone = $rs_list_user->Fields("ofc_phoneno");
        $int_ofc_countrypkid = $rs_list_user->Fields("ofc_countrypkid");
        $int_ofc_statepkid = $rs_list_user->Fields("ofc_statepkid");;
        $str_ofc_city = $rs_list_user->Fields("ofc_city");
        $str_ofc_zipcode = $rs_list_user->Fields("ofc_zipcode");
        $str_ofc_address = $rs_list_user->Fields("ofc_address");


        if ($rs_list_user->Fields("isusertype") == "REGULAR") {
            $str_allow_promocode = "YES";
            $str_allow_wholesaler_discount = "NO";
        }

        if ($rs_list_user->Fields("isusertype") == "WHOLESALER") {
            $str_allow_promocode = "NO";
            $str_allow_wholesaler_discount = "YES";
        }
    }
}
//print $str_allow_promocode;

#----------------------------------------------------------------------
#read main xml file
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS . $str_xml_file_name_cms);
$str_desc_cms = "";
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION3", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE3", $fp);

#Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM  " . $STR_DB_TABLE_NAME_STATE . " ORDER BY title ASC";
$rs_list_state = GetRecordSet($str_query_select);
$str_statevalues = "";
while (!$rs_list_state->EOF() == true) {
    $str_statevalues = $str_statevalues . $rs_list_state->fields("masterpkid") . "," . $rs_list_state->fields("pkid") . ",'" . addslashes($rs_list_state->fields("title")) . "',";
    $rs_list_state->MoveNext();
}
$str_statevalues = substr($str_statevalues, 0, strlen($str_statevalues) - 1);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE); ?> : <?php print($str_title_page); ?></title>
    <?php //print(Display_Page_Metatag($str_title_page_metatag)); 
    ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script>
        // document on ready on change radio value set input value according to address like home or office  
        $(document).ready(function() {
            // var address = "home";
            $(document).on("change", ".address", function(e) {
                var address = $(this).val();
                if (address == 'home') {
                    // if home selected set related input value
                    $("input#txt_name").val('<?php echo $str_username ?>');
                    $("textarea#ta_address").val('<?php echo $str_home_address ?>');
                    $("input#txt_email").val('<?php echo $str_user_email ?>');
                    $("input#txt_phone").val(<?php echo $str_user_phone ?>);
                    $("select#cbo_country").val(<?php echo $int_user_countrypkid ?>);
                    $("select#cbo_state").val(<?php echo $int_user_statepkid ?>);
                    $("input#txt_city").val('<?php echo $str_user_city ?>');
                    $("input#txt_pincode").val('<?php echo $str_user_zipcode ?>');
                    // set parameter of get_state using countrypkid
                    get_state(<?php echo $int_user_countrypkid ?>);
                }
                if (address == 'office') {
                    // if office selected then set related input value
                    $("input#txt_name").val('<?php echo $str_ofc_username ?>');
                    $("textarea#ta_address").val('<?php echo $str_ofc_address ?>');
                    $("input#txt_email").val('<?php echo $str_ofc_email ?>');
                    $("input#txt_phone").val(<?php echo $str_ofc_phone ?>);
                    $("select#cbo_country").val(<?php echo $int_ofc_countrypkid ?>);
                    $("select#cbo_state").val(<?php echo $int_ofc_statepkid ?>);
                    $("input#txt_city").val('<?php echo $str_ofc_city ?>');
                    $("input#txt_pincode").val('<?php echo $str_ofc_zipcode ?>');
                    // set parameter of get_state using ofc_countrypkid
                    get_state(<?php echo $int_ofc_countrypkid ?>);
                }
            });
        });
    </script>
</head>

<body>
    <?php include $STR_USER_HEADER_PATH2; ?>
    <div class="container center-bg" style="margin-top:25px;">
        <div class="row padding-10">
            <div class="col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1>
                <hr />
            </div>
        </div>
        <?php if ($str_visible_cms == "YES") { ?>
            <?php if ($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
                <div class="row padding-10">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <div class="well">
                            <p align="justify"><?php print($str_desc_cms); ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>

        <div class="row">
            <form name="frm_shipping_address" id="frm_shipping_address" novalidate>
                <div class="col-md-7">
                    <h4 class="nopadding"><b>Name & Address</b></h4>
                    <hr />

                    <div class="row padding-10">
                        <label class="col-md-12">Please Select Address</label>
                        <div class="col-md-12">
                            <div class="form-check">
                                <input class="form-check-input address" type="radio" name="address" id="home" checked value="home">
                                <label class="form-check-label" for="home">Home</label><br>
                                <span style="padding-left: 15px;"><?php print $str_home_address; ?></span>
                            </div>
                        </div>
                        <div class="col-md-12 mb-5">
                            <div class="form-check">
                                <input class="form-check-input address" type="radio" name="address" id="office" value="office">
                                <label class="form-check-label" for="office">Office
                                </label><br>
                                <span style="padding-left: 15px;"><?php print $str_ofc_address; ?></span>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <label>Enter Name <span class="text-help-form">* </span></label></label>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <input type="text" class="form-control" id="txt_name" name="txt_name" required data-validation-required-message="Enter Full Name" placeholder="Full Name" value="<?php print $str_username; ?>">
                                </div>
                                <p class="help-block"></p>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-6">
                            <label>Phone <span class="text-help-form">* </span></label>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <input type="text" class="form-control" id="txt_phone" name="txt_phone" required="" data-validation-required-message="Enter Phone Number" placeholder="Phone" value="<?php print $str_user_phone; ?>" />
                                </div>
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Email </label>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <input type="email" class="form-control" id="txt_email" name="txt_email" placeholder="Email" value="<?php print $str_user_email; ?>" />
                                </div>
                                <p class="help-block"></p>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-12">
                            <label>Address <span class="text-help-form">* </span></label></label>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <textarea rows="3" cols="100" class="form-control" id="ta_address" name="ta_address" required data-validation-required-message="Enter Address" maxlength="999" placeholder="Address" style="resize:none"><?php print $str_home_address; ?></textarea>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-6">
                            <div class="control-group form-group">
                                <div class="controls">
                                    <label>Select Country</label><span class="text-help-form"> * </span>
                                    <?php
                                    $str_query_select = "";
                                    $str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME_COUNTRY . " WHERE visible='YES' ORDER BY title ASC";
                                    //print $str_query_select; exit;
                                    $rs_list_country = GetRecordSet($str_query_select); ?>
                                    <select name="cbo_country" id="cbo_country" class="form-control" onChange="get_state(this.value);" required="" data-validation-required-message="Select Country">
                                        <option <?php // print(CheckSelected("",$rs_list->fields("statepkid")));
                                                ?> value="0">-- Select Country --</option>
                                        <?php
                                        while (!$rs_list_country->EOF() == true) { ?>
                                            <option value="<?php print($rs_list_country->fields("pkid")) ?>" <?php print(CheckSelected($rs_list_country->fields("pkid"), $int_user_countrypkid)); ?>><?php print($rs_list_country->fields("title")); ?></option>
                                        <?php
                                            $rs_list_country->MoveNext();
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <?php /* ?>
                        <div class="col-md-6">
                            <div class="control-group form-group">
                                <div class="controls">
                                    <label>Select State</label><span class="text-help-form"> *</span>
                                    <select class='form-control' name='cbo_state' id='cbo_state'>
                                        <option value="0">-- Select State --</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <?php */ ?>
                        <div class="col-md-6">
                            <div class="control-group form-group">
                                <div class="controls">
                                    <label>Select State</label><span class="text-help-form"> * </span>
                                    <div id="before_get_state">
                                        <?php
                                        //print_r($_SESSION);

                                        if ($int_user_countrypkid > 0) {
                                            $str_query_select = "";
                                            $str_query_select = "SELECT * FROM  " . $STR_DB_TABLE_NAME_STATE . " WHERE masterpkid=" . $int_user_countrypkid . " AND visible='YES' ORDER BY title ASC";
                                            $rs_list_state_default = GetRecordSet($str_query_select);
                                        ?>
                                            <select name="cbo_state" id="cbo_state" class="form-control">
                                                <option value="0">-- Select State --</option>
                                                <?php
                                                while (!$rs_list_state_default->EOF() == true) { ?>
                                                    <option value="<?php print($rs_list_state_default->fields("pkid")) ?>" <?php print(CheckSelected($rs_list_state_default->fields("pkid"), $int_user_statepkid)); ?>><?php print($rs_list_state_default->fields("title")); ?></option>
                                                <?php
                                                    $rs_list_state_default->MoveNext();
                                                }
                                                ?>
                                            </select>
                                        <?php } else { ?>
                                            <select name="cbo_state" id="cbo_state" class="form-control">
                                                <option value="0">-- Select State --</option>
                                            </select>
                                        <?php } ?>
                                    </div>
                                    <div id="after_get_state"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row padding-10">
                        <div class="col-md-6">
                            <label>Town / City</label><span class="text-help-form"> *</span>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <input type="text" class="form-control" id="txt_city" name="txt_city" required="" data-validation-required-message="Enter City" placeholder="Town / City" value="<?php print $str_user_city; ?>">
                                </div>
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Zip / Postal Code</label><span class="text-help-form"> *</span>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <input type="text" class="form-control" id="txt_pincode" name="txt_pincode" required="" data-validation-required-message="Enter Zip / Postal Code" placeholder="Zip / Postal Code" value="<?php print $str_user_zipcode; ?>">
                                </div>
                                <p class="help-block"></p>
                            </div>
                        </div>
                    </div>
                    <div id="success"></div>
                    <br />
                </div>
                <div class="col-md-5">
                    <?php
                    $var_gift_sku = "FALSE";
                    $var_promo_sku = "FALSE";
                    $var_extprice = 0;
                    $hid_options = "";
                    $hid_serialno = "";

                    # Below recordset fetches all purchased related data from table for particular table.

                    $str_where = "";
                    if (isset($_COOKIE["sessionid"]) && isset($_COOKIE["uniqueid"]) && $_COOKIE["sessionid"] != "" && $_COOKIE["uniqueid"] != "") {
                        $str_where = " WHERE sessionid='" . $_COOKIE["sessionid"] . "' AND uniqueid='" . $_COOKIE["uniqueid"] . "' ";
                    } else {
                        $str_where = " WHERE sessionid='" . $item_sessionid . "' AND uniqueid='" . $item_uniqueid . "' ";
                    }

                    $str_query_select = "";
                    $str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME_SESSION_CART . $str_where . " ORDER BY serialno,sku,producttitle";
                    //print $strsql; exit;
                    $rs_list = GetRecordSet($str_query_select);
                    if ($rs_list->Count() > 0) { ?>
                        <h4 class="nopadding"><b>Order Review</b> <a href="./product_cart.php" class="pull-right">Edit</a></h4>
                        <hr />

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="12%">Product</th>
                                        <th>Details</th>
                                        <th width="1%">Qty</th>
                                        <th width="20%">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $int_subtotal = 0.00;
                                    $int_wholesaler_discount = 0;
                                    $int_total_shipping_price = 0;
                                    while (!$rs_list->EOF()) { ?>
                                        <tr>
                                            <td>
                                                <?php
                                                $str_query_select = "";
                                                $str_query_select = "SELECT a.thumbphotofilename, a.imageurl, b.listprice FROM " . $STR_DB_TABLE_NAME_PHOTO . " a LEFT JOIN " . $STR_DB_TABLE_NAME . " b ON a.masterpkid=b.pkid AND b.approved='YES' AND b.visible='YES' WHERE a.masterpkid=" . $rs_list->Fields("productpkid") . " AND a.visible='YES' AND a.setasfront='YES'";
                                                $rs_list_image = GetRecordSet($str_query_select); ?>
                                                <?php if ($rs_list_image->Fields("thumbphotofilename") != "") { ?>
                                                    <img src="<?php print $UPLOAD_IMG_PATH . $rs_list->Fields("productpkid") . "/" . $rs_list_image->Fields("thumbphotofilename"); ?>" class="img-responsive" alt="<?php print $rs_list->Fields("producttitle"); ?>" title="<?php print $rs_list->Fields("producttitle"); ?>" width="100%" />
                                                <?php } else if ($rs_list_image->Fields("imageurl") != "") { ?>
                                                    <img src="<?php print $rs_list_image->Fields("imageurl"); ?>" class="img-responsive" alt="<?php print $rs_list->Fields("producttitle"); ?>" title="<?php print $rs_list->Fields("producttitle"); ?>" width="100%" />
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <p><?php print MakeStringShort($rs_list->Fields("producttitle"), 30); ?></p>
                                                <hr />

                                                <?php if ($rs_list->Fields("color") != "") { ?>
                                                    <p><?php print $rs_list->Fields("color"); ?></p>
                                                <?php } ?>
                                                <?php if ($rs_list->Fields("size") != "") { ?>
                                                    <p><?php print $rs_list->Fields("size"); ?></p>
                                                <?php } ?>
                                                <?php
                                                $int_tailoring_price = 0.00;
                                                if ($rs_list->Fields("tailoringprice") > 0) {
                                                    $int_tailoring_price = ceil($rs_list->Fields("tailoringprice") / $int_conversionrate); ?>
                                                <?php } else {
                                                    $int_tailoring_price = 0.00;
                                                } ?>
                                            </td>
                                            <td class="text-center align-middle"><?php print $rs_list->Fields("quantity"); ?></td>
                                            <td align="right" class="align-middle">

                                                <?php
                                                /*$int_shipping_price = 0; 
                                        $int_shipping_price = (($rs_list->Fields("weight") * $int_courier_price_per_kg) / $int_conversionrate) * $rs_list->Fields("quantity"); */
                                                $int_total_shipping_price = $int_total_shipping_price + $rs_list->Fields("shippingvalue");

                                                $int_price = 0;
                                                $int_price = ceil($rs_list->Fields("price") / $int_conversionrate);
                                                $int_extended_total = 0;
                                                $int_extended_total = ($int_price + $int_tailoring_price) * $rs_list->Fields("quantity"); ?>
                                                <?php print $str_currency_symbol . " " . number_format($int_extended_total, 2); ?>
                                            </td>
                                        </tr>
                                    <?php
                                        $int_wholesaler_discount = $rs_list->Fields("wholesalerdiscount");
                                        $int_subtotal = $int_subtotal + $int_extended_total;
                                        $rs_list->MoveNext();
                                    } ?>
                                    <tr>
                                        <td colspan="3" align="right"><b>Item Total</b></td>
                                        <td align="right"><b><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format(ceil($int_subtotal), 2); ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right"><b>Shipping Total</b></td>
                                        <td align="right"><b><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format(ceil($int_total_shipping_price), 2); ?></b></td>
                                    </tr>
                                    <?php
                                    $int_discount_amount = 0;
                                    if ($int_wholesaler_discount > 0 && $str_allow_wholesaler_discount == "YES") {
                                        $int_discount_amount = ($int_subtotal * $int_wholesaler_discount) / 100;
                                        //print $int_discount_amount;
                                        $int_discount_amount = $int_discount_amount;
                                    ?>
                                        <tr>
                                            <td colspan="3" align="right"><b> - Wholesaler Discount</b></td>
                                            <td align="right"><b><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_discount_amount, 2); ?></b></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td colspan="3" align="right"><b class="text-success"> = Total </b></td>
                                        <td align="right"><b class="text-success"><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_subtotal + $int_total_shipping_price - $int_discount_amount, 2); ?></b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                    <button id="btn_continue" class="btn btn-primary btn-lg btn-block"><b>SAVE & CONTINUE&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i><i class="fa fa-chevron-right" aria-hidden="true"></i></b></button>
                </div>
            </form>
        </div>



    </div>
    <div class="footer-bottom-margin">
        <?php include $STR_USER_FOOTER_PATH2;
        CloseConnection(); ?>
    </div>
    <div class="container-fluid">
        <div class="row padding-10">
            <div class="price-and-checkout-btn-panel hidden-lg hidden-md">
                <div class="col-xs-12 text-center nopadding">
                    <a onclick="$('#btn_continue').click();">
                        <div class="buy-now-btn">
                            <?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_subtotal - $int_discount_amount, 2) . " " . $str_currency_shortform;; ?> / SAVE & CONTINUE
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_address.js"></script>

    <script>
        function get_state(countrypkid) { // Call to ajax function

            var state = "";
            // check countrypkid is same then sent state id of that related country to product_address_get_state_p.php
            // also sent country id
            if (countrypkid == <?php echo $int_user_countrypkid; ?>) {
                state = <?php echo $int_user_statepkid; ?>;
            } else {
                state = <?php echo $int_ofc_statepkid; ?>;
            }

            $.ajax({
                type: "POST",
                url: "./product_address_get_state_p.php", // Name of the php files
                data: {
                    countrypkid: countrypkid,
                    state: state
                },
                success: function(html) {
                    $("#before_get_state").hide();
                    $("#after_get_state").html(html);

                    //$("#after_get_qty").hide();
                    //$("#before_get_qty").html(html);
                }
            });
        }
    </script>
</body>

</html>