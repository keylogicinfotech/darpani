<?php 
$STR_TITLE_PAGE = "Online Store";
$STR_TITLE_PAGE_DASHBOARD = "My Account";
$STR_TITLE_PAGE_PERSONAL_DETAIL = "Manage Personal Details";
$STR_TITLE_PAGE_CHANGE_PASSWORD = "Change Password";
$STR_TITLE_PAGE_ORDER = "Order List";
$STR_TITLE_PAGE_FAVORITE = "Favorite List";

//$UPLOAD_IMG_PATH = "./mdm/user/";
//$INT_IMG_WIDTH_LARGE = 900;	
//$INT_IMG_WIDTH_THUMB = 300; 

//$INT_RECORD_PER_PAGE = 2;
//$INT_DAYS_DISPLAY_AS_NEW = 3;

# For Favorite List
global $STR_DB_TR_TABLE_NAME_FAVORITES;
$STR_DB_TR_TABLE_NAME_FAVORITES = "tr_user_favorite_item";

# For Main List
//global $STR_DB_TABLE_NAME;
//global $STR_DB_TABLE_NAME_ORDER_BY;
//$STR_DB_TABLE_NAME = "t_store"; 
//$STR_DB_TABLE_NAME_ORDER_BY = "";

$STR_HOVER_MANAGE_PERSONAL_DETAIL = "Click To Manage Personal Details";
$STR_HOVER_CHANGE_PASSWORD = "Click To Change Password";
$STR_HOVER_ORDER = "Click To View Order List";
$STR_HOVER_FAVORITE_LIST = "Click To View Favorite List";
$STR_HOVER_FAVORITE = "Click To Favorite This Item";

$STR_MSG_ACTION_FAVORITED = "Item is added to favorite list.<br/><a href='".$STR_SITENAME_WITH_PROTOCOL."/user/user_favorite.php?&#ptop' title='".$STR_HOVER_FAVORITE."'>Go To Favorite List</a>";
$STR_MSG_ACTION_UNFAVORITED = "Item is removed from favorite list";
?>