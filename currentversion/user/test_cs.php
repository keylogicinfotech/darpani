<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
//print "HELLO"; exit;
#----------------------------------------------------------------------
#Include files
//session_start();
include "../user/product_validatesession.php";
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_xml.php";
include "../includes/lib_file_upload.php";
include "../includes/lib_image.php";
include "../includes/lib_datetimeyear.php";
include "../user/product_config.php";
//include "./../includes/http_to_https.php";

#----------------------------------------------------------------------
$str_title_page = "";
$str_title_page = $STR_TITLE_PAGE_ADDRESS;
//print $str_title_page; exit;


$str_xml_file_name_cms = "";
$str_xml_file_name_cms = "store_cms.xml";

#------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------

# Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
	case("E"): $str_type = "E"; break;
	case("W"): $str_type = "W"; break;
    }
}
#	Get message text.

if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;        
    }
} 


$item_sessionid="";
$item_uniqueid="";

if(isset($_SESSION['sessionid']))
{
        $item_sessionid=$_SESSION['sessionid'];
}
if(isset($_SESSION['uniqueid']))
{
        $item_uniqueid=$_SESSION['uniqueid'];
}

if($item_sessionid=="" || $item_uniqueid=="")
{
    CloseConnection();
    Redirect($STR_SITENAME_WITH_PROTOCOL);
    exit();
}

$str_member_flag = false;
$int_userpkid = 0;
$str_username = "";
if(isset($_SESSION['userpkid']) && $_SESSION['userpkid'] != "")
{
    $str_member_flag = true;
    $int_userpkid = $_SESSION['userpkid'];
    $str_username = $_SESSION['username'];
}

$str_allow_promocode = "NO";
$str_allow_wholesaler_discount = "NO";
$str_username = "";
$str_user_address = "";
$str_user_email = "";
$str_user_phone = "";
$int_user_countrypkid = "";
$int_user_statepkid = "";
$str_user_city = "";
$str_user_zipcode = "";

if($int_userpkid > 0)
{
    $str_query_select = "";
    $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_USER." WHERE pkid=".$int_userpkid;
    $rs_list_user = GetRecordSet($str_query_select);
    if(!$rs_list_user->EOF())
    {
        $str_username = $rs_list_user->Fields("name");
        $str_user_address = $rs_list_user->Fields("address");
        $str_user_email = $rs_list_user->Fields("emailid");
        $str_user_phone = $rs_list_user->Fields("phoneno");
        $int_user_countrypkid = $rs_list_user->Fields("countrypkid");
        $int_user_statepkid = $rs_list_user->Fields("statepkid");;
        $str_user_city = $rs_list_user->Fields("city");
        $str_user_zipcode = $rs_list_user->Fields("zipcode");
        
        
        if($rs_list_user->Fields("isusertype") == "REGULAR")
        {
            $str_allow_promocode = "YES";
            $str_allow_wholesaler_discount = "NO";
        }
        
        if($rs_list_user->Fields("isusertype") == "WHOLESALER")
        {
            $str_allow_promocode = "NO";
            $str_allow_wholesaler_discount = "YES";
        }
        
    }
}
//print $str_allow_promocode;



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($str_title_page) ;?></title>
    <?php //print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />   
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
</head>
<body>
<?php //include $STR_USER_HEADER_PATH2; ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>           
            </div>
        </div>
        
        
        <div class="row">
            <form name="frm_shipping_address" method="POST" action="./product_address_p.php">
            <div class="col-md-7">
                <h4 class="nopadding"><b>Name & Address</b></h4><hr/>
                    
                    <div class="row padding-10">
                        <div class="col-md-6">
                            <div class="control-group form-group">
                                <div class="controls">
                                <label>Select Country</label><span class="text-help-form"> * </span>
                                    <?php 
                                    $str_query_select="";
                                    $str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_COUNTRY. " WHERE visible='YES' ORDER BY title ASC";
                                    //print $str_query_select; exit;
                                    $rs_list_country=GetRecordSet($str_query_select);?>
                                    <select name="cbo_country" id="cbo_country" class="form-control" onChange="get_state(this.value);" >
                                        <option  <?php // print(CheckSelected("",$rs_list->fields("statepkid")));?> value="0">-- Select Country --</option>
                                            <?php
                                            while(!$rs_list_country->EOF()==true)
                                            {?>
                                                <option value="<?php print($rs_list_country->fields("pkid"))?>" <?php print(CheckSelected($rs_list_country->fields("pkid"),$int_user_countrypkid)); ?>><?php print($rs_list_country->fields("title")); ?></option>
                                                <?php
                                                $rs_list_country->MoveNext();
                                            }
                                        ?>				
                                    </select>
                            </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="control-group form-group">
                                <div class="controls">
                                    <label>Select State</label><span class="text-help-form"> * </span>
                                    <div id="before_get_state">
                                                <option value="0">-- Select State --</option>
                                            </select>
                                    </div>
                                    <div id="after_get_state"></div>    
                                </div>
                            </div>                       
                    </div>
                        
                    </div> 
                    
                    <div id="success"></div>
                <br/>
            </div>
            <div class="col-md-5">
                
                <button id="btn_continue" class="btn btn-primary btn-lg btn-block"><b>SAVE & CONTINUE&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i><i class="fa fa-chevron-right" aria-hidden="true"></i></b></button>
            </div>
            </form>
        </div>
        
        
        
    </div>
   
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    
    <script>
    function get_state(countrypkid) { // Call to ajax function
    var int_countrypkid = countrypkid;

        var dataString = "countrypkid="+int_countrypkid;
    	//alert(dataString);
        $.ajax({
            type: "POST",
            url: "./product_address_get_state_p.php", // Name of the php files
            data: dataString,
            success: function(html)
            {
                $("#before_get_state").hide();
                $("#after_get_state").html(html);
            }
        });
    }
    </script>
</body>
</html>
