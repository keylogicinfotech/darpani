<?php

session_start();
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_email.php";
include "../includes/lib_xml.php";
include "../includes/lib_file_upload.php";
include "../includes/lib_image.php";
#--------------------------------------------------------------------------------------------------------
//print_r($_POST);exit;
$str_db_table_name = "tr_blog";
#----------------------------------------------------------------------------------------------------
$int_blogpkid=0;	
if(isset($_POST['hdn_blogpkid']))
{
    $int_blogpkid = trim($_POST['hdn_blogpkid']);
}	
if($int_blogpkid == 0 || $int_blogpkid == "" || !is_numeric($int_blogpkid))
{
    $response['status']='ERR';
    $response['message']= $STR_MSG_ACTION_INFO_MISSING;
    echo json_encode($response); 
    return;
}

$str_name = "";	 
if(isset($_POST['txt_name']))
{
    $str_name = trim($_POST['txt_name']);
}
if(empty($str_name))
{
    $response['status']='ERR';
    $response['message']= "Please enter name!";
    echo json_encode($response); 
    return;
}

$str_location = "";	
if(isset($_POST['txt_location']))
{
    $str_location = trim($_POST['txt_location']);
}

$str_emailid = "";	 
if(isset($_POST['txt_emailid']))
{
    $str_emailid = trim($_POST['txt_emailid']);
}
if($str_emailid != "")
{
    if(validateEmail($str_emailid) == false)
    {
        $response['status']='ERR';
        $response['message']= $STR_MSG_ACTION_INVALID_EMAIL_FORMAT;
        echo json_encode($response); 
        return;
    }
}
/*$int_modelpkid=0;	
if(isset($_POST['hdn_modelpkid']))
{
    $int_modelpkid = trim($_POST['hdn_modelpkid']);
}	
if($int_modelpkid == 0 || $int_modelpkid == "" || !is_numeric($int_modelpkid))
{
    $response['status']='ERR';
    $response['message']= "Some Information Missing!";
    echo json_encode($response); 
    return;
}

$int_memberpkid=0;	
if(isset($_POST['hdn_memberpkid']))
{
    $int_memberpkid = trim($_POST['hdn_memberpkid']);
}
if($int_memberpkid=="") { $int_memberpkid = 0; }

if($int_memberpkid != $_SESSION["freememberpkid"] && false && $int_modelpkid != $_SESSION["modelpkid"]) {

    if($int_memberpkid == 0 || $int_memberpkid == "" || !is_numeric($int_memberpkid))
    {
        $response['status']='ERR';
        $response['message']= "Please login first to comment!";
        echo json_encode($response); 
        return;
    }
}*/

$str_desc= "";
if(isset($_POST['ta_desc'])){
    $str_desc= trim($_POST['ta_desc']);
}

/*$str_sel_member = ""; 
$str_sel_member = "SELECT * FROM t_freemember WHERE pkid=".$int_memberpkid; 
$rs_mem_details = GetRecordSet($str_sel_member);

$str_shortenurlkey = "";
$str_shortenurlkey = $rs_mem_details->Fields("shortenurlkey");*/

if(empty($str_desc))
{
    $response['status']='ERR';
    $response['message']= "Please enter comment!";
    echo json_encode($response); 
    return;
}

$str_secretcode = "";
if (isset($_POST["txt_secretcode"])) { $str_secretcode = trim($_POST["txt_secretcode"]); }

if(empty($str_secretcode) || $_SESSION['image_secret_code'] != $str_secretcode)
{
    $response['status']='ERR';
    $response['message']= $STR_MSG_ACTION_INVALID_SECRET_CODE;
    echo json_encode($response);
    return;
}

$str_blogtype = "";
$str_blogtype = "C";

$str_approved = "";
$str_approved = "YES";

$str_visible = "";
$str_visible = "YES";

#upload image file.
//$UPLOAD_PHOTO_PATH="../mdm/contact/";
//$str_dir=$UPLOAD_PHOTO_PATH;
#----------------------------------------------------------------------------------------------------
#upload image file.
/*$str_large_file_name="";
if($str_files != "")
{
	$str_dir="";
	$str_dir=$UPLOAD_PHOTO_PATH;
	//print $str_dir; exit;
	if(!file_exists($str_dir))
	{
		$response['status']='ERR';
		$response['message']= "Invalid file path!";
		echo json_encode($response);
		return;
	}
	#upload image file.	
	$str_large_path="";
	
	$str_large_file_name=$str_files;
	$str_large_path = trim($str_dir . $str_large_file_name);
	//print $str_large_path; exit;
	UploadFile($_FILES['uploadimages']['tmp_name'],$str_large_path);
	//ResizeImage($str_large_path,$INT_BIO_WIDTH);
}*/
//exit;
//$str_fax = $_POST['txt_phone'];
$str_ip_address = $_SERVER['REMOTE_ADDR'];
$str_datetime = date('Y-m-d H:i:s');
#----------------------------------------------------------------------------------------------------
$str_query_insert = "";
$str_query_insert = "INSERT INTO ".$str_db_table_name."(blogpkid, personname, emailid, location, blogtype, description, visible, approved, submitdatetime, ipaddress)";
$str_query_insert.=" VALUES(".$int_blogpkid.", '".ReplaceQuote($str_name)."', '".ReplaceQuote($str_emailid)."', '".ReplaceQuote($str_location)."', '".ReplaceQuote($str_blogtype)."','".ReplaceQuote($str_desc)."','".ReplaceQuote($str_visible)."', '".ReplaceQuote($str_approved)."', '".ReplaceQuote($str_datetime)."',";
$str_query_insert.="'".$str_ip_address."')";	
//print($str_query_insert); exit;
ExecuteQuery($str_query_insert);	
#----------------------------------------------------------------------------------------------------
/*$fp=openXMLfile($UPLOAD_XML_CONTENT_PATH."siteconfiguration.xml");
$str_from = getTagValue($STR_FROM_DEFAULT,$fp);
$str_to = getTagValue($STR_FROM_DEFAULT,$fp);
closeXMLfile($fp);

$str_query_select = "SELECT shortenurlkey, emailid, getnotification FROM t_model WHERE modelpkid=".$int_modelpkid."";
$rs_list = GetRecordSet($str_query_select);
$str_to_model = $rs_list->fields("emailid");

$str_query_select = "SELECT shortenurlkey FROM t_freemember WHERE pkid=".$int_memberpkid."";
$rs_list2 = GetRecordSet($str_query_select);

$str_query_select = "SELECT title FROM tr_blog WHERE blogpkid=".$int_blogpkid."";
$rs_list3 = GetRecordSet($str_query_select);

if(!$rs_list->eof() && !$rs_list2->eof() && !$rs_list3->eof())
{
	$str_subject="New comment is posted on your blog on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
	$mailbody="New comment is posted on your blow. Below are details.<br/><br/>";
	$mailbody.="<strong>Blog Posted By:</strong> ".$rs_list->fields("shortenurlkey")."<br/>";
	$mailbody.="<strong>Blog Name:</strong> ".$rs_list3->fields("title")."<br/>";
	$mailbody.="<strong>Comment By:</strong> ".$rs_list2->fields("shortenurlkey")."<br/>";
	$mailbody.="<strong>Comment:</strong> ".ReplaceQuote($str_desc)."<br/>";
	$mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL."";
	//print "<br/>".$str_from; print "<br/>".$str_to; print "<br/>".$mailbody; exit;
	sendmail($str_to,$str_subject,$mailbody,$str_from,1);  
	if($rs_list->fields("getnotification")=='YES')
	{	
		sendmail($str_to_model,$str_subject,$mailbody,$str_from,1);  
	}
}

// Send mail to all members of particular blog when model comment on her blog
if($int_memberpkid=="" || $int_memberpkid = 0)
{
	$str_query_select = "SELECT DISTINCT(b.memberpkid), m.emailid, m.getnotification ";
	$str_query_select .= " FROM tr_blog b LEFT JOIN t_freemember m ";				
	$str_query_select .= " ON b.memberpkid = m.pkid ";
        $str_query_select .= " WHERE b.approved='YES' AND blogpkid=".$int_blogpkid."";
        $rs_member_list = GetRecordSet($str_query_select);

	while(!$rs_member_list->EOF()) 
	{ 
		$str_subject="New comment is posted on blog on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
		$mailbody="New comment is posted on blow. Below are details.<br/><br/>";
		$mailbody.="<strong>Blog Posted By:</strong> ".$rs_list->fields("shortenurlkey")."<br/>";
		$mailbody.="<strong>Blog Name:</strong> ".$rs_list3->fields("title")."<br/>";
		$mailbody.="<strong>Comment By:</strong> ".$rs_list->fields("shortenurlkey")."<br/>";
		$mailbody.="<strong>Comment:</strong> ".ReplaceQuote($str_desc)."<br/>";
		$mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL."";
		//print "<br/>".$str_from; print "<br/>".$str_to; print "<br/>".$mailbody; exit;
		sendmail($str_to,$str_subject,$mailbody,$str_from,1);  
		if($rs_member_list->fields("getnotification")=='YES')
		{	
			sendmail($rs_member_list->fields("emailid"),$str_subject,$mailbody,$str_from,1);  
		}

		$rs_member_list->MoveNext(); 
	}
}*/
#----------------------------------------------------------------------------------------------------
//$_SESSION['image_secret_code']="";
$response['status']='SUC';
$response['message']="Your comment has been submitted successfully.";
echo json_encode($response);
return;
?>
