<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#-------------------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/http_to_https.php";	
include "./../includes/lib_image.php";
include "./../includes/lib_xml.php";

#-------------------------------------------------------------------------------
$str_page_title = "Landing Page";
$str_img_path = "./mdm/landing/";
$str_xml_file_name = "about.xml";
$str_xml_file_name_cms = "landingpage_cms.xml";

$str_title_page_metatag = "PG_DEFAULT";
$str_db_table_name_metatag = "t_page_metatag";

$str_button_view_details = "Enter";
$str_button_view_details_exit = "Exit";
//$str_db_table_name = "t_about"; 

$str_desc_about = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_MODULE.$str_xml_file_name);
$str_desc_about = getTagValue("DESCRIPTION", $fp);
$str_title_about = getTagValue("TITLE", $fp);
$str_image_about = getTagValue("IMAGENAME", $fp);

#getting datas from module xmlfile.
$arr_xml_list="";
$arr_xml_list=readXml($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");


#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_img_cms = getTagValue("ITEMKEYVALUE_PHOTO", $fp);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
//print_r($str_desc_cms);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);

#----------------------------------------------------------------------
# Select Query to get blog details
/*$str_query_select = "";
$str_query_select = "SELECT * FROM ".$str_db_table_name." WHERE visible = 'YES' ORDER BY title ASC";
//print $str_query_select;exit;
$rs_list_about = GetRecordSet($str_query_select);

$str_about_title = "";
$str_about_title = $rs_list_about->Fields("title"); */
//print $str_about_title;exit;
#----------------------------------------------------------------------
# Get metatag page title
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatag. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' "; 
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = "";
$str_title_page = $rs_list_mt->fields("titletag"); 
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($str_title_page) ;?></title>
    <?php print(Display_Page_Metatag("PG_DEFAULT")); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>

<body>
    <?php // include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <?php if($str_visible_cms == "YES" && $str_img_cms != ""){ ?>
                <div class="col-lg-12 col-md-12" align="center"><br/>
                    <img src="<?php print $str_img_path.$str_img_cms; ?>" class="img-responsive img-thumbnail " alt="" title=""/><br/>
                </div>
            </div>
            <?php } ?><br/>
            
            <div class="row padding-10">
            <div class="col-lg-12 col-md-12" align="center">
                <img src="./images/logo.png" class="img-responsive" alt="" title=""/>
                </div>
            </div>
            <br/>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12" align="center">
                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/home" class="btn btn-success btn-lg" title="<?php ?>"><i class="fa fa-check-circle" style="font-size: 30px;"></i>&nbsp;<b style="font-size: 30px;"><?php print $str_button_view_details; ?></b></a>


                <a href="<?php print "https://www.google.com"; ?>" class="btn btn-danger btn-lg" title="<?php ?>"><i class="fa fa-times-circle" style="font-size: 30px;"></i>&nbsp;<b style="font-size: 30px;"><?php print $str_button_view_details_exit; ?></b></a>
                </div>
            </div>
            <br/> 
                    
         <?php if($str_visible_cms == "YES"){ ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12">
                    <h4><b class="text-danger"><?php // print $str_title_cms; ?></b></h4>
                    <?php print $str_desc_cms; ?>
                </div>
            </div>
           <?php } ?>
        
    </div>
    <script type="text/javascript" src="../js/jquery.min.js" ></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
    <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
    </script>
<div class="scrollup" style="display: block;"></div>
<script type="text/javascript">
            $(document).ready(function(){ 

            $(window).scroll(function(){
                if ($(this).scrollTop() > 100) {
                    $('.scrollup').fadeIn();
                } else {
                    $('.scrollup').fadeOut();
                }
            }); 
            $('.scrollup').click(function(){
                $("html, body").animate({ scrollTop: 0 }, 600);
                return false;
            });
        });
    </script>
     <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php // include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
</body>

<script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
</script>
<div class="scrollup" style="display: block;"></div>
<script type="text/javascript">
            $(document).ready(function(){ 

            $(window).scroll(function(){
                if ($(this).scrollTop() > 100) {
                    $('.scrollup').fadeIn();
                } else {
                    $('.scrollup').fadeOut();
                }
            }); 
            $('.scrollup').click(function(){
                $("html, body").animate({ scrollTop: 0 }, 600);
                return false;
            });
        });
    </script>
    
</html>
