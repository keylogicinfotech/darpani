<?php 
/*
File Name  :- user_home.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#---------------------------------------------------------------------
#Include files
session_start();
include "./user_validate_session.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
//include "./../includes/http_to_https.php";
include "./user_config.php";
//print_r($_SESSION);exit;
#----------------------------------------------------------------------
//$str_title_page = "";
$str_title_page = $STR_TITLE_MODEL_PAST_ANNOUNCEMENT;
$str_db_table_name = " t_user";
$str_db_table_name_news = "t_news";
$str_db_table_name_news_order = "ORDER BY displayorder DESC, date DESC,heading";

$terms_usage = "";
$fp=OpenXMLFile($STR_UPLOAD_CONTENT_MANANGEMENT_PATH."model_login.xml");
$terms_usage = getTagValue("MODEL_USAGETERMS",$fp);
CloseXmlFile($fp);
//--------------------------------------------------------------------------------------------------------
$str_query_select = "";
$str_query_select = "SELECT approved, isusertype FROM " .$str_db_table_name. " WHERE pkid=".$_SESSION["userpkid"]."";
$rs_list = GetRecordset($str_query_select);
//--------------------------------------------------------------------------------------------------------?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print $STR_TITLE_MODEL; ?> - <?php print $str_title_page; ?></title>
    <title><?php // print($STR_SITE_TITLE);?> : <?php //print($rs_list_mt->fields("titletag")) ;?></title>
    <?php //print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />   
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> 
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <a name="ptop" id="ptop"></a>
            <div class="col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1>           
            </div>
        </div><hr/>
        <div class="row padding-10">
            <div class="col-md-3 col-xs-12 col-sm-12">
                <?php include($STR_USER_PANEL_PATH); ?>
            </div>
            <div class="col-md-9 col-xs-12 col-sm-12">
               <?php $str_user_type="";
                if($rs_list->fields("isusertype") == "REGULAR")
                {
                    $str_user_type = "REGULAR";
                }
                else {
                    $str_user_type = "WHOLESALER";
                }
               
               
            # select query for get news details
            $str_query_select = "";
            $str_query_select = "SELECT * FROM " .$str_db_table_name_news. " WHERE typefornews='".$str_user_type."' and visible='YES' and archive='YES' " .$str_db_table_name_news_order. " ";
            //print $str_query_select;
            $rs_list_archived_news = GetRecordSet($str_query_select);?>
            <?php  if(!$rs_list_archived_news->eof()) { ?>
            <div class="row padding-10">  
                <div class="col-md-1 text-center"></div> 
                <div class="col-md-5 text-center"> 
                    <a href="./user_home.php#ptop" title="Click to view latest announcements" type="button" class="btn btn-block btn-default"><i class="fa fa-bullhorn" aria-hidden="true"></i>&nbsp;<b>View Latest Announcements</b></a>
                </div>
                <div class="col-md-5 text-center"> 
                    <a  title="Site features & past announcements" type="button" class="btn btn-block btn-warning disabled"><i class="fa fa-bullhorn" aria-hidden="true"></i>&nbsp;<b>Other Site Features & Past Announcements</b></a>
                </div>
                <div class="col-md-1 text-center"></div> 
            </div>
            <br/>
            <?php 
            $int_cnt=1;
            while(!$rs_list_archived_news->eof())
            { ?>
                <div class="row padding-10">
                    <div class="col-md-12">
                    <?php if($rs_list_archived_news->fields("imagefilename")!="") { ?>
                    <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list_archived_news->fields("pkid"));?>" rel="thumbnail"><img class="img-responsive" src="<?php print($STR_UPLOAD_NEWS_PATH.$rs_list_archived_news->fields("imagefilename"));?>"  border="0" align="absmiddle" alt="<?php print $rs_list_archived_news->fields("imagefilename")?>" title="<?php print $rs_list_archived_news->fields("imagefilename")?>"></a>
                    <div class="modal fade f-pop-up-<?php print($rs_list_archived_news->fields("pkid")); ?> " align="center" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                    <img src="<?php print($STR_UPLOAD_NEWS_PATH.$rs_list_archived_news->fields("imagefilenamelarge"));?>" class="img-responsive img-rounded" title="<?php print $str_title_page; ?> image" alt="<?php print $str_title_page; ?> image">
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div>															
                
              <?php } ?>
                    
                <?php /* ?>    <?php if($rs_list_archived_news->fields("imagefilename")!="") { ?>
                        <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list_archived_news->fields("pkid"));?>" rel="thumbnail">
                            <img class="img-responsive img-thumbnail" src="<?php print($STR_UPLOAD_NEWS_PATH.$rs_list_archived_news->fields("imagefilename"));?>"  border="0" align="absmiddle" alt="<?php print $rs_list_archived_news->fields("imagefilename")?>" title="<?php print $rs_list_archived_news->fields("imagefilename")?>">
                        </a>
                        <div class="modal fade f-pop-up-<?php print($rs_list_archived_news->fields("pkid")); ?> " align="center" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                        <img src="<?php print($STR_UPLOAD_NEWS_PATH.$rs_list_archived_news->fields("imagefilenamelarge"));?>" class="img-responsive img-rounded" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image">
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div>															
                    </div>
                    <?php } ?>    <?php */ ?>
                        <h4 class=""><b><?php print $rs_list_archived_news->Fields("heading"); ?></b></h4>
                        <p align="justify"><?php print $rs_list_archived_news->Fields("description"); ?></p><hr/>                             
                </div>
                </div>    
                <?php 
                $int_cnt=$int_cnt+1;
                $rs_list_archived_news->MoveNext();
            } //while end
            ?>
            
            <?php } ?>
        </div>
    </div>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_personal_details.js"></script>
</body>
</html>
