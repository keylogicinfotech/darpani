<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
include "./../user/store_config.php";
#----------------------------------------------------------------------
$str_xml_file_name_cms = "store_cms.xml";
$str_image_path = ".".$UPLOAD_IMG_PATH;
#----------------------------------------------------------------------        
#Get values of all passed GET / POST variables
$str_subscriptionid = "";
if(isset($_GET["txt_subscrid"]) && trim($_GET["txt_subscrid"]) != "" )
{   
    $str_subscriptionid = trim($_GET["txt_subscrid"]);    
}
//print_r($_GET);
$str_stype = "";
$str_smessage = "";
if(isset($_GET["btn_submit"]))
{
    if(isset($_GET["txt_subscrid"]))
    {		
        $str_subscriptionid = trim($_GET["txt_subscrid"]);
    }
    if($str_subscriptionid == "")
    {
        $str_stype = "E";$str_smessage = "Please enter ccbill order number or emailid.";
    } 
}
if($str_subscriptionid != "")
{ 
    $str_query_select = "";
    $str_query_select = "SELECT subscriptionid FROM ".$STR_DB_TABLE_NAME_PURCHASE." WHERE subscriptionid='".$str_subscriptionid."' OR emailid='".$str_subscriptionid."'";
    $rs_subid_list=GetRecordset($str_query_select);
    if($rs_subid_list->eof()){$str_stype = "E";$str_smessage = "Order number doesn't exist. Please check your ccbill mail and enter correct one.";}
                  }
//print_r($_GET);

$str_where = "";
//if($str_subscriptionid != "")
//{
    $str_where = " AND a.subscriptionid='".$str_subscriptionid."' OR emailid = '".$str_subscriptionid."'";
//}
//print $str_subscriptionid; exit;
#----------------------------------------------------------------------
# Select Query to get list
$str_query_select = "";
$str_query_select = "SELECT a.* FROM " .$STR_DB_TABLE_NAME_PURCHASE. " a ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME." b ON a.productpkid=b.pkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_PHOTO." c ON a.productpkid=c.masterpkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." d ON a.subcatpkid=d.subcatpkid WHERE d.visible='YES' AND c.setasfront='YES' AND c.visible='YES' ".$str_where." ";
$str_query_select .= "ORDER BY a.purchasedatetime";
//print $str_query_select; //exit;
$rs_list = GetRecordSet($str_query_select);
//print $rs_list->Count();
#----------------------------------------------------------------------
#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION4", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE4", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
//print $str_subscriptionid;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE_ORDER_STATUS) ;?></title>
    <?php //print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $STR_TITLE_PAGE_ORDER_STATUS; ?></h1><hr/>
            </div>
        </div>
        <?php if($str_visible_cms2 == "YES") { ?>
            <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        <br/>
        <?php
        //print $str_subscriptionid;
        if($str_subscriptionid == "" || $str_stype == "E"){ ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <form id="frm_add" name="frm_add" action="" method="get" onsubmit="return frm_add_validate();">
                    <label>Enter Subscription ID or Email ID</label>
                    <span class="input-group">
                        <input type="text" id="txt_subscrid" name="txt_subscrid" class="form-control" placeholder="Enter Subscription ID or Email ID">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit" id="btn_submit" name="btn_submit"><b>Enter</b></button>
                        </span>
                    </span>
                </form>
            </div>
        </div><br/>        
        <?php } ?>
        <?php 
            if($rs_list->count() > 0) {
                //print $rs_list->count();
            $int_cnt = 0;    
                while(!$rs_list->EOF())
                {?>
                <div class="row padding-10">
                    <div class="col-lg-4 col-md-4" align="center">
                            <?php 
                            $sel_qry_photo = "";
                            $sel_qry_photo = "SELECT largephotofilename FROM tr_store_photo WHERE masterpkid = ".$rs_list->Fields("productpkid")." AND setasfront = 'YES'";
                            $rs_photo_details = GetRecordSet($sel_qry_photo);  ?>
                        
                        <?php if($rs_photo_details->Fields("largephotofilename") != ""){   ?>
                            <img class="img-responsiv img-thumbnail" src="<?php print $str_image_path.$rs_list->Fields("productpkid")."/".$rs_photo_details->Fields("largephotofilename");  ?>" alt="<?php print $rs_list->Fields("producttitle"); ?>" title="<?php print $rs_list->Fields("producttitle"); ?>" width="100%"><br/><br/>
                        <?php }  ?>
                        
                    </div>
                    <div class="col-lg-8 col-md-8">
                            <h3 class=""><b class=""><?php print $rs_list->Fields("producttitle"); ?></b> / <?php print $rs_list->Fields("subcattitle"); ?></h3>
                        <?php /* ?><label><h5 class="nopadding"><b class="text-primary"><?php print $rs_list->Fields("subcattitle"); ?></b></h5></label><?php */ ?>
                            <label><h5 class="nopadding">Price :</h5></label>&nbsp;<label><h4 class="nopadding text-default"><b><?php print "US$ ".$rs_list->Fields("extendedprice"); ?></b></h4></label><br/>
                        
                        <label><h5 class="nopadding">Quantity :</h5></label>&nbsp;<label><h4 class="nopadding text-default"><b><?php print $rs_list->Fields("quantity"); ?></b></h4></label><br/>
                        <?php 
                        /*$sel_qry_mem_details= "";
                        $sel_qry_mem_details = "SELECT shortenurlkey FROM t_freemember WHERE pkid = ".$rs_list->Fields("memberpkid");
                        $rs_mem_details = GetRecordSet($sel_qry_mem_details); ?>
                        <p >Purchased By: <b><a href="<?php print $SITENAME_WITH_PROTOCOL."/".$rs_mem_details->Fields("shortenurlkey"); ?>" title="Click to view Profile"><?php print $rs_mem_details->Fields("shortenurlkey"); ?></a></b></p>
                        <p><?php */ ?>
                        <?php if($rs_list->fields("color") != "") { ?>
                            <label><h5 class="nopadding">Color :</h5></label>&nbsp;<label><h4 class="nopadding text-default"><b><?php print($rs_list->fields("color")); ?></b></h4></label><br/>
                        <?php } ?>
                        <?php if($rs_list->fields("size") != "") { ?>
                            <label><h5 class="nopadding">Size :</h5></label>&nbsp;<label><h4 class="nopadding text-default"><b><?php print($rs_list->fields("size")); ?></b></h4></label><br/>
                        <?php } ?>
                        <?php if($rs_list->fields("type") != "") { ?>
                            <label><h5 class="nopadding">Type :</h5></label>&nbsp;<label><h4 class="nopadding text-default"><b><?php print($rs_list->fields("type")); ?></b></h4></label><br/>
                        <?php } ?>
                        <?php if($rs_list->fields("shippingaddress") != "") { ?>
                            <label><h5 class="nopadding">Shipping Address :</h5></label>&nbsp;<span class="text-default"><?php print($rs_list->fields("shippingaddress")); ?></span>
                            <?php } ?>   
                            <?php 
                            $str_status_icon = "";
                            if($rs_list->fields("shippingstatus") == "PROCESSING") {
                                $str_status_icon = "<i class='fa fa-check-circle-o'></i>";
                            } else if($rs_list->fields("shippingstatus") == "SHIPPED") {
                                $str_status_icon = "<i class='fa fa-truck'></i>";
                            } else if($rs_list->fields("shippingstatus") == "DELIVERED") {
                                $str_status_icon = "<i class='fa fa-check-circle-o'></i>";
                            }  ?>
                            
                            <h3 class="btn btn-info btn-xs disabled"><?php print $str_status_icon; ?>&nbsp;<b><?php print $rs_list->Fields("shippingstatus"); ?></b></h3>
                        </p>                        
                    </div>
                </div>
                <hr />
                <?php
                    $int_cnt++;
                    $rs_list->MoveNext();
                }
            } /*else {?>
                <div class="row padding-10">
                    <div class="col-md-12 alert alert-danger" align="center">
                        <?php print $STR_MSG_NO_DATA_AVAILABLE; ?>
                    </div>
                </div>
            <?php }*/ ?>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script>
        function frm_add_validate(){
            with(document.frm_add){
                if(txt_subscriptionid.value == ""){alert("Please enter ccbill order number or emailid.");txt_subscriptionid.focus(); return false;  }  return true;
            }
        }    
    </script>
</body>
</html>
