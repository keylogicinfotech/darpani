<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_EVENT";
$str_img_path = $STR_SITENAME_WITH_PROTOCOL."/mdm/event/";
$str_db_table_name_mt = "t_page_metatag";
$str_db_table_name = "t_event";
$str_xml_file_name = "";
$str_xml_file_name_cat = "";
$str_xml_file_name_cms = "event_cms.xml";
$int_records_per_page = 1;
#----------------------------------------------------------------------
$str_m = "";
if(isset($_POST["cbo_month"]) && trim($_POST["cbo_month"])!="")
{ 
    $str_m = trim($_POST["cbo_month"]); 
}
if($str_m=="" || $str_m<=0 || is_numeric($str_m)==false)
{ 
    $str_m = date("m");
}

$str_y = "";
if(isset($_POST["cbo_year"]) && trim($_POST["cbo_year"])!="")
{ 
    $str_y=trim($_POST["cbo_year"]); 
}
if($str_y == "" || $str_y<=0 || is_numeric($str_y)==false)
{ 
    $str_y = date("Y"); 
}

#----------------------------------------------------------------------
$str_query_select = "";
$str_query_select = "SELECT * ";
$str_query_select .= "FROM ".$str_db_table_name." ";
$str_query_select .= "WHERE visible='YES' AND approved='YES' AND month = ".$str_m." AND year = ".$str_y." ";
$str_query_select .= "ORDER BY eventdatetime DESC, title ASC";
//print $str_query_select;
$rs_list =  GetRecordSet($str_query_select);

#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_SESSION);
//print $int_total_records; exit;
#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table

$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_mt. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
//print_r($_POST);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
            </div>
        </div>
        <?php if($str_visible_cms == "YES") { ?>
            <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        <div class="row padding-10">
            <div class="col-md-12" align="center">
               <form name="frm_filter" method="POST" action="#">
                   <label class="nopadding">
                    <div class="form-group input-group">
                         <?php print(DisplayMonth($str_m,"-- Select Month --"));?>
                    </div>
                   </label>
                   <label class="nopadding">
                    <div class="form-group input-group">
                        <?php print(DisplayYear($str_y,"-- Select Year --",0));?>
                        
                    </div>
                   </label>
                   <label>
                       <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit"><b>View Events</b></button>
                        </span>
                   </label>
                </form>
            </div>
        </div><hr/>
        <?php 
        $int_cnt = 0;
        //print $rs_list->Count();
        while($rs_list->eof() == false)
        {
        ?>
        <div class="row padding-10">
            <?php if($rs_list->Fields("eventimagefilename") != "") {?>
            <div class="col-lg-4 col-md-4">
                    <?php
                    $str_eventurl_new_window = "_self";
                    if($rs_list->Fields("eventurlinnewwindow") == "YES")
                        $str_eventurl_new_window = "_blank";
                    else {
                        $str_eventurl_new_window = "_self";
                    }
                    
                    ?>
                    <a href="<?php print $rs_list->Fields("eventurl"); ?>" target="<?php print $str_eventurl_new_window; ?>" title="<?php print $rs_list->Fields("title"); ?>"><img src="<?php print $str_img_path.$rs_list->Fields("eventimagefilename"); ?>" class="img-responsive" alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" /></a>
                    
                
            </div>
            <div class="col-lg-8 col-md-8">
               <label><h3 align="left" class=""><?php print $rs_list->Fields("title"); ?></h3></label>&nbsp;<?php if($rs_list->Fields("displayasnew") == "YES") { print "&nbsp;".$STR_ICON_PATH_NEW; } ?>
                
                <p align="justify" class="text-primary">
                    <b>    
                        <?php if($rs_list->Fields("eventdatetime") != "") { ?>
                        <i class="fa fa-calendar"></i>&nbsp;<?php print DDMMMYYYYFormat($rs_list->Fields("eventdatetime")); ?>&nbsp;&nbsp;<?php print $rs_list->fields("starttime"); ?>&nbsp;&nbsp;<?php print $rs_list->fields("timezone"); ?>
                        <?php } ?>
                        <?php if($rs_list->Fields("location") != "") { ?>
                        &nbsp;&nbsp;<i class="fa fa-map-marker"></i>&nbsp;<?php print $rs_list->Fields("location"); ?>
                        <?php } ?>
                    </b>
                </p>
                <?php if($rs_list->Fields("description") != "" && $rs_list->Fields("description") != "<br>") { ?>
                    <p align="justify"><?php print $rs_list->Fields("description"); ?></p>
                <?php } ?>
                    <p>
                <?php if($rs_list->fields("contactname") != "") { ?>
                    <span><i class="fa fa-user"></i>&nbsp;<?php print(MyhtmlEncode($rs_list->fields("contactname"))) ?></span>
                <?php } ?>
                <?php if($rs_list->fields("phone") != "") { ?>
                    &nbsp;&nbsp;<span><i class="fa fa-phone"></i>&nbsp;<?php print(MyhtmlEncode($rs_list->fields("phone"))) ?></span>
                <?php } ?>
                <?php if($rs_list->fields("emailid") != "") { ?>
                    &nbsp;&nbsp;<span><i class="fa fa-envelope"></i>&nbsp;<?php print(MyhtmlEncode($rs_list->fields("emailid"))) ?></span>
                <?php } ?>    
                <?php if($rs_list->fields("companyurl") != "") 
                    { ?>
                    &nbsp;&nbsp;<span><i class="fa fa-globe"></i>&nbsp;<?php  print(DisplayWebSiteURL($rs_list->fields("companyurl"),$rs_list->fields("companyname"),'YES'," ","",$rs_list->fields("companyurl"))); ?></span>
                <?php } else if($rs_list->fields("companyname") != "" && $rs_list->fields("companyurl") == "") { ?>    
                    &nbsp;&nbsp;<span><i class="fa fa-building"></i>&nbsp;<?php print(MyhtmlEncode($rs_list->fields("companyname"))) ?></span>
                <?php } ?>
                   
                </p>
                
                
            </div>
            <?php } else { ?>
            <div class="col-lg-12 col-md-12">
                <label><h3 align="left" class=""><?php print $rs_list->Fields("title"); ?></h3></label>&nbsp;<?php if($rs_list->Fields("displayasnew") == "YES") { print "&nbsp;".$STR_ICON_PATH_NEW; } ?>
                <p align="justify" class="text-primary">
                    <b>
                        <?php if($rs_list->Fields("eventdatetime") != "") { ?>
                        <i class="fa fa-calendar"></i>&nbsp;<?php print DDMMMYYYYFormat($rs_list->Fields("eventdatetime")); ?>&nbsp;&nbsp;<?php print $rs_list->fields("starttime"); ?>&nbsp;&nbsp;<?php print $rs_list->fields("timezone"); ?>
                        <?php } ?>
                        <?php if($rs_list->Fields("location") != "") { ?>
                        &nbsp;&nbsp;<i class="fa fa-map-marker"></i>&nbsp;<?php print $rs_list->Fields("location"); ?>
                        <?php } ?>
                    </b>
                </p>
                <?php if($rs_list->Fields("description") != "" && $rs_list->Fields("description") != "<br>") { ?>
                    <p align="justify"><?php print $rs_list->Fields("description"); ?></p>
                <?php } ?>
                <p>
                <?php if($rs_list->fields("contactname") != "") { ?>
                    <span><i class="fa fa-user"></i>&nbsp;<?php print(MyhtmlEncode($rs_list->fields("contactname"))) ?></span>
                <?php } ?>
                <?php if($rs_list->fields("phone") != "") { ?>
                    &nbsp;&nbsp;<span><i class="fa fa-phone"></i>&nbsp;<?php print(MyhtmlEncode($rs_list->fields("phone"))) ?></span>
                <?php } ?>
                <?php if($rs_list->fields("emailid") != "") { ?>
                    &nbsp;&nbsp;<span><i class="fa fa-envelope"></i>&nbsp;<?php print(MyhtmlEncode($rs_list->fields("emailid"))) ?></span>
                <?php } ?>    
                <?php if($rs_list->fields("companyurl") != "") 
                    { ?>
                    &nbsp;&nbsp;<span><i class="fa fa-globe"></i>&nbsp;<?php  print(DisplayWebSiteURL($rs_list->fields("companyurl"),$rs_list->fields("companyname"),'YES'," ","",$rs_list->fields("companyurl"))); ?></span>
                <?php } else if($rs_list->fields("companyname") != "" && $rs_list->fields("companyurl") == "") { ?>    
                    &nbsp;&nbsp;<span><i class="fa fa-building"></i>&nbsp;<?php print(MyhtmlEncode($rs_list->fields("companyname"))) ?></span>
                <?php } ?>
                </p>    
            </div>
            <?php } ?>
        </div>
        <?php if($rs_list->Fields("bannerimagefilename") != "") {?><br/>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12" align="center">
                <?php
                    $str_bannerurl_new_window = "_self";
                    if($rs_list->Fields("bannerurlinnewwindow") == "YES")
                        $str_bannerurl_new_window = "_blank";
                    else {
                        $str_bannerurl_new_window = "_self";
                    }
                    
                    ?>
                    <a href="<?php print $rs_list->Fields("bannerurl"); ?>" target="<?php print $str_bannerurl_new_window; ?>" title="<?php print $rs_list->Fields("title"); ?>"><img src="<?php print $str_img_path.$rs_list->Fields("bannerimagefilename"); ?>" class="img-responsive" alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" /></a>
            </div>
        </div><br/>
        <?php } ?>
        <hr/>
        <?php 
        $int_cnt++;
        $rs_list->MoveNext();
        } ?>
        <br/>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
</body>
</html>
