<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/lib_file_system.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_LINK";
$str_img_path = $STR_SITENAME_WITH_PROTOCOL."/mdm/link/";
$str_db_table_name_mt = "t_page_metatag";
$str_db_table_name = "tr_link";
$str_db_table_name_cat = "t_linkcategory";
$str_xml_file_name = "";
$str_xml_file_name_cms = "link_cms.xml";
#----------------------------------------------------------------------
#read main xml file
$str_query_select = "";
$str_query_select = "SELECT a.title AS cattitle, a.pkid AS catpkid, a.description AS catdesc, a.visible, a.displayorder, b.catpkid, b.pkid, b.title, b.description, displaylinkasimage, linksourcestring, imageurl, imagefilename, linkclickurl, openinnewwindow ";
$str_query_select.= "FROM ".$str_db_table_name." b ";
$str_query_select.= "LEFT JOIN ".$str_db_table_name_cat." a ON b.catpkid = a.pkid AND b.visible = 'YES' ";
$str_query_select.= "WHERE a.visible = 'YES' ";
$str_query_select.= "ORDER BY displaylinkasimage DESC , a.displayorder, b.displayorder, a.title, b.catpkid";
$rs_list=GetRecordSet($str_query_select);
//print $rs_list->Count();
//print $int_total_records;
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
##get Query String Data
$str_aid="";
$str_pt="";
//if(isset($_POST['txt_login']))
//{
//	$str_aid=trim($_POST['txt_login']);
//}
if(isset($_POST['pt']))
{
	//$str_pt=trim($_POST['pt']);
    if(isset($_POST['txt_login']))
    {
        $str_aid=trim($_POST['txt_login']);

        // Set Affiliate ID cookies so it can be accessed from all pages
        setcookie('affid',$str_aid,0,'/');
    }
}
else 
{
    if(isset($_POST["aid"])==true)
    {
        $str_aid=trim($_POST["aid"]);
    }	
}
//print_r($_SESSION);
/*$int_page = 1;
if($int_total_records>0)
{
    $int_total_pages = ceil($int_total_records / $int_records_per_page); 
    //print $int_total_pages;
    if ($int_page > $int_total_pages)
    {
        $int_page = $int_total_pages;
    }
    if($int_page < 1)
    {
        $int_page=1;
    }
}*/
/*if($int_total_records > 0)
{
    $int_total_pages = ceil($int_total_records / $int_records_per_page); 
    //print $int_total_pages;exit;
    if ($int_page > $int_total_pages)
    {
        //$int_page = $int_total_pages;
        $_SESSION["catid"] = "";
        $_SESSION["key"] = "";
        $_SESSION["PagePosition"] = 1;
    }
    
}*/

#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table

$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_mt. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
            </div>
        </div>
        <?php if($str_visible_cms == "YES") { ?>
            <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        <?php if($rs_list->Count() > 0) { ?>
            <?php 
            $int_cnt = 0;
            while(!$rs_list->EOF()) 
            { 
                $flag=0;
		$int_cat_pkid=0;
                
                if($rs_list->Fields("displaylinkasimage") == "YES") 
                {
                    ?>
                    <div class="row padding-10">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <?php 
                            if($int_cat_pkid != $rs_list->fields("catpkid"))
                            {
                                $int_cat_pkid = $rs_list->fields("catpkid"); ?>
                                <h4>&bull;&nbsp;<?php print(MyHtmlEncode($rs_list->Fields("cattitle")));?></h4>
                                <?php if($rs_list->Fields("catdesc") != "")
                                    { ?>
                                <p align="justify"><?php print $rs_list->Fields("catdesc"); ?></p>
                            <?php   }
                            }?>
                            <?php
                            $str_new_window="target=\"_blank\"";
                            if($rs_list->fields("openinnewwindow")=="NO")
                            {
                                    $str_new_window="target=\"_self\"";
                            }
                            if($rs_list->Fields("linksourcestring")!="")
                            {
                                    print($rs_list->Fields("linksourcestring"));
                            }
                            else if(($rs_list->Fields("linksourcestring")=="") && ($rs_list->Fields("imageurl")!=""))
                            { ?>
                                <a href="<?php print($rs_list->fields("linkclickurl")); ?>" <?php print($str_new_window);?> class="Link8ptNormal"><img src="<?php print(MyHtmlEncode($rs_list->fields("imageurl"))); ?>" border="0" alt="Link Banner Photograph" title="Click to visit this website"></a> 
                            <?php
                            }
                            elseif($rs_list->Fields("imagefilename")!="")
                            { ?>
                                <p align="center">
				<a href="<?php print($rs_list->fields("linkclickurl")); ?>" <?php print($str_new_window);?>>
				<?php $path = $str_img_path.$rs_list->Fields("imagefilename");
				//$image_tag=ResizeImageTag($path,0,0,$INT_LINK,"","",0);
				//print($image_tag);
                                ?><img src="<?php print $path; ?>" class="img-responsive" />
                                </a>
                                </p>
                            <?php
                            }
			?>
                        </div>
                    </div>                
            <?php }
            $rs_list->MoveNext();
            } $rs_list->MoveFirst();  ?>
                    
            <?php 
            $int_cnt = 0;
            while(!$rs_list->EOF()) 
            { 
                $flag=0;
		$int_cat_pkid=0;
                if($rs_list->Fields("displaylinkasimage") == "NO") 
                {
                    ?>
                    <div class="row padding-10">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <?php 
                            if($int_cat_pkid != $rs_list->fields("catpkid"))
                            {
                                $int_cat_pkid = $rs_list->fields("catpkid"); ?>
                                <h4>&bull;&nbsp;<?php print(MyHtmlEncode($rs_list->Fields("cattitle")));?></h4>
                                <?php if($rs_list->Fields("catdesc") != "")
                                    { ?>
                                <p align="justify"><?php print $rs_list->Fields("catdesc"); ?></p>
                            <?php   }
                            }?>
                            <?php
                            /*$str_new_window="target=\"_blank\"";
                            if($rs_list->fields("openinnewwindow")=="NO")
                            {
                                    $str_new_window="target=\"_self\"";
                            }
                            if($rs_list->Fields("linksourcestring")!="")
                            {
                                    print($rs_list->Fields("linksourcestring"));
                            }
                            else if(($rs_list->Fields("linksourcestring")=="") && ($rs_list->Fields("imageurl")!=""))
                            { ?>
                                <a href="<?php print($rs_list->fields("linkclickurl")); ?>" <?php print($str_new_window);?> class="Link8ptNormal"><img src="<?php print(MyHtmlEncode($rs_list->fields("imageurl"))); ?>" border="0" alt="Link Banner Photograph" title="Click to visit this website"></a> 
                            <?php
                            }
                            elseif($rs_list->Fields("imagefilename")!="")
                            { ?>
                                <p align="center">
				<a href="<?php print($rs_list->fields("linkclickurl")); ?>" <?php print($str_new_window);?>>
				<?php $path = $str_img_path.$rs_list->Fields("imagefilename");
				//$image_tag=ResizeImageTag($path,0,0,$INT_LINK,"","",0);
				//print($image_tag);
                                ?><img src="<?php print $path; ?>" class="img-responsive" />
                                </a>
                                </p>
                            <?php
                            }*/
			?>
                            <?php 
                            print(DisplayWebSiteUrl(MyHtmlEncode($rs_list->Fields("linkclickurl")),MyHtmlEncode($rs_list->Fields("title")),$rs_list->Fields("openinnewwindow"),"","Click to view Image Url","","Link Url")); ?>
			</div>
                    </div>                
            <?php }
            $rs_list->MoveNext();
            } ?>
        <?php } ?><br/>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/banner.js"></script>
</body>
</html>
