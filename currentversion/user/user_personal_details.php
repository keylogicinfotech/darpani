<?php
/*
File Name  :- user_personal_details.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#--------------------------------------------------------------------------------------------------------
#Include files
session_start();
include("./user_validate_session.php");
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_file_system.php";
include "./../includes/lib_xml.php";
include "./../includes/lib_image.php";
include "./../includes/count_site_unique_view.php";
include "./../includes/lib_app_specific.php";
//include "./../includes/http_to_https.php";
include "./user_config.php";
#-------------------------------------------------------------------------------------------------------
?>
<?php
//print_r($_SESSION);
$str_title_page = "Manage Personal Details";
$str_img_path = "../mdm/user/";

$str_db_table_name = " t_user";

$str_db_table_name_country = " t_site_country";
$str_db_table_name_order_by_country = " ORDER BY displayorder DESC, title ASC ";

$str_db_table_name_states = " t_state";
$str_db_table_name_order_by_state = " ORDER BY title";

//$STR_DB_TABLE_NAME_ORDER_BY_PROMOCODE = " ORDER BY displayorder DESC, title ASC ";

$INT_MODEL_SESSION_PKID = $_SESSION["userpkid"];
//print $INT_MODEL_SESSION_PKID;
#-------------------------------------------------------------------------------------------------------
#Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM " . $str_db_table_name . " WHERE pkid=" . $INT_MODEL_SESSION_PKID;
// print $str_query_select;
$rs_list = GetRecordSet($str_query_select);
$str_desc = trim($rs_list->fields("description"));
//print "desc.........:" .$str_desc;
//print($str_query_select);exit();

#Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM  " . $str_db_table_name_states . $str_db_table_name_order_by_state;
$rs_list_state = GetRecordSet($str_query_select);
$str_statevalues = "";
while (!$rs_list_state->EOF() == true) {
    $str_statevalues = $str_statevalues . $rs_list_state->fields("masterpkid") . "," . $rs_list_state->fields("pkid") . ",'" . addslashes($rs_list_state->fields("title")) . "',";
    $rs_list_state->MoveNext();
}
$str_statevalues = substr($str_statevalues, 0, strlen($str_statevalues) - 1);
//print_r($str_statevalues);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        statearray = new Array(<?php print($str_statevalues) ?>);

        function selectstate(arg) {
            if (arg == 'payee') {
                var_pkid = document.frm_edit.cbo_country.options[document.frm_edit.cbo_country.selectedIndex].value;
                document.frm_edit.cbo_state.options.length = 0;

                payee_state_pkid = 0;
                <?php if ($rs_list->fields("statepkid") > 0) { ?>
                    payee_state_pkid = <?php print($rs_list->fields("statepkid")); ?>;
                <?php } ?>
                for (cnt = 0; cnt < statearray.length; cnt = cnt + 3) {
                    if (statearray[cnt] == var_pkid) {
                        state_pkid = statearray[cnt + 1];
                        state_name = statearray[cnt + 2];
                        document.frm_edit.cbo_state.options.length = document.frm_edit.cbo_state.options.length + 1;
                        document.frm_edit.cbo_state.options[document.frm_edit.cbo_state.options.length - 1].value = state_pkid;
                        document.frm_edit.cbo_state.options[document.frm_edit.cbo_state.options.length - 1].text = state_name;
                        if (state_pkid == payee_state_pkid) {
                            document.frm_edit.cbo_state.options[document.frm_edit.cbo_state.options.length - 1].selected = true;
                        }
                    }
                }
                if (document.frm_edit.cbo_state.options.length == 0) {
                    document.frm_edit.cbo_state.options.length = document.frm_edit.cbo_state.options.length + 1;
                    document.frm_edit.cbo_state.options[document.frm_edit.cbo_state.options.length - 1].value = 0;
                    document.frm_edit.cbo_state.options[document.frm_edit.cbo_state.options.length - 1].text = "No States Available";
                }
            }
        }

        function ofc_selectstate(arg) {
            if (arg == 'ofc_state') {
                var_pkid = document.frm_edit.ofc_country.options[document.frm_edit.ofc_country.selectedIndex].value;
                document.frm_edit.ofc_state.options.length = 0;

                payee_state_pkid = 0;
                <?php if ($rs_list->fields("ofc_statepkid") > 0) { ?>
                    payee_state_pkid = <?php print($rs_list->fields("ofc_statepkid")); ?>;
                <?php } ?>
                for (cnt = 0; cnt < statearray.length; cnt = cnt + 3) {
                    if (statearray[cnt] == var_pkid) {
                        state_pkid = statearray[cnt + 1];
                        state_name = statearray[cnt + 2];
                        document.frm_edit.ofc_state.options.length = document.frm_edit.ofc_state.options.length + 1;
                        document.frm_edit.ofc_state.options[document.frm_edit.ofc_state.options.length - 1].value = state_pkid;
                        document.frm_edit.ofc_state.options[document.frm_edit.ofc_state.options.length - 1].text = state_name;
                        if (state_pkid == payee_state_pkid) {
                            document.frm_edit.ofc_state.options[document.frm_edit.ofc_state.options.length - 1].selected = true;
                        }
                    }
                }
                if (document.frm_edit.ofc_state.options.length == 0) {
                    document.frm_edit.ofc_state.options.length = document.frm_edit.ofc_state.options.length + 1;
                    document.frm_edit.ofc_state.options[document.frm_edit.ofc_state.options.length - 1].value = 0;
                    document.frm_edit.ofc_state.options[document.frm_edit.ofc_state.options.length - 1].text = "No States Available";
                }
            }
        }

        function PreLoadStates() {
            <?php if ($rs_list->fields("countrypkid") > 0) { ?> selectstate('payee');
                ofc_selectstate('ofc_state');
            <?php } ?>
        }
    </script>
    <title><?php print($STR_SITE_TITLE); ?> : <?php print($rs_list->fields("name")); ?> - <?php print $str_title_page; ?></title>
    <?php //include "./../includes/include_files_user.php"; 
    ?>
</head>
<!--<body <?php // if($rs_list->Fields("pkid") != 0) {  
            ?>onLoad="getState(<?php // print $rs_list->Fields("pkid"); 
                                ?>);"<?php // } 
                                        ?>>-->

<body onLoad="PreLoadStates();">
    <?php include($STR_USER_HEADER_PATH); ?>

    <div class="container center-bg">
        <div class="row padding-10">
            <a name="ptop" id="ptop"></a>
            <div class="col-md-12">
                <h2 align="right"><?php print $str_title_page; ?></h2>
            </div>
        </div>
        <hr />
        <div class="row padding-10">
            <div class="col-md-3 col-xs-12 col-sm-12">
                <?php include($STR_USER_PANEL_PATH); ?>
            </div>

            <div class="col-md-9 col-xs-12 col-sm-12">


                <div class="row padding-10">
                    <div class="col-md-12">
                        <form name="frm_edit" id="frm_edit" novalidate enctype="multipart/form-data">

                            <div class="row padding-10">
                                <div class="col-md-12">
                                    <div class="control-group form-group">
                                        <div class="controls">
                                            <label>Login ID</label><span class="text-help-form"> *</span>
                                            <input type="text" name="txt_loginid" id="txt_loginid" placeholder="<?php print $STR_PLACEHOLDER_LOGINID; ?>" class="form-control" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("loginid")))); ?>" <?php  ?>required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_EMAIL_FORMAT; ?>" <?php  ?> maxlength="100">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <fieldset>
                                <legend><b>Home Address:</b></legend>

                                <div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="control-group form-group">
                                            <div class="controls">
                                                <label>Full Name</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_name" id="txt_name" placeholder="<?php print $STR_PLACEHOLDER_FULLNAME; ?>" class="form-control" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("name")))); ?>" <?php /* ?>required data-validation-required-message="Please Enter Full Name."<?php */ ?> maxlength="100">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row padding-10">
                                    <div class="col-md-6">

                                        <div class="control-group form-group">
                                            <div class="controls"><label>Business Name</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_businessname" id="txt_businessname" class="form-control" placeholder="<?php print $STR_PLACEHOLDER_TEXT; ?>" <?php /* ?>required data-validation-required-message="Please Enter City."<?php */ ?> value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("businessname")))); ?>" maxlength="100">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email ID</label><span class="text-help-form"> </span>
                                            <input id="txt_email" name="txt_email" class="form-control" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?>" type="text" value="<?php print(RemoveQuote(trim($rs_list->Fields("emailid")))); ?>">
                                        </div>
                                    </div>

                                </div>

                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="control-group form-group">
                                            <div class="controls">
                                                <label>Select Country</label><span class="text-help-form"> * </span>
                                                <?php
                                                $str_query_select = "";
                                                $str_query_select = "SELECT * FROM " . $str_db_table_name_country . " WHERE visible='YES' ORDER BY title DESC";
                                                // print $str_query_select;
                                                // exit;
                                                $rs_list_country = GetRecordSet($str_query_select); ?>

                                                <select name="cbo_country" id="cbo_country" class="form-control" onClick="selectstate('payee'); ">

                                                    <option <?php print(CheckSelected("", $rs_list->fields("statepkid"))); ?> value="0">-- Select Country --</option>
                                                    <?php
                                                    while (!$rs_list_country->EOF() == true) { ?>
                                                        <option value="<?php print($rs_list_country->fields("pkid")) ?>" <?php print(CheckSelected($rs_list_country->fields("pkid"), $rs_list->fields("countrypkid"))); ?>><?php print($rs_list_country->fields("title")); ?></option>
                                                    <?php
                                                        $rs_list_country->MoveNext();
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="controls">
                                            <label>Select State</label><span class="text-help-form"> * </span>
                                            <select class='form-control' name='cbo_state' id='cbo_state'>
                                                <option value='0'>-- Select State --</option>
                                            </select>
                                        </div>
                                        <!--<div id="get_state"></div>-->
                                    </div>
                                </div>

                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="control-group form-group">
                                            <div class="controls"><label>City</label><span class="text-help-form"> * </span>
                                                <input type="text" name="txt_city" id="txt_city" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_CITY; ?>" value="<?php print(RemoveQuote(trim($rs_list->Fields("city")))); ?>" size="40" maxlength="100">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group form-group">
                                            <div class="controls"><label>Zip / Postal Code</label><span class="text-help-form"> * </span><input type="text" name="txt_zipcode" id="txt_zipcode" class="form-control input-sm" value="<?php print(RemoveQuote(trim($rs_list->Fields("zipcode")))); ?>" placeholder="<?php print $STR_PLACEHOLDER_ZIP; ?>" size="40" maxlength="100">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row padding-10">

                                    <div class="col-md-12">
                                        <div class="control-group form-group">
                                            <div class="controls"><label>Home Address</label><span class="text-help-form"> </span><input type="text" name="ta_add" id="ta_add" placeholder="<?php print $STR_PLACEHOLDER_ADDRESS; ?>" <?php /* ?>required data-validation-required-message="Please Enter Address."<?php */ ?> class="form-control" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("address")))); ?>" maxlength="100"></div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Mobile Number</label><span class="text-help"><?php //print $STR_MSG_ACTION_INVALID_LOGINID; 
                                                                                                ?></span>
                                            <input id="txt_mobno" name="txt_mobno" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_PHONENUMBER; ?>" type="text" value="<?php print($rs_list->Fields("mobileno")); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Phone Number</label><span class="text-help-form"> </span>
                                            <input id="txt_phoneno" name="txt_phoneno" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_PHONENUMBER; ?>" type="text" value="<?php print($rs_list->Fields("phoneno")); ?>">
                                        </div>
                                    </div>
                                </div><br />
                                <fieldset>
                                    <legend><b>Office Address: </b><span class="text-help text-success">(Optional)</span></legend>

                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="control-group form-group">
                                                <div class="controls">
                                                    <label>Full Name</label><span class="text-help-form"> </span>
                                                    <input type="text" name="ofc_fullname" id="ofc_fullname" placeholder="<?php print $STR_PLACEHOLDER_FULLNAME; ?>" class="form-control" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("ofc_fullname")))); ?>" <?php /* ?>required data-validation-required-message="Please Enter Full Name."<?php */ ?> maxlength="100">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row padding-10">
                                        <div class="col-md-6">

                                            <div class="control-group form-group">
                                                <div class="controls"><label>Business Name</label><span class="text-help-form"> </span>
                                                    <input type="text" name="ofc_businessname" id="ofc_businessname" class="form-control" placeholder="<?php print $STR_PLACEHOLDER_TEXT; ?>" <?php /* ?>required data-validation-required-message="Please Enter City."<?php */ ?> value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("ofc_businessname")))); ?>" maxlength="100">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email ID</label><span class="text-help-form"> </span>
                                                <input id="ofc_email" name="ofc_email" class="form-control" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?>" type="text" value="<?php print(RemoveQuote(trim($rs_list->Fields("ofc_email")))); ?>">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="control-group form-group">
                                                <div class="controls">
                                                    <label>Select Country</label><span class="text-help-form"> * </span>
                                                    <?php
                                                    $str_query_select = "";
                                                    $str_query_select = "SELECT * FROM " . $str_db_table_name_country . " WHERE visible='YES' ORDER BY title DESC";
                                                    //print $str_query_select; exit;
                                                    $rs_list_country = GetRecordSet($str_query_select); ?>

                                                    <select name="ofc_country" id="ofc_country" class="form-control" onClick="ofc_selectstate('ofc_state'); ">

                                                        <option <?php print(CheckSelected("", $rs_list->fields("ofc_statepkid"))); ?> value="0">-- Select Country --</option>
                                                        <?php
                                                        while (!$rs_list_country->EOF() == true) { ?>
                                                            <option value="<?php print($rs_list_country->fields("pkid")) ?>" <?php print(CheckSelected($rs_list_country->fields("pkid"), $rs_list->fields("ofc_countrypkid"))); ?>><?php print($rs_list_country->fields("title")); ?></option>
                                                        <?php
                                                            $rs_list_country->MoveNext();
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="controls">
                                                <label>Select State</label><span class="text-help-form"> * </span>
                                                <select class='form-control' name='ofc_state' id='ofc_state'>
                                                    <option value='0'>-- Select State --</option>
                                                </select>
                                            </div>
                                            <!--<div id="get_state"></div>-->
                                        </div>
                                    </div>

                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="control-group form-group">
                                                <div class="controls"><label>City</label><span class="text-help-form"> * </span>
                                                    <input type="text" name="ofc_city" id="ofc_city" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_CITY; ?>" value="<?php print(RemoveQuote(trim($rs_list->Fields("ofc_city")))); ?>" size="40" maxlength="100">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="control-group form-group">
                                                <div class="controls"><label>Zip / Postal Code</label><span class="text-help-form"> * </span><input type="text" name="ofc_zipcode" id="ofc_zipcode" class="form-control input-sm" value="<?php print(RemoveQuote(trim($rs_list->Fields("ofc_zipcode")))); ?>" placeholder="<?php print $STR_PLACEHOLDER_ZIP; ?>" size="40" maxlength="100">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row padding-10">

                                        <div class="col-md-12">
                                            <div class="control-group form-group">
                                                <div class="controls"><label>Office Address</label><span class="text-help-form"> </span><input type="text" name="ofc_add" id="ofc_add" placeholder="<?php print $STR_PLACEHOLDER_ADDRESS; ?>" <?php /* ?>required data-validation-required-message="Please Enter Address."<?php */ ?> class="form-control" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("ofc_address")))); ?>" maxlength="100"></div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Mobile Number</label><span class="text-help"><?php //print $STR_MSG_ACTION_INVALID_LOGINID; 
                                                                                                    ?></span>
                                                <input id="ofc_mobno" name="ofc_mobno" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_PHONENUMBER; ?>" type="text" value="<?php print($rs_list->Fields("ofc_mobileno")); ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Phone Number</label><span class="text-help-form"> </span>
                                                <input id="ofc_phoneno" name="ofc_phoneno" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_PHONENUMBER; ?>" type="text" value="<?php print($rs_list->Fields("ofc_phoneno")); ?>">
                                            </div>
                                        </div>
                                    </div><br />

                                </fieldset>


                                <!--<input type="hidden" name="ta_keyword_list" id="ta_keyword_list" value="<?php // print(MyHtmlEncode(RemoveQuote($rs_list->fields("keywordlist"))));
                                                                                                            ?>">-->
                                <input type="hidden" name="hdn_pkid" id="hdn_pkid" value="<?php print($INT_MODEL_SESSION_PKID); ?>">
                                <div id="success"></div>
                                <?php print DisplayFormButton("EDIT", 0); ?><?php print DisplayFormButton("RESET", 0); ?>

                            </fieldset>
                        </form>


                    </div>
                </div>
                <?php /* ?><div class="row"><div class="col-md-12"><div class="alert alert-warning"><h3 class="nopadding">Help Section</h3><p class="text-help-form">• 	Fields marked with '*' are mandatory.</p></div></div></div><?php */ ?>
            </div>
        </div><br />
        <?php // include($STR_USER_BANNER_PATH); 
        ?>
    </div>

    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH);
    CloseConnection(); ?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_personal_details.js"></script>


    <script>
        function get_state2(masterpkid) { // Call to ajax function
            var pkid = masterpkid;
            var dataString = "country_pkid=" + pkid + "&state_pkid=<?php print $rs_list->Fields("statepkid"); ?>";
            //    alert(dataString);
            $.ajax({
                type: "POST",
                url: "item_get_state_edit_p.php", // Name of the php files
                data: dataString,
                success: function(html) {
                    $("#before_get_state").hide();
                    $("#get_state").html(html);
                }
            });
        }

        function getState(masterpkid) { // Call to ajax function
            var cpkid = masterpkid;
            //alert(cpkid);
            var dataString = "country_pkid=" + cpkid;
            $.ajax({
                type: "POST",
                url: "user_personal_details_get_states_p.php?#ptop", // Name of the php files
                data: dataString,
                success: function(html) {

                    $("#before_states").hide();
                    $("#get_states").html(html);
                }
            });
        }
    </script>
    <!--<script type="text/javascript" src="./../includes/richetxteditor.js"></script>-->

</body>

</html>