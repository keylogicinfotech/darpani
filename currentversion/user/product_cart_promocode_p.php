<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();
set_time_limit(150);
include "./../user/product_validatesession.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_xml.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/lib_datetimeyear.php";
include "./../user/product_config.php";
//include "./../includes/http_to_https.php";	
//print_r($_POST);exit;
#----------------------------------------------------------------------
# posted variables...
$str_promocode = 0;
if(isset($_POST["txt_promocode"]) && $_POST["txt_promocode"] != "")
{
    $str_promocode = $_POST["txt_promocode"];
}

if($str_promocode == "") 
{
    $response['status']='ERR';
    $response['message']="Error... Please Enter Promocode.";
    echo json_encode($response);
    return;
}

$int_userpkid = 0;
if(isset($_POST["hdn_userpkid"]) && $_POST["hdn_userpkid"] != "")
{
    $int_userpkid = $_POST["hdn_userpkid"];
}

if($int_userpkid == 0) 
{
    $response['status']='ERR';
    $response['message']="Error... Please Login to Apply Promocode.";
    echo json_encode($response);
    return;
}

$int_extprice = 0;
if(isset($_POST["hdn_extprice"]) && $_POST["hdn_extprice"] != "")
{
    $int_extprice = $_POST["hdn_extprice"];
}


$item_sessionid="";
if(isset($_SESSION['sessionid']))
{ 
    $item_sessionid = $_SESSION['sessionid']; 
}
$item_uniqueid = "";
if(isset($_SESSION['uniqueid']))
{ 
    $item_uniqueid = $_SESSION['uniqueid']; 
}




$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_PROMOCODE." WHERE title = '".$str_promocode."'";

$rs_list = GetRecordSet($str_query_select);
if($rs_list->EOF())
{
    $response['status']='ERR';
    $response['message']="Promocode Not available.";
    echo json_encode($response);
    return;
}
else 
{
    
    $str_query_select = "";
    $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_PURCHASE." WHERE userpkid=".$int_userpkid. " AND promocodepkid = '".$rs_list->Fields("pkid")."'";
    $rs_list_used_count = GetRecordSet($str_query_select);
    //print $rs_list_used_count->Count(); exit;
    if($rs_list_used_count->Count() > 0)
    {
        $response['status']='ERR';
        $response['message']="Promocode has been applied aleready.";
        echo json_encode($response);
        return;
    }
    
    if($int_extprice >= $rs_list->fields("minimumpurchaserequired"))
    {
    
        if($rs_list->fields("discountpercentage") != 0.00 || $rs_list->fields("discountpercentage") != 0)
        {
            $int_final_discountedamount = ($int_extprice * $rs_list->fields("discountpercentage"))/100;
            $int_extprice = $int_extprice - $int_final_discountedamount;
        }
        else if($rs_list->fields("discountamount") != 0.00 || $rs_list->fields("discountamount") != 0)
        {
            $int_final_discountedamount = $rs_list->fields("discountamount");
            $int_extprice = $int_extprice - $int_final_discountedamount;
        }
        else 
        {
            $int_final_discountedamount = 0.00;
            $int_extprice = $int_extprice; // By chance both discountpercentage & discountamount are set to ZERO then send original value.
        } 
    // print $int_extprice; exit; 

        $str_query_update = "";
        $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_SESSION_CART." SET promocodepkid = ".$rs_list->fields("pkid").", discountpercentage=".$rs_list->fields("discountpercentage").",discountamount=".$rs_list->fields("discountamount"). ",finaldiscountedamount=".$int_final_discountedamount." WHERE sessionid='".$item_sessionid."' AND uniqueid='".$item_uniqueid."'" ;
        //print $str_query_update; exit; 
        ExecuteQuery($str_query_update);
    }
    else
    {
        $response['status']='ERR';
        $response['message']="Minumum Purchase Required.";
        echo json_encode($response);
        return;
    }
}

$response['status']='SUC';
$response['message']="Promocode Applied Successfully.";
echo json_encode($response);
return;

?>