/*
  Jquery Validation using jqBootstrapValidation
   example is taken from jqBootstrapValidation docs 
  */

$(function () {
  $(
    "#frm_shipping_address input, #frm_shipping_address textarea, #frm_shipping_address select"
  ).jqBootstrapValidation({
    preventSubmit: true,
    submitError: function ($form, event, errors) {
      // something to have when submit produces an error ?
      // Not decided if I need it yet
    },

    submitSuccess: function ($form, event) {
      event.preventDefault(); // prevent default submit behaviour
      // get values from FORM
      var txt_name = $("input#txt_name").val();
      var txt_phone = $("input#txt_phone").val();
      var txt_email = $("input#txt_email").val();
      var txt_pincode = $("input#txt_pincode").val();
      var ta_address = $("textarea#ta_address").val();
      var txt_city = $("input#txt_city").val();
      var cbo_country = $("select#cbo_country").val();
      //var cbo_state = $("select#cbo_state").val();
      //var cbo_state = $("#after_get_state").find("select[name=cbo_state]").val();

      //console.log("length = "+$("#after_get_state").find("select[name=cbo_state]").length)
      //check if div has select inside it or not
      if ($("#after_get_state").find("select[name=cbo_state]").length > 0) {
        //there ..send this value
        //console.log("slected value = "+$("#after_get_state").find("select[name=cbo_state]").val())
        var cbo_state = $("#after_get_state")
          .find("select[name=cbo_state]")
          .val();
      } else {
        var cbo_state = $("#before_get_state")
          .find("select[name=cbo_state]")
          .val();
        //not there send other select-box value
      }

      $.ajax({
        url: "./product_address_p.php",
        type: "POST",
        data: {
          txt_name: txt_name,
          txt_phone: txt_phone,
          txt_email: txt_email,
          txt_pincode: txt_pincode,
          ta_address: ta_address,
          txt_city: txt_city,
          cbo_country: cbo_country,
          cbo_state: cbo_state,
        },
        cache: false,

        success: function (data) {
          //alert(data);
          var $responseText = JSON.parse(data);
          if ($responseText.status == "SUC") {
            // Success message
            /*$('#success').html("<div class='alert alert-success'>");
                        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                        $('#success > .alert-success').append("<strong> " + $responseText.message + " </strong>");
                        $('#success > .alert-success').append('</div>');

                        //clear all fields
                        $('#frm_add').trigger("reset");
                        
                        $("#success").show();*/
            window.location.href = "./product_payment_method.php?&#ptop";
            //setTimeout(function() { $("#success").hide(); }, 2000);
          } else if ($responseText.status == "ERR") {
            // Fail message
            $("#success").html("<div class='alert alert-danger'>");
            $("#success > .alert-danger")
              .html(
                "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;"
              )
              .append("</button>");
            $("#success > .alert-danger").append(
              "<strong> " + $responseText.message + " "
            );
            $("#success > .alert-danger").append("</div>");
          }
        },

        /*success: function() {
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Your contact inquiry has been submitted, we will contact you Soon. Thank you for communication.</strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + " it seems that my mail server is not responding...</strong> Could you please email me directly to <a href='mailto:info@websitename.com?Subject=Message_Me from websitename.com;>info@websitename.com</a> ? Sorry for the inconvenience!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },*/
      });
    },
    filter: function () {
      return $(this).is(":visible");
    },
  });

  $('a[data-toggle="tab"]').click(function (e) {
    e.preventDefault();
    $(this).tab("show");
  });
});

/*When clicking on Full hide fail/success boxes */
$("#name").focus(function () {
  $("#success").html("");
});
