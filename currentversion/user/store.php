<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
include "./../user/store_config.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_STORE";
$str_db_table_name_metatag = "t_page_metatag";

$str_xml_file_name = "";
$str_xml_file_name_cat = "";

$str_xml_file_name_cms = "store_cms.xml";
$int_records_per_page = 1;


#----------------------------------------------------------------------
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_cat_pkid = "";
if(isset($_POST["catid"]) && trim($_POST["catid"]) != "" )
{   
    $int_cat_pkid = trim($_POST["catid"]);    
}

$str_name_key = "";
if(isset($_POST["key"]) && trim($_POST["key"]) != "" )
{ $str_name_key = trim($_POST["key"]); }


# get data for paging
$int_page = "";
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"]) != "" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"]) > 0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page = 1; }
//print_r($_POST);
$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";

$str_where = "";
if($int_cat_pkid > 0)
{
    $str_where = " AND a.subcatpkid=".$int_cat_pkid." ";
}
#----------------------------------------------------------------------
# Select Query to get Categories
$str_query_select = "";
$str_query_select = "SELECT DISTINCT(b.subcatpkid), b.subcattitle FROM " .$STR_DB_TABLE_NAME. " a LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." b ON a.subcatpkid=b.subcatpkid WHERE b.visible='YES'";
$rs_list_cat = GetRecordSet($str_query_select);
//print $rs_list_cat_list->Count();


# Select Query to get list
$str_query_select = "";
$str_query_select = "SELECT a.*, b.subcattitle, c.thumbphotofilename, c.largephotofilename FROM " .$STR_DB_TABLE_NAME. " a ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." b ON a.subcatpkid=b.subcatpkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_PHOTO." c ON a.pkid=c.masterpkid WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND approved='YES'".$str_where." ";
$str_query_select .= "ORDER BY a.displayorder, a.createdatetime, a.title ";
//print $str_query_select; exit;
$rs_list = GetRecordSet($str_query_select);
//print $rs_list->Count();
#----------------------------------------------------------------------
$int_total_records = 0;
$int_total_records = 0;	
//print $int_total_records;
//print_r($_SESSION);
//print $int_total_records; exit;
#open cms xml file
$str_desc_cms = "";
$str_desc_cms2 = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION1", $fp);
$str_desc_cms2 = getTagValue("ITEMKEYVALUE_DESCRIPTION2", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE1", $fp);
$str_visible_cms2 = getTagValue("ITEMKEYVALUE_VISIBLE2", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatag. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
            </div>
        </div>
        <?php if($str_visible_cms == "YES") { ?>
            <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        <?php if($rs_list_cat->Count() > 0) { ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <form id="frm_fltr" id="frm_fltr" action="" method="POST">
                    <select class="form-control" id="catid" name="catid" onChange="document.forms['frm_fltr'].submit();">
                        <option value="0">View All Categories</option>
                        <?php
                        while(!$rs_list_cat->EOF())         
                        { ?>    
                        <option value="<?php print $rs_list_cat->Fields("subcatpkid"); ?>" <?php print CheckSelected($rs_list_cat->Fields("subcatpkid"), $int_cat_pkid); ?>><?php print $rs_list_cat->Fields("subcattitle"); ?></option>
                <?php  $rs_list_cat->MoveNext();
                        } ?>
                    </select>
                </form>
            </div>
        </div><br/>    
        <?php } ?>
         <?php if($str_visible_cms2 == "YES") { ?>
        <?php if($str_desc_cms2 != "" && $str_desc_cms2 != "<br>") { ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms2);?></p></div>
            </div>
        </div>
         <?php } } ?>
        <?php if($rs_list->Count() > 0) { ?>
        <div class="row padding-10">
            <?php 
            $int_cnt = 0;
            while(!$rs_list->EOF()) {
            ?>
            <div class="col-sm-4 col-md-3 col-xs-6 col-lg-3" align="center"> 
                <div class="thumbnail"> 
                    <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/online-store/<?php print(GetEncryptId($rs_list->Fields("subcatpkid")));?>/<?php  print(GetEncryptId($rs_list->Fields("pkid")));?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>"><img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive"> </a>
                    <div class=" text-top-right">
                        <?php if($rs_list->Fields("displayasnew") == "YES") { print $STR_ICON_PATH_NEW; } ?>
                        <?php if($rs_list->Fields("displayashot") == "YES") { print $STR_ICON_PATH_HOT; } ?>
                        <?php if($rs_list->Fields("displayasfeatured") == "YES") { print $STR_ICON_PATH_FEATURED; } ?>
                    </div>
                    
                    
                    <div class="caption">
                        <h4><a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/online-store/<?php print(GetEncryptId($rs_list->Fields("subcatpkid")));?>/<?php  print(GetEncryptId($rs_list->Fields("pkid")));?>" title="<?php print $str_hover_view_details; ?>"><b><?php print $rs_list->Fields("title"); ?></b></a></h4>
                        <h4><?php print $rs_list->Fields("subcattitle"); ?></h4>
                        <?php if($rs_list->Fields("ourprice") > 0) { ?>
                            <label><h5 class="nopadding">List Price :</h5></label>&nbsp;<label><b class="text-muted"><?php print "<strike>US $".$rs_list->Fields("listprice")."</strike>"; ?></b></label>
                            <br/>
                            <label><h5 class="nopadding">Our Price :</h5></label>&nbsp;<label><b class="text-primary"><?php print "US $".$rs_list->Fields("ourprice").""; ?></b></label>
                        <?php } else { ?>
                            <label><h5 class="nopadding">Price :</h5></label>&nbsp;<label><b class="text-primary"><?php print "US $".$rs_list->Fields("listprice").""; ?></b></label>
                        <?php }  ?>
                        <?php if($rs_list->Fields("memberprice") > 0) { ?><br/>
                        <label><h5 class="nopadding">Member Price :</h5></label>&nbsp;<label><b class="text-primary"><?php print "US $".$rs_list->Fields("memberprice").""; ?></b></label>  <hr/>  
                        <?php } else { print "<hr/>"; }  ?>
                        <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/online-store/<?php print(GetEncryptId($rs_list->Fields("subcatpkid")));?>/<?php  print(GetEncryptId($rs_list->Fields("pkid")));?>" class="btn btn-primary btn-sm" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>"><b><?php print $STR_BUTTON_VIEW_DETAILS; ?></b>&nbsp;<i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i></a>
                    </div> 
                </div> 
            </div>
            <?php
            $int_cnt++; if($int_cnt%4 == 0) { print "</div><div class='row padding-10'>"; }
            $rs_list->MoveNext();
            }
            ?>
        </div>
        <?php } ?>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
</body>
</html>
