<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/http_to_https.php";	
include "./../includes/unique_site_view_count.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------

$str_title_page = "PG_LEGAL";
$str_img_path = $STR_IMG_PATH_LEGAL;
$str_db_table_name = "t_page_metatag";
$str_xml_file_name = "legalstuff.xml";
//print $str_xml_file_name;
$str_xml_file_name_cms = "legalstuff_cms.xml";
#----------------------------------------------------------------------
#read main xml file
$str_list_xml = "";
$str_list_xml = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");
//print_r($str_list_xml);
#open cms xml file
$str_desc_cms="";
$fp=OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms=getTagValue("ITEMKEYVALUE_DESCRIPTION",$fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE",$fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table
$str_select_query = "";
$str_select_query = "SELECT title FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page."'";
$rs_list = GetRecordset($str_select_query);
//$str_title_page_name = "";
$str_title_page = $rs_list->fields("title");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list->fields("title")) ;?></title>
    <?php print(Display_Page_Metatag("PG_LEGAL")); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />       
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>           
            </div>
        </div>
         <?php if(strtoupper(trim($str_visible_cms)) == "YES"){ ?>
        <?php if($str_desc_cms != "" && $str_desc_cms != "<br/>") { ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms);?></p></div>
            </div>
        </div>
	<?php } ?><?php } ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <?php 
            $arr_test=array_keys($str_list_xml);
            if(trim($arr_test[0]) != "ROOT_ITEM"){
                foreach($str_list_xml as $key => $val) 
                { 
                    if(is_array($val)){ ?>
                
               <?php /* ?> <h4><b><a href="#<?php print(MyHtmlEncode($str_list_xml[$key]['PKID']));?>" title="<?php print(MyHtmlEncode($str_list_xml[$key]['TITLE']));?>">&bull;&nbsp;<?php print(MyHtmlEncode($str_list_xml[$key]['TITLE']));?></a></b></h4><hr/>   <?php */ ?>                     
        <?php       }
                }
            }?>
            </div>
        </div>
        <?php 
        $arr_test=array_keys($str_list_xml);
        if(trim($arr_test[0]) != "ROOT_ITEM"){
            foreach($str_list_xml as $key => $val) 
            { 
                if(is_array($val)){ ?> 
            <a id="<?php print(MyHtmlEncode($str_list_xml[$key]['PKID']));?>"></a>    
            <div class="row padding-10">
                <?php if($str_list_xml[$key]["IMAGENAME"]!="") { ?>
                <div class="col-lg-4 col-md-4">
                    <img class="img-responsive <?php /*?>img-hover<?php */?>" src="<?php print($str_img_path.$str_list_xml[$key]["IMAGENAME"]); ?>" border="0" alt="<?php print $str_title_page;?> Image" title="<?php print $str_title_page;?> Image" align="middle"/>
                  <?php /* ?>  <div class="modal fade f-pop-up-<?php print(MyHtmlEncode($str_list_xml[$key]['PKID'])); ?>" align="center" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                    <img class="img-responsive" src="<?php print($str_img_path.$str_list_xml[$key]["IMAGENAME"]); ?>" border="0" alt="<?php print $str_title_page;?> Image" title="<?php print $str_title_page;?> Image" align="middle"/>
                                </div>
                            </div>
                        </div>
                    </div>  <?php */ ?>
                </div>
                <div class="col-lg col-md-">
                    <h3 class="nopadding"><?php print(MyHtmlEncode($str_list_xml[$key]['TITLE']));?></h3>
                    <p align="justify"><?php print($str_list_xml[$key]['DESCRIPTION']);?></p>
                </div>
                <?php } else { ?>
                    <div class="col-lg-12 col-md-12">
                        <h3 class="nopadding"><?php print(MyHtmlEncode($str_list_xml[$key]['TITLE']));?></h3>
                        <p align="justify"><?php print($str_list_xml[$key]["DESCRIPTION"]);?></p>
                    </div>
                <?php } ?>
            </div><hr/>
        <?php   }
            }
        }?>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script> 
</body>
</html>
