<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_LATESTUPDATES";
$str_img_path_photoalbum = $STR_SITENAME_WITH_PROTOCOL."/mdm/photoalbum/";
$str_img_path_sp_series_album = $STR_SITENAME_WITH_PROTOCOL."/mdm/spseriesalbum/";
$str_img_path_event = $STR_SITENAME_WITH_PROTOCOL."/mdm/event/";
$str_img_path_video = $STR_SITENAME_WITH_PROTOCOL."/mdm/video/";
$str_img_path_diary = $STR_SITENAME_WITH_PROTOCOL."/mdm/journal/";
$str_img_path_blog = $STR_SITENAME_WITH_PROTOCOL."/mdm/blog/";
$str_img_path_testimonial = $STR_SITENAME_WITH_PROTOCOL."/mdm/testimonial/";
$str_img_path_wishlist = $STR_SITENAME_WITH_PROTOCOL."/mdm/wishlist/";
$str_img_path_store = $STR_SITENAME_WITH_PROTOCOL."/mdm/store/";
$str_db_table_name_mt = "t_page_metatag";
$str_xml_file_name = "video.xml";
$str_xml_file_name_cat = "video_cat.xml";
$str_xml_file_name_cms = "video_cms.xml";
$int_records_per_page = 1;
#----------------------------------------------------------------------
#----------------------------------------------------------------------
#get metatag page title from table

$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_mt. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
$str_member_flag = false;
if(isset($_SESSION['vipmemberid']) && $_SESSION['vipmemberid'] != "")
{
    $str_member_flag = true;
}
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
            </div>
        </div><br/>
       
        <?php 
        ## Start - Special Series
        
        $str_query_select = "";
        $str_query_select = "SELECT a.pkid AS masterpkid, a.title AS mastertitle, a.*, b.* ";
        $str_query_select.= " FROM t_sp_series_album AS a ";
        $str_query_select.= "LEFT JOIN tr_sp_seriesalbum_detail AS b ON a.pkid = b.masterpkid AND b.setasfront = 'YES' AND b.visible = 'YES' ";
        $str_query_select.= "WHERE a.visible='YES' AND noofvisiblephoto > 0 AND a.displayasnew='YES' ";
        $str_query_select.= "ORDER BY  a.lastupdatedate DESC, a.displayorder, b.displayorder, a.title, a.pkid";
        //print "<br/>".$str_query_select; exit;
        $rs_list_sp_series = GetRecordSet($str_query_select);
        
        $int_total_sp_series_album = 0;
        $int_total_sp_series_album = $rs_list_sp_series->Count();
        //print $rs_list_photo->Count();
        if($rs_list_sp_series->Count() > 0)
        {
        ?>
        <div class="row padding-10">
            <div class="col-lg-12">
                <h3 align="right">Special Series Album<?php if($int_total_sp_series_album > 0) { print " (".$int_total_sp_series_album.")";}?></h3><hr/>
            </div>
        </div><br/>
        <div class="row padding-10">
            
            
            <?php
            $int_cnt = 0;
            while(!$rs_list_sp_series->EOF()) 
            {
                $str_preview_flag = false;
                $str_access_flag = false;
                       
                if($str_member_flag)
                {
                    $str_preview_flag=true;
                    $str_access_flag=true;
                }
                else
                {
                    if($rs_list_sp_series->Fields("previewtoall")=="YES")
                    {
                        $str_preview_flag=true;
                    }
                    else
                    {
                        $str_preview_flag=false;
                    }
                    if($rs_list_sp_series->Fields("accesstoall") == "YES")
                    {
                        $str_access_flag=true;
                    }
                    else
                    {
                        $str_access_flag=false;
                    }
                }
                ?>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="row padding-10">
                    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-3"> 
                        <?php if($rs_list_sp_series->Fields("thumbimagefilename") != "") { ?>
                        <?php
                        if($str_member_flag == true) { ?>    
                            <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/special-series-album/<?php  print(GetEncryptId($rs_list_sp_series->Fields("masterpkid")));?>" title="Click to view this album"><img src="<?php print $str_img_path_sp_series_album.$rs_list_sp_series->Fields("masterpkid")."/".$rs_list_sp_series->Fields("thumbimagefilename"); ?>" alt="<?php print $rs_list_sp_series->Fields("mastertitle"); ?>" title="<?php print $rs_list_sp_series->Fields("mastertitle"); ?>" class="img-responsive"/></a>
                        <?php } else if($str_access_flag == true && $str_preview_flag == true) { ?>    
                            <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/special-series-album/<?php  print(GetEncryptId($rs_list_sp_series->Fields("masterpkid")));?>" title="Click to view this album"><img src="<?php print $str_img_path_sp_series_album.$rs_list_sp_series->Fields("masterpkid")."/".$rs_list_sp_series->Fields("thumbimagefilename"); ?>" alt="<?php print $rs_list_sp_series->Fields("mastertitle"); ?>" title="<?php print $rs_list_sp_series->Fields("mastertitle"); ?>" class="img-responsive"/></a>
                        <?php } else { ?>
                            <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view more info"><img src="<?php print $str_img_path_sp_series_album.$rs_list_sp_series->Fields("masterpkid")."/".$rs_list_sp_series->Fields("thumbimagefilename"); ?>" alt="<?php print $rs_list_sp_series->Fields("mastertitle"); ?>" title="<?php print $rs_list_sp_series->Fields("mastertitle"); ?>" class="img-responsive"/></a>
                        <?php } ?> 
                        
                        <?php } ?>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-9">
                        <h5 class="nopadding">
                            <?php
                            if($str_member_flag == true) { ?>    
                            <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/special-series-album/<?php  print(GetEncryptId($rs_list_sp_series->Fields("masterpkid")));?>" title="Click to view this album"><b><?php print(MakeStringShort(MyHtmlEncode($rs_list_sp_series->Fields("mastertitle")),20));?></b></a> <i>(<?php print($rs_list_sp_series->Fields("noofvisiblephoto")); ?>)</i>
                            <?php } else if($str_access_flag == true && $str_preview_flag == true) { ?>    
                            <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/special-series-album/<?php  print(GetEncryptId($rs_list_sp_series->Fields("masterpkid")));?>" title="Click to view this album"><b><?php print(MakeStringShort(MyHtmlEncode($rs_list_sp_series->Fields("mastertitle")),20)); ?></b></a> <i>(<?php print($rs_list_sp_series->Fields("noofvisiblephoto")); ?>)</i>
                            <?php } else { ?>
                            <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view more info"><b><?php print(MakeStringShort(MyHtmlEncode($rs_list_sp_series->Fields("mastertitle")),20)); ?></b></a> <i>(<?php print($rs_list_sp_series->Fields("noofvisiblephoto")); ?>)</i>
                            <?php } ?> 
                        </h5>    
                        <?php if($rs_list_sp_series->Fields("noofnewphoto") != 0){?>
                        <label class="label label-primary"><?php print $rs_list_sp_series->Fields("noofnewphoto"); ?> NEW PHOTOS</label>
                        
                        <?php } ?>
                        <p class="text-help"><i class="fa fa-calendar"></i>&nbsp;<b><?php print date("d-M-Y", strtotime($rs_list_sp_series->Fields("lastupdatedate"))); ?></b></p>
                    </div>
                </div><br/>
            </div>
            <?php 
            $int_cnt++;
            if($int_cnt % 4 == 0) { print "</div><div class='row padding-10'>"; }
            $rs_list_sp_series->MoveNext();
            }?>
        </div>
        <?php
        }
        ## END - Special Series Album ?>
        
        <?php 
        ## Start - Photo Album 
        
        $str_query_select = "";
        $str_query_select = "SELECT a.pkid AS masterpkid, a.title AS mastertitle, a.*, b.* FROM t_photoalbum AS a ";
        $str_query_select.= "LEFT JOIN tr_photoalbum_detail AS b ON a.pkid=b.masterpkid AND b.setasfront = 'YES' AND b.visible = 'YES'";
        $str_query_select.= "WHERE a.visible='YES' AND noofvisiblephoto > 0 AND a.displayasnew='YES' ";
        $str_query_select.= "ORDER BY a.lastupdatedate DESC, a.displayorder, b.displayorder, a.title, a.pkid";
        $rs_list_photo = GetRecordSet($str_query_select);
        
        $int_total_photoalbum = 0;
        $int_total_photoalbum = $rs_list_photo->Count();
        //print $rs_list_photo->Count();
        if($rs_list_photo->Count() > 0)
        {
        ?>
        <div class="row padding-10">
            <div class="col-lg-12">
                <h3 align="right">Photo Album<?php if($int_total_photoalbum > 0) { print " (".$int_total_photoalbum.")";}?></h3><hr/>
            </div>
        </div><br/>
        <div class="row padding-10">
            
            
            <?php
            $int_cnt = 0;
            while(!$rs_list_photo->EOF()) 
            {
                $str_preview_flag = false;
                $str_access_flag = false;
                       
                if($str_member_flag)
                {
                    $str_preview_flag=true;
                    $str_access_flag=true;
                }
                else
                {
                    if($rs_list_photo->Fields("previewtoall")=="YES")
                    {
                        $str_preview_flag=true;
                    }
                    else
                    {
                        $str_preview_flag=false;
                    }
                    if($rs_list_photo->Fields("accesstoall") == "YES")
                    {
                        $str_access_flag=true;
                    }
                    else
                    {
                        $str_access_flag=false;
                    }
                }
                ?>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="row padding-10">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <?php if($rs_list_photo->Fields("thumbimagefilename") != "") { ?>
                        <?php
                        if($str_member_flag == true) { ?>    
                            <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/photo-album/<?php  print(GetEncryptId($rs_list_photo->Fields("masterpkid")));?>" title="Click to view this album"><img src="<?php print $str_img_path_photoalbum.$rs_list_photo->Fields("masterpkid")."/".$rs_list_photo->Fields("thumbimagefilename"); ?>" alt="<?php print $rs_list_photo->Fields("mastertitle"); ?>" title="<?php print $rs_list_photo->Fields("mastertitle"); ?>" class="img-responsive"/></a>
                        <?php } else if($str_access_flag == true && $str_preview_flag == true) { ?>    
                            <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/photo-album/<?php  print(GetEncryptId($rs_list_photo->Fields("masterpkid")));?>" title="Click to view this album"><img src="<?php print $str_img_path_photoalbum.$rs_list_photo->Fields("masterpkid")."/".$rs_list_photo->Fields("thumbimagefilename"); ?>" alt="<?php print $rs_list_photo->Fields("mastertitle"); ?>" title="<?php print $rs_list_photo->Fields("mastertitle"); ?>" class="img-responsive"/></a>
                        <?php } else { ?>
                            <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view more info"><img src="<?php print $str_img_path_photoalbum.$rs_list_photo->Fields("masterpkid")."/".$rs_list_photo->Fields("thumbimagefilename"); ?>" alt="<?php print $rs_list_photo->Fields("mastertitle"); ?>" title="<?php print $rs_list_photo->Fields("mastertitle"); ?>" class="img-responsive"/></a>
                        <?php } ?> 
                        
                        <?php } ?>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <h5 class="nopadding">
                            <?php
                            if($str_member_flag == true) { ?>    
                            <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/photo-album/<?php  print(GetEncryptId($rs_list_photo->Fields("masterpkid")));?>" title="Click to view this album"><b><?php print(MakeStringShort(MyHtmlEncode($rs_list_photo->Fields("mastertitle")),20)); ?></b></a> <i>(<?php print($rs_list_photo->Fields("noofvisiblephoto")); ?>)</i>
                            <?php } else if($str_access_flag == true && $str_preview_flag == true) { ?>    
                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/photo-album/<?php  print(GetEncryptId($rs_list_photo->Fields("masterpkid")));?>" title="Click to view this album"><b><?php print(MakeStringShort(MyHtmlEncode($rs_list_photo->Fields("mastertitle")),20)); ?></b></a> <i>(<?php print($rs_list_photo->Fields("noofvisiblephoto")); ?>)</i>
                            <?php } else { ?>
                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view more info"><b><?php print(MakeStringShort(MyHtmlEncode($rs_list_photo->Fields("mastertitle")),20)); ?></b></a> <i>(<?php print($rs_list_photo->Fields("noofvisiblephoto")); ?>)</i>
                            <?php } ?> 
                        </h5>    
                        <?php if($rs_list_photo->Fields("noofnewphoto") != 0){?>
                            <label class="label label-primary"><?php print $rs_list_photo->Fields("noofnewphoto"); ?> NEW PHOTOS</label>
                        <?php } ?>
                        <p class="text-help"><i class="fa fa-calendar"></i>&nbsp;<b><?php print date("d-M-Y", strtotime($rs_list_photo->Fields("lastupdatedate"))); ?></b></p>
                    </div>
                </div><br/>
            </div>
            <?php 
            $int_cnt++;
            if($int_cnt % 4 == 0) { print "</div><div class='row padding-10'>"; }
            $rs_list_photo->MoveNext();
            }?>
        </div>
        <?php
        }
        ## END - Photo Album ?>
        
        
        <?php 
        ## Start - Events
        
        $str_query_select = "";
        $str_query_select = "SELECT pkid,title,eventimagefilename,createdatetime,visible,displayorder ";
        $str_query_select.= "FROM t_event ";
        $str_query_select.= "WHERE visible='YES' AND displayasnew='YES' ";
        $str_query_select.= "ORDER BY createdatetime DESC";
        //print "<br/>".$str_query_select; exit;
        $rs_list_event= GetRecordSet($str_query_select);
        
        $int_total_event = 0;
        $int_total_event = $rs_list_event->Count();
        //print $rs_list_photo->Count();
        if($rs_list_event->Count() > 0)
        {
        ?>
        <div class="row padding-10">
            <div class="col-lg-12">
                <h3 align="right">Events<?php if($int_total_event > 0) { print " (".$int_total_event.")";}?></h3><hr/>
            </div>
        </div><br/>
        <div class="row padding-10">
            <?php
            $int_cnt = 0;
            while(!$rs_list_event->EOF()) 
            { ?>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="row padding-10">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"> 
                        <?php if($rs_list_event->Fields("eventimagefilename") != "") { ?>
                        <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/event-calendar" title="Click to view event"><img src="<?php print $str_img_path_event.$rs_list_event->Fields("eventimagefilename"); ?>" alt="<?php print $rs_list_event->Fields("title"); ?>" title="<?php print $rs_list_event->Fields("title"); ?>" class="img-responsive"/></a>
                        <?php } ?>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <h5 class="nopadding"><a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/event-calendar" title="Click to view event"><b><?php print(MyHtmlEncode(MakeStringShort($rs_list_event->fields("title"),20)));?></b></a></h5>    
                        <p class="text-help"><i class="fa fa-calendar"></i>&nbsp;<b><?php print date("d-M-Y", strtotime($rs_list_event->Fields("createdatetime"))); ?></b></p>
                    </div>
                </div><br/>
            </div>
            <?php 
            $int_cnt++;
            if($int_cnt % 4 == 0) { print "</div><div class='row padding-10'>"; }
            $rs_list_event->MoveNext();
            }?>
        </div>
        <?php
        }
        ## END - Events ?>
        
        <?php 
        ## Start - Unsubscribe
        ?>
        <div class="row padding-10">
            <div class="col-lg-12">
                <h3 align="right">Unsubscribe</h3><hr/>
            </div>
        </div><br/>
        
        <?php
        ## END - Unsubscribe?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <a href="https://www.ccbill.com/system/member.cgi" class="Link10ptBold" target="_blank" title="Click to Unsubscribe"><b>Unsubscribe</b></a>
                <p class="nopadding small">For Cancellation of your Membership.</p>
                <br/>
            </div>
        </div>
        
        <?php 
        ## Start - 1-On-1 Live session -- PENDING
        ?>
        <div class="row padding-10">
            <div class="col-lg-12"><h3 align="right">1-ON-1 Live Sessions (PENDING...)</h3><hr/></div>
        </div><br/>
        
        <?php
        ## END - 1-On-1 Live session?>
        
        
        <?php 
        ## Start - Videos
        $str_query_select = "";
        $str_query_select = "SELECT a.pkid AS itempkid, a.title AS itemtitle, a.*, count(b.masterpkid) totalclip ";
        $str_query_select.= "FROM t_video AS a ";
        $str_query_select.= "LEFT JOIN tr_video_clip AS b ON a.pkid = b.masterpkid AND b.visible = 'YES' ";
        $str_query_select.= "WHERE a.visible='YES' AND a.displayasnew='YES' ";
        $str_query_select.= "GROUP BY a.pkid ";
        $str_query_select.= "ORDER BY a.displayorder,a.title";
        $rs_list_video = GetRecordSet($str_query_select);
        
        $int_total_video = 0;
        $int_total_video = $rs_list_video->Count();
        //print $rs_list_photo->Count();
        if($rs_list_video->Count() > 0)
        {
        ?>
        <div class="row padding-10">
            <div class="col-lg-12">
                <h3 align="right">Exclusive Videos<?php if($int_total_video > 0) { print " (".$int_total_video.")";}?></h3><hr/>
            </div>
        </div><br/>
        <div class="row padding-10">
            <?php
            $int_cnt = 0;
            while(!$rs_list_video->EOF()) 
            {
                $str_preview_flag = false;
                $str_access_flag = false;
                       
                if($str_member_flag)
                {
                    $str_preview_flag=true;
                    $str_access_flag=true;
                }
                else
                {
                    if($rs_list_video->Fields("previewtoall")=="YES")
                    {
                        $str_preview_flag=true;
                    }
                    else
                    {
                        $str_preview_flag=false;
                    }
                    if($rs_list_video->Fields("accesstoall") == "YES")
                    {
                        $str_access_flag=true;
                    }
                    else
                    {
                        $str_access_flag=false;
                    }
                } ?>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="row padding-10">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <?php if($rs_list_video->Fields("imagefilename") != "") { ?>
                            <?php
                            if($str_member_flag == true) { ?>    
                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/video/<?php print(GetEncryptId($rs_list_video->Fields("catpkid")));?>/<?php  print(GetEncryptId($rs_list_video->Fields("pkid")));?>" title="Click to view" ><img src="<?php print $str_img_path_video.$rs_list_video->Fields("imagefilename"); ?>" alt="<?php print $rs_list_video->Fields("itemtitle"); ?>" title="<?php print $rs_list_video->Fields("itemtitle"); ?>" class="img-responsive"/></a>
                            <?php } else if($str_access_flag == true && $str_preview_flag == true) { ?>    
                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/video/<?php print(GetEncryptId($rs_list_video->Fields("catpkid")));?>/<?php  print(GetEncryptId($rs_list_video->Fields("pkid")));?>" title="Click to view" ><img src="<?php print $str_img_path_video.$rs_list_video->Fields("imagefilename"); ?>" alt="<?php print $rs_list_video->Fields("itemtitle"); ?>" title="<?php print $rs_list_video->Fields("itemtitle"); ?>" class="img-responsive"/></a>
                            <?php } else { ?>
                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view"><img src="<?php print $str_img_path_video.$rs_list_video->Fields("imagefilename"); ?>" alt="<?php print $rs_list_video->Fields("itemtitle"); ?>" title="<?php print $rs_list_video->Fields("itemtitle"); ?>" class="img-responsive"/></a>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <h5 class="nopadding">
                            <?php
                            if($str_member_flag == true) { ?>    
                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/video/<?php print(GetEncryptId($rs_list_video->Fields("catpkid")));?>/<?php  print(GetEncryptId($rs_list_video->Fields("pkid")));?>" title="Click to view" ><b><?php print(MyHtmlEncode(MakeStringShort($rs_list_video->fields("title"),20)));?></b></a><?php if($rs_list_video->fields("totalclip")>0) { print(" <i>(".$rs_list_video->fields("totalclip").")</i>"); } ?>
                            <?php } else if($str_access_flag == true && $str_preview_flag == true) { ?>    
                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/video/<?php print(GetEncryptId($rs_list_video->Fields("catpkid")));?>/<?php  print(GetEncryptId($rs_list_video->Fields("pkid")));?>" title="Click to view" ><b><?php print(MyHtmlEncode(MakeStringShort($rs_list_video->fields("title"),20)));?></b></a><?php if($rs_list_video->fields("totalclip")>0) { print(" <i>(".$rs_list_video->fields("totalclip").")</i>"); } ?>
                            <?php } else { ?>
                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view"><b><?php print(MyHtmlEncode(MakeStringShort($rs_list_video->fields("title"),20)));?></b></a><?php if($rs_list_video->fields("totalclip")>0) { print(" <i>(".$rs_list_video->fields("totalclip").")</i>"); } ?>
                            <?php } ?>
                        </h5>   
                        <?php if($rs_list_video->Fields("totalclip") > 0){?>
                            <label class="label label-primary"><?php print $rs_list_video->Fields("totalclip"); ?> NEW VIDEO CLIPS</label>
                        <?php } ?>
                        <p class="text-help"><i class="fa fa-calendar"></i>&nbsp;<b><?php print date("d-M-Y", strtotime($rs_list_video->Fields("lastupdatedatetime"))); ?></b></p>
                    </div>
                </div><br/>
            </div>
            <?php 
            $int_cnt++; 
            if($int_cnt % 4 == 0) { print "</div><div class='row padding-10'>"; }
            $rs_list_video->MoveNext();
            }?>
        </div>
        <?php
        }
        ## END - Videos ?>
        
        <?php 
        ## Start - Diary
        
        $str_query_select = "";
        $str_query_select = "SELECT pkid,title,thumbimagefilename,date,visible,displayorder, noofnewphoto, updatedetails, lastupdatedate, previewtoall, accesstoall ";
        $str_query_select.= "FROM t_journal ";
        $str_query_select.= "WHERE visible='YES' AND displayasnew='YES' AND noofvisiblephoto > 0  ";
        $str_query_select.= "ORDER BY displayorder, date DESC , title";
        //print "<br/>".$str_query_select; exit;
        $rs_list_diary = GetRecordSet($str_query_select);
        
        $int_total_diary = 0;
        $int_total_diary = $rs_list_diary->Count();
        //print $rs_list_photo->Count();
        if($rs_list_diary->Count() > 0)
        {
        ?>
        <div class="row padding-10">
            <div class="col-lg-12">
                <h3 align="right">Diary<?php if($int_total_diary > 0) { print " (".$int_total_diary.")";}?></h3><hr/>
            </div>
        </div><br/>
        <div class="row padding-10">
            <?php
            $int_cnt = 0;
            while(!$rs_list_diary->EOF()) 
            {
                $str_preview_flag = false;
                $str_access_flag = false;
                       
                if($str_member_flag)
                {
                    $str_preview_flag=true;
                    $str_access_flag=true;
                }
                else
                {
                    if($rs_list_diary->Fields("previewtoall")=="YES")
                    {
                        $str_preview_flag=true;
                    }
                    else
                    {
                        $str_preview_flag=false;
                    }
                    if($rs_list_diary->Fields("accesstoall") == "YES")
                    {
                        $str_access_flag=true;
                    }
                    else
                    {
                        $str_access_flag=false;
                    }
                } ?>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="row padding-10">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"> 
                        
                        <?php if($rs_list_diary->Fields("thumbimagefilename") != "") { ?>
                            <?php if($str_member_flag == true) { ?>    
                                    <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/diary/<?php  print(GetEncryptId($rs_list_diary->Fields("pkid")));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_diary->Fields("title"))))); ?>" title="Click to view" ><img src="<?php print $str_img_path_diary.$rs_list_diary->Fields("pkid")."/".$rs_list_diary->Fields("thumbimagefilename"); ?>" alt="<?php print $rs_list_diary->Fields("title"); ?>" title="<?php print $rs_list_diary->Fields("title"); ?>" class="img-responsive"/></a>
                                <?php } else if($str_access_flag == true && $str_preview_flag == true) { ?>    
                                    <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/diary/<?php  print(GetEncryptId($rs_list_diary->Fields("pkid")));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_diary->Fields("title"))))); ?>" title="Click to view" ><img src="<?php print $str_img_path_diary.$rs_list_diary->Fields("pkid")."/".$rs_list_diary->Fields("thumbimagefilename"); ?>" alt="<?php print $rs_list_diary->Fields("title"); ?>" title="<?php print $rs_list_diary->Fields("title"); ?>" class="img-responsive"/></a>
                                <?php } else { ?>
                                    <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view"><img src="<?php print $str_img_path_diary.$rs_list_diary->Fields("pkid")."/".$rs_list_diary->Fields("thumbimagefilename"); ?>" alt="<?php print $rs_list_diary->Fields("title"); ?>" title="<?php print $rs_list_diary->Fields("title"); ?>" class="img-responsive"/></a>
                            <?php } ?>
                        <?php } //else { print("".MakeBlankTab(0,0,$STR_PREVIEW_NOT_AVAILABLE,"preview-danger")); } ?>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <?php if($str_member_flag == true) { ?>    
                            <h5 class="nopadding"><a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/diary/<?php  print(GetEncryptId($rs_list_diary->Fields("pkid")));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_diary->Fields("title"))))); ?>" title=""><b><?php print(MyHtmlEncode(MakeStringShort($rs_list_diary->fields("title"),20)));?></b></a></h5>
                        <?php } else if($str_access_flag == true && $str_preview_flag == true) { ?>
                           <h5 class="nopadding"><a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/diary/<?php  print(GetEncryptId($rs_list_diary->Fields("pkid")));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_diary->Fields("title"))))); ?>" title=""><b><?php print(MyHtmlEncode(MakeStringShort($rs_list_diary->fields("title"),20)));?></b></a></h5>
                        <?php } else { ?>
                           <h5 class="nopadding"><a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view"><b><?php print(MyHtmlEncode(MakeStringShort($rs_list_diary->fields("title"),20)));?></b></a></h5>
                        <?php } ?>
                        
                        <?php
                        $str_new="";
                        $str_update="";
                        $str_no_new = $rs_list_diary->Fields("noofnewphoto");
                        $str_date=DDMMMYYYYFORMAT($rs_list_diary->Fields("lastupdatedate"));?>
                        
                        <?php 
                        if(($rs_list_diary->Fields("noofnewphoto")!=0) && ($rs_list_diary->Fields("updatedetails")=="YES"))
                        {
                                $str_new="<label class='label label-primary'>".$str_no_new." NEW PHOTOS</label>";
                                //$str_new="New <b>(".$str_no_new.")</b></span> Photos Added";
                                
                                $str_update="<i class='fa fa-calendar'></i>&nbsp;<b>".$str_date."</b>" ;
                        }
                        elseif(($rs_list_diary->Fields("noofnewphoto")!=0) && ($rs_list_diary->Fields("updatedetails")=="NO"))
                        {
                                //$str_new="New <span class='DescriptionText8ptBold'>(".$str_no_new.")</span> Photos <br>Added on <span class=''>".$str_date."</span>";
                            $str_new="<label class='label label-primary'>".$str_no_new." NEW PHOTOS</label>";
                            //$str_new="New <b>(".$str_no_new.")</b></span> Photos Added on <i class='fa fa-calendar'></i>&nbsp;<b>".$str_date."</b>";
                        }
                        elseif(($rs_list_diary->Fields("noofnewphoto")==0) && ($rs_list_diary->Fields("updatedetails")=="YES"))
                        {
                                //$str_update="Information Updated <br>on <span class=''>".$str_date."</span>" ;
                                $str_update="<i class='fa fa-calendar'></i>&nbsp;<b>".$str_date."</b>" ;
                        }
                        elseif(($rs_list_diary->Fields("noofnewphoto")==0) && ($rs_list_diary->Fields("updatedetails")=="NO"))
                        { 
                                //$str_new="Added on <span class='DescriptionText8ptBold'>".$str_date."</span>";
                                $str_new="<i class='fa fa-calendar'></i>&nbsp;<b>".$str_date."</b>" ;
                                
                        }
                        if(($rs_list_diary->fields("noofnewphoto")!=0) || ($rs_list_diary->Fields("updatedetails")=="NO"))
                        {
                                                                                ?>
                            <?php //$str_new = $str_new;
                            $str_new="<label class='label label-primary'>".$str_no_new." NEW PHOTOS</label>";
                            ?>
                <?php
                        }
                ?>
                        <p class="nopadding small"><?php print $str_new; ?></p>
                        <p class="nopadding text-help"><?php print $str_update; ?></p>
                        <?php /* ?><p class=""><i class="fa fa-calendar"></i>&nbsp;Added on <b><?php print date("d-M-Y", strtotime($rs_list_diary->Fields("date"))); ?></b></p><?php */ ?>
                    </div>
                </div><br/>
            </div>
            <?php 
            $int_cnt++;
            if($int_cnt % 4 == 0) { print "</div><div class='row padding-10'>"; }
            $rs_list_diary->MoveNext();
            }?>
        </div>
        <?php
        }
        ## END - Diary ?>
        
        
        
        <?php 
        ## Start - Store ?>
        <?php 
        
        $str_query_select = "";
        $str_query_select = "SELECT a.*, b.subcattitle, c.thumbphotofilename, c.largephotofilename FROM t_store a ";
        $str_query_select .= "LEFT JOIN t_store_subcat b ON a.subcatpkid=b.subcatpkid ";
        $str_query_select .= "LEFT JOIN tr_store_photo c ON a.pkid=c.masterpkid WHERE a.visible='YES' AND b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND approved='YES' AND a.displayasnew='YES' ";
        $str_query_select .= "ORDER BY a.displayorder, a.createdatetime, a.title ";
        //print "<br/>".$str_query_select; exit;
        $rs_list_store = GetRecordSet($str_query_select);
        
        $int_total_store = 0;
        $int_total_store = $rs_list_store->Count();
        
        if($int_total_store > 0) {
        ?>
        <div class="row padding-10">
            <div class="col-lg-12">
                <h3 align="right">Kinky Store<?php if($int_total_store > 0) { print " (".$int_total_store.")";}?></h3><hr/>
            </div>
        </div><br/>
        <div class="row padding-10">
            <?php
            $int_cnt = 0;
            $int_cat_pkid = 0;
            while(!$rs_list_store->EOF()) 
            { 
                if($int_cat_pkid != $rs_list_store->fields("subcatpkid"))
		{
                    $int_cat_pkid = $rs_list_store->fields("subcatpkid"); 
                    
                    $str_query_select = "";
                    $str_query_select = "SELECT count(*) AS totalproducts FROM t_store WHERE subcatpkid=".$int_cat_pkid." AND visible='YES' AND displayasnew='YES'";
                    $rs_list_cat_cnt = GetRecordSet($str_query_select);
                    ?>
            
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h4 class=""><i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;<b><?php print(MyHtmlEncode(MakeStringShort($rs_list_store->fields("subcattitle"),100)));?><?php if($rs_list_cat_cnt->Fields("totalproducts") > 0) { print " (".$rs_list_cat_cnt->Fields("totalproducts").")"; } ?></b></h4>
                    </div>
		<?php
		} ?>
                
                
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="row padding-10">
                    <?php if($rs_list_store->Fields("thumbphotofilename") != "") { ?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"> 
                            <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/online-store/<?php print(GetEncryptId($rs_list_store->Fields("subcatpkid")));?>/<?php  print(GetEncryptId($rs_list_store->Fields("pkid")));?>" title="<?php print $rs_list_store->Fields("title"); ?>"><img src="<?php print $str_img_path_store.$rs_list_store->Fields("pkid")."/".$rs_list_store->Fields("thumbphotofilename"); ?>" alt="<?php print $rs_list_store->Fields("title"); ?>" title="<?php print $rs_list_store->Fields("title"); ?>" class="img-responsive"/></a>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <h5 class="nopadding"><a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/online-store/<?php print(GetEncryptId($rs_list_store->Fields("subcatpkid")));?>/<?php  print(GetEncryptId($rs_list_store->Fields("pkid")));?>" title="Click to view details"><b><?php print(MyHtmlEncode(MakeStringShort($rs_list_store->fields("title"),20)));?></b></a></h5>    
                        
                        <?php if($rs_list_store->fields("memberprice") > 0) { ?>
                            <p class="nopadding small">Price: <b>US $<?php print $rs_list_store->fields("memberprice"); ?></b></p>
                        <?php } else if($rs_list_store->fields("ourprice") > 0) { ?>
                            <p class="nopadding small">Price: <b>US $<?php print $rs_list_store->fields("ourprice"); ?></b></p>
                        <?php } else { ?>
                            <p class="nopadding small">Price: <b>US $<?php print $rs_list_store->fields("listprice"); ?></b></p>
                        <?php } ?>
                        <p class="text-help"><i class="fa fa-calendar"></i>&nbsp;<b><?php print date("d-M-Y", strtotime($rs_list_store->Fields("createdatetime"))); ?></b></p>    
                    </div>
                    <?php } else { ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h5 class="nopadding"><a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/online-store/<?php print(GetEncryptId($rs_list_store->Fields("subcatpkid")));?>/<?php  print(GetEncryptId($rs_list_store->Fields("pkid")));?>" title="<?php print $rs_list_store->Fields("title"); ?>"><b><?php print(MyHtmlEncode(MakeStringShort($rs_list_store->fields("title"),20)));?></b></a></h5>    
                        
                        <?php if($rs_list_store->fields("memberprice") > 0) { ?>
                            <p class="nopadding small">Price: <b>US $<?php print $rs_list_store->fields("memberprice"); ?></b></p>
                        <?php } else if($rs_list_store->fields("ourprice") > 0) { ?>
                            <p class="nopadding small">Price: <b>US $<?php print $rs_list_store->fields("ourprice"); ?></b></p>
                        <?php } else { ?>
                            <p class="nopadding small">Price: <b>US $<?php print $rs_list_store->fields("price"); ?></b></p>
                        <?php } ?>
                        <p class="text-help"><i class="fa fa-calendar"></i>&nbsp;<b><?php print date("d-M-Y", strtotime($rs_list_store->Fields("createdatetime"))); ?></b></p>    
                    </div>
                    <?php } ?>
                </div><br/>
            </div>
            <?php 
            $int_cnt++;
            if($int_cnt % 4 == 0) { print "</div><div class='row padding-10'>"; }
            $rs_list_store->MoveNext();
            }?>
        </div>
        
        <?php 
        }
        ## End - Store
        ?>
        
        <?php 
        ## Start - Wish List
        $str_query_select = "";
        $str_query_select = "SELECT * ";
        $str_query_select.= "FROM t_wish ";
        $str_query_select.= "WHERE visible='YES' AND displayasnew='YES' ";
        $str_query_select.= "ORDER BY pkid DESC";
        //print "<br/>".$str_query_select; exit;
        $rs_list_wishlist= GetRecordSet($str_query_select);
        
        $int_total_wishlist = 0;
        $int_total_wishlist = $rs_list_wishlist->Count();
        //print $rs_list_photo->Count();
        if($rs_list_wishlist->Count() > 0)
        {
        ?>
        <div class="row padding-10">
            <div class="col-lg-12">
                <h3 align="right">Wish List<?php if($int_total_wishlist > 0) { print " (".$int_total_wishlist.")";}?></h3><hr/>
            </div>
        </div><br/>
        <div class="row padding-10">
            <?php
            $int_cnt = 0;
            while(!$rs_list_wishlist->EOF()) 
            { ?>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="row padding-10">
                    <?php if($rs_list_wishlist->Fields("imagefilename") != "") { ?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"> 
                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/wishlist"><img src="<?php print $str_img_path_wishlist.$rs_list_wishlist->Fields("imagefilename"); ?>" alt="<?php print $rs_list_wishlist->Fields("title"); ?>" title="<?php print $rs_list_wishlist->Fields("title"); ?>" class="img-responsive"/></a>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <h5 class="nopadding"><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/wishlist" title="Click to leave a comment"><b><?php print(MyHtmlEncode(MakeStringShort($rs_list_wishlist->fields("title"),20)));?></b></a></h5>    
                        <?php $string = $rs_list_wishlist->Fields("description"); 
//                                $string = "";
                                $string = strip_tags($string);
                                if (strlen($string) > 100) {

                                $stringCut = substr($string, 0, 80);
                                $endPoint = strrpos($stringCut, ' ');

                                //if the string doesn't contain any space then it will cut without word basis.
                                $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                $string .= '...';
                            } ?>
                        <p class="nopadding small"><?php print $string; ?></p>
                    </div>
                    <?php } else { ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h5 class="nopadding"><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/wishlist" title="Click to view details"><b><?php print(MyHtmlEncode(MakeStringShort($rs_list_wishlist->fields("title"),20)));?></b></a></h5>    
                        <?php $string = $rs_list_wishlist->Fields("description"); 
//                                $string = "";
                                $string = strip_tags($string);
                                if (strlen($string) > 100) {

                                $stringCut = substr($string, 0, 100);
                                $endPoint = strrpos($stringCut, ' ');

                                //if the string doesn't contain any space then it will cut without word basis.
                                $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                $string .= '...';
                            } ?>
                        <p class="nopadding small"><?php print $string; ?></p>
                    </div>
                    <?php } ?>
                </div><br/>
            </div>
            <?php 
            $int_cnt++;
            if($int_cnt % 4 == 0) { print "</div><div class='row padding-10'>"; }
            $rs_list_wishlist->MoveNext();
            }?>
        </div>
        <?php
        }
        ## END - Wish List ?>
        
        <?php 
        ## Start - Blog
        
        $str_query_select = "";
        $str_query_select = "SELECT * ";
        $str_query_select.= "FROM tr_blog ";
        $str_query_select.= "WHERE visible='YES' AND displayasnew='YES' AND approved='YES' ";
        $str_query_select.= "ORDER BY submitdatetime DESC";
        //print "<br/>".$str_query_select; exit;
        $rs_list_blog= GetRecordSet($str_query_select);
        
        $int_total_blog = 0;
        $int_total_blog = $rs_list_blog->Count();
        //print $rs_list_photo->Count();
        if($rs_list_blog->Count() > 0)
        {
        ?>
        <div class="row padding-10">
            <div class="col-lg-12">
                <h3 align="right">Blog<?php if($int_total_blog > 0) { print " (".$int_total_blog.")";}?></h3><hr/>
            </div>
        </div><br/>
        <div class="row padding-10">
            <?php
            $int_cnt = 0;
            while(!$rs_list_blog->EOF()) 
            { ?>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="row padding-10">
                    <?php if($rs_list_blog->Fields("imagefilename") != "") { ?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"> 
                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/blog-<?php print $rs_list_blog->Fields("blogpkid");?>#blogleavecomment" title="Click to leave a comment"><img src="<?php print $str_img_path_blog.$rs_list_blog->Fields("imagefilename"); ?>" alt="<?php print $rs_list_blog->Fields("title"); ?>" title="<?php print $rs_list_blog->Fields("title"); ?>" class="img-responsive"/></a>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <h5 class="nopadding"><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/blog-<?php print $rs_list_blog->Fields("blogpkid");?>#blogleavecomment" title="Click to leave a comment"><b><?php print(MyHtmlEncode(MakeStringShort($rs_list_blog->fields("title"),20)));?></b></a></h5>    
                        <p class="text-help"><i class="fa fa-calendar"></i>&nbsp;<b><?php print date("d-M-Y", strtotime($rs_list_blog->Fields("submitdatetime"))); ?></b></p>
                    </div>
                    <?php } else { ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h5 class="nopadding"><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/blog-<?php print $rs_list_blog->Fields("blogpkid");?>#blogleavecomment" title="Click to leave a comment"><b><?php print(MyHtmlEncode(MakeStringShort($rs_list_blog->fields("title"),20)));?></b></a></h5>    
                        <p class="text-help"><i class="fa fa-calendar"></i>&nbsp;<b><?php print date("d-M-Y", strtotime($rs_list_blog->Fields("submitdatetime"))); ?></b></p>
                    </div>
                    <?php } ?>
                </div><br/>
            </div>
            <?php 
            $int_cnt++;
            if($int_cnt % 4 == 0) { print "</div><div class='row padding-10'>"; }
            $rs_list_blog->MoveNext();
            }?>
        </div>
        <?php
        }
        ## END - Blog ?>
        
        
        <?php 
        ## Start - Testimonial
        $str_query_select = "";
        $str_query_select = "SELECT * ";
        $str_query_select.= "FROM t_testimonial ";
        $str_query_select.= "WHERE visible='YES' AND displayasnew='YES' AND approved='YES' ";
        $str_query_select.= "ORDER BY createdatetime DESC";
        //print "<br/>".$str_query_select; exit;
        $rs_list_testimonial= GetRecordSet($str_query_select);
        
        $int_total_testimonial = 0;
        $int_total_testimonial = $rs_list_testimonial->Count();
        //print $rs_list_photo->Count();
        if($rs_list_testimonial->Count() > 0)
        {
        ?>
        <div class="row padding-10">
            <div class="col-lg-12">
                <h3 align="right">Testimonial<?php if($int_total_blog > 0) { print " (".$int_total_blog.")";}?></h3><hr/>
            </div>
        </div><br/>
        <div class="row padding-10">
            <?php
            $int_cnt = 0;
            while(!$rs_list_testimonial->EOF()) 
            { ?>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="row padding-10">
                    <?php if($rs_list_testimonial->Fields("imagefilename") != "") { ?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"> 
                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/praise" title="View Details"><img src="<?php print $str_img_path_testimonial.$rs_list_testimonial->Fields("imagefilename"); ?>" alt="Testimonial" title="View Testimonial" class="img-responsive"/></a>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <h5 class="nopadding"><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/praise" title="View Details"><b><?php print(MakeStringShort(MyHtmlEncode($rs_list_testimonial->Fields("personname")),20));?></b></a></h5>    
                        <?php $string = $rs_list_testimonial->Fields("description"); 
//                                $string = "";
                                $string = strip_tags($string);
                                if (strlen($string) > 80) {

                                $stringCut = substr($string, 0, 80);
                                $endPoint = strrpos($stringCut, ' ');

                                //if the string doesn't contain any space then it will cut without word basis.
                                $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                $string .= '...';
                            } ?>
                        <p class="nopadding small"><?php print $string; ?></p>
                    </div>
                    <?php } else { ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h5 class="nopadding"><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/praise" title="View Details"><b><?php print(MakeStringShort(MyHtmlEncode($rs_list_testimonial->Fields("personname")),20));?></b></a></h5>    
                         <?php $string = $rs_list_testimonial->Fields("description"); 
//                                $string = "";
                                $string = strip_tags($string);
                                if (strlen($string) > 100) {

                                $stringCut = substr($string, 0, 100);
                                $endPoint = strrpos($stringCut, ' ');

                                //if the string doesn't contain any space then it will cut without word basis.
                                $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                
                                $string .= '...';
                            } ?>
                        <p class="nopadding small"><?php print $string; ?></p>
                    </div>
                    <?php } ?>
                </div><br/>
            </div>
            <?php 
            $int_cnt++;
            if($int_cnt % 4 == 0) { print "</div><div class='row padding-10'>"; }
            $rs_list_testimonial->MoveNext();
            }?>
        </div>
        <?php
        }
        ## END - Testimonial ?>
        
        <?php 
        ## Start - Banners & Links
        $str_query_select = "";
        $str_query_select="SELECT count(*) AS totalnewbanner FROM t_banner WHERE visible='YES' AND displayasnew='YES' ORDER BY displayorder,pkid";
	$rs_list_banner_new = GetRecordSet($str_query_select);
	
        $str_query_select = "";
        $str_query_select = "SELECT count(pkid) AS totalbanner FROM t_banner WHERE visible='YES'";
	$rs_list_banner = GetRecordset($str_query_select);
        
        $str_query_select = "";
        $str_query_select = "SELECT count(b.catpkid) AS totallinks FROM t_linkcategory AS a LEFT JOIN tr_link AS b ON a.pkid=b.catpkid and b.visible='YES' AND a.visible='YES'";
        $rs_list_links = GetRecordset($str_query_select);
        
//print $rs_list_photo->Count();
        ?>
        <div class="row padding-10">
            <div class="col-lg-12">
                <h3 align="right">Banners & Links</h3><hr/>
            </div>
        </div><br/>
        <div class="row padding-10">
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row padding-10">
                    <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                        <h5 class="nopadding"><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/banner" title="View Banners"><b>Banners</b></a> (<?php print $rs_list_banner->Fields("totalbanner"); ?>)</h5> 
                       <?php if($rs_list_banner_new->Fields("totalnewbanner") != 0){?>
                            
                            <label class="label label-primary"><?php print $rs_list_banner_new->Fields("totalnewbanner"); ?> NEW BANNERS</label>
                        <?php } ?>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                        <h5 class="nopadding"><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/link" title="View Links"><b>Links</b></a><?php if($rs_list_links->fields("totallinks")!=0){ print(" (".$rs_list_links->fields("totallinks").")"); } ?>
                        </h5>
                        <p class="nopadding small">View my favorite Links.</p>
                    </div>
                    
                </div><br/>
            </div>
        </div>
        <?php
       
        ## END - Email Me ?>
        
        
        <?php 
        ## Start - Email Me ?>
        <div class="row padding-10">
            <div class="col-lg-12">
                <h3 align="right">Email Me</h3><hr/>
            </div>
        </div><br/>
        <div class="row padding-10">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="row padding-10">
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h5 class="nopadding"><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/email-me" title="Click to send email"><b>Email Me</b></a></h5>    
                        <p class="nopadding small">Send me a private email.</p>
                    </div>
                    
                </div><br/>
            </div>
           
        </div>
        <?php
        ## END - Email Me ?>
        <br/>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
</body>
</html>
