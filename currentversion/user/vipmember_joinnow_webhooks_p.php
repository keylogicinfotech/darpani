<?php
/*
	Module Name	:-  ccbill member webhooks
	File Name	:- signup_webhooks_p.php
	Create Date	:- 01-NOV-2018
	Initially Created by :- 015
*/
#------------------------------------------------------------------------------------------
#	include files
//	session_start();
	include "./includes/validatesession.php";
	include "./includes/configuration.php";
	include "./includes/lib_data_access.php";
	include "./includes/lib_common.php";
	include "./includes/lib_email.php";
	include "./includes/lib_xml.php";
//	include "./includes/http_to_https.php";		
#------------------------------------------------------------------------------------------
##### IMPORTANT NOTE #####
# CCBill sends POST data after successful transaction
# Now, if any data has single quote in string then
# this transaction data are not stored into website database 
# due to syntax error of single quote. This results into missing
# transaction for website. To resolve this issue, we are using 
# str_replace function of PHP where we replace single quote with blank 
##########################
	//print_r($_POST); exit;
	//$item_sessionid=$_SESSION["sessionid"];
	//$item_uniqueid=$_SESSION["uniqueid"];
	
	$str_initial_subscription_id="";
	if(isset($_POST["subscriptionId"]) && trim($_POST["subscriptionId"])!="")
	{
		$str_initial_subscription_id=RemoveQuote(trim($_POST["subscriptionId"]));
	}
        
        $str_recurringdate="";
	if(isset($_POST["timestamp"]) && trim($_POST["timestamp"])!="")
	{
		$str_recurringdate=RemoveQuote(trim($_POST["timestamp"]));
	}
	
	$str_firstname="";
	if(isset($_POST["firstName"]) && trim($_POST["firstName"])!="")
	{
		//$str_firstname=RemoveQuote(trim($_POST["firstName"]));
		$str_firstname=str_replace("'", "", RemoveQuote(trim($_POST["firstName"]))); 
	}

	$str_lastname="";
	if(isset($_POST["lastName"]) && trim($_POST["lastName"])!="")
	{
		//$str_lastname=RemoveQuote(trim($_POST["lastName"]));
		$str_lastname=str_replace("'", "", RemoveQuote(trim($_POST["lastName"]))); 
	}

	$str_address1="";
	if(isset($_POST["address1"]) && trim($_POST["address1"])!="")
	{
		//$str_address1=RemoveQuote(trim($_POST["address1"]));
		$str_address1=str_replace("'", "", RemoveQuote(trim($_POST["address1"]))); 
	}
	$str_city="";
	if(isset($_POST["city"]) && trim($_POST["city"])!="")
	{
		$str_city=RemoveQuote(trim($_POST["city"]));
	}
	$str_state="";
	if(isset($_POST["state"]) && trim($_POST["state"])!="")
	{
		$str_state=RemoveQuote(trim($_POST["state"]));
	}
	$str_country="";
	if(isset($_POST["country"]) && trim($_POST["country"])!="")
	{
		$str_country=RemoveQuote(trim($_POST["country"]));
	}
	$str_postalcode="";
	if(isset($_POST["postalCode"]) && trim($_POST["postalCode"])!="")
	{
		$str_postalcode=RemoveQuote(trim($_POST["postalCode"]));
	}

	$str_email="";
	if(isset($_POST["email"]) && trim($_POST["email"])!="")
	{
		$str_email=RemoveQuote(trim($_POST["email"]));
	}

	$str_phonenumber="";
	if(isset($_POST["phoneNumber"]) && trim($_POST["phoneNumber"])!="")
	{
		$str_phonenumber=RemoveQuote(trim($_POST["phoneNumber"]));
	}

	$str_ipaddress="";
	if(isset($_POST["ipAddress"]) && trim($_POST["ipAddress"])!="")
	{
		$str_ipaddress=RemoveQuote(trim($_POST["ipAddress"]));
	}

	$str_username="";
	if(isset($_POST["username"]) && trim($_POST["username"])!="")
	{
		$str_username=RemoveQuote(trim($_POST["username"]));
	}

	$str_password="";
	if(isset($_POST["password"]) && trim($_POST["password"])!="")
	{
		$str_password=RemoveQuote(trim($_POST["password"]));
	}

	$int_price=0.00;
	if(isset($_POST["billedInitialPrice"]) && trim($_POST["billedInitialPrice"])!="")
	{
		$int_price=RemoveQuote(trim($_POST["billedInitialPrice"]));
	}

	

	//$str_purchase_date=date("Y-m-d H:i:s");
	/*$str_purchase_date=$str_recurringdate;
	$expire_date=date("Y-m-d H:i:s",mktime(23, 59, 59, date("m")  , date("d")+$INT_PRODUCT_DOWNLOAD_LIMIT_DAY, date("Y")));
	$str_purchase_day="";
	$str_purchase_month="";
	$str_purchase_year="";
	*/
	/// find the day, month and year from the transaction date
	//$str_purchase_day=date("d",strtotime($str_purchase_date));
	//$str_purchase_month=date("m",strtotime($str_purchase_date));
	//$str_purchase_year=date("Y",strtotime($str_purchase_date));
	
	//$str_download="YES";
	//$str_addedby="YES";	
	
	/*#Query to select very first time purchase record from 't_product_purchase' table with subscriptionid but not with initialsubscriptionid...
	$sel_qry="";
	$sel_qry="SELECT * FROM t_ccbill_member WHERE loginid='".$str_initial_subscription_id."' AND password=' '";
	//print "SELECT: ".$sel_qry;
	$rs_list=GetRecordSet($sel_qry);
	
	if($rs_list->eof()==false)
	{*/
            #Add recurring record In t_product_purchase table for recurring
            $str_insert="INSERT INTO t_ccbill_member(loginid, password,emailid, subscriptionid,price,";
            $str_insert.="firstname, lastname, address, city, state, country, zipcode,ipaddress,phoneno,";
            $str_insert.="registrationdatetime,lastlogindatetime, allowlogin, visible, approved, isccbillmember) VALUES (";
            $str_insert.="'".$str_username."', ";
            $str_insert.="'".$str_password."', ";
            $str_insert.="'".$str_email."', ";
            $str_insert.="'".$str_initial_subscription_id."', ";
            $str_insert.= "".$int_price.", ";
            $str_insert.="'".$str_firstname."', ";
            $str_insert.="'".$str_lastname."', ";
            $str_insert.="'".$str_address1."', ";
            $str_insert.="'".$str_city."', ";
            $str_insert.="'".$str_state."', ";
            $str_insert.="'".$str_country."', ";
            $str_insert.="'".$str_postalcode."', ";
            $str_insert.="'".$str_ipaddress."', ";
            $str_insert.="'".$str_phonenumber."', ";
            $str_insert.="'".$str_recurringdate."', ";
            $str_insert.="'".$str_recurringdate."', ";
            $str_insert.="'YES', 'YES', 'YES', 'YES' ";
            $str_insert.=")";
            //print "<br/><br/>INSERT: ".$str_insert.""; //exit;
            ExecuteQuery($str_insert);

	//}
	
            
	
	$mailbody = "";
	$str_subject = "";
	//if($rs_list->fields("paymentmode") == "PAYMENTPLAN") { 
	$str_subject="New member is registered on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
	$mailbody="New member is registered. Below are details.<br/><br/>";
        $mailbody.="<strong>Reistered On:</strong> ".$str_recurringdate."<br/>";
	$mailbody.="<strong>Subscription ID:</strong> ".$str_initial_subscription_id."<br/>";
        $mailbody.="<strong>Login ID:</strong> ".$str_username."<br/>";
	$mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL."";
        
        
        //print $str_subject."<br/><br/><br/>";
       // print $mailbody;exit;
        
	$fp=openXMLfile($UPLOAD_XML_CONTENT_PATH."siteconfiguration.xml");
	$STR_SITE_URL=getTagValue($STR_WEBSITE_NAME,$fp);	
	$str_from=getTagValue($STR_FROM_DEFAULT,$fp);
	//$str_to=getTagValue($STR_FROM_DEFAULT,$fp);
	closeXMLfile($fp);
	
	#For Testing:
	//sendmail("kra.rd.test@gmail.com",$str_subject,$mailbody,"kra.rd.test@gmail.com",1); // Sent to Customer / User
	//sendmail("kra.rd.test@gmail.com",$str_subject,$mailbody,"kra.rd.test@gmail.com",1); // Sent to Admin
	
	#For Real:
	//sendmail($str_to,$str_subject,$mailbody,$str_from,1); // Sent to Customer / User
	sendmail($str_from,$str_subject,$mailbody,$str_from,1); // Sent to Admin
	
?>
