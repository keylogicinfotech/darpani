<?php
include "../user/product_validatesession.php";
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_xml.php";
include "../includes/lib_file_upload.php";
include "../includes/lib_image.php";
include "../includes/lib_datetimeyear.php";
include "../user/product_config.php";
//include "./../includes/http_to_https.php";

#----------------------------------------------------------------------
$str_title_page = "";
$str_title_page = $STR_TITLE_PAGE_ORDER_MESSAGE;
//print $str_title_page; exit;

$str_xml_file_name_cms = "";
$str_xml_file_name_cms = "store_cms.xml";
#------------------------------------------------------------------------------------------------
# Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
# Get message type.
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($str_title_page) ;?></title>
    <?php //print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />   
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
    
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    
</head>
<body>
<?php include $STR_USER_HEADER_PATH; ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>           
            </div>
        </div>
        
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 ">
                <div  class="alert alert-success ">
                    <p align="center"><i class="fa fa-check"></i>
                        <?php 
		
                            print "&nbsp;Congratulations, your order is placed successfully.<br/>You will receive order details mail from site very soon. Please keep Transaction ID for this order for any future reference regarding this mail.<br/>Your order will soon be shipped.";
			            ?>    
                    </p>
                </div>
            </div>
        </div>
        
        <br/>
    </div>
    <div class="footer-bottom-margin">
        <?php include $STR_USER_FOOTER_PATH; CloseConnection();?>
    </div>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_promocode.js"></script>    
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_giftcard.js"></script>    
    <script>
        $(document).ready(function(){
            var defaultoption = $('input[name="rdo_payment_option"]:checked').val()
            //alert(defaultoption);
            if(defaultoption == 1)
            {
                $("#option1").show();
                $("#option2").hide();
                $("#option3").hide();
                $("#option4").hide();
                
            }
            else if(defaultoption == 2)
            {
                $("#option1").hide();
                $("#option2").show();
                $("#option3").hide();
                $("#option4").hide();
            }
            else if(defaultoption == 3)
            {
                $("#option1").hide();
                $("#option2").hide();
                $("#option3").show();
                $("#option4").hide();
            }
            else if(defaultoption == 4)
            {
                $("#option1").hide();
                $("#option2").hide();
                $("#option3").hide();
                $("#option4").show();
            }
        });
        
        
        
        
        $('input[type="radio"]').click(function()
        {
            if($(this).attr("name")=="rdo_payment_option")
            {
                var val= $(this).val();
                //alert(val);
                //var quantity = $('#txt_quantity').val();
                  //alert(quantity);

                //var rd_optionTWO = document.getElementById("rd_option2");
                if(val == 1)
                {
                    $("#option1").show();
                    $("#option2").hide();
                    $("#option3").hide();
                    $("#option4").hide();

                }
                else if(val == 2)
                {
                    $("#option1").hide();
                    $("#option2").show();
                    $("#option3").hide();
                    $("#option4").hide();
                }
                else if(val == 3)
                {
                    $("#option1").hide();
                    $("#option2").hide();
                    $("#option3").show();
                    $("#option4").hide();
                }
                else if(val == 4)
                {
                    $("#option1").hide();
                    $("#option2").hide();
                    $("#option3").hide();
                    $("#option4").show();
                }
                //document.getElementById("output").innerHTML = val;
                //document.getElementById("output1").innerHTML = val;

                //document.getElementById("rawclr").addClass("alert alert-warning");
                //temp=document.getElementById("temp");
                //temp.className="alert alert-warning";
                /*$('input[type="radio"]').click(function() {
                //alert("hi");
                        var selected = $(this).hasClass("alert alert-warning");
                        $("#data tr").removeClass("alert alert-warning");
                        if(!selected)
                                        $("#data tr").addClass("alert alert-warning");
                });*/
            }
        });
    </script>
</body>
</html>
