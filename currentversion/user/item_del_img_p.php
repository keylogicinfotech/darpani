<?php 
/*
File Name  :- item_del_img_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#-----------------------------------------------------------------------------------------
#Include files
include "./../includes/validatesession.php";
include "./../includes/configuration.php";
//include "item_config.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_file_system.php";
#-----------------------------------------------------------------------------------
#Get previous page values
$str_db_table_name = "t_freemember";
$int_pkid="";
$str_query_select = "";

if(isset($_GET["pkid"]))
{
    $int_pkid = trim($_GET["pkid"]);
}
//	print $int_pkid;exit;
//	if( $int_pkid == "" || !is_numeric($int_pkid) || $int_pkid < 0 )
//	{
//		CloseConnection();	  
//		Redirect("item_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid));
//		exit();
//	}
#Select query to get records from t_country table
$str_query_select = "";
$rs_list = "";				
$str_query_select = "SELECT name,imagefilename FROM " .$str_db_table_name. " WHERE pkid=".$int_pkid;
//	print $str_query_select;exit;
$rs_list = GetRecordSet($str_query_select);
if($rs_list->eof())
{
        CloseConnection();	  
        Redirect("item_edit.php?msg=F&type=E");
        exit();
}
$str_title=$rs_list->Fields("name");
#Get image name from database ...

$str_thumb_file=$rs_list->Fields("imagefilename");

//Delete image from server 
if(trim($str_thumb_file)!="")
{
    DeleteFile($UPLOAD_IMG_PATH.$str_thumb_file);
}

#Update the fields which store filename with null values
$str_query_update = "";
$str_query_update = "UPDATE " .$str_db_table_name. " SET imagefilename='' WHERE pkid=".$int_pkid;
//print $str_query_update;exit;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file

CloseConnection();	  
Redirect("user_personal_details.php?msg=DEL&type=S&pkid=".urlencode($int_pkid)."&tit=".urlencode(RemoveQuote($str_title)));
exit();	 
?>