<?php
/*
File Name  :- email_me.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
include "./../includes/lib_image.php";
include "./../includes/lib_email.php";
include "./../includes/http_to_https.php";
#------------------------------------------------------------------------------
$str_title_page_metatag = "PG_EMAIL";
$str_db_table_name = "t_page_metatag";
$str_title_page = "Email Me";
$str_xml_file_name_cat = "email_subject.xml";
$str_xml_file_name_cms = "email_cms.xml";
#----------------------------------------------------------------------
#read cat xml file
$str_xml_list_cat = "";
$str_xml_list_cat = readXML($STR_XML_FILE_PATH_MODULE . $str_xml_file_name_cat, "ROOT_ITEM");

#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS . $str_xml_file_name_cms, "ROOT_ITEM_CMS");
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
//print $str_desc_cms; exit;
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);

///--------------------------------------------------------------------
/// getting datas from module xmlfile.
///--------------------------------------------------------------------
#getting datas from module xmlfile.
//$result_contact="";
//$result_contact=readXML($STR_XML_FILE_PATH_MODULE."contact_subject.xml","ROOT_ITEM");

//$contact_content="";
//$contact_content=readXML($STR_XML_FILE_PATH_CMS.$str_xml_cms_file_name);
//print_r($contact_content[]['ITEMKEYVALUEDESCRIPTION']);exit;

///--------------------------------------------------------------------

# Get Page Title for SEO
$str_query_select = "";
$str_query_select = "SELECT titletag FROM " . $str_db_table_name . " WHERE visible='YES' AND pagekey='" . $str_title_page_metatag . "'";
//print $str_query_select;exit;
$rs_list = GetRecordset($str_query_select);
//$str_query_select="SELECT titletag FROM t_page_metatag WHERE visible='YES' AND pagekey='PG_MT_CONTACT'";
//print $str_query_select;exit;
//$rs_mt_list=GetRecordset($str_query_select);

$str_l_loginid = "";
$str_l_password = "";
$str_l_secretcode = "";
if (isset($_GET["l_loginid"])) {
    $str_l_loginid = MyHtmlEncode(RemoveQuote($_GET["l_loginid"]));
}
if (isset($_GET["l_password"])) {
    $str_l_password = MyHtmlEncode(RemoveQuote($_GET["l_password"]));
}
if (isset($_GET["l_secretcode"])) {
    $str_l_secretcode = MyHtmlEncode(RemoveQuote($_GET["l_secretcode"]));
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE); ?> : <?php print($rs_list->fields("titletag")); ?></title>
    <?php print(Display_Page_Metatag("PG_EMAIL")); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />
</head>
<style>
    .font-color {
        color: #fff;
    }
</style>

<body>
    <?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-md-12">
                <h2 align="right"><?php print $str_title_page; ?></h2>
            </div>
        </div>
        <hr />

        <?php if (strtoupper(trim($str_visible_cms)) == "YES") { ?>
            <?php if ($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
                <div class="row padding-10">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="breadcrumb">
                            <p align="justify"><?php print($str_desc_cms); ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?> <?php } ?>
        <div class="row padding-10">
            <div class="col-md-12">
                <form name="frm_add" id="frm_add" novalidate>
                    <?php /* ?>  <div class="control-group form-group">
                        <div class="controls input-group"> 
                            <span class="input-group-addon"><i class="fa fa-menu-hamburger"></i></span>
                            <select name="subject" id="subject" class="form-control" required data-validation-required-message="Please select category." >
                                <option value="">-- Select Category* --</option>
                                <?php
                                foreach($str_xml_list_cat as $key => $val) 
                                { 
                                 if(is_array($val)) {
                                if($str_sub==$str_xml_list_cat[$key]['PKID'])  { $str_selected="selected"; }
                                    else { $str_selected=""; } ?>
                                <option value="<?php print($str_xml_list_cat[$key]['PKID']);?>" <?php print($str_selected);?>>
                                    <?php print(MyHtmlEncode($str_xml_list_cat[$key]['TITLE']));?>
                                </option>
                            <?php }}?>
                            </select>
                        </div>
                        <p class="help-block"></p>
                    </div> <?php */ ?>
                    <div class="control-group form-group">
                        <div class="controls input-group">
                            <span class="input-group-addon"><i class="fa fa-user font-color"></i></span>
                            <input maxlength="100" name="name" id="name" class="form-control" required data-validation-required-message="<?php print $STR_PLACEHOLDER_FULLNAME; ?>" placeholder="<?php print $STR_PLACEHOLDER_FULLNAME; ?>" type="text">
                        </div>
                        <p class="help-block"></p>
                    </div>
                    <?php /* ?><div class="control-group form-group">
                        <div class="controls">
                            <label>Phone Number</label><span class="text-help"></span>
                            <input placeholder="Enter Phone Number" class="form-control" id="phone" type="tel" >
                        <div class="help-block"></div></div>
                    </div><?php */ ?>
                    <div class="control-group form-group">
                        <div class="controls input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope font-color"></i></span>
                            <input type="email" maxlength="100" name="email" id="email" class="form-control" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?>" required data-validation-required-message="<?php print $STR_PLACEHOLDER_EMAIL; ?>">
                        </div>
                        <p class="help-block"></p>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil font-color"></i></span>
                            <textarea name="message" id="message" rows="10" class="form-control font-color" data-validation-required-message="<?php print $STR_PLACEHOLDER_MESSAGE; ?>" placeholder="<?php print $STR_PLACEHOLDER_MESSAGE; ?>"></textarea>
                        </div>
                        <p class="help-block"></p>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil-square-o font-color"></i></span>
                            <input type="text" class="form-control font-color" id="secretcode" name="secretcode" required data-validation-required-message="<?php print $STR_PLACEHOLDER_SECRET_CODE; ?>" placeholder="<?php print $STR_PLACEHOLDER_SECRET_CODE; ?>" minlength="7" maxlength="7">
                            <span class="input-group-addon input-group-addon-no-padding nopadding">
                                <img src="./includes/get_unique_image.php" border="0" alt="Captcha" width="130" height="29">
                            </span>
                        </div>
                        <p class="help-block"></p>
                    </div>

                    <?php /* ?>    <div class="control-group form-group"><div class="controls input-group"><span class="input-group-addon"><i class="fa   fa-pencil-square-o"></i></span><input type="text" class="form-control" id="secretcode" name="secretcode" required data-validation-required-message="Please enter secret code from given image." placeholder="Secret Code *"  minlength="7" maxlength="7"  ><span class="input-group-addon input-group-addon-no-padding nopadding"><img src="<?php GetCurrentPathRespectToUser()?>./../includes/get_unique_image.php" border="0" alt="Captcha" width="130" height="29"></span></div><p class="help-block"></p></div> <?php */ ?>
                    <div id="success"></div>
                    <!-- For success/fail messages -->
                    <?php print DisplayFormButton("SEND", 0); ?><?php print DisplayFormButton("RESET", 0); ?>
                </form><br />
            </div>
        </div>
    </div>


    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>




    <!--<input type="submit" value="" />-->
    <?php // include "../includes/include_files_user.php"; 
    ?>
    <?php include($STR_USER_FOOTER_PATH);
    CloseConnection(); ?>
    <script language="JavaScript" src="./user/email_me.js" type="text/JavaScript"></script>
    <!--<script src="../../contact.js"></script>-->
    <!--<script src="<?php // print(GetCurrentPathRespectToUser());
                        ?>./contact.js"></script>-->
    <!--<script src="./../contact.js"></script>-->
</body>

</html>