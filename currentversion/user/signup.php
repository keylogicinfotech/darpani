<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
include "./../includes/http_to_https.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_MT_SIGNUP";
$str_db_table_name = "t_page_metatag";
$str_title_page = "";
$str_img_path = "";
$str_xml_file_name = "";
$str_xml_file_name_cms = "user_cms.xml";
#----------------------------------------------------------------------
#read main xml file
//$str_xml_list = "";
//$str_xml_list = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");

#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_REGISTRATION",$fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE",$fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table
$str_query_select = "";
$str_query_select = "SELECT titletag FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_query_select; exit;
$rs_list = GetRecordset($str_query_select);

$str_title_page = $rs_list->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($str_title_page); ?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="./css/user.css" rel="stylesheet">      
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-md-12">
                <h2 align="right"><?php print($str_title_page); ?></h2>          
            </div>
        </div>
        <hr/> 
             <?php if(strtoupper(trim($str_visible_cms)) == "YES"){ ?>
        <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
        <div class="row padding-10">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms);?></p></div>
            </div>
        </div>
	<?php } ?><?php } ?>
        <div class="row padding-10">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <form name="frm_signup" id="frm_signup" novalidate>
                <div class="control-group form-group">
                    <div class="controls">  
                        <label>Login Id</label><span class="help-text"> *</span>
                        <input type="email" class="form-control" id="l_loginid" name="l_loginid" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_EMAIL_FORMAT; ?>" placeholder="<?php print $STR_PLACEHOLDER_LOGINID; ?>" >
                        <?php /* ?><input type="text" class="form-control" id="l_loginid" name="l_loginid" pattern="^[a-zA-Z0-9]+$" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_LOGINID; ?>" value="" placeholder="<?php print $STR_PLACEHOLDER_LOGINID; ?> | <?php print $STR_MSG_LOGINID_FORMAT;?>" minlength="4" maxlength="20"><?php */ ?>
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Password</label><span class="help-text"> *</span>
                        <input type="password" class="form-control" id="l_password" name="l_password" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_PASSWORD; ?>" placeholder="<?php print $STR_PLACEHOLDER_PASSWORD; ?> | <?php print $STR_MSG_PASSWORD_FORMAT;?>" minlength="6" maxlength="20"> 
                       <p class="help-block"></p> 
                    </div>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Confirm Password</label><span class="help-text"> *</span>
                        <input type="password" class="form-control" id="l_conf_password" name="l_conf_password" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_CONFIRM_PASSWORD; ?>" data-validation-matches-message="<?php print $STR_MSG_ACTION_INVALID_CONFIRM_PASSWORD; ?>" data-validation-matches-match="l_password" placeholder="<?php print $STR_PLACEHOLDER_CONFIRM_PASSWORD; ?> | <?php print $STR_MSG_PASSWORD_FORMAT;?>" minlength="6" maxlength="20">
                        <p class="help-block"></p>
                    </div>
                </div>
                <?php /* ?><div class="control-group form-group">
                    <div class="controls">
                        <label>Email</label><span class="help-text"> </span>
                        <input type="email" class="form-control" id="l_email" name="l_email" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?>" >
                        <p class="help-block"></p>
                    </div>
                </div><?php */ ?>
                <div id="success"></div>
                <?php print DisplayFormButton("SIGNUP",0); ?>&nbsp;<?php print DisplayFormButton("RESET",0); ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Already an account? <a href="./signin">Login</a><br><br>
            </form>
        </div>        
    </div>
        <br/>
    </div>
    <script language="JavaScript" src="./js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH);CloseConnection();?>
    <script language="JavaScript" src="./js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="./js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print GetCurrentPathRespectToUser(); ?>signup.js"></script>    
</body>
</html>
