<?php /*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();
set_time_limit(150);
include "./../user/product_validatesession.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_xml.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/lib_datetimeyear.php";
include "./../user/product_config.php";
//include "./../includes/http_to_https.php";	
//print_r($_POST);exit;
#----------------------------------------------------------------------
# posted variables...
$item_sessionid="";
$item_uniqueid="";
if(isset($_POST["hdn_sessionid"]) && trim($_POST["hdn_sessionid"])!="")
{
        $item_sessionid=trim($_POST["hdn_sessionid"]);
}

if(isset($_POST["hdn_uniqueid"]) && trim($_POST["hdn_uniqueid"])!="")
{
        $item_uniqueid=trim($_POST["hdn_uniqueid"]);
}

$int_giftcard_remove_amount = 0;
if(isset($_POST["txt_giftcard_remove_amt"]) && $_POST["txt_giftcard_remove_amt"] != "")
{
    $int_giftcard_remove_amount = $_POST["txt_giftcard_remove_amt"];
}


$int_userpkid = 0;
if(isset($_POST["hdn_userpkid"]) && $_POST["hdn_userpkid"] != "")
{
    $int_userpkid = $_POST["hdn_userpkid"];
}

if($int_userpkid == 0) 
{
    $response['status']='ERR';
    $response['message']="Error... Please login to remove your applied gift card amount.";
    echo json_encode($response);
    return;
}




$str_where = "";
$str_where = " WHERE sessionid='".$item_sessionid."' AND uniqueid='".$item_uniqueid."' ";

$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_SESSION_CART." SET giftcard = 0".$str_where ;        
//print $str_query_update; 
ExecuteQuery($str_query_update);

$str_query_select = "";
$str_query_select = "SELECT conversionrate FROM ".$STR_DB_TABLE_NAME_COUNTRY." WHERE pkid=".$_SESSION["usr_cntry"];
$rs_list_conversionrate = GetRecordSet($str_query_select);

//print $rs_list_conversionrate->Fields("conversionrate"); exit;
$int_giftcard_remove_amount = $int_giftcard_remove_amount * $rs_list_conversionrate->Fields("conversionrate");
//print $int_giftcard_remove_amount; exit;
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_USER." SET giftcard = giftcard+".$int_giftcard_remove_amount." WHERE pkid=".$int_userpkid ;        
//print $str_query_update; exit; 
ExecuteQuery($str_query_update);

    

$response['status']='SUC';
$response['message']="Success... Gift Card Removed.";
echo json_encode($response);
return;

?>