<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/lib_xml.php";
include "./../includes/http_to_https.php";	
include "./../includes/unique_site_view_count.php";
include "./webmaster_config.php";
#----------------------------------------------------------------------
#check whether pkid has arrived properly or not.
$int_promopkid="";
if(isset($_GET["pkid"]))
{
    $enc_id = trim($_GET["pkid"]);
    $int_promopkid = GetDecryptId($enc_id);
}	
	
if($int_promopkid==trim(""))
{	
    CloseConnection();
    redirect("webmaster_hostedgallery.php");
    exit();
}
#---------------------------------------------------	
##get Affiliate ID Data
$str_aid="";
if(isset($_GET["aid"])==true)
{
    $str_aid=trim($_GET["aid"]);
}
else
{
	// Get cookies value when this page is accessed by putting link in browser directly
    $str_aid = $_COOKIE["affid"];
}	
#---------------------------------------------------
#get data from database.
$str_query_select = "";
$str_query_select = "SELECT title,createdate,description,imagefilename,urltitletop,urltitlebottom,noofvisiblephoto";
$str_query_select = $str_query_select." FROM " .$STR_DB_TABLE_NAME_GALLERY. " WHERE pkid=".$int_promopkid." and visible='YES'";
$rs_list_details = GetRecordset($str_query_select);	

$str_query_select = "";
$str_query_select = "SELECT photopkid,imagefilenamethumb, imagefilenamelarge";
$str_query_select=$str_query_select." FROM " .$STR_DB_TABLE_NAME_PHOTO;	
$str_query_select=$str_query_select." WHERE pkid=".$int_promopkid." and visible='YES' ORDER BY displayorder";	
$rs_list=GetRecordset($str_query_select);	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_TOOLS);?> : <?php print($STR_TITLE_GALLERY);?> : <?php print($STR_TITLE_GALLERY_VIEW);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<?php // include "./includes/cssfont.php";?>

</head>
<body>
<?php include($STR_USER_HEADER_PATH);?>
<div class="container center_bg">
    <div class="row padding-10">
        <div class="col-lg-12 col-md-12 col-lg-12">
            <?php /*?><h1 class="text-primary page-header" align="right">Promo1</h1><?php */?>
            <?php 
                if($rs_list_details->fields("imagefilename")!="")
                {
                    $str_image=$STR_UPLOAD_PATH_PROMOGALLERY . $int_promopkid ."/". $rs_list_details->fields("imagefilename");
                }
                else
                {
                    $str_image=$STR_UPLOAD_PATH_PROMOGALLERY . "default_promo_gallery_header.jpg";
                }

            ?><br/>
            <img src="<?php print $str_image; ?>" class="img-responsive" width="100%" title="Header Image" alt="Header Image">
            <?php //print $str_image; ?>

            <?php /*?><h3 class="page-header text-info"><b><?php print(MyHtmlEncode($rs_list_details->fields("title"))); ?></b>&nbsp;<span class="text-help">(<?php print($rs_list_details->fields("noofvisiblephoto")); ?> images | posted on <?php print(DDMMMYYYYFormat($rs_list_details->fields("createdate"))); ?>)</span></h3>
                            <?php if($rs_list_details->fields("description")!="") { ?>
            <p class="content06_bg DescriptionText" align="justify">
               <?php print($rs_list_details->fields("description"));?>
            </p>
                            <?php } ?><?php */?>
        </div>
    </div>
    <br/>
    <div class="row padding-10">
        <div class="col-lg-12 col-md-12 col-lg-12" align="left">
            <h3 class="nopadding"><?php print(MyHtmlEncode($rs_list_details->fields("title"))); ?>&nbsp;<span class="text-help">(<?php print($rs_list_details->fields("noofvisiblephoto")); ?> images | Posted on <?php print(DDMMMYYYYFormat($rs_list_details->fields("createdate"))); ?>)</span></h3>
        </div>
    </div>
    <hr/>
    <div class="row padding-10">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php if($rs_list_details->fields("description")!="") { ?>
                <p class="text-help " align="justify">
                    <?php print($rs_list_details->fields("description"));?>
                </p>
            <?php } ?>
        </div>
    </div>
    <hr/>
    <!-- /.row -->
    <div class="row padding-10">
        <div class="col-lg-12 col-md-12 col-lg-12" align="center">
            <?php $enc = GetEncryptId($int_promopkid); ?>
            <h3>  <?php print("".DisplayWebSiteUrl("http://refer.ccbill.com/cgi-bin/clicks.cgi?CA=".$STR_CCBILL_MEMBER_WEBMASTER_CLIENT_ACCOUNT."&PA=".$str_aid."&HTML=".$STR_SITENAME_WITH_PROTOCOL."?pkid=".$enc,$rs_list_details->fields("urltitletop"),"","link","Click to visit site","",$STR_PROMO_GALLERY_DEFAULT_URL)."");?></h3>
        </div>
    </div>
    <br/>
        <!-- Projects Row -->
    <?php 
    if($rs_list->EOF()) { ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-lg-12">Currently no images available.</div>
        </div>
    <?php } else { ?>
        <div class="row padding-10">
        <?php 
            $int_count=0;
            while(!$rs_list->EOF()){
          ?>
        <div class="col-lg-3 col-md-3  col-lg-3" align="center">
            <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($int_count); ?>">
                <img class="img-responsive" width="100%" src="<?php print($STR_UPLOAD_PATH_PROMOGALLERY .$int_promopkid."/". $rs_list->fields("imagefilenamethumb"));?>" alt="Promo Gallery Image" title="Promo Gallery Image">
            </a>
            <div class="modal fade f-pop-up-<?php print($int_count); ?> " align="center" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-<?php print($int_featured_color_cnt); ?>" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <img src="<?php print($STR_UPLOAD_PATH_PROMOGALLERY .$int_promopkid."/". $rs_list->fields("imagefilenamelarge"));?>" class="img-responsive img-rounded" alt="Photo galllery header image">
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
        </div>
        <?php
        $rs_list->MoveNext();
        $int_count++;
        if(($int_count % 4)==0 && !($rs_list->EOF()))	
        {?>
            </div>
            <hr/>
            <div class="row padding-10">	
        <?php }	

           } ?>
          </div>		
      <?php } ?>
          <br/>
          <div class="row padding-10">
              <div class="col-lg-12 col-md-12 col-lg-12" align="center">
                  <?php $enc = GetEncryptId($int_promopkid); ?>
                  <h3><?php print("".DisplayWebSiteUrl("http://refer.ccbill.com/cgi-bin/clicks.cgi?CA=".$STR_CCBILL_MEMBER_WEBMASTER_CLIENT_ACCOUNT."&PA=".$str_aid."&HTML=".$STR_SITENAME_WITH_PROTOCOL."?pkid=".$enc,$rs_list_details->fields("urltitlebottom"),"","","Click to visit site","",$STR_PROMO_GALLERY_DEFAULT_URL)."");?></h3>
              </div>
          </div>
          <br/>
    </div>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <?php include "./../includes/include_files_user.php"; ?>
    <script language="javascript" src="./includes/preloadlibrary.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/JavaScript" src="./webmaster_galleryview.js"></script>		
</body>
</html>
