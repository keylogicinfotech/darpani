//---------------------------------------------------------------------------------------------------------
///	Module Name:- Manage Change model Password
///	File Name  :- mdl_change_password.php
///	Create Date:- 17-JUL-2007
///	Intially Create By :- 
///	Update History:
//---------------------------------------------------------------------------------------------------------
/*  
Jquery Validation using jqBootstrapValidation
 example is taken from jqBootstrapValidation docs 
*/
$(function() {
//    alert("error");
    $("#frm_change_password input").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // something to have when submit produces an error ?
            // Not decided if I need it yet
        },
        submitSuccess: function($form, event) { 
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var txt_old_password = $("input#txt_old_password").val();
            var txt_new_password = $("input#txt_new_password").val();
            var txt_confirm_password= $("input#txt_confirm_password").val();
            var hdn_mid= $("input#hdn_mid").val();
			
            $.ajax({
                url: "user_change_password_p.php",
                type: "POST",
                data:   {
                        txt_old_password: txt_old_password,
                        txt_new_password: txt_new_password,
                        txt_confirm_password: txt_confirm_password,
                        hdn_mid: hdn_mid
                        },
				//dataType: "json",
                cache: false,
                success: function(data) 
                {
                        //alert(data);
                        var $ResponseText_L=JSON.parse(data);

                        if($ResponseText_L.status == 'ERR')
                        {
                                // Fail message
                                $('#success').html("<div class='alert alert-danger'>");
                                $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                        .append("</button>");
                                $('#success > .alert-danger').append("<strong> " + $ResponseText_L.message + " ");
                                $('#success > .alert-danger').append('</div>');
                                
                                
                                
                        }
                        else if($ResponseText_L.status == 'SUC')
                        {
                                // Success message

                                //window.location.href = "./user/mdl_home.php";

                                //window.location.href = "./user/postabill_home.php";
                                $('#success').html("<div class='alert alert-success'>");
                                $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                .append("</button>");
                                $('#success > .alert-success').append("<strong> " + $ResponseText_L.message + " ");
                                $('#success > .alert-success').append('</div>');
                                
                                setTimeout(function(){ location.reload(); }, 2000);
                                //window.location.href = "./mdl_home.php";
                                //clear all fields
                                //$('#FormPLogin').trigger("reset");
                        }
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});

/*When clicking on Full hide fail/success boxes */
$('#l_name').focus(function() {
    $('#success').html('');
});
