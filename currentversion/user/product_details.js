/*
  Jquery Validation using jqBootstrapValidation
   example is taken from jqBootstrapValidation docs 
  */
var flg_pressed_btn; // global variable declared so it can be avaiable on entire page and inside all functions
$(function () {
  // This function is used to get which button is pressed on PHP page when FROM submitted
  $(document).ready(function () {
    $("button").click(function () {
      flg_pressed_btn = $(this).attr("id"); // Assigned pressed button ID to variable
      // $(".color").attr("checked", true); // for without user login color checked
      //   alert(flg_pressed_btn);
    });
  });

  $("#frm_add input, #frm_add select").jqBootstrapValidation({
    preventSubmit: true,
    submitError: function ($form, event, errors) {
      // something to have when submit produces an error ?
      // Not decided if I need it yet
    },

    submitSuccess: function ($form, event) {
      event.preventDefault(); // prevent default submit behaviour
      // get values from FORM
      var hdn_prodpkid = $("input#hdn_prodpkid").val();
      var hdn_prodtitle = $("input#hdn_prodtitle").val();
      var hdn_itemcode = $("input#hdn_itemcode").val();
      var hdn_catpkid = $("input#hdn_catpkid").val();
      var hdn_subcatpkid = $("input#hdn_subcatpkid").val();
      //var hdn_memberpkid = $("input#hdn_memberpkid").val();
      //var hdn_memberid = $("input#hdn_memberid").val();
      var hdn_weight = $("input#hdn_weight").val();

      var hdn_userpkid = $("input#hdn_userpkid").val();
      var hdn_username = $("input#hdn_username").val();

      var hdn_discount = $("input#hdn_discount").val();
      var hdn_referredbypkid = $("input#hdn_referredbypkid").val();
      var hdn_commission = $("input#hdn_commission").val();

      var rdo_color = $("input[name='rdo_color']:checked").val();
      var rdo_size = $("input[name='rdo_size']:checked").val();

      var rdo_tailoringoption = $(
        "input[name='rdo_tailoringoption']:checked"
      ).val();
      var cbo_semistitched_stitchingsize = $(
        "select#cbo_semistitched_stitchingsize"
      ).val();

      //Blouse
      var txt_blouse_length = $("input#txt_blouse_length").val();
      var txt_blouse_sleeve_length = $("input#txt_blouse_sleeve_length").val();
      var cbo_blouse_aroundbust = $("select#cbo_blouse_aroundbust").val();
      var txt_blouse_abovearoundwaist = $(
        "input#txt_blouse_abovearoundwaist"
      ).val();
      var txt_blouse_frontneckdepth = $(
        "input#txt_blouse_frontneckdepth"
      ).val();
      var txt_blouse_backneckdepth = $("input#txt_blouse_backneckdepth").val();

      //Lehenga
      var txt_lehenga_aroundwaist = $("input#txt_lehenga_aroundwaist").val();
      var txt_lehenga_aroundhips = $("input#txt_lehenga_aroundhips").val();
      var txt_lehenga_length = $("input#txt_lehenga_length").val();

      //Salwar
      var txt_salwar_yourheight = $("input#txt_salwar_yourheight").val();
      var txt_salwar_frontneckdepth = $(
        "input#txt_salwar_frontneckdepth"
      ).val();
      var txt_salwar_bottomlength = $("input#txt_salwar_bottomlength").val();
      var txt_salwar_kameezlength = $("input#txt_salwar_kameezlength").val();
      var txt_salwar_sleevelength = $("input#txt_salwar_sleevelength").val();
      var cbo_salwar_sleevestyle = $("select#cbo_salwar_sleevestyle").val();
      var cbo_salwar_aroundbust = $("select#cbo_salwar_aroundbust").val();
      var txt_salwar_abovearoundwaist = $(
        "input#txt_salwar_abovearoundwaist"
      ).val();
      var txt_salwar_aroundwaist = $("input#txt_salwar_aroundwaist").val();
      var txt_salwar_aroundhips = $("input#txt_salwar_aroundhips").val();
      var txt_salwar_backneckdepth = $("input#txt_salwar_backneckdepth").val();
      var txt_salwar_aroundthigh = $("input#txt_salwar_aroundthigh").val();
      var txt_salwar_aroundknee = $("input#txt_salwar_aroundknee").val();
      var txt_salwar_aroundcalf = $("input#txt_salwar_aroundcalf").val();
      var txt_salwar_aroundankle = $("input#txt_salwar_aroundankle").val();

      /*var cbo_customized_aroundbust = $("select#cbo_customized_aroundbust").val();
            var txt_customized_above_waist = $("input#txt_customized_above_waist").val();
            var txt_customized_around_hips = $("input#txt_customized_around_hips").val();
            var txt_customized_backneckdepth = $("input#txt_customized_backneckdepth").val();
            var txt_customized_backneckdepth = $("input#txt_customized_backneckdepth").val();
            var txt_customized_backneckdepth = $("input#txt_customized_backneckdepth").val();
            var txt_customized_backneckdepth = $("input#txt_customized_backneckdepth").val();*/

      //var hdn_mdlpkid = $("input#hdn_mdlpkid").val();
      //var hdn_modelname = $("input#hdn_modelname").val();
      //var hdn_pricepkid = $("input#hdn_pricepkid").val();
      var hdn_price = $("input#hdn_price").val();
      //alert(hdn_price);
      var txt_quantity = $("input#txt_quantity").val();
      //var cbo_color = $("select#cbo_color").val();
      //var cbo_size = $("select#cbo_size").val();
      //var cbo_type = $("select#cbo_type").val();
      //var hdn_ishippingvalue = $("input#hdn_ishippingvalue").val();
      //var hdn_dshippingvalue = $("input#hdn_dshippingvalue").val();
      //var hdn_ishippinginfo = $("input#hdn_ishippinginfo").val();
      //var hdn_dshippinginfo = $("input#hdn_dshippinginfo").val();
      var hdn_description = $("input#hdn_description").val();
      var specssku = $("input#specssku").val();
      //alert(arr_payment_method);
      var rdo_shipping = $("input[id='rdo_shipping']:checked").val();

      $.ajax({
        url: "../../../user/product_addtocart_p.php",
        type: "POST",
        data: {
          //flg_pressed_btn: flg_pressed_btn,

          hdn_prodpkid: hdn_prodpkid,
          hdn_prodtitle: hdn_prodtitle,
          hdn_itemcode: hdn_itemcode,
          hdn_catpkid: hdn_catpkid,
          hdn_subcatpkid: hdn_subcatpkid,
          //hdn_memberpkid: hdn_memberpkid,
          //hdn_memberid: hdn_memberid,
          hdn_weight: hdn_weight,
          hdn_userpkid: hdn_userpkid,
          hdn_username: hdn_username,

          hdn_discount: hdn_discount,
          hdn_referredbypkid: hdn_referredbypkid,
          hdn_commission: hdn_commission,

          rdo_color: rdo_color,
          rdo_size: rdo_size,

          rdo_tailoringoption: rdo_tailoringoption,
          cbo_semistitched_stitchingsize: cbo_semistitched_stitchingsize,

          txt_blouse_length: txt_blouse_length,
          txt_blouse_sleeve_length: txt_blouse_sleeve_length,
          cbo_blouse_aroundbust: cbo_blouse_aroundbust,
          txt_blouse_abovearoundwaist: txt_blouse_abovearoundwaist,
          txt_blouse_frontneckdepth: txt_blouse_frontneckdepth,
          txt_blouse_backneckdepth: txt_blouse_backneckdepth,

          txt_lehenga_aroundwaist: txt_lehenga_aroundwaist,
          txt_lehenga_aroundhips: txt_lehenga_aroundhips,
          txt_lehenga_length: txt_lehenga_length,

          txt_salwar_yourheight: txt_salwar_yourheight,
          txt_salwar_frontneckdepth: txt_salwar_frontneckdepth,
          txt_salwar_bottomlength: txt_salwar_bottomlength,
          txt_salwar_kameezlength: txt_salwar_kameezlength,
          txt_salwar_sleevelength: txt_salwar_sleevelength,
          cbo_salwar_sleevestyle: cbo_salwar_sleevestyle,
          cbo_salwar_aroundbust: cbo_salwar_aroundbust,
          txt_salwar_abovearoundwaist: txt_salwar_abovearoundwaist,
          txt_salwar_aroundwaist: txt_salwar_aroundwaist,
          txt_salwar_aroundhips: txt_salwar_aroundhips,
          txt_salwar_backneckdepth: txt_salwar_backneckdepth,
          txt_salwar_aroundthigh: txt_salwar_aroundthigh,
          txt_salwar_aroundknee: txt_salwar_aroundknee,
          txt_salwar_aroundcalf: txt_salwar_aroundcalf,
          txt_salwar_aroundankle: txt_salwar_aroundankle,

          //hdn_mdlpkid: hdn_mdlpkid,
          //hdn_modelname: hdn_modelname,
          //hdn_pricepkid: hdn_pricepkid,
          hdn_price: hdn_price,
          txt_quantity: txt_quantity,
          //cbo_color: cbo_color,
          //cbo_size: cbo_size,
          //cbo_type: cbo_type,
          //hdn_ishippingvalue: hdn_ishippingvalue,
          //hdn_dshippingvalue: hdn_dshippingvalue,
          hdn_description: hdn_description,
          specssku: specssku,
          rdo_shipping: rdo_shipping,
          //hdn_ishippinginfo: hdn_ishippinginfo,
          //hdn_dshippinginfo: hdn_dshippinginfo
        },
        cache: false,

        success: function (data) {
          //alert(data);
          var $responseText = JSON.parse(data);
          if ($responseText.status == "SUC") {
            // If BUY NOW button is pressed then add record into cart and redirect to defined page
            if (flg_pressed_btn == "btn_buy_now") {
              window.location.href = "../../../user/product_address.php";
            } else {
              // If ADD TO CART button is pressed then add record into cart and display success message
              $("#success").html("<div class='alert alert-success'>");
              $("#success > .alert-success")
                .html(
                  "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;"
                )
                .append("</button>");
              $("#success > .alert-success").append(
                "<strong> " + $responseText.message + " </strong>"
              );
              $("#success > .alert-success").append("</div>");

              //clear all fields
              // $("#frm_add").trigger("reset"); // for last checked color value

              $("#success").show();
              setTimeout(function () {
                $("#success").hide();
              }, 2000);
            }
          } else if ($responseText.status == "ERR") {
            // Fail message
            $("#success").html("<div class='alert alert-danger'>");
            $("#success > .alert-danger")
              .html(
                "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;"
              )
              .append("</button>");
            $("#success > .alert-danger").append(
              "<strong> " + $responseText.message + " "
            );
            $("#success > .alert-danger").append("</div>");
          }
        },

        /*success: function() {
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Your contact inquiry has been submitted, we will contact you Soon. Thank you for communication.</strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + " it seems that my mail server is not responding...</strong> Could you please email me directly to <a href='mailto:info@websitename.com?Subject=Message_Me from websitename.com;>info@websitename.com</a> ? Sorry for the inconvenience!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },*/
      });
    },
    filter: function () {
      return $(this).is(":visible");
    },
  });

  $('a[data-toggle="tab"]').click(function (e) {
    e.preventDefault();
    $(this).tab("show");
  });
});

/*When clicking on Full hide fail/success boxes */
$("#name").focus(function () {
  $("#success").html("");
});
