<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/lib_file_system.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_SITEMAP";
$str_img_path = "";
$str_db_table_name_mt = "t_page_metatag";
$str_db_table_name = "";
$str_db_table_name_cat = "";
$str_xml_file_name = "";
$str_xml_file_name_cms = "";
#----------------------------------------------------------------------
#read main xml file
#----------------------------------------------------------------------
#get metatag page title from table

$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_mt. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
            </div>
        </div>
        <div class="row padding-10">
            <div class="col-md-6">
                <h3>FREE AREA</h3>
                <ul>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/" title="">Home</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/about" title="">About Me</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/book-me" title="">Book Me</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/diary" title="">Diary</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/event" title="">Events</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/latest-updates" title="">Updates</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/news" title="">News</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/photo-album" title="">Member Gallery</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/campone" title="">Phone / Cam Sessions</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/videochat-room" title="">LIVE chat/cam room</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/video" title=""> Members Videos</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/download" title="">Downloads</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/online-store" title="">Kinky Store</a></li>
                    <li><a href="http://mistressalexiajordon.stockroom.com/" title="">Fetish Shop</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/fourm" title="">Forum</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/testimonial" title="">Praise</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/email-me" title="">Email Me</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/blog" title="">Blog</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/wishlist" title="">Wish List</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/banner" title="">Banners</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/link" title="">Links</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/" title="">Newsletter Signup</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/affiliate" title="">Affiliate Program</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/" title="">Refer this website to a friend</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/terms" title="">Terms & Conditions</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/legal" title="">18 U.S.C. 2257 Record-Keeping Requirements Compliance Statement</a></li>
                </ul>
            </div> 
            <div class="col-md-6">
                <h3>VIP MEMBER AREA</h3>
                <ul>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/member-home" title="">VIP Member Home</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/speacial-series-album" title="">VIP Member Photos / Special Series Photos</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/email-me" title="">Email Me</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/" title="">VIP Member Login</a></li>
                    <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/ " title="">VIP Member JOIN NOW</a></li> 
                </ul>
            </div> 
        </div>
        <br/>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/banner.js"></script>
</body>
</html>
