$(function() {
    $("#frm_forgot_password input").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // something to have when submit produces an error ?
            // Not decided if I need it yet
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM

            var loginid = $("input#loginid").val();
            //alert(loginid);
		
            $.ajax({
                
                url: "./user/forgot_password_p.php",
                type: "POST",
                data: {
                    loginid: loginid
                },
                cache: false,
                success: function(data) 
                {
                    alert(data);
                    var $responseText = JSON.parse(data);
                    if($responseText.status == 'SUC')
//                                        if(($responseText.status).equal("SUC"))
                    {
                        // Success message
//                        alert(data);
                        
                        $('#success').html("<div class='alert alert-success'>");
                        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                        $('#success > .alert-success').append("<strong> " + $responseText.message + " </strong>");
                        $('#success > .alert-success').append('</div>');
                        
                        setTimeout(function(){ window.location.href="./user-login"; }, 5000);
                        
                    }
                    else if($responseText.status == 'ERR')
                    {
                        // Fail message
                        $('#success').html("<div class='alert alert-danger'>");
                        $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                .append("</button>");
                        $('#success > .alert-danger').append("<strong> " + $responseText.message + " ");
                        $('#success > .alert-danger').append('</div>');
                        
                    }
                },
	
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});
/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
