<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files

include "../user/product_validatesession.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
include "./../user/product_config.php";

#----------------------------------------------------------------------
$str_title_page_metatag = "PG_STORE";
$str_db_table_name_metatag = "t_page_metatag";

$str_xml_file_name = "";
$str_xml_file_name_cat = "";

$str_xml_file_name_cms = "store_cms.xml";
$int_records_per_page = 1;
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_GET);

if(isset($_GET['cid']))
{ 
    $str_category_without_chars = RemoveSpecialCharsFromString(str_replace('_slash_', '/', (str_replace('_and_', '&', strtolower(trim($_GET['cid']))))));  
    $str_category_with_hyphen = trim($_GET['cid']);
}


$str_seo_title = "";
$str_seo_keyword = "";
$str_seo_desc = "";

$int_cat_pkid = 0;
$str_category = "";
$str_category_seo_title = "";
$str_category_seo_keyword = "";
$str_category_seo_desc = "";
//$str_category_desc = "";
$str_query_select = "";
$str_query_select = "SELECT catpkid, title, description, seotitle, seokeyword, seodescription FROM ".$STR_DB_TABLE_NAME_CAT." WHERE REPLACE(REPLACE(LOWER(title),' ',''),'-','')='".$str_category_without_chars."'";
//print $str_query_select; exit;
$rs_list_cat=GetRecordSet($str_query_select);
if($rs_list_cat->EOF()!=true)
{
    $str_category = $rs_list_cat->fields("title");
    $str_category_desc = $rs_list_cat->fields("description");
    $int_cat_pkid = $rs_list_cat->fields("catpkid");
    $str_category_seo_title = $rs_list_cat->fields("seotitle");
    $str_category_seo_keyword = $rs_list_cat->fields("seokeyword");
    $str_category_seo_desc = $rs_list_cat->fields("seodescription");
    //print $str_category_desc;exit;
}
else
{
    CloseConnection();
    Redirect("./");
    exit();
}

$str_subcategory_without_chars = "";
$str_subcategory_with_hyphen = "";
if(isset($_GET['sid']) && $_GET['sid']!= "")
{ 
    $str_subcategory_without_chars = RemoveSpecialCharsFromString(str_replace('_slash_', '/', (str_replace('_and_', '&', strtolower(trim($_GET['sid']))))));  
    $str_subcategory_with_hyphen = trim($_GET['sid']);
}

$int_sub_cat_pkid=0;	
$str_sub_category = "";
$str_sub_category_seo_title = "";
$str_sub_category_seo_keyword = "";
$str_sub_category_seo_desc = "";
//$str_sub_category_desc = "";

$str_query_select = "";
$str_query_select = "SELECT subcatpkid, subcattitle, description, seotitle, seokeyword, seodescription FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE REPLACE(REPLACE(LOWER(subcattitle),' ',''),'-','')='".$str_subcategory_without_chars."' AND catpkid = ".$int_cat_pkid." AND visible='YES'";
//print $str_query_select; //exit;
$rs_list_subcat = GetRecordSet($str_query_select);
if($rs_list_subcat->EOF()!=true)
{
    $str_sub_category = $rs_list_subcat->fields("subcattitle");
    //print $str_sub_category;exit;
    $int_sub_cat_pkid = $rs_list_subcat->fields("subcatpkid");
    $str_sub_category_desc = $rs_list_subcat->fields("description");
    $str_sub_category_seo_title = $rs_list_subcat->fields("seotitle");
    $str_sub_category_seo_keyword = $rs_list_subcat->fields("seokeyword");
    $str_sub_category_seo_desc = $rs_list_subcat->fields("seodescription");
}

if(isset($_GET['cid']) && isset($_GET['sid']) && $_GET['sid']!="")
{
    $str_seo_title = $str_sub_category_seo_title;
    $str_seo_keyword = $str_sub_category_seo_keyword;
    $str_seo_desc = $str_sub_category_seo_desc;
}
else
{
    $str_seo_title = $str_category_seo_title;
    $str_seo_keyword = $str_category_seo_keyword;
    $str_seo_desc = $str_category_seo_desc;
}





$str_color = "";
if(isset($_GET['clr']) && $_GET['clr']!="")
{
    $str_color = strtolower($_GET['clr']);
}

$str_fabric = ""; // OR Material
if(isset($_GET['mtrl']) && $_GET['mtrl']!="")
{
    $str_fabric = RemoveSpecialCharsFromString(str_replace('_slash_', '/', (str_replace('_and_', '&', $_GET['mtrl']))));
}

$str_type = ""; // OR Material
if(isset($_GET['type']) && $_GET['type']!="")
{
    $str_type = RemoveSpecialCharsFromString(str_replace('_slash_', '/', (str_replace('_and_', '&', $_GET['type']))));
}

$str_work = ""; // OR Material
if(isset($_GET['work']) && $_GET['work']!="")
{
    $str_work = RemoveSpecialCharsFromString(str_replace('_slash_', '/', (str_replace('_and_', '&', $_GET['work']))));
}
$str_price = ""; // OR Material
$int_price = 0.00;
if(isset($_GET['price']) && $_GET['price']!="")
{
    $str_price = str_replace('_', '.', $_GET['price']);
    
    $str_query_select = "";
    $str_query_select = "SELECT * FROM t_store_pricerange WHERE pkid='".$str_price."' AND visible='YES'";
    //print $str_query_select;
    $rs_list_price_range = GetRecordSet($str_query_select); 
    //print $rs_list_price_range->Fields("fromamount");
}
//print $str_price;

//print_r($_GET);
$str_where = "";
$str_where_for_filter = "";
if(isset($_GET['cid']) && isset($_GET['sid']) && $_GET['sid']!="")
{ 
    $str_where = " AND a.catpkid = ".$int_cat_pkid. " AND a.subcatpkid = ".$int_sub_cat_pkid." ";
    $str_where_for_filter = " AND a.catpkid = ".$int_cat_pkid. " AND a.subcatpkid = ".$int_sub_cat_pkid." ";
    //print $str_where; exit;
}
else if(isset($_GET['cid']) && $str_color != "")
{ 
    $str_where = " AND a.catpkid = ".$int_cat_pkid." AND LOWER(f.title)='".$str_color."' ";
    //print $str_where; exit;
}
else if(isset($_GET['cid']) && $str_fabric != "")
{ 
    $str_where = " AND a.catpkid = ".$int_cat_pkid." AND REPLACE(REPLACE(LOWER(h.title),' ',''),'-','')='".strtolower($str_fabric)."' ";
    //print $str_where; exit;
}
else if(isset($_GET['cid']) && $str_type != "")
{ 
    $str_where = " AND a.catpkid = ".$int_cat_pkid." AND REPLACE(REPLACE(LOWER(j.title),' ',''),'-','')='".strtolower($str_type)."' ";
    //print $str_where; exit;
}
else if(isset($_GET['cid']) && $str_work != "")
{ 
    $str_where = " AND a.catpkid = ".$int_cat_pkid." AND REPLACE(REPLACE(LOWER(l.title),' ',''),'-','')='".strtolower($str_work)."' ";
    //print $str_where; exit;
}
else if(isset($_GET['cid']) && $str_price != "")
{ 
    $str_where = " AND a.catpkid = ".$int_cat_pkid." AND a.ourprice BETWEEN ".$rs_list_price_range->Fields("fromamount")." AND ".$rs_list_price_range->Fields("toamount"). "";
    //print $str_where; exit;
}
else if(isset($_GET['cid'])) {
    $str_where = " AND a.catpkid = ".$int_cat_pkid." ";
    $str_where_for_filter = " AND a.catpkid = ".$int_cat_pkid. " ";
}


## For Price 
$int_min_price = 0;
$int_max_price = 0;
if(isset($_POST["txt_min_price"]) && $_POST["txt_min_price"] != '')
{
    $int_min_price = $_POST["txt_min_price"];
}

if(isset($_POST["txt_max_price"]) && $_POST["txt_max_price"] != '')
{
    $int_max_price = $_POST["txt_max_price"];
}

//print $int_min_price ; exit;
//print_r($_POST);
if($int_min_price != "" && $int_max_price != "")
{
    //print "HE"; exit;
    $str_where.= " AND a.ourprice BETWEEN ".$int_min_price." AND ".$int_max_price. " ";
}


//print $str_where."<br/>";


$str_filter = "";
##COLOR FILTER
$arr_filter_color = array();
$int_remove_key_color = "";
//print_r($_POST);

if(isset($_POST['cbx_fltr_color']) && $_POST['cbx_fltr_color']!= "")
{ 
    
    $arr_filter_color = $_POST['cbx_fltr_color'];
    //print $arr_filter_color;
    //print $int_remove_key_color; 
    
    $str_filter = " AND (";
    for($i=0; $i<count($arr_filter_color); $i++)
    {
        $str_filter .= "f.title='".$arr_filter_color[$i]. "' OR ";
        
    }
    $str_filter = rtrim($str_filter, " OR ");
    $str_filter.=")";
}
else if(isset($_POST['cbx_fltr_color2']) && isset($_POST['cbx_fltr_color2']))
{ 
    $arr_filter_color = $_POST['cbx_fltr_color2'];
    $str_filter = " AND (";
    for($i=0; $i<count($arr_filter_color); $i++)
    {
        $str_filter .= "f.title='".$arr_filter_color[$i]. "' OR ";
        
    }
    $str_filter = rtrim($str_filter, " OR ");
    $str_filter.=")";
    
}

if(isset($_POST['hdn_selected_color']) && isset($_POST['hdn_selected_color'])) 
{
    $arr_filter_color = explode(",", $_POST['hdn_selected_color']);
    //print $_POST['hdn_clr_badge'];
    //print_r($arr_filter_color);
    $int_remove_key_from_array = "";
    if(isset($_POST['hdn_clr_badge']) && $_POST['hdn_clr_badge'] != "") 
    {
        $int_remove_key_from_array = $_POST['hdn_clr_badge'];
        //print $int_remove_key_from_array;
        unset($arr_filter_color[$int_remove_key_from_array]);
        $arr_filter_color = array_values($arr_filter_color);
        //print_r($arr_filter_color);
        if(Count($arr_filter_color) > 0) {
        $str_filter .= " AND (";
        for($i=0; $i<count($arr_filter_color); $i++)
        {
            $str_filter .= "f.title='".$arr_filter_color[$i]. "' OR ";
            

        } //print $str_filter."<br/>"; exit;
        $str_filter = rtrim($str_filter, " OR ");
        $str_filter.=")";
        }
        
        
    }
    else if($int_min_price != "" && $int_max_price != "")
    {
        if(Count($arr_filter_color) > 0) {
        $str_filter .= " AND (";
        for($i=0; $i<count($arr_filter_color); $i++)
        {
            $str_filter .= "f.title='".$arr_filter_color[$i]. "' OR ";
            

        } //print $str_filter."<br/>"; exit;
        $str_filter = rtrim($str_filter, " OR ");
        $str_filter.=")";
        }
    }
    
}
    //print $str_where; exit;

##MATERIAL FILTER
$arr_filter_material = array();
if(isset($_POST['cbx_fltr_material']) && isset($_POST['cbx_fltr_material']))
{ 
    $arr_filter_material = $_POST['cbx_fltr_material'];
    $str_filter .= " AND (";
    for($i=0; $i<count($arr_filter_material); $i++)
    {
        $str_filter .= "h.title='".$arr_filter_material[$i]. "' OR ";
        
    }
    $str_filter = rtrim($str_filter, " OR ");
    $str_filter.=")";
}
else if(isset($_POST['cbx_fltr_material2']) && isset($_POST['cbx_fltr_material2']))
{ 
    $arr_filter_material = $_POST['cbx_fltr_material2'];
    $str_filter .= " AND (";
    for($i=0; $i<count($arr_filter_material); $i++)
    {
        $str_filter .= "h.title='".$arr_filter_material[$i]. "' OR ";
        
    }
    $str_filter = rtrim($str_filter, " OR ");
    $str_filter.=")";
}
if(isset($_POST['hdn_selected_material']) && isset($_POST['hdn_selected_material'])) 
{
    $arr_filter_material = explode(",", $_POST['hdn_selected_material']);
//    print_r($arr_filter_material);exit;
    //print $_POST['hdn_clr_badge'];
    //print_r($arr_filter_color);
    $int_remove_key_from_array = "";
    //print $_POST['hdn_material_badge'];exit;
    if(isset($_POST['hdn_material_badge']) && $_POST['hdn_material_badge'] != "") 
    {
        $int_remove_key_from_array = $_POST['hdn_material_badge'];
        //print $int_remove_key_from_array;
        unset($arr_filter_material[$int_remove_key_from_array]);
        $arr_filter_material = array_values($arr_filter_material);
        //print_r($arr_filter_color);
        if(Count($arr_filter_material) > 0) {
            $str_filter .= " AND (";
            for($i=0; $i<count($arr_filter_material); $i++)
            {
                $str_filter .= "h.title='".$arr_filter_material[$i]. "' OR ";


            } //print $str_filter."<br/>"; exit;
            $str_filter = rtrim($str_filter, " OR ");
            $str_filter.=")";
        }
        //print $str_filter; exit;
    }
    else if($int_min_price != "" && $int_max_price != "")
    {
        if(Count($arr_filter_material) > 0) {
            $str_filter .= " AND (";
            for($i=0; $i<count($arr_filter_material); $i++)
            {
                $str_filter .= "h.title='".$arr_filter_material[$i]. "' OR ";


            } //print $str_filter."<br/>"; exit;
            $str_filter = rtrim($str_filter, " OR ");
            $str_filter.=")";
        }
    }
    
}

##TYPE FILTER
$arr_filter_type = array();
if(isset($_POST['cbx_fltr_type']) && isset($_POST['cbx_fltr_type']))
{ 
    $arr_filter_type = $_POST['cbx_fltr_type'];
    $str_filter .= " AND (";
    for($i=0; $i<count($arr_filter_type); $i++)
    {
        $str_filter .= "j.title='".$arr_filter_type[$i]. "' OR ";
        
    }
    $str_filter = rtrim($str_filter, " OR ");
    $str_filter.=")";
}
else if(isset($_POST['cbx_fltr_type2']) && isset($_POST['cbx_fltr_type2']))
{ 
    $arr_filter_type = $_POST['cbx_fltr_type2'];
    $str_filter .= " AND (";
    for($i=0; $i<count($arr_filter_type); $i++)
    {
        $str_filter .= "j.title='".$arr_filter_type[$i]. "' OR ";
        
    }
    $str_filter = rtrim($str_filter, " OR ");
    $str_filter.=")";
}
if(isset($_POST['hdn_selected_type']) && isset($_POST['hdn_selected_type'])) 
{
    $arr_filter_type = explode(",", $_POST['hdn_selected_type']);
//    print_r($arr_filter_material);exit;
    //print $_POST['hdn_clr_badge'];
    //print_r($arr_filter_color);
    $int_remove_key_from_array = "";
    //print $_POST['hdn_material_badge'];exit;
    if(isset($_POST['hdn_type_badge']) && $_POST['hdn_type_badge'] != "") 
    {
        $int_remove_key_from_array = $_POST['hdn_type_badge'];
        //print $int_remove_key_from_array;
        unset($arr_filter_type[$int_remove_key_from_array]);
        $arr_filter_type = array_values($arr_filter_type);
        //print_r($arr_filter_color);
        if(Count($arr_filter_type) > 0) {
            $str_filter .= " AND (";
            for($i=0; $i<count($arr_filter_type); $i++)
            {
                $str_filter .= "j.title='".$arr_filter_type[$i]. "' OR ";


            } //print $str_filter."<br/>"; exit;
            $str_filter = rtrim($str_filter, " OR ");
            $str_filter.=")";
        }
        //print $str_filter; exit;
    }
    else if($int_min_price != "" && $int_max_price != "")
    {
        if(Count($arr_filter_type) > 0) {
            $str_filter .= " AND (";
            for($i=0; $i<count($arr_filter_type); $i++)
            {
                $str_filter .= "j.title='".$arr_filter_type[$i]. "' OR ";


            } //print $str_filter."<br/>"; exit;
            $str_filter = rtrim($str_filter, " OR ");
            $str_filter.=")";
        }
    }
    
}



##TYPE FILTER
$arr_filter_size = array();
if(isset($_POST['cbx_fltr_size']) && isset($_POST['cbx_fltr_size']))
{ 
    $arr_filter_size = $_POST['cbx_fltr_size'];
    $str_filter .= " AND (";
    for($i=0; $i<count($arr_filter_size); $i++)
    {
        $str_filter .= "n.title='".$arr_filter_size[$i]. "' OR ";
        
    }
    $str_filter = rtrim($str_filter, " OR ");
    $str_filter.=")";
}
else if(isset($_POST['cbx_fltr_size2']) && isset($_POST['cbx_fltr_size2']))
{ 
    $arr_filter_size = $_POST['cbx_fltr_size2'];
    $str_filter .= " AND (";
    for($i=0; $i<count($arr_filter_size); $i++)
    {
        $str_filter .= "n.title='".$arr_filter_size[$i]. "' OR ";
        
    }
    $str_filter = rtrim($str_filter, " OR ");
    $str_filter.=")";
}

if(isset($_POST['hdn_selected_size']) && isset($_POST['hdn_selected_size'])) 
{
    $arr_filter_size = explode(",", $_POST['hdn_selected_size']);
//    print_r($arr_filter_material);exit;
    //print $_POST['hdn_clr_badge'];
    //print_r($arr_filter_color);
    $int_remove_key_from_array = "";
    //print $_POST['hdn_material_badge'];exit;
    if(isset($_POST['hdn_size_badge']) && $_POST['hdn_size_badge'] != "") 
    {
        $int_remove_key_from_array = $_POST['hdn_size_badge'];
        //print $int_remove_key_from_array;
        unset($arr_filter_size[$int_remove_key_from_array]);
        $arr_filter_size = array_values($arr_filter_size);
        //print_r($arr_filter_color);
        if(Count($arr_filter_size) > 0) {
            $str_filter .= " AND (";
            for($i=0; $i<count($arr_filter_size); $i++)
            {
                $str_filter .= "j.title='".$arr_filter_size[$i]. "' OR ";


            } //print $str_filter."<br/>"; exit;
            $str_filter = rtrim($str_filter, " OR ");
            $str_filter.=")";
        }
        //print $str_filter; exit;
    }
    else if($int_min_price != "" && $int_max_price != "")
    {
        if(Count($arr_filter_size) > 0) 
        {
            $str_filter .= " AND (";
            for($i=0; $i<count($arr_filter_size); $i++)
            {
                $str_filter .= "j.title='".$arr_filter_size[$i]. "' OR ";


            } //print $str_filter."<br/>"; exit;
            $str_filter = rtrim($str_filter, " OR ");
            $str_filter.=")";
        }
    }    
}

//print($str_filter);


//print_r($_POST);
//print $str_where;exit;



## For Sorting
$str_order_by = "";

if(isset($_GET["sortby"]) && $_GET["sortby"] == "price_asc")
{
    $str_order_by = " ORDER BY a.ourprice, a.title";
}
else if(isset($_GET["sortby"]) && $_GET["sortby"] == "price_desc")
{
    $str_order_by = " ORDER BY a.ourprice DESC, a.title";
}
else if(isset($_GET["sortby"]) && $_GET["sortby"] == "popular")
{
    $str_order_by = " ORDER BY a.noofview DESC, a.title";
}
else if(isset($_GET["sortby"]) && $_GET["sortby"] == "new")
{
    $str_order_by = " ORDER BY a.displayasnew DESC, a.createdatetime DESC, a.title";
}
else if(isset($_GET["sortby"]) && $_GET["sortby"] == "default")
{
    $str_order_by = " ORDER BY a.displayorder DESC, a.createdatetime DESC, a.title";
}
else
{
    $str_order_by = " ORDER BY a.displayasfeatured DESC, a.displayashot DESC, a.displayorder DESC, a.createdatetime DESC, a.title";
}
//print $str_where;
#----------------------------------------------------------------------
# Select Query to get list
$str_query_select = "";

/*
// Query #1
$str_query_select = "SELECT DISTINCT a.*, b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title AS cattitle, ";
$str_query_select .= " f.title AS colortitle, n.title AS sizetitle ";
$str_query_select .= "FROM " .$STR_DB_TABLE_NAME. " a ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON a.catpkid=d.catpkid AND d.visible='YES' ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." b ON a.subcatpkid=b.subcatpkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_PHOTO." c ON a.pkid=c.masterpkid ";

//$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_COLOR." e ON a.pkid=e.itempkid AND e.masterpkid=f.pkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_COLOR." e ON a.pkid=e.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_COLOR." f ON e.masterpkid=f.pkid ";

$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_FABRIC." g ON a.pkid=g.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_FABRIC." h ON g.masterpkid=h.pkid ";

$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_TYPE." i ON a.pkid=i.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_TYPE." j ON i.masterpkid=j.pkid ";

$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_WORK." k ON a.pkid=k.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_WORK." l ON k.masterpkid=l.pkid ";

//$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_SIZE." m ON a.pkid=m.itempkid AND m.masterpkid=n.pkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_SIZE." m ON a.pkid=m.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_SIZE." n ON m.masterpkid=n.pkid ";

$str_query_select .="WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.visible='YES' AND a.approved='YES' ".$str_where.$str_filter." ";
$str_query_select .= $str_order_by;
$str_query_select .= " LIMIT 0,30";
*/

/*
// Query #2
$str_query_select = "SELECT DISTINCT a.*, b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title AS cattitle, ";
$str_query_select .= " f.title AS colortitle, n.title AS sizetitle ";
//$str_query_select .= "FROM " .$STR_DB_TABLE_NAME. " a ";
$str_query_select .= "FROM (select *, row_number() over(partition by pkid) AS rn FROM " .$STR_DB_TABLE_NAME. ") a ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON a.catpkid=d.catpkid AND d.visible='YES' AND a.rn=1";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." b ON a.subcatpkid=b.subcatpkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_PHOTO." c ON a.pkid=c.masterpkid ";

//$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_COLOR." e ON a.pkid=e.itempkid AND e.masterpkid=f.pkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_COLOR." e ON a.pkid=e.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_COLOR." f ON e.masterpkid=f.pkid ";

$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_FABRIC." g ON a.pkid=g.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_FABRIC." h ON g.masterpkid=h.pkid ";

$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_TYPE." i ON a.pkid=i.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_TYPE." j ON i.masterpkid=j.pkid ";

$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_WORK." k ON a.pkid=k.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_WORK." l ON k.masterpkid=l.pkid ";

//$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_SIZE." m ON a.pkid=m.itempkid AND m.masterpkid=n.pkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_SIZE." m ON a.pkid=m.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_SIZE." n ON m.masterpkid=n.pkid ";

$str_query_select .="WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.visible='YES' AND a.approved='YES' ".$str_where.$str_filter." ";
$str_query_select .= " AND a.rn=1 ";
$str_query_select .= $str_order_by;
$str_query_select .= " LIMIT 0,30";
*/

/*

*/
// Query #3

//$str_query_select = "SELECT a.*, "; // If GROUP BY is used in query then * can not be used
$str_query_select = "SELECT a.pkid, a.catpkid, a.subcatpkid, a.title, a.listprice, a.ourprice, a.memberprice, weight, ";
$str_query_select .= "a.description, a.createdatetime, a.displayasnew, a.displayashot, a.displayasfeatured, ";
$str_query_select .= "b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title AS cattitle, ";
$str_query_select .= "f.title AS colortitle, ";
//$str_query_select .= "group_concat(n.title ) AS sizetitle "; // Use this line if want all sizes in same record
$str_query_select .= "min(n.title) AS sizetitle "; // Use this line if want first size only
//$str_query_select .= "max(n.title) AS sizetitle "; // Use this line if want last size only
$str_query_select .= "FROM " .$STR_DB_TABLE_NAME. " a ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON a.catpkid=d.catpkid AND d.visible='YES' ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." b ON a.subcatpkid=b.subcatpkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_PHOTO." c ON a.pkid=c.masterpkid ";

$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_COLOR." e ON a.pkid=e.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_COLOR." f ON e.masterpkid=f.pkid ";

$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_FABRIC." g ON a.pkid=g.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_FABRIC." h ON g.masterpkid=h.pkid ";

$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_TYPE." i ON a.pkid=i.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_TYPE." j ON i.masterpkid=j.pkid ";

$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_WORK." k ON a.pkid=k.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_WORK." l ON k.masterpkid=l.pkid ";

$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_SIZE." m ON a.pkid=m.itempkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_SIZE." n ON m.masterpkid=n.pkid ";

$str_query_select .= " WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.visible='YES' AND a.approved='YES' ".$str_where.$str_filter." ";
$str_query_select .= " GROUP BY a.pkid, a.catpkid, a.subcatpkid, a.title, a.listprice, a.ourprice, a.memberprice, a.weight, a.description, a.createdatetime, a.displayasnew, a.displayashot, a.displayasfeatured, b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title, f.title ";
$str_query_select .= $str_order_by;
$str_query_select .= " LIMIT 0,30";
//print $str_query_select;
$rs_list = GetRecordSet($str_query_select);
//print $rs_list->Count();

#----------------------------------------------------------------------
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_TAILORING_SERVICE." WHERE visible='YES' AND price=0 LIMIT 0,1" ;
$rs_list_tailoring = GetRecordSet($str_query_select);
$int_tailoring_service_pkid = 0;
$int_tailoring_service_title = "Material Only";
if($rs_list_tailoring->Count() > 0)
{ 
	$int_tailoring_service_pkid = $rs_list_tailoring->Fields("pkid");
	$int_tailoring_service_title = $rs_list_tailoring->Fields("price");
}
#----------------------------------------------------------------------
$int_total_records = 0;
//print $int_total_records;
//print_r($_SESSION);
//print $int_total_records; exit;
#open cms xml file
$str_desc_cms = "";
$str_desc_cms2 = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION1", $fp);
$str_desc_cms2 = getTagValue("ITEMKEYVALUE_DESCRIPTION2", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE1", $fp);
$str_visible_cms2 = getTagValue("ITEMKEYVALUE_VISIBLE2", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatag. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
$str_member_flag = false;
$int_userpkid = 0;
$str_username = "";
$str_user_type = "";

if(isset($_SESSION['userpkid']) && $_SESSION['userpkid'] != "")
{
    $str_member_flag = true;
    $int_userpkid = $_SESSION['userpkid'];
    $str_username = $_SESSION['username'];
}

$int_referredbypkid = 0;
$int_commission = 0.00;
$int_wholesaler_discount = 0;

if($int_userpkid > 0)
{
    $str_query_select = "";
    $str_query_select = "SELECT referredbypkid, discount,isusertype FROM ".$STR_DB_TABLE_NAME_USER." WHERE pkid=".$int_userpkid;
    
    $rs_list_user = GetRecordSet($str_query_select);
    if(!$rs_list_user->EOF())
    {
        if(strtoupper($rs_list_user->Fields("isusertype")) == "WHOLESALER") 
        {
            $str_user_type = "WHOLESALER";
            $int_referredbypkid = $rs_list_user->Fields("referredbypkid");
            $int_wholesaler_discount = $rs_list_user->Fields("discount");
            if($int_referredbypkid > 0)
            {
                $str_query_select = "";
                $str_query_select = "SELECT discount, commission FROM ".$STR_DB_TABLE_NAME_USER." WHERE pkid=".$int_referredbypkid;
                $rs_list_commmision = GetRecordSet($str_query_select);

                $int_commission = $rs_list_commmision->Fields("commission");
                
            }
        }
    }
    //$str_where = " WHERE userpkid = ".$int_userpkid." ";
}
//print $str_user_type;
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="author" content="">
    <title><?php //print($STR_SITE_TITLE);?><?php //print($rs_list_mt->fields("titletag")) ;?><?php print $str_seo_title; ?></title>
    <meta name="description" content="<?php print $str_seo_desc; ?>">
    <meta name="keyword" content="<?php print $str_seo_keyword; ?>">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />   
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> 
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_listing.js"></script>
</head>
<body>
<div id="show_popup" class="alert alert-success" style="display: none; position: fixed; top: calc(100% - 35px); left: 26%; height: 35px; width: 46%; z-index: 100; text-align: center;"></div>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 class="text-right"><?php if($str_sub_category != "") { print $str_sub_category; } else { print $str_category; } ?></h1><hr/>
            </div>
        </div>
        
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <ol class="breadcrumb thumbnail-margin">
                    <?php if($str_sub_category != ""){ ?>
                        <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $int_cat_pkid; ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $str_category))));?>" title="<?php print $rs_list->Fields("cattitle"); ?>"><?php print $str_category; ?></a></li>
                        <li class=""><?php print $str_sub_category; ?></li>
                    <?php } else { ?>
                        <li class=""><?php print $str_category; ?></li>
                    <?php } ?>
                </ol>
            </div>
        </div>
        <?php if($str_visible_cms == "YES") { ?>
            <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div  class="well"><p align="justify"><?php print($str_desc_cms);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        <?php if($str_visible_cms2 == "YES") { ?>
        <?php if($str_desc_cms2 != "" && $str_desc_cms2 != "<br>") { ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <div  class="well"><p align="justify"><?php print($str_desc_cms2);?></p></div>
            </div>
        </div>
        <?php } } ?>
        
        <?php ## ------------------------- START : Category wise image & description ------------------- 
        $str_query_select = "";
        $str_cat_subcat_desc = "";
            
        if($str_sub_category == "")
        {  //print $str_sub_category;
            $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_CAT_PHOTO." WHERE masterpkid = ".$int_cat_pkid. " AND visible='YES' ORDER BY setasfront DESC, displayorder DESC, photosrno DESC"; 
            $str_cat_subcat_desc = $str_category_desc;
        } else 
        {  
            $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_SUBCAT_PHOTO." WHERE catpkid = ".$int_cat_pkid. " AND masterpkid=".$int_sub_cat_pkid." AND visible='YES' ORDER BY setasfront DESC, displayorder DESC, photosrno DESC";
            $str_cat_subcat_desc = $str_sub_category_desc;
        } 
        //print $str_query_select;
        $rs_list_images = GetRecordSet($str_query_select);
        if($rs_list_images->Count() > 0)
        {
        ?>
          <div class="bg-slider">
                <div class="row carousel-holder">
                    <div class="col-md-12">
                        <div id="slider" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" align="center">

                                <?php 
                                $int_cat_img_cnt = 0;
                                //PRINT $rs_subcat_images->count();
                                while($rs_list_images->eof() != true) {
                                    $str_active_cat = "";
                                    if($int_cat_img_cnt == 0) { $str_active_cat = "item active"; } else { $str_active_cat = "item"; } ?>
                                    <div class="<?php print $str_active_cat; ?>">                                                                                                           <img src="<?php print $UPLOAD_IMG_PATH_CAT_SUBCAT.$rs_list_images->Fields("largephotofilename");?>" alt="<?php print $str_category; ?>" title="<?php print $str_category; ?>" class="img-responsive img-thumbnail" width="100%" border="0" align="absmiddle">
                                    </div>
                                <?php
                                $int_cat_img_cnt++;
                                $rs_list_images->MoveNext();
                                }
                                ?>
                            </div>
                            <?php if($rs_list_images->count() > 1) { ?>
                                <a class="left carousel-control" href="#slider" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                                <a class="right carousel-control" href="#slider" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <a id="listing"></a>
        <?php if($str_category_desc != "<br>") {?>
        <div class="row padding-10">
            <div class="col-md-12">
                <p align="justify">
                    <?php print $str_category_desc; ?>
                </p>
            </div>
        </div><br/>
        <?php } ?>    
        <?php ## -------------------------END : Category wise image & description------------------- ?>
        
        <div class="row hidden-sm hidden-xs">
            <div class="col-md-3 col-lg-3">
                <?php if($int_cat_pkid > 0 && $int_sub_cat_pkid == 0) { ?>
                <?php 
                $str_query_select = "";
                $str_query_select  = "SELECT DISTINCT c.subcatpkid, c.subcattitle FROM t_store a "; 
                //$str_query_select .= "LEFT JOIN tr_store_photo d ON a.pkid=d.masterpkid AND d.visible='YES' AND d.setasfront='YES' AND d.setasofferimage='' "; 
                $str_query_select .= "LEFT JOIN t_store_cat b ON a.catpkid=b.catpkid AND b.visible='YES' ";
                $str_query_select .= "LEFT JOIN t_store_subcat c ON a.subcatpkid=c.subcatpkid AND c.visible='YES' WHERE a.catpkid=".$int_cat_pkid." AND a.approved='YES' AND a.visible='YES' ORDER BY c.subcattitle ASC";
                
                $rs_list_subcat_panel = GetRecordSet($str_query_select); 
                if($rs_list_subcat_panel->Count() > 0) {
                ?>
                <div class="sidenav-title black-bg">
                    <h4 class="text-white text-uppercase nopadding">
                        <span><i class="fa fa-bars"></i></span> SHOP BY CATEGORY
                    </h4>
                </div>
                <div class="list-group">
                    <?php 
                    while(!$rs_list_subcat_panel->EOF()) {?>
                        <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list_menu_subcat->Fields("pkid"); ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $str_category))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_subcat_panel->Fields("subcattitle")))));?>" class="list-group-item" title="<?php print $rs_list_subcat_panel->Fields("subcattitle"); ?>"><?php print $rs_list_subcat_panel->Fields("subcattitle"); ?></a>
                    <?php $rs_list_subcat_panel->MoveNext();
                    }?>
                </div>
                <?php } ?>
                <?php } ?>
                <div class="filter-box">
                    <form id="frm_filter" name="frm_filter" method="POST" action="#listing">
                        <?php 
                        $str_where_for_filter = "";
                        if(isset($_GET['cid']) && isset($_GET['sid']) && $_GET['sid']!="")
                        { 
                            $str_where_for_filter = " AND c.catpkid = ".$int_cat_pkid. " AND c.subcatpkid = ".$int_sub_cat_pkid." ";
                        }
                        else if(isset($_GET['cid'])) {
                            $str_where_for_filter = " AND c.catpkid = ".$int_cat_pkid. " ";
                        }?>    
                        <?php 
                        ## --------------------------------START: SHOP BY COLOR------------------------------------
                        $str_query_select = "";
                        $str_query_select = "SELECT DISTINCT b.title, b.cmyk_code ";
                        $str_query_select.= "FROM ".$STR_DB_TR_TABLE_NAME_COLOR." a ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_COLOR." b ON b.pkid=a.masterpkid ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME." c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                        $str_query_select.= "WHERE b.visible='YES'".$str_where_for_filter." ";
                        $str_query_select.= "ORDER BY b.title ASC";
                        //print $str_query_select;
                        $rs_list_color_filter = GetRecordSet($str_query_select);
                        if($rs_list_color_filter->Count() > 0) { ?>    
                        <div class="row padding-10">
                            <div class="col-md-12">
                                <div class="filter-head-bg">
                                    <h3 class="panel-title"><b> SHOP BY COLOR </b></h3>
                                </div>
                                
                                        
                                <?php /* ?><style>
                                       .bg-check {position:absolute;display:none;margin:9px -25px 0px;}
                                       .circle {background-color:#f1f1f1;color:#FFFFFF;display: inline-block;width: 30px;height: 30px;margin: 0 2px; text-align: center;font-size: 18px;font-family: 'Kavoon', cursive;line-height: 28px;-webkit-transition: all 0.2s ease-in-out; -moz-transition: all 0.2s ease-in-out; -ms-transition: all 0.2s ease-in-out;
                                        -o-transition: all 0.2s ease-in-out; transition: all 0.2s ease-in-out; cursor:pointer;-webkit-border-radius: 50%;-moz-border-radius: 50%;border-radius: 50%;}
                                       input[id="cbx_fltr_color"] {visibility: hidden;position: absolute;}
                                       input[id="cbx_fltr_color"] + img { opacity: 1;filter: alpha(opacity=1); }
                                       input[id="cbx_fltr_color"]:checked + img { opacity: 0.4; }
                                       input[id="cbx_fltr_color"]:checked ~ span { display: inline-block; }
                                </style><?php */ ?>
                                <ul class="chec-radio02">
                                <?php
                                //print_r($_POST);
                                $int_cnt = 1;
                                while(!$rs_list_color_filter->EOF()) { ?>
                                <style>
                                            /*--------------------Filter color Select Css---------------------*/
                                    ul.chec-radio02 {margin: 0px;margin-left: -40px;}
                                    ul.chec-radio02 li.pz02 {display: inline;}
                                    .chec-radio02 label.radio-inline02 input[id="cbx_fltr_color"] {display: none;}
                                    .chec-radio02 label.radio-inline02 input[id="cbx_fltr_color"]:checked+div {color: #fff;padding-left: 10px;padding-right:10px;border:2px solid #F5F5F5;color: #fff; -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: #939393;}
                                    .chec-radio02 .radio-inline02 .clab02 {cursor: pointer;padding: 7px 20px;text-align: center;text-transform: uppercase;color: #fff;position: relative;height: 35px;float: left;margin: 0;margin-bottom: 5px;border: 2px solid #F5F5F5;}
                                    .chec-radio02 label.radio-inline02 input[id="cbx_fltr_color"]:checked+div:before {content: "\e013";margin-right: 5px;font-family: 'Glyphicons Halflings';}
/*--------------------End Filter color Select Css---------------------*/
                                            
                                </style>
                                    <?php
                                    $str_fltr_color_checked = "";
                                    //print $arr_filter_color;
                                    if(in_array($rs_list_color_filter->Fields("title"), $arr_filter_color)) { $str_fltr_color_checked = "checked";  } ?>
                                    <li class="pz02">
                                        <label class="radio-inline02">
                                            <input type="checkbox" name="cbx_fltr_color[]" id="cbx_fltr_color" tabindex="1" class="pro-chx02" value="<?php print $rs_list_color_filter->Fields("title"); ?>" aria-invalid="false" <?php print $str_fltr_color_checked; ?> />                                                                            <div class="clab02" style="background: <?php print $rs_list_color_filter->Fields("cmyk_code"); ?>;"></div>
                                        </label>
                                    </li>
                                
                                <?php $int_cnt++;
                                $rs_list_color_filter->MoveNext();
                                } ?>
                                </ul>
                            </div>
                        </div>
                        <?php } ?>  
                        <?php  ## --------------------------------END: SHOP BY COLOR-------------------------------------- ?>
                        
                        <?php  ## --------------------------------START: SHOP BY MATERIAL--------------------------------- 
                        $str_query_select = "";
                        $str_query_select = "SELECT DISTINCT b.title ";
                        $str_query_select.= "FROM ".$STR_DB_TR_TABLE_NAME_FABRIC." a ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_FABRIC." b ON b.pkid=a.masterpkid ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME." c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                        $str_query_select.= "WHERE b.visible='YES'".$str_where_for_filter." ";
                        $str_query_select.= "ORDER BY b.title ASC";
                        
                        //print $str_query_select;
                        $rs_list_material_filter = GetRecordSet($str_query_select);
                        if($rs_list_material_filter->Count() > 0) { ?>    
                            <div class="row padding-10">
                                <div class="col-md-12">
                                    <div class="filter-head-bg">
                                        <h3 class="panel-title"><b> SHOP BY MATERIAL</b> </h3>
                                    </div>
                                    <?php
                                    while(!$rs_list_material_filter->EOF()) { ?>
                                    <div class="form-group">
                                            <label class="control control--checkbox label-normal"><?php print $rs_list_material_filter->Fields("title"); ?>
                                            <?php
                                            $str_fltr_material_checked = "";
                                            if(in_array($rs_list_material_filter->Fields("title"), $arr_filter_material)) { $str_fltr_material_checked = "checked";  } ?>
                                                <input type="checkbox" name="cbx_fltr_material[]" id="cbx_fltr_material" value="<?php print $rs_list_material_filter->Fields("title"); ?>" <?php print $str_fltr_material_checked; ?>>&nbsp;
                                                <div class="control__indicator" style="top:3px;"></div>
                                            </label>
                                        <hr/>
                                    </div>
                                    <?php
                                    $rs_list_material_filter->MoveNext();
                                    } ?>
                                </div>
                            </div>
                        <?php 
                        }
                        ?>    
                        <?php  ## --------------------------------END: SHOP BY MATERIAL-----------------------------------?>    

                        <?php  ## --------------------------------START: SHOP BY TYPE---------------------------------?> 
                        <?php 
                        $str_query_select = "";
                        $str_query_select = "SELECT DISTINCT b.title ";
                        $str_query_select.= "FROM ".$STR_DB_TR_TABLE_NAME_TYPE." a ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_TYPE." b ON b.pkid=a.masterpkid ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME." c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                        $str_query_select.= "WHERE b.visible='YES'".$str_where_for_filter." ";
                        $str_query_select.= "ORDER BY b.title ASC";
                        //print $str_query_select;exit;
                        $rs_list_type_filter = GetRecordSet($str_query_select);
                        //print $rs_list_type_filter->Count();
                        if($rs_list_type_filter->Count() > 0) { ?>    
                            <div class="row padding-10">
                                <div class="col-md-12">
                                    <div class="filter-head-bg">
                                        <h3 class="panel-title"><b> SHOP BY TYPE</b></h3>
                                    </div>
                                    <?php
                                    while(!$rs_list_type_filter->EOF()) { ?>
                                    <div class="form-group">
                                        
                                            <label class="control control--checkbox label-normal"><?php print $rs_list_type_filter->Fields("title"); ?>
                                            <?php
                                            $str_fltr_type_checked = "";
                                            if(in_array($rs_list_type_filter->Fields("title"), $arr_filter_type)) { $str_fltr_type_checked = "checked";  } ?>
                                                <input type="checkbox" name="cbx_fltr_type[]" id="cbx_fltr_type" value="<?php print $rs_list_type_filter->Fields("title"); ?>" <?php print $str_fltr_type_checked; ?>>
                                                <div class="control__indicator" style="top:3px;"></div>
                                            </label>
                                        <hr/>
                                    </div>
                                    <?php
                                    $rs_list_type_filter->MoveNext();
                                    } ?>
                                </div>
                            </div>
                        <?php 
                        }
                        ?>    
                        <?php  ## --------------------------------END: SHOP BY TYPE-----------------------------------?> 

                        <?php  ## --------------------------------START: SHOP BY SIZE---------------------------------?> 
                        <?php 
                        $str_query_select = "";
                        $str_query_select = "SELECT DISTINCT b.title ";
                        $str_query_select.= "FROM ".$STR_DB_TR_TABLE_NAME_SIZE." a ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_SIZE." b ON b.pkid=a.masterpkid ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME." c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                        $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                        $str_query_select.= "WHERE b.visible='YES'".$str_where_for_filter." ";
                        $str_query_select.= "ORDER BY b.title ASC";
                        //print $str_query_select;exit;
                        $rs_list_size_filter = GetRecordSet($str_query_select);
                        //print $rs_list_type_filter->Count();
                        if($rs_list_size_filter->Count() > 0) { ?>    
                            <div class="row padding-10">
                                <div class="col-md-12">
                                    <div class="filter-head-bg">
                                        <h3 class="panel-title"><b> SHOP BY SIZE</b></h3>
                                    </div>
                                    <?php
                                    while(!$rs_list_size_filter->EOF()) { ?>
                                    <div class="form-group">
                                        
                                            <label class="control control--checkbox label-normal"><?php print $rs_list_size_filter->Fields("title"); ?>
                                                <?php
                                            $str_fltr_size_checked = "";
                                            if(in_array($rs_list_size_filter->Fields("title"), $arr_filter_size)) { $str_fltr_size_checked = "checked";  } ?>
                                                <input type="checkbox" name="cbx_fltr_size[]" id="cbx_fltr_size" value="<?php print $rs_list_size_filter->Fields("title"); ?>" <?php print $str_fltr_size_checked; ?> />
                                                <div class="control__indicator" style="top:3px;"></div>
                                            </label>
                                        <hr/>
                                    </div>
                                    <?php
                                    $rs_list_size_filter->MoveNext();
                                    } ?>
                                </div>
                            </div>
                        <?php 
                        }
                        ?>    
                        <?php  ## --------------------------------END: SHOP BY SIZE-----------------------------------?>                         
                    </form>
                    <?php //print_r($_POST);   
                        $str_selected_type = "";
                        $str_selected_material = ""; 
                        $str_selected_color = "";
                        $str_selected_size = "";
                        if(Count($arr_filter_material) > 0) { ?>
                            
                            <?php for($i=0; $i<Count($arr_filter_material); $i++) 
                                  { $str_selected_material.= $arr_filter_material[$i].","; ?>
                            
                            <?php } ?>
                        <?php } ?>
                        <?php 
                        if(Count($arr_filter_color) > 0) { ?>
                            <?php for($i=0; $i<Count($arr_filter_color); $i++) 
                                  { $str_selected_color .= $arr_filter_color[$i].","; ?>
                            
                            <?php } ?>
                        <?php } ?>
                        <?php 
                        if(Count($arr_filter_type) > 0) { ?>
                            <?php  ?>
                            <?php for($i=0; $i<Count($arr_filter_type); $i++) 
                                  { $str_selected_type.= $arr_filter_type[$i].","; ?>
                            
                            <?php } ?>
                        <?php } ?>
                        <?php 
                        if(Count($arr_filter_size) > 0) { ?>
                            <?php  ?>
                            <?php for($i=0; $i<Count($arr_filter_size); $i++) 
                                  { $str_selected_size.= $arr_filter_size[$i].","; ?>
                            
                            <?php } ?>
                        <?php } ?>
                        
                        
                    <?php  ## --------------------------------START: SHOP BY PRICE-----------------------------------?>
                    <div class="row padding-10">
                        <div class="col-md-12">
                            <div class="filter-head-bg">
                                <h3 class="panel-title"><b>SHOP BY PRICE</b></h3>
                            </div>
                            <br />
                            <div class="double-handle-slider"></div>
                            <br/>
                            <?php //print_r($_POST); ?>
                            <form id="frm_filter_price" action="#listing" name="frm_filter_price" method="POST">
                                <div class="row padding-10">
                                    <div class="col-md-5">
                                        <div class="form-group">
<!--                                                <label>Min</label>-->
                                            <input id="txt_min_price" name="txt_min_price" type="number" class="min-value form-control" value="<?php if($int_min_price > 0) { print $int_min_price; } ?>" placeholder="100" />
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
<!--                                                <label>Max</label>-->
<input id="txt_max_price" name="txt_max_price" type="number" class="max-value form-control" value="<?php if($int_max_price > 0) { print $int_max_price; } ?>" placeholder="50000" />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <?php if(Count($arr_filter_color) > 0) { ?>
                                        <input type="hidden" id="hdn_selected_color" name="hdn_selected_color" value="<?php print rtrim($str_selected_color,","); ?>" /> 
                                        <?php } ?>
                                        <?php if(Count($arr_filter_material) > 0) { ?>
                                        <input type="hidden" id="hdn_selected_material" name="hdn_selected_material" value="<?php print rtrim($str_selected_material,","); ?>" /> 
                                        <?php } ?>
                                        <?php if(Count($arr_filter_type) > 0) { ?>
                                        <input type="hidden" id="hdn_selected_type" name="hdn_selected_type" value="<?php print rtrim($str_selected_type,","); ?>" /> 
                                        <?php } ?>
                                        <?php if(Count($arr_filter_size) > 0) { ?>
                                        <input type="hidden" id="hdn_selected_size" name="hdn_selected_size" value="<?php print rtrim($str_selected_size,","); ?>" /> 
                                        <?php } ?>
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php  ## --------------------------------END: SHOP BY PRICE-----------------------------------?>    
                </div>  
            </div>
            <div class="col-md-9 col-lg-9">
                <div class="row padding-10 hidden-sm hidden-xs">
                    <div class="col-lg-12 col-md-12">
                        
                        
                        
                        
                        <?php //print_r($_POST);   
                        if(Count($arr_filter_color) > 0) { ?>
                        <?php 
                            for($i=0; $i<Count($arr_filter_color); $i++) 
                            {  ?>
                            <form id="frm_fltr_badges_color" name="frm_fltr_badges_color" method="POST">
                                <div class="filte-badge"><?php print $arr_filter_color[$i]; ?>
                                    <input type="hidden" id="hdn_clr_badge" name="hdn_clr_badge" value="<?php print $i; ?>" />
                                    &nbsp;<button type="submit" class="close filter-badge-close" ><span class="">&times;</span></button>
                                    
                                </div>
                                <input type="hidden" id="hdn_selected_color" name="hdn_selected_color" value="<?php print rtrim($str_selected_color,","); ?>" /> 
                                <?php if(Count($arr_filter_material) > 0) { ?>
                                <input type="hidden" id="hdn_selected_material" name="hdn_selected_material" value="<?php print rtrim($str_selected_material,","); ?>" /> 
                                <?php } ?>
                                <?php if(Count($arr_filter_type) > 0) { ?>
                                <input type="hidden" id="hdn_selected_type" name="hdn_selected_type" value="<?php print rtrim($str_selected_type,","); ?>" /> 
                                <?php } ?>
                                <?php if(Count($arr_filter_size) > 0) { ?>
                                <input type="hidden" id="hdn_selected_size" name="hdn_selected_size" value="<?php print rtrim($str_selected_size,","); ?>" /> 
                                <?php } ?>
                            </form>
                            <?php 
                            } ?>
                                
                            
                        <?php } ?>
                        <?php //print_r($_POST);   
                        if(Count($arr_filter_material) > 0) { ?>
                        <?php 
                            for($i=0; $i<Count($arr_filter_material); $i++) 
                            {  ?>
                            <form id="frm_fltr_badges_material" name="frm_fltr_badges_material" method="POST">
                                <div class="filte-badge"><?php print $arr_filter_material[$i]; ?>
                                    <input type="hidden" id="hdn_material_badge" name="hdn_material_badge" value="<?php print $i; ?>" />
                                    &nbsp;<button type="submit" class="close filter-badge-close" ><span class="">&times;</span></button>
                                    
                                </div>
                                <?php if(Count($arr_filter_color) > 0) { ?>
                                <input type="hidden" id="hdn_selected_color" name="hdn_selected_color" value="<?php print rtrim($str_selected_color,","); ?>" /> 
                                <?php } ?>
                                <input type="hidden" id="hdn_selected_material" name="hdn_selected_material" value="<?php print rtrim($str_selected_material,","); ?>" /> 
                                <?php if(Count($arr_filter_type) > 0) { ?>
                                <input type="hidden" id="hdn_selected_type" name="hdn_selected_type" value="<?php print rtrim($str_selected_type,","); ?>" /> 
                                <?php } ?>
                                <?php if(Count($arr_filter_size) > 0) { ?>
                                <input type="hidden" id="hdn_selected_size" name="hdn_selected_size" value="<?php print rtrim($str_selected_size,","); ?>" /> 
                                <?php } ?>
                            </form>
                            <?php 
                            } //print rtrim($str_selected_material,","); ?>
                                
                            
                        <?php } ?>
                        
                        <?php //print_r($_POST);   
                        
                        if(Count($arr_filter_type) > 0) { ?>
                        <?php 
                            for($i=0; $i<Count($arr_filter_type); $i++) 
                            {  ?>
                            <form id="frm_fltr_badges_type" name="frm_fltr_badges_type" method="POST">
                                <div class="filte-badge"><?php print $arr_filter_type[$i]; ?>
                                    <input type="hidden" id="hdn_type_badge" name="hdn_type_badge" value="<?php print $i; ?>" />
                                    &nbsp;<button type="submit" class="close filter-badge-close" ><span class="">&times;</span></button>
                                    
                                </div>
                                <?php if(Count($arr_filter_color) > 0) { ?>
                                <input type="hidden" id="hdn_selected_color" name="hdn_selected_color" value="<?php print rtrim($str_selected_color,","); ?>" /> 
                                <?php } ?>
                                <?php if(Count($arr_filter_material) > 0) { ?>
                                <input type="hidden" id="hdn_selected_material" name="hdn_selected_material" value="<?php print rtrim($str_selected_material,","); ?>" /> 
                                <?php } ?>
                                <input type="hidden" id="hdn_selected_type" name="hdn_selected_type" value="<?php print rtrim($str_selected_type,","); ?>" /> 
                                <?php if(Count($arr_filter_size) > 0) { ?>
                                <input type="hidden" id="hdn_selected_size" name="hdn_selected_size" value="<?php print rtrim($str_selected_size,","); ?>" /> 
                                <?php } ?>
                            </form>
                            <?php 
                            } //print rtrim($str_selected_material,","); ?>
                                
                            
                        <?php } ?>
                        
                        
                        <?php //print_r($_POST);   
                        
                        if(Count($arr_filter_size) > 0) { ?>
                        <?php 
                            for($i=0; $i<Count($arr_filter_size); $i++) 
                            {  ?>
                            <form id="frm_fltr_badges_size" name="frm_fltr_badges_size" method="POST">
                                <div class="filte-badge"><?php print $arr_filter_size[$i]; ?>
                                    <input type="hidden" id="hdn_size_badge" name="hdn_size_badge" value="<?php print $i; ?>" />
                                    &nbsp;<button type="submit" class="close filter-badge-close" ><span class="">&times;</span></button>
                                    
                                </div>
                                <?php if(Count($arr_filter_color) > 0) { ?>
                                <input type="hidden" id="hdn_selected_color" name="hdn_selected_color" value="<?php print rtrim($str_selected_color,","); ?>" /> 
                                <?php } ?>
                                <?php if(Count($arr_filter_material) > 0) { ?>
                                <input type="hidden" id="hdn_selected_material" name="hdn_selected_material" value="<?php print rtrim($str_selected_material,","); ?>" /> 
                                <?php } ?>
                                <?php if(Count($arr_filter_type) > 0) { ?>
                                <input type="hidden" id="hdn_selected_type" name="hdn_selected_type" value="<?php print rtrim($str_selected_type,","); ?>" /> 
                                <?php } ?>
                                <input type="hidden" id="hdn_selected_size" name="hdn_selected_size" value="<?php print rtrim($str_selected_size,","); ?>" /> 
                                
                            </form>
                            <?php 
                            } //print rtrim($str_selected_material,","); ?>
                                
                            
                        <?php } ?>
                        
                    </div>
                </div>
                <?php //print_r($_POST); 
                if($rs_list->Count() > 0) { ?>
                <div class="row padding-10 hidden-sm hidden-xs">
                    <div class="col-lg-12 col-md-12">
                        <p><?php //print_r($_GET); ?>
                            <b>Sort By:</b>&nbsp;
                            <?php
                            $str_active_sorting_low_to_high = "text-muted";
                            $str_active_sorting_high_to_low = "text-muted";
                            $str_active_sorting_new = "text-muted";
                            $str_active_sorting_popular = "text-muted";
                            
                            if(isset($_GET["sortby"]) && $_GET["sortby"] == "price_asc") { $str_active_sorting_low_to_high = "text-primary"; }  
                            if(isset($_GET["sortby"]) && $_GET["sortby"] == "price_desc") { $str_active_sorting_high_to_low = "text-primary"; } 
                            if(isset($_GET["sortby"]) && $_GET["sortby"] == "new") { $str_active_sorting_new = "text-primary"; } 
                            if(isset($_GET["sortby"]) && $_GET["sortby"] == "popular") { $str_active_sorting_popular = "text-primary"; } ?>
                            <a href="?sortby=price_asc" title="Sort By Price: Low To High"><span class="<?php print $str_active_sorting_low_to_high; ?>">Price: Low To High</span></a>&nbsp;&nbsp;|&nbsp;&nbsp; 
                            <a href="?sortby=price_desc" title="Sort By Price: High To Low"><span class="<?php print $str_active_sorting_high_to_low; ?>">Price: High To Low</span></a>&nbsp;&nbsp;|&nbsp;&nbsp;
                            <a href="?sortby=new" title="Sort By New"><span class="<?php print $str_active_sorting_new; ?>">New</span></a>&nbsp;&nbsp;|&nbsp;&nbsp;
                            <?php /* ?><a href=""><b class="text-muted">Best Selling</b></a>&nbsp;&nbsp;|&nbsp;&nbsp;<?php */ ?>
                            <a href="?sortby=popular" title="Sort By Popular"><span class="<?php print $str_active_sorting_popular; ?>">Popular</span></a>&nbsp;&nbsp;|&nbsp;&nbsp;
                            <a href="?sortby=default" title="Reset"><span class="text-muted">Reset</span></a>
                        </p><hr/>
                    </div>
                </div>
                <div class="row padding-10 hidden-sm hidden-xs">
                    <?php 
                    $int_cnt = 0;
                    while(!$rs_list->EOF()) { 
                        $str_class = "product-box";
                        $str_class_bg = "product-content-bg";
                        if($rs_list->Fields("displayasfeatured") == "YES") { $str_class = "product-box-featured"; $str_class_bg = "product-content-bg-featured"; }
                        if($rs_list->Fields("displayashot") == "YES") { $str_class = "product-box-trending"; $str_class_bg = "product-content-bg-trending";}
                        ?>
                    <div class="col-lg-4 col-md-4">
                        <div class="<?php print $str_class; ?> text-center">
                            <div class="row padding-10">
                                <div class="col-md-12 nopadding" align="center">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" title="<?php print $rs_list->Fields("title"); ?>">
                                        <?php if($rs_list->Fields("thumbphotofilename") != "") { ?>
                                            <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                        <?php } else if($rs_list->Fields("imageurl") != "") { ?>
                                            <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $rs_list->Fields("imageurl"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                        <?php } ?>
                                    </a>

                                    <?php 
                                    $str_query_select = "";
                                    $str_query_select = "SELECT mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='LEFT' AND visible='YES' ORDER BY setasofferimage ASC";
                                    $rs_list_offerimages_left = GetRecordSet($str_query_select);
                                    //print $rs_list_offerimages->Count();
                                    if($rs_list_offerimages_left->Count() > 0) {
                                    ?>

                                    <img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list_offerimages_left->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-left"/>
                                    <?php }  ?>
                                    <?php 
                                    $str_query_select = "";
                                    $str_query_select = "SELECT mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='RIGHT' AND visible='YES' ORDER BY setasofferimage ASC";
                                    $rs_list_offerimages_right = GetRecordSet($str_query_select);
                                    //print $rs_list_offerimages->Count();
                                    if($rs_list_offerimages_right->Count() > 0) {
                                    ?>
                                    <img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list_offerimages_right->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-right"/>
                                    <?php }  ?>
                                </div>
                            </div>
                            <div class="row padding-10">
                                <div class="col-md-12 <?php print $str_class_bg; ?>">
                                    <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" title="<?php print $rs_list->Fields("title"); ?>"><b class="text-muted"><?php print MakeStringShort($rs_list->Fields("title"), 23); ?></b></a></p>
                                    <?php /* ?>
                                    <p>
                                        <?php if($rs_list->Fields("displayasnew") == 'YES') { print $STR_ICON_PATH_NEW; } ?><?php if($rs_list->Fields("displayasfeatured") == 'YES') { print "&nbsp;".$STR_ICON_PATH_FEATURED; } ?><?php if($rs_list->Fields("displayashot") == 'YES') { print "&nbsp;".$STR_ICON_PATH_HOT; } ?>
                                    </p>
                                    <?php */ ?>
                                    <p>
                                        <?php if($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) { ?>
                                    <?php 
                                        $int_wholesaler_price = "";
                                        $int_wholesaler_price = $rs_list->Fields("memberprice") / $int_conversionrate; 
					$int_hdn_price = $int_wholesaler_price;
					?>
                                        <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_wholesaler_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                <?php } else {  ?>
                                    <?php if($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                        $int_list_price = "";
                                        $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;

                                        $int_our_price = "";
                                        $int_our_price = $rs_list->Fields("ourprice") / $int_conversionrate;
					$int_hdn_price = $int_our_price;
                                        ?>
                                        <strike class="text-muted"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 0)."&nbsp;".$str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                        <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_our_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                        <?php
                                        $int_per_discount = 0;
                                        $int_per_discount = (100 - ($rs_list->Fields("ourprice")/$rs_list->Fields("listprice")) * 100); 
                                        if($int_per_discount > 0) { ?>
                                        <span class="label label-primary"><b><?php print number_format($int_per_discount, 2); ?>% OFF</b></span> 
                                        <?php } ?>
                                    <?php } else {
                                        $int_list_price = "";
                                        $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;
					$int_hdn_price = $int_list_price;
					?>
                                        <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                        <?php }  ?>    
                                <?php }  ?> 

                                    </p>
                                </div>
                            </div>
                            <div id="success<?php // print $rs_list->fields("pkid"); ?>"></div>
                            <div class="row padding-10">
                                <div class="col-md-12">
                                    <div class="row padding-10">
                                        <?php /*?><div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-button" title="Add to Cart"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a> 
                                        </div> <?php */?>
                                        <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
						<form class='myform' id="frm_add<?php print $rs_list->Fields("pkid"); ?>" name="frm_add<?php print $rs_list->Fields("pkid"); ?>" novalidate>
						<input type="hidden" name="hdn_prodpkid" id="hdn_prodpkid" value="<?php print $rs_list->Fields("pkid"); ?>">
						<input type="hidden" name="hdn_prodtitle" id="hdn_prodtitle" value="<?php print $rs_list->Fields("title"); ?>">

						<?php /* ?>
						<input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $int_cat_pkid; ?>">
						<input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $int_sub_cat_pkid; ?>">
						<?php */ ?>
						<input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $rs_list->Fields("catpkid"); ?>">
						<input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $rs_list->Fields("subcatpkid"); ?>">

						<input type="hidden" name="hdn_weight" id="hdn_weight" value="<?php print $rs_list->Fields("weight"); ?>">
						<input type="hidden" name="hdn_userpkid" id="hdn_userpkid" value="<?php print $int_userpkid; ?>">
						<input type="hidden" name="hdn_username" id="hdn_username" value="<?php print $str_username; ?>">
						<input type="hidden" name="hdn_discount" id="hdn_discount" value="<?php print $int_wholesaler_discount; ?>">
						<input type="hidden" name="hdn_referredbypkid" id="hdn_referredbypkid" value="<?php print $int_referredbypkid; ?>">
						<input type="hidden" name="hdn_commission" id="hdn_commission" value="<?php print $int_commission; ?>">
						<input type="hidden" name="rdo_color" id="rdo_color" value="<?php print $rs_list->Fields("colortitle"); ?>">
						<input type="hidden" name="rdo_size" id="rdo_size" value="<?php print $rs_list->Fields("sizetitle"); ?>">
						<input type="hidden" name="rdo_tailoringoption" id="rdo_tailoringoption" value="<?php print $int_tailoring_service_pkid; ?>">
						<input type="hidden" name="cbo_semistitched_stitchingsize" id="cbo_semistitched_stitchingsize" value="0">
						<input type="hidden" name="txt_blouse_length" id="txt_blouse_length" value="0">
						<input type="hidden" name="txt_blouse_sleeve_length" id="txt_blouse_sleeve_length" value="0">
						<input type="hidden" name="cbo_blouse_aroundbust" id="cbo_blouse_aroundbust" value="0">
						<input type="hidden" name="txt_blouse_abovearoundwaist" id="txt_blouse_abovearoundwaist" value="0">
						<input type="hidden" name="txt_blouse_frontneckdepth" id="txt_blouse_frontneckdepth" value="0">
						<input type="hidden" name="txt_blouse_backneckdepth" id="txt_blouse_backneckdepth" value="0">
						<input type="hidden" name="txt_lehenga_aroundwaist" id="txt_lehenga_aroundwaist" value="0">
						<input type="hidden" name="txt_lehenga_aroundhips" id="txt_lehenga_aroundhips" value="0">
						<input type="hidden" name="txt_lehenga_length" id="txt_lehenga_length" value="0">
						<input type="hidden" name="txt_salwar_yourheight" id="txt_salwar_yourheight" value="0">
						<input type="hidden" name="txt_salwar_frontneckdepth" id="txt_salwar_frontneckdepth" value="0">
						<input type="hidden" name="txt_salwar_bottomlength" id="txt_salwar_bottomlength" value="0">
						<input type="hidden" name="txt_salwar_kameezlength" id="txt_salwar_kameezlength" value="0">
						<input type="hidden" name="txt_salwar_sleevelength" id="txt_salwar_sleevelength" value="0">
						<input type="hidden" name="cbo_salwar_sleevestyle" id="cbo_salwar_sleevestyle" value="0">
						<input type="hidden" name="cbo_salwar_aroundbust" id="cbo_salwar_aroundbust" value="0">
						<input type="hidden" name="txt_salwar_abovearoundwaist" id="txt_salwar_abovearoundwaist" value="0">
						<input type="hidden" name="txt_salwar_aroundwaist" id="txt_salwar_aroundwaist" value="0">
						<input type="hidden" name="txt_salwar_aroundhips" id="txt_salwar_aroundhips" value="0">
						<input type="hidden" name="txt_salwar_backneckdepth" id="txt_salwar_backneckdepth" value="0">
						<input type="hidden" name="txt_salwar_aroundthigh" id="txt_salwar_aroundthigh" value="0">
						<input type="hidden" name="txt_salwar_aroundknee" id="txt_salwar_aroundknee" value="0">
						<input type="hidden" name="txt_salwar_aroundcalf" id="txt_salwar_aroundcalf" value="0">
						<input type="hidden" name="txt_salwar_aroundankle" id="txt_salwar_aroundankle" value="0">

                                		<input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $int_hdn_price;  ?>" />
                                		<input type="hidden" id="txt_quantity" name="txt_quantity" value="1" />
                                		<input type="hidden" id="hdn_description" name="hdn_description" value="" />
                                		<input type="hidden" id="rdo_shipping" name="rdo_shipping" value="" />
						<?php 
						$skuval=date("YmdHis"); 
						$skuval= GetEncryptId($rs_list->Fields("pkid"));
						?> 
                    				<input type="hidden" id="specssku" name="specssku" value="<?php print $skuval;  ?>" />


                                                <button type="submit" class="product-buynow-button" name='btn_addtocart' id="btn_addtocart_<?php print $rs_list->Fields("pkid"); ?>" style="border-bottom: 1px solid transparent; width: 100%;"><i class="fa fa-shopping-cart"></i></button>

						</form>
                                            	<?php /* ?><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-buynow-button"  title="Buy Now"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a> <?php */ ?>
                                            
						<?php //print $rs_list->Fields("pkid")." - ".$rs_list->Fields("title")." - ".$int_cat_pkid." - ".$int_sub_cat_pkid." - ".$rs_list->Fields("weight")." - ".$int_userpkid." - ".$str_username." - ".$int_wholesaler_discount." - ".$int_referredbypkid." - ".$int_commission." - ".$rs_list->Fields("colortitle")." - ".$rs_list->Fields("sizetitle")." - ".$int_tailoring_service_pkid." - ".$int_hdn_price." - ".$skuval." - "; ?>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                        <?php 
                                        ## --------------------------------------------- START : WISHLIST------------------------------------------
                                        if($str_member_flag) 
                                        { 
                                            $str_query_select="";
                                            $str_query_select="SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=".$_SESSION["userpkid"]." AND itempkid=".$rs_list->fields("pkid");
                                            $rs_list_fav=GetRecordSet($str_query_select);

                                            $str_fav_class = "";
                                            $str_fav_flag = "";
                                            if($rs_list_fav->Count() == 0) { 
                                                $str_fav_class = "";
                                                $str_fav_flag = "FAV"; 
                                            } else if($rs_list_fav->Count() > 0) { 
                                              $str_fav_class = "text-primary";
                                              $str_fav_flag = "UNFAV"; 
                                            } ?>
                                            <form name="frm_user_favorite<?php print $rs_list->fields("pkid"); ?>" id="frm_user_favorite<?php print $rs_list->fields("pkid"); ?>" novalidate>

                                                <input type="hidden" name="userpkid" id="userpkid<?php print($rs_list->fields("pkid")) ?>" value="<?php print($_SESSION["userpkid"]); ?>">
                                                <input type="hidden" name="itempkid" id="itempkid<?php print($rs_list->fields("pkid")) ?>" value="<?php print($rs_list->fields("pkid")) ?>">

                                                <input type="hidden" name="flgfav" id="flgfav<?php print($rs_list->fields("pkid")) ?>" value="<?php print($str_fav_flag); ?>">



                                              <?php //print $rs_list_fav->Count(); ?>
                                                <button id="frm_submit<?php print($rs_list->fields("pkid")) ?>" name="frm_submit<?php print($rs_list->fields("pkid")) ?>" type="submit" style="display: none;" ></button>
                                            </form>

                                            <a onclick="$('#frm_submit<?php print($rs_list->fields("pkid")) ?>').click();" class="product-button <?php print $str_fav_class; ?>"><i class="fa fa-heart"></i></a>
                                            <script>
                                                $(function() {
                                                    $("#frm_user_favorite<?php print $rs_list->fields("pkid"); ?> input").jqBootstrapValidation({
                                                        preventSubmit: true,
                                                        submitError: function($form, event, errors) {
                                                            // something to have when submit produces an error ?
                                                            // Not decided if I need it yet
                                                        },
                                                        submitSuccess: function($form, event) {
                                                            event.preventDefault(); // prevent default submit behaviour
                                                            // get values from FORM

                                                            var userpkid = $("input#userpkid<?php print($rs_list->fields("pkid")) ?>").val();
                                                            var itempkid = $("input#itempkid<?php print($rs_list->fields("pkid")) ?>").val();
                                                            var flgfav = $("input#flgfav<?php print($rs_list->fields("pkid")) ?>").val();

                                                                $.ajax({

                                                                url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                                                type: "POST",
                                                                data: 
                                                                {
                                                                    userpkid : userpkid,
                                                                    itempkid : itempkid,
                                                                    flgfav : flgfav
                                                                },
                                                                cache: false,
                                                                success: function(data) 
                                                                {
                                                                //alert(data);
                                                                    var $responseText = JSON.parse(data);
                                                                    if($responseText.status == 'SUC')
                                                //                                        if(($responseText.status).equal("SUC"))
                                                                    {
                                                                        // Success message
                                                //                        alert(data);

                                                                        $('#success<?php print $rs_list->fields("pkid"); ?>').html("<div class='alert alert-success small'>");
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                        .append("</button>");
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-success ').append('</div>');
                                  //setTimeout(function(){ location.reload() }, 2000);
                                                                            setTimeout(function(){ location.reload(); }, 5000);
                                                                    }
                                                                    else if($responseText.status == 'ERR')
                                                                    {
                                                                        // Fail message
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?>').html("<div class='alert alert-danger'>");
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                                .append("</button>");
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-danger').append('</div>');

                                                                    }
                                                                },

                                                            })
                                                        },
                                                        filter: function() {
                                                            return $(this).is(":visible");
                                                        },
                                                    });

                                                    $("a[data-toggle=\"tab\"]").click(function(e) {
                                                        e.preventDefault();
                                                        $(this).tab("show");
                                                    });
                                                });
                                                /*When clicking on Full hide fail/success boxes */
                                                $('#name').focus(function() {
                                                    $('#success').html('');
                                                });

                                            </script>
                                        <?php } else { ?>
                                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>" class="product-button"><i class="fa fa-heart"></i></a>
                                        <?php } 
                                        ## --------------------------------------------- END : WISHLIST------------------------------------------ ?>    
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <?php
                    $int_cnt++; if($int_cnt%3 == 0) { print "</div><div class='row padding-10 hidden-sm hidden-xs'>"; }
                    $rs_list->MoveNext();
                    }$rs_list->MoveFirst();
                    ?>
                </div>
                <?php } else { ?>
                <div class="row padding-10 hidden-sm hidden-xs">
                    <div class="col-md-12 col-lg-12 text-center alert alert-danger"><?php print $STR_MSG_NO_DATA_AVAILABLE; ?></div>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="row padding-10 hidden-md hidden-lg">
            <div class="col-sm-12 col-xs-12">
                <?php //print_r($_POST);   
                $str_selected_type = "";
                $str_selected_material = ""; 
                $str_selected_color = "";
                $str_selected_size = "";
                if(Count($arr_filter_material) > 0) { ?>

                    <?php for($i=0; $i<Count($arr_filter_material); $i++) 
                          { $str_selected_material.= $arr_filter_material[$i].","; ?>

                    <?php } ?>
                <?php } ?>
                <?php 
                if(Count($arr_filter_color) > 0) { ?>
                    <?php for($i=0; $i<Count($arr_filter_color); $i++) 
                          { $str_selected_color .= $arr_filter_color[$i].","; ?>

                    <?php } ?>
                <?php } ?>
                <?php 
                if(Count($arr_filter_type) > 0) { ?>
                    <?php  ?>
                    <?php for($i=0; $i<Count($arr_filter_type); $i++) 
                          { $str_selected_type.= $arr_filter_type[$i].","; ?>

                    <?php } ?>
                <?php } ?>
                <?php 
                if(Count($arr_filter_size) > 0) { ?>
                    <?php  ?>
                    <?php for($i=0; $i<Count($arr_filter_size); $i++) 
                          { $str_selected_size.= $arr_filter_size[$i].","; ?>

                    <?php } ?>
                <?php } ?>





                <?php //print_r($_POST);   
                if(Count($arr_filter_color) > 0) { ?>
                <?php 
                    for($i=0; $i<Count($arr_filter_color); $i++) 
                    {  ?>
                    <form id="frm_fltr_badges_color" name="frm_fltr_badges_color" method="POST">
                        <div class="filte-badge"><?php print $arr_filter_color[$i]; ?>
                            <input type="hidden" id="hdn_clr_badge" name="hdn_clr_badge" value="<?php print $i; ?>" />
                            &nbsp;<button type="submit" class="close filter-badge-close" ><span class="">&times;</span></button>

                        </div>
                        <input type="hidden" id="hdn_selected_color" name="hdn_selected_color" value="<?php print rtrim($str_selected_color,","); ?>" /> 
                        <?php if(Count($arr_filter_material) > 0) { ?>
                        <input type="hidden" id="hdn_selected_material" name="hdn_selected_material" value="<?php print rtrim($str_selected_material,","); ?>" /> 
                        <?php } ?>
                        <?php if(Count($arr_filter_type) > 0) { ?>
                        <input type="hidden" id="hdn_selected_type" name="hdn_selected_type" value="<?php print rtrim($str_selected_type,","); ?>" /> 
                        <?php } ?>
                        <?php if(Count($arr_filter_size) > 0) { ?>
                        <input type="hidden" id="hdn_selected_size" name="hdn_selected_size" value="<?php print rtrim($str_selected_size,","); ?>" /> 
                        <?php } ?>
                    </form>
                    <?php 
                    } ?>


                <?php } ?>
                <?php //print_r($_POST);   
                if(Count($arr_filter_material) > 0) { ?>
                <?php 
                    for($i=0; $i<Count($arr_filter_material); $i++) 
                    {  ?>
                    <form id="frm_fltr_badges_material" name="frm_fltr_badges_material" method="POST">
                        <div class="filte-badge"><?php print $arr_filter_material[$i]; ?>
                            <input type="hidden" id="hdn_material_badge" name="hdn_material_badge" value="<?php print $i; ?>" />
                            &nbsp;<button type="submit" class="close filter-badge-close" ><span class="">&times;</span></button>

                        </div>
                        <?php if(Count($arr_filter_color) > 0) { ?>
                        <input type="hidden" id="hdn_selected_color" name="hdn_selected_color" value="<?php print rtrim($str_selected_color,","); ?>" /> 
                        <?php } ?>
                        <input type="hidden" id="hdn_selected_material" name="hdn_selected_material" value="<?php print rtrim($str_selected_material,","); ?>" /> 
                        <?php if(Count($arr_filter_type) > 0) { ?>
                        <input type="hidden" id="hdn_selected_type" name="hdn_selected_type" value="<?php print rtrim($str_selected_type,","); ?>" /> 
                        <?php } ?>
                        <?php if(Count($arr_filter_size) > 0) { ?>
                        <input type="hidden" id="hdn_selected_size" name="hdn_selected_size" value="<?php print rtrim($str_selected_size,","); ?>" /> 
                        <?php } ?>
                    </form>
                    <?php 
                    } //print rtrim($str_selected_material,","); ?>


                <?php } ?>

                <?php //print_r($_POST);   

                if(Count($arr_filter_type) > 0) { ?>
                <?php 
                    for($i=0; $i<Count($arr_filter_type); $i++) 
                    {  ?>
                    <form id="frm_fltr_badges_type" name="frm_fltr_badges_type" method="POST">
                        <div class="filte-badge"><?php print $arr_filter_type[$i]; ?>
                            <input type="hidden" id="hdn_type_badge" name="hdn_type_badge" value="<?php print $i; ?>" />
                            &nbsp;<button type="submit" class="close filter-badge-close" ><span class="">&times;</span></button>

                        </div>
                        <?php if(Count($arr_filter_color) > 0) { ?>
                        <input type="hidden" id="hdn_selected_color" name="hdn_selected_color" value="<?php print rtrim($str_selected_color,","); ?>" /> 
                        <?php } ?>
                        <?php if(Count($arr_filter_material) > 0) { ?>
                        <input type="hidden" id="hdn_selected_material" name="hdn_selected_material" value="<?php print rtrim($str_selected_material,","); ?>" /> 
                        <?php } ?>
                        <input type="hidden" id="hdn_selected_type" name="hdn_selected_type" value="<?php print rtrim($str_selected_type,","); ?>" /> 
                        <?php if(Count($arr_filter_size) > 0) { ?>
                        <input type="hidden" id="hdn_selected_size" name="hdn_selected_size" value="<?php print rtrim($str_selected_size,","); ?>" /> 
                        <?php } ?>
                    </form>
                    <?php 
                    } //print rtrim($str_selected_material,","); ?>


                <?php } ?>


                <?php //print_r($_POST);   

                if(Count($arr_filter_size) > 0) { ?>
                <?php 
                    for($i=0; $i<Count($arr_filter_size); $i++) 
                    {  ?>
                    <form id="frm_fltr_badges_size" name="frm_fltr_badges_size" method="POST">
                        <div class="filte-badge"><?php print $arr_filter_size[$i]; ?>
                            <input type="hidden" id="hdn_size_badge" name="hdn_size_badge" value="<?php print $i; ?>" />
                            &nbsp;<button type="submit" class="close filter-badge-close" ><span class="">&times;</span></button>

                        </div>
                        <?php if(Count($arr_filter_color) > 0) { ?>
                        <input type="hidden" id="hdn_selected_color" name="hdn_selected_color" value="<?php print rtrim($str_selected_color,","); ?>" /> 
                        <?php } ?>
                        <?php if(Count($arr_filter_material) > 0) { ?>
                        <input type="hidden" id="hdn_selected_material" name="hdn_selected_material" value="<?php print rtrim($str_selected_material,","); ?>" /> 
                        <?php } ?>
                        <?php if(Count($arr_filter_type) > 0) { ?>
                        <input type="hidden" id="hdn_selected_type" name="hdn_selected_type" value="<?php print rtrim($str_selected_type,","); ?>" /> 
                        <?php } ?>
                        <input type="hidden" id="hdn_selected_size" name="hdn_selected_size" value="<?php print rtrim($str_selected_size,","); ?>" /> 

                    </form>
                    <?php 
                    } //print rtrim($str_selected_material,","); ?>
                <?php } ?>
            </div>
        </div>
        <?php if($rs_list->Count() > 0) { ?>
        <div class="row padding-10 hidden-md hidden-lg hidden-xs">
            <?php 
            $int_cnt = 0;
            while(!$rs_list->EOF()) {
                $str_class = "product-box";
                $str_class_bg = "product-content-bg";
                if($rs_list->Fields("displayasfeatured") == "YES") { $str_class = "product-box-featured"; $str_class_bg = "product-content-bg-featured"; }
                if($rs_list->Fields("displayashot") == "YES") { $str_class = "product-box-trending"; $str_class_bg = "product-content-bg-trending"; }
                ?>
            <div class="col-sm-4">
                <div class="<?php print $str_class; ?> text-center">
                    <div class="row padding-10">
                        <div class="col-md-12 nopadding" align="center">
                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" title="<?php print $rs_list->Fields("title"); ?>">
                                <?php if($rs_list->Fields("thumbphotofilename") != "") { ?>
                                    <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                <?php } else if($rs_list->Fields("imageurl") != "") { ?>
                                    <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $rs_list->Fields("imageurl"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                <?php } ?>
                            </a>
                            
                            <?php 
                            $str_query_select = "";
                            $str_query_select = "SELECT mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='LEFT' AND visible='YES' ORDER BY setasofferimage ASC";
                            $rs_list_offerimages_left = GetRecordSet($str_query_select);
                            //print $rs_list_offerimages->Count();
                            if($rs_list_offerimages_left->Count() > 0) {
                            ?>
                            
                            <img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list_offerimages_left->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-left"/>
                            <?php }  ?>
                            <?php 
                            $str_query_select = "";
                            $str_query_select = "SELECT mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='RIGHT' AND visible='YES' ORDER BY setasofferimage ASC";
                            $rs_list_offerimages_right = GetRecordSet($str_query_select);
                            //print $rs_list_offerimages->Count();
                            if($rs_list_offerimages_right->Count() > 0) { ?>
                                <img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list_offerimages_right->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-right"/>
                            <?php }  ?>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-12 <?php print $str_class_bg; ?>">
                            <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" title="<?php print $rs_list->Fields("title"); ?>"><b class="text-muted"><?php print MakeStringShort($rs_list->Fields("title"), 18); ?></b></a></p>
                            <?php /* ?> 
                            <p>
                                <?php if($rs_list->Fields("displayasnew") == 'YES') { print $STR_ICON_PATH_NEW; } ?><?php if($rs_list->Fields("displayasfeatured") == 'YES') { print "&nbsp;".$STR_ICON_PATH_FEATURED; } ?><?php if($rs_list->Fields("displayashot") == 'YES') { print "&nbsp;".$STR_ICON_PATH_HOT; } ?>
                            </p>
                            <?php */ ?>
                            <p>
                                <?php if($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) { ?>
                                    <?php 

                                        $int_wholesaler_price = "";
                                        $int_wholesaler_price = $rs_list->Fields("memberprice") / $int_conversionrate; 
					$int_hdn_price = $int_wholesaler_price;
					?>
                                        <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_wholesaler_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;

                                <?php } else {  ?>
                                    <?php if($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                        $int_list_price = "";
                                        $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;

                                        $int_our_price = "";
                                        $int_our_price = $rs_list->Fields("ourprice") / $int_conversionrate;
					$int_hdn_price = $int_our_price;
                                        ?>
                                        <strike class="text-muted"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 0)."&nbsp;".$str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                        <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_our_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                        <?php
                                        $int_per_discount = 0;
                                        $int_per_discount = (100 - ($rs_list->Fields("ourprice")/$rs_list->Fields("listprice")) * 100); 
                                        if($int_per_discount > 0) { ?>
                                        <span class="label label-primary"><b><?php print number_format($int_per_discount, 2); ?>% OFF</b></span> 
                                        <?php } ?>
                                    <?php } else {
                                        $int_list_price = "";
                                        $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate; 
					$int_hdn_price = $int_list_price;
					?>
                                            <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                        <?php }  ?>    
                                <?php }  ?>
				<input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $int_hdn_price;  ?>" />
                            </p>
                        </div>
                    </div>
                    <div id="success2<?php print $rs_list->fields("pkid"); ?>"></div>
                    <div class="row padding-10">
                        <div class="col-md-12">
                            <div class="row padding-10">
                                <?php /*?><div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-button" title="Add to Cart"><b><i class="fa fa-cart-plus" aria-hidden="true" ></i></b></a>
                                </div><?php */?>
                                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
					<form class='myform' id="frm_add<?php print $rs_list->Fields("pkid"); ?>" name="frm_add<?php print $rs_list->Fields("pkid"); ?>" novalidate>
						<input type="hidden" name="hdn_prodpkid" id="hdn_prodpkid" value="<?php print $rs_list->Fields("pkid"); ?>">
						<input type="hidden" name="hdn_prodtitle" id="hdn_prodtitle" value="<?php print $rs_list->Fields("title"); ?>">

						<?php /* ?>
						<input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $int_cat_pkid; ?>">
						<input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $int_sub_cat_pkid; ?>">
						<?php */ ?>
						<input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $rs_list->Fields("catpkid"); ?>">
						<input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $rs_list->Fields("subcatpkid"); ?>">

						<input type="hidden" name="hdn_weight" id="hdn_weight" value="<?php print $rs_list->Fields("weight"); ?>">
						<input type="hidden" name="hdn_userpkid" id="hdn_userpkid" value="<?php print $int_userpkid; ?>">
						<input type="hidden" name="hdn_username" id="hdn_username" value="<?php print $str_username; ?>">
						<input type="hidden" name="hdn_discount" id="hdn_discount" value="<?php print $int_wholesaler_discount; ?>">
						<input type="hidden" name="hdn_referredbypkid" id="hdn_referredbypkid" value="<?php print $int_referredbypkid; ?>">
						<input type="hidden" name="hdn_commission" id="hdn_commission" value="<?php print $int_commission; ?>">
						<input type="hidden" name="rdo_color" id="rdo_color" value="<?php print $rs_list->Fields("colortitle"); ?>">
						<input type="hidden" name="rdo_size" id="rdo_size" value="<?php print $rs_list->Fields("sizetitle"); ?>">
						<input type="hidden" name="rdo_tailoringoption" id="rdo_tailoringoption" value="<?php print $int_tailoring_service_pkid; ?>">
						<input type="hidden" name="cbo_semistitched_stitchingsize" id="cbo_semistitched_stitchingsize" value="0">
						<input type="hidden" name="txt_blouse_length" id="txt_blouse_length" value="0">
						<input type="hidden" name="txt_blouse_sleeve_length" id="txt_blouse_sleeve_length" value="0">
						<input type="hidden" name="cbo_blouse_aroundbust" id="cbo_blouse_aroundbust" value="0">
						<input type="hidden" name="txt_blouse_abovearoundwaist" id="txt_blouse_abovearoundwaist" value="0">
						<input type="hidden" name="txt_blouse_frontneckdepth" id="txt_blouse_frontneckdepth" value="0">
						<input type="hidden" name="txt_blouse_backneckdepth" id="txt_blouse_backneckdepth" value="0">
						<input type="hidden" name="txt_lehenga_aroundwaist" id="txt_lehenga_aroundwaist" value="0">
						<input type="hidden" name="txt_lehenga_aroundhips" id="txt_lehenga_aroundhips" value="0">
						<input type="hidden" name="txt_lehenga_length" id="txt_lehenga_length" value="0">
						<input type="hidden" name="txt_salwar_yourheight" id="txt_salwar_yourheight" value="0">
						<input type="hidden" name="txt_salwar_frontneckdepth" id="txt_salwar_frontneckdepth" value="0">
						<input type="hidden" name="txt_salwar_bottomlength" id="txt_salwar_bottomlength" value="0">
						<input type="hidden" name="txt_salwar_kameezlength" id="txt_salwar_kameezlength" value="0">
						<input type="hidden" name="txt_salwar_sleevelength" id="txt_salwar_sleevelength" value="0">
						<input type="hidden" name="cbo_salwar_sleevestyle" id="cbo_salwar_sleevestyle" value="0">
						<input type="hidden" name="cbo_salwar_aroundbust" id="cbo_salwar_aroundbust" value="0">
						<input type="hidden" name="txt_salwar_abovearoundwaist" id="txt_salwar_abovearoundwaist" value="0">
						<input type="hidden" name="txt_salwar_aroundwaist" id="txt_salwar_aroundwaist" value="0">
						<input type="hidden" name="txt_salwar_aroundhips" id="txt_salwar_aroundhips" value="0">
						<input type="hidden" name="txt_salwar_backneckdepth" id="txt_salwar_backneckdepth" value="0">
						<input type="hidden" name="txt_salwar_aroundthigh" id="txt_salwar_aroundthigh" value="0">
						<input type="hidden" name="txt_salwar_aroundknee" id="txt_salwar_aroundknee" value="0">
						<input type="hidden" name="txt_salwar_aroundcalf" id="txt_salwar_aroundcalf" value="0">
						<input type="hidden" name="txt_salwar_aroundankle" id="txt_salwar_aroundankle" value="0">

                                		<input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $int_hdn_price;  ?>" />
                                		<input type="hidden" id="txt_quantity" name="txt_quantity" value="1" />
                                		<input type="hidden" id="hdn_description" name="hdn_description" value="" />
                                		<input type="hidden" id="rdo_shipping" name="rdo_shipping" value="" />
						<?php 
						$skuval=date("YmdHis"); 
						$skuval= GetEncryptId($rs_list->Fields("pkid"));
						?> 
                    				<input type="hidden" id="specssku" name="specssku" value="<?php print $skuval;  ?>" />


                                                <button type="submit" class="product-buynow-button" name='btn_addtocart' id="btn_addtocart_<?php print $rs_list->Fields("pkid"); ?>" style="border-bottom: 1px solid transparent; width: 100%;"><i class="fa fa-shopping-cart"></i></button>

						</form>

                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-buynow-button" title="Buy Now"><b><i class="fa fa-shopping-cart" aria-hidden="true" ></i></b></a>

 
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                <?php 
                                ## --------------------------------------------- START : WISHLIST------------------------------------------
                                if($str_member_flag) 
                                { 
                                    $str_query_select="";
                                    $str_query_select="SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=".$_SESSION["userpkid"]." AND itempkid=".$rs_list->fields("pkid");
                                    $rs_list_fav=GetRecordSet($str_query_select);

                                    $str_fav_class = "";
                                    $str_fav_flag = "";
                                    if($rs_list_fav->Count() == 0) { 
                                        $str_fav_class = "";
                                        $str_fav_flag = "FAV"; 
                                    } else if($rs_list_fav->Count() > 0) { 
                                      $str_fav_class = "text-primary";
                                      $str_fav_flag = "UNFAV"; 
                                    } ?>
                                    <form name="frm_user_favorite2<?php print $rs_list->fields("pkid"); ?>" id="frm_user_favorite2<?php print $rs_list->fields("pkid"); ?>" novalidate>

                                        <input type="hidden" name="userpkid" id="userpkid<?php print($rs_list->fields("pkid")) ?>" value="<?php print($_SESSION["userpkid"]); ?>">
                                        <input type="hidden" name="itempkid" id="itempkid<?php print($rs_list->fields("pkid")) ?>" value="<?php print($rs_list->fields("pkid")) ?>">

                                        <input type="hidden" name="flgfav" id="flgfav<?php print($rs_list->fields("pkid")) ?>" value="<?php print($str_fav_flag); ?>">



                                      <?php //print $rs_list_fav->Count(); ?>
                                        <button id="frm_submit2<?php print($rs_list->fields("pkid")) ?>" name="frm_submit2<?php print($rs_list->fields("pkid")) ?>" type="submit" style="display: none;" ></button>
                                    </form>

                                    <a onclick="$('#frm_submit2<?php print($rs_list->fields("pkid")) ?>').click();" class="product-button <?php print $str_fav_class; ?>"><i class="fa fa-heart"></i></a>
                                    <script>
                                        $(function() {
                                            $("#frm_user_favorite2<?php print $rs_list->fields("pkid"); ?> input").jqBootstrapValidation({
                                                preventSubmit: true,
                                                submitError: function($form, event, errors) {
                                                    // something to have when submit produces an error ?
                                                    // Not decided if I need it yet
                                                },
                                                submitSuccess: function($form, event) {
                                                    event.preventDefault(); // prevent default submit behaviour
                                                    // get values from FORM

                                                    var userpkid = $("input#userpkid<?php print($rs_list->fields("pkid")) ?>").val();
                                                    //alert(userpkid);
                                                    var itempkid = $("input#itempkid<?php print($rs_list->fields("pkid")) ?>").val();
                                                    var flgfav = $("input#flgfav<?php print($rs_list->fields("pkid")) ?>").val();


                                                    $.ajax({

                                                        url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                                        type: "POST",
                                                        data: 
                                                        {

                                                            userpkid : userpkid,
                                                            itempkid : itempkid,
                                                            flgfav : flgfav
                                                        },
                                                        cache: false,
                                                        success: function(data) 
                                                        {
                                                        //alert(data);
                                                            var $responseText = JSON.parse(data);
                                                            if($responseText.status == 'SUC')
                                        //                                        if(($responseText.status).equal("SUC"))
                                                            {
                                                                // Success message
                                        //                        alert(data);

                                                                $('#success2<?php print $rs_list->fields("pkid"); ?>').html("<div class='alert alert-success small'>");
                                                                $('#success2<?php print $rs_list->fields("pkid"); ?> > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                .append("</button>");
                                                                $('#success2<?php print $rs_list->fields("pkid"); ?> > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                                $('#success2<?php print $rs_list->fields("pkid"); ?> > .alert-success ').append('</div>');
                          //setTimeout(function(){ location.reload() }, 2000);
                                                                    setTimeout(function(){ location.reload(); }, 5000);
                                                            }
                                                            else if($responseText.status == 'ERR')
                                                            {
                                                                // Fail message
                                                                $('#success2<?php print $rs_list->fields("pkid"); ?>').html("<div class='alert alert-danger'>");
                                                                $('#success2<?php print $rs_list->fields("pkid"); ?> > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                        .append("</button>");
                                                                $('#success2<?php print $rs_list->fields("pkid"); ?> > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                                $('#success2<?php print $rs_list->fields("pkid"); ?> > .alert-danger').append('</div>');

                                                            }
                                                        },

                                                    })
                                                },
                                                filter: function() {
                                                    return $(this).is(":visible");
                                                },
                                            });

                                            $("a[data-toggle=\"tab\"]").click(function(e) {
                                                e.preventDefault();
                                                $(this).tab("show");
                                            });
                                        });
                                        /*When clicking on Full hide fail/success boxes */
                                        $('#name').focus(function() {
                                            $('#success').html('');
                                        });

                                    </script>
                                <?php } else { ?>
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>" class="product-button"><i class="fa fa-heart"></i></a>
                                <?php } 
                                ## --------------------------------------------- END : WISHLIST------------------------------------------ ?>    
                                </div>
                            </div>
                            
                        </div>
                    </div>    
                </div>
            </div>
            <?php
            $int_cnt++; if($int_cnt%3 == 0) { print "</div><div class='row padding-10 hidden-lg hidden-md hidden-xs'>"; }
            $rs_list->MoveNext();
            }$rs_list->MoveFirst();
            ?>
        </div>
        <?php } else { ?>
        <div class="row padding-10 hidden-md hidden-lg hidden-xs">
            <div class="col-sm-12 text-center alert alert-danger"><?php print $STR_MSG_NO_DATA_AVAILABLE; ?></div>
        </div>
        <?php } ?>
        <?php if($rs_list->Count() > 0) { ?>
        <div class="row padding-10 hidden-md hidden-lg hidden-sm">
            <?php 
            $int_cnt = 0;
            while(!$rs_list->EOF()) { 
            $str_class = "product-box";
                $str_class_bg = "product-content-bg";
                if($rs_list->Fields("displayasfeatured") == "YES") { $str_class = "product-box-featured"; $str_class_bg = "product-content-bg-featured"; }
                if($rs_list->Fields("displayashot") == "YES") { $str_class = "product-box-trending"; $str_class_bg = "product-content-bg-trending"; }  ?>
            <div class="col-xs-6">
                <div class="<?php print $str_class; ?> text-center">
                    <div class="row padding-10">
                        <div class="col-md-12 nopadding" align="center">
                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" title="<?php print $rs_list->Fields("title"); ?>">
                                <?php if($rs_list->Fields("thumbphotofilename") != "") { ?>
                                    <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                <?php } else if($rs_list->Fields("imageurl") != "") { ?>
                                    <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $rs_list->Fields("imageurl"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                <?php } ?>
                            </a>
                            
                            <?php 
                            $str_query_select = "";
                            $str_query_select = "SELECT mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='LEFT' AND visible='YES' ORDER BY setasofferimage ASC";
                            $rs_list_offerimages_left = GetRecordSet($str_query_select);
                            //print $rs_list_offerimages->Count();
                            if($rs_list_offerimages_left->Count() > 0) {
                            ?>
                            
                            <img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list_offerimages_left->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-left"/>
                            <?php }  ?>
                            <?php 
                            $str_query_select = "";
                            $str_query_select = "SELECT mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='RIGHT' AND visible='YES' ORDER BY setasofferimage ASC";
                            $rs_list_offerimages_right = GetRecordSet($str_query_select);
                            //print $rs_list_offerimages->Count();
                            if($rs_list_offerimages_right->Count() > 0) {
                            ?>
                            <img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list_offerimages_right->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-right"/>
                            <?php }  ?>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-12 <?php print $str_class_bg; ?>">
                            <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" title="<?php print $rs_list->Fields("title"); ?>"><b class="text-muted"><?php print MakeStringShort($rs_list->Fields("title"), 18); ?></b></a></p>
                            <?php /* ?>
                             <p>
                                <?php if($rs_list->Fields("displayasnew") == 'YES') { print $STR_ICON_PATH_NEW; } ?><?php if($rs_list->Fields("displayasfeatured") == 'YES') { print "&nbsp;".$STR_ICON_PATH_FEATURED; } ?><?php if($rs_list->Fields("displayashot") == 'YES') { print "&nbsp;".$STR_ICON_PATH_HOT; } ?>
                            </p>
                            <?php */ ?>
                            <p>
                                <?php if($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) { ?>
                                    <?php 
                                        $int_wholesaler_price = "";
                                        $int_wholesaler_price = $rs_list->Fields("memberprice") / $int_conversionrate; ?>
                                        <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_wholesaler_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                <?php } else {  ?>
                                    <?php if($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                        $int_list_price = "";
                                        $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;

                                        $int_our_price = "";
                                        $int_our_price = $rs_list->Fields("ourprice") / $int_conversionrate;
                                        ?>
                                        <strike class="text-muted"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 0)."&nbsp;".$str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                        <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_our_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                        <?php
                                        $int_per_discount = 0;
                                        $int_per_discount = (100 - ($rs_list->Fields("ourprice")/$rs_list->Fields("listprice")) * 100); 
                                        if($int_per_discount > 0) { ?>
                                        <span class="label label-primary"><b><?php print number_format($int_per_discount, 2); ?>% OFF</b></span> 
                                        <?php } ?>
                                    <?php } else {
                                        $int_list_price = "";
                                        $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate; ?>

                                            <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                        <?php }  ?>    
                                <?php }  ?> 
                                    
                            </p>
                        </div>
                    </div>
                    <div id="success3<?php print $rs_list->fields("pkid"); ?>"></div>
                    <div class="row padding-10">
                        <div class="col-md-12">
                            <div class="row padding-10">
                                <?php /*?><div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-button" title="Add to Cart"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a> 
                                </div><?php */?>
                                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">

					<form class='myform' id="frm_add<?php print $rs_list->Fields("pkid"); ?>" name="frm_add<?php print $rs_list->Fields("pkid"); ?>" novalidate>
						<input type="hidden" name="hdn_prodpkid" id="hdn_prodpkid" value="<?php print $rs_list->Fields("pkid"); ?>">
						<input type="hidden" name="hdn_prodtitle" id="hdn_prodtitle" value="<?php print $rs_list->Fields("title"); ?>">

						<?php /* ?>
						<input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $int_cat_pkid; ?>">
						<input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $int_sub_cat_pkid; ?>">
						<?php */ ?>
						<input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $rs_list->Fields("catpkid"); ?>">
						<input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $rs_list->Fields("subcatpkid"); ?>">

						<input type="hidden" name="hdn_weight" id="hdn_weight" value="<?php print $rs_list->Fields("weight"); ?>">
						<input type="hidden" name="hdn_userpkid" id="hdn_userpkid" value="<?php print $int_userpkid; ?>">
						<input type="hidden" name="hdn_username" id="hdn_username" value="<?php print $str_username; ?>">
						<input type="hidden" name="hdn_discount" id="hdn_discount" value="<?php print $int_wholesaler_discount; ?>">
						<input type="hidden" name="hdn_referredbypkid" id="hdn_referredbypkid" value="<?php print $int_referredbypkid; ?>">
						<input type="hidden" name="hdn_commission" id="hdn_commission" value="<?php print $int_commission; ?>">
						<input type="hidden" name="rdo_color" id="rdo_color" value="<?php print $rs_list->Fields("colortitle"); ?>">
						<input type="hidden" name="rdo_size" id="rdo_size" value="<?php print $rs_list->Fields("sizetitle"); ?>">
						<input type="hidden" name="rdo_tailoringoption" id="rdo_tailoringoption" value="<?php print $int_tailoring_service_pkid; ?>">
						<input type="hidden" name="cbo_semistitched_stitchingsize" id="cbo_semistitched_stitchingsize" value="0">
						<input type="hidden" name="txt_blouse_length" id="txt_blouse_length" value="0">
						<input type="hidden" name="txt_blouse_sleeve_length" id="txt_blouse_sleeve_length" value="0">
						<input type="hidden" name="cbo_blouse_aroundbust" id="cbo_blouse_aroundbust" value="0">
						<input type="hidden" name="txt_blouse_abovearoundwaist" id="txt_blouse_abovearoundwaist" value="0">
						<input type="hidden" name="txt_blouse_frontneckdepth" id="txt_blouse_frontneckdepth" value="0">
						<input type="hidden" name="txt_blouse_backneckdepth" id="txt_blouse_backneckdepth" value="0">
						<input type="hidden" name="txt_lehenga_aroundwaist" id="txt_lehenga_aroundwaist" value="0">
						<input type="hidden" name="txt_lehenga_aroundhips" id="txt_lehenga_aroundhips" value="0">
						<input type="hidden" name="txt_lehenga_length" id="txt_lehenga_length" value="0">
						<input type="hidden" name="txt_salwar_yourheight" id="txt_salwar_yourheight" value="0">
						<input type="hidden" name="txt_salwar_frontneckdepth" id="txt_salwar_frontneckdepth" value="0">
						<input type="hidden" name="txt_salwar_bottomlength" id="txt_salwar_bottomlength" value="0">
						<input type="hidden" name="txt_salwar_kameezlength" id="txt_salwar_kameezlength" value="0">
						<input type="hidden" name="txt_salwar_sleevelength" id="txt_salwar_sleevelength" value="0">
						<input type="hidden" name="cbo_salwar_sleevestyle" id="cbo_salwar_sleevestyle" value="0">
						<input type="hidden" name="cbo_salwar_aroundbust" id="cbo_salwar_aroundbust" value="0">
						<input type="hidden" name="txt_salwar_abovearoundwaist" id="txt_salwar_abovearoundwaist" value="0">
						<input type="hidden" name="txt_salwar_aroundwaist" id="txt_salwar_aroundwaist" value="0">
						<input type="hidden" name="txt_salwar_aroundhips" id="txt_salwar_aroundhips" value="0">
						<input type="hidden" name="txt_salwar_backneckdepth" id="txt_salwar_backneckdepth" value="0">
						<input type="hidden" name="txt_salwar_aroundthigh" id="txt_salwar_aroundthigh" value="0">
						<input type="hidden" name="txt_salwar_aroundknee" id="txt_salwar_aroundknee" value="0">
						<input type="hidden" name="txt_salwar_aroundcalf" id="txt_salwar_aroundcalf" value="0">
						<input type="hidden" name="txt_salwar_aroundankle" id="txt_salwar_aroundankle" value="0">

                                		<input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $int_hdn_price;  ?>" />
                                		<input type="hidden" id="txt_quantity" name="txt_quantity" value="1" />
                                		<input type="hidden" id="hdn_description" name="hdn_description" value="" />
                                		<input type="hidden" id="rdo_shipping" name="rdo_shipping" value="" />
						<?php 
						$skuval=date("YmdHis"); 
						$skuval= GetEncryptId($rs_list->Fields("pkid"));
						?> 
                    				<input type="hidden" id="specssku" name="specssku" value="<?php print $skuval;  ?>" />


                                                <button type="submit" class="product-buynow-button" name='btn_addtocart' id="btn_addtocart_<?php print $rs_list->Fields("pkid"); ?>" style="border-bottom: 1px solid transparent; width: 100%;"><i class="fa fa-shopping-cart"></i></button>

						</form>
                                    <?php /* ?><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-buynow-button" title="Buy Now"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a><?php */ ?>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                <?php 
                                ## --------------------------------------------- START : WISHLIST------------------------------------------
                                if($str_member_flag) 
                                { 
                                    $str_query_select="";
                                    $str_query_select="SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=".$_SESSION["userpkid"]." AND itempkid=".$rs_list->fields("pkid");
                                    $rs_list_fav=GetRecordSet($str_query_select);

                                    $str_fav_class = "";
                                    $str_fav_flag = "";
                                    if($rs_list_fav->Count() == 0) { 
                                        $str_fav_class = "";
                                        $str_fav_flag = "FAV"; 
                                    } else if($rs_list_fav->Count() > 0) { 
                                      $str_fav_class = "text-primary";
                                      $str_fav_flag = "UNFAV"; 
                                    } ?>
                                    <form name="frm_user_favorite3<?php print $rs_list->fields("pkid"); ?>" id="frm_user_favorite3<?php print $rs_list->fields("pkid"); ?>" novalidate>

                                        <input type="hidden" name="userpkid" id="userpkid<?php print($rs_list->fields("pkid")) ?>" value="<?php print($_SESSION["userpkid"]); ?>">
                                        <input type="hidden" name="itempkid" id="itempkid<?php print($rs_list->fields("pkid")) ?>" value="<?php print($rs_list->fields("pkid")) ?>">

                                        <input type="hidden" name="flgfav" id="flgfav<?php print($rs_list->fields("pkid")) ?>" value="<?php print($str_fav_flag); ?>">



                                      <?php //print $rs_list_fav->Count(); ?>
                                        <button id="frm_submit3<?php print($rs_list->fields("pkid")) ?>" name="frm_submit3<?php print($rs_list->fields("pkid")) ?>" type="submit" style="display: none;" ></button>
                                    </form>

                                    <a onclick="$('#frm_submit3<?php print($rs_list->fields("pkid")) ?>').click();" class="product-button <?php print $str_fav_class; ?>"><i class="fa fa-heart"></i></a>
                                    <script>
                                        $(function() {
                                            $("#frm_user_favorite3<?php print $rs_list->fields("pkid"); ?> input").jqBootstrapValidation({
                                                preventSubmit: true,
                                                submitError: function($form, event, errors) {
                                                    // something to have when submit produces an error ?
                                                    // Not decided if I need it yet
                                                },
                                                submitSuccess: function($form, event) {
                                                    event.preventDefault(); // prevent default submit behaviour
                                                    // get values from FORM

                                                    var userpkid = $("input#userpkid<?php print($rs_list->fields("pkid")) ?>").val();
                                                    //alert(userpkid);
                                                    var itempkid = $("input#itempkid<?php print($rs_list->fields("pkid")) ?>").val();
                                                    var flgfav = $("input#flgfav<?php print($rs_list->fields("pkid")) ?>").val();


                                                    $.ajax({

                                                        url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                                        type: "POST",
                                                        data: 
                                                        {

                                                            userpkid : userpkid,
                                                            itempkid : itempkid,
                                                            flgfav : flgfav
                                                        },
                                                        cache: false,
                                                        success: function(data) 
                                                        {
                                                        //alert(data);
                                                            var $responseText = JSON.parse(data);
                                                            if($responseText.status == 'SUC')
                                        //                                        if(($responseText.status).equal("SUC"))
                                                            {
                                                                // Success message
                                        //                        alert(data);

                                                                $('#success3<?php print $rs_list->fields("pkid"); ?>').html("<div class='alert alert-success small'>");
                                                                $('#success3<?php print $rs_list->fields("pkid"); ?> > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                .append("</button>");
                                                                $('#success3<?php print $rs_list->fields("pkid"); ?> > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                                $('#success3<?php print $rs_list->fields("pkid"); ?> > .alert-success ').append('</div>');
                          //setTimeout(function(){ location.reload() }, 2000);
                                                                    setTimeout(function(){ location.reload(); }, 5000);
                                                            }
                                                            else if($responseText.status == 'ERR')
                                                            {
                                                                // Fail message
                                                                $('#success3<?php print $rs_list->fields("pkid"); ?>').html("<div class='alert alert-danger'>");
                                                                $('#success3<?php print $rs_list->fields("pkid"); ?> > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                        .append("</button>");
                                                                $('#success3<?php print $rs_list->fields("pkid"); ?> > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                                $('#success3<?php print $rs_list->fields("pkid"); ?> > .alert-danger').append('</div>');

                                                            }
                                                        },

                                                    })
                                                },
                                                filter: function() {
                                                    return $(this).is(":visible");
                                                },
                                            });

                                            $("a[data-toggle=\"tab\"]").click(function(e) {
                                                e.preventDefault();
                                                $(this).tab("show");
                                            });
                                        });
                                        /*When clicking on Full hide fail/success boxes */
                                        $('#name').focus(function() {
                                            $('#success').html('');
                                        });

                                    </script>
                                <?php } else { ?>
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>" class="product-button"><i class="fa fa-heart"></i></a>
                                <?php } 
                                ## --------------------------------------------- END : WISHLIST------------------------------------------ ?>    
                                </div>
                            </div>
                            
                        </div>
                    </div>    
                </div>
            </div>
            <?php
            $int_cnt++; if($int_cnt%2 == 0) { print "</div><div class='row padding-10 hidden-md hidden-lg hidden-sm'>"; }
            $rs_list->MoveNext();
            }$rs_list->MoveFirst();
            ?>
        </div>
        <?php } else { ?>
        <div class="row padding-10 hidden-sm hidden-md hidden-lg">
            <div class="col-xs-12 text-center alert alert-danger"><?php print $STR_MSG_NO_DATA_AVAILABLE; ?></div>
        </div>
        <?php } ?>
        <div class="row padding-10 hidden-lg hidden-md">
            <div class="col-sm-12 col-xs-12">
                <!-- Small modal -->
                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" style="bottom: 0px !important; top: auto">
                  <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                      <div class="modal-header"> 
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                          <h4 class="modal-title text-center" id="mySmallModalLabel">SORT BY</h4> 
                      </div>
                        <div class="modal-body">
                            
                            
                            <?php
                            
                            $str_active_sorting_low_to_high = "text-muted";
                            $str_active_sorting_high_to_low = "text-muted";
                            $str_active_sorting_new = "text-muted";
                            $str_active_sorting_popular = "text-muted";
                            
                            if(isset($_GET["sortby"]) && $_GET["sortby"] == "price_asc") { $str_active_sorting_low_to_high = "text-primary"; }  
                            if(isset($_GET["sortby"]) && $_GET["sortby"] == "price_desc") { $str_active_sorting_high_to_low = "text-primary"; } 
                            if(isset($_GET["sortby"]) && $_GET["sortby"] == "new") { $str_active_sorting_new = "text-primary"; } 
                            if(isset($_GET["sortby"]) && $_GET["sortby"] == "popular") { $str_active_sorting_popular = "text-primary"; } 
                            ?>
                            <p class="text-center">
                                <a href="?sortby=default" class="btn btn-danger btn-xs"><span aria-hidden="true">Reset</span></a>
                            </p>
                            <hr/>
                                <p class="text-center">
                                    <a href="?sortby=price_asc" title="Sort By Price: Low To High"><b class="<?php print $str_active_sorting_low_to_high; ?>">Price: Low To High</b></a>
                                </p>
                            <hr/>
                                <p class="text-center">
                                    <a href="?sortby=price_desc" title="Sort By Price: High To Low"><b class="<?php print $str_active_sorting_high_to_low; ?>">Price: High To Low</b></a>
                                </p>
                            <hr/>
                                <p class="text-center">
                                    <a href="?sortby=new" title="Sort By New"><b class="<?php print $str_active_sorting_new; ?>">New</b></a>
                                </p>
                            <hr/>
                                <p class="text-center">
                                    <a href="?sortby=popular" title="Sort By Popular"><b class="<?php print $str_active_sorting_popular; ?>">Popular</b></a>
                                </p>
                            <hr/>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="hidden-lg hidden-md ">
            <div class="page-wrapper chiller-theme">
            <nav id="sidebar" class="sidebar-wrapper">
                <div class="sidebar-content">
                    <div class="sidebar-brand">
                        <a><h4 class="nopadding"><i class="fa fa-filter"></i>&nbsp;Filter By</h4></a>
                        <div id="close-sidebar">
                            <i class="fa fa-times-circle"></i>
                        </div>
                    </div>
                    <div class="sidebar-menu">

                        <div class="panel-body" style="padding-top: 0px;">

                            <form id="frm_filter2" name="frm_filter2" method="POST">
                                <?php 

                                $str_where_for_filter = "";
                                if(isset($_GET['cid']) && isset($_GET['sid']) && $_GET['sid']!="")
                                { 
                                    $str_where_for_filter = " AND c.catpkid = ".$int_cat_pkid. " AND c.subcatpkid = ".$int_sub_cat_pkid." ";
                                }
                                else if(isset($_GET['cid'])) {
                                    $str_where_for_filter = " AND c.catpkid = ".$int_cat_pkid. " ";
                                }?>    


                                <?php 
                                ## --------------------------------START: SHOP BY COLOR------------------------------------
                                $str_query_select = "";
                                $str_query_select = "SELECT DISTINCT b.title, b.cmyk_code ";
                                $str_query_select.= "FROM ".$STR_DB_TR_TABLE_NAME_COLOR." a ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_COLOR." b ON b.pkid=a.masterpkid ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME." c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                                $str_query_select.= "WHERE b.visible='YES'".$str_where_for_filter." ";
                                $str_query_select.= "ORDER BY b.title ASC";
                                //print $str_query_select;
                                $rs_list_color_filter = GetRecordSet($str_query_select);
                                if($rs_list_color_filter->Count() > 0) { ?>    
                                <div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="filter-head-bg">
                                            <h3 class="panel-title"><b>SHOP BY COLOR</b></h3>
                                        </div>
                                        
                                        <?php /* ?><style>
                                               .bg-check {position:absolute;display:none;margin:9px -25px 0px;}
                                               .circle {background-color:#f1f1f1;color:#FFFFFF;display: inline-block;width: 30px;height: 30px;margin: 0 2px; text-align: center;font-size: 18px;font-family: 'Kavoon', cursive;line-height: 28px;-webkit-transition: all 0.2s ease-in-out; -moz-transition: all 0.2s ease-in-out; -ms-transition: all 0.2s ease-in-out;
                                                -o-transition: all 0.2s ease-in-out; transition: all 0.2s ease-in-out; cursor:pointer;-webkit-border-radius: 50%;-moz-border-radius: 50%;border-radius: 50%;}
                                               input[id="cbx_fltr_color2"] {visibility: hidden;position: absolute;}
                                               input[id="cbx_fltr_color2"] + img { opacity: 1;filter: alpha(opacity=1); }
                                               input[id="cbx_fltr_color2"]:checked + img { opacity: 0.4; }
                                               input[id="cbx_fltr_color2"]:checked ~ span { display: inline-block; }
                                        </style><?php */ ?>
                                        
                                        <ul class="chec-radio03">
                                <?php
                                //print_r($_POST);
                                $int_cnt = 1;
                                while(!$rs_list_color_filter->EOF()) { ?>
                                <style>
                                            /*--------------------Filter color Select Css---------------------*/
                                    ul.chec-radio03 {margin: 0px;margin-left: 0px;}
                                    ul.chec-radio03 li.pz03 {display: inline;}
                                    .chec-radio03 label.radio-inline03 input[id="cbx_fltr_color2"] {display: none;}
                                    .chec-radio03 label.radio-inline03 input[id="cbx_fltr_color2"]:checked+div {color: #fff;padding-left: 10px;padding-right:10px;border:2px solid #F5F5F5;color: #fff;  -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: #939393;}
                                    .chec-radio03 .radio-inline03 .clab03 {cursor: pointer;padding: 7px 20px;text-align: center;text-transform: uppercase;color: #fff;position: relative;height: 35px;float: left;margin: 0;margin-bottom: 5px;border: 2px solid #F5F5F5;}
                                    .chec-radio03 label.radio-inline03 input[id="cbx_fltr_color2"]:checked+div:before {content: "\e013";margin-right: 5px;font-family: 'Glyphicons Halflings';}
/*--------------------End Filter color Select Css---------------------*/
                                            
                                </style>
                                    <?php
                                    $str_fltr_color_checked = "";
                                    //print $arr_filter_color;
                                    if(in_array($rs_list_color_filter->Fields("title"), $arr_filter_color)) { $str_fltr_color_checked = "checked";  } ?>
                                    <li class="pz03">
                                        <label class="radio-inline03">
                                            <input type="checkbox" name="cbx_fltr_color2[]" id="cbx_fltr_color2" tabindex="1" class="pro-chx03" value="<?php print $rs_list_color_filter->Fields("title"); ?>" aria-invalid="false" <?php print $str_fltr_color_checked; ?> />                                                                            <div class="clab03" style="background: <?php print $rs_list_color_filter->Fields("cmyk_code"); ?>;"></div>
                                        </label>
                                    </li>
                                
                                <?php $int_cnt++;
                                $rs_list_color_filter->MoveNext();
                                } ?>
                                </ul>
                                    </div>
                                </div>
                                <?php } ?>  
                                <?php  ## --------------------------------END: SHOP BY COLOR--------------------------------------?>
                                <?php  ## --------------------------------START: SHOP BY MATERIAL---------------------------------?> 
                                <?php 
                                $str_query_select = "";
                                $str_query_select = "SELECT DISTINCT b.title ";
                                $str_query_select.= "FROM ".$STR_DB_TR_TABLE_NAME_FABRIC." a ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_FABRIC." b ON b.pkid=a.masterpkid ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME." c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                                $str_query_select.= "WHERE b.visible='YES'".$str_where_for_filter." ";
                                $str_query_select.= "ORDER BY b.title ASC";

                                //print $str_query_select;
                                $rs_list_material_filter = GetRecordSet($str_query_select);
                                if($rs_list_material_filter->Count() > 0) { ?>    
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="filter-head-bg">
                                                <h3 class="panel-title"><b> SHOP BY MATERIAL</b> </h3>
                                            </div>
                                            <?php
                                            while(!$rs_list_material_filter->EOF()) { ?>
                                            <div class="form-group">
                                                
                                                    <label class="control control--checkbox label-normal">
                                                        <?php
                                                    $str_fltr_material_checked = "";
                                                    if(in_array($rs_list_material_filter->Fields("title"), $arr_filter_material)) { $str_fltr_material_checked = "checked";  } ?>
                                                        <input type="checkbox" name="cbx_fltr_material2[]" id="cbx_fltr_material2" value="<?php print $rs_list_material_filter->Fields("title"); ?>" <?php print $str_fltr_material_checked; ?>>&nbsp;<?php print $rs_list_material_filter->Fields("title"); ?> 
                                                        <div class="control__indicator" style="top:3px;"></div>
                                                    </label>
                                                <hr/>
                                            </div>
                                            <?php
                                            $rs_list_material_filter->MoveNext();
                                            } ?>
                                        </div>
                                    </div>
                                <?php 
                                }
                                ?>    
                                <?php  ## --------------------------------END: SHOP BY MATERIAL-----------------------------------?>    

                                <?php  ## --------------------------------START: SHOP BY TYPE---------------------------------?> 
                                <?php 
                                $str_query_select = "";
                                $str_query_select = "SELECT DISTINCT b.title ";
                                $str_query_select.= "FROM ".$STR_DB_TR_TABLE_NAME_TYPE." a ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_TYPE." b ON b.pkid=a.masterpkid ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME." c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                                $str_query_select.= "WHERE b.visible='YES'".$str_where_for_filter." ";
                                $str_query_select.= "ORDER BY b.title ASC";
                                //print $str_query_select;exit;
                                $rs_list_type_filter = GetRecordSet($str_query_select);
                                //print $rs_list_type_filter->Count();
                                if($rs_list_type_filter->Count() > 0) { ?>    

                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="filter-head-bg">
                                                <h3 class="panel-title"><b> SHOP BY TYPE</b></h3>
                                            </div>
                                            <?php
                                            while(!$rs_list_type_filter->EOF()) { ?>
                                            <div class="form-group">
                                                  <label class="control control--checkbox label-normal">
                                                        <?php
                                                    $str_fltr_type_checked = "";
                                                    if(in_array($rs_list_type_filter->Fields("title"), $arr_filter_type)) { $str_fltr_type_checked = "checked";  } ?>
                                                        <input type="checkbox" name="cbx_fltr_type2[]" id="cbx_fltr_type2" value="<?php print $rs_list_type_filter->Fields("title"); ?>" <?php print $str_fltr_type_checked; ?>>&nbsp;<?php print $rs_list_type_filter->Fields("title"); ?>
                                                        <div class="control__indicator" style="top:3px;"></div>
                                                    </label>
                                                <hr/>
                                            </div>
                                            <?php
                                            $rs_list_type_filter->MoveNext();
                                            } ?>
                                        </div>
                                    </div>
                                <?php 
                                }
                                ?>    
                                <?php  ## --------------------------------END: SHOP BY TYPE-----------------------------------?> 

                                <?php  ## --------------------------------START: SHOP BY SIZE---------------------------------?> 
                                <?php 
                                $str_query_select = "";
                                $str_query_select = "SELECT DISTINCT b.title ";
                                $str_query_select.= "FROM ".$STR_DB_TR_TABLE_NAME_SIZE." a ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_SIZE." b ON b.pkid=a.masterpkid ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME." c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                                $str_query_select.= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                                $str_query_select.= "WHERE b.visible='YES'".$str_where_for_filter." ";
                                $str_query_select.= "ORDER BY b.title ASC";
                                //print $str_query_select;exit;
                                $rs_list_size_filter = GetRecordSet($str_query_select);
                                //print $rs_list_type_filter->Count();
                                if($rs_list_size_filter->Count() > 0) { ?>    
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="filter-head-bg">
                                                <h3 class="panel-title"><b> SHOP BY SIZE</b></h3>
                                            </div>
                                            <?php
                                            while(!$rs_list_size_filter->EOF()) { ?>
                                            <div class="form-group">
                                                
                                                    <label class="control control--checkbox label-normal">
                                                        <?php
                                                    $str_fltr_size_checked = "";
                                                    if(in_array($rs_list_size_filter->Fields("title"), $arr_filter_size)) { $str_fltr_size_checked = "checked";  } ?>
                                                        <input type="checkbox" name="cbx_fltr_size2[]" id="cbx_fltr_size2" value="<?php print $rs_list_size_filter->Fields("title"); ?>" <?php print $str_fltr_size_checked; ?>>&nbsp;<?php print $rs_list_size_filter->Fields("title"); ?>
                                                        <div class="control__indicator" style="top:3px;"></div>
                                                    </label>
                                                <hr/>
                                            </div>
                                            <?php
                                            $rs_list_size_filter->MoveNext();
                                            } ?>
                                        </div>
                                    </div>
                                <?php 
                                }
                                ?>    
                                <?php  ## --------------------------------END: SHOP BY SIZE-----------------------------------?> 
                                
                                <?php  ## --------------------------------START: SHOP BY PRICE-----------------------------------?>
                                <?php  ## --------------------------------END: SHOP BY PRICE-----------------------------------?>    
                            
                                <?php /*?><div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><b>SHOP BY PRICE</b></h3>
                                        </div>
                                        <hr/>
                                        <label>
                                            <div class="form-group">
                                                <label>Min</label>
                                                <input type="number" class="form-control" id="inputEmail4" placeholder="$0">
                                            </div>
                                        </label>
                                        <label>
                                            <div class="form-group">
                                                <label>Max</label>
                                                <input type="number" class="form-control" placeholder="$1,0000">
                                              </div>
                                        </label>
                                    </div>
                                </div><?php */?>
                                 <div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="filter-head-bg">
                                            <h3 class="panel-title"><b>SHOP BY PRICE</b></h3>
                                        </div>

                                        <?php //print_r($_POST); ?>
                                        <div class="col-xs-12 col-sm-12">
                                            <br/>
                                            <div class="double-handle-slider2"></div>
                                            <br/>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <input id="txt_min_price" name="txt_min_price" type="number" class="min-value form-control input-sm" value="<?php if($int_min_price > 0) { print $int_min_price; } else { print 100;} ?>" placeholder="100" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="form-group">
                                                <input id="txt_max_price" name="txt_max_price" type="number" class="max-value form-control input-sm" value="<?php if($int_max_price > 0) { print $int_max_price; } else { print 50000; } ?>" placeholder="50000" />
                                            </div>
                                        </div>
                                    </div>
                                </div><hr/>
                                <?php  ## --------------------------------END: SHOP BY PRICE-----------------------------------?>    
                                <div class="row padding-10">
                                    <div class="col-xs-6 col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-block">Apply</button> 
                                    </div>
                                    <div class="col-xs-6 col-sm-6">
                                        <button type="reset" class="btn btn-default btn-block">Clear</button> 
                                    </div>
                                </div><br/>
                            </form>
                        </div>
                    </div>
                </div>
            </nav>
            </div>  
        </div>
    </div>
    
    
    <div class="footer-bottom-margin">
        <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    </div>    
    <div class="row padding-10 hidden-lg hidden-md">
        <div class="col-xs-6 col-sm-6">
            <a href="" class="btn-filter" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-long-arrow-up" aria-hidden="true"></i><i class="fa fa-long-arrow-down" aria-hidden="true"></i>&nbsp;<b>Sort</b></a>
        </div>
        <div class="col-xs-6 col-sm-6">
            <a id="show-sidebar" class="" href="#">
                <i class="fa fa-filter" aria-hidden="true"></i>&nbsp;<b>Filter</b>
            </a>
        </div>
    </div>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/popper.min.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
    <script>
        jQuery(function ($) {

        $(".sidebar-dropdown > a").click(function() {
      $(".sidebar-submenu").slideUp(200);
      if (
        $(this)
          .parent()
          .hasClass("active")
      ) {
        $(".sidebar-dropdown").removeClass("active");
        $(this)
          .parent()
          .removeClass("active");
      } else {
        $(".sidebar-dropdown").removeClass("active");
        $(this)
          .next(".sidebar-submenu")
          .slideDown(200);
        $(this)
          .parent()
          .addClass("active");
      }
    });

    $("#close-sidebar").click(function() {
      $(".page-wrapper").removeClass("toggled");
    });
    $("#show-sidebar").click(function() {
      $(".page-wrapper").addClass("toggled");
    });




    });
    </script>
    <script type="text/javascript">  
        $(function(){
            $('input[id=cbx_fltr_color]').on('change',function(){
             //alert("hi");

                $('#frm_filter').submit();
            });
        });

        $(function(){
            $('input[id=cbx_fltr_material]').on('change',function(){
             //alert("hi");
                $('#frm_filter').submit();
            });
        });
        $(function(){
            $('input[id=cbx_fltr_type]').on('change',function(){
             //alert("hi");
                $('#frm_filter').submit();
            });
        });
        $(function(){
            $('input[id=cbx_fltr_size]').on('change',function(){
             //alert("hi");
                $('#frm_filter').submit();
            });
        });
        // For Mobile 
        /*$(function(){
            $('input[id=cbx_fltr_color2]').on('change',function(){
             //alert("hi");
                $('#frm_filter2').submit();
            });
        });

        $(function(){
            $('input[id=cbx_fltr_material2]').on('change',function(){
             //alert("hi");
                $('#frm_filter2').submit();
            });
        });
        $(function(){
            $('input[id=cbx_fltr_type2]').on('change',function(){
             //alert("hi");
                $('#frm_filter2').submit();
            });
        });
        $(function(){
            $('input[id=cbx_fltr_size2]').on('change',function(){
             //alert("hi");
                $('#frm_filter2').submit();
            });
        });*/
    </script>
    <script>
        /*! nouislider - 8.1.0 - 2015-10-25 16:05:43 */

!function(a){"function"==typeof define&&define.amd?define([],a):"object"==typeof exports?module.exports=a():window.noUiSlider=a()}(function(){"use strict";function a(a){return a.filter(function(a){return this[a]?!1:this[a]=!0},{})}function b(a,b){return Math.round(a/b)*b}function c(a){var b=a.getBoundingClientRect(),c=a.ownerDocument,d=c.documentElement,e=m();return/webkit.*Chrome.*Mobile/i.test(navigator.userAgent)&&(e.x=0),{top:b.top+e.y-d.clientTop,left:b.left+e.x-d.clientLeft}}function d(a){return"number"==typeof a&&!isNaN(a)&&isFinite(a)}function e(a){var b=Math.pow(10,7);return Number((Math.round(a*b)/b).toFixed(7))}function f(a,b,c){j(a,b),setTimeout(function(){k(a,b)},c)}function g(a){return Math.max(Math.min(a,100),0)}function h(a){return Array.isArray(a)?a:[a]}function i(a){var b=a.split(".");return b.length>1?b[1].length:0}function j(a,b){a.classList?a.classList.add(b):a.className+=" "+b}function k(a,b){a.classList?a.classList.remove(b):a.className=a.className.replace(new RegExp("(^|\\b)"+b.split(" ").join("|")+"(\\b|$)","gi")," ")}function l(a,b){a.classList?a.classList.contains(b):new RegExp("(^| )"+b+"( |$)","gi").test(a.className)}function m(){var a=void 0!==window.pageXOffset,b="CSS1Compat"===(document.compatMode||""),c=a?window.pageXOffset:b?document.documentElement.scrollLeft:document.body.scrollLeft,d=a?window.pageYOffset:b?document.documentElement.scrollTop:document.body.scrollTop;return{x:c,y:d}}function n(a){return function(b){return a+b}}function o(a,b){return 100/(b-a)}function p(a,b){return 100*b/(a[1]-a[0])}function q(a,b){return p(a,a[0]<0?b+Math.abs(a[0]):b-a[0])}function r(a,b){return b*(a[1]-a[0])/100+a[0]}function s(a,b){for(var c=1;a>=b[c];)c+=1;return c}function t(a,b,c){if(c>=a.slice(-1)[0])return 100;var d,e,f,g,h=s(c,a);return d=a[h-1],e=a[h],f=b[h-1],g=b[h],f+q([d,e],c)/o(f,g)}function u(a,b,c){if(c>=100)return a.slice(-1)[0];var d,e,f,g,h=s(c,b);return d=a[h-1],e=a[h],f=b[h-1],g=b[h],r([d,e],(c-f)*o(f,g))}function v(a,c,d,e){if(100===e)return e;var f,g,h=s(e,a);return d?(f=a[h-1],g=a[h],e-f>(g-f)/2?g:f):c[h-1]?a[h-1]+b(e-a[h-1],c[h-1]):e}function w(a,b,c){var e;if("number"==typeof b&&(b=[b]),"[object Array]"!==Object.prototype.toString.call(b))throw new Error("noUiSlider: 'range' contains invalid value.");if(e="min"===a?0:"max"===a?100:parseFloat(a),!d(e)||!d(b[0]))throw new Error("noUiSlider: 'range' value isn't numeric.");c.xPct.push(e),c.xVal.push(b[0]),e?c.xSteps.push(isNaN(b[1])?!1:b[1]):isNaN(b[1])||(c.xSteps[0]=b[1])}function x(a,b,c){return b?void(c.xSteps[a]=p([c.xVal[a],c.xVal[a+1]],b)/o(c.xPct[a],c.xPct[a+1])):!0}function y(a,b,c,d){this.xPct=[],this.xVal=[],this.xSteps=[d||!1],this.xNumSteps=[!1],this.snap=b,this.direction=c;var e,f=[];for(e in a)a.hasOwnProperty(e)&&f.push([a[e],e]);for(f.length&&"object"==typeof f[0][0]?f.sort(function(a,b){return a[0][0]-b[0][0]}):f.sort(function(a,b){return a[0]-b[0]}),e=0;e<f.length;e++)w(f[e][1],f[e][0],this);for(this.xNumSteps=this.xSteps.slice(0),e=0;e<this.xNumSteps.length;e++)x(e,this.xNumSteps[e],this)}function z(a,b){if(!d(b))throw new Error("noUiSlider: 'step' is not numeric.");a.singleStep=b}function A(a,b){if("object"!=typeof b||Array.isArray(b))throw new Error("noUiSlider: 'range' is not an object.");if(void 0===b.min||void 0===b.max)throw new Error("noUiSlider: Missing 'min' or 'max' in 'range'.");a.spectrum=new y(b,a.snap,a.dir,a.singleStep)}function B(a,b){if(b=h(b),!Array.isArray(b)||!b.length||b.length>2)throw new Error("noUiSlider: 'start' option is incorrect.");a.handles=b.length,a.start=b}function C(a,b){if(a.snap=b,"boolean"!=typeof b)throw new Error("noUiSlider: 'snap' option must be a boolean.")}function D(a,b){if(a.animate=b,"boolean"!=typeof b)throw new Error("noUiSlider: 'animate' option must be a boolean.")}function E(a,b){if("lower"===b&&1===a.handles)a.connect=1;else if("upper"===b&&1===a.handles)a.connect=2;else if(b===!0&&2===a.handles)a.connect=3;else{if(b!==!1)throw new Error("noUiSlider: 'connect' option doesn't match handle count.");a.connect=0}}function F(a,b){switch(b){case"horizontal":a.ort=0;break;case"vertical":a.ort=1;break;default:throw new Error("noUiSlider: 'orientation' option is invalid.")}}function G(a,b){if(!d(b))throw new Error("noUiSlider: 'margin' option must be numeric.");if(a.margin=a.spectrum.getMargin(b),!a.margin)throw new Error("noUiSlider: 'margin' option is only supported on linear sliders.")}function H(a,b){if(!d(b))throw new Error("noUiSlider: 'limit' option must be numeric.");if(a.limit=a.spectrum.getMargin(b),!a.limit)throw new Error("noUiSlider: 'limit' option is only supported on linear sliders.")}function I(a,b){switch(b){case"ltr":a.dir=0;break;case"rtl":a.dir=1,a.connect=[0,2,1,3][a.connect];break;default:throw new Error("noUiSlider: 'direction' option was not recognized.")}}function J(a,b){if("string"!=typeof b)throw new Error("noUiSlider: 'behaviour' must be a string containing options.");var c=b.indexOf("tap")>=0,d=b.indexOf("drag")>=0,e=b.indexOf("fixed")>=0,f=b.indexOf("snap")>=0;if(d&&!a.connect)throw new Error("noUiSlider: 'drag' behaviour must be used with 'connect': true.");a.events={tap:c||f,drag:d,fixed:e,snap:f}}function K(a,b){if(b===!0&&(a.tooltips=!0),b&&b.format){if("function"!=typeof b.format)throw new Error("noUiSlider: 'tooltips.format' must be an object.");a.tooltips={format:b.format}}}function L(a,b){if(a.format=b,"function"==typeof b.to&&"function"==typeof b.from)return!0;throw new Error("noUiSlider: 'format' requires 'to' and 'from' methods.")}function M(a,b){if(void 0!==b&&"string"!=typeof b)throw new Error("noUiSlider: 'cssPrefix' must be a string.");a.cssPrefix=b}function N(a){var b,c={margin:0,limit:0,animate:!0,format:S};b={step:{r:!1,t:z},start:{r:!0,t:B},connect:{r:!0,t:E},direction:{r:!0,t:I},snap:{r:!1,t:C},animate:{r:!1,t:D},range:{r:!0,t:A},orientation:{r:!1,t:F},margin:{r:!1,t:G},limit:{r:!1,t:H},behaviour:{r:!0,t:J},format:{r:!1,t:L},tooltips:{r:!1,t:K},cssPrefix:{r:!1,t:M}};var d={connect:!1,direction:"ltr",behaviour:"tap",orientation:"horizontal"};return Object.keys(d).forEach(function(b){void 0===a[b]&&(a[b]=d[b])}),Object.keys(b).forEach(function(d){var e=b[d];if(void 0===a[d]){if(e.r)throw new Error("noUiSlider: '"+d+"' is required.");return!0}e.t(c,a[d])}),c.pips=a.pips,c.style=c.ort?"top":"left",c}function O(b,d){function e(a,b,c){var d=a+b[0],e=a+b[1];return c?(0>d&&(e+=Math.abs(d)),e>100&&(d-=e-100),[g(d),g(e)]):[d,e]}function o(a,b){a.preventDefault();var c,d,e=0===a.type.indexOf("touch"),f=0===a.type.indexOf("mouse"),g=0===a.type.indexOf("pointer"),h=a;return 0===a.type.indexOf("MSPointer")&&(g=!0),e&&(c=a.changedTouches[0].pageX,d=a.changedTouches[0].pageY),b=b||m(),(f||g)&&(c=a.clientX+b.x,d=a.clientY+b.y),h.pageOffset=b,h.points=[c,d],h.cursor=f||g,h}function p(a,b){var c=document.createElement("div"),d=document.createElement("div"),e=["-lower","-upper"];return a&&e.reverse(),j(d,aa[3]),j(d,aa[3]+e[b]),j(c,aa[2]),c.appendChild(d),c}function q(a,b,c){switch(a){case 1:j(b,aa[7]),j(c[0],aa[6]);break;case 3:j(c[1],aa[6]);case 2:j(c[0],aa[7]);case 0:j(b,aa[6])}}function r(a,b,c){var d,e=[];for(d=0;a>d;d+=1)e.push(c.appendChild(p(b,d)));return e}function s(a,b,c){j(c,aa[0]),j(c,aa[8+a]),j(c,aa[4+b]);var d=document.createElement("div");return j(d,aa[1]),c.appendChild(d),d}function t(a){return a}function u(a){var b=document.createElement("div");return b.className=aa[18],a.firstChild.appendChild(b)}function v(a){var b=a.format?a.format:t,c=W.map(u);S("update",function(a,d,e){c[d].innerHTML=b(a[d],e[d])})}function w(a,b,c){if("range"===a||"steps"===a)return Z.xVal;if("count"===a){var d,e=100/(b-1),f=0;for(b=[];(d=f++*e)<=100;)b.push(d);a="positions"}return"positions"===a?b.map(function(a){return Z.fromStepping(c?Z.getStep(a):a)}):"values"===a?c?b.map(function(a){return Z.fromStepping(Z.getStep(Z.toStepping(a)))}):b:void 0}function x(b,c,d){function e(a,b){return(a+b).toFixed(7)/1}var f=Z.direction,g={},h=Z.xVal[0],i=Z.xVal[Z.xVal.length-1],j=!1,k=!1,l=0;return Z.direction=0,d=a(d.slice().sort(function(a,b){return a-b})),d[0]!==h&&(d.unshift(h),j=!0),d[d.length-1]!==i&&(d.push(i),k=!0),d.forEach(function(a,f){var h,i,m,n,o,p,q,r,s,t,u=a,v=d[f+1];if("steps"===c&&(h=Z.xNumSteps[f]),h||(h=v-u),u!==!1&&void 0!==v)for(i=u;v>=i;i=e(i,h)){for(n=Z.toStepping(i),o=n-l,r=o/b,s=Math.round(r),t=o/s,m=1;s>=m;m+=1)p=l+m*t,g[p.toFixed(5)]=["x",0];q=d.indexOf(i)>-1?1:"steps"===c?2:0,!f&&j&&(q=0),i===v&&k||(g[n.toFixed(5)]=[i,q]),l=n}}),Z.direction=f,g}function y(a,b,c){function e(a){return["-normal","-large","-sub"][a]}function f(a,b,c){return'class="'+b+" "+b+"-"+h+" "+b+e(c[1])+'" style="'+d.style+": "+a+'%"'}function g(a,d){Z.direction&&(a=100-a),d[1]=d[1]&&b?b(d[0],d[1]):d[1],i.innerHTML+="<div "+f(a,"noUi-marker",d)+"></div>",d[1]&&(i.innerHTML+="<div "+f(a,"noUi-value",d)+">"+c.to(d[0])+"</div>")}var h=["horizontal","vertical"][d.ort],i=document.createElement("div");return j(i,"noUi-pips"),j(i,"noUi-pips-"+h),Object.keys(a).forEach(function(b){g(b,a[b])}),i}function z(a){var b=a.mode,c=a.density||1,d=a.filter||!1,e=a.values||!1,f=a.stepped||!1,g=w(b,e,f),h=x(c,b,g),i=a.format||{to:Math.round};return X.appendChild(y(h,d,i))}function A(){return V["offset"+["Width","Height"][d.ort]]}function B(a,b){void 0!==b&&1!==d.handles&&(b=Math.abs(b-d.dir)),Object.keys(_).forEach(function(c){var d=c.split(".")[0];a===d&&_[c].forEach(function(a){a(h(M()),b,C(Array.prototype.slice.call($)))})})}function C(a){return 1===a.length?a[0]:d.dir?a.reverse():a}function D(a,b,c,e){var f=function(b){return X.hasAttribute("disabled")?!1:l(X,aa[14])?!1:(b=o(b,e.pageOffset),a===Q.start&&void 0!==b.buttons&&b.buttons>1?!1:(b.calcPoint=b.points[d.ort],void c(b,e)))},g=[];return a.split(" ").forEach(function(a){b.addEventListener(a,f,!1),g.push([a,f])}),g}function E(a,b){if(0===a.buttons&&0===a.which&&0!==b.buttonsProperty)return F(a,b);var c,d,f=b.handles||W,g=!1,h=100*(a.calcPoint-b.start)/b.baseSize,i=f[0]===W[0]?0:1;if(c=e(h,b.positions,f.length>1),g=J(f[0],c[i],1===f.length),f.length>1){if(g=J(f[1],c[i?0:1],!1)||g)for(d=0;d<b.handles.length;d++)B("slide",d)}else g&&B("slide",i)}function F(a,b){var c=V.querySelector("."+aa[15]),d=b.handles[0]===W[0]?0:1;null!==c&&k(c,aa[15]),a.cursor&&(document.body.style.cursor="",document.body.removeEventListener("selectstart",document.body.noUiListener));var e=document.documentElement;e.noUiListeners.forEach(function(a){e.removeEventListener(a[0],a[1])}),k(X,aa[12]),B("set",d),B("change",d)}function G(a,b){var c=document.documentElement;if(1===b.handles.length&&(j(b.handles[0].children[0],aa[15]),b.handles[0].hasAttribute("disabled")))return!1;a.stopPropagation();var d=D(Q.move,c,E,{start:a.calcPoint,baseSize:A(),pageOffset:a.pageOffset,handles:b.handles,buttonsProperty:a.buttons,positions:[Y[0],Y[W.length-1]]}),e=D(Q.end,c,F,{handles:b.handles});if(c.noUiListeners=d.concat(e),a.cursor){document.body.style.cursor=getComputedStyle(a.target).cursor,W.length>1&&j(X,aa[12]);var f=function(){return!1};document.body.noUiListener=f,document.body.addEventListener("selectstart",f,!1)}}function H(a){var b,e,g=a.calcPoint,h=0;return a.stopPropagation(),W.forEach(function(a){h+=c(a)[d.style]}),b=h/2>g||1===W.length?0:1,g-=c(V)[d.style],e=100*g/A(),d.events.snap||f(X,aa[14],300),W[b].hasAttribute("disabled")?!1:(J(W[b],e),B("slide",b),B("set",b),B("change",b),void(d.events.snap&&G(a,{handles:[W[b]]})))}function I(a){var b,c;if(!a.fixed)for(b=0;b<W.length;b+=1)D(Q.start,W[b].children[0],G,{handles:[W[b]]});a.tap&&D(Q.start,V,H,{handles:W}),a.drag&&(c=[V.querySelector("."+aa[7])],j(c[0],aa[10]),a.fixed&&c.push(W[c[0]===W[0]?1:0].children[0]),c.forEach(function(a){D(Q.start,a,G,{handles:W})}))}function J(a,b,c){var e=a!==W[0]?1:0,f=Y[0]+d.margin,h=Y[1]-d.margin,i=Y[0]+d.limit,l=Y[1]-d.limit,m=Z.fromStepping(b);return W.length>1&&(b=e?Math.max(b,f):Math.min(b,h)),c!==!1&&d.limit&&W.length>1&&(b=e?Math.min(b,i):Math.max(b,l)),b=Z.getStep(b),b=g(parseFloat(b.toFixed(7))),b===Y[e]&&m===$[e]?!1:(window.requestAnimationFrame?window.requestAnimationFrame(function(){a.style[d.style]=b+"%"}):a.style[d.style]=b+"%",a.previousSibling||(k(a,aa[17]),b>50&&j(a,aa[17])),Y[e]=b,$[e]=Z.fromStepping(b),B("update",e),!0)}function K(a,b){var c,e,f;for(d.limit&&(a+=1),c=0;a>c;c+=1)e=c%2,f=b[e],null!==f&&f!==!1&&("number"==typeof f&&(f=String(f)),f=d.format.from(f),(f===!1||isNaN(f)||J(W[e],Z.toStepping(f),c===3-d.dir)===!1)&&B("update",e))}function L(a){var b,c,e=h(a);for(d.dir&&d.handles>1&&e.reverse(),d.animate&&-1!==Y[0]&&f(X,aa[14],300),b=W.length>1?3:1,1===e.length&&(b=1),K(b,e),c=0;c<W.length;c++)B("set",c)}function M(){var a,b=[];for(a=0;a<d.handles;a+=1)b[a]=d.format.to($[a]);return C(b)}function O(){aa.forEach(function(a){a&&k(X,a)}),X.innerHTML="",delete X.noUiSlider}function P(){var a=Y.map(function(a,b){var c=Z.getApplicableStep(a),d=i(String(c[2])),e=$[b],f=100===a?null:c[2],g=Number((e-c[2]).toFixed(d)),h=0===a?null:g>=c[1]?c[2]:c[0]||!1;return[h,f]});return C(a)}function S(a,b){_[a]=_[a]||[],_[a].push(b),"update"===a.split(".")[0]&&W.forEach(function(a,b){B("update",b)})}function T(a){var b=a.split(".")[0],c=a.substring(b.length);Object.keys(_).forEach(function(a){var d=a.split(".")[0],e=a.substring(d.length);b&&b!==d||c&&c!==e||delete _[a]})}function U(a){var b=N({start:[0,0],margin:a.margin,limit:a.limit,step:a.step,range:a.range,animate:a.animate});d.margin=b.margin,d.limit=b.limit,d.step=b.step,d.range=b.range,d.animate=b.animate,Z=b.spectrum}var V,W,X=b,Y=[-1,-1],Z=d.spectrum,$=[],_={},aa=["target","base","origin","handle","horizontal","vertical","background","connect","ltr","rtl","draggable","","state-drag","","state-tap","active","","stacking","tooltip"].map(n(d.cssPrefix||R));if(X.noUiSlider)throw new Error("Slider was already initialized.");return V=s(d.dir,d.ort,X),W=r(d.handles,d.dir,V),q(d.connect,X,W),I(d.events),d.pips&&z(d.pips),d.tooltips&&v(d.tooltips),{destroy:O,steps:P,on:S,off:T,get:M,set:L,updateOptions:U}}function P(a,b){if(!a.nodeName)throw new Error("noUiSlider.create requires a single element.");var c=N(b,a),d=O(a,c);return d.set(c.start),a.noUiSlider=d,d}var Q=window.navigator.pointerEnabled?{start:"pointerdown",move:"pointermove",end:"pointerup"}:window.navigator.msPointerEnabled?{start:"MSPointerDown",move:"MSPointerMove",end:"MSPointerUp"}:{start:"mousedown touchstart",move:"mousemove touchmove",end:"mouseup touchend"},R="noUi-";y.prototype.getMargin=function(a){return 2===this.xPct.length?p(this.xVal,a):!1},y.prototype.toStepping=function(a){return a=t(this.xVal,this.xPct,a),this.direction&&(a=100-a),a},y.prototype.fromStepping=function(a){return this.direction&&(a=100-a),e(u(this.xVal,this.xPct,a))},y.prototype.getStep=function(a){return this.direction&&(a=100-a),a=v(this.xPct,this.xSteps,this.snap,a),this.direction&&(a=100-a),a},y.prototype.getApplicableStep=function(a){var b=s(a,this.xPct),c=100===a?2:1;return[this.xNumSteps[b-2],this.xVal[b-c],this.xNumSteps[b-c]]},y.prototype.convert=function(a){return this.getStep(this.toStepping(a))};var S={to:function(a){return void 0!==a&&a.toFixed(2)},from:Number};return{create:P}});


;(function(){
	
	var doubleHandleSlider = document.querySelector('.double-handle-slider');
	var minValInput = document.querySelector('.min-value');
	var maxValInput = document.querySelector('.max-value');

	
	noUiSlider.create(doubleHandleSlider, {
                start: [ <?php if($int_min_price != "") { print $int_min_price;} else { print "100"; } ?>, <?php if($int_max_price != "") { print $int_max_price;} else { print "50000"; } ?> ],
		connect: true,
		tooltips: true,
		step: 1,
		range: {
			'min': [ 100 ],
			'max': [ 50000 ]
		},
		format: {
			to: function(value) {
				return Math.ceil(value);
			},
			from: function(value) {
				return Math.ceil(value);
			}
		}
	});
	
	// can also be on 'update' for instant update
	doubleHandleSlider.noUiSlider.on('change', function( values, handle ) { 
	
		// This version updates both inputs.
                //alert("hi");
		var rangeValues = values;
		minValInput.value = rangeValues[0];
		maxValInput.value = rangeValues[1];
                $("#frm_filter_price").submit();
/*		
		// This version updates a single input on change
		var val = values[handle]; // 0 or 1
		
		if(handle) {
			maxValInput.value = Math.round(val);
		} else {
			minValInput.value = Math.round(val);
		}*/
	});

	
minValInput.addEventListener('change', function(){
	doubleHandleSlider.noUiSlider.set([this.value, null]);
});
	
maxValInput.addEventListener('change', function(){
	doubleHandleSlider.noUiSlider.set([null, this.value]);
});
	
        
        
        
        var doubleHandleSlider2 = document.querySelector('.double-handle-slider2');
	var minValInput = document.querySelector('.min-value');
	var maxValInput = document.querySelector('.max-value');

	
	noUiSlider.create(doubleHandleSlider2, {
		start: [ <?php if($int_min_price != "") { print $int_min_price;} else { print "100"; } ?>, <?php if($int_max_price != "") { print $int_max_price;} else { print "50000"; } ?>  ],
		connect: true,
		tooltips: true,
		step: 1,
		range: {
			'min': [ 100 ],
			'max': [ 50000 ]
		},
		format: {
			to: function(value) {
				return Math.ceil(value);
			},
			from: function(value) {
				return Math.ceil(value);
			}
		}
	});
	
	// can also be on 'update' for instant update
	doubleHandleSlider2.noUiSlider.on('change', function( values, handle ) { 
	
		// This version updates both inputs.
                //alert("hi");
		var rangeValues = values;
                minValInput.value = rangeValues[0];
                maxValInput.value = rangeValues[1];
                //$("#frm_filter_price").submit();
/*		
		// This version updates a single input on change
		var val = values[handle]; // 0 or 1
		
		if(handle) {
			maxValInput.value = Math.round(val);
		} else {
			minValInput.value = Math.round(val);
		}*/
	});

	
minValInput.addEventListener('change', function(){
	doubleHandleSlider2.noUiSlider.set([this.value, null]);
        
});
	
maxValInput.addEventListener('change', function(){
	doubleHandleSlider2.noUiSlider.set([null, this.value]);
});
	
	
})();
</script>
</body>
</html>
