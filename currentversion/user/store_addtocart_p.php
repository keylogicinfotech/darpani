<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();
include "./../user/store_validatesession.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/http_to_https.php";	
include "./../includes/lib_xml.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/lib_datetimeyear.php";
include "./../user/store_config.php";
//print_r($_SESSION);exit;
#----------------------------------------------------------------------
# posted variables...
$int_pkid = 0;
if(isset($_POST["hdn_prodpkid"]) && $_POST["hdn_prodpkid"] != "")
{
    $int_pkid = $_POST["hdn_prodpkid"];
}

$str_title = "";
if(isset($_POST["hdn_prodtitle"]) && $_POST["hdn_prodtitle"] != "")
{
    $str_title = trim($_POST["hdn_prodtitle"]);
}

$int_catpkid = 0;
if(isset($_POST["hdn_catpkid"]) && $_POST["hdn_catpkid"] != "")
{
    $int_catpkid = $_POST["hdn_catpkid"];
}

$int_memberpkid = 0;
if(isset($_POST["hdn_memberpkid"]) && $_POST["hdn_memberpkid"] != "")
{
    $int_memberpkid = $_POST["hdn_memberpkid"];
}
//print_r($_POST);exit;

$str_memberid = "";
if(isset($_POST["hdn_memberid"]) && $_POST["hdn_memberid"] != "")
{
    $str_memberid = $_POST["hdn_memberid"];
}


$str_prodcattitle = "";
if($int_catpkid > 0)
{
    $str_sel_cat = "";
    $str_sel_cat = "SELECT title FROM ".$STR_DB_TABLE_NAME_CAT." WHERE catpkid = ".$int_catpkid;
    $rs_cat_title = GetRecordSet($str_sel_cat);
    if($rs_cat_title->Count() > 0)
    {
        $str_prodcattitle = $rs_cat_title->Fields("title");
    }
}

$int_subcatpkid = 0;
if(isset($_POST["hdn_subcatpkid"]) && $_POST["hdn_subcatpkid"] != "")
{
    $int_subcatpkid = $_POST["hdn_subcatpkid"];
}
$str_prodsubcattitle = "";
if($int_subcatpkid > 0)
{
    $str_sel_subcat = "";
    $str_sel_subcat = "SELECT subcattitle FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid = ".$int_subcatpkid;
    $rs_subcat_title = GetRecordSet($str_sel_subcat);
    if($rs_subcat_title->Count() > 0)
    {
        $str_prodsubcattitle = $rs_subcat_title->Fields("subcattitle");
    }
    
}
//print $str_prodsubcattitle; exit;
//print $str_prodsubcattitle; exit;
/*$int_modelpkid = 0;
if(isset($_POST["hdn_mdlpkid"]) && $_POST["hdn_mdlpkid"] != "")
{
    $int_modelpkid = $_POST["hdn_mdlpkid"];
}

$str_modelname = "";
if(isset($_POST["hdn_modelname"]) && $_POST["hdn_modelname"] != "")
{
    $str_modelname = $_POST["hdn_modelname"];
}*/

$int_quantity = 0;
if(isset($_POST["txt_quantity"]) && $_POST["txt_quantity"] != "")
{
    $int_quantity = trim($_POST["txt_quantity"]);
}

//$int_colorpkid = 0;
$str_color = "";
if(isset($_POST["cbo_color"]) && $_POST["cbo_color"] != "")
{
    $str_color  = $_POST["cbo_color"];
}

/*if($int_colorpkid != 0)
{
    $str_sel_color = "";
    $str_sel_color = "SELECT * FROM t_store_color WHERE pkid = ".$int_colorpkid;
    $rs_color = GetRecordSet($str_sel_color);
    if($rs_color->Count() > 0)
    {
        $str_color = $rs_color->Fields("title");
    }
}*/


$str_size= "";
if(isset($_POST["cbo_size"]) && $_POST["cbo_size"] != "")
{
    $str_size = $_POST["cbo_size"];
}

$str_type = "";
if(isset($_POST["cbo_type"]) && $_POST["cbo_type"] != "")
{
    $str_type = $_POST["cbo_type"];
}


$int_shippingvalue = 0.00;
if(isset($_POST["rdo_shipping"]) && $_POST["rdo_shipping"] != "")
{
    $int_shippingvalue = $_POST["rdo_shipping"];
}
//print $int_shippingvalue; exit;


/*$str_desc = "";
if(isset($_POST["hdn_description"]) && $_POST["hdn_description"] != "")
{
    $str_desc = trim($_POST["hdn_description"]);
}

$str_shipping_info= "";
if(isset($_POST["hdn_shippinginfo"]) && $_POST["hdn_shippinginfo"] != "")
{
    $str_shipping_info = trim($_POST["hdn_shippinginfo"]);
}*/

$int_price = 0.00;
if(isset($_POST["hdn_price"]) && $_POST["hdn_price"] != "")
{
    $int_price = $_POST["hdn_price"];
}
//print $int_price;exit;


$item_sku = "";
$item_sessionid = "";
$item_uniqueid = "";

if(isset($_POST['specssku']))
{
    $item_sku = GetDecryptId($_POST['specssku']);
        //$item_sku=$_GET['specssku'];
}
//print $item_sku; exit;
if($item_sku=="")
{
    CloseConnection();		
    Redirect("../online-store");
    exit();
}
//print($_SESSION["sessionid"]); exit;
if(isset($_SESSION['sessionid']))
{
    $item_sessionid = $_SESSION['sessionid'];
}

if(isset($_SESSION['uniqueid']))
{
    $item_uniqueid = $_SESSION['uniqueid'];
}
#----------------------------------------------------------------------
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_SESSION_CART." WHERE sku='" . $item_sku . "' AND sessionid='" . $item_sessionid . "'";
$str_query_select = $str_query_select." AND uniqueid='" . $item_uniqueid . "'";
	//print "SELECT: ".$str_query_select."<br/>"; exit;
$rs_list_cart = GetRecordSet($str_query_select);
	
if(!$rs_list_cart->EOF())
{
    $var_quantity = $rs_list_cart->fields("quantity");
    $var_price = $rs_list_cart->fields("price");
    $var_quantity = $var_quantity + $int_quantity;
    $var_extendedprice = $var_price * $var_quantity;

    $strsql="UPDATE ".$STR_DB_TABLE_NAME_SESSION_CART." SET quantity=" . $var_quantity . ",extendedprice='" . $var_extendedprice . "' WHERE sku='" . $item_sku . "' AND sessionid='" . $item_sessionid . "' AND uniqueid='" . $item_uniqueid . "'";
    ExecuteQuery($strsql);
    //print "UPDATE: ".$strsql."<br>";  exit;

    $response['status']='SUC';
    $response['message']="Item updated to cart.";
    echo json_encode($response);
    return;
}
else
{
    $var_cnt=0;
    #Below query get max serialno for t_sessioncart table.
    $str_query_select = "SELECT max(serialno) AS srno from ".$STR_DB_TABLE_NAME_SESSION_CART." WHERE sessionid='" . $item_sessionid . "'";
    $str_query_select = $str_query_select." AND uniqueid='" . $item_uniqueid . "'";
    //print $str_query_select; exit;
    $rs_list_serialno = GetRecordSet($str_query_select);
    if($rs_list_serialno->EOF() == false)
    {
        if($rs_list_serialno->fields("srno") == "")
        {
            $var_cnt = 1;
        }
        else
        {				
            $var_cnt = $rs_list_serialno->fields("srno") + 1;
        }
    }


    $item_extprice = $int_price * $int_quantity;

    #Below query inserts purchased item data into t_sessioncart table.
    $str_query_insert = "";
    $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_SESSION_CART."(sku,serialno,sessionid,uniqueid,catpkid,subcatpkid,productpkid, userpkid, username,memberpkid, membername,producttitle,cattitle, subcattitle,quantity, price ,extendedprice, shippingvalue, color, size, type) values("; 
    $str_query_insert = $str_query_insert."" . ReplaceQuote($item_sku) . "," . $var_cnt . ",'" . $item_sessionid . "','" . $item_uniqueid . "',". $int_catpkid.",".$int_subcatpkid."," . $int_pkid . ", 0, '', ".$int_memberpkid.", '".$str_memberid."','" . $str_title . "','" . $str_prodcattitle . "', '".$str_prodsubcattitle."' ," . $int_quantity . ", " . $int_price . "," . $item_extprice  . "," . $int_shippingvalue . ", '".$str_color."', '".$str_size."', '".$str_type."')"; 

    //print "INSERT: ".$str_query_insert; exit;
    ExecuteQuery($str_query_insert);

    $response['status']='SUC';
    $response['message']="Item added to cart.";
    echo json_encode($response);
    return;
}
exit; 
?>