<?php 
/*
File Name  :- forget_password_p.php
Create Date:- JUN-2019
Intially Create By :- 0015
Update History:
*/
#--------------------------------------------------------------------------------------------------------
#Include files
session_start();
//include("./../includes/validate_mdl_session.php");
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_xml.php";
include "./../includes/lib_common.php";
include "./../includes/http_to_https.php";
include "./../includes/lib_email.php";
#------------------------------------------------------------------------------------------------------
?>
<?php 
$str_db_table_name = "t_user";
//$str_loginid="";
if(isset($_POST["loginid"]))
{ $str_loginid = trim($_POST["loginid"]); }
#--------------------------------------------------------------------------------------------------------
#check whether values are passed properly or not.
if($str_loginid == "" )
{
    $response['status']='ERR';
    $response['message']= "Please enter Login ID!";
    echo json_encode($response); 
    return;
}
#---------------------------------------------------------------------------------------------------------
#Select Query.
$str_query_select = "";
$str_query_select = "SELECT pkid, loginid, password, emailid FROM " .$str_db_table_name. " WHERE loginid='".$str_loginid."' OR emailid='".$str_loginid."'";
$rs_list = GetRecordSet($str_query_select);
$loginid = $rs_list->fields("loginid");
if($rs_list->EOF())
{
    $response['status']='ERR';
    $response['message']= "Login ID or Email does not exists. Plase enter valid Login ID or Email!";
    echo json_encode($response); 
    return;
}
else
{
    $fp=openXMLfile($STR_XML_FILE_PATH_CMS."siteconfiguration.xml");
    $STR_SITE_URL=getTagValue($STR_WEBSITE_NAME,$fp);	
    $str_from=getTagValue($STR_FROM_DEFAULT,$fp);
    $str_to=$rs_list->Fields("emailid");
    closeXMLfile($fp);	
    //print $str_to;exit;
    $str_subject=$STR_SITENAME_WITHOUT_PROTOCOL." Password Reset";
    $mailbody="Your login details are as below.<br/><br/>";
    $mailbody.="<strong>Login ID:</strong> ".$rs_list->fields("loginid")."<br/>";
    $mailbody.="<strong>Password:</strong> ".$rs_list->fields("password")."<br/>";
    $mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL."";
    //print "<br/>".$str_from; print "<br/>".$str_to; print "<br/>".$mailbody; exit;
    sendmail($str_to,$str_subject,$mailbody,$str_from,1);
}
#------------------------------------------------------------------------------------------------------------
$response['status']='SUC';
$response['message']="Congratulations!!! Your password has been sent to your registered email address.";
echo json_encode($response);
return;
?>
