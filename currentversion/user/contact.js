
$(function() {

    $("#frm_add input,#frm_add textarea ,#frm_add select").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // something to have when submit produces an error ?
            // Not decided if I need it yet
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var subject = $("select#subject").val();
            var name = $("input#name").val();
//            var phone = $("input#phone").val();
            //var country = $("input#cbo_country").val();
            //var url = $("input#url").val();
            var email = $("input#email").val();
            var message = $("textarea#message").val();
            var secretcode = $("input#secretcode").val();

			var firstName = name; // For Success/Failure Message
            // Check for white space in name for Success/Fail message
            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
			
            $.ajax({
                url: "./user/contact_p.php",
                type: "POST",
                data: {
                    subject: subject,
                    name: name,
//                    phone: phone,
                    //country: country,
                    // url: url,
                    email: email,
                    message: message,
                    secretcode: secretcode
                },
                cache: false,
                
                success: function(data) 
                {
                        //alert(data);
                        var $responseText=JSON.parse(data);
                        if($responseText.status == 'SUC')
                        {
                            // Success message

                            $('#success').html("<div class='alert alert-success'>");
                            $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                            $('#success > .alert-success').append("<strong> " + $responseText.message + " </strong>");
                            $('#success > .alert-success').append('</div>');

                            //clear all fields
                            //$('#frm_add').trigger("reset");
                             //$('form')[0].reset();
                            //$('#frm_add').trigger("reset");
                            //$('#frm_add').trigger("reset");
                            $("#resetFormNew").trigger("click");
                            setTimeout(function(){ location.reload() }, 5000);
                            //$('#frm_add')[0].reset($responseText);
                            //$("#frm_add").reset($responseText);
                            //$('#frm_add').trigger("reset");
                                    //$('#frm_add').clearInputs();
                                    //$("#frm_add").reset();

                        }

                        else if($responseText.status == 'ERR')
                        {
                            // Fail message
                            $('#success').html("<div class='alert alert-danger'>");
                            $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                    .append("</button>");
                            $('#success > .alert-danger').append("<strong> " + $responseText.message + " ");
                            $('#success > .alert-danger').append('</div>');
                        }
                },
				
				/*success: function() {
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Your contact inquiry has been submitted, we will contact you Soon. Thank you for communication.</strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#frm_add').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + " it seems that my mail server is not responding...</strong> Could you please email me directly to <a href='mailto:info@websitename.com?Subject=Message_Me from websitename.com;>info@websitename.com</a> ? Sorry for the inconvenience!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#frm_add').trigger("reset");
                },*/
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
