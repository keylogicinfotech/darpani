<?php
/*
Create Date:- 11-JAN-2016
Intially Create By :- 0026
Update History:
*/
#-------------------------------------------------------------------------------
#Include files
session_start();
//include "../user/product_validatesession.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_xml.php";
include "./../includes/lib_image.php";
//include "./../includes/count_site_unique_view.php";
include "./user_config.php";
include "./product_config.php";
//include "./../includes/http_to_https.php";
//include "./../user/store_config.php";
//------------------------------------------------------------------------------
$str_button_view_details = "View This Product";
$str_title_signin = "Login";
$str_title_registration = "Registration";

$str_title_page_metatag = "PG_HOME";
$str_db_table_name_metatag = "t_page_metatag";
$str_img_path_home_slider = $STR_IMG_PATH_HOME_SLIDER;
$str_db_table_name = "t_page_metatag";
$str_db_table_name_store_photo = "tr_store_photo";
$str_db_table_name_store = "t_store";
$str_db_table_name_store_cat = "t_store_cat";
$str_db_table_name_store_subcat = "t_store_subcat";
$str_db_table_name_color_master = "t_store_color";
$str_db_table_name_color = "tr_store_color";
$str_db_table_name_size_master = "t_store_size";
$str_db_table_name_size = "tr_store_size";

/*$str_db_table_name_ppd_photo = "tr_ppd_photo"; 
$str_db_table_name_ppd = "t_ppd"; 
$str_db_table_name_ppd_subcat = "t_ppd_subcat"; */
$str_db_table_name_camphone = "cm_phone";
$str_db_table_name_phoneschedule = "t_phoneschedule";

$str_img_path_home = "./mdm/home/";
$str_img_path_home2 = "./mdm/home2/";
$str_img_path_photoalbum = "./mdm/photoalbum/";
$str_img_path_video = "./mdm/video/";
$str_img_path_store = "./mdm/store/";
$str_img_path_ppd = "./mdm/ppd/";

$str_xml_file_name_photoalbum = "photoalbum.xml";
$str_xml_file_name_cat = "video_cat.xml";
$str_xml_file_name_video = "video.xml";
$str_xml_file_name_camcms = "cam_cms.xml";
$str_xml_file_name_cms = "user_cms.xml";


$str_desc_cms_login = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS . $str_xml_file_name_cms);
$str_desc_cms_login = getTagValue("ITEMKEYVALUE_LOGIN", $fp);
$str_visible_cms_login = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
$str_desc_cms_registration = getTagValue("ITEMKEYVALUE_REGISTRATION", $fp);
$str_visible_cms_registration = getTagValue("ITEMKEYVALUE_VISIBLE1", $fp);
//CloseXmlFile($fp);

//# Get Page Title for SEO
$str_query_select = "";
$str_query_select = "SELECT titletag FROM " . $str_db_table_name_metatag . " WHERE visible='YES' AND pagekey='" . $str_title_page_metatag . "' ";
//print $str_query_select;
$rs_list_mt = GetRecordset($str_query_select);

//--------------------------------------------------------------------------------
// for Home Page Image Slider - IMAGE SLIDE SHOW
$arr_xml_list_hime_slider = "";
$arr_xml_list_hime_slider = readXml($STR_XML_FILE_PATH_MODULE . "homeslider.xml", "ROOT_ITEM");
//print_r($arr_xml_list_hime_slider);
////------------------------------------------------------------------------------
#for content image 
$arr_xml_list_home_content = "";
$arr_xml_list_home_content = readXml($STR_XML_FILE_PATH_MODULE . "homecontent.xml", "ROOT_ITEM");
$arr_home_content_cnt = (count($arr_xml_list_home_content));

$arr_xml_list_home_content2 = "";
$arr_xml_list_home_content2 = readXml($STR_XML_FILE_PATH_MODULE . "homecontent2.xml", "ROOT_ITEM");
$arr_home_content_cnt2 = (count($arr_xml_list_home_content2));

$arr_xml_list_photoalbum = "";
$arr_xml_list_photoalbum = readXML($STR_XML_FILE_PATH_MODULE . "photoalbum/" . $str_xml_file_name_photoalbum, "ROOT_ITEM");
$int_photoalbum_cnt = 0;
foreach ($arr_xml_list_photoalbum as $key => $val) {
    //    if(isset($manta_option['iso_format_recent_works']) && $manta_option['iso_format_recent_works'] == 1){
    //    $theme_img = 'recent_works_thumbnail';
    if (isset($arr_xml_list_photoalbum[$key]["FEATURED"]) == 'YES' && $arr_xml_list_photoalbum[$key]["VISIBLE"] == 'YES') {
        $int_photoalbum_cnt++;
    }
}
//print $int_photoalbum_cnt;

$arr_xml_list_video = "";
$arr_xml_list_video = readXML($STR_XML_FILE_PATH_MODULE . $str_xml_file_name_video, "ROOT_ITEM");
$int_video_cnt = 0;
foreach ($arr_xml_list_video as $key => $val) {
    //print $arr_xml_list_video[$key]["FEATURED"];
    if (isset($arr_xml_list_video[$key]["FEATURED"]) == 'YES' && $arr_xml_list_video[$key]["VISIBLE"] == 'YES') {
        $int_video_cnt++;
    }
}
//print $int_video_cnt;
//$int_video_cnt = (count($arr_xml_list_video));

$arr_xml_list_phoneschedule = "";
$arr_xml_list_phoneschedule = readXML($STR_XML_FILE_PATH_MODULE . "phoneschedule.xml", "ROOT_ITEM");
//print_r($arr_xml_list_phoneschedule);

$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS . "cam_cms.xml");
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_status_cms = getTagValue("ITEMKEY_STATUS", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
//print($str_status_cms);
CloseXmlFile($fp);





$str_xml_list_cat = "";
$str_xml_list_cat = readXML($STR_XML_FILE_PATH_MODULE . $str_xml_file_name_cat, "ROOT_ITEM_CAT");
//print_r($arr_xml_list_home_content2);
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_cat_pkid = "";
if (isset($_POST["catid"]) && trim($_POST["catid"]) != "") {
    $int_cat_pkid = trim($_POST["catid"]);
} else if (isset($_SESSION["catid"]) && trim($_SESSION["catid"]) != "") {
    $int_cat_pkid = trim($_SESSION["catid"]);
}
//echo count($days);

//echo "<br>";

//echo sizeof($days);

//$arr_xml_list_portfolio="";
//$arr_xml_list_portfolio=readXml($STR_XML_FILE_PATH_MODULE.$str_xml_file_name_portfolio,"ROOT_ITEM");
//------------------------------------------------------------------------------
$str_member_flag = false;
$int_userpkid = 0;
$str_username = "";
$str_user_type = "";

if (isset($_SESSION['userpkid']) && $_SESSION['userpkid'] != "") {
    $str_member_flag = true;
    $int_userpkid = $_SESSION['userpkid'];
    $str_username = $_SESSION['username'];
}


$int_referredbypkid = 0;
$int_commission = 0.00;
$int_wholesaler_discount = 0;

if ($int_userpkid > 0) {
    $str_query_select = "";
    $str_query_select = "SELECT referredbypkid, discount,isusertype FROM " . $STR_DB_TABLE_NAME_USER . " WHERE pkid=" . $int_userpkid;

    $rs_list_user = GetRecordSet($str_query_select);
    if (!$rs_list_user->EOF()) {
        if (strtoupper($rs_list_user->Fields("isusertype")) == "WHOLESALER") {
            $str_user_type = "WHOLESALER";
            $int_referredbypkid = $rs_list_user->Fields("referredbypkid");
            $int_wholesaler_discount = $rs_list_user->Fields("discount");
            if ($int_referredbypkid > 0) {
                $str_query_select = "";
                $str_query_select = "SELECT discount, commission FROM " . $STR_DB_TABLE_NAME_USER . " WHERE pkid=" . $int_referredbypkid;
                $rs_list_commmision = GetRecordSet($str_query_select);

                $int_commission = $rs_list_commmision->Fields("commission");
            }
        }
    }
    //$str_where = " WHERE userpkid = ".$int_userpkid." ";
}
//print $rs_list_cat_list->Count();
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_cat_pkid = "";
if (isset($_POST["catid"]) && trim($_POST["catid"]) != "") {
    $int_cat_pkid = trim($_POST["catid"]);
}

$str_name_key = "";
if (isset($_POST["key"]) && trim($_POST["key"]) != "") {
    $str_name_key = trim($_POST["key"]);
}


# get data for paging
$int_page = "";
if (isset($_POST["PagePosition"]) && trim($_POST["PagePosition"]) != "" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"]) > 0) {
    $int_page = $_POST["PagePosition"];
} else {
    $int_page = 1;
}
//print_r($_POST);
$str_filter = "";
$str_filter = "&catid=" . $int_cat_pkid . "&key=" . $str_name_key . "&PagePosition=" . $int_page . "&#ptop";

$str_where = "";
if ($int_cat_pkid > 0) {
    $str_where = " AND a.subcatpkid=" . $int_cat_pkid . " ";
}


// --------------------------------------------------
$int_tailoring_service_pkid = 0;

// --------------------------------------------------
?>

<?php
if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $_SERVER['HTTP_USER_AGENT']) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($_SERVER['HTTP_USER_AGENT'], 0, 4))) {
    // For mobile
    $str_flag_view = "M";
    $str_row_class = " hidden-md hidden-lg ";
    $str_col_class = " col-sm-6 col-xs-6 ";
    $int_new_row_divison = 2;
} else {
    // For desktop / laptop    
    $str_flag_view = "D";
    $str_row_class = " hidden-sm hidden-xs ";
    $str_col_class = " col-lg-3 col-md-3 ";
    $int_new_row_divison = 4;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="author" content="">

    <title><?php print($STR_SITE_TITLE); ?> : <?php print($rs_list_mt->fields("titletag")); ?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <!-- Custom CSS -->
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />

    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_listing.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162432112-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-162432112-1');
    </script>
    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '798484083973532');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=798484083973532&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
</head>

<body>
    <?php include($STR_USER_HEADER_PATH); ?>
    <header id="myCarousel" class="carousel slide">
        <div class="carousel-inner" align="center">
            <?php $arr_test = array_keys($arr_xml_list_hime_slider);

            if ($arr_test[0] != "ROOT_ITEM") {
                $int_mdl_cnt = 1;
                foreach ($arr_xml_list_hime_slider as $key => $val) {
                    if (is_array($val)) {
                        if ($arr_xml_list_hime_slider[$key]["IMAGEFILENAME"] !== "") {
                            if ($int_mdl_cnt == 1) {
                                $str_flg_active = "item active";
                            } else {
                                $str_flg_active = "item";
                            }  //checks first image for make it Active image in slider 

                            if ($arr_xml_list_hime_slider[$key]["TITLE"] != "") {
                                $str_img_alt = $arr_xml_list_hime_slider[$key]["TITLE"];
                            } else if ($arr_xml_list_hime_slider[$key]["PHOTOURL"] != "") {
                                $str_img_alt = $arr_xml_list_hime_slider[$key]["PHOTOURL"];
                            } else {
                                $str_img_alt = $STR_SITE_TITLE;
                            } ?>

                            <div class="<?php print($str_flg_active); ?>">
                                <?php
                                if ($arr_xml_list_hime_slider[$key]["OPENURLINNEWWINDOW"] == 'YES') { ?>

                                    <a href="<?php print($arr_xml_list_hime_slider[$key]["PHOTOURL"]); ?>" target="_blank"><img src="<?php print($str_img_path_home_slider . $arr_xml_list_hime_slider[$key]["IMAGEFILENAME"]); ?>" alt="<?php print($str_img_alt); ?>" title="<?php print($str_img_alt); ?>" class="img-responsive" border="0" align="absmiddle" width="100%" /></a>

                                    <?php if ($arr_xml_list_hime_slider[$key]["TITLE"] != '' && ($arr_xml_list_hime_slider[$key]["DESCRIPTION"] != '<br>' && $arr_xml_list_hime_slider[$key]["DESCRIPTION"] != '')) { ?>
                                        <div class="carousel-caption slider-content hidden-xs hidden-sm">
                                            <?php if ($arr_xml_list_hime_slider[$key]["TITLE"] != '') { ?>
                                                <h2><?php print($arr_xml_list_hime_slider[$key]["TITLE"]); ?></h2>
                                            <?php } ?>
                                            <?php if ($arr_xml_list_hime_slider[$key]["DESCRIPTION"] != '<br>' && $arr_xml_list_hime_slider[$key]["DESCRIPTION"] != '') { ?>
                                                <p><?php print($arr_xml_list_hime_slider[$key]["DESCRIPTION"]); ?></p>
                                            <?php  } ?>
                                        </div>

                                        <?php //  if($arr_xml_list_hime_slider[$key]["TITLE"] != '')                  { 
                                        ?>

                                    <?php }
                                } else { ?>

                                    <a href="<?php print($arr_xml_list_hime_slider[$key]["PHOTOURL"]); ?>" target="_self"><img src="<?php print($str_img_path_home_slider . $arr_xml_list_hime_slider[$key]["IMAGEFILENAME"]); ?>" alt="<?php print($str_img_alt); ?>" title="<?php print($str_img_alt); ?>" class="img-responsive" border="0" align="absmiddle" width="100%" /></a>
                                    <div class="carousel-caption slider-content">
                                        <?php if ($arr_xml_list_hime_slider[$key]["TITLE"] != '') { ?>
                                            <h2><?php print($arr_xml_list_hime_slider[$key]["TITLE"]); ?></h2>
                                        <?php } ?>
                                        <?php if ($arr_xml_list_hime_slider[$key]["DESCRIPTION"] != '<br>' || $arr_xml_list_hime_slider[$key]["DESCRIPTION"] != '') { ?>
                                            <p><?php print($arr_xml_list_hime_slider[$key]["DESCRIPTION"]); ?></p>
                                        <?php  } ?>
                                    </div>

                                <?php  } ?>
                            </div>
                <?php
                        }
                        $int_mdl_cnt++;
                    }
                } ?>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="left:0">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    <?php   }
    ?>
    <!--</div>-->
    </header>

    <div class="container center-bg">
        <?php #------------------------------ START - Trending Products--------------------------------------------------- 
        ?>
        <?php
        # Select Query to get list
        /*
	$str_query_select = "";
        $str_query_select = "SELECT a.*, b.subcattitle, d.title AS cattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl FROM " .$str_db_table_name_store. " a ";
        $str_query_select .= "LEFT JOIN ".$str_db_table_name_store_cat." d ON a.catpkid=d.catpkid  AND d.visible='YES' ";
        $str_query_select .= "LEFT JOIN ".$str_db_table_name_store_subcat." b ON a.subcatpkid=b.subcatpkid AND b.visible='YES' ";
        $str_query_select .= "LEFT JOIN ".$str_db_table_name_store_photo." c ON a.pkid=c.masterpkid WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.visible='YES' AND a.displayashot='YES' ";
        $str_query_select .= "ORDER BY a.displayorder, a.createdatetime, a.title LIMIT 0,4 ";
	*/

        /*
	//$str_query_select = "SELECT a.*, "; // If GROUP BY is used in query then * can not be used
	$str_query_select = "SELECT a.pkid, a.catpkid, a.subcatpkid, a.title, a.listprice, a.ourprice, a.memberprice, weight, ";
	$str_query_select .= "a.description, a.createdatetime, a.displayasnew, a.displayashot, a.displayasfeatured, ";
	$str_query_select .= "b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title AS cattitle, ";
	$str_query_select .= "f.title AS colortitle, ";
	//$str_query_select .= "group_concat(n.title ) AS sizetitle "; // Use this line if want all sizes in same record
	$str_query_select .= "min(n.title) AS sizetitle "; // Use this line if want first size only
	//$str_query_select .= "max(n.title) AS sizetitle "; // Use this line if want last size only
	$str_query_select .= "FROM " .$str_db_table_name_store. " a ";
	$str_query_select .= "LEFT JOIN ".$str_db_table_name_store_cat." d ON a.catpkid=d.catpkid AND d.visible='YES' ";
	$str_query_select .= "LEFT JOIN ".$str_db_table_name_store_subcat." b ON a.subcatpkid=b.subcatpkid ";
	$str_query_select .= "LEFT JOIN ".$str_db_table_name_store_photo." c ON a.pkid=c.masterpkid ";
	$str_query_select .= "LEFT JOIN ".$str_db_table_name_color." e ON a.pkid=e.itempkid ";
	$str_query_select .= "LEFT JOIN ".$str_db_table_name_color_master." f ON e.masterpkid=f.pkid ";
	$str_query_select .= "LEFT JOIN ".$str_db_table_name_size." m ON a.pkid=m.itempkid ";
	$str_query_select .= "LEFT JOIN ".$str_db_table_name_size_master." n ON m.masterpkid=n.pkid ";
	$str_query_select .= " WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.visible='YES' AND a.approved='YES' AND a.displayashot='YES' ";
	$str_query_select .= " GROUP BY a.pkid, a.catpkid, a.subcatpkid, a.title, a.listprice, a.ourprice, a.memberprice, a.weight, a.description, a.createdatetime, a.displayasnew, a.displayashot, a.displayasfeatured, b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title, f.title ";
        $str_query_select .= "ORDER BY a.displayorder, a.createdatetime, a.title ";
	$str_query_select .= " LIMIT 0,4";
	*/

        /*
28JUL2020 - IMPORTANT : In this query, if product has same color and different sizes OR product has same size and different colors then query will get rid off duplicate records and will show only one record. 
*/
        //$str_query_select = "SELECT a.*, "; // If GROUP BY is used in query then * can not be used
        $str_query_select = "SELECT temp.pkid, temp.catpkid, temp.subcatpkid, temp.title, temp.listprice, temp.ourprice, temp.memberprice, temp.weight, ";
        $str_query_select .= "temp.description, temp.createdatetime, temp.displayasnew, temp.displayashot, temp.displayasfeatured, temp.displayorder, ";
        $str_query_select .= "temp.subcattitle, temp.thumbphotofilename, temp.largephotofilename, temp.imageurl, temp.cattitle, ";
        $str_query_select .= "temp.colortitle, temp.sizetitle ";
        $str_query_select .= "FROM ( ";
        $str_query_select .= "SELECT a.pkid, a.catpkid, a.subcatpkid, a.title, a.listprice, a.ourprice, a.memberprice, a.weight, ";
        $str_query_select .= "a.description, a.createdatetime, a.displayasnew, a.displayashot, a.displayasfeatured, a.displayorder, ";
        $str_query_select .= "b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title AS cattitle, ";
        $str_query_select .= "f.title AS colortitle, ";
        //$str_query_select .= "group_concat(n.title ) AS sizetitle "; // Use this line if want all sizes in same record
        $str_query_select .= "min(n.title) AS sizetitle "; // Use this line if want first size only
        //$str_query_select .= "max(n.title) AS sizetitle "; // Use this line if want last size only
        $str_query_select .= " FROM " . $str_db_table_name_store . " a ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_store_cat . " d ON a.catpkid=d.catpkid AND d.visible='YES' ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_store_subcat . " b ON a.subcatpkid=b.subcatpkid ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_store_photo . " c ON a.pkid=c.masterpkid ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_color . " e ON a.pkid=e.itempkid ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_color_master . " f ON e.masterpkid=f.pkid ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_size . " m ON a.pkid=m.itempkid ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_size_master . " n ON m.masterpkid=n.pkid ";
        $str_query_select .= " WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.visible='YES' AND a.approved='YES' AND a.displayashot='YES' ";
        $str_query_select .= " GROUP BY a.pkid, a.catpkid, a.subcatpkid, a.title, a.listprice, a.ourprice, a.memberprice, a.weight, a.description, a.createdatetime, a.displayasnew, a.displayashot, a.displayasfeatured, b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title, f.title ";
        $str_query_select .= " ORDER BY a.createdatetime, a.displayorder, a.title ) AS temp ";
        $str_query_select .= " GROUP BY temp.pkid, temp.catpkid, temp.subcatpkid, temp.title, temp.listprice, temp.ourprice, temp.memberprice, temp.weight, temp.description, temp.createdatetime, temp.displayasnew, temp.displayashot, temp.displayasfeatured, temp.subcattitle, temp.thumbphotofilename, temp.largephotofilename, temp.imageurl, temp.cattitle, temp.title ";
        $str_query_select .= " HAVING MAX(temp.createdatetime) ";
        $str_query_select .= " ORDER BY temp.createdatetime DESC, temp.displayorder, temp.title ";
        $str_query_select .= " LIMIT 0,4";

        //print $str_query_select;
        $rs_list = GetRecordSet($str_query_select);

        $int_total_records = $rs_list->Count();
        if ($int_total_records > 0) { ?>
            <div class="row padding-10">
                <div class="col-md-12" align="center">
                    <div class="section-title text-center border-line mb-10">
                        <h2 class="block_title"><?php print $STR_TITLE_HOME_HOT; ?></h2>
                    </div>
                </div>
            </div>
            <?php if ($str_flag_view == "M") {
                print "<br/>";
            } ?>
            <div class="row padding-10 <?php print($str_row_class); ?>">
                <?php
                $int_cnt = 0;
                while (!$rs_list->EOF()) {  ?>
                    <div class="<?php print($str_col_class); ?>">
                        <div class="product-box text-center">
                            <div class="row padding-10">
                                <div class="col-md-12 nopadding" align="center">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))))); ?>" title="<?php print $rs_list->Fields("title"); ?>">
                                        <?php if ($rs_list->Fields("thumbphotofilename")) { ?>
                                            <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $str_img_path_store . $rs_list->Fields("pkid") . "/" . $rs_list->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                        <?php } else if ($rs_list->Fields("imageurl") != "") { ?>
                                            <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $rs_list->Fields("imageurl"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                        <?php } ?>
                                    </a>
                                    <?php
                                    $str_query_select = "";
                                    $str_query_select = "SELECT * FROM " . $str_db_table_name_store_photo . " WHERE masterpkid=" . $rs_list->Fields("pkid") . " AND setasofferimage='LEFT' ORDER BY setasofferimage ASC";
                                    $rs_list_offerimages_left = GetRecordSet($str_query_select);
                                    //print $rs_list_offerimages->Count();
                                    if ($rs_list_offerimages_left->Count() > 0) { ?>
                                        <img src="<?php print $str_img_path_store . $rs_list->Fields("pkid") . "/" . $rs_list_offerimages_left->Fields("thumbphotofilename"); ?>" alt="" title="" class="img-top-left" />
                                    <?php }  ?>
                                    <?php
                                    $str_query_select = "";
                                    $str_query_select = "SELECT * FROM " . $str_db_table_name_store_photo . " WHERE masterpkid=" . $rs_list->Fields("pkid") . " AND setasofferimage='RIGHT' ORDER BY setasofferimage ASC";
                                    $rs_list_offerimages_right = GetRecordSet($str_query_select);
                                    //print $rs_list_offerimages->Count();
                                    if ($rs_list_offerimages_right->Count() > 0) {
                                    ?>
                                        <img src="<?php print $str_img_path_store . $rs_list->Fields("pkid") . "/" . $rs_list_offerimages_right->Fields("thumbphotofilename"); ?>" alt="" title="" class="img-top-right" />
                                    <?php }  ?>
                                </div>
                            </div>
                            <div class="row padding-10">
                                <div class="col-md-12 product-content-bg">
                                    <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))))); ?>" title="<?php print $rs_list->Fields("title"); ?>"><b class="text-muted"><?php print MakeStringShort($rs_list->Fields("title"), 23); ?></b></a></p>
                                    <p>
                                        <?php if ($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) { ?>
                                            <?php
                                            $int_wholesaler_price = "";
                                            $int_wholesaler_price = $rs_list->Fields("memberprice") / $int_conversionrate;
                                            $int_hdn_price = $int_wholesaler_price;
                                            echo $int_hdn_price;
                                            ?>
                                            <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_wholesaler_price), 0) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                        <?php } else {  ?>
                                            <?php if ($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                                $int_list_price = "";
                                                $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;

                                                $int_our_price = "";
                                                $int_our_price = $rs_list->Fields("ourprice");
                                                // ------------------- changes here $int_our_price = $rs_list->Fields("ourprice") / $int_conversionrate; to  $int_our_price = $rs_list->Fields("ourprice") ------------------
                                                $int_hdn_price = $int_our_price;
                                                // echo $int_hdn_price;
                                            ?>
                                                <strike class="text-muted"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 0) . "&nbsp;" . $str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                                <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_our_price / $int_conversionrate), 0) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                                <?php
                                                $int_per_discount = 0;
                                                $int_per_discount = (100 - ($rs_list->Fields("ourprice") / $rs_list->Fields("listprice")) * 100);
                                                if ($int_per_discount > 0) { ?>
                                                    <span class="label label-primary"><b><?php print number_format($int_per_discount, 2); ?>% OFF</b></span>
                                                <?php } ?>
                                            <?php } else {
                                                $int_list_price = "";
                                                $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;
                                                $int_hdn_price = $int_list_price;
                                            ?>
                                                <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 0) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                            <?php }  ?>
                                        <?php }  ?>
                                    </p>
                                </div>
                            </div>
                            <div id="success<?php print $rs_list->fields("pkid"); ?>"></div>
                            <div class="row padding-10">
                                <div class="col-md-12">
                                    <div class="row padding-10">
                                        <?php /*?><div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-button"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a> 
                                </div><?php */ ?>
                                        <div class="col-md-6 col-sm-6 col-xs-6 nopadding">

                                            <form class='myform' id="frm_add<?php print $rs_list->Fields("pkid"); ?>" name="frm_add<?php print $rs_list->Fields("pkid"); ?>" onsubmit="return addToCart('frm_add<?php print $rs_list->Fields("pkid"); ?>')" novalidate>
                                                <input type="hidden" name="hdn_prodpkid" id="hdn_prodpkid" value="<?php print $rs_list->Fields("pkid"); ?>">
                                                <input type="hidden" name="hdn_prodtitle" id="hdn_prodtitle" value="<?php print $rs_list->Fields("title"); ?>">

                                                <?php /* ?>
						<input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $int_cat_pkid; ?>">
						<input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $int_sub_cat_pkid; ?>">
						<?php */ ?>
                                                <input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $rs_list->Fields("catpkid"); ?>">
                                                <input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $rs_list->Fields("subcatpkid"); ?>">

                                                <input type="hidden" name="hdn_weight" id="hdn_weight" value="<?php print $rs_list->Fields("weight"); ?>">
                                                <input type="hidden" name="hdn_userpkid" id="hdn_userpkid" value="<?php print $int_userpkid; ?>">
                                                <input type="hidden" name="hdn_username" id="hdn_username" value="<?php print $str_username; ?>">
                                                <input type="hidden" name="hdn_discount" id="hdn_discount" value="<?php print $int_wholesaler_discount; ?>">
                                                <input type="hidden" name="hdn_referredbypkid" id="hdn_referredbypkid" value="<?php print $int_referredbypkid; ?>">
                                                <input type="hidden" name="hdn_commission" id="hdn_commission" value="<?php print $int_commission; ?>">
                                                <input type="hidden" name="rdo_color" id="rdo_color" value="<?php print $rs_list->Fields("colortitle"); ?>">
                                                <input type="hidden" name="rdo_size" id="rdo_size" value="<?php print $rs_list->Fields("sizetitle"); ?>">
                                                <input type="hidden" name="rdo_tailoringoption" id="rdo_tailoringoption" value="<?php print $int_tailoring_service_pkid; ?>">
                                                <input type="hidden" name="cbo_semistitched_stitchingsize" id="cbo_semistitched_stitchingsize" value="0">
                                                <input type="hidden" name="txt_blouse_length" id="txt_blouse_length" value="0">
                                                <input type="hidden" name="txt_blouse_sleeve_length" id="txt_blouse_sleeve_length" value="0">
                                                <input type="hidden" name="cbo_blouse_aroundbust" id="cbo_blouse_aroundbust" value="0">
                                                <input type="hidden" name="txt_blouse_abovearoundwaist" id="txt_blouse_abovearoundwaist" value="0">
                                                <input type="hidden" name="txt_blouse_frontneckdepth" id="txt_blouse_frontneckdepth" value="0">
                                                <input type="hidden" name="txt_blouse_backneckdepth" id="txt_blouse_backneckdepth" value="0">
                                                <input type="hidden" name="txt_lehenga_aroundwaist" id="txt_lehenga_aroundwaist" value="0">
                                                <input type="hidden" name="txt_lehenga_aroundhips" id="txt_lehenga_aroundhips" value="0">
                                                <input type="hidden" name="txt_lehenga_length" id="txt_lehenga_length" value="0">
                                                <input type="hidden" name="txt_salwar_yourheight" id="txt_salwar_yourheight" value="0">
                                                <input type="hidden" name="txt_salwar_frontneckdepth" id="txt_salwar_frontneckdepth" value="0">
                                                <input type="hidden" name="txt_salwar_bottomlength" id="txt_salwar_bottomlength" value="0">
                                                <input type="hidden" name="txt_salwar_kameezlength" id="txt_salwar_kameezlength" value="0">
                                                <input type="hidden" name="txt_salwar_sleevelength" id="txt_salwar_sleevelength" value="0">
                                                <input type="hidden" name="cbo_salwar_sleevestyle" id="cbo_salwar_sleevestyle" value="0">
                                                <input type="hidden" name="cbo_salwar_aroundbust" id="cbo_salwar_aroundbust" value="0">
                                                <input type="hidden" name="txt_salwar_abovearoundwaist" id="txt_salwar_abovearoundwaist" value="0">
                                                <input type="hidden" name="txt_salwar_aroundwaist" id="txt_salwar_aroundwaist" value="0">
                                                <input type="hidden" name="txt_salwar_aroundhips" id="txt_salwar_aroundhips" value="0">
                                                <input type="hidden" name="txt_salwar_backneckdepth" id="txt_salwar_backneckdepth" value="0">
                                                <input type="hidden" name="txt_salwar_aroundthigh" id="txt_salwar_aroundthigh" value="0">
                                                <input type="hidden" name="txt_salwar_aroundknee" id="txt_salwar_aroundknee" value="0">
                                                <input type="hidden" name="txt_salwar_aroundcalf" id="txt_salwar_aroundcalf" value="0">
                                                <input type="hidden" name="txt_salwar_aroundankle" id="txt_salwar_aroundankle" value="0">

                                                <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $int_hdn_price;  ?>" />
                                                <input type="hidden" id="txt_quantity" name="txt_quantity" value="1" />
                                                <input type="hidden" id="hdn_description" name="hdn_description" value="" />
                                                <input type="hidden" id="rdo_shipping" name="rdo_shipping" value="" />
                                                <?php
                                                $skuval = date("YmdHis");
                                                $skuval = GetEncryptId($rs_list->Fields("pkid"));
                                                ?>
                                                <input type="hidden" id="specssku" name="specssku" value="<?php print $skuval;  ?>" />


                                                <button type="submit" class="product-buynow-button" name='btn_addtocart' id="btn_addtocart_<?php print $rs_list->Fields("pkid"); ?>" style="border-bottom: 1px solid transparent; width: 100%;"><i class="fa fa-shopping-cart"></i></button>

                                            </form>

                                            <?php /* ?><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-buynow-button"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a><?php */ ?>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                            <?php
                                            ## --------------------------------------------- START : WISHLIST------------------------------------------
                                            if ($str_member_flag) {
                                                $str_query_select = "";
                                                $str_query_select = "SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=" . $_SESSION["userpkid"] . " AND itempkid=" . $rs_list->fields("pkid");
                                                $rs_list_fav = GetRecordSet($str_query_select);

                                                $str_fav_class = "";
                                                $str_fav_flag = "";
                                                if ($rs_list_fav->Count() == 0) {
                                                    $str_fav_class = "";
                                                    $str_fav_flag = "FAV";
                                                } else if ($rs_list_fav->Count() > 0) {
                                                    $str_fav_class = "text-primary";
                                                    $str_fav_flag = "UNFAV";
                                                } ?>
                                                <form name="frm_user_favorite<?php print $rs_list->fields("pkid"); ?>" id="frm_user_favorite<?php print $rs_list->fields("pkid"); ?>" novalidate>

                                                    <input type="hidden" name="userpkid" id="userpkid<?php print($rs_list->fields("pkid")) ?>" value="<?php print($_SESSION["userpkid"]); ?>">
                                                    <input type="hidden" name="itempkid" id="itempkid<?php print($rs_list->fields("pkid")) ?>" value="<?php print($rs_list->fields("pkid")) ?>">

                                                    <input type="hidden" name="flgfav" id="flgfav<?php print($rs_list->fields("pkid")) ?>" value="<?php print($str_fav_flag); ?>">



                                                    <?php //print $rs_list_fav->Count(); 
                                                    ?>
                                                    <button id="frm_submit<?php print($rs_list->fields("pkid")) ?>" name="frm_submit<?php print($rs_list->fields("pkid")) ?>" type="submit" style="display: none;"></button>
                                                </form>

                                                <a onclick="$('#frm_submit<?php print($rs_list->fields("pkid")) ?>').click();" class="product-button <?php print $str_fav_class; ?>"><i class="fa fa-heart"></i></a>
                                                <script>
                                                    $(function() {

                                                        $("#frm_user_favorite<?php print $rs_list->fields("pkid"); ?> input").jqBootstrapValidation({
                                                            preventSubmit: true,
                                                            submitError: function($form, event, errors) {
                                                                // something to have when submit produces an error ?
                                                                // Not decided if I need it yet
                                                            },
                                                            submitSuccess: function($form, event) {
                                                                event.preventDefault(); // prevent default submit behaviour
                                                                // get values from FORM

                                                                var userpkid = $("input#userpkid<?php print($rs_list->fields("pkid")) ?>").val();
                                                                //alert(userpkid);
                                                                var itempkid = $("input#itempkid<?php print($rs_list->fields("pkid")) ?>").val();
                                                                var flgfav = $("input#flgfav<?php print($rs_list->fields("pkid")) ?>").val();


                                                                $.ajax({

                                                                    url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                                                    type: "POST",
                                                                    data: {

                                                                        userpkid: userpkid,
                                                                        itempkid: itempkid,
                                                                        flgfav: flgfav
                                                                    },
                                                                    cache: false,
                                                                    success: function(data) {
                                                                        //alert(data);
                                                                        var $responseText = JSON.parse(data);
                                                                        if ($responseText.status == 'SUC')
                                                                        //                                        if(($responseText.status).equal("SUC"))
                                                                        {
                                                                            // Success message
                                                                            //                        alert(data);

                                                                            $('#success<?php print $rs_list->fields("pkid"); ?>').html("<div class='alert alert-success small'>");
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                                .append("</button>");
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-success ').append('</div>');
                                                                            //setTimeout(function(){ location.reload() }, 2000);
                                                                            setTimeout(function() {
                                                                                window.location.href = "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>#trendingdeals";
                                                                            }, 5000);
                                                                        } else if ($responseText.status == 'ERR') {
                                                                            // Fail message
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?>').html("<div class='alert alert-danger'>");
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                                .append("</button>");
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-danger').append('</div>');

                                                                        }
                                                                    },

                                                                })
                                                            },
                                                            filter: function() {
                                                                return $(this).is(":visible");
                                                            },
                                                        });

                                                        $("a[data-toggle=\"tab\"]").click(function(e) {
                                                            e.preventDefault();
                                                            $(this).tab("show");
                                                        });
                                                    });
                                                    /*When clicking on Full hide fail/success boxes */
                                                    $('#name').focus(function() {
                                                        $('#success').html('');
                                                    });
                                                </script>
                                            <?php } else { ?>
                                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>" class="product-button"><i class="fa fa-heart"></i></a>
                                            <?php }
                                            ## --------------------------------------------- END : WISHLIST------------------------------------------ 
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                <?php
                    $int_cnt++;
                    if ($int_cnt % $int_new_row_divison == 0) {
                        print "</div><div class='row padding-10 " . $str_row_class . "'>";
                    }
                    $rs_list->MoveNext();
                }
                $rs_list->MoveFirst(); ?>
            </div>

            <div class="row padding-10 <?php print($str_row_class); ?>">
                <div class="col-md-12" align="center">
                    <div class="brn-btn">
                        <a href="./product/trending" title="" class="btn btn-pink"><b>View More</b>&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div><br />


            <br />
        <?php } ?>
        <?php #------------------------------ END - Trending Products--------------------------------------------------- 
        ?>


        <?php #------------------------------ START - Home Content 1 --------------------------------------------------- 
        ?>
        <?php $arr_test1 = array_keys($arr_xml_list_home_content);
        if ($arr_test1[0] != "ROOT_ITEM") { ?>

            <?php /* ?><div class="row padding-10">
                <div class="col-md-12" align="center">
                    <h1></h1>
                    <hr class="star-primary"/>
                </div>
            </div>  
            <?php */ ?>
            <div class="row padding-10">
                <?php
                $int_arr_lim = 0;
                $i = 1;

                foreach ($arr_xml_list_home_content as $key => $val) {
                    $int_arr_lim++;
                    if (is_array($val)) {  ?>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" align="center">
                            <?php if ($arr_xml_list_home_content[$key]["IMAGEFILENAME"] != "") { ?>
                                <?php if ($arr_xml_list_home_content[$key]["PHOTOURL"] != "") { ?>
                                    <?php if ($arr_xml_list_home_content[$key]["OPENURLINNEWWINDOW"] == "YES") {
                                        $str_openurlinnewwindow = "_blank";
                                    } else {
                                        $str_openurlinnewwindow = "_self";
                                    } ?>
                                    <a href="<?php print($arr_xml_list_home_content[$key]["PHOTOURL"]) ?>" alt="<?php print($arr_xml_list_home_content[$key]["HOVERTEXT"]) ?>" title="<?php print($arr_xml_list_home_content[$key]["HOVERTEXT"]) ?>" target="<?php print($str_openurlinnewwindow) ?>"><img src="<?php print($str_img_path_home . $arr_xml_list_home_content[$key]["IMAGEFILENAME"]); ?>" alt="" title="" class="img-responsive" /></a>
                                <?php } else { ?>
                                    <img src="<?php print($str_img_path_home . $arr_xml_list_home_content[$key]["IMAGEFILENAME"]); ?>" alt="" title="" class="img-responsive" />
                                <?php } ?>
                            <?php } ?>
                            <?php if ($arr_xml_list_home_content[$key]["TITLE"] != "") { ?>
                                <h3><?php print($arr_xml_list_home_content[$key]["TITLE"]); ?></h3>
                            <?php } ?>
                            <?php if ($arr_xml_list_home_content[$key]["DESCRIPTION"] != "") { ?>
                                <p align="justify">
                                    <?php print($arr_xml_list_home_content[$key]["DESCRIPTION"]); ?>
                                </p>
                            <?php } ?><br />
                        </div>
                <?php
                    }
                    if ($int_arr_lim % 3 == 0) {
                        print "</div><div class='row padding-10'>";
                    }
                } ?>
            </div>

        <?php } ?>
        <?php #------------------------------ END - Home Content 1 --------------------------------------------------- 
        ?>

        <?php #------------------------------ START - New Products--------------------------------------------------- 
        ?>
        <?php
        # Select Query to get list
        $str_query_select = "";
        /*
    $str_query_select = "SELECT a.*, b.subcattitle, d.title AS cattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl FROM " .$str_db_table_name_store. " a ";
    $str_query_select .= "LEFT JOIN ".$str_db_table_name_store_cat." d ON a.catpkid=d.catpkid  AND d.visible='YES' ";
    $str_query_select .= "LEFT JOIN ".$str_db_table_name_store_subcat." b ON a.subcatpkid=b.subcatpkid AND b.visible='YES' ";
    $str_query_select .= "LEFT JOIN ".$str_db_table_name_store_photo." c ON a.pkid=c.masterpkid WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.visible='YES' AND a.displayasnew='YES' ";
    $str_query_select .= "ORDER BY a.createdatetime, a.displayorder, a.title LIMIT 0,4 ";
*/

        //$str_query_select = "SELECT a.*, "; // If GROUP BY is used in query then * can not be used
        $str_query_select = "SELECT temp.pkid, temp.catpkid, temp.subcatpkid, temp.title, temp.listprice, temp.ourprice, temp.memberprice, temp.weight, ";
        $str_query_select .= "temp.description, temp.createdatetime, temp.displayasnew, temp.displayashot, temp.displayasfeatured, temp.displayorder, ";
        $str_query_select .= "temp.subcattitle, temp.thumbphotofilename, temp.largephotofilename, temp.imageurl, temp.cattitle, ";
        $str_query_select .= "temp.colortitle, temp.sizetitle ";
        $str_query_select .= "FROM ( ";
        $str_query_select .= "SELECT a.pkid, a.catpkid, a.subcatpkid, a.title, a.listprice, a.ourprice, a.memberprice, a.weight, ";
        $str_query_select .= "a.description, a.createdatetime, a.displayasnew, a.displayashot, a.displayasfeatured, a.displayorder, ";
        $str_query_select .= "b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title AS cattitle, ";
        $str_query_select .= "f.title AS colortitle, ";
        //$str_query_select .= "group_concat(n.title ) AS sizetitle "; // Use this line if want all sizes in same record
        $str_query_select .= "min(n.title) AS sizetitle "; // Use this line if want first size only
        //$str_query_select .= "max(n.title) AS sizetitle "; // Use this line if want last size only
        $str_query_select .= " FROM " . $str_db_table_name_store . " a ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_store_cat . " d ON a.catpkid=d.catpkid AND d.visible='YES' ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_store_subcat . " b ON a.subcatpkid=b.subcatpkid ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_store_photo . " c ON a.pkid=c.masterpkid ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_color . " e ON a.pkid=e.itempkid ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_color_master . " f ON e.masterpkid=f.pkid ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_size . " m ON a.pkid=m.itempkid ";
        $str_query_select .= "LEFT JOIN " . $str_db_table_name_size_master . " n ON m.masterpkid=n.pkid ";
        $str_query_select .= " WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.visible='YES' AND a.approved='YES' AND a.displayasnew ='YES' ";
        $str_query_select .= " GROUP BY a.pkid, a.catpkid, a.subcatpkid, a.title, a.listprice, a.ourprice, a.memberprice, a.weight, a.description, a.createdatetime, a.displayasnew, a.displayashot, a.displayasfeatured, b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title, f.title ";
        $str_query_select .= " ORDER BY a.createdatetime, a.displayorder, a.title ) AS temp ";
        $str_query_select .= " GROUP BY temp.pkid, temp.catpkid, temp.subcatpkid, temp.title, temp.listprice, temp.ourprice, temp.memberprice, temp.weight, temp.description, temp.createdatetime, temp.displayasnew, temp.displayashot, temp.displayasfeatured, temp.subcattitle, temp.thumbphotofilename, temp.largephotofilename, temp.imageurl, temp.cattitle, temp.title ";
        $str_query_select .= " HAVING MAX(temp.createdatetime) ";
        $str_query_select .= " ORDER BY temp.createdatetime DESC, temp.displayorder, temp.title ";
        $str_query_select .= " LIMIT 0,4";

        //print $str_query_select;
        $rs_list = GetRecordSet($str_query_select);
        $int_total_records = $rs_list->Count();
        if ($int_total_records > 0) { ?>

            <div class="row padding-10" id="user_favorite">
                <div class="col-md-12" align="center">
                    <div class="section-title text-center border-line mb-10">
                        <h2 class="block_title"><?php print $STR_TITLE_HOME_NEW; ?></h2>
                    </div>
                </div>
            </div>
            <?php if ($str_flag_view == "M") {
                print "<br/>";
            } ?>
            <div class="row padding-10 <?php print($str_row_class); ?>">
                <?php
                $int_cnt = 0;
                while (!$rs_list->EOF()) {  ?>
                    <div class="<?php print($str_col_class); ?>">
                        <div class="product-box text-center">
                            <div class="row padding-10">
                                <div class="col-md-12 nopadding" align="center">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))))); ?>" title="<?php print $rs_list->Fields("title"); ?>">
                                        <?php if ($rs_list->Fields("thumbphotofilename") != "") { ?>
                                            <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $str_img_path_store . $rs_list->Fields("pkid") . "/" . $rs_list->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                        <?php } else if ($rs_list->Fields("imageurl") != "") { ?>
                                            <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $rs_list->Fields("imageurl"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                        <?php } ?>
                                    </a>
                                    <?php
                                    $str_query_select = "";
                                    $str_query_select = "SELECT mainphotofilename FROM " . $str_db_table_name_store_photo . " WHERE masterpkid=" . $rs_list->Fields("pkid") . " AND setasofferimage='LEFT' ORDER BY setasofferimage ASC";
                                    $rs_list_offerimages_left = GetRecordSet($str_query_select);
                                    //print $rs_list_offerimages->Count();
                                    if ($rs_list_offerimages_left->Count() > 0) {
                                    ?>

                                        <img src="<?php print $str_img_path_store . $rs_list->Fields("pkid") . "/" . $rs_list_offerimages_left->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-left" />
                                    <?php }  ?>
                                    <?php
                                    $str_query_select = "";
                                    $str_query_select = "SELECT mainphotofilename FROM " . $str_db_table_name_store_photo . " WHERE masterpkid=" . $rs_list->Fields("pkid") . " AND setasofferimage='RIGHT' ORDER BY setasofferimage ASC";
                                    $rs_list_offerimages_right = GetRecordSet($str_query_select);
                                    //print $rs_list_offerimages->Count();
                                    if ($rs_list_offerimages_right->Count() > 0) {
                                    ?>
                                        <img src="<?php print $str_img_path_store . $rs_list->Fields("pkid") . "/" . $rs_list_offerimages_right->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-right" />
                                    <?php }  ?>
                                </div>
                            </div>
                            <div class="row padding-10">
                                <div class="col-md-12 product-content-bg">
                                    <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))))); ?>" title="<?php print $rs_list->Fields("title"); ?>"><b class="text-muted"><?php print MakeStringShort($rs_list->Fields("title"), 23); ?></b></a></p>
                                    <?php /* ?><p>Product Code : 000000</p><?php */ ?>
                                    <p>
                                        <?php if ($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) { ?>
                                            <?php
                                            $int_wholesaler_price = "";
                                            $int_wholesaler_price = $rs_list->Fields("memberprice") / $int_conversionrate;
                                            $int_hdn_price = $int_wholesaler_price;
                                            ?>
                                            <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_wholesaler_price), 0) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                        <?php } else {  ?>
                                            <?php if ($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                                $int_list_price = "";
                                                $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;

                                                $int_our_price = "";
                                                $int_our_price = $rs_list->Fields("ourprice") / $int_conversionrate;
                                                $int_hdn_price = $int_our_price;
                                            ?>
                                                <strike class="text-muted"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 0) . "&nbsp;" . $str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                                <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_our_price), 0) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                                <?php
                                                $int_per_discount = 0;
                                                $int_per_discount = (100 - ($rs_list->Fields("ourprice") / $rs_list->Fields("listprice")) * 100);
                                                if ($int_per_discount > 0) { ?>
                                                    <span class="label label-primary"><b><?php print number_format($int_per_discount, 2); ?>% OFF</b></span>
                                                <?php } ?>
                                            <?php } else {
                                                $int_list_price = "";
                                                $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;
                                                $int_hdn_price = $int_list_price;
                                            ?>

                                                <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 0) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                            <?php }  ?>
                                        <?php }  ?>
                                    </p>
                                </div>
                            </div>
                            <div id="success<?php print $rs_list->fields("pkid"); ?>"></div>
                            <div class="row padding-10">
                                <div class="col-md-12">
                                    <div class="row padding-10">
                                        <?php /*?> <div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                     <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-button"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a> 
                                </div> <?php */ ?>
                                        <div class="col-md-6 col-sm-6 col-xs-6 nopadding">

                                            <form class='myform' id="frm_add<?php print $rs_list->Fields("pkid"); ?>" name="frm_add<?php print $rs_list->Fields("pkid"); ?>" onsubmit="return addToCart('frm_add<?php print $rs_list->Fields("pkid"); ?>')" novalidate>
                                                <input type="hidden" name="hdn_prodpkid" id="hdn_prodpkid" value="<?php print $rs_list->Fields("pkid"); ?>">
                                                <input type="hidden" name="hdn_prodtitle" id="hdn_prodtitle" value="<?php print $rs_list->Fields("title"); ?>">

                                                <?php /* ?>
						<input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $int_cat_pkid; ?>">
						<input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $int_sub_cat_pkid; ?>">
						<?php */ ?>
                                                <input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $rs_list->Fields("catpkid"); ?>">
                                                <input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $rs_list->Fields("subcatpkid"); ?>">

                                                <input type="hidden" name="hdn_weight" id="hdn_weight" value="<?php print $rs_list->Fields("weight"); ?>">
                                                <input type="hidden" name="hdn_userpkid" id="hdn_userpkid" value="<?php print $int_userpkid; ?>">
                                                <input type="hidden" name="hdn_username" id="hdn_username" value="<?php print $str_username; ?>">
                                                <input type="hidden" name="hdn_discount" id="hdn_discount" value="<?php print $int_wholesaler_discount; ?>">
                                                <input type="hidden" name="hdn_referredbypkid" id="hdn_referredbypkid" value="<?php print $int_referredbypkid; ?>">
                                                <input type="hidden" name="hdn_commission" id="hdn_commission" value="<?php print $int_commission; ?>">
                                                <input type="hidden" name="rdo_color" id="rdo_color" value="<?php print $rs_list->Fields("colortitle"); ?>">
                                                <input type="hidden" name="rdo_size" id="rdo_size" value="<?php print $rs_list->Fields("sizetitle"); ?>">
                                                <input type="hidden" name="rdo_tailoringoption" id="rdo_tailoringoption" value="<?php print $int_tailoring_service_pkid; ?>">
                                                <input type="hidden" name="cbo_semistitched_stitchingsize" id="cbo_semistitched_stitchingsize" value="0">
                                                <input type="hidden" name="txt_blouse_length" id="txt_blouse_length" value="0">
                                                <input type="hidden" name="txt_blouse_sleeve_length" id="txt_blouse_sleeve_length" value="0">
                                                <input type="hidden" name="cbo_blouse_aroundbust" id="cbo_blouse_aroundbust" value="0">
                                                <input type="hidden" name="txt_blouse_abovearoundwaist" id="txt_blouse_abovearoundwaist" value="0">
                                                <input type="hidden" name="txt_blouse_frontneckdepth" id="txt_blouse_frontneckdepth" value="0">
                                                <input type="hidden" name="txt_blouse_backneckdepth" id="txt_blouse_backneckdepth" value="0">
                                                <input type="hidden" name="txt_lehenga_aroundwaist" id="txt_lehenga_aroundwaist" value="0">
                                                <input type="hidden" name="txt_lehenga_aroundhips" id="txt_lehenga_aroundhips" value="0">
                                                <input type="hidden" name="txt_lehenga_length" id="txt_lehenga_length" value="0">
                                                <input type="hidden" name="txt_salwar_yourheight" id="txt_salwar_yourheight" value="0">
                                                <input type="hidden" name="txt_salwar_frontneckdepth" id="txt_salwar_frontneckdepth" value="0">
                                                <input type="hidden" name="txt_salwar_bottomlength" id="txt_salwar_bottomlength" value="0">
                                                <input type="hidden" name="txt_salwar_kameezlength" id="txt_salwar_kameezlength" value="0">
                                                <input type="hidden" name="txt_salwar_sleevelength" id="txt_salwar_sleevelength" value="0">
                                                <input type="hidden" name="cbo_salwar_sleevestyle" id="cbo_salwar_sleevestyle" value="0">
                                                <input type="hidden" name="cbo_salwar_aroundbust" id="cbo_salwar_aroundbust" value="0">
                                                <input type="hidden" name="txt_salwar_abovearoundwaist" id="txt_salwar_abovearoundwaist" value="0">
                                                <input type="hidden" name="txt_salwar_aroundwaist" id="txt_salwar_aroundwaist" value="0">
                                                <input type="hidden" name="txt_salwar_aroundhips" id="txt_salwar_aroundhips" value="0">
                                                <input type="hidden" name="txt_salwar_backneckdepth" id="txt_salwar_backneckdepth" value="0">
                                                <input type="hidden" name="txt_salwar_aroundthigh" id="txt_salwar_aroundthigh" value="0">
                                                <input type="hidden" name="txt_salwar_aroundknee" id="txt_salwar_aroundknee" value="0">
                                                <input type="hidden" name="txt_salwar_aroundcalf" id="txt_salwar_aroundcalf" value="0">
                                                <input type="hidden" name="txt_salwar_aroundankle" id="txt_salwar_aroundankle" value="0">

                                                <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $int_hdn_price;  ?>" />
                                                <input type="hidden" id="txt_quantity" name="txt_quantity" value="1" />
                                                <input type="hidden" id="hdn_description" name="hdn_description" value="" />
                                                <input type="hidden" id="rdo_shipping" name="rdo_shipping" value="" />
                                                <?php
                                                $skuval = date("YmdHis");
                                                $skuval = GetEncryptId($rs_list->Fields("pkid"));
                                                ?>
                                                <input type="hidden" id="specssku" name="specssku" value="<?php print $skuval;  ?>" />


                                                <button type="submit" class="product-buynow-button" name='btn_addtocart' id="btn_addtocart_<?php print $rs_list->Fields("pkid"); ?>" style="border-bottom: 1px solid transparent; width: 100%;"><i class="fa fa-shopping-cart"></i></button>

                                            </form>
                                            <?php /* ?><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-buynow-button"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a><?php */ ?>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                            <?php
                                            ## --------------------------------------------- START : WISHLIST------------------------------------------
                                            if ($str_member_flag) {
                                                $str_query_select = "";
                                                $str_query_select = "SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=" . $_SESSION["userpkid"] . " AND itempkid=" . $rs_list->fields("pkid");
                                                $rs_list_fav = GetRecordSet($str_query_select);

                                                $str_fav_class = "";
                                                $str_fav_flag = "";
                                                if ($rs_list_fav->Count() == 0) {
                                                    $str_fav_class = "";
                                                    $str_fav_flag = "FAV";
                                                } else if ($rs_list_fav->Count() > 0) {
                                                    $str_fav_class = "text-primary";
                                                    $str_fav_flag = "UNFAV";
                                                } ?>
                                                <form name="frm_user_favorite_new<?php print $rs_list->fields("pkid"); ?>" id="frm_user_favorite_new<?php print $rs_list->fields("pkid"); ?>" novalidate>

                                                    <input type="hidden" name="userpkid" id="userpkid<?php print($rs_list->fields("pkid")) ?>" value="<?php print($_SESSION["userpkid"]); ?>">
                                                    <input type="hidden" name="itempkid" id="itempkid<?php print($rs_list->fields("pkid")) ?>" value="<?php print($rs_list->fields("pkid")) ?>">

                                                    <input type="hidden" name="flgfav" id="flgfav<?php print($rs_list->fields("pkid")) ?>" value="<?php print($str_fav_flag); ?>">



                                                    <?php //print $rs_list_fav->Count(); 
                                                    ?>
                                                    <button id="frm_submit_new<?php print($rs_list->fields("pkid")) ?>" name="frm_submit_new<?php print($rs_list->fields("pkid")) ?>" type="submit" style="display: none;"></button>
                                                </form>

                                                <a onclick="$('#frm_submit_new<?php print($rs_list->fields("pkid")) ?>').click();" class="product-button <?php print $str_fav_class; ?>"><i class="fa fa-heart"></i></a>
                                                <script>
                                                    $(function() {

                                                        $("#frm_user_favorite_new<?php print $rs_list->fields("pkid"); ?> input").jqBootstrapValidation({
                                                            preventSubmit: true,
                                                            submitError: function($form, event, errors) {
                                                                // something to have when submit produces an error ?
                                                                // Not decided if I need it yet
                                                            },
                                                            submitSuccess: function($form, event) {
                                                                event.preventDefault(); // prevent default submit behaviour
                                                                // get values from FORM

                                                                var userpkid = $("input#userpkid<?php print($rs_list->fields("pkid")) ?>").val();
                                                                //alert(userpkid);
                                                                var itempkid = $("input#itempkid<?php print($rs_list->fields("pkid")) ?>").val();
                                                                var flgfav = $("input#flgfav<?php print($rs_list->fields("pkid")) ?>").val();


                                                                $.ajax({

                                                                    url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                                                    type: "POST",
                                                                    data: {

                                                                        userpkid: userpkid,
                                                                        itempkid: itempkid,
                                                                        flgfav: flgfav
                                                                    },
                                                                    cache: false,
                                                                    success: function(data) {
                                                                        //alert(data);
                                                                        var $responseText = JSON.parse(data);
                                                                        if ($responseText.status == 'SUC')
                                                                        //                                        if(($responseText.status).equal("SUC"))
                                                                        {
                                                                            // Success message
                                                                            //                        alert(data);

                                                                            $('#success<?php print $rs_list->fields("pkid"); ?>').html("<div class='alert alert-success small'>");
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                                .append("</button>");
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-success ').append('</div>');
                                                                            //setTimeout(function(){ location.reload() }, 2000);
                                                                            setTimeout(function() {
                                                                                window.location.href = "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>#newarrivals";
                                                                            }, 5000);
                                                                        } else if ($responseText.status == 'ERR') {
                                                                            // Fail message
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?>').html("<div class='alert alert-danger'>");
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                                .append("</button>");
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                                            $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-danger').append('</div>');

                                                                        }
                                                                    },

                                                                })
                                                            },
                                                            filter: function() {
                                                                return $(this).is(":visible");
                                                            },
                                                        });

                                                        $("a[data-toggle=\"tab\"]").click(function(e) {
                                                            e.preventDefault();
                                                            $(this).tab("show");
                                                        });
                                                    });
                                                    /*When clicking on Full hide fail/success boxes */
                                                    $('#name').focus(function() {
                                                        $('#success').html('');
                                                    });
                                                </script>
                                            <?php } else { ?>
                                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>" class="product-button"><i class="fa fa-heart"></i></a>
                                            <?php }
                                            ## --------------------------------------------- END : WISHLIST------------------------------------------ 
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                <?php
                    $int_cnt++;
                    if ($int_cnt % $int_new_row_divison == 0) {
                        print "</div><div class='row padding-10 " . $str_row_class . "'>";
                    }
                    $rs_list->MoveNext();
                }
                $rs_list->MoveFirst(); ?>
            </div>

            <div class="row padding-10 <?php print($str_row_class); ?>">
                <div class="col-md-12" align="center">
                    <div class="brn-btn">
                        <a href="./product/new" title="" class="btn btn-pink"><b>View More</b>&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <br />

        <?php } ?>
        <?php #------------------------------ END - New Products--------------------------------------------------- 
        ?>


        <?php #------------------------------ START - Home Content 2 --------------------------------------------------
        ?>
        <?php $arr_test1 = array_keys($arr_xml_list_home_content2);
        if ($arr_test1[0] != "ROOT_ITEM") {  ?>

            <div class="row padding-10">
                <?php

                if ($arr_test1[0] != "ROOT_ITEM") {
                    $i = 1;
                    $int_arr_lim = 0; ?>

                    <?php foreach ($arr_xml_list_home_content2 as $key => $val) {
                        $int_arr_lim++;

                        if (is_array($val)) {
                    ?>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" align="center">
                                <?php if ($arr_xml_list_home_content2[$key]["IMAGEFILENAME"] != "") { ?>
                                    <?php if ($arr_xml_list_home_content2[$key]["PHOTOURL"] != "") { ?>
                                        <?php if ($arr_xml_list_home_content2[$key]["OPENURLINNEWWINDOW"] == "YES") {
                                            $str_openurlinnewwindow = "_blank";
                                        } else {
                                            $str_openurlinnewwindow = "_self";
                                        } ?>
                                        <a href="<?php print($arr_xml_list_home_content2[$key]["PHOTOURL"]) ?>" alt="<?php print($arr_xml_list_home_content2[$key]["HOVERTEXT"]) ?>" title="<?php print($arr_xml_list_home_content2[$key]["HOVERTEXT"]) ?>" target="<?php print($str_openurlinnewwindow) ?>"><img src="<?php print($str_img_path_home2 . $arr_xml_list_home_content2[$key]["IMAGEFILENAME"]); ?>" alt="" title="" class="img-responsive" /></a>
                                    <?php } else { ?>
                                        <img src="<?php print($str_img_path_home2 . $arr_xml_list_home_content2[$key]["IMAGEFILENAME"]); ?>" alt="" title="" class="img-responsive" />
                                    <?php } ?>
                                <?php } ?>
                                <?php if ($arr_xml_list_home_content2[$key]["TITLE"] != "") { ?>
                                    <h3><?php print($arr_xml_list_home_content2[$key]["TITLE"]); ?></h3>
                                <?php } ?>
                                <?php if ($arr_xml_list_home_content2[$key]["DESCRIPTION"] != "") { ?>
                                    <p align="justify">
                                        <?php print($arr_xml_list_home_content2[$key]["DESCRIPTION"]); ?>
                                    </p>
                                <?php } ?><br />
                            </div>

                    <?php  }

                        if ($int_arr_lim % 3 == 0) {
                            print "</div><div class='row padding-10'>";
                        }
                        //                                if($i % 2 != 0){ print "<hr/><h4><b><div class='row test'>"; }
                        //                            $i++;
                    } ?>
            </div>
        <?php  } ?>

    <?php } ?>
    <?php #------------------------------ END - Home Content 2 --------------------------------------------------- 
    ?>

    <?php #------------------------------ START - Featured Products--------------------------------------------------- 
    ?>
    <?php
    # Select Query to get list
    $str_query_select = "";
    /*
    $str_query_select = "SELECT a.*, b.subcattitle, d.title AS cattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl FROM " .$str_db_table_name_store. " a ";
    $str_query_select .= "LEFT JOIN ".$str_db_table_name_store_cat." d ON a.catpkid=d.catpkid ";
    $str_query_select .= "LEFT JOIN ".$str_db_table_name_store_subcat." b ON a.subcatpkid=b.subcatpkid ";
    $str_query_select .= "LEFT JOIN ".$str_db_table_name_store_photo." c ON a.pkid=c.masterpkid WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.visible='YES' AND a.displayasfeatured='YES' ";
    $str_query_select .= "ORDER BY a.createdatetime, a.displayorder, a.title LIMIT 0,4 ";
*/
    //$str_query_select = "SELECT a.*, "; // If GROUP BY is used in query then * can not be used
    $str_query_select = "SELECT temp.pkid, temp.catpkid, temp.subcatpkid, temp.title, temp.listprice, temp.ourprice, temp.memberprice, temp.weight, ";
    $str_query_select .= "temp.description, temp.createdatetime, temp.displayasnew, temp.displayashot, temp.displayasfeatured, temp.displayorder, ";
    $str_query_select .= "temp.subcattitle, temp.thumbphotofilename, temp.largephotofilename, temp.imageurl, temp.cattitle, ";
    $str_query_select .= "temp.colortitle, temp.sizetitle ";
    $str_query_select .= "FROM ( ";
    $str_query_select .= "SELECT a.pkid, a.catpkid, a.subcatpkid, a.title, a.listprice, a.ourprice, a.memberprice, a.weight, ";
    $str_query_select .= "a.description, a.createdatetime, a.displayasnew, a.displayashot, a.displayasfeatured, a.displayorder, ";
    $str_query_select .= "b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title AS cattitle, ";
    $str_query_select .= "f.title AS colortitle, ";
    //$str_query_select .= "group_concat(n.title ) AS sizetitle "; // Use this line if want all sizes in same record
    $str_query_select .= "min(n.title) AS sizetitle "; // Use this line if want first size only
    //$str_query_select .= "max(n.title) AS sizetitle "; // Use this line if want last size only
    $str_query_select .= " FROM " . $str_db_table_name_store . " a ";
    $str_query_select .= "LEFT JOIN " . $str_db_table_name_store_cat . " d ON a.catpkid=d.catpkid AND d.visible='YES' ";
    $str_query_select .= "LEFT JOIN " . $str_db_table_name_store_subcat . " b ON a.subcatpkid=b.subcatpkid ";
    $str_query_select .= "LEFT JOIN " . $str_db_table_name_store_photo . " c ON a.pkid=c.masterpkid ";
    $str_query_select .= "LEFT JOIN " . $str_db_table_name_color . " e ON a.pkid=e.itempkid ";
    $str_query_select .= "LEFT JOIN " . $str_db_table_name_color_master . " f ON e.masterpkid=f.pkid ";
    $str_query_select .= "LEFT JOIN " . $str_db_table_name_size . " m ON a.pkid=m.itempkid ";
    $str_query_select .= "LEFT JOIN " . $str_db_table_name_size_master . " n ON m.masterpkid=n.pkid ";
    $str_query_select .= " WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.visible='YES' AND a.approved='YES' AND a.displayasfeatured ='YES' ";
    $str_query_select .= " GROUP BY a.pkid, a.catpkid, a.subcatpkid, a.title, a.listprice, a.ourprice, a.memberprice, a.weight, a.description, a.createdatetime, a.displayasnew, a.displayashot, a.displayasfeatured, b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title, f.title ";
    $str_query_select .= " ORDER BY a.createdatetime, a.displayorder, a.title ) AS temp ";
    $str_query_select .= " GROUP BY temp.pkid, temp.catpkid, temp.subcatpkid, temp.title, temp.listprice, temp.ourprice, temp.memberprice, temp.weight, temp.description, temp.createdatetime, temp.displayasnew, temp.displayashot, temp.displayasfeatured, temp.subcattitle, temp.thumbphotofilename, temp.largephotofilename, temp.imageurl, temp.cattitle, temp.title ";
    $str_query_select .= " HAVING MAX(temp.createdatetime) ";
    $str_query_select .= " ORDER BY temp.createdatetime DESC, temp.displayorder, temp.title ";
    $str_query_select .= " LIMIT 0,4";

    //print $str_query_select;
    $rs_list = GetRecordSet($str_query_select);
    $int_total_records = $rs_list->Count();
    if ($int_total_records > 0) { ?>
        <div class="row padding-10">
            <div class="col-md-12" align="center">
                <div class="section-title text-center border-line mb-10">
                    <h2 class="block_title"><?php print $STR_TITLE_HOME_FEATURED; ?></h2>
                </div>
            </div>
        </div>
        <?php if ($str_flag_view == "M") {
            print "<br/>";
        } ?>
        <div class="row padding-10 <?php print($str_row_class); ?>">
            <?php
            $int_cnt = 0;
            while (!$rs_list->EOF()) {  ?>
                <div class="<?php print($str_col_class); ?>">
                    <div class="product-box text-center">
                        <div class="row padding-10">
                            <div class="col-md-12 nopadding" align="center">
                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))))); ?>" title="<?php print $rs_list->Fields("title"); ?>">
                                    <?php if ($rs_list->Fields("thumbphotofilename") != "") { ?>
                                        <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $str_img_path_store . $rs_list->Fields("pkid") . "/" . $rs_list->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                    <?php } else if ($rs_list->Fields("imageurl") != "") { ?>
                                        <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $rs_list->Fields("imageurl"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                    <?php } ?>
                                </a>
                                <?php
                                $str_query_select = "";
                                $str_query_select = "SELECT mainphotofilename FROM " . $str_db_table_name_store_photo . " WHERE masterpkid=" . $rs_list->Fields("pkid") . " AND setasofferimage='LEFT' ORDER BY setasofferimage ASC";
                                $rs_list_offerimages_left = GetRecordSet($str_query_select);
                                //print $rs_list_offerimages->Count();
                                if ($rs_list_offerimages_left->Count() > 0) {
                                ?>

                                    <img src="<?php print $str_img_path_store . $rs_list->Fields("pkid") . "/" . $rs_list_offerimages_left->Fields("thumbphotofilename"); ?>" alt="" title="" class="img-top-left" />
                                <?php }  ?>
                                <?php
                                $str_query_select = "";
                                $str_query_select = "SELECT mainphotofilename FROM " . $str_db_table_name_store_photo . " WHERE masterpkid=" . $rs_list->Fields("pkid") . " AND setasofferimage='RIGHT' ORDER BY setasofferimage ASC";
                                $rs_list_offerimages_right = GetRecordSet($str_query_select);
                                //print $rs_list_offerimages->Count();
                                if ($rs_list_offerimages_right->Count() > 0) {
                                ?>
                                    <img src="<?php print $str_img_path_store . $rs_list->Fields("pkid") . "/" . $rs_list_offerimages_right->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-right" />
                                <?php }  ?>
                            </div>
                        </div>
                        <div class="row padding-10">
                            <div class="col-md-12 product-content-bg">
                                <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))))); ?>" title="<?php print $rs_list->Fields("title"); ?>"><b class="text-muted"><?php print MakeStringShort($rs_list->Fields("title"), 23); ?></b></a></p>
                                <p>
                                    <?php if ($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) { ?>
                                        <?php
                                        $int_wholesaler_price = "";
                                        $int_wholesaler_price = $rs_list->Fields("memberprice") / $int_conversionrate;
                                        $int_hdn_price = $int_wholesaler_price;
                                        ?>
                                        <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_wholesaler_price), 0) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                    <?php } else {  ?>
                                        <?php if ($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                            $int_list_price = "";
                                            $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;

                                            $int_our_price = "";
                                            $int_our_price = $rs_list->Fields("ourprice") / $int_conversionrate;
                                            $int_hdn_price = $int_our_price;
                                        ?>
                                            <strike class="text-muted"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 0) . "&nbsp;" . $str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                            <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_our_price), 0) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                            <?php
                                            $int_per_discount = 0;
                                            $int_per_discount = (100 - ($rs_list->Fields("ourprice") / $rs_list->Fields("listprice")) * 100);
                                            if ($int_per_discount > 0) { ?>
                                                <span class="label label-primary"><b><?php print number_format($int_per_discount, 2); ?>% OFF</b></span>
                                            <?php } ?>
                                        <?php } else {
                                            $int_list_price = "";
                                            $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;
                                            $int_hdn_price = $int_list_price;
                                        ?>

                                            <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 0) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                        <?php }  ?>
                                    <?php }  ?>
                                </p>
                            </div>
                        </div>
                        <div id="success<?php print $rs_list->fields("pkid"); ?>"></div>
                        <div class="row padding-10">
                            <div class="col-md-12">
                                <div class="row padding-10">
                                    <?php /*?><div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-button"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a>                                </div><?php */ ?>
                                    <div class="col-md-6 col-sm-6 col-xs-6 nopadding">

                                        <form class='myform' id="frm_add<?php print $rs_list->Fields("pkid"); ?>" name="frm_add<?php print $rs_list->Fields("pkid"); ?>" onsubmit="return addToCart('frm_add<?php print $rs_list->Fields("pkid"); ?>')" novalidate>
                                            <input type="hidden" name="hdn_prodpkid" id="hdn_prodpkid" value="<?php print $rs_list->Fields("pkid"); ?>">
                                            <input type="hidden" name="hdn_prodtitle" id="hdn_prodtitle" value="<?php print $rs_list->Fields("title"); ?>">

                                            <?php /* ?>
						<input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $int_cat_pkid; ?>">
						<input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $int_sub_cat_pkid; ?>">
						<?php */ ?>
                                            <input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $rs_list->Fields("catpkid"); ?>">
                                            <input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $rs_list->Fields("subcatpkid"); ?>">

                                            <input type="hidden" name="hdn_weight" id="hdn_weight" value="<?php print $rs_list->Fields("weight"); ?>">
                                            <input type="hidden" name="hdn_userpkid" id="hdn_userpkid" value="<?php print $int_userpkid; ?>">
                                            <input type="hidden" name="hdn_username" id="hdn_username" value="<?php print $str_username; ?>">
                                            <input type="hidden" name="hdn_discount" id="hdn_discount" value="<?php print $int_wholesaler_discount; ?>">
                                            <input type="hidden" name="hdn_referredbypkid" id="hdn_referredbypkid" value="<?php print $int_referredbypkid; ?>">
                                            <input type="hidden" name="hdn_commission" id="hdn_commission" value="<?php print $int_commission; ?>">
                                            <input type="hidden" name="rdo_color" id="rdo_color" value="<?php print $rs_list->Fields("colortitle"); ?>">
                                            <input type="hidden" name="rdo_size" id="rdo_size" value="<?php print $rs_list->Fields("sizetitle"); ?>">
                                            <input type="hidden" name="rdo_tailoringoption" id="rdo_tailoringoption" value="<?php print $int_tailoring_service_pkid; ?>">
                                            <input type="hidden" name="cbo_semistitched_stitchingsize" id="cbo_semistitched_stitchingsize" value="0">
                                            <input type="hidden" name="txt_blouse_length" id="txt_blouse_length" value="0">
                                            <input type="hidden" name="txt_blouse_sleeve_length" id="txt_blouse_sleeve_length" value="0">
                                            <input type="hidden" name="cbo_blouse_aroundbust" id="cbo_blouse_aroundbust" value="0">
                                            <input type="hidden" name="txt_blouse_abovearoundwaist" id="txt_blouse_abovearoundwaist" value="0">
                                            <input type="hidden" name="txt_blouse_frontneckdepth" id="txt_blouse_frontneckdepth" value="0">
                                            <input type="hidden" name="txt_blouse_backneckdepth" id="txt_blouse_backneckdepth" value="0">
                                            <input type="hidden" name="txt_lehenga_aroundwaist" id="txt_lehenga_aroundwaist" value="0">
                                            <input type="hidden" name="txt_lehenga_aroundhips" id="txt_lehenga_aroundhips" value="0">
                                            <input type="hidden" name="txt_lehenga_length" id="txt_lehenga_length" value="0">
                                            <input type="hidden" name="txt_salwar_yourheight" id="txt_salwar_yourheight" value="0">
                                            <input type="hidden" name="txt_salwar_frontneckdepth" id="txt_salwar_frontneckdepth" value="0">
                                            <input type="hidden" name="txt_salwar_bottomlength" id="txt_salwar_bottomlength" value="0">
                                            <input type="hidden" name="txt_salwar_kameezlength" id="txt_salwar_kameezlength" value="0">
                                            <input type="hidden" name="txt_salwar_sleevelength" id="txt_salwar_sleevelength" value="0">
                                            <input type="hidden" name="cbo_salwar_sleevestyle" id="cbo_salwar_sleevestyle" value="0">
                                            <input type="hidden" name="cbo_salwar_aroundbust" id="cbo_salwar_aroundbust" value="0">
                                            <input type="hidden" name="txt_salwar_abovearoundwaist" id="txt_salwar_abovearoundwaist" value="0">
                                            <input type="hidden" name="txt_salwar_aroundwaist" id="txt_salwar_aroundwaist" value="0">
                                            <input type="hidden" name="txt_salwar_aroundhips" id="txt_salwar_aroundhips" value="0">
                                            <input type="hidden" name="txt_salwar_backneckdepth" id="txt_salwar_backneckdepth" value="0">
                                            <input type="hidden" name="txt_salwar_aroundthigh" id="txt_salwar_aroundthigh" value="0">
                                            <input type="hidden" name="txt_salwar_aroundknee" id="txt_salwar_aroundknee" value="0">
                                            <input type="hidden" name="txt_salwar_aroundcalf" id="txt_salwar_aroundcalf" value="0">
                                            <input type="hidden" name="txt_salwar_aroundankle" id="txt_salwar_aroundankle" value="0">

                                            <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $int_hdn_price;  ?>" />
                                            <input type="hidden" id="txt_quantity" name="txt_quantity" value="1" />
                                            <input type="hidden" id="hdn_description" name="hdn_description" value="" />
                                            <input type="hidden" id="rdo_shipping" name="rdo_shipping" value="" />
                                            <?php
                                            $skuval = date("YmdHis");
                                            $skuval = GetEncryptId($rs_list->Fields("pkid"));
                                            ?>
                                            <input type="hidden" id="specssku" name="specssku" value="<?php print $skuval;  ?>" />


                                            <button type="submit" class="product-buynow-button" name='btn_addtocart' id="btn_addtocart_<?php print $rs_list->Fields("pkid"); ?>" style="border-bottom: 1px solid transparent; width: 100%;"><i class="fa fa-shopping-cart"></i></button>

                                        </form>
                                        <?php /* ?><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-buynow-button"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a><?php */ ?>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                        <?php
                                        ## --------------------------------------------- START : WISHLIST------------------------------------------
                                        if ($str_member_flag) {
                                            $str_query_select = "";
                                            $str_query_select = "SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=" . $_SESSION["userpkid"] . " AND itempkid=" . $rs_list->fields("pkid");
                                            $rs_list_fav = GetRecordSet($str_query_select);

                                            $str_fav_class = "";
                                            $str_fav_flag = "";
                                            if ($rs_list_fav->Count() == 0) {
                                                $str_fav_class = "";
                                                $str_fav_flag = "FAV";
                                            } else if ($rs_list_fav->Count() > 0) {
                                                $str_fav_class = "text-primary";
                                                $str_fav_flag = "UNFAV";
                                            } ?>
                                            <form name="frm_user_favorite_featured<?php print $rs_list->fields("pkid"); ?>" id="frm_user_favorite_featured<?php print $rs_list->fields("pkid"); ?>" novalidate>

                                                <input type="hidden" name="userpkid" id="userpkid<?php print($rs_list->fields("pkid")) ?>" value="<?php print($_SESSION["userpkid"]); ?>">
                                                <input type="hidden" name="itempkid" id="itempkid<?php print($rs_list->fields("pkid")) ?>" value="<?php print($rs_list->fields("pkid")) ?>">

                                                <input type="hidden" name="flgfav" id="flgfav<?php print($rs_list->fields("pkid")) ?>" value="<?php print($str_fav_flag); ?>">



                                                <?php //print $rs_list_fav->Count(); 
                                                ?>
                                                <button id="frm_submit_featured<?php print($rs_list->fields("pkid")) ?>" name="frm_submit_featured<?php print($rs_list->fields("pkid")) ?>" type="submit" style="display: none;"></button>
                                            </form>

                                            <a onclick="$('#frm_submit_featured<?php print($rs_list->fields("pkid")) ?>').click();" class="product-button <?php print $str_fav_class; ?>"><i class="fa fa-heart"></i></a>
                                            <script>
                                                $(function() {

                                                    $("#frm_user_favorite_featured<?php print $rs_list->fields("pkid"); ?> input").jqBootstrapValidation({
                                                        preventSubmit: true,
                                                        submitError: function($form, event, errors) {
                                                            // something to have when submit produces an error ?
                                                            // Not decided if I need it yet
                                                        },
                                                        submitSuccess: function($form, event) {
                                                            event.preventDefault(); // prevent default submit behaviour
                                                            // get values from FORM

                                                            var userpkid = $("input#userpkid<?php print($rs_list->fields("pkid")) ?>").val();
                                                            //alert(userpkid);
                                                            var itempkid = $("input#itempkid<?php print($rs_list->fields("pkid")) ?>").val();
                                                            var flgfav = $("input#flgfav<?php print($rs_list->fields("pkid")) ?>").val();


                                                            $.ajax({

                                                                url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                                                type: "POST",
                                                                data: {

                                                                    userpkid: userpkid,
                                                                    itempkid: itempkid,
                                                                    flgfav: flgfav
                                                                },
                                                                cache: false,
                                                                success: function(data) {
                                                                    //alert(data);
                                                                    var $responseText = JSON.parse(data);
                                                                    if ($responseText.status == 'SUC')
                                                                    //                                        if(($responseText.status).equal("SUC"))
                                                                    {
                                                                        // Success message
                                                                        //                        alert(data);

                                                                        $('#success<?php print $rs_list->fields("pkid"); ?>').html("<div class='alert alert-success small'>");
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                            .append("</button>");
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-success ').append('</div>');
                                                                        //setTimeout(function(){ location.reload() }, 2000);
                                                                        setTimeout(function() {
                                                                            window.location.href = "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>#featured";
                                                                        }, 5000);
                                                                    } else if ($responseText.status == 'ERR') {
                                                                        // Fail message
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?>').html("<div class='alert alert-danger'>");
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                            .append("</button>");
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                                        $('#success<?php print $rs_list->fields("pkid"); ?> > .alert-danger').append('</div>');
                                                                    }
                                                                },

                                                            })
                                                        },
                                                        filter: function() {
                                                            return $(this).is(":visible");
                                                        },
                                                    });

                                                    $("a[data-toggle=\"tab\"]").click(function(e) {
                                                        e.preventDefault();
                                                        $(this).tab("show");
                                                    });
                                                });
                                                /*When clicking on Full hide fail/success boxes */
                                                $('#name').focus(function() {
                                                    $('#success').html('');
                                                });
                                            </script>
                                        <?php } else { ?>
                                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>" class="product-button"><i class="fa fa-heart"></i></a>
                                        <?php }
                                        ## --------------------------------------------- END : WISHLIST------------------------------------------ 
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            <?php
                $int_cnt++;
                if ($int_cnt % $int_new_row_divison == 0) {
                    print "</div><div class='row padding-10 " . $str_row_class . "'>";
                }
                $rs_list->MoveNext();
            }
            $rs_list->MoveFirst(); ?>
        </div>
        <div class="row padding-10 <?php print($str_row_class); ?>">
            <div class="col-md-12" align="center">
                <div class="brn-btn">
                    <a href="./product/featured" title="" class="btn btn-pink"><b>View More</b>&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>


    <?php } ?>
    </div>
    <?php #------------------------------ END - Featured Products--------------------------------------------------- 
    ?>
</body>

<?php include($STR_USER_FOOTER_PATH);
CloseConnection(); ?>
<link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/sidebar.css" rel="stylesheet" type="text/css" />
<link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
<link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
<link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
<link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
<script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
<script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
<script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
<script type="text/javascript">
    /*function reload_item()
{
    setTimeout(function(){ location.reload() }, 300);
    header('Location: ');
}
function formSubmit(userpkid)
{
  document.forms[0].userpkid.value = userpkid;
  document.forms[0].submit();
}
function add_favorite_click()
{
    with(document.contactForm)
    {
        action="user_favorite.php";
        method="post";
        submit();
    }
}
function confirm_favourites()
{
if(confirm("Are you sure you want to add this item to your favorite list?"))
	{
            return true;
           
             }
	return false;
}
function confirm_unfavourites()
{
if(confirm("Item alredy in favorites list, Are you sure you want unfavorite this item?"))
	{
            return true;
	}
	return false;
}*/
</script>
<script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
</script>

</html>