<?php
/*
Module Name:- USER
File Name  :- signup_p.php
Create Date:- 02-FEB-2019
Intially Create By :- 015
Update History:
*/
#--------------------------------------------------------------------------------------------------------
#Include Files
session_start();
//include "../includes/configuration.php";
//include "../includes/lib_data_access.php";
//include "../includes/lib_common.php";
//include "../includes/lib_xml.php";
//include "../includes/lib_email.php";
include "./../includes/lib_common.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_email.php";
include "./../includes/lib_xml.php";
//include "./../includes/count_site_unique_view.php";

//header('Content-type: application/json');
//print_r($_POST);exit;
//print("error");
$STR_DB_TABLE_NAME = "";
$STR_DB_TABLE_NAME = "t_user";

//print_r($_POST);exit;
#--------------------------------------------------------------------------------------------------------
// check if fields passed are empty
/*$str_usertype = "";
if (isset($_POST["rdo_usertype"])) { $str_usertype = trim($_POST["rdo_usertype"]); }*/
//print_r($_POST);exit;

$str_loginid = "";
if (isset($_POST["r_loginid"])) { $str_loginid = trim($_POST["r_loginid"]); }
$str_password = "";
if (isset($_POST["r_password"])) { $str_password = trim($_POST["r_password"]); }
$str_conf_password = "";
if (isset($_POST["r_conf_password"])) { $str_conf_password = trim($_POST["r_conf_password"]); }

$str_email = "";
//if (isset($_POST["l_email"])) { $str_email = trim($_POST["l_email"]); }
$str_email = $str_loginid;
//print $str_email; exit;
/*$str_l_secretcode = "";
if (isset($_POST["l_secretcode"])) { $str_l_secretcode = trim($_POST["l_secretcode"]); }*/


/*if(empty($str_usertype))
{
    //$_SESSION['image_secret_code']="";
    $response['status']='ERR';
    $response['message']= "Select User Type!";
    echo json_encode($response); 
    return;
    //echo "Invalid Login ID!";
    //return false;
}*/

/*if(empty($str_loginid) || validateEmail($str_loginid)==false )
{
    //$_SESSION['image_secret_code']="";
    $response['status']='ERR';
    $response['message']= "Invalid Login ID!";
    echo json_encode($response); 
    return;
    //echo "Invalid Login ID!";
    //return false;
}
if(empty($str_password) || is_valid_userpassword($str_password)==false )
{
    //$_SESSION['image_secret_code']="";
    $response['status']='ERR';
    $response['message']= "Invalid Password!";
    echo json_encode($response); 
    return;
}
if(strtolower($str_password)!=strtolower($str_conf_password))
{
    //$_SESSION['image_secret_code']="";
    $response['status']='ERR';
    $response['message']= "Password and Confirm Password must be same!";
    echo json_encode($response); 
    return;
}*/
if(!empty($str_email))
{
   /* if(validateEmail($str_email)==false )
    {
        //$_SESSION['image_secret_code']="";
        $response['status']='ERR';
        $response['message']= "Invalid Email ID!";
        echo json_encode($response); 
        return;
    } */
}
/*if( empty($str_l_secretcode) || $_SESSION['image_secret_code'] != $str_l_secretcode )
{
	//$_SESSION['image_secret_code']="";
	$response['status']='ERR';
    $response['message']= "Invalid Secrete Code!";
	
    echo json_encode($response);
    return;
}*/
$str_reg_date = "";
$str_reg_date = date("Y-m-d H:i:s");
$str_ip_address = "";
$str_ip_address = $_SERVER['REMOTE_ADDR'];


#----------------------------------------------------------------------------------------------------
#Duplication Check.
$str_query_select = "";
$str_query_select = "SELECT loginid FROM ".$STR_DB_TABLE_NAME." WHERE loginid= '".ReplaceQuote($str_loginid)."'";
//print $str_query_select; exit;
$rs_list_check_duplicate = GetRecordSet($str_query_select);

if($rs_list_check_duplicate->eof() !=true)
{
    //$_SESSION['image_secret_code']="";
    $response['status']='ERR';
    $response['message']= "Login ID already exists. Please try with another!";
    echo json_encode($response);
    return;
}
#----------------------------------------------------------------------------------------------------
#code for randomly generated password...
/*function generate_password( $length = 8 ) {
$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#$";
$password = substr( str_shuffle( $chars ), 0, $length );
return $password;
}
$str_password= generate_password();*/
#----------------------------------------------------------------------------------------------------
#Insert Query

$str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME."";
$str_query_insert .= " (loginid, password, emailid, registrationdatetime, allowlogin, approved, visible, ipaddress)";
$str_query_insert .= " VALUES('".ReplaceQuote($str_loginid)."','".ReplaceQuote($str_password)."',";
$str_query_insert .= "'".ReplaceQuote($str_email)."','".ReplaceQuote($str_reg_date)."','YES','YES','YES','".ReplaceQuote($str_ip_address)."')";
//print $str_query_insert; exit;
ExecuteQuery($str_query_insert);

/*if($str_subscriber!="")
{
        $str_query_select="SELECT * FROM t_nlsubscribe WHERE emailaddress='".ReplaceQuote($str_email)."'";
        $rs_check=GetRecordset($str_query_select);

        if (!$rs_check->EOF())
        {
                if($rs_check->Fields("subscribe")=="NO")
                {
                        $str_query_select="update t_nlsubscribe set catnlpkid=3,subscribe='YES',registrationdate='".date('Y-m-d')."' where emailaddress='".ReplaceQuote($str_email)."'";
                        ExecuteQuery($str_query_select);
                }
        }
        else
        {
                $str_query_select="insert into t_nlsubscribe(catnlpkid,registrationdate,emailaddress,firstname,unsubscribedate,subscribe)";
                $str_query_select .= " values (3,'".date('Y-m-d')."','".ReplaceQuote($str_email)."','".ReplaceQuote($str_name)."','".date('Y-m-d')."','YES')";
                ExecuteQuery($str_query_select);			
        }
}*/

$fp=openXMLfile($STR_XML_FILE_PATH_MODULE."siteconfiguration.xml");
$str_from=getTagValue($STR_FROM_DEFAULT,$fp);
$str_to_user = $str_email;
$str_to_admin = getTagValue($STR_FROM_DEFAULT,$fp);
closeXMLfile($fp);

$str_subject_admin = "New user registered on ". $STR_SITE_URL . "";
$mailbody_admin = "New user registered on " . $STR_SITE_URL . " <br> Following are details: ";
$mailbody_admin.= "<BR><BR> <strong>Registration Date Time:</strong> " . $str_reg_date . " ";
$mailbody_admin.= "<br><br><strong>Login ID:</strong> " . $str_loginid . " ";
$mailbody_admin.= "<br><strong>Password:</strong> " . $str_password . " ";
//$mailbody_admin.= "<br><strong>Email Address:</strong> " . $str_email . " ";
$mailbody_admin.= "<br><strong>IP Address:</strong> " . $str_ip_address . " ";
sendmail($str_to_admin,$str_subject_admin,$mailbody_admin,$str_from,1);

$str_subject_user = "You are registered on ". $STR_SITE_URL . "";
$mailbody_user = "You are registered on " . $STR_SITE_URL . " <br> Following are details: ";
$mailbody_user.= "<BR><BR> <strong>Registration Date Time:</strong> " . $str_reg_date . " ";
$mailbody_user.= "<br><br><strong>Login ID:</strong> " . $str_loginid . " ";
$mailbody_user.= "<br><strong>Password:</strong> " . $str_password . " ";
//$mailbody_user.="<br><strong>Email Address:</strong> " . $str_email . " ";
sendmail($str_to_user,$str_subject_user,$mailbody_user,$str_from,1); 
  

//$_SESSION['image_secret_code']="";
$response['status']='SUC';
$response['message']="Your Registration has been done successfully!!!";
//        . "<br/><br/>Click here to <a href='./signin'>LOGIN</a> now.";
echo json_encode($response);
return;
#--------------------------------------------------------------------------------------------------------------------------------------------
    


/*CloseConnection();
$_SESSION['image_secret_code']="";
return true;*/			
#--------------------------------------------------------------------------------------------------------------------------------------------
?>
