//console.log("in product_listing file");
$(function () {
  //   $("form").submit(function(e){
  //       e.preventDefault();
  // var form = $(this);
  // var id = form.attr('id');
  // //console.log("in form submit");
  // $.ajax({
  //           url: "../user/product_addtocart_p.php", // For Product Listing Page
  //           type: "POST",
  //           data: form.serialize(),
  //           cache: false,
  //           success: function(data)
  //           {
  //               //console.log(data);
  // 		//alert(data);
  // 		var responseText=JSON.parse(data);
  // 		var x = form.find("input[name='hdn_prodpkid']").val();
  //               show_popup(x, responseText.message, responseText.status);
  //               show_popup('2'+x, responseText.message, responseText.status);
  //               show_popup('3'+x, responseText.message, responseText.status);
  // 	},
  // 	error: function (jqXHR, exception) {
  // 		console.log(jqXHR);
  // 	},
  //       });
  //   }); // End of $(function())

  $('a[data-toggle="tab"]').click(function (e) {
    e.preventDefault();
    $(this).tab("show");
  });
}); // End of $(function())

function addToCart(formID) {
  var form = $("#" + formID);
  // var id = form.attr('id');
  //console.log("in form submit");
  $.ajax({
    url: "../user/product_addtocart_p.php", // For Product Listing Page
    type: "POST",
    data: form.serialize(),
    cache: false,
    success: function (data) {
      //console.log(data);
      // alert(data);
      var responseText = JSON.parse(data);
      var x = form.find("input[name='hdn_prodpkid']").val();
      show_popup(x, responseText.message, responseText.status);
      show_popup("2" + x, responseText.message, responseText.status);
      show_popup("3" + x, responseText.message, responseText.status);
    },
    error: function (jqXHR, exception) {
      console.log(jqXHR);
    },
  }); // End of $(function())
  return false;
}

function show_popup(id, msg, sts) {
  //console.log("in show popup");
  if (sts == "SUC") {
    // If ADD TO CART button is pressed then add record into cart and display success message
    $("#success" + id).html("<div class='alert alert-success'>");
    $("#success" + id + " > .alert-success")
      .html(
        "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;"
      )
      .append("</button>");
    $("#success" + id + " > .alert-success").append(
      "<strong> " + msg + " </strong>"
    );
    $("#success" + id + " > .alert-success").append("</div>");
    //clear all fields
    $("#frm_add").trigger("reset");
    $("#success" + id).show();
    setTimeout(function () {
      $("#success" + id).hide();
    }, 10000);
  } else if (sts == "ERR") {
    // Fail message
    $("#success" + id).html("<div class='alert alert-danger'>");
    $("#success" + id + " > .alert-danger")
      .html(
        "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;"
      )
      .append("</button>");
    $("#success" + id + " > .alert-danger").append("<strong> " + msg + " ");
    $("#success" + id + " > .alert-danger").append("</div>");
  }
}
