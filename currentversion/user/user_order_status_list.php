<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./user_validate_session.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_xml.php";
include "./../includes/lib_datetimeyear.php";
include "./product_config.php";
//include "./../includes/http_to_https.php";
include "./user_config.php";
//print_r($_SESSION);exit;
#----------------------------------------------------------------------
$str_title_page = "";
$str_title_page = $STR_TITLE_PAGE_ORDER_STATUS;

$int_userpkid = 0;
$int_userpkid = $_SESSION["userpkid"]; 
#------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------
# get Query String Data
$str_status = "";
if(isset($_GET["cbo_status"])) { $str_status = trim($_GET["cbo_status"]); }
#------------------------------------------------------------------------------------------------
$str_query_where = "";
if($str_status != "") { 
    $str_query_where = " AND shippingstatus = '".$str_status."'";
}
# Select Query
$str_query_select = "";
$str_query_select = "SELECT *";
$str_query_select .= " FROM ".$STR_DB_TABLE_NAME_PURCHASE." ";
$str_query_select .= " WHERE userpkid = ".$int_userpkid.$str_query_where;
$str_query_select .= " ORDER BY purchasedatetime DESC";
//print $str_query_select;
$rs_list = GetRecordSet($str_query_select);//print $str_query_select."<br/><br/><br/><br/><br/><br/><br/>";
//print $rs_list->Count();exit;
#------------------------------------------------------------------------------------------------
# get Query String Data
$int_cat_pkid="";
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ $int_cat_pkid=trim($_GET["catid"]); }

$str_key = "";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_key = trim($_GET["key"]); }

# get data for paging
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page = 1; }


$str_filter = ""; 
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_key."&PagePosition=".$int_page;
#-----------------------------------------------------------------------------

#------------------------------------------------------------------------------------------------
$str_type = "";
$str_message = "";
# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
	case("E"): $str_type = "E"; break;
	case("W"): $str_type = "W"; break;
    }
}
#	Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_md; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;        
    }
} 
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($str_title_page) ;?></title>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />   
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> 
</head>
<body>
<?php include("../includes/header.php"); ?>
    <div class="container center-bg">
        
        <div class="row padding-10">
            <a name="ptop" id="ptop"></a>
            <div class="col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1>         
            </div>
        </div>
        <hr/>  
        
        <div class="row padding-10">
            <div class="col-md-3 col-xs-12 col-sm-12">
                <?php include($STR_USER_PANEL_PATH); ?>
            </div>
            
            <div class="col-md-9 col-xs-12 col-sm-12">
                <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); } ?><br/>
                <div class="row">
                    <div class="col-md-12" align="right">
                        <form name="frm_filter" method="GET" action="user_order_status_list.php#ptop">
                            <label>
                                <select id="cbo_status" name="cbo_status" class="form-control" onChange="document.forms['frm_filter'].submit();">
                                    <option value=""> -- ALL -- </option>
                                    <option value="<?php print $STR_CBO_OPTION1; ?>" <?php print(CheckSelected($STR_CBO_OPTION1,strtoupper($str_status)));?>><?php print $STR_CBO_OPTION1; ?></option>;
                                    <option value="<?php print $STR_CBO_OPTION2; ?>" <?php print(CheckSelected($STR_CBO_OPTION2,strtoupper($str_status)));?>><?php print $STR_CBO_OPTION2; ?></option>
                                    <option value="<?php print $STR_CBO_OPTION3; ?>" <?php print(CheckSelected($STR_CBO_OPTION3,strtoupper($str_status)));?>><?php print $STR_CBO_OPTION3; ?></option>
                                    <option value="<?php print $STR_CBO_OPTION4; ?>" <?php print(CheckSelected($STR_CBO_OPTION4,strtoupper($str_status)));?>><?php print $STR_CBO_OPTION4; ?></option>
                                </select>
                            </label>
                        </form>
                    </div>
                </div>
                <br/>
                    
                <?php
                if($rs_list->Count() <= 0)
                { ?>
                    <div class="row padding-10">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 alert alert-danger text-center"><?php print($STR_MSG_NO_DATA_AVAILABLE);?></div>
                    </div>
                <?php 
                } else {
                    $int_cnt = 1;    
                    while(!$rs_list->EOF()) 
                    { 
                        
                        $str_query_select = "";
                        $str_query_select = "SELECT currency_symbol, currency_shortform FROM ".$STR_DB_TABLE_NAME_COUNTRY." WHERE pkid=".$rs_list->Fields("countrypkid");
                        $rs_list_country = GetRecordSet($str_query_select);
                        
                        
                        ?>
                    <div class="thumbnail">
                            <div class="row padding-10">
                                <?php
                                $str_query_select = "";
                                $str_query_select = "SELECT a.imageurl, a.thumbphotofilename, b.listprice FROM ".$STR_DB_TABLE_NAME_PHOTO." a LEFT JOIN ".$STR_DB_TABLE_NAME." b ON a.masterpkid=b.pkid AND b.approved='YES' AND b.visible='YES' WHERE a.masterpkid=".$rs_list->Fields("productpkid")." AND a.visible='YES' AND a.setasfront='YES'" ;
                                //print $str_query_select;
                                $rs_list_image = GetRecordSet($str_query_select); ?>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                    
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("productpkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("producttitle")))));?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>">
                                        <?php if($rs_list_image->Fields("thumbphotofilename") != "") { ?>
                                            <img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("productpkid")."/".$rs_list_image->Fields("thumbphotofilename"); ?>" class="img-responsive" alt="<?php print $rs_list->Fields("producttitle"); ?>" title="<?php print $rs_list->Fields("producttitle"); ?>"/>
                                        <?php } else if($rs_list_image->Fields("imageurl") != "") { ?>
                                            <img src="<?php print $rs_list_image->Fields("imageurl"); ?>" class="img-responsive" alt="<?php print $rs_list->Fields("producttitle"); ?>" title="<?php print $rs_list->Fields("producttitle"); ?>"/>
                                        <?php } ?>
                                    </a>
                                </div>

                                <div class="col-md-7 col-lg-7 col-sm-7 col-xs-12">
                                    <span id="ret<?php print $rs_list->Fields("serialno"); ?>"></span>
                                    <h4><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("productpkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("producttitle")))));?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>"><?php print MakeStringShort($rs_list->Fields("producttitle"), 50); ?></a></h4>
                                    <h5 class="text-muted"><i><?php print DDMMMYYYYFormat($rs_list->Fields("purchasedatetime")); ?></i></h5>
                                    <ul>
                                        <?php if($rs_list->Fields("itemcode") != "") { ?>
                                            <li><span class="text-muted">Item Code</span> - <b><?php print $rs_list->Fields("itemcode"); ?></b></li>
                                        <?php } ?>
                                        
                                        <?php if($rs_list->Fields("subscriptionid") != "") { ?>
                                            <li><span class="text-muted">Transaction IDe</span> - <b><?php print $rs_list->Fields("subscriptionid"); ?></b></li>
                                        <?php } ?>
                                        
                                        <?php if($rs_list->Fields("cattitle") != "") { ?>
                                            <li><span class="text-muted">Category</span> - <b><?php print $rs_list->Fields("cattitle"); ?></b></li>
                                        <?php } ?>
                                        <?php /* ?><?php if($rs_list->Fields("subcattitle") != "") { ?>
                                            <li><span class="text-muted">Sub Category</span> - <b><?php print $rs_list->Fields("subcattitle"); ?></b></li>
                                        <?php } ?><?php */ ?>   
                                        <?php if($rs_list->Fields("color") != "") { ?>
                                            <li><span class="text-muted">Color</span> - <b><?php print $rs_list->Fields("color"); ?></b></li>
                                        <?php } ?>
                                        <?php if($rs_list->Fields("size") != "") { ?>    
                                        <li><span class="text-muted">Size</span> - <b><?php print $rs_list->Fields("size"); ?></b></li>
                                        <?php } ?>

                                            <li>
                                                <span class="text-muted">Per Unit Price</span> - <b class="text-success"><?php print $rs_list_country->Fields("currency_symbol"); ?>&nbsp;<?php
                                                $int_per_unit_price = 0;
                                                $int_per_unit_price = ceil($rs_list->Fields("price"));
                                                print $int_per_unit_price."&nbsp;".$rs_list_country->Fields("currency_shortform");
                                                ?></b>
                                                <?php /*if($rs_list_image->Fields("listprice") > 0) { ?>
                                                    &nbsp;&nbsp;
                                                    <strike><?php print $rs_list_country->Fields("currency_symbol"); ?>&nbsp;<?php print number_format(ceil($rs_list_image->Fields("listprice") / $int_conversionrate),2)."&nbsp;".$rs_list_country->Fields("currency_shortform"); ?></strike><?php } */?>
                                            </li>
                                        <?php 
                                        $int_tailoring_price = 0.00;

                                        if($rs_list->Fields("tailoringprice") > 0) { 

                                                $int_tailoring_price = ceil($rs_list->Fields("tailoringprice"));?>

                                            <li><span class="text-muted">Per Unit Tailoring Price</span> - <b class="text-success"><?php print $rs_list_country->Fields("currency_symbol"); ?>&nbsp;<?php print $int_tailoring_price."&nbsp;".$rs_list_country->Fields("currency_shortform"); ?></b></li>
                                        <?php } else { $int_tailoring_price = 0.00; } ?>

					<?php $str_tailoring_options_list = "";
				        if($rs_list->fields("tailoringoptiontitle") != "")
				        { ?>
				            <li><span class="text-muted">Tailoring Option</span> - <b><?php print $rs_list_country->Fields("tailoringoptiontitle"); ?></b></li>
				        <?php }
					if($rs_list->fields("tailoringmeasurements") != "")
				        { ?>
				            <li><span class="text-muted">Tailoring Measurements</span> - <b><?php print $rs_list_country->Fields("tailoringmeasurements"); ?></b></li>
				        <?php } ?>

                                        <li><span class="text-muted">Quantity</span> - <b><?php print $rs_list->Fields("quantity"); ?></b></li>
                                        <li><span class="text-muted">Shipping Charge</span> - <b><?php print $rs_list_country->Fields("currency_symbol"); ?>&nbsp;<?php $int_shipping_total = ceil($rs_list->Fields("shippingvalue") * $rs_list->Fields("quantity") * $rs_list->Fields("weight"));  print number_format($int_shipping_total,2)."&nbsp;".$rs_list_country->Fields("currency_shortform");?></b></li>
                                        <li><span class="text-muted">Promocode Discount</span> - <b><?php print $rs_list_country->Fields("currency_symbol"); ?>&nbsp;<?php print number_format(ceil($rs_list->Fields("finaldiscountedamount")),2)."&nbsp;".$rs_list_country->Fields("currency_shortform");?></b></li>
                                        <li><span class="text-muted">Gift Card Discount</span> - <b><?php print $rs_list_country->Fields("currency_symbol"); ?>&nbsp;<?php print number_format(ceil($rs_list->Fields("giftcard")),2)."&nbsp;".$rs_list_country->Fields("currency_shortform");?></b></li>
                                        <li><span class="text-muted">Wholesaler Discount</span> - <b><?php 
                                        $int_wholesaler_discount = 0;
                                        $int_wholesaler_discount = ceil(($rs_list->Fields("price") * $rs_list->Fields("wholesalerdiscount")) / 100);
                                        
                                        
                                        print $rs_list_country->Fields("currency_symbol"); ?>&nbsp;<?php print number_format($int_wholesaler_discount,2)."&nbsp;".$rs_list_country->Fields("currency_shortform");?></b></li>
                                        <li><span class="text-muted">Total</span> - 
                                            <b>
                                                <?php print $rs_list_country->Fields("currency_symbol")."&nbsp;"; ?><span id="extended_price<?php print $int_cnt; ?>">
                                                <?php $int_extended_total = 0; 
                                                $int_extended_total = ($int_per_unit_price + $int_tailoring_price + $int_shipping_total - ceil($rs_list->Fields("finaldiscountedamount")) - ceil($rs_list->Fields("giftcard")) - $int_wholesaler_discount) * $rs_list->Fields("quantity"); print number_format($int_extended_total,2)."&nbsp;".$rs_list_country->Fields("currency_shortform");  ?></span><span id="hdn_extended_price<?php print $int_cnt; ?>" style="display:none;"><?php print $int_extended_total; ?></span>
                                            </b>
                                            &nbsp;(<?php print $rs_list->Fields("paymentoptiontitle"); ?>)
                                        </li>
                                        
                                        <?php if($rs_list->Fields("address") != "") { ?>
                                            <li><span class="text-muted">Shipping Address</span> - <?php print $rs_list->Fields("address").", ".$rs_list->Fields("city").", ".$rs_list->Fields("zipcode").", ".$rs_list->Fields("state").", ".$rs_list->Fields("country")."" ; ?></li>
                                        <?php } ?>

                                    </ul>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                                    <h4 class="text-success text-center">
                                        <b><?php 
                                            $str_status_class = "";
                                            $str_img = "";
                                            if($rs_list->Fields("shippingstatus") == "PROCESSING") 
                                            {
                                                $str_img = "<i class='fa fa-cog'></i>&nbsp;";
                                                $str_status_class = "btn btn-info btn-block disabled";
                                            } 
                                            else if($rs_list->Fields("shippingstatus") == "SHIPPED") 
                                            {
                                                $str_img = "<i class='fa fa-truck'></i>&nbsp;";
                                                $str_status_class = "btn btn-warning btn-block disabled";
                                            }
                                            else if($rs_list->Fields("shippingstatus") == "DELIVERED") 
                                            {
                                                $str_img = "<i class='fa fa-check'></i>&nbsp;";
                                                $str_status_class = "btn btn-success btn-block disabled";
                                            }
                                            else if($rs_list->Fields("shippingstatus") == "CANCELLED") 
                                            {
                                                $str_img = "<i class='fa fa-ban'></i>&nbsp;";
                                                $str_status_class = "btn btn-danger btn-block disabled";
                                            }
                                            ?>
                                        <span class="<?php print $str_status_class; ?>"><?php print $str_img; ?><?php print $rs_list->Fields("shippingstatus"); ?></span></b>
                                    </h4>
                                    <?php print $rs_list->Fields("shippinginfo"); ?>

                                </div>
                            </div>
                            <?php if($rs_list->Fields("shippingstatus") == "SHIPPED" || $rs_list->Fields("shippingstatus") == "PROCESSING") {  ?>
                            <div class="row padding-10">
                                <div class="col-md-6 col-sm-6 col-xs-12 nopadding">
                                    <a href="user_order_status_list_p.php?pkid=<?php print $rs_list->Fields("purchasepkid"); ?>" title="Click to remove this item from cart">
                                        <div class="text-center btn-cart">
                                            <i class="fa fa-remove"></i>&nbsp;Cancel This Order
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/return-policy" title="Click to view cancel & return policy">
                                        <div class="text-center btn-cart">
                                            Cancellation & Refund Policy
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    <?php 
                    $int_cnt++;
                    $rs_list->MoveNext();
                    }?>

        <?php  } ?>
            </div>        
        </div>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
</body>
</html>
