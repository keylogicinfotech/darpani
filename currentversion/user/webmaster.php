<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/http_to_https.php";	
include "./../includes/lib_image.php";
include "./../includes/lib_xml.php";
include "./webmaster_config.php";

#----------------------------------------------------------------------
$str_title_page_metatag = 'PG_AFFILIATE';
#------------------------------------------------------------------------------	
# Get Page Title for SEO
$str_query_select="";
$str_query_select="SELECT titletag FROM " .$STR_DB_TABLE_NAME_METATAGE. " WHERE visible='YES' AND pagekey='" .$str_title_page_metatag. "'";
//print $str_query_select;
$rs_list_mt=GetRecordset($str_query_select);
#------------------------------------------------------------------------------	
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>

<?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <!-- Custom CSS -->
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />     
    
    <script language="JavaScript" type="text/JavaScript">
function frm_login_validate() { with(document.frm_login) { if(txt_login.value=="") { alert("Please enter your Affiliate ID"); txt_login.select(); txt_login.focus(); return false; } } return true; }
</script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-126128140-1');
    </script>

</head>

<body>
    <?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $STR_TITLE_AFFILIATE_PROGRAM; ?></h1>
                <hr/>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="well">
                    <p>
                    <?php 
                    $result="";
                    $result=readXml($STR_XML_FILE_PATH_MODULE."webmaster.xml","ROOT_ITEM");
//                    print_r($result);
                    $str_flag=false;
                    //while(list($key, $val) = each($result)) {
	            foreach($result as $key => $val) {    
                        if(is_array($val)) 
                        {
                            if($result[$key]["TITLE"]!="") 
                            { 
                                $str_flag=true; ?>
                        <b><?php print(MyHtmlEncode($result[$key]["TITLE"])); ?></b>
                            <?php 
                            } 
                            if($result[$key]["DESCRIPTION"]!="") 
                            { 
                                $str_flag=true; ?>
                    <p><?php print($result[$key]["DESCRIPTION"]);?></p>
                            <?php 
                            }
                        } 
                    } 
                    ?>
                    </p>
                </div>   
            </div>
        </div>
        <div class="row padding-10">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="nopadding" align="center"><b>Step 1: Sign Up</b></h4>
                    </div>
                    <div class="panel-body" style="height: 236px;">
                        <p class="text-help">If you do not already have a CCBill Affiliate ID, Sign up for one here first.</p>
                        <div align="center">
                            <form action="https://affiliateadmin.ccbill.com/signup.cgi" method="post"><input  type="hidden" name="CA" value="<?php print($STR_WEBMASTER_CA);?>"/>
            <!-- Place a Group Name or Group ID number in the value for GR to automatically add new affiliates into a group.  The Group must exist in your admin setup for this option to work. -->
                                <input type="hidden" name="GR" value="" /><input type="hidden" name="page_background" value=""/><input type="hidden" name="page_bgcolor" value="#FFFFFF" /><input type="hidden" name="page_text" value="#000000" /><input type="hidden" name="page_link" value="blue" /><input type="hidden" name="page_vlink" value="purple" /><input type="hidden" name="page_alink" value="blue" /><input type="hidden" name="table_left" value="#AEAEFF" /><input type="hidden" name="table_right" value="#FEFFC1" /><input type="hidden" name="table_text" value="#000000" /><input type="hidden" name="star_color" value="#CC0000" /><input type="hidden" name="second" value="1" />
                                <button type="submit" name="Submit" id="Submit" class="btn btn-primary"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;<b>Create a New Account</b></button>
                            <?php /*?><input type="submit" name="Submit" value="Create a New Account" class="ButtonStyle" /><?php */?>
                            </form>
                        </div>  
<!--                        <p align="center"><a href="" title="" class="btn btn-primary"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;<b>Create a New Account</b></a></p>-->
                    </div>
                </div>  
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="nopadding" align="center"><b>OR Retrieve Data</b></h4>
                    </div>
                    <div class="panel-body" style="height: 236px;">
                        <p class="text-help"> To retrieve your current data, please enter your ID, Username and Password below:</p>
                            <form action="https://affiliateadmin.ccbill.com/signup.cgi" name="sentMessage" id="contactForm" method="post">
                                <input type="hidden" name="CA" value="<?php print($STR_WEBMASTER_CA);?>" />
                                <input type="hidden" name="GR" value="" />
                                <input type="hidden" name="page_background" value="" />
                                <input type="hidden" name="page_bgcolor" value="#FFFFFF" />
                                <input type="hidden" name="page_text" value="#000000" />
                                <input type="hidden" name="page_link" value="blue" />
                                <input type="hidden" name="page_vlink" value="purple" />
                                <input type="hidden" name="page_alink" value="blue" />
                                <input type="hidden" name="table_left" value="#AEAEFF" />
                                <input type="hidden" name="table_right" value="#FEFFC1" />
                                <input type="hidden" name="table_text" value="#000000" />
                                <input type="hidden" name="star_color" value="#CC0000" />
                                
                                <div class="control-group form-group">
                                    <div class="controls">
                                        <input type="text" class="form-control input-sm" name="EID" id="EID" required data-validation-required-message="<?php print $STR_PLACEHOLDER_AFFILIATEID; ?>" placeholder="<?php print($STR_PLACEHOLDER_AFFILIATEID); ?>">
                                        <p class="help-block"></p>
                                    </div>
                                </div>
                                
                                <div class="control-group form-group">
                                    <div class="controls">
                                        <input type="text" class="form-control input-sm" id="name" name="username" placeholder="<?php print($STR_PLACEHOLDER_FULLNAME); ?>" id="username"  required data-validation-required-message="<?php print($STR_PLACEHOLDER_FULLNAME); ?>" >
                                        <p class="help-block"></p>
                                    </div>
                                 </div>

                                <div class="control-group form-group">
                                    <div class="controls">
                                        <input name="" type="password" class="form-control input-sm" value="" placeholder="<?php print($STR_PLACEHOLDER_PASSWORD); ?>" name="password" id="password" required data-validation-required-message="<?php print($STR_PLACEHOLDER_PASSWORD); ?>">
                                        <p class="help-block"></p>
                                    </div>
                                 </div>
                                
                                <div id="success"></div>
                                    <input type="hidden" name="second" value="1" />
                                        <?php /*?><input type="submit" name="Submit2" value="Retrieve Data" class="ButtonStyle" />&nbsp;
                                        <input type="reset" name="Clear" value="Clear" class="ButtonStyle" /><?php */?>
                                    <button type="submit" name="Submit2" id="Submit2" class="btn btn-primary"><b><i class="fa fa-arrow-circle-down" aria-hidden="true"></i>&nbsp;Retrieve Data</b></button>
                                    <button type="reset" name="Clear" id="Clear" class="btn btn-primary"><b><i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;Clear</b></button>
                            </form>
                         
                    </div>
                </div>  
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="nopadding" align="center"><b>Step 2: Login!</b></h4>
                    </div>
                    <div class="panel-body" style="height: 236px;">
                        <p class="text-help">Get your promo tools by logging in below with your new 6 or 7 digit Affiliate ID from CCBill.</p>
                        
                        <form action="./affiliate-home" method="POST" name="frm_login" id="frm_login" onSubmit="return frm_login_validate();">
                            <div class="controls" style="margin-top:34px">
                                <input type="text" name="txt_login" id="txt_login" class="form-control" required data-validation-required-message="Please enter your Affiliate ID." placeholder="Enter Affiliate ID" />
                            <p class="help-block"></p>
                            </div>
                            <div id="success"></div>
                            <input type="hidden" name="pt" value="MP"/>
                            <!--<input type="submit" name="Submit" id="Submit" value="Login" class="btn btn-success"/>-->
                             <button type="submit" name="Submit" id="Submit" value="Login" class="btn btn-primary"/><b><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;Login</b></button>
                            <p class="help-block"></p>
                        </form>
                        
                    </div>
                </div>  
            </div>
        </div>
    </div>

<?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
</body>
<?php include "./../includes/include_files_user.php"; ?>
<script src="<?php print(GetCurrentPathRespectToUser());?>includes/stoprightclick.js"></script>
 <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script> 
<!--<link href="<?php // print(GetCurrentPathRespectToUser());?>includes/user.css" rel="stylesheet">-->
<script src="./signup_mdl.js"></script>
<script src="./signup.js"></script>
    <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
    </script>
<div class="scrollup" style="display: block;"></div>
<script type="text/javascript">
    $(document).ready(function(){ 

    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    }); 
    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});
</script>
</html>
