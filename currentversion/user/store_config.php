<?php 
$STR_TITLE_PAGE = "Online Store";
$STR_TITLE_PAGE_ORDER_STATUS = "Order Status List";
$STR_TITLE_PAGE_PHOTO_LIST = "Photo List";
$STR_TITLE_PAGE_CART = "Cart";
$STR_TITLE_PAGE_PURCHASE_PAYMENT_LOCAL = "Payment Purchase";
$STR_TITLE_PAGE_CHECKOUT_MESSAGE = "Checkout Message";



$UPLOAD_IMG_PATH = "./mdm/store/";
$INT_IMG_WIDTH_LARGE = 900;	
$INT_IMG_WIDTH_THUMB = 300; 

$INT_RECORD_PER_PAGE = 2;
$INT_DAYS_DISPLAY_AS_NEW = 3;

# For Main List
global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "t_store"; 
$STR_DB_TABLE_NAME_ORDER_BY = "";

global $STR_DB_TABLE_NAME_PURCHASE;
global $STR_DB_TABLE_NAME_ORDER_BY_PURCHASE;
$STR_DB_TABLE_NAME_PURCHASE = "t_store_purchase"; 
$STR_DB_TABLE_NAME_ORDER_BY_PURCHASE = " ORDER BY purchasedatetime DESC ";


# For Color
global $STR_DB_TABLE_NAME_COLOR;
global $STR_DB_TABLE_NAME_ORDER_BY_COLOR;
$STR_DB_TABLE_NAME_COLOR = "t_store_color"; 
$STR_DB_TABLE_NAME_ORDER_BY_COLOR = " ORDER BY title ASC ";

# For Promocode
global $STR_DB_TABLE_NAME_PROMOCODE;
global $STR_DB_TABLE_NAME_ORDER_BY_PROMOCODE;
$STR_DB_TABLE_NAME_PROMOCODE = "t_store_promocode"; 
$STR_DB_TABLE_NAME_ORDER_BY_PROMOCODE = " ORDER BY displayorder DESC, title ASC ";

#For Category
global $STR_DB_TABLE_NAME_CAT;
global $STR_DB_TABLE_NAME_ORDER_BY_CAT;
$STR_DB_TABLE_NAME_CAT = "t_store_cat";

#For cart
global $STR_DB_TABLE_NAME_SESSION_CART;
$STR_DB_TABLE_NAME_SESSION_CART = "t_store_sessioncart";

#For Sub Category
$STR_DB_TABLE_NAME_SUBCAT = "t_store_subcat"; 
$STR_DB_TABLE_NAME_ORDER_BY_SUBCAT = " ORDER BY subcattitle ASC";

#For Store Photo
global $STR_DB_TABLE_NAME_PHOTO;
global $STR_DB_TABLE_NAME_ORDER_BY_PHOTO;
$STR_DB_TABLE_NAME_PHOTO = "tr_store_photo"; 
$STR_DB_TABLE_NAME_ORDER_BY_PHOTO= " ORDER BY displayorder DESC";

#For User
global $STR_DB_TABLE_NAME_USER;
global $STR_DB_TABLE_NAME_ORDER_BY_USER;
$STR_DB_TABLE_NAME_USER = "t_user";

#For Member
global $STR_DB_TABLE_NAME_MEMBER;
global $STR_DB_TABLE_NAME_ORDER_BY_MEMBER;
$STR_DB_TABLE_NAME_MEMBER = "t_freemember";

global $STR_DB_TABLE_NAME_STORE_VIEW;
$STR_DB_TABLE_NAME_STORE_VIEW = "t_store_item_view";

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "cms_store";

# XML Path
global $XML_FILE_PATH;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/store/";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/store_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";

$INT_BAR_CHART_WIDTH = 100;
$VIEW_BAR_CHART_COLOR = "#A8DBFF";	

$STR_TITLE_DROPDOWN_USER = "-- ALL USERS --";
$STR_TITLE_DROPDOWN_CAT = "-- ALL CATEGORY --";
$STR_TITLE_DROPDOWN_ALPHABET = "-- ALL ALPHABET --";

#Error / Success Messages
$STR_MSG_ACTION_DISCOUNT_POSITIVE = "Error... You can only enter positive numeric value for Discount Percentage and Discount Amount.";
$STR_MSG_ACTION_DISCOUNT_BLANK = "Error... Both Discount Percentage and Discount Amount can not be blank. Please enter any one.";

# Common Messages / Notes / Help displaying on Form
$STR_MSG_COLOR = "Separate multiple colors with pipe | (pipe) e.g. White | Black";
$STR_MSG_SIZE = "Separate multiple sizes with pipe | (pipe) e.g. S | M | L | XL";
$STR_MSG_TYPE = "Separate multiple types with pipe | (pipe) e.g. Type1 | Type2";
$STR_MSG_DOMESTIC_SHIPPING = "Enter 0 (zero) for no shipping charge.";
$STR_MSG_INTERNATIONAL_SHIPPING = "Enter 0 (zero) for no shipping charge.";
$STR_DISPLAY_AS_PURCHASE = "If set YES, item purchase link will be dipaly at user side.";	

$STR_CBO_OPTION1 = "PROCESSING";
$STR_CBO_OPTION2 = "SHIPPED";
$STR_CBO_OPTION3 = "DELIVERED";

$STR_BUTTON_VIEW_DETAILS = "View This Product";
$STR_HOVER_VIEW_DETAILS = "Click To View This Product";

?>
