<?php
/*
	Module Name	:-  ccbill member webhooks
	File Name	:- signup_webhooks_p.php
	Create Date	:- 01-NOV-2018
	Initially Created by :- 015
*/
#------------------------------------------------------------------------------------------
#	include files
//	session_start();
	include "./includes/validatesession.php";
	include "./includes/configuration.php";
	include "./includes/lib_data_access.php";
	include "./includes/lib_common.php";
	include "./includes/lib_email.php";
	include "./includes/lib_xml.php";
//	include "./includes/http_to_https.php";		
#------------------------------------------------------------------------------------------
##### IMPORTANT NOTE #####
# CCBill sends POST data after successful transaction
# Now, if any data has single quote in string then
# this transaction data are not stored into website database 
# due to syntax error of single quote. This results into missing
# transaction for website. To resolve this issue, we are using 
# str_replace function of PHP where we replace single quote with blank 
##########################
	//print_r($_POST); exit;
	//$item_sessionid=$_SESSION["sessionid"];
	//$item_uniqueid=$_SESSION["uniqueid"];

	$str_initial_subscription_id="3434343434676767";
	if(isset($_POST["subscriptionId"]) && trim($_POST["subscriptionId"])!="")
	{
		$str_initial_subscription_id=RemoveQuote(trim($_POST["subscriptionId"]));
	}
        $str_cancelleddate="";
	if(isset($_POST["timestamp"]) && trim($_POST["timestamp"])!="")
	{
		$str_cancelleddate=RemoveQuote(trim($_POST["timestamp"]));
	}
	$str_reason="";
	if(isset($_POST["reason"]) && trim($_POST["reason"])!="")
	{
		$str_reason=RemoveQuote(trim($_POST["reason"]));
	}
	
	//$str_purchase_date=date("Y-m-d H:i:s");
	/*$str_purchase_date=$str_recurringdate;
	$expire_date=date("Y-m-d H:i:s",mktime(23, 59, 59, date("m")  , date("d")+$INT_PRODUCT_DOWNLOAD_LIMIT_DAY, date("Y")));
	$str_purchase_day="";
	$str_purchase_month="";
	$str_purchase_year="";
	*/
	/// find the day, month and year from the transaction date
	//$str_purchase_day=date("d",strtotime($str_purchase_date));
	//$str_purchase_month=date("m",strtotime($str_purchase_date));
	//$str_purchase_year=date("Y",strtotime($str_purchase_date));
	
	//$str_download="YES";
	//$str_addedby="YES";	
	
	/*#Query to select very first time purchase record from 't_product_purchase' table with subscriptionid but not with initialsubscriptionid...
	$sel_qry="";
	$sel_qry="SELECT * FROM t_ccbill_member WHERE loginid='".$str_initial_subscription_id."' AND password=' '";
	//print "SELECT: ".$sel_qry;
	$rs_list=GetRecordSet($sel_qry);
	
	*/
        $str_query_update="UPDATE t_ccbill_member SET ";
	$str_query_update .= "allowlogin='NO', ";
	$str_query_update .= "isccbillmember='NO', ";
	$str_query_update .= "reason='" . ReplaceQuote($str_reason) . "' ";
	$str_query_update .= "WHERE subscriptionid='". $str_initial_subscription_id."'";
        //print "<br/><br/>INSERT: ".$str_query_update.""; //exit;
	ExecuteQuery($str_query_update);
	
	# First check this member in this table
	$str_query_select="";
	$str_query_select="SELECT loginid, subscriptionid FROM t_freemember WHERE subscriptionid =".$str_initial_subscription_id;
	$rs_list = GetRecordSet($str_query_select);
	
	$str_username = "";

	# If member NOT found in this table, then look for another table	
	if($rs_list->eof())
	{
		$str_query_select="";
		$str_query_select="SELECT modelloginid, subscriptionid FROM t_model WHERE subscriptionid =".$str_initial_subscription_id;
		$rs_list = GetRecordSet($str_query_select);
		if(!$rs_list->eof())
		{

			$str_username = $rs_list->fields("modelloginid");

			# If member found in this table, then update this member
			$str_query_update="UPDATE t_model SET ";
			$str_query_update .= "allowlogin='YES', ";
			$str_query_update .= "isccbillmember='NO' ";
			$str_query_update .= "WHERE subscriptionid='". $str_initial_subscription_id."'";
			//print "<br/><br/>INSERT: ".$str_query_update.""; //exit;
			ExecuteQuery($str_query_update);
		}
		else
		{
			CloseConnection();
			redirect("./"); 
		    	exit();
		}	
	}
        else if(!$rs_list->eof())
	{
		$str_username = $rs_list->fields("loginid");

		# If member found in this table, then update this member
		$str_query_update="UPDATE t_freemember SET ";
		$str_query_update .= "allowlogin='NO', ";
		$str_query_update .= "isccbillmember='NO' ";
		$str_query_update .= "WHERE subscriptionid='". $str_initial_subscription_id."'";
	        //print "<br/><br/>INSERT: ".$str_query_update.""; //exit;
		ExecuteQuery($str_query_update);	
	}
	else
	{
		CloseConnection();
		redirect("./"); 
	    	exit();
	}

	
	$mailbody = "";
	$str_subject = "";
	//if($rs_list->fields("paymentmode") == "PAYMENTPLAN") { 
	$str_subject="Member has cancelled his membership on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
	$mailbody="Member has cancelled his membership. Below are details.<br/><br/>";
        $mailbody.="<strong>Cancelled On:</strong> ".$str_cancelleddate."<br/>";
	$mailbody.="<strong>Subscription ID:</strong> ".$str_initial_subscription_id."<br/>";
        $mailbody.="<strong>Login ID:</strong> ".$str_username."<br/>";
        $mailbody.="<strong>Reason:</strong> ".$str_reason."<br/>";
	$mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL."";
        
        //print "<br/><br/>".$str_subject."<br/><br/><br/>";
       	//print $mailbody;exit;
        
	$fp=openXMLfile($UPLOAD_XML_CONTENT_PATH."siteconfiguration.xml");
	$STR_SITE_URL=getTagValue($STR_WEBSITE_NAME,$fp);	
	$str_from=getTagValue($STR_FROM_DEFAULT,$fp);
	$str_to=getTagValue($STR_FROM_DEFAULT,$fp);
	closeXMLfile($fp);
	
	#For Testing:
	//sendmail("kra.rd.test@gmail.com",$str_subject,$mailbody,"kra.rd.test@gmail.com",1); // Sent to Customer / User
	//sendmail("kra.rd.test@gmail.com",$str_subject,$mailbody,"kra.rd.test@gmail.com",1); // Sent to Admin
	
	#For Real:
	//sendmail($str_to,$str_subject,$mailbody,$str_from,1); // Sent to Customer / User
	sendmail($str_to,$str_subject,$mailbody,$str_from,1); // Sent to Admin
	
?>
