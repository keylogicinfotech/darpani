<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_image.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_TESTIMONIAL";
$str_img_path = $STR_SITENAME_WITH_PROTOCOL."/mdm/testimonial/";
$str_db_table_name = "t_page_metatag";
$str_xml_file_name = "testimonial.xml";
$str_xml_file_name_cms = "testimonial_cms.xml";
$int_records_per_page = 1;
#----------------------------------------------------------------------
#read main xml file
$str_xml_list = "";
$str_xml_list = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");

$int_total_records = 0;
$int_total_records = (count(array_keys($str_xml_list)));	
//print $int_total_records;
#----------------------------------------------------------------------
#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
            </div>
        </div>
        <?php if($str_visible_cms == "YES") { ?>
            <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <div class="row padding-10">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <span>
                                            <a class="accordion-toggle collapsed" title="<?php print $STR_HOVER_ADD; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>
                                        </span>
                                        
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="panel-body">
                                        <form id="frm_add" name="frm_add" novalidate>
                                            <div class="row padding-10">
                                                <div class="col-md-4  col-lg-4">
                                                    <div class="form-group control-group">
                                                        <div class="controls">
                                                            <label>Name</label><span class="text-help-form">&nbsp;*</span>
                                                            <input type="text" id="txt_name" name="txt_name" class="form-control" required data-validation-required-message="<?php print $STR_PLACEHOLDER_NAME; ?>" placeholder="<?php print $STR_PLACEHOLDER_NAME; ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4  col-lg-4">
                                                    <div class="form-group control-group">
                                                        <div class="controls">
                                                            <label>Location</label><span class="text-help-form">&nbsp;</span>
                                                            <input type="text" id="txt_location" name="txt_location" class="form-control" placeholder="<?php print $STR_PLACEHOLDER_TEXT; ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-lg-4">
                                                    <div class="form-group control-group">
                                                        <label>Email ID</label><span class="text-help-form">&nbsp;</span>
                                                        <div class="controls">
                                                            <input type="email" id="txt_emailid" name="txt_emailid" class="form-control" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row padding-10">
                                                <div class="col-md-12  col-lg-12">
                                                    <div class="form-group control-group">
                                                        <label>Message</label><span class="text-help-form">&nbsp;*</span>
                                                        <div class="controls">
                                                            <textarea id="ta_desc" name="ta_desc" class="form-control" rows="2" required data-validation-required-message="<?php print $STR_PLACEHOLDER_MESSAGE; ?>" placeholder="<?php print $STR_PLACEHOLDER_MESSAGE; ?>"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row padding-10">
                                                <div class="col-md-12  col-lg-12">
                                                    <div class="control-group form-group">
                                                        <label>Secret Code</label><span class="text-help-form">&nbsp;*</span>
                                                        <div class="controls input-group">
                                                            <input type="text" class="form-control" id="txt_secretcode" name="txt_secretcode" required data-validation-required-message="<?php print $STR_PLACEHOLDER_SECRET_CODE; ?>" placeholder="<?php print $STR_PLACEHOLDER_SECRET_CODE; ?>" minlength="7" maxlength="7">
                                                            <span class="input-group-addon input-group-addon-no-padding nopadding">
                                                                <img src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/includes/get_unique_image.php" border="0" alt="Captcha" width="130" height="29">
                                                            </span>
                                                        </div>
                                                        <p class="help-block"></p>                            
                                                    </div>
                                                </div>
                                            </div>
                                            <?php //print_r($_GET);?>
                                            <div id="success"></div>
                                            <input type="hidden" id="hdn_blogpkid" name="hdn_blogpkid" value="<?php print $int_pkid; ?>" />
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i>&nbsp;Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php 
        $arr_test = array_keys($str_xml_list);
        $int_cnt=0;
        if(trim($arr_test[0])!="ROOT_ITEM")
        { ?>
        
            <?php
            $int_cnt = 0;
            //print_r($str_xml_list);
            foreach($str_xml_list as $key => $val)         
            {?>
                <div class="row padding-10">
                    <?php if(isset($str_xml_list[$key]["IMAGENAME"]) != "") { ?>
                    <div class="col-lg-4 col-md-4">
                        <img src="<?php print $str_img_path.$str_xml_list[$key]["IMAGENAME"]; ?>" class="img-responsive" alt="<?php //print $rs_list->Fields("title"); ?>" title="<?php //print $rs_list->Fields("title"); ?>" />
                    </div>
                    <div class="col-lg-8 col-md-8">
                        <?php if($str_xml_list[$key]["DESCRIPTION"] != "") { ?>
                        <p align="justify"><?php print $str_xml_list[$key]["DESCRIPTION"]; ?></p>
                        <?php } ?>
                        <p align="justify" class="text-primary">
                            <?php if($str_xml_list[$key]["PERSONNAME"] != "") { ?>
                                <i class="fa fa-user"></i>&nbsp;<?php print $str_xml_list[$key]["PERSONNAME"]; ?>
                            <?php } ?>
                            <?php if($str_xml_list[$key]["LOCATION"] != "") { ?>
                                &nbsp;&nbsp;<i class="fa fa-map-marker"></i>&nbsp;<?php print $str_xml_list[$key]["LOCATION"]; ?>
                            <?php } ?>
                            <?php if($str_xml_list[$key]["CREATEDATETIME"] != "") { ?>
                                &nbsp;&nbsp;<i class="fa fa-calendar"></i>&nbsp;<?php print DDMMMYYYYHHIISSFormat($str_xml_list[$key]["CREATEDATETIME"]); ?>
                            <?php } ?>
                            <?php if($str_xml_list[$key]["NEW"] == "YES") { ?>
                                &nbsp;&nbsp;<?php print $STR_ICON_PATH_NEW; ?>
                            <?php } ?>    
                        </p><br/><br/>
                    </div>
                    <?php } else { ?>
                    <div class="col-lg-12 col-md-12">
                        <?php if(isset($str_xml_list[$key]["DESCRIPTION"]) != "") { ?>
                        <p align="justify"><?php print $str_xml_list[$key]["DESCRIPTION"]; ?></p>
                        <?php } ?>
                        <p align="justify" class="text-primary">
                            <?php if(isset($str_xml_list[$key]["PERSONNAME"]) != "") { ?>
                                <i class="fa fa-user"></i>&nbsp;<?php print $str_xml_list[$key]["PERSONNAME"]; ?>
                            <?php } ?>
                            <?php if(isset($str_xml_list[$key]["LOCATION"]) != "") { ?>
                                &nbsp;&nbsp;<i class="fa fa-map-marker"></i>&nbsp;<?php print $str_xml_list[$key]["LOCATION"]; ?>
                            <?php } ?>
                            <?php if(isset($str_xml_list[$key]["CREATEDATETIME"]) != "") { ?>
                                &nbsp;&nbsp;<i class="fa fa-calendar"></i>&nbsp;<?php print DDMMMYYYYFormat($str_xml_list[$key]["CREATEDATETIME"]); ?>
                            <?php } ?>
                                <?php if(isset($str_xml_list[$key]["NEW"]) == "YES") { ?>
                                &nbsp;&nbsp;<?php print $STR_ICON_PATH_NEW; ?>
                            <?php } ?>    
                        </p>
                    </div>
                    <?php } ?>
                </div><hr/>
        <?php 
            }?>
        
        <?php
        }?>
        <?php include($STR_USER_BANNER_PATH); ?>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/testimonial.js"></script>
</body>
</html>
