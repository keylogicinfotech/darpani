<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_MEMBERVIP";
$str_img_path = "";
$str_db_table_name = "t_page_metatag";
$str_xml_file_name = "";
$str_xml_file_name_cms = "member_cms.xml";
$int_records_per_page = 0;
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
$str_desc = "";
$str_desc2 = "";
$str_desc3 = "";
$str_desc4 = "";
$str_desc5 = "";
$str_desc6 = "";
$str_desc7 = "";
$str_desc8 = "";

$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_desc2 = getTagValue("ITEMKEYVALUE_DESCRIPTION2", $fp);;
$str_desc3 = getTagValue("ITEMKEYVALUE_DESCRIPTION3", $fp);;
$str_desc4 = getTagValue("ITEMKEYVALUE_DESCRIPTION4", $fp);;
$str_desc5 = getTagValue("ITEMKEYVALUE_DESCRIPTION5", $fp);;
$str_desc6 = getTagValue("ITEMKEYVALUE_DESCRIPTION6", $fp);;
$str_desc7 = getTagValue("ITEMKEYVALUE_DESCRIPTION7", $fp);;
$str_desc8 = getTagValue("ITEMKEYVALUE_DESCRIPTION8", $fp);;

$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
$str_title_page = "Member Home"
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag("PG_MEMBERVIP")); ?>
    <link href="./../css/bootstrap.min.css" rel="stylesheet">
    <link href="./../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="./../css/user.css" rel="stylesheet">    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1>
                <hr/>
            </div>
        </div>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h4><a href="./vipmember_logout_p.php" />Log Out</a></h4>
            </div>
        </div>        
    </div>
    <script language="JavaScript" src="./js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="./../js/bootstrap.min.js"></script>
</body>
</html>
