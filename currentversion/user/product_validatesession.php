<?php
session_start();
if (!(isset($_SESSION['sessionid'])))
{
    $_SESSION['sessionid'] = session_id();
    setcookie("sessionid", session_id(), time() + (10 * 365 * 24 * 60 * 60)); // Cookie set for next 10 years
}
if (!(isset($_SESSION['uniqueid'])))
{
    $arr=getdate();
    $uid= $arr['year'] . substr("0" . $arr['mon'],-2) . substr("0" . $arr['mday'],-2) . substr("0" . $arr['hours'],-2) . substr("0" . $arr['minutes'],-2) . substr("0" . $arr['seconds'],-2) ;
    $_SESSION['uniqueid'] = $uid;
    setcookie("uniqueid", $uid, time() + (10 * 365 * 24 * 60 * 60)); // Cookie set for next 10 years
}
?>
