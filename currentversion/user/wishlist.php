<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#-------------------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/http_to_https.php";	
include "./../includes/lib_image.php";
include "./../includes/lib_xml.php";

#-------------------------------------------------------------------------------
$str_title_page_metatag = "PG_WISH";
$str_db_table_name_metatag = "t_page_metatag";

$str_page_title = "Wishlist";
$str_img_path = "./mdm/wishlist/";
$str_xml_file_name = "wishlist.xml";
$str_xml_file_name_cms = "wishlist_cms.xml";


#getting datas from module xmlfile.
$arr_xml_list="";
$arr_xml_list=readXml($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");

#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
# Get metatag page title
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatag. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = "";
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>

<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
         <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_page_title; ?></h1>
                <hr/>
            </div>
        </div>  
        <div class="row padding-10">
            <div class="col-md-12">
                <?php if($str_desc_cms != "" && $str_desc_cms != "<br>" && $str_visible_cms == "YES") { ?>
                    <div  class="breadcrumb"><p align="justify" class="nopadding"><?php print($str_desc_cms);?></p></div>
                <?php } ?>
            </div>
        </div>
        <?php  $arr_test=array_keys($arr_xml_list);
//            print_r($arr_xml_list);
      
             
              $int_cnt = 0; ?>
                <?php
                if($arr_test[0]!= "ROOT_ITEM")
                { ?>
                <?php 
                while(list($key,$val) = each($arr_xml_list)) 
                {
                    if(is_array($val))
                    { ?>
            
                    <?php if(($arr_xml_list[$key]["IMAGENAME"]!="")) { ?>
                 <div class="row padding-10">
                    <div class="col-md-3 col-lg-3" align="center"> 
                        <img class="img-responsive <?php /*?>img-hover<?php */?>" src="<?php print($str_img_path.$arr_xml_list[$key]["IMAGENAME"]); ?>" border="0" alt="<?php print $str_page_title;?> Image" title="<?php print $str_page_title;?> Image" align="middle"/>
                        
                    </div>  
                    <div class="col-lg-9 col-md-9">
                        <label><h3 align="left" class="nopadding"><?php print(MyHtmlEncode($arr_xml_list[$key]["TITLE"])) ?> </h3></label>&nbsp;&nbsp;<?php if($arr_xml_list[$key]["NEW"] == "YES") { print $STR_ICON_PATH_NEW; } ?>
                        
                        <?php if($arr_xml_list[$key]["URL"] != "") {?>
                            <p align="left" class="nopadding"><a href="" title=""><i class="fa fa-globe"></i>&nbsp;<?php print(DisplayWebsiteUrl(trim($arr_xml_list[$key]["URL"]),trim(MyHtmlEncode($arr_xml_list[$key]["URLTITLE"])),"YES","",$STR_HOVER_VIEW_URL,"",35)); ?></a></p>
                            <?php } ?>
                        <p align="justify" class="nopadding"><?php print(($arr_xml_list[$key]["DESCRIPTION"])) ?></p>
                    </div>
                 </div><hr/>
                <?php } else if(($arr_xml_list[$key]["IMAGENAME"]=="")) { ?>
               
                <!--<div class="row nopadding">-->
                <!--<div class="col-lg-12 col-md-12">-->
                 <div class="row padding-10">
                    <div class="col-lg-12 col-md-12">
                        <h4 align="left" class="nopadding"><?php print(MyHtmlEncode($arr_xml_list[$key]["TITLE"])) ?> <?php if($arr_xml_list[$key]["NEW"] == "YES") { print $STR_ICON_PATH_NEW; } ?></h4>
                        
                        <?php if($arr_xml_list[$key]["URL"] != "") {?>
                            <p align="left"><a href="" title=""><i class="fa fa-globe"></i>&nbsp;<?php print(DisplayWebsiteUrl(trim($arr_xml_list[$key]["URL"]),trim(MyHtmlEncode($arr_xml_list[$key]["URLTITLE"])),"YES","",$STR_HOVER_VIEW_URL,"",35)); ?></a></p>
                            <?php } ?>
                            <?php if($arr_xml_list[$key]["DESCRIPTION"] != "") {?>
                            <p align="justify" class="nopadding"><?php print(($arr_xml_list[$key]["DESCRIPTION"])) ?></p>
                            <?php } ?>
                       
                    </div>
                 </div><hr/>
                <?php } else{ ?>
                  <!--<div class="col-lg-12 col-md-12">-->
                  <div class="row padding-10">
                    <div class="col-lg-9 col-md-9">
                        <p class="nopadding"><?php print(($arr_xml_list[$key]["DESCRIPTION"])) ?></p>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <p href="#" data-toggle="modal" data-target=".f-pop-up-<?php print(MyHtmlEncode($arr_xml_list[$key]['PKID'])); ?>" rel="thumbnail"><img class="img-responsive <?php /*?>img-hover<?php */?> thumbnail thumbnail-margin" src="<?php print($str_img_path.$arr_xml_list["IMAGENAME"]); ?>" border="0" alt="<?php print $str_page_title;?> Image" title="<?php print $str_page_title;?> Image" align="middle"/></p>
                    </div> 
                  </div><hr/>
                  <?php  } 
                } }  }?>
   
   
    </div>
       
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    
</body>
    <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
    </script>
<div class="scrollup" style="display: block;"></div>
<script type="text/javascript">
            $(document).ready(function(){ 

            $(window).scroll(function(){
                if ($(this).scrollTop() > 100) {
                    $('.scrollup').fadeIn();
                } else {
                    $('.scrollup').fadeOut();
                }
            }); 
            $('.scrollup').click(function(){
                $("html, body").animate({ scrollTop: 0 }, 600);
                return false;
            });
        });
    </script>
    
</html>
