/*
  Jquery Validation using jqBootstrapValidation
   example is taken from jqBootstrapValidation docs 
  */
$(function () {
  $(
    "#frm_edit input, #frm_edit textarea, #frm_edit select"
  ).jqBootstrapValidation({
    preventSubmit: true,

    submitSuccess: function ($form, event) {
      event.preventDefault(); // prevent default submit behaviour

      var formData = new FormData();
      //		alert("flag");
      //		formData.append("txt_shortenurlkey",$("input#txt_shortenurlkey").val());

      //formData.append("txt_shortenurlkey",$("input#txt_shortenurlkey").val());
      formData.append("txt_loginid", $("input#txt_loginid").val());
      formData.append("txt_name", $("input#txt_name").val());
      formData.append("txt_email", $("input#txt_email").val());
      formData.append("cbo_country", $("select#cbo_country").val());
      formData.append("cbo_state", $("select#cbo_state").val());
      formData.append("txt_businessname", $("input#txt_businessname").val());
      formData.append("txt_city", $("input#txt_city").val());
      formData.append("txt_zipcode", $("input#txt_zipcode").val());
      formData.append("ta_add", $("input#ta_add").val());
      formData.append("txt_phoneno", $("input#txt_phoneno").val());
      formData.append("txt_mobno", $("input#txt_mobno").val());

      formData.append("ofc_fullname", $("input#ofc_fullname").val());
      formData.append("ofc_email", $("input#ofc_email").val());
      formData.append("ofc_country", $("select#ofc_country").val());
      formData.append("ofc_state", $("select#ofc_state").val());
      formData.append("ofc_businessname", $("input#ofc_businessname").val());
      formData.append("ofc_city", $("input#ofc_city").val());
      formData.append("ofc_zipcode", $("input#ofc_zipcode").val());
      formData.append("ofc_add", $("input#ofc_add").val());
      formData.append("ofc_phoneno", $("input#ofc_phoneno").val());
      formData.append("ofc_mobno", $("input#ofc_mobno").val());
      /*formData.append("txturl2",$("input#txturl2").val());
                formData.append("txturl4",$("input#txturl4").val());
                formData.append("txturl3",$("input#txturl3").val()); */
      //                formData.append("ta_desc",$("textarea#ta_desc").val());
      //formData.append("ta_keyword_list",$("textarea#ta_keyword_list").val());
      //formData.append("check_photo_uploaded",$("input#check_photo_uploaded").val());
      //formData.append("check_photo_uploaded2",$("input#check_photo_uploaded2").val());
      //formData.append("check_photo_uploaded3",$("input#check_photo_uploaded3").val());
      formData.append("hdn_pkid", $("input#hdn_pkid").val()); // number 123456 is immediately converted to a string "123456"
      //formData.append('file_photo', $('input[type=file]')[0].files[0]);
      //formData.append('file_image', $('input[id=file_image]')[0].files[0]);

      //alert(arr_contact_method);

      //formData.append('file_photo3', $('input[id=file_photo3]')[0].files[0]);
      $.ajax({
        url: "./user_personal_details_p.php",
        type: "POST",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        //dataType: "json",
        success: function (data) {
          // alert(data);
          var $responseText = JSON.parse(data);
          if ($responseText.status == "SUC") {
            ////Below given code display messsage on same page after edit personal details
            // Success message
            $("#success").html("<div class='alert alert-success'>");
            $("#success> .alert-success")
              .html(
                "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;"
              )
              .append("</button>");
            $("#success > .alert-success").append(
              "<strong> " + $responseText.message + " </strong>"
            );
            $("#success > .alert-success").append("</div>");

            //setTimeout(function(){ window.location.href="./mdl_home.php?&#ptop"; }, 2000);
            setTimeout(function () {
              location.reload();
            }, 2000);

            ////Below given code redirect to dashboard page after edit personal details
          } else if ($responseText.status == "ERR") {
            // Fail message
            $("#success").html("<div class='alert alert-danger'>");
            $("#success > .alert-danger")
              .html(
                "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;"
              )
              .append("</button>");
            $("#success > .alert-danger").append(
              "<strong> " + $responseText.message + " "
            );
            $("#success > .alert-danger").append("</div>");
          }
        },
      });
    },
    filter: function () {
      return $(this).is(":visible");
    },
  });
  $('a[data-toggle="tab"]').click(function (e) {
    e.preventDefault();
    $(this).tab("show");
  });
});

/*When clicking on Full hide fail/success boxes */
$("#txt_shortenurlkey").focus(function () {
  $("#success").html("");
});
// JavaScript Document

function Confirm_Deletion() {
  if (confirm("Are you sure you want to delete this image permanently.")) {
    if (
      confirm(
        "Confirm Deletion:Click 'OK' to delete this image or 'Cancel' to deletion."
      )
    ) {
      return true;
    }
  }
  return false;
}
