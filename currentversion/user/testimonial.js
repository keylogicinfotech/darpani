/*
  Jquery Validation using jqBootstrapValidation
   example is taken from jqBootstrapValidation docs 
  */
$(function() {

    $("#frm_add textarea, #frm_add input").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // something to have when submit produces an error ?
            // Not decided if I need it yet
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var txt_name = $("input#txt_name").val();
            var ta_desc = $("textarea#ta_desc").val();
            var hdn_blogpkid = $("input#hdn_blogpkid").val();
            var txt_location = $("input#txt_location").val();
            var txt_emailid = $("input#txt_emailid").val();
            var txt_secretcode = $("input#txt_secretcode").val();
            
            $.ajax({
                url: "./user/testimonial_p.php",
                type: "POST",
                data: {
                    txt_name: txt_name,
                    ta_desc: ta_desc,
                    hdn_blogpkid: hdn_blogpkid,
                    txt_location: txt_location,
                    txt_emailid: txt_emailid,
                    txt_secretcode: txt_secretcode
                },
                cache: false,
                
                success: function(data) 
                {
                        alert(data);
                        var $responseText=JSON.parse(data);
                        if($responseText.status == 'SUC')
                        {
                            // Success message

                            $('#success').html("<div class='alert alert-success'>");
                            $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                            $('#success > .alert-success').append("<strong> " + $responseText.message + " </strong>");
                            $('#success > .alert-success').append('</div>');

                            setTimeout(function(){ location.reload() }, 1500);
                            //clear all fields
                            //$('#contactForm').trigger("reset");
                             //$('form')[0].reset();
                            //$('#contactForm').trigger("reset");
                            //$('#contactForm').trigger("reset");
                            //$("#resetFormNew").trigger("click");
                            
                            //$('#contactForm')[0].reset($responseText);
                            //$("#contactForm").reset($responseText);
                            //$('#contactForm').trigger("reset");
                                    //$('#contactForm').clearInputs();
                                    //$("#contactForm").reset();

                        }

                        else if($responseText.status == 'ERR')
                        {
                            // Fail message
                            $('#success').html("<div class='alert alert-danger'>");
                            $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                    .append("</button>");
                            $('#success > .alert-danger').append("<strong> " + $responseText.message + " ");
                            $('#success > .alert-danger').append('</div>');
                        }
                },
				
				/*success: function() {
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Your contact inquiry has been submitted, we will contact you Soon. Thank you for communication.</strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + " it seems that my mail server is not responding...</strong> Could you please email me directly to <a href='mailto:info@websitename.com?Subject=Message_Me from websitename.com;>info@websitename.com</a> ? Sorry for the inconvenience!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },*/
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
