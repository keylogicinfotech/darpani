<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/validate_session_user.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_xml.php";
include "./store_item_config.php";
include "./../includes/http_to_https.php";
//print_r($_SESSION);exit;
#----------------------------------------------------------------------
$str_title_page = "";
$str_title_page = $STR_TITLE_PAGE_ORDER_STATUS;

$int_userpkid = 0;
$int_userpkid = $_SESSION["userpkid"]; 
//print $int_userpkid;
#------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------
# get Query String Data
$str_status = "";
if(isset($_GET["cbo_status"])) { $str_status = trim($_GET["cbo_status"]); }
#------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT *";
$str_query_select .= " FROM t_store_purchase";
$str_query_select .= " WHERE userpkid= ".$int_userpkid;
//print $str_query_select;
//$str_query_select .= " ORDER BY displayorder DESC";
$rs_list_promocode = GetRecordSet($str_query_select);

$str_query_where = "";
if($str_status != "") { 
    $str_query_where = " AND promocodepkid = '".$str_status."'";
}
# Select Query
$str_query_select = "";
$str_query_select = "SELECT *";
$str_query_select .= " FROM t_store_promocode";
$str_query_select .= " WHERE visible='YES'";
$str_query_select .= " ORDER BY displayorder DESC";
//print $str_query_select;
$rs_list = GetRecordSet($str_query_select);//print $str_query_select."<br/><br/><br/><br/><br/><br/><br/>";
//print $rs_list->Count();exit;
#------------------------------------------------------------------------------------------------
# get Query String Data
$int_cat_pkid="";
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ $int_cat_pkid=trim($_GET["catid"]); }

$str_key = "";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_key = trim($_GET["key"]); }

# get data for paging
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page = 1; }


$str_filter = ""; 
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_key."&PagePosition=".$int_page;
#-----------------------------------------------------------------------------

#------------------------------------------------------------------------------------------------
$str_type = "";
$str_message = "";
# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
	case("E"): $str_type = "E"; break;
	case("W"): $str_type = "W"; break;
    }
}
#	Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_md; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;        
    }
} 
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($str_title_page) ;?></title>
    <?php //print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <?php include "./../includes/include_files_user.php"; ?>     
</head>
<body>
<?php include("../includes/header.php"); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-md-3 col-xs-12 col-sm-12">
                <?php include($STR_USER_PANEL_PATH); ?>
            </div>
            
            <div class="col-md-9 col-xs-12 col-sm-12">
                <div class="row padding-10">
                    <a name="ptop" id="ptop"></a>
                    <div class="col-md-12">
                        <h3 align="right"><?php print $str_title_page; ?></h3>         
                    </div>
                </div>
                <hr/>  
                <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); } ?>
         <?php /* ?>       <div class="row">
                    <div class="col-md-12" align="left">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <div class="row"><div class="col-md-12  padding-10"><div class="col-md-12 col-sm-12 col-xs-12"><strong><i class="fa fa-filter"></i>&nbsp;&nbsp;Filter Criteria</strong></div></div></div>
                                </h4>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12" align="center">
                                    <form name="frm_filter" method="GET" action="store_item_order_status_list.php#ptop">
                                        <label>Select  </label>
                                        <label>
                                            <select id="cbo_status" name="cbo_status" class="form-control input-sm">
                                                <option value=""> -- ALL -- </option>
                                                <option value="<?php print $STR_PROMO_OPTION1; ?>" <?php print(CheckSelected($STR_PROMO_OPTION1,strtoupper($str_status)));?>><?php print $STR_PROMO_OPTION1; ?></option>;
                                                <option value="<?php print $STR_PROMO_OPTION2; ?>" <?php print(CheckSelected($STR_PROMO_OPTION2,strtoupper($str_status)));?>><?php print $STR_PROMO_OPTION2; ?></option>
                                               
                                            </select>
                                        </label>
                                        <label>&nbsp;<?php print DisplayFormButton("VIEW",0); ?></label>
                                    </form>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>  <?php */ ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered ">
                        <thead>
                            <tr>
                                <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                                <!--<th width="10%"><?php // print $STR_TABLE_COLUMN_NAME_DATE; ?></th>-->
                                <!--<th width=""><?php // print $STR_TABLE_COLUMN_NAME_IMAGE; ?></th>-->
                                <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                                <th width="6%"><?php print $STR_TABLE_COLUMN_NAME_MINIMUM_PURCHASE_AMOUNT; ?></th>
                                <th width="14%"><?php print $STR_TABLE_COLUMN_NAME_DISCOUNT_AMOUNT; ?></th>
                               <th width="16%"><?php print $STR_TABLE_COLUMN_NAME_ORDER_STATUS; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if($rs_list->Count() <= 0)
                            { ?>
                                <tr>
                                    <td colspan="9" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE);?></td>
                                </tr>
                            <?php 
                            } else {
                                $int_cnt = 1;    
                                while(!$rs_list->EOF()) 
                                { ?>
                                <tr>
                                    <td align="center"><?php print $int_cnt; ?><input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>"></td>
                                  <?php /* ?>  <td align="center" class="text-help"><?php print(date("d-M-Y h:i:s", strtotime($rs_list->Fields("purchasedatetime")))); ?></td>
                                    <td class="align-top">
                                        <div class="row padding-10">
                                        <?php 
                                        if($rs_list->Fields("productpkid") != 0)
                                        {
                                            $str_query_select = "";
                                            $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME." WHERE pkid = ".$rs_list->Fields("productpkid");
//                                            print $str_query_select;
                                            $rs_list_brag = GetRecordSet($str_query_select);
                                            
                                            if($rs_list_brag->Fields("imagefilename2") != "")
                                            { ?>
                                            <div class="col-md-6">
                                                <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list->fields("purchasepkid")); ?>" rel="thumbnail"><img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("productpkid")."/".$rs_list_brag->Fields("imagefilename2"); ?>" class="img-responsive" title="<?php print $rs_list_brag->Fields("title"); ?>" alt="<?php print $rs_list_brag->Fields("title"); ?>" /></a>
                                                <div class="modal fade f-pop-up-<?php print($rs_list->fields("purchasepkid")); ?> " align="center" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                                <img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("productpkid")."/".$rs_list_brag->Fields("imagefilename2"); ?>" class="img-responsive" title="<?php print $rs_list_brag->Fields("title"); ?>" alt="<?php print $rs_list_brag->Fields("title"); ?>" />
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div>
                                            </div>
                                        <?php }?>
                                        <?php 
                                        if($rs_list_brag->Fields("imagefilename3") != "")
                                        { ?> 
                                            <div class="col-md-6">
                                                <a href="#" data-toggle="modal" data-target=".f-pop-up-back-<?php print($rs_list->fields("purchasepkid")); ?>" rel="thumbnail"><img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("productpkid")."/".$rs_list_brag->Fields("imagefilename3"); ?>" class="img-responsive" title="<?php print $rs_list_brag->Fields("title"); ?>" alt="<?php print $rs_list_brag->Fields("title"); ?>" /></a>
                                                <div class="modal fade f-pop-up-back-<?php print($rs_list->fields("purchasepkid")); ?> " align="center" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                                <img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("productpkid")."/".$rs_list_brag->Fields("imagefilename3"); ?>" class="img-responsive" title="<?php print $rs_list_brag->Fields("title"); ?>" alt="<?php print $rs_list_brag->Fields("title"); ?>" />
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div>
                                            </div>
                                        <?php } ?>
                                        </div>
                                    <?php 
                                        }
                                        ?>
                                    </td> <?php */ ?>
                                   <td class="align-middle"><h4 class="nopadding"><b><?php print $rs_list->fields("title");?></b></h4>
                                    <p><?php print $rs_list->fields("description");?></p></td>
                                    <td align="right" class=""><?php print $rs_list->fields("minimumpurchaserequired");?></td>
                                    <td align="center">
                                        <?php 
                                        if($rs_list->fields("discountpercentage") != "" && $rs_list->fields("discountpercentage") != 0)
                                            { print(" ".MyHtmlEncode($rs_list->fields("discountpercentage"))."&nbsp;%"); } ?>
                                        <?php 
                                        if($rs_list->fields("discountamount") != "" && $rs_list->fields("discountamount") != 0)
                                        { print("$&nbsp;".MyHtmlEncode($rs_list->fields("discountamount")).""); }	
                                        ?>				
                                    </td>
                    
                                    
                                   <td align="center">
                                        <?php 
                                        
                                        $str_query_select = "";
                                        $str_query_select = "SELECT a.*, b.userpkid";
                                        $str_query_select .= " FROM t_store_promocode a LEFT  JOIN  t_store_purchase b ON a.pkid=b.promocodepkid ";
                                        $str_query_select .= " WHERE visible='YES' ";
                                        $str_query_select .= " ORDER BY displayorder DESC";
//                                        print $str_query_select;
                                        $rs_list_status = GetRecordSet($str_query_select);
                                        $str_status_class = "";
                                        $str_img = "";
                                        $int_purchase_promocode = $rs_list_promocode->Fields("userpkid");
                                        $int_purchase = $rs_list->Fields("pkid");
//                                        print $int_purchase_promocode;
//                                        print $int_userpkid;
//                                        $int_promocode = $int_userpkid;
                                        if($int_purchase_promocode == $int_purchase) 
                                        {
                                            
                                           $str_img = "<i class='fa fa-cog'></i>&nbsp;";
                                            $str_status_class = "alert-danger";
                                            $str_status = "This Promocode Was Used";
                                            
                                            
                                        } 
                                        else
                                        {
                                            $str_img = "";
                                            $str_status_class = "";
                                            $str_status = "";
                                          /*  $str_img = "<i class='fa fa-check'></i>&nbsp;";
                                            $str_status_class = "alert-success";
                                            $str_status = "Active"; */
                                        }
                                        ?>
                                        <span class="<?php print $str_status_class; ?>"><?php print $str_img; ?><?php print $str_status; ?></span>
                                    </td>
                                </tr>
                                <?php 
                                $int_cnt++;
                                $rs_list->MoveNext();
                                }?>
                                
                    <?php  } ?>
                        </tbody>
                    </table>
                </div>                
            </div>        
        </div>
    </div>
    <?php include("../includes/footer.php");CloseConnection();?>
    <script language="JavaScript" src="./store_item_list.js"></script>    
    <script language="JavaScript" src="./store_item_add.js"></script>    
</body>
</html>
