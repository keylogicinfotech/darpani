<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_PHOTO";
$str_img_path = "./mdm/photoalbum/";
$str_db_table_name = "t_page_metatag";
$str_xml_file_name = "photoalbum.xml";
$str_xml_file_name_cat = "video_cat.xml";
$str_xml_file_name_cms = "photoalbum_cms.xml";
$int_records_per_page = 1;
//print_r($_POST);
#----------------------------------------------------------------------
#read main xml file
//$str_xml_list_cat = "";
//$str_xml_list_cat = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name_cat,"ROOT_ITEM_CAT");

$str_xml_list = "";
$str_xml_list = readXML($STR_XML_FILE_PATH_MODULE."photoalbum/".$str_xml_file_name,"ROOT_ITEM");

$int_total_records = 0;
$int_total_records = (count(array_keys($str_xml_list)));	
//print $int_total_records;
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_cat_pkid = "";
if(isset($_POST["catid"]) && trim($_POST["catid"]) != "" )
{   
    $int_cat_pkid = trim($_POST["catid"]);    
}
else if(isset($_SESSION["catid"]) && trim($_SESSION["catid"]) != "" )
{   
    $int_cat_pkid = trim($_SESSION["catid"]);    
}

$str_name_key = "";
if(isset($_POST["key"]) && trim($_POST["key"]) != "" )
{ $str_name_key = trim($_POST["key"]); }
else if(isset($_SESSION["key"]) && trim($_SESSION["key"]) != "" )
{ $str_name_key = trim($_SESSION["key"]); }


# get data for paging
$int_page = "";
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"]) != "" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"]) > 0)
{ $int_page = $_POST["PagePosition"]; }
else if(isset($_SESSION["PagePosition"]) && trim($_SESSION["PagePosition"]) != "" && is_numeric($_SESSION["PagePosition"]) && trim($_SESSION["PagePosition"]) > 0)
{ $int_page = $_SESSION["PagePosition"]; }
else
{ $int_page = 1; }
//print_r($_POST);
$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";

$_SESSION["catid"] = $int_cat_pkid;
$_SESSION["key"] = $str_name_key;
$_SESSION["PagePosition"] = $int_page;

//print_r($_SESSION);

/*if($int_total_records > 0)
{
    $int_total_pages = ceil($int_total_records / $int_records_per_page); 
    //print $int_total_pages;exit;
    if ($int_page > $int_total_pages)
    {
        //$int_page = $int_total_pages;
        $_SESSION["catid"] = "";
        $_SESSION["key"] = "";
        $_SESSION["PagePosition"] = 1;
    }
    
}*/

if($int_total_records > 0)
{
    $int_total_pages = ceil($int_total_records / $int_records_per_page); 
    //print $int_total_pages;exit;
    if ($int_page > $int_total_pages)
    {
        //$int_page = $int_total_pages;
        $_SESSION["catid"] = "";
        $_SESSION["key"] = "";
        $_SESSION["PagePosition"] = 1;
    }
    else
    {
        $_SESSION["catid"] = $int_cat_pkid;
        $_SESSION["key"] = $str_name_key;
        $_SESSION["PagePosition"] = $int_page;

    }
    
}

$str_member_flag = false;
if(isset($_SESSION['vipmemberid']) && $_SESSION['vipmemberid'] != "")
{
    $str_member_flag = true;
}
//print_r($_SESSION);
//print $int_total_records; exit;
#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table

$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
            </div>
        </div>
        <?php if($str_visible_cms == "YES") { ?>
            <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        <?php 
	$arr_test=array_keys($str_xml_list);
        $int_cnt=0;
	if($arr_test[0]!="ROOT_ITEM")
	{ 
            if($int_page != 1)
            {
                for($int_cnt;$int_cnt<($int_page-1)*($int_records_per_page);$int_cnt++)
		{
                    array_shift($str_xml_list);
		}	
            }?>
        <div class="row padding-10">
            <?php
            $int_arr_lim=0;
            foreach($str_xml_list as $key => $val)        
            {
                if($int_arr_lim < $int_records_per_page)
                {
                    $int_arr_lim++;
                    if(is_array($val))
                    {
                        if($str_member_flag)
                        {
                            $str_preview_flag=true;
                            $str_access_flag=true;
                        }
                        else
                        {
                            if($str_xml_list[$key]["PREVIEWTOALL"]=="YES")
                            {
                                $str_preview_flag=true;
                            }
                            else
                            {
                                $str_preview_flag=false;
                            }
                            if($str_xml_list[$key]["ACCESSTOALL"]=="YES")
                            {
                                $str_access_flag=true;
                            }
                            else
                            {
                                $str_access_flag=false;
                            }
                        }
                        
                        if($str_preview_flag && $str_access_flag)
                        {
                            //$str_href="./photo_album_p.php?pkid=".$result_album[$key]["PHOTOALBUMPKID"];
                        }
                        else
                        {
                            //$str_forward_page="photo_album_p.php?pkid=".$result_album[$key]["PHOTOALBUMPKID"];
                            //$str_href="./ccbill/index.php?forward_page=".urlencode($str_forward_page)."&#pmember";
                        }
                        ?>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="img">
                                <?php if($str_preview_flag == true || $str_member_flag) { ?>
                                    <img src="<?php print $str_img_path.$str_xml_list[$key]["PKID"]."/".$str_xml_list[$key]["IMAGENAME"]; ?>" alt="<?php print $str_xml_list[$key]["TITLE"]; ?>" title="<?php print $str_xml_list[$key]["TITLE"]; ?>" class="image img-responsive thumbnail" />
                                <?php } else { print("".MakeBlankTab(0,0,$STR_PREVIEW_NOT_AVAILABLE,"preview-danger")); }?>

                                <div class="overlay">
                                    <?php if($str_xml_list[$key]["NEW"] == "YES") { print $STR_ICON_PATH_NEW; } ?>
                                    <?php if($str_xml_list[$key]["HOT"] == "YES") { print $STR_ICON_PATH_HOT; } ?>
                                    <?php if($str_xml_list[$key]["FEATURED"] == "YES") { print $STR_ICON_PATH_FEATURED; } ?>
                                    <?php if($str_xml_list[$key]["PREVIEWTOALL"] == "YES" && $str_xml_list[$key]["ACCESSTOALL"] == "YES") { print $STR_ICON_PATH_FREE; } ?>
                                    <?php
                                            //print $str_member_flag;exit;
                                    if($str_member_flag == true) { ?>    
                                    <h4 class=""><a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/photo-album/<?php  print(GetEncryptId($str_xml_list[$key]["PKID"]));?>" title="Click to view this album" ><b><?php print $str_xml_list[$key]["TITLE"]; ?></b></a></h4>
                                    <?php } else if($str_access_flag == true && $str_preview_flag == true) { ?>    
                                    <h4 class=""><a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/photo-album/<?php  print(GetEncryptId($str_xml_list[$key]["PKID"]));?>" title="Click to view this album" ><b><?php print $str_xml_list[$key]["TITLE"]; ?></b></a></h4>
                                    <?php } else { ?>
                                    <h4 class=""><a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view more info"><b><?php print $str_xml_list[$key]["TITLE"]; ?></b></a></h4>
                                    <?php } ?>
                                    
                                    
                                    
                                    
                                    
                                    
                                    <h4 class=""><b><?php print $STR_LINK_ICON_PATH_IMAGE."&nbsp;".$str_xml_list[$key]["NOOFVISIBLEPHOTO"]; ?></b><?php /* ?>&nbsp;&nbsp;&nbsp;<b><?php print $STR_LINK_ICON_PATH_VISIBLE."&nbsp;".$str_xml_list[$key]["NOOFVIEW"]; ?></b><?php */ ?></h4>
                                    <?php if($str_xml_list[$key]["PHOTOGRAPHERTITLE"] != "" || $str_xml_list[$key]["PHOTOGRAPHERURL"] != "") {?>
                                    <p class=""><a href="" title=""><i class="fa fa-camera"></i>&nbsp;<?php print(DisplayWebsiteUrl(trim($str_xml_list[$key]["PHOTOGRAPHERURL"]),trim(MyHtmlEncode($str_xml_list[$key]["PHOTOGRAPHERTITLE"])),"YES","",$STR_HOVER_VIEW_URL,"",35)); ?></a></p>
                                    <?php } ?>
                                    <?php
                                            //print $str_member_flag;exit;
                                    if($str_member_flag == true) { ?>    
                                        <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/photo-album/<?php  print(GetEncryptId($str_xml_list[$key]["PKID"]));?>" title="Click to view this album" class="btn btn-primary btn-sm"><b>View This Album</b></a>
                                    <?php } else if($str_access_flag == true && $str_preview_flag == true) { ?>    
                                        <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/photo-album/<?php  print(GetEncryptId($str_xml_list[$key]["PKID"]));?>" title="Click to view this album" class="btn btn-primary btn-sm"><b>View This Album</b></a>
                                    <?php } else { ?>
                                        <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view more info" class="btn btn-primary btn-sm"><b>View This Album</b></a>
                                    <?php } ?> 
                                </div>
                            </div>
                        </div>                            
            <?php   
                    }  if($int_arr_lim % 4 == 0) { print "</div><div class='row padding-10'>"; }
                }
            }?>
    </div>
    <div class="row padding-10">
        <div class="col-md-12 col-xs-12 col-sm-12" align="center">
            <nav aria-label="Page navigation">
                <ul class="pagination nopadding">
                <?php 
                $int_margine = 2;
                print(PagingWithMargineWithPost($int_total_records,$int_margine,$int_page,"photo-album",$int_records_per_page,"","key=".$str_name_key."&catid=".$int_cat_pkid."&#ptop"));?>
                </ul>
            </nav>
        </div>
    </div><br/>    
    <?php } ?>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
</body>
</html>
