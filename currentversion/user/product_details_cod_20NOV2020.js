/*
  Jquery Validation using jqBootstrapValidation
   example is taken from jqBootstrapValidation docs 
  */
$(function() {

    $("#frm_cod input").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // something to have when submit produces an error ?
            // Not decided if I need it yet
        },
        
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var txt_pincode = $("input#txt_pincode").val();
            
            $.ajax({
                url: "../../../user/product_details_cod_p.php",
                type: "POST",
                data: 
                {
                    txt_pincode: txt_pincode
                    
                },
                cache: false,
                
                success: function(data) 
                { 
                    //alert(data);
                    var $responseText=JSON.parse(data);
                    if($responseText.status == 'SUC')
                    {
                        // Success message
                        $('#success_cod').html("<div class='alert alert-success small'>");
                        $('#success_cod > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                        $('#success_cod > .alert-success').append("<strong> " + $responseText.message + " </strong>");
                        $('#success_cod > .alert-success').append('</div>');

                        //clear all fields
                        
                        setTimeout(function() { location.reload(); }, 5000);
                    }
                    else if($responseText.status == 'ERR')
                    {
                        // Fail message
                        $('#success_cod').html("<div class='alert alert-danger small'>");
                        $('#success_cod > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                .append("</button>");
                        $('#success_cod > .alert-danger').append("<strong> " + $responseText.message + " ");
                        $('#success_cod > .alert-danger').append('</div>');
                        setTimeout(function() { location.reload(); }, 5000);
                    }
                },
				
		/*success: function() {
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Your contact inquiry has been submitted, we will contact you Soon. Thank you for communication.</strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + " it seems that my mail server is not responding...</strong> Could you please email me directly to <a href='mailto:info@websitename.com?Subject=Message_Me from websitename.com;>info@websitename.com</a> ? Sorry for the inconvenience!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },*/
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
