<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_PHOTO";
$str_img_path = "../mdm/photoalbum/";
$str_xml_filename_album = $STR_XML_FILE_PATH_MODULE."photoalbum/photoalbum.xml";
//print $str_xml_file_album;
$str_db_table_name = "t_page_metatag";
$str_db_table_name_album = "t_photoalbum";
$str_xml_file_name = "photoalbum.xml";
$str_xml_file_name_cat = "video_cat.xml";
$str_xml_file_name_cms = "photoalbum_cms.xml";
$int_records_per_page = 10;
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid = 0;
if(isset($_GET["pid"]) && trim($_GET["pid"]) != "" )
{   
    $int_pkid = trim(GetDecryptId($_GET["pid"]));
}

#read main xml file
//$str_xml_list_cat = "";
//$str_xml_list_cat = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name_cat,"ROOT_ITEM_CAT");

//$int_total_records = 0;
//$int_total_records = (count(array_keys($str_xml_list)));	
//print $int_total_records;exit;
#----------------------------------------------------------------------
/*if(file_exists($str_xml_filename_album)) 
{
    $mod_time = filemtime($str_xml_filename_album);
}
if(time()-$mod_time > $INT_XML_REFRESH_TIME)
{
    WriteXml();//this function is in /includes/lib_app_specific.php.
}*/

$result_album = "";
$result_album = readXml($str_xml_filename_album,"ROOT_ITEM");
$arr_test = array_keys($result_album);
if($arr_test[0] == "ROOT_ITEM")
{
    Closeconnection();
    Redirect("../photo-album#ptop");
    exit();
}

$str_flag=false;
$str_album_title = "";
$str_displayasnew = "";
$str_displayashot = "";
$str_displayasfeatured = "";
$str_album_preview = "";
$str_album_access = "";
$str_album_noofvisphoto = "";
$str_album_noofview = "";
$str_album_photographertitle = "";
$str_album_photographerurl = "";
$str_album_desc = "";
//print $int_pkid; exit;
while(list($key, $val) = each($result_album)) 
{
    if($result_album[$key]["PKID"] == $int_pkid)
    {
        $str_flag = true;
        $str_album_title = $result_album[$key]["TITLE"];
        $str_displayasnew = $result_album[$key]["NEW"];
        $str_displayashot = $result_album[$key]["HOT"];
        $str_displayasfeatured = $result_album[$key]["FEATURED"];
        $str_album_preview = $result_album[$key]["PREVIEWTOALL"];
        $str_album_access = $result_album[$key]["ACCESSTOALL"];
        $str_album_noofvisphoto = $result_album[$key]["NOOFVISIBLEPHOTO"];
        $str_album_noofview = $result_album[$key]["NOOFVIEW"];
        $str_album_photographertitle = $result_album[$key]["PHOTOGRAPHERTITLE"];
        $str_album_photographerurl = $result_album[$key]["PHOTOGRAPHERURL"];
        $str_album_desc = $result_album[$key]["DESCRIPTION"];
        break;
    }		
}	
if(!$str_flag)
{
        Closeconnection();
        Redirect("../photo-album#ptop");
        exit();
}
#----------------------------------------------------------------------
$str_member_flag = false;
if(isset($_SESSION['vipmemberid']) && $_SESSION['vipmemberid'] != "")
{
    $str_member_flag = true;
}
#----------------------------------------------------------------------
if($str_member_flag)
{
    $str_preview_flag = true;
    $str_access_flag = true;
}
else
{
    $str_preview_flag = true;
    $str_access_flag = false;
    if(strtoupper($str_album_preview) == "NO")	
    {	
        $str_preview_flag=false;	
    }	
    if(strtoupper($str_album_access)=="YES")
    {										
        $str_access_flag=true;
    }	
}		
if(!$str_member_flag && (!$str_preview_flag || !$str_access_flag))
{
    CloseConnection();
    Redirect("../vipmember-joinnow#ptop");
    exit();
}
#----------------------------------------------------------------------
$str_xml_list = "";
$str_xml_list = readXml($STR_XML_FILE_PATH_MODULE."photoalbum/photoalbum_".$int_pkid.".xml","ROOT_ITEM_".$int_pkid."");
$int_total_record = count(array_keys($str_xml_list));
//print $int_total_record;
#----------------------------------------------------------------------
#get metatag page title from table

$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?> : <?php if($str_album_title != "") { print "".$str_album_title.""; } ?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
                <?php if($str_album_title != ""){ ?>
                <ol class="breadcrumb thumbnail-margin">

                    <li><a href="<?php print $_SERVER['HTTP_REFERER']; ?>"><?php print $str_album_title; ?></a></li>
                </ol>
                <?php } ?>
            </div>
        </div>
        <?php if($str_album_desc != "" && $str_album_desc != "<br>") { ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <p align="justify"><?php print($str_album_desc);?></p>
            </div>
        </div>
        <?php } ?>
        <?php
        $arr_test = array_keys($str_xml_list);
	$int_cnt=0;
        if(trim($arr_test[0])!="ROOT_ITEM")
        { 
        ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <?php
                $int_cnt = 1;
                //print_r($str_xml_list);
                foreach($str_xml_list as $key => $val)         
                { ?>
                <div class="mySlides5">
                  <div class="numbertext5"><?php print $int_cnt; ?> / <?php print $int_total_record; ?></div>
                  <img src="<?php print $str_img_path.$int_pkid."/".$str_xml_list[$key]["LARGEIMAGEFILENAME"]; ?>" style="width:100%" alt="<?php print $str_xml_list[$key]["PHOTOTITLE"]; ?>" title="<?php print $str_xml_list[$key]["PHOTOTITLE"]; ?>" />
                </div>
                <?php $int_cnt++;
                } ?>
                
                <a class="prev5" onclick="plusSlides(-1)"><i class="fa fa-chevron-left"></i></a>
                <a class="next5" onclick="plusSlides(1)"><i class="fa fa-chevron-right"></i></a>
                <div class="caption-container5">
                    <p align="center" id="caption"></p>
                </div>                
            </div>
        </div>
        <div class="row padding-10"> 
            <?php
            $int_cnt = 1;
            //print_r($str_xml_list);
            foreach($str_xml_list as $key => $val)         
            { ?>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <img class="demo cursor active" src="<?php print $str_img_path.$int_pkid."/".$str_xml_list[$key]["THUMBIMAGEFILENAME"]; ?>" style="width:100%" onclick="currentSlide(<?php print $int_cnt; ?>)" alt="<?php print $str_xml_list[$key]["PHOTOTITLE"]; ?>"  title="<?php print $str_xml_list[$key]["PHOTOTITLE"]; ?>" />
                
                    <div class=" text-top-right">
                        <?php /*/*print $STR_LINK_ICON_PATH_VISIBLE; ?>&nbsp;<?php print $str_xml_list[$key]["NOOFVIEW"]; */ ?>
                        <?php if($str_xml_list[$key]["NEW"] == "YES") { ?> 
                            <?php print $STR_ICON_PATH_NEW; ?>
                        <?php } ?>
                        
                    </div>
                
            </div>
            <?php  $int_cnt++; if($int_cnt % 6 == 1) { print "</div><br/><div class='row padding-10'>"; }
            } ?>
            
        </div>
        <?php 
        } ?>
        <br/>
    </div>
    <script language="JavaScript" src="./../js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="./../js/bootstrap.min.js"></script>
    <style>
        .active,
.demo:hover {
  opacity: 1;
}
        
    </style>
    <script>
        /*GALLERY04 https://www.w3schools.com/howto/howto_js_slideshow_gallery.asp*/
        var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides5");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      //alert(i);
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
/*END GALLERY04 https://www.w3schools.com/howto/howto_js_slideshow_gallery.asp*/
    </script>
</body>
</html>
