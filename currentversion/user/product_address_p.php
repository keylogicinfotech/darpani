<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();
set_time_limit(150);
include "./../user/product_validatesession.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_xml.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/lib_datetimeyear.php";
include "./../user/product_config.php";
//include "./../includes/http_to_https.php";	
//print_r($_POST); exit;
#----------------------------------------------------------------------
# posted variables...
$str_name = "";
if(isset($_POST["txt_name"]) && $_POST["txt_name"] != "")
{
    $str_name = $_POST["txt_name"];
}
if(empty($str_name))
{
    $response['status']='ERR';
    $response['message']="Error... Please Enter Name.";
    echo json_encode($response);
    return;
}

$str_phone = "";
if(isset($_POST["txt_phone"]) && $_POST["txt_phone"] != "")
{
    $str_phone = $_POST["txt_phone"];
}
if(empty($str_phone))
{
    $response['status']='ERR';
    $response['message']="Error... Please Enter Phone Number.";
    echo json_encode($response);
    return;
}

$str_email = "";
if(isset($_POST["txt_email"]) && $_POST["txt_email"] != "")
{
    $str_email = $_POST["txt_email"];
}

if(validateEmail($str_email) == false)
{
    $response['status']='ERR';
    $response['message']= $STR_MSG_ACTION_INVALID_EMAIL_FORMAT;
    echo json_encode($response); 
    return;
}



$str_address = "";
if(isset($_POST["ta_address"]) && $_POST["ta_address"] != "")
{
    $str_address = $_POST["ta_address"];
}
if(empty($str_address))
{
    $response['status']='ERR';
    $response['message']="Error... Please Enter Address.";
    echo json_encode($response);
    return;
}

$str_address = "";
if(isset($_POST["ta_address"]) && $_POST["ta_address"] != "")
{
    $str_address = $_POST["ta_address"];
}
if(empty($str_address))
{
    $response['status']='ERR';
    $response['message']="Error... Please Enter Address.";
    echo json_encode($response);
    return;
}



$int_countrypkid = 0;
$str_country = "";

if(isset($_POST["cbo_country"]) && $_POST["cbo_country"] != "")
{
    $int_countrypkid = $_POST["cbo_country"];
}

if($int_countrypkid == 0 || $int_countrypkid == "") 
{
    $response['status']='ERR';
    $response['message']="Error... Please Select Country.";
    echo json_encode($response);
    return;
}

$int_shipping_value_countrywise = 0;
$int_courier_price_per_kg_countrywise = 0;
$int_conversionrate_countrywise = 1;
if($int_countrypkid > 0)
{
    $str_query_select = "";
    $str_query_select = "SELECT title, courierpriceperkg, conversionrate FROM ".$STR_DB_TABLE_NAME_COUNTRY." WHERE visible='YES' AND pkid=".$int_countrypkid;
    $rs_list_country = GetRecordSet($str_query_select);
    if(!$rs_list_country->EOF())
    {
        $str_country = $rs_list_country->Fields("title");
        $int_courier_price_per_kg_countrywise = $rs_list_country->Fields("courierpriceperkg");
        $int_conversionrate_countrywise = $rs_list_country->Fields("conversionrate");
    }
}
 //print $int_conversionrate_countrywise;exit;   

$int_statepkid = 0;
$str_state = "";
if(isset($_POST["cbo_state"]) && $_POST["cbo_state"] != "")
{
    $int_statepkid = $_POST["cbo_state"];
}

if($int_statepkid == 0 || $int_statepkid == "") 
{
    $response['status']='ERR';
    $response['message']="Error... Please Select State.";
    echo json_encode($response);
    return;
}

if($int_statepkid > 0)
{
    $str_query_select = "";
    $str_query_select = "SELECT title FROM ".$STR_DB_TABLE_NAME_STATE." WHERE visible='YES' AND masterpkid=".$int_countrypkid." AND pkid=".$int_statepkid;
    $rs_list_state = GetRecordSet($str_query_select);
    if(!$rs_list_state->EOF())
    {
        $str_state = $rs_list_state->Fields("title");
    }
}
//print_r($_POST); exit;
$str_city = "";
if(isset($_POST["txt_city"]) && $_POST["txt_city"] != "")
{
    $str_city = $_POST["txt_city"];
}
if(empty($str_city))
{
    $response['status']='ERR';
    $response['message']="Error... Please Enter Town / City.";
    echo json_encode($response);
    return;
}

$str_zipcode = "";
if(isset($_POST["txt_pincode"]) && $_POST["txt_pincode"] != "")
{
    $str_zipcode = $_POST["txt_pincode"];
}
if(empty($str_zipcode))
{
    $response['status']='ERR';
    $response['message']="Error... Please Enter Zip / Postal Code.";
    echo json_encode($response);
    return;
}


$item_sessionid="";
if(isset($_SESSION['sessionid']))
{ 
    $item_sessionid = $_SESSION['sessionid']; 
}
$item_uniqueid = "";
if(isset($_SESSION['uniqueid']))
{ 
    $item_uniqueid = $_SESSION['uniqueid']; 
}

$str_where = "";
if(isset($_COOKIE["sessionid"]) && isset($_COOKIE["uniqueid"]) && $_COOKIE["sessionid"] != "" && $_COOKIE["uniqueid"] != "") 
{
    $str_where = " WHERE sessionid='".$_COOKIE["sessionid"]."' AND uniqueid='".$_COOKIE["uniqueid"]."' ";
}
else
{
    $str_where = " WHERE sessionid='".$item_sessionid."' AND uniqueid='".$item_uniqueid."' ";
}


$str_member_flag = false;
$int_userpkid = 0;
$str_username = "";
if(isset($_SESSION['userpkid']) && $_SESSION['userpkid'] != "")
{
    $str_member_flag = true;
    $int_userpkid = $_SESSION['userpkid'];
    $str_username = $_SESSION['username'];
}

$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_SESSION_CART." ".$str_where;
$rs_list = GetRecordSet($str_query_select);

if($rs_list->Count() > 0)
{
    while(!$rs_list->EOF())
    {
        $int_tailoring_price = 0;
        $int_tailoring_price = number_format(ceil($rs_list->Fields("tailoringprice") / $int_conversionrate_countrywise), 2);
        //print $int_tailoring_price."<br/>";
        
        $int_shipping_value = 0;
        //$int_shipping_value = number_format(ceil($int_shipping_value_countrywise / $int_conversionrate_countrywise), 2);
	$int_shipping_value = (($rs_list->Fields("weight") * $int_courier_price_per_kg_countrywise) / $int_conversionrate_countrywise) * $rs_list->Fields("quantity"); 
        
        //print $int_shipping_value." - ";
        
        $int_price = 0;
        $int_price = ceil($rs_list->Fields("price") / $int_conversionrate_countrywise);   
        
        
        $int_extended_price = 0;
        $int_extended_price = $int_price * $rs_list->Fields("quantity");


        $str_query_update = "";
        $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_SESSION_CART." SET name = '".$str_name."', ";
        $str_query_update.= "shippingvalue = ".$int_shipping_value.", ";
        $str_query_update.= "tailoringprice = ".$int_tailoring_price.", ";
        
        $str_query_update.= "price = ".$int_price.", ";
        $str_query_update.= "extendedprice = ".$int_extended_price.", ";
        $str_query_update.= "userpkid = ".$int_userpkid.", ";
        $str_query_update.= "username = '".$str_username."', ";
        $str_query_update.= "memberpkid = ".$int_userpkid.", ";
        $str_query_update.= "emailid = '".$str_email."', ";
        $str_query_update.= "address= '".$str_address."', ";
        $str_query_update.= "phone = '".$str_phone."', ";
        $str_query_update.= "countrypkid= '".$int_countrypkid."', ";
        $str_query_update.= "conversionrate= '".$int_conversionrate_countrywise."', ";
        $str_query_update.= "country= '".$str_country."', ";
        $str_query_update.= "state = '".$str_state."', ";
        $str_query_update.= "city = '".$str_city."', ";
        $str_query_update.= "zipcode='".$str_zipcode."' ";
        $str_query_update.= " WHERE serialno=".$rs_list->Fields("serialno")." AND sessionid='".$item_sessionid."'"." AND uniqueid='".$item_uniqueid."'";
        //print $str_query_update;
        
        //print $int_price."<br/>";
        ExecuteQuery($str_query_update);
        
        $rs_list->MoveNext();
    }
}

//exit;

/*$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_SESSION_CART." SET name = '".$str_name."', ";
$str_query_update.= "shippingvalue = ". number_format(ceil($int_shipping_value/$int_conversionrate_countrywise), 2).", ";

$str_query_update.= "emailid = '".$str_email."', ";
$str_query_update.= "address= '".$str_address."', ";
$str_query_update.= "phone = '".$str_phone."', ";
$str_query_update.= "countrypkid= '".$int_countrypkid."', ";
$str_query_update.= "conversionrate= '".$int_conversionrate_countrywise."', ";
$str_query_update.= "country= '".$str_country."', ";
$str_query_update.= "state = '".$str_state."', ";
$str_query_update.= "city = '".$str_city."', ";
$str_query_update.= "zipcode='".$str_zipcode."' ";
$str_query_update.= $str_where;
//$str_query_update.= "WHERE sessionid='".$item_sessionid."' AND uniqueid='".$item_uniqueid."'" ;

ExecuteQuery($str_query_update);
*/
//$_SESSION["usr_cntry"] = $int_countrypkid; // Commented on 02JAN2020


$response['status']='SUC';
$response['message']="Delivery Address Saved Successfully.";
echo json_encode($response);
return;

?>
