<?php

session_start();
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_email.php";
include "../includes/lib_xml.php";
include "../includes/lib_file_upload.php";
include "../includes/lib_image.php";
#--------------------------------------------------------------------------------------------------------
//print_r($_POST);exit;

$str_db_table_name = "t_bookme";
#----------------------------------------------------------------------------------------------------

$str_title = "";	 
if(isset($_POST['txt_title']))
{
    $str_title = trim($_POST['txt_title']);
}
if(empty($str_title))
{
    $response['status']='ERR';
    $response['message']= "Error... Please enter assignment type.";
    echo json_encode($response); 
    return;
}

$int_price = 0.00;	 
if(isset($_POST['txt_price']))
{
    $int_price = trim($_POST['txt_price']);
}
if(empty($int_price) || !is_numeric($int_price) || $int_price <= 0 )
{
    $response['status']='ERR';
    $response['message']= $STR_MSG_ACTION_INVALID_PRICE;
    echo json_encode($response); 
    return;
}


$int_day = "";
if(isset($_POST['cbo_day']))
{
    $int_day = trim($_POST['cbo_day']);
}

$int_month = "";
if(isset($_POST['cbo_month']))
{
    $int_month = trim($_POST['cbo_month']);
}

$int_year = "";
if(isset($_POST['cbo_year']))
{
    $int_year = trim($_POST['cbo_year']);
}
//print $int_day;

//print $int_month;

//print $int_year;exit;


if($int_day == "" || $int_month == "" || $int_year == "")
{
    $response['status']='ERR';
    $response['message']= "Error... Please select valid date";
    echo json_encode($response); 
    return;
}


if(($int_day != "" && is_numeric($int_day)) || ($int_month != ""  && is_numeric($int_month)) || ($int_year != ""  && is_numeric($int_year)))
{
    
    if(!checkdate($int_month,$int_day,$int_year))
    {
        $response['status']='ERR';
        $response['message']= $STR_MSG_ACTION_INVALID_DATE_FORMAT;
        echo json_encode($response); 
        return;
    }
}

$str_event_date = "";
$str_event_date = $int_year."-". $int_month ."-". $int_day;

$str_current_date = "";
$str_current_date = date('Y-m-d');

if($str_event_date < $str_current_date)
{
    $response['status']='ERR';
    $response['message']= "Error... Event date should be greater than current date.";
    echo json_encode($response); 
    return;
}


$str_time_hh = "";
if(isset($_POST['cbo_time_hh']))
{
    $str_time_hh = trim($_POST['cbo_time_hh']);
}

$str_time_mm= "";
if(isset($_POST['cbo_time_mm']))
{
    $str_time_mm = trim($_POST['cbo_time_mm']);
}

$str_time_ampm = "";
if(isset($_POST['cbo_time_ampm']))
{
    $str_time_ampm = trim($_POST['cbo_time_ampm']);
}

if(empty($str_time_hh) || empty($str_time_mm) || empty($str_time_ampm))
{
    $response['status']='ERR';
    $response['message']= "Error... Please select time.";
    echo json_encode($response); 
    return;
}



$str_timezone = "";
if(isset($_POST['cbo_timezone']))
{
    $str_timezone = trim($_POST['cbo_timezone']);
}

if(empty($str_timezone))
{
    $response['status']='ERR';
    $response['message']= "Error... Please select timezone.";
    echo json_encode($response); 
    return;
}

$str_name = "";	 
if(isset($_POST['txt_name']))
{
    $str_name = trim($_POST['txt_name']);
}
if(empty($str_name))
{
    $response['status']='ERR';
    $response['message']= "Error... Please enter name!";
    echo json_encode($response); 
    return;
}

$str_location = "";	
if(isset($_POST['txt_location']))
{
    $str_location = trim($_POST['txt_location']);
}

if(empty($str_location))
{
    $response['status']='ERR';
    $response['message']= "Error... Please enter event venue!";
    echo json_encode($response); 
    return;
}

$str_emailid = "";	 
if(isset($_POST['txt_emailid']))
{
    $str_emailid = trim($_POST['txt_emailid']);
}
if($str_emailid != "")
{
    if(validateEmail($str_emailid) == false)
    {
        $response['status']='ERR';
        $response['message']= $STR_MSG_ACTION_INVALID_EMAIL_FORMAT;
        echo json_encode($response); 
        return;
    }
}
else
{
    $response['status']='ERR';
    $response['message']= "Error... Enter email ID.";
    echo json_encode($response); 
    return;
}

$str_phone = "";	 
if(isset($_POST['txt_phone']))
{
    $str_phone = trim($_POST['txt_phone']);
}

$str_fax = "";	 
if(isset($_POST['txt_fax']))
{
    $str_fax = trim($_POST['txt_fax']);
}
/*$int_modelpkid=0;	
if(isset($_POST['hdn_modelpkid']))
{
    $int_modelpkid = trim($_POST['hdn_modelpkid']);
}	
if($int_modelpkid == 0 || $int_modelpkid == "" || !is_numeric($int_modelpkid))
{
    $response['status']='ERR';
    $response['message']= "Some Information Missing!";
    echo json_encode($response); 
    return;
}

$int_memberpkid=0;	
if(isset($_POST['hdn_memberpkid']))
{
    $int_memberpkid = trim($_POST['hdn_memberpkid']);
}
if($int_memberpkid=="") { $int_memberpkid = 0; }

if($int_memberpkid != $_SESSION["freememberpkid"] && false && $int_modelpkid != $_SESSION["modelpkid"]) {

    if($int_memberpkid == 0 || $int_memberpkid == "" || !is_numeric($int_memberpkid))
    {
        $response['status']='ERR';
        $response['message']= "Please login first to comment!";
        echo json_encode($response); 
        return;
    }
}*/

$str_desc= "";
if(isset($_POST['ta_desc'])){
    $str_desc= trim($_POST['ta_desc']);
}

/*$str_sel_member = ""; 
$str_sel_member = "SELECT * FROM t_freemember WHERE pkid=".$int_memberpkid; 
$rs_mem_details = GetRecordSet($str_sel_member);

$str_shortenurlkey = "";
$str_shortenurlkey = $rs_mem_details->Fields("shortenurlkey");*/

if(empty($str_desc))
{
    $response['status']='ERR';
    $response['message']= "Please enter comment!";
    echo json_encode($response); 
    return;
}

$str_secretcode = "";
if (isset($_POST["txt_secretcode"])) { $str_secretcode = trim($_POST["txt_secretcode"]); }

if(empty($str_secretcode) || $_SESSION['image_secret_code'] != $str_secretcode)
{
    $response['status']='ERR';
    $response['message']= $STR_MSG_ACTION_INVALID_SECRET_CODE;
    echo json_encode($response);
    return;
}


#upload image file.
//$UPLOAD_PHOTO_PATH="../mdm/contact/";
//$str_dir=$UPLOAD_PHOTO_PATH;
#----------------------------------------------------------------------------------------------------
#upload image file.
/*$str_large_file_name="";
if($str_files != "")
{
	$str_dir="";
	$str_dir=$UPLOAD_PHOTO_PATH;
	//print $str_dir; exit;
	if(!file_exists($str_dir))
	{
		$response['status']='ERR';
		$response['message']= "Invalid file path!";
		echo json_encode($response);
		return;
	}
	#upload image file.	
	$str_large_path="";
	
	$str_large_file_name=$str_files;
	$str_large_path = trim($str_dir . $str_large_file_name);
	//print $str_large_path; exit;
	UploadFile($_FILES['uploadimages']['tmp_name'],$str_large_path);
	//ResizeImage($str_large_path,$INT_BIO_WIDTH);
}*/
//exit;
//$str_fax = $_POST['txt_phone'];
$str_create_datetime = "";
$str_create_datetime = date("Y-m-d H:i:s");
$str_ip_address = $_SERVER['REMOTE_ADDR'];

$str_unique_id =  "";
$str_unique_id = $str_create_datetime ."|". $str_emailid ."|". $str_secretcode;

$int_max_displayorder = 0;
//$int_max_displayorder = GetSubcatMaxValue($STR_DB_TABLE_NAME,"subcatpkid",$int_subcatpkid,"displayorder");
//$int_max_displayorder = GetMaxValue($str_db_table_name,"displayorder");
#----------------------------------------------------------------------------------------------------
$str_query_insert = "";
$str_query_insert = "INSERT INTO ".$str_db_table_name."(submitdatetime, emailid, uniquerowid, skilltitle, budget, eventdate, eventtime_hh, eventtime_mm";
$str_query_insert.=" , time_ampm, timezone, venue, description,contactperson,phone,fax,ipaddress,displayasnew)";
$str_query_insert.=" VALUES('".ReplaceQuote($str_create_datetime)."', '".ReplaceQuote($str_emailid)."' , '".ReplaceQuote($str_unique_id)."' , '".ReplaceQuote($str_title)."' , ".$int_price." , '".ReplaceQuote($str_event_date)."' , '".ReplaceQuote($str_time_hh)."' , '".ReplaceQuote($str_time_mm)."' , '".ReplaceQuote($str_time_ampm)."' , '".ReplaceQuote($str_timezone)."' , '".ReplaceQuote($str_location)."' , '".ReplaceQuote($str_desc)."', '".ReplaceQuote($str_name)."', '".ReplaceQuote($str_phone)."', '".ReplaceQuote($str_fax)."', ";
$str_query_insert.="'".$str_ip_address."', 'YES')";	
//print($str_query_insert); exit; 
ExecuteQuery($str_query_insert);	
#----------------------------------------------------------------------------------------------------
$fp=openXMLfile($STR_XML_FILE_PATH_MODULE."siteconfiguration.xml");
$str_from = getTagValue($STR_FROM_DEFAULT,$fp);
$str_to = getTagValue($STR_FROM_DEFAULT,$fp);
closeXMLfile($fp);
	
$str_subject="Inquiry Submitted From " . $STR_SITE_URL. "'s Book Me Section";
	
$mailbody="User has sent mail inquiry from Book Me Section of " . $STR_SITE_URL . " <br> Following are the details:";
$mailbody.="<BR><BR> <strong>Submit Datetime:</strong> " . DDMMMYYYYFormat(date('Y-m-d H:i:s')) . " ";
$mailbody.="<br><br><strong>Type Of Assignment:</strong> " . MyHtmlEncode(RemoveQuote($str_title)) . " ";
$mailbody.="<br><strong>Budget:</strong> ". GetPriceValue($int_price) . " ";
$mailbody.="<br><br><strong>Event Date :</strong> " . DDMMMYYYYFormat($int_year . "-" . $int_month . "-" . $int_day . " ");
$mailbody.="<br><strong>Event Time:</strong> " . $str_time_hh .":" . $str_time_mm.":". $str_time_ampm .":". strtoupper($str_timezone) . "";
$mailbody.="<br><strong>Event Venue:</strong> " . RemoveQuote($str_location) . "";
$mailbody.="<br><strong>Event Description:</strong> " . RemoveQuote($str_desc)."";
$mailbody.="<br><br><strong>Contact Person Name:</strong> " . MyHtmlEncode(RemoveQuote($str_name)) . "";
$mailbody.="<br><strong>Email Address:</strong> " . $str_emailid . "";
$mailbody.="<br><strong>Phone Number:</strong> " . ReplaceNullStringWithCharacter(MyHtmlEncode(RemoveQuote($str_phone)),"Not Specified") . "";
$mailbody.="<br><strong>Fax Number:</strong> " . ReplaceNullStringWithCharacter(MyHtmlEncode(RemoveQuote($str_fax)),"Not Specified") . "";
$mailbody.="<br><strong>IP Address:</strong> " . $str_ip_address;
sendmail($str_to,$str_subject,$mailbody,$str_from,1);

#----------------------------------------------------------------------------------------------------
//$_SESSION['image_secret_code']="";
$response['status']='SUC';
$response['message']="Your inquiry has been submitted successfully.";
echo json_encode($response);
return;
?>
