<?php
/*
File Name  :- user_personal_details_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#--------------------------------------------------------------------------------------------------------
#Include files
session_start();
error_reporting(0);
include("./../includes/validate_mdl_session.php");
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_file_system.php";
include "./../includes/lib_xml.php";
include "./../includes/lib_image.php";

//include "./../includes/unique_site_view_count.php";
//include("./../includes/lib_app_specific.php");
include "./../includes/http_to_https.php";
//print_r($_POST);exit;
//-------------------------------------------------------------------------------------------------------
$str_db_table_name = " t_user";
$str_img_path = "../mdm/user/";
$INT_USER_SESSION_PKID = $_SESSION["userpkid"];
$int_pkid = "";

if (isset($_POST["hdn_pkid"])) {
    $int_pkid = trim($_POST["hdn_pkid"]);
}
if ($int_pkid == "" || $int_pkid <= 0 || !is_numeric($int_pkid)) {
    $response['status'] = 'ERR';
    $response['message'] = "Some Information Missing!";
    echo json_encode($response);
    return;
}
if ($int_pkid != $INT_USER_SESSION_PKID) {
    $response['status'] = 'ERR';
    $response['message'] = "Some Information Missing!";
    echo json_encode($response);
    return;
}

//Get Initialize Variables
if (isset($_POST["txt_loginid"])) {
    $str_loginid = trim($_POST["txt_loginid"]);
}
$str_name = "";
if (isset($_POST["txt_name"])) {
    $str_name = trim($_POST["txt_name"]);
}
$str_email_id = "";
if (isset($_POST["txt_email"]) && trim($_POST["txt_email"]) != "") {
    $str_email_id = trim($_POST["txt_email"]);
}
$str_address = "";
if (isset($_POST["ta_add"])) {
    $str_address = trim($_POST["ta_add"]);
}
/*$str_usertype="";
if(isset($_POST["cbo_user"]))   
{
    $str_usertype=trim($_POST["cbo_user"]);
}*/
$str_businessname = "";
if (isset($_POST["txt_businessname"])) {
    $str_businessname = trim($_POST["txt_businessname"]);
}
$int_phoneno = 0;
if (isset($_POST["txt_phoneno"])) {
    $int_phoneno = trim($_POST["txt_phoneno"]);
}
$int_mobno = 0;
if (isset($_POST["txt_mobno"])) {
    $int_mobno = trim($_POST["txt_mobno"]);
}
$int_zipcode = 0;
if (isset($_POST["txt_zipcode"])) {
    $int_zipcode = trim($_POST["txt_zipcode"]);
}

if (isset($_FILES['fileimage'])) {
    $str_image = trim($_FILES['fileimage']['name']);
}
if (isset($_FILES['file_image'])) {
    $str_image = trim($_FILES['file_image']['name']);
}
$str_country = "";
if (isset($_POST["cbo_country"])) {
    $str_country = trim($_POST["cbo_country"]);
}
$str_state = "";
if (isset($_POST["cbo_state"])) {
    $str_state = trim($_POST["cbo_state"]);
}
$str_city = "";
if (isset($_POST["txt_city"])) {
    $str_city = trim($_POST["txt_city"]);
}


// -------------------------------- OFFICE FORM INSERT DATA ---------------------------------------------------


$str_ofc_fullname = "";
if (isset($_POST["ofc_fullname"])) {
    $str_ofc_fullname = trim($_POST["ofc_fullname"]);
}
$str_ofc_businessname = "";
if (isset($_POST["ofc_businessname"])) {
    $str_ofc_businessname = trim($_POST["ofc_businessname"]);
}
$str_ofc_email_id = "";
if (isset($_POST["ofc_email"]) && trim($_POST["ofc_email"]) != "") {
    $str_ofc_email_id = trim($_POST["ofc_email"]);
}
$str_ofc_country = "";
if (isset($_POST["ofc_country"])) {
    $str_ofc_country = trim($_POST["ofc_country"]);
}
$str_ofc_state = "";
if (isset($_POST["ofc_state"])) {
    $str_ofc_state = trim($_POST["ofc_state"]);
}
$str_ofc_city = "";
if (isset($_POST["ofc_city"])) {
    $str_ofc_city = trim($_POST["ofc_city"]);
}
$int_ofc_zipcode = 0;
if (isset($_POST["ofc_zipcode"])) {
    $int_ofc_zipcode = trim($_POST["ofc_zipcode"]);
}
$str_ofc_address = "";
if (isset($_POST["ofc_add"])) {
    $str_ofc_address = trim($_POST["ofc_add"]);
}
$int_ofc_mobno = 0;
if (isset($_POST["ofc_mobno"])) {
    $int_ofc_mobno = trim($_POST["ofc_mobno"]);
}
$int_ofc_phoneno = 0;
if (isset($_POST["ofc_phoneno"])) {
    $int_ofc_phoneno = trim($_POST["ofc_phoneno"]);
}





#--------------------------------------------------------------------------------------------------------------------------

/*if(empty($str_fullname))
{
    $response['status']='ERR';
    $response['message']= "Enter Full Name";
    echo json_encode($response); 
    return;
}*/

if (empty($str_loginid) || validateEmail($str_loginid) == false) {
    $response['status'] = 'ERR';
    $response['message'] = "Invalid Login ID!";
    echo json_encode($response);
    return;
}

/*if(isset($_POST["txt_zipcode"]))
{
    $int_zipcode=trim($_POST["txt_zipcode"]);
}


if(isset($_POST["txt_city"]))
{
    $str_city=trim($_POST["txt_city"]);
}*/
/*if(empty($str_city))
{
    $response['status']='ERR';
    $response['message']= "Enter City";
    echo json_encode($response); 
    return;
}

if(isset($_POST["txt_state"]))
{
    $str_state=trim($_POST["txt_state"]);
}
  */
/*if(empty($str_city))
{
    $response['status']='ERR';
    $response['message']= "Enter State";
    echo json_encode($response); 
    return;
}

if(isset($_POST["cbo_country"]))
{
    $int_country_pkid=trim($_POST["cbo_country"]);
}*/
/*if(empty($int_country_pkid) || $int_country_pkid==0 )
{
    $response['status']='ERR';
    $response['message']= "Select Country";
    echo json_encode($response); 
    return;
}

if(isset($_POST["cbo_state"]))
{
    $int_state_pkid=trim($_POST["cbo_state"]);
}*/
/*if(empty($int_state_pkid) || $int_state_pkid == 0)
{
    $response['status']='ERR';
    $response['message']= "Select State";
    echo json_encode($response); 
    return;
}*/

/*if(isset($_POST["ta_headline"]))
{ 
        $str_headline=trim($_POST["ta_headline"]); 
}*/

//print "des:" .$str_desc;
/*if(isset($_POST["ta_keyword_list"]))
{
    $str_keyword_list=trim($_POST["ta_keyword_list"]);
}*/

$str_query_select = "";
$str_query_select = "SELECT loginid FROM " . $str_db_table_name . " WHERE pkid != " . $int_pkid . " AND loginid='" . $str_login_id . "'";
//print $str_query_select; exit;
$rs_list = GetRecordSet($str_query_select);
if ($rs_list->Count() > 0) {
    $response['status'] = 'ERR';
    $response['message'] = "Login id entered by you is already exist. Please try with another Login id!";
    echo json_encode($response);
    return;
} else {
    $str_query_select = "";
    $str_query_select = "SELECT loginid FROM " . $str_db_table_name . " WHERE loginid='" . $str_login_id . "'";
    //print $str_query_select; exit;
    $rs_list_mem = GetRecordSet($str_query_select);

    if ($rs_list_mem->Count() > 0) {
        $response['status'] = 'ERR';
        $response['message'] = "Login id entered by you is already exist. Please try with another login id!";
        echo json_encode($response);
        return;
    }
}

if (isset($_FILES['file_image'])) {
    $str_model_photo = trim($_FILES['file_image']['name']);
}


//Check Validate Email Id.
if (ValidateEmail($str_email_id) == false) {
    $response['status'] = 'ERR';
    $response['message'] = "Email Id is not in valid format...!!!";
    echo json_encode($response);
    return;
}



//-------------------------------------------------------------------------------------------------------
//Upload Image
$str_query_select = "";
$str_query_select = "SELECT imagefilename FROM " . $str_db_table_name . " WHERE pkid=" . $int_pkid;
$rs_list = GetRecordset($str_query_select);
if ($rs_list->eof()) {
    CloseConnection();
    Redirect("item_edit.php?msg=I&type=E");
    exit();
}

$str_thumb_file_name = "";
$str_thumb_path = "";
$str_thumb_file_name = $rs_list->fields("imagefilename");
if ($str_image != "") {
    #Delete old image
    if ($str_thumb_file_name != "") {
        DeleteFile($str_img_path . trim($str_thumb_file_name));
    }
    #Upload new image
    $str_thumb_file_name = GetUniqueFileName() . "_t." . getextension($str_image);
    $str_thumb_path = trim($str_img_path . "/" . $str_thumb_file_name);
    UploadFile($_FILES['file_image']['tmp_name'], $str_thumb_path);
    //ResizeImage($str_thumb_path,$INT_IMG_THUMB_WIDTH);
}

#---------------------------------------------------------------------------------------------------------	
# Select Query
/*$str_query_select = "";
$str_query_select = "SELECT imagefilename FROM " .$str_db_table_name. " WHERE pkid=".$int_pkid;
//print $str_select_select;exit;
$rs_list = GetRecordset($str_query_select);

$str_large_file_name = "";
$str_img_path = "";
$str_large_file_name = trim($rs_list->fields("imagefilename"));

if($str_image!="")
{
    #delete old image

    if($str_large_file_name != "")
    {
        DeleteFile($str_img_path.trim($str_large_file_name));
    }

    $int_max_pkid = GetMaxValue($str_db_table_name,"pkid");	
    #upload new image
    $str_large_file_name = GetUniqueFileName()."_t.".strtolower(getextension($str_image));
    $str_img_path = trim($str_img_path.$str_large_file_name);
    UploadFile($_FILES['fileimage']['tmp_name'],$str_img_path);
    //ResizeImage($str_img_path,$INT_HOME_CONTENT_WIDTH);
} */
//---------------------------------------------------------------------------------------------------------------------
//Update Query In t_model Table.
$str_query_update = "";
$str_query_update = "UPDATE " . $str_db_table_name . " SET ";
$str_query_update .= "loginid='" . ReplaceQuote($str_loginid) . "',";
$str_query_update .= "name='" . ReplaceQuote($str_name) . "',";
$str_query_update .= "businessname='" . ReplaceQuote($str_businessname) . "',";
$str_query_update .= "address='" . ReplaceQuote($str_address) . "',";
$str_query_update .= "emailid='" . ReplaceQuote($str_email_id) . "',";
//    $str_query_update .= "isusertype='".ReplaceQuote($str_usertype)."',";
$str_query_update .= "mobileno='" . ReplaceQuote($int_mobno) . "',";
$str_query_update .= "phoneno='" . ReplaceQuote($int_phoneno) . "',";
$str_query_update .= "zipcode='" . ReplaceQuote($int_zipcode) . "',";
$str_query_update .= "city='" . ReplaceQuote($str_city) . "',";
//    $str_query_update .= "state='".ReplaceQuote($str_state)."',";
$str_query_update .= "countrypkid='" . ReplaceQuote($str_country) . "',";
//    $str_query_update .= "countryname='".ReplaceQuote($str_country_name)."',";
$str_query_update .= "statepkid=" . ReplaceQuote($str_state) . ",";
//    $str_query_update .= "statename='".ReplaceQuote($str_state_name)."',";	
//    $str_query_update .= "imagefilename='".ReplaceQuote($str_model_thumb_filename)."'";

//------------------ OFFICE UPDATE DATA --------------------
$str_query_update .= "ofc_fullname='" . ReplaceQuote($str_ofc_fullname) . "',";
$str_query_update .= "ofc_businessname='" . ReplaceQuote($str_ofc_businessname) . "',";
$str_query_update .= "ofc_email='" . ReplaceQuote($str_ofc_email_id) . "',";
$str_query_update .= "ofc_countrypkid='" . ReplaceQuote($str_ofc_country) . "',";
$str_query_update .= "ofc_statepkid='" . ReplaceQuote($str_ofc_state) . "',";
$str_query_update .= "ofc_city='" . ReplaceQuote($str_ofc_city) . "',";
$str_query_update .= "ofc_zipcode='" . ReplaceQuote($int_ofc_zipcode) . "',";
$str_query_update .= "ofc_address='" . ReplaceQuote($str_ofc_address) . "',";
$str_query_update .= "ofc_mobileno='" . ReplaceQuote($int_ofc_mobno) . "',";
$str_query_update .= "ofc_phoneno='" . ReplaceQuote($int_ofc_phoneno) . "',";


$str_query_update .= "imagefilename='" . ReplaceQuote($str_thumb_file_name) . "'";
//    $str_query_update .= "imagefilenamelarge='".ReplaceQuote($str_user_large_filename)."' ";
//    $str_query_update .= "imagefilsenamelarge='".ReplaceQuote($str_user_large_filename)."' ";
//    print $str_query_update;



$str_query_update .= " WHERE pkid=" . $int_pkid;

// print($str_query_update);
// exit();
//print_r($_FILES);
//print_r($_POST); exit(); 
ExecuteQuery($str_query_update);

//-----------------------------------------------------------------------------------------------------------------
/*if($int_modelpkid != "")        
{
    $fp=openXMLfile($UPLOAD_XML_CONTENT_PATH."siteconfiguration.xml");
    $str_from = getTagValue($STR_FROM_DEFAULT,$fp);
    $str_to = getTagValue($STR_FROM_DEFAULT,$fp);
    closeXMLfile($fp);
    
    $str_query_select = "";
    $str_query_select = "SELECT shortenurlkey, emailid, getnotification FROM " .$str_db_table_name. " WHERE pkid=".$int_modelpkid."";
    //PRINT $str_query_select; EXIT;
    $rs_list = GetRecordSet($str_query_select);
    //$str_to_model = $rs_list->fields("emailid");

    if(!$rs_list->eof() && $str_model_large_filename_new!='')
    {
        $str_subject="New main profile image is submitted for approval on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
        $mailbody="New main profile image is submitted for approval. Below are details.<br/><br/>";
        $mailbody.="<strong>Uploaded By:</strong> ".$rs_list->fields("shortenurlkey")."<br/>";
        $mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL."";
        //print "<br/>".$str_from; print "<br/>".$str_to; print "<br/>".$mailbody; exit;
        sendmail($str_to,$str_subject,$mailbody,$str_from,1);  
    }
} */
//-----------------------------------------------------------------------------------------------------------------
$response['status'] = 'SUC';
$response['message'] = "Personal Information Saved Successfully.";
echo json_encode($response);
return; 
//-----------------------------------------------------------------------------------------------------------------
