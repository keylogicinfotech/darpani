<?php
// header('Refresh: 1800'); // This is used to auto refresh page after every 30 minutes
//print_r($_SESSION);exit;
if (!isset($_SESSION["userpkid"]) || isset($_SESSION["userpkid"])=="" || is_null(isset($_SESSION["userpkid"])))
{
    header('Location: ../user-login');
    exit();
}

// This functionality is used to auto log off model from website - START HERE
// if(!session_is_registered('session_count')) 
if(!isset($_SESSION['session_count'])) 
{
    $session_count = 0;
    $session_start = time();
    $_SESSION['session_count'] = $session_count;
    $_SESSION['session_start'] = $session_start;
}
else 
{
    $session_count = $_SESSION['session_count'];
    $session_count++;
}

$session_timeout = 1800; // 15 minutes (in sec)
$session_duration = time() - $_SESSION['session_start'];
if ($session_duration > $session_timeout) 
{
    if(isset($_SESSION["userpkid"]) && trim($_SESSION["userpkid"])!="")
    {
        include "../includes/configuration.php";
        include "../includes/lib_data_access.php";
        $str_query_update = "UPDATE t_user SET online='NO' WHERE pkid = ".$_SESSION["userpkid"];
        ExecuteQuery($str_query_update);
    }
    session_unset();
    session_destroy();
    $_SESSION = array();
//header('Location: ./signin.php?expired=yes');  // Redirect to Login Page
    header('Location: ../user-login');
}
else
{
    $session_start = time();
    $_SESSION['session_start']=$session_start;
}
// This functionality is used to auto log off model from website - END HERE
?>