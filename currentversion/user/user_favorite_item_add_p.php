<?php 
/*
File Name  :- user_favorite_item_add_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
//print "HI"; exit;
session_start();
include "./../includes/configuration.php";
include "./../user/product_config.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";
//include "./../includes/lib_file_upload.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
include "./user_config.php";

//print_r($_POST); exit;
#----------------------------------------------------------------------
//Initializing Variables.
//print_r($_GET);exit;
$int_itempkid=0;
if(isset($_POST["itempkid"]) && trim($_POST["itempkid"])!="")
{
    $int_itempkid = trim($_POST["itempkid"]);
}
//print $int_itempkid;exit;
$int_userpkid=0;
if(isset($_POST["userpkid"]) && trim($_POST["userpkid"])!="")
{
    $int_userpkid = trim($_POST["userpkid"]);
}
//print $int_itempkid. "<br>" ;exit;
$str_flg_fav="";
if(isset($_POST["flgfav"]) && trim($_POST["flgfav"])!="")
{
    $str_flg_fav = trim($_POST["flgfav"]);
}
//print $str_flg_fav; exit;
#----------------------------------------------------------------------------------
//Insert Query
$str_query_insert="";
$str_create_datetime = date("Y-m-d H:i:s");
//print $str_flg_fav; exit;
if($str_flg_fav=="FAV")
{
    $str_query_insert="INSERT INTO ".$STR_DB_TR_TABLE_NAME_FAVORITES."(userpkid,itempkid,date) VALUES (".$int_userpkid.",".$int_itempkid.",'".$str_create_datetime."')";
    ExecuteQuery($str_query_insert);
    #----------------------------------------------------------------------------------------------------
    CloseConnection();
    $response['status'] = 'SUC';
    $response['message'] = $STR_MSG_ACTION_FAVORITED;
    echo json_encode($response);
    return; 
}
else {
 
    $str_query_delete = "";
    $str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_FAVORITES." WHERE itempkid=".$int_itempkid." AND userpkid=".$int_userpkid; 
    ExecuteQuery($str_query_delete);
    #----------------------------------------------------------------------------------------------------
    CloseConnection();
    $response['status'] = 'SUC';
    $response['message'] = $STR_MSG_ACTION_UNFAVORITED;
    echo json_encode($response);
 }
?> 