<?php
/*
File Name  :- newsletter_p.php
Create Date:- APRIL2019
Intially Create By :- 0013
Update History:
*/
#-------------------------------------------------------------------------------
#Include files
include "./../includes/configuration.php";
include "./../includes/lib_common.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_email.php";
include "./../includes/lib_xml.php";
#-------------------------------------------------------------------------------
//print_r($_POST);exit;
$str_db_table_name = "t_nlsubscribe";
?>

<?php
$str_email = "";
if(isset($_POST['email'])){
    $str_email=trim($_POST['email']);
}
//print $str_email;exit; 
//$str_redirect ="email=".urlencode(RemoveQuote($str_email));
/*if( empty($str_email) || validateEmail($str_email)==false )
{
    $response['status']='ERR';
    $response['message']= "Invalid Email ID!";
    echo json_encode($response); 
    return;
}*/
if( empty($str_email) || validateEmail($str_email)==false )
{
    $response['status']='ERR';
    $response['message']= "Invalid Email ID";
    echo json_encode($response); 
    return;
}
#----------------------------------------------------------------------------------------------------
#check for unique email address
$str_query_select = "";
$str_query_select = "SELECT emailaddress FROM " .$str_db_table_name. " WHERE emailaddress = '".$str_email."'";
$rs_list_check_duplicate = GetRecordSet($str_query_select);
if($rs_list_check_duplicate->eof() !=true)
{
    $response['status']='ERR';
    $response['message']= "Email address already exist, please try with another";
    echo json_encode($response); 
    return;    
}
#----------------------------------------------------------------------------------------------------

$str_query_select="SELECT * FROM " .$str_db_table_name. " WHERE emailaddress='" . ReplaceQuote($str_email) . "'";
//print $str_query_select;exit;
$rs_check=GetRecordset($str_query_select);
if (!$rs_check->EOF())
{
    if($rs_check->Fields("subscribe")=="NO")
    {
        $str_query_select="UPDATE " .$str_db_table_name. " SET subscribe='YES',catpkid='1',registrationdate='".date('Y-m-d')."' WHERE emailaddress='".ReplaceQuote($str_email)."'";
        ExecuteQuery($str_query_select);
    }
}
else
{
    $str_query_insert="INSERT INTO " .$str_db_table_name. "(catpkid,registrationdate,emailaddress,unsubscribedate,subscribe)";
    $str_query_insert .= " VALUES ('1','" . date('Y-m-d') . "','" . ReplaceQuote($str_email) . "','" . date('Y-m-d') . "','YES')";
//    print $str_query_insert;
    ExecuteQuery($str_query_insert);

    /*$str_sub_subject="Mail from " . $STR_SITE_URL . " Newsletter SignUp Page for News Letter";
    $sub_mail_body="User has sent NewsLetter subscription request from Newsletter SignUp section of " . $STR_SITE_URL . "<br>Followings are details:<br><br>Email Address: " . $str_email;

    $fp=openXMLfile($UPLOAD_XML_PATH."siteconfiguration.xml");
    $str_sub_from=getTagValue($STR_FROM_DEFAULT,$fp);
    $str_sub_to=getTagValue($STR_NEWS_LETTER_SUBSCRIBED_NOTIFY_EMAILID,$fp);
    closeXMLfile($fp);

    sendmail($str_sub_to,$str_sub_subject,$sub_mail_body,$str_sub_from,1);*/
}	
/*CloseConnection();
Redirect("./user/footer.php?msg=A&type=S");
exit(); */
$response['status']='SUC';
$response['message']="Your email address subscribed successfully.";
echo json_encode($response);
return;
?>