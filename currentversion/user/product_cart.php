<?php
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
//print "HELLO"; exit;
#----------------------------------------------------------------------
#Include files
//session_start();
include "../user/product_validatesession.php";
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_xml.php";
include "../includes/lib_file_upload.php";
include "../includes/lib_image.php";
include "../includes/lib_datetimeyear.php";
include "../user/product_config.php";
//include "./../includes/http_to_https.php";

#----------------------------------------------------------------------
$str_title_page = "";
$str_title_page = $STR_TITLE_PAGE_CART;
//print $str_title_page; exit;


$str_xml_file_name_cms = "";
$str_xml_file_name_cms = "store_cms.xml";

#------------------------------------------------------------------------------------------------
/*$int_userpkid = 0;
$int_userpkid = $_SESSION["userpkid"];

$str_username = "";
$str_username = $_SESSION["userid"];
*/
$int_prodpkid = "";
if (isset($_GET["pkid"])) {
    $int_prodpkid = RemoveQuote(trim($_GET["pkid"]));
}
/*if($int_prodpkid=="" || !is_numeric($int_prodpkid) || $int_prodpkid<=0)
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/

// PRODUCT CATEGORY PKID
$int_cpkid = "";
if (isset($_GET["catpkid"])) {
    $int_cpkid = RemoveQuote(trim($_GET["catpkid"]));
}
/*if($int_cpkid=="" || !is_numeric($int_cpkid) || $int_cpkid<=0)
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/

// PRICE PKID
/*$int_pricepkid="";
if(isset($_GET["pripkid"]))
{
        $int_pricepkid=RemoveQuote(trim($_GET["pripkid"]));	
}

if($int_pricepkid=="" || !is_numeric($int_pricepkid))
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/

// PRICE VALUE
$int_priceval = "";
if (isset($_GET["prival"])) {
    $int_priceval = RemoveQuote(trim($_GET["prival"]));
}
/*if($int_priceval=="" || !is_numeric($int_priceval))
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/

// SHIPPING VALUE
$int_shippingval = "";
if (isset($_GET["shippingval"])) {
    $int_shippingval = RemoveQuote(trim($_GET["shippingval"]));
}
/*if($int_shippingval=="" || !is_numeric($int_shippingval))
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/

// PRODUCT TITLE
$str_title = "";
if (isset($_GET["title"])) {
    $str_title = RemoveQuote(trim($_GET["title"]));
}
/*if($str_title=="")
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/

// PRODUCT CATEGORY TITLE
$str_ctitle = "";
if (isset($_GET["ctitle"])) {
    $str_ctitle = RemoveQuote(trim($_GET["ctitle"]));
}
/*if($str_ctitle=="")
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/

$str_color = "";
$str_size = "";
if (isset($_GET["color"])) {
    $str_color = trim($_GET["color"]);
}
if (isset($_GET["size"])) {
    $str_size = trim($_GET["size"]);
}
$str_type = "";
if (isset($_POST["os2"])) {
    $str_type = trim($_GET["os2"]);
}

/*$int_modelpkid=0;
$str_modelname="";
if (isset($_GET["mid"]))
{
        $int_modelpkid= trim($_GET["mid"]);
}
if (isset($_GET["mname"]))
{
        $str_modelname= trim($_GET["mname"]);
}*/
#------------------------------------------------------------------------------------------------

# Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
# Get message type.
if (isset($_GET["type"])) {
    switch (trim($_GET["type"])) {
        case ("S"):
            $str_type = "S";
            break;
        case ("E"):
            $str_type = "E";
            break;
        case ("W"):
            $str_type = "W";
            break;
    }
}
#	Get message text.

if (isset($_GET["msg"])) {
    switch (trim($_GET["msg"])) {
        case ("F"):
            $str_message = $STR_MSG_ACTION_INFO_MISSING;
            break;
        case ("S"):
            $str_message = $STR_MSG_ACTION_ADD;
            break;
        case ("D"):
            $str_message = $STR_MSG_ACTION_DELETE;
            break;
        case ("U"):
            $str_message = $STR_MSG_ACTION_EDIT;
            break;
        case ("O"):
            $str_message = $STR_MSG_ACTION_DISPLAY_ORDER;
            break;
    }
}


$item_sessionid = "";
$item_uniqueid = "";

if (isset($_SESSION['sessionid'])) {
    $item_sessionid = $_SESSION['sessionid'];
}
if (isset($_SESSION['uniqueid'])) {
    $item_uniqueid = $_SESSION['uniqueid'];
}

if ($item_sessionid == "" || $item_uniqueid == "") {
    CloseConnection();
    Redirect("../online_store");
    exit();
}

$str_member_flag = false;
$int_userpkid = 0;
$str_username = "";
$str_where = "";
if (isset($_SESSION['userpkid']) && $_SESSION['userpkid'] != "") {
    $str_member_flag = true;
    $int_userpkid = $_SESSION['userpkid'];
    $str_username = $_SESSION['username'];
}

$str_allow_promocode = "NO";
$str_allow_wholesaler_discount = "NO";
$str_user_type = "";
if ($int_userpkid > 0) {
    $str_query_select = "";
    $str_query_select = "SELECT isusertype FROM " . $STR_DB_TABLE_NAME_USER . " WHERE pkid=" . $int_userpkid;
    $rs_list_user = GetRecordSet($str_query_select);
    if (!$rs_list_user->EOF()) {
        if ($rs_list_user->Fields("isusertype") == "REGULAR") {
            $str_allow_promocode = "YES";
            $str_allow_wholesaler_discount = "NO";
        }

        if ($rs_list_user->Fields("isusertype") == "WHOLESALER") {
            $str_allow_promocode = "NO";
            $str_allow_wholesaler_discount = "YES";
            $str_user_type = "WHOLESALER";
        }
    }
    //$str_where = " WHERE userpkid = ".$int_userpkid." ";
}

if (isset($_COOKIE["sessionid"]) && isset($_COOKIE["uniqueid"]) && $_COOKIE["sessionid"] != "" && $_COOKIE["uniqueid"] != "") {
    $str_where = " WHERE sessionid='" . $_COOKIE["sessionid"] . "' AND uniqueid='" . $_COOKIE["uniqueid"] . "' ";
} else {
    $str_where = " WHERE sessionid='" . $item_sessionid . "' AND uniqueid='" . $item_uniqueid . "' ";
}
//print $str_allow_promocode;
//print_r($_SESSION);
//print_r($_COOKIE);

$int_discount_amount = 0;
#----------------------------------------------------------------------
#read main xml file
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS . $str_xml_file_name_cms);
$str_desc_cms = "";
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION3", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE3", $fp);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE); ?> : <?php print($str_title_page); ?></title>
    <?php //print(Display_Page_Metatag($str_title_page_metatag)); 
    ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
</head>

<body>
    <?php include $STR_USER_HEADER_PATH; ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="row padding-10">
                    <div class="col-md-12">
                        <h1 align="right"><?php print $str_title_page; ?></h1>
                        <hr />
                    </div>
                </div>
                <?php if ($str_visible_cms == "YES") { ?>
                    <?php if ($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
                        <div class="row padding-10">
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <div class="well">
                                    <p align="justify"><?php print($str_desc_cms); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
                <?php if ($str_type != "" && $str_message != "") {
                    print(DisplayMessage(0, $str_message, $str_type));
                } ?>
                <?php
                $var_gift_sku = "FALSE";
                $var_promo_sku = "FALSE";
                $var_extprice = 0;
                $hid_options = "";
                $hid_serialno = "";

                # Below recordset fetches all purchased related data from table for particular table.
                $str_query_select = "";
                $str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME_SESSION_CART . " " . $str_where . " ORDER BY serialno,sku,producttitle";

                // print $str_query_select; 
                $rs_list = GetRecordSet($str_query_select); ?>
                <?php
                $int_subtotal = 0.00;
                $int_promocode_discount_amount = 0.00;
                if ($rs_list->EOF() == true) { ?>
                    <div class="row padding-10">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 alert alert-danger">
                            <div class="">
                                <p align="center">No items added into your shopping cart</p>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <?php
                    $int_highest_shipping_value = 0;
                    $int_wholesaler_discount = 0;
                    $int_total_shipping_price = 0;

                    if ($rs_list->EOF() == false) {
                        while (!$rs_list->EOF()) {
                            // Use below equation if want to update shipping charge according to no. of quantity for each item
                            //$int_highest_shipping_value = $int_highest_shipping_value + ($cartrs->fields("quantity") * $cartrs->fields("shippingvalue"));
                            //print $cartrs->fields("shippingvalue")."<br/>";
                            $int_wholesaler_discount = $rs_list->fields("wholesalerdiscount");
                            $int_highest_shipping_value = $int_highest_shipping_value + $rs_list->fields("shippingvalue");
                            $rs_list->MoveNext();
                        }
                        $rs_list->MoveFirst();
                        //print $int_wholesaler_discount;
                    } ?>
                    <div class="row padding-10">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <div class="row padding-10">
                                <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                                    <?php
                                    $int_cnt = 0;

                                    while (!$rs_list->EOF() == true) {
                                        $var_cnt = 0;
                                        $var_extprice = $var_extprice + $rs_list->fields("extendedprice");

                                        //print $var_extprice;
                                        if (substr($rs_list->fields("sku"), 0, 4) == "GIFT") {
                                            $hid_options = $hid_options . $cartrs->fields("serialno") . "_cmb_";
                                            $var_gift_sku = "TRUE";
                                        } else {
                                            $var_gift_sku = "FALSE";
                                        }

                                        if (substr($rs_list->fields("sku"), 0, 9) == "PromoItem") {
                                            $var_promo_sku = "TRUE";
                                        } else {
                                            $var_promo_sku = "FALSE";
                                        }
                                        $int_pkid = 0;
                                        $int_pkid = GetEncryptId($rs_list->Fields("productpkid")); ?>

                                        <div class="thumbnail">
                                            <div class="row padding-10">
                                                <?php
                                                $str_query_select = "";
                                                $str_query_select = "SELECT a.thumbphotofilename, a.imageurl, b.listprice FROM " . $STR_DB_TABLE_NAME_PHOTO . " a LEFT JOIN " . $STR_DB_TABLE_NAME . " b ON a.masterpkid=b.pkid AND b.approved='YES' AND b.visible='YES' WHERE a.masterpkid=" . $rs_list->Fields("productpkid") . " AND a.visible='YES' AND a.setasfront='YES'";
                                                $rs_list_image = GetRecordSet($str_query_select); ?>
                                                <div class="col-md-2 col-sm-2 col-xs-12">
                                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("productpkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle"))))); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle"))))); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("producttitle"))))); ?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>">
                                                        <?php if ($rs_list_image->Fields("thumbphotofilename") != "") { ?>
                                                            <img src="<?php print $UPLOAD_IMG_PATH . $rs_list->Fields("productpkid") . "/" . $rs_list_image->Fields("thumbphotofilename"); ?>" class="img-responsive" alt="<?php print $rs_list->Fields("producttitle"); ?>" title="<?php print $rs_list->Fields("producttitle"); ?>" />
                                                        <?php } else if ($rs_list_image->Fields("imageurl") != "") { ?>
                                                            <img src="<?php print $rs_list_image->Fields("imageurl"); ?>" class="img-responsive" alt="<?php print $rs_list->Fields("producttitle"); ?>" title="<?php print $rs_list->Fields("producttitle"); ?>" />
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                                <div class="col-md-8 col-lg-8 col-sm-7 col-xs-12">
                                                    <span id="ret<?php print $rs_list->Fields("serialno"); ?>"></span>
                                                    <h4><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("productpkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle"))))); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle"))))); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("producttitle"))))); ?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>"><?php print MakeStringShort($rs_list->Fields("producttitle"), 50); ?></a></h4>
                                                    <ul>
                                                        <?php if ($rs_list->Fields("cattitle") != "") { ?>
                                                            <li><span class="text-muted">Category</span> - <b><?php print $rs_list->Fields("cattitle"); ?></b></li>
                                                        <?php } ?>
                                                        <?php if ($rs_list->Fields("subcattitle") != "") { ?>
                                                            <li><span class="text-muted">Sub Category</span> - <b><?php print $rs_list->Fields("subcattitle"); ?></b></li>
                                                        <?php } ?>
                                                        <li>
                                                            <span class="text-muted">Per Unit Price</span> - <b class="text-success"><?php print $str_currency_symbol; ?>&nbsp;<?php
                                                                                                                                                                                $int_per_unit_price = 0;
                                                                                                                                                                                $int_per_unit_price = ceil($rs_list->Fields("price") / $int_conversionrate);
                                                                                                                                                                                print number_format($int_per_unit_price, 2);
                                                                                                                                                                                ?></b>
                                                            <?php

                                                            if ($str_user_type == "" && $rs_list_image->Fields("listprice") > 0) { ?>
                                                                &nbsp;&nbsp;
                                                                <strike><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format(ceil($rs_list_image->Fields("listprice") / $int_conversionrate), 2); ?></strike><?php } ?>
                                                        </li>

                                                        <?php
                                                        $int_tailoring_price = 0.00;

                                                        if ($rs_list->Fields("tailoringprice") > 0) {

                                                            $int_tailoring_price = ceil($rs_list->Fields("tailoringprice") / $int_conversionrate); ?>

                                                            <li><span class="text-muted">Per Unit Tailoring Price</span> - <b class="text-success"><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_tailoring_price, 2); ?></b></li>
                                                        <?php } else {
                                                            $int_tailoring_price = 0.00;
                                                        } ?>

                                                        <?php
                                                        /*$int_shipping_price = 0; 
                                                        $int_shipping_price = (($rs_list->Fields("weight") * $int_courier_price_per_kg) / $int_conversionrate) * $rs_list->Fields("quantity"); 
							*/
                                                        $int_total_shipping_price = (($rs_list->Fields("weight") * $int_courier_price_per_kg) / $int_conversionrate) * $rs_list->Fields("quantity");
                                                        // $int_total_shipping_price = $int_total_shipping_price + $rs_list->Fields("shippingvalue");

                                                        if ($rs_list->Fields("shippingvalue") > 0) {
                                                        ?>
                                                            <li><span class="text-muted">Shipping Price</span> - <b class="text-success"><?php print $str_currency_symbol; ?>&nbsp;<?php print $int_total_shipping_price; ?></b></li>
                                                        <?php } ?>

                                                        <?php if ($rs_list->Fields("color") != "") { ?>
                                                            <li><span class="text-muted">Color</span> - <b><?php print $rs_list->Fields("color"); ?></b></li>
                                                        <?php } ?>
                                                        <?php if ($rs_list->Fields("size") != "") { ?>
                                                            <li><span class="text-muted">Size</span> - <b><?php print $rs_list->Fields("size"); ?></b></li>
                                                        <?php } ?>

                                                    </ul>
                                                </div>
                                                <div class="col-md-2 col-lg-2 col-sm-3 col-xs-12">
                                                    <?php $int_extended_total = 0; ?>
                                                    <?php
                                                    //$int_extended_total = (($int_per_unit_price + $int_tailoring_price) * $rs_list->Fields("quantity")) + $rs_list->Fields("shippingvalue"); 
                                                    $int_extended_total = ($int_per_unit_price + $int_tailoring_price) * $rs_list->Fields("quantity");
                                                    ?>
                                                    <span id="extended_price<?php print $int_cnt; ?>" style="display:none;"><?php // print number_format($int_extended_total,2);  
                                                                                                                            ?></span>
                                                    <span id="hdn_extended_price<?php print $int_cnt; ?>" style="display:none;"><?php // print $int_extended_total; 
                                                                                                                                ?></span>
                                                    <h4 class="text-success text-right"><b><?php print $str_currency_symbol . "&nbsp;"; ?><?php print number_format($int_extended_total, 2);  ?></b></h4>

                                                    <form id="frm_cart<?php print $int_cnt; ?>" name="frm_cart<?php print $int_cnt; ?>" method="POST" action="product_cart_p.php">
                                                        <div class="form-group input-group">
                                                            <span class="input-group-btn"><button class="btn btn-default" type="button" onclick="return QtyDecrement(<?php print $int_cnt; ?>);"><i class="fa fa-minus"></i></button></span>
                                                            <input type="text" id="txt_quantity<?php print $int_cnt; ?>" name="txt_quantity<?php print $int_cnt; ?>" class="form-control text-center" value="<?php print $rs_list->Fields("quantity"); ?>" readonly />
                                                            <span class="input-group-btn"><button class="btn btn-default" type="button" onclick="return QtyIncrement(<?php print $int_cnt; ?>);"><i class="fa fa-plus"></i></button></span>
                                                        </div>

                                                        <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $rs_list->Fields("price"); ?>" />
                                                        <input type="hidden" id="hdn_weight" name="hdn_weight" value="<?php print $rs_list->Fields("weight"); ?>" />
                                                        <input type="hidden" id="hdn_courier_price_per_kg" name="hdn_courier_price_per_kg" value="<?php print $int_courier_price_per_kg; ?>" />
                                                        <input type="hidden" id="hdn_conversionrate" name="hdn_conversionrate" value="<?php print $int_conversionrate; ?>" />
                                                        <input type="hidden" id="hdn_tailoring_price" name="hdn_tailoring_price" value="<?php print $rs_list->Fields("tailoringprice"); ?>" />
                                                        <input type="hidden" id="hdn_cnt" name="hdn_cnt" value="<?php print $int_cnt; ?>" />
                                                        <input type="hidden" id="hdn_serialno" name="hdn_serialno" value="<?php print $rs_list->Fields("serialno"); ?>" />
                                                    </form>
                                                </div>
                                            </div>
                                            <div id="success<?php print $int_cnt; ?>"></div>
                                            <div class="row padding-10">
                                                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                                    <a href="product_cart_del_p.php?sr_no=<?php print $rs_list->Fields("serialno"); ?>" title="Click to remove this item from cart">
                                                        <div class="text-center btn-cart">
                                                            <i class="fa fa-remove"></i>&nbsp;REMOVE
                                                        </div>
                                                    </a>
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                                    <?php
                                                    ## --------------------------------------------- START : WISHLIST--------------------------------------
                                                    if ($str_member_flag) {
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=" . $_SESSION["userpkid"] . " AND itempkid=" . $rs_list->fields("productpkid");
                                                        $rs_list_fav = GetRecordSet($str_query_select);

                                                        $str_fav_class = "";
                                                        $str_fav_flag = "";
                                                        if ($rs_list_fav->Count() == 0) {
                                                            $str_fav_class = "";
                                                            $str_fav_flag = "FAV";
                                                        } else if ($rs_list_fav->Count() > 0) {
                                                            $str_fav_class = "text-primary";
                                                            $str_fav_flag = "UNFAV";
                                                        } ?>
                                                        <form name="frm_user_favorite<?php print $int_cnt; ?>" id="frm_user_favorite<?php print $int_cnt; ?>" novalidate>

                                                            <input type="hidden" name="userpkid" id="userpkid<?php print($int_cnt); ?>" value="<?php print($_SESSION["userpkid"]); ?>">
                                                            <input type="hidden" name="itempkid" id="itempkid<?php print($int_cnt); ?>" value="<?php print($rs_list->fields("productpkid")) ?>">

                                                            <input type="hidden" name="flgfav" id="flgfav<?php print($int_cnt) ?>" value="<?php print($str_fav_flag); ?>">



                                                            <?php //print $rs_list_fav->Count(); 
                                                            ?>
                                                            <button id="frm_submit<?php print($int_cnt); ?>" name="frm_submit<?php print($int_cnt); ?>" type="submit" style="display: none;"></button>
                                                        </form>
                                                        <a onclick="$('#frm_submit<?php print($int_cnt); ?>').click();" class="">
                                                            <div class="text-center btn-cart">
                                                                <span class="<?php print $str_fav_class; ?>"><i class="fa fa-heart "></i>&nbsp;
                                                                    <?php if ($str_fav_flag == 'FAV') {
                                                                        print "Add To Favorite List";
                                                                    } else if ($str_fav_flag == 'UNFAV') {
                                                                        print "Remove From Favorite List";
                                                                    } ?>
                                                                </span>
                                                            </div>
                                                        </a>
                                                        <script>
                                                            $(function() {
                                                                $("#frm_user_favorite<?php print $int_cnt; ?> input").jqBootstrapValidation({
                                                                    preventSubmit: true,
                                                                    submitError: function($form, event, errors) {
                                                                        // something to have when submit produces an error ?
                                                                        // Not decided if I need it yet
                                                                    },
                                                                    submitSuccess: function($form, event) {
                                                                        event.preventDefault(); // prevent default submit behaviour
                                                                        // get values from FORM

                                                                        var userpkid = $("input#userpkid<?php print($int_cnt); ?>").val();
                                                                        //alert(userpkid);
                                                                        var itempkid = $("input#itempkid<?php print($int_cnt); ?>").val();
                                                                        var flgfav = $("input#flgfav<?php print($int_cnt); ?>").val();


                                                                        $.ajax({

                                                                            url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                                                            type: "POST",
                                                                            data: {
                                                                                userpkid: userpkid,
                                                                                itempkid: itempkid,
                                                                                flgfav: flgfav
                                                                            },
                                                                            cache: false,
                                                                            success: function(data) {
                                                                                //alert(data);
                                                                                var $responseText = JSON.parse(data);
                                                                                if ($responseText.status == 'SUC') {
                                                                                    // Success message
                                                                                    $('#success<?php print $int_cnt; ?>').html("<div class='alert alert-success text-center'>");
                                                                                    $('#success<?php print $int_cnt; ?> > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                                        .append("</button>");
                                                                                    $('#success<?php print $int_cnt; ?> > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                                                    $('#success<?php print $int_cnt; ?> > .alert-success ').append('</div>');
                                                                                    //setTimeout(function(){ location.reload() }, 2000);
                                                                                    setTimeout(function() {
                                                                                        location.reload();
                                                                                    }, 5000);
                                                                                } else if ($responseText.status == 'ERR') {
                                                                                    // Fail message
                                                                                    $('#success<?php print $int_cnt; ?>').html("<div class='alert alert-danger'>");
                                                                                    $('#success<?php print $int_cnt; ?> > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                                        .append("</button>");
                                                                                    $('#success<?php print $int_cnt; ?> > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                                                    $('#success<?php print $int_cnt; ?> > .alert-danger').append('</div>');

                                                                                }
                                                                            },

                                                                        })
                                                                    },
                                                                    filter: function() {
                                                                        return $(this).is(":visible");
                                                                    },
                                                                });

                                                                $("a[data-toggle=\"tab\"]").click(function(e) {
                                                                    e.preventDefault();
                                                                    $(this).tab("show");
                                                                });
                                                            });
                                                            /*When clicking on Full hide fail/success boxes */
                                                            $('#name').focus(function() {
                                                                $('#success').html('');
                                                            });
                                                        </script>
                                                    <?php } else { ?>
                                                        <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>">
                                                            <div class="text-center btn-cart">
                                                                <span class="<?php //print $str_fav_class; 
                                                                                ?>"><i class="fa fa-heart "></i>&nbsp;Add To Favorite</span>
                                                            </div>
                                                        </a>
                                                    <?php }
                                                    ## --------------------------------------------- END : WISHLIST------------------------------------------ 
                                                    ?>
                                                </div>

                                            </div>
                                        </div>

                                    <?php
                                        $int_cnt++;
                                        $int_subtotal = $int_subtotal + $int_extended_total;
                                        $int_promocode_discount_amount = $int_promocode_discount_amount + $rs_list->Fields("finaldiscountedamount");
                                        $rs_list->MoveNext();
                                    } ?>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="count-bg">
                                                <p>Cart Total:</p>
                                                <hr />

                                                <h4 align="right"><span class="text-muted">Item Total</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="text-danger"><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_subtotal, 2); ?></span></h4>
                                                <hr />
                                                <h4 align="right"><span class="text-muted">Shipping Total</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="text-danger"><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_total_shipping_price, 2); ?></span></h4>
                                                <hr />
                                                <?php
                                                if ($int_wholesaler_discount > 0 && $str_allow_wholesaler_discount == "YES") {
                                                    $int_discount_amount = ($int_subtotal * $int_wholesaler_discount) / 100;

                                                ?>
                                                    <h4 align="right"><span class="text-muted">- Wholesaler Discount</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="text-danger"><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_discount_amount, 2); ?></span></h4>
                                                    <hr />
                                                <?php } ?>
                                                <h3 align="right">Total Pay : <b class="text-success"><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_subtotal + $int_total_shipping_price - $int_discount_amount, 2); ?></b></h3>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-12 col-sm-6 col-xs-12">

                                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_address.php" type="submit" title="Click to proceed" class="btn btn-primary btn-lg btn-block"><i class="fa fa-check"></i>&nbsp;<b>PROCEED TO CHECKOUT</b></a>

                                        </div>
                                        <?php /* ?><div class="col-md-12 col-lg-12 col-sm-6 col-xs-12">
                                        <a href="" title="" class="btn btn-primary btn-block"><b><i class="fa fa-inr"></i>&nbsp;<?php print number_format($int_subtotal,2); ?> /  PLACE ORDER</b></a>
                                    </div><?php */ ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div> <br />
    </div>
    <div class="container-fluid">
        <div class="row padding-10">
            <div class="price-and-checkout-btn-panel hidden-lg hidden-md">
                <div class="col-xs-12 text-center nopadding">
                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_address.php">
                        <div class="buy-now-btn">
                            <?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_subtotal - $int_discount_amount, 2) . " " . $str_currency_shortform; ?> / PROCEED TO CHECKOUT
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>



    <div class="footer-bottom-margin">
        <?php include $STR_USER_FOOTER_PATH;
        CloseConnection(); ?>
    </div>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_cart_promocode.js"></script>
    <script language="JavaScript" type="text/javascript">
        /*function formvalidation(arg)
    {
        
            var str_serialno=document.frmupdatecart.hid_serialno.value;
            //alert(str_serialno);
            var sr1=(str_serialno.split(";"));
            for(sr_count=0;sr_count<sr1.length-1;sr_count++)
            {
                var i=sr1[sr_count];
                if(!isDigit(eval('document.frmupdatecart.'+i).value))
                {
                    alert('Please enter positive numeric value for Quantity');
                    eval('document.frmupdatecart.'+i).select();
                    eval('document.frmupdatecart.'+i).focus();
                    return false;
                }
            }
            //updatecart('./store_item_updatecart_p.php?mid=<?php //print $int_userpkid; 
                                                            ?>&mname=<?php //print $str_username;  
                                                                        ?>&id='+arg);
            updatecart('../user/store_updatecart_p.php?id='+arg);
    }
    function updatecart(url1)
    {
            document.frmupdatecart.action=url1;
            document.frmupdatecart.method="post";
            document.frmupdatecart.submit();
    }


    function applypromocodeform(arg)
    {
            applypromocode('../user/store_promocode_p.php?id='+arg);
    }
    function applypromocode(url2)
    {
            document.frmupdatecart.action=url2;
            document.frmupdatecart.method="post";
            document.frmupdatecart.submit();
    }
    
    function checkccbillform()
    {
        with(document.ccbillform)
        {
           if(trim(ta_ship_add.value)=="")
            {
                alert("Complete 'Required' field to continue.");
                ta_ship_add.select();
                ta_ship_add.focus();	
                return false;
            }
        }
        return true;
    }
    */
        function QtyIncrement(i) {
            var quantity = $('#txt_quantity' + i).val();
            //alert(quantity);
            var remainingqty = parseInt(quantity) + 1;
            //alert(remainingqty);
            var txt_qty = document.getElementById('txt_quantity' + i);
            txt_qty.value = remainingqty;

            var extended_price = parseFloat(parseFloat(document.getElementById('hdn_extended_price' + i).innerHTML).toFixed(2) * txt_qty.value).toFixed(2);
            document.getElementById('extended_price' + i).innerHTML = extended_price;

            $('#frm_cart' + i).submit();
        }

        function QtyDecrement(i) {
            //alert($('#txt_quantity').val());
            var quantity = $('#txt_quantity' + i).val();
            var remainingqty = parseInt(quantity) - 1;
            //alert(remainingqty);
            var txt_qty = document.getElementById('txt_quantity' + i);
            if (remainingqty == 0) {
                txt_qty.value = 1;
            } else {
                txt_qty.value = remainingqty;
            }
            var extended_price = parseFloat(parseFloat(document.getElementById('hdn_extended_price' + i).innerHTML).toFixed(2) * txt_qty.value).toFixed(2);
            document.getElementById('extended_price' + i).innerHTML = extended_price;

            $('#frm_cart' + i).submit();
        }
    </script>
</body>

</html>