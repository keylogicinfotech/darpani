<?php 
$STR_TITLE_PAGE = "Products";
$STR_TITLE_PAGE_ORDER_STATUS = "Order List";
$STR_TITLE_PAGE_PHOTO_LIST = "Photo List";
$STR_TITLE_PAGE_CART = "Cart";
$STR_TITLE_PAGE_ADDRESS = "Shipping Address";
$STR_TITLE_PAGE_PAYMENT_METHOD = "Payment Options";
$STR_TITLE_PAGE_PURCHASE_PAYMENT_LOCAL = "Payment Purchase";
$STR_TITLE_PAGE_CHECKOUT_MESSAGE = "Checkout Message";
$STR_TITLE_PAGE_ORDER_MESSAGE = "Message";

$UPLOAD_IMG_PATH = $STR_SITENAME_WITH_PROTOCOL."/mdm/store/";
$UPLOAD_IMG_PATH_CAT_SUBCAT = $STR_SITENAME_WITH_PROTOCOL."/mdm/storecategory/";;
$UPLOAD_IMG_PATH_MEASUREMENT_CHART = $STR_SITENAME_WITH_PROTOCOL."/mdm/measurechart/";
$UPLOAD_IMG_PATH_ADDITIONAL_INFO = $STR_SITENAME_WITH_PROTOCOL."/mdm/storeadditionalinfo/";
$UPLOAD_IMG_PATH_PAYMENT_OPTIONS = $STR_SITENAME_WITH_PROTOCOL."/mdm/paymentoptions/";
$INT_IMG_WIDTH_LARGE = 900;	
$INT_IMG_WIDTH_THUMB = 300; 

$INT_RECORD_PER_PAGE = 30;
$INT_DAYS_DISPLAY_AS_NEW = 3;

# For Main List
global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "t_store"; 
$STR_DB_TABLE_NAME_ORDER_BY = "";

global $STR_DB_TABLE_NAME_PURCHASE;
global $STR_DB_TABLE_NAME_ORDER_BY_PURCHASE;
$STR_DB_TABLE_NAME_PURCHASE = "t_store_purchase"; 
$STR_DB_TABLE_NAME_ORDER_BY_PURCHASE = " ORDER BY purchasedatetime DESC ";


# For Additional Information
global $STR_DB_TABLE_NAME_ADDITIONAL_INFO;
global $STR_DB_TABLE_NAME_ADDITIONAL_INFO_ORDER_BY;
$STR_DB_TABLE_NAME_ADDITIONAL_INFO = "t_store_additional_info"; 
$STR_DB_TABLE_NAME_ADDITIONAL_INFO_ORDER_BY = " ORDER BY displayorder DESC";

# For Color
global $STR_DB_TABLE_NAME_COLOR;
global $STR_DB_TABLE_NAME_ORDER_BY_COLOR;
$STR_DB_TABLE_NAME_COLOR = "t_store_color"; 
$STR_DB_TABLE_NAME_ORDER_BY_COLOR = " ORDER BY title ASC ";

global $STR_DB_TR_TABLE_NAME_COLOR;
$STR_DB_TR_TABLE_NAME_COLOR = "tr_store_color";  


## For country
global $STR_DB_TABLE_NAME_COUNTRY;
$STR_DB_TABLE_NAME_COUNTRY = "t_site_country";  

## For state
global $STR_DB_TABLE_NAME_STATE;
$STR_DB_TABLE_NAME_STATE = "t_state";


## For Pincode
global $STR_DB_TABLE_NAME_PINCODE;
$STR_DB_TABLE_NAME_PINCODE = "t_store_pincode";


## For Payment Options
global $STR_DB_TABLE_NAME_PAYMENT_OPTION;
$STR_DB_TABLE_NAME_PAYMENT_OPTION = "t_paymentoptions";


# For Tailoring Services    
global $STR_DB_TABLE_NAME_TAILORING_SERVICE;
global $STR_DB_TABLE_NAME_ORDER_BY_TAILORING_SERVICE;
$STR_DB_TABLE_NAME_TAILORING_SERVICE = "t_store_tailoringservice"; 
$STR_DB_TABLE_NAME_ORDER_BY_TAILORING_SERVICE = " ORDER BY title ASC ";

global $STR_DB_TR_TABLE_NAME_TAILORING_SERVICE;
global $STR_DB_TR_TABLE_NAME_ORDER_BY_TAILORING_SERVICE;
$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE = "tr_store_tailoringservice"; 
$STR_DB_TR_TABLE_NAME_ORDER_BY_TAILORING_SERVICE = "";

# For Tailoring Option    
global $STR_DB_TABLE_NAME_TAILORING_OPTION;
global $STR_DB_TABLE_NAME_ORDER_BY_TAILORING_OPTION;
$STR_DB_TABLE_NAME_TAILORING_OPTION = "t_store_tailoringoption"; 
$STR_DB_TABLE_NAME_ORDER_BY_TAILORING_OPTION = " ORDER BY title ASC ";

global $STR_DB_TR_TABLE_NAME_TAILORING_OPTION;
global $STR_DB_TR_TABLE_NAME_ORDER_BY_TAILORING_OPTION;
$STR_DB_TR_TABLE_NAME_TAILORING_OPTION = "tr_store_tailoringoption"; 
$STR_DB_TR_TABLE_NAME_ORDER_BY_TAILORING_OPTION = "";


global $STR_DB_TR_TABLE_NAME_CATWISE_TAILORING_OPTION;
$STR_DB_TR_TABLE_NAME_CATWISE_TAILORING_OPTION = "tr_store_catwise_tailoringoption"; 


# For Size
global $STR_DB_TABLE_NAME_SIZE;
global $STR_DB_TABLE_NAME_ORDER_BY_SIZE;
$STR_DB_TABLE_NAME_SIZE = "t_store_size"; 
$STR_DB_TABLE_NAME_ORDER_BY_SIZE = " ORDER BY title ASC ";

global $STR_DB_TR_TABLE_NAME_SIZE;
$STR_DB_TR_TABLE_NAME_SIZE = "tr_store_size"; 


# For Type
global $STR_DB_TABLE_NAME_TYPE;
global $STR_DB_TABLE_NAME_ORDER_BY_TYPE;
$STR_DB_TABLE_NAME_TYPE = "t_store_type"; 
$STR_DB_TABLE_NAME_ORDER_BY_TYPE = " ORDER BY title ASC ";

global $STR_DB_TR_TABLE_NAME_TYPE;
global $STR_DB_TR_TABLE_NAME_ORDER_BY_TYPE;
$STR_DB_TR_TABLE_NAME_TYPE = "tr_store_type"; 
$STR_DB_TR_TABLE_NAME_ORDER_BY_TYPE = "";

# For Work
global $STR_DB_TABLE_NAME_WORK;
global $STR_DB_TABLE_NAME_ORDER_BY_WORK;
$STR_DB_TABLE_NAME_WORK = "t_store_work"; 
$STR_DB_TABLE_NAME_ORDER_BY_WORK = " ORDER BY title ASC ";

global $STR_DB_TR_TABLE_NAME_WORK;
global $STR_DB_TR_TABLE_NAME_ORDER_BY_WORK;
$STR_DB_TR_TABLE_NAME_WORK= "tr_store_work"; 
$STR_DB_TR_TABLE_NAME_ORDER_BY_WORK= "";

# For Fabric
global $STR_DB_TABLE_NAME_FABRIC;
global $STR_DB_TABLE_NAME_ORDER_BY_FABRIC;
$STR_DB_TABLE_NAME_FABRIC = "t_store_fabric"; 
$STR_DB_TABLE_NAME_ORDER_BY_FABRIC = " ORDER BY title ASC ";

global $STR_DB_TR_TABLE_NAME_FABRIC;
global $STR_DB_TR_TABLE_NAME_ORDER_BY_FABRIC;
$STR_DB_TR_TABLE_NAME_FABRIC = "tr_store_fabric"; 
$STR_DB_TR_TABLE_NAME_ORDER_BY_FABRIC = "";

# For Occasion
global $STR_DB_TABLE_NAME_OCCASION;
global $STR_DB_TABLE_NAME_ORDER_BY_OCCASION;
$STR_DB_TABLE_NAME_OCCASION = "t_store_occasion"; 
$STR_DB_TABLE_NAME_ORDER_BY_OCCASION = " ORDER BY title ASC ";

global $STR_DB_TR_TABLE_NAME_OCCASION;
global $STR_DB_TR_TABLE_NAME_ORDER_BY_OCCASION;
$STR_DB_TR_TABLE_NAME_OCCASION= "tr_store_occasion"; 
$STR_DB_TR_TABLE_NAME_ORDER_BY_OCCASION = "";

# For Length
global $STR_DB_TABLE_NAME_LENGTH;
global $STR_DB_TABLE_NAME_ORDER_BY_LENGTH;
$STR_DB_TABLE_NAME_LENGTH = "t_store_length"; 
$STR_DB_TABLE_NAME_ORDER_BY_LENGTH = " ORDER BY title ASC ";

global $STR_DB_TR_TABLE_NAME_LENGTH;
global $STR_DB_TR_TABLE_NAME_ORDER_BY_LENGTH;
$STR_DB_TR_TABLE_NAME_LENGTH = "tr_store_length"; 
$STR_DB_TR_TABLE_NAME_ORDER_BY_LENGTH = "";

# For Shipping
global $STR_DB_TABLE_NAME_SHIPPING;
global $STR_DB_TABLE_NAME_ORDER_BY_SHIPPING;
$STR_DB_TABLE_NAME_SHIPPING = "t_store_shipping"; 
$STR_DB_TABLE_NAME_ORDER_BY_SHIPPING = " ORDER BY title ASC ";

global $STR_DB_TR_TABLE_NAME_SHIPPING;
global $STR_DB_TR_TABLE_NAME_ORDER_BY_SHIPPING;
$STR_DB_TR_TABLE_NAME_SHIPPING = "tr_store_shipping"; 
$STR_DB_TR_TABLE_NAME_ORDER_BY_SHIPPING = " ORDER BY title ASC ";


# For Promocode
global $STR_DB_TABLE_NAME_PROMOCODE;
global $STR_DB_TABLE_NAME_ORDER_BY_PROMOCODE;
$STR_DB_TABLE_NAME_PROMOCODE = "t_store_promocode"; 
$STR_DB_TABLE_NAME_ORDER_BY_PROMOCODE = " ORDER BY displayorder DESC, title ASC ";

#For Category
global $STR_DB_TABLE_NAME_CAT;
global $STR_DB_TABLE_NAME_ORDER_BY_CAT;
$STR_DB_TABLE_NAME_CAT = "t_store_cat";

global $STR_DB_TABLE_NAME_CAT_PHOTO;
$STR_DB_TABLE_NAME_CAT_PHOTO = "tr_store_cat_photo";

global $STR_DB_TABLE_NAME_SUBCAT_PHOTO;
$STR_DB_TABLE_NAME_SUBCAT_PHOTO = "tr_store_subcat_photo";

#For cart
global $STR_DB_TABLE_NAME_SESSION_CART;
$STR_DB_TABLE_NAME_SESSION_CART = "t_store_sessioncart";

#For Sub Category
$STR_DB_TABLE_NAME_SUBCAT = "t_store_subcat"; 
$STR_DB_TABLE_NAME_ORDER_BY_SUBCAT = " ORDER BY subcattitle ASC";

#For Store Photo
global $STR_DB_TABLE_NAME_PHOTO;
global $STR_DB_TABLE_NAME_ORDER_BY_PHOTO;
$STR_DB_TABLE_NAME_PHOTO = "tr_store_photo"; 
$STR_DB_TABLE_NAME_ORDER_BY_PHOTO= " ORDER BY displayorder DESC";

#For User
global $STR_DB_TABLE_NAME_USER;
global $STR_DB_TABLE_NAME_ORDER_BY_USER;
$STR_DB_TABLE_NAME_USER = "t_user";

#For Member
global $STR_DB_TABLE_NAME_MEMBER;
global $STR_DB_TABLE_NAME_ORDER_BY_MEMBER;
$STR_DB_TABLE_NAME_MEMBER = "t_freemember";

global $STR_DB_TABLE_NAME_STORE_VIEW;
$STR_DB_TABLE_NAME_STORE_VIEW = "t_store_item_view";

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "cms_store";


# XML Path
global $XML_FILE_PATH;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/store/";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/store_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";

$INT_BAR_CHART_WIDTH = 100;
$VIEW_BAR_CHART_COLOR = "#A8DBFF";	

$STR_TITLE_DROPDOWN_USER = "-- ALL USERS --";
$STR_TITLE_DROPDOWN_CAT = "-- ALL CATEGORY --";
$STR_TITLE_DROPDOWN_ALPHABET = "-- ALL ALPHABET --";

#Error / Success Messages
$STR_MSG_ACTION_DISCOUNT_POSITIVE = "Error... You can only enter positive numeric value for Discount Percentage and Discount Amount.";
$STR_MSG_ACTION_DISCOUNT_BLANK = "Error... Both Discount Percentage and Discount Amount can not be blank. Please enter any one.";

# Common Messages / Notes / Help displaying on Form
$STR_MSG_COLOR = "Separate multiple colors with pipe | (pipe) e.g. White | Black";
$STR_MSG_SIZE = "Separate multiple sizes with pipe | (pipe) e.g. S | M | L | XL";
$STR_MSG_TYPE = "Separate multiple types with pipe | (pipe) e.g. Type1 | Type2";
$STR_MSG_DOMESTIC_SHIPPING = "Enter 0 (zero) for no shipping charge.";
$STR_MSG_INTERNATIONAL_SHIPPING = "Enter 0 (zero) for no shipping charge.";
$STR_DISPLAY_AS_PURCHASE = "If set YES, item purchase link will be dipaly at user side.";	

$STR_CBO_OPTION1 = "PROCESSING";
$STR_CBO_OPTION2 = "SHIPPED";
$STR_CBO_OPTION3 = "DELIVERED";
$STR_CBO_OPTION4 = "CANCELLED";

$STR_BUTTON_VIEW_DETAILS = "View This Product";
$STR_HOVER_VIEW_DETAILS = "Click To View This Product";

# For Tailoring Options
$STR_TAILORING_OPTION_SEMI_STITCHING_SIZE = "XXS, XS, S, M, L, XL, XXL";
$STR_TAILORING_OPTION_CUSTOMIZED_SLEEVE_STYLE = "As shown on model, Full Sleeves, 3/4 Sleeves, Half Sleeves, Sleeveless";
$STR_TAILORING_OPTION_CUSTOMIZED_AROUND_BUST = "30 inches,31 inches,32 inches,33 inches,34 inches,35 inches,36 inches,37 inches,38 inches,39 inches,40 inches,41 inches,42 inches";


/**
 * Paypal Payment Details
 */
//$PAYPAL_CLIENT_KEY = "AX10VW5f7KPIKagbVkKyPHyQQOA11pUWoGEN_v0lsffEaP7Pcl4UZwTr-rg_NKr9Cqt9xUVzm7lodyyx"; // SANDBOX
//$PAYPAL_SECRET_KEY = "EFMYqhxnvWpijympzxpLmuJ2gvQUZGAKfqP7n7hQz9EHZEjZVttXXIedXXqFdSLbDz4_Kxg67j0H8EL0"; // SANDBOX

$PAYPAL_CLIENT_KEY = "AfDSgqEEpK9rnHWQK47eWZgCpMPQTi6mQ7Nz4mXI2kYzN7VrLNGBeoNJ9buSZVzG3xZerT8bLIq5AHBU"; // LIVE
$PAYPAL_SECRET_KEY = "EIKgi712sxhoqsYCi2oQYTXZqofgaL9OMD42uSVbPXbaDzEV5vLYClIazrGeNFRZ5br86aA28nsfChd4"; // LIVE


/**
 * Get Currency 
 */
$currency = '';
if(isset( $_SESSION["usr_cntry"])){
   $country  = $_SESSION["usr_cntry"];
    if($country == '1'){
       $currency = 'USD';
    }elseif($country == '82'){
       $currency = 'INR';
    }elseif($country == '35'){
       $currency = 'CAD';
    }else{
       $currency = 'GBP';
    }
}
