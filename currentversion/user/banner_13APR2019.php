<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/lib_file_system.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_MT_BANNER";
$str_img_path = $STR_SITENAME_WITH_PROTOCOL."/mdm/banner/";
$str_db_table_name = "t_page_metatag";
$str_xml_file_name = "banner.xml";
$str_xml_file_name_cms = "banner_cms.xml";
$int_records_per_page = 1;
$INT_BANNER = 780;
#----------------------------------------------------------------------
#read main xml file
$arr_xml_list = "";
$arr_xml_list = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");

$int_total_records = 0;
$int_total_records = (count(array_keys($arr_xml_list)));	
//print $int_total_records;
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
##get Query String Data
$str_aid="";
$str_pt="";
//if(isset($_POST['txt_login']))
//{
//	$str_aid=trim($_POST['txt_login']);
//}
if(isset($_POST['pt']))
{
	//$str_pt=trim($_POST['pt']);
    if(isset($_POST['txt_login']))
    {
        $str_aid=trim($_POST['txt_login']);

        // Set Affiliate ID cookies so it can be accessed from all pages
        setcookie('affid',$str_aid,0,'/');
    }
}
else 
{
    if(isset($_POST["aid"])==true)
    {
        $str_aid=trim($_POST["aid"]);
    }	
}
//print_r($_SESSION);
/*$int_page = 1;
if($int_total_records>0)
{
    $int_total_pages = ceil($int_total_records / $int_records_per_page); 
    //print $int_total_pages;
    if ($int_page > $int_total_pages)
    {
        $int_page = $int_total_pages;
    }
    if($int_page < 1)
    {
        $int_page=1;
    }
}*/
/*if($int_total_records > 0)
{
    $int_total_pages = ceil($int_total_records / $int_records_per_page); 
    //print $int_total_pages;exit;
    if ($int_page > $int_total_pages)
    {
        //$int_page = $int_total_pages;
        $_SESSION["catid"] = "";
        $_SESSION["key"] = "";
        $_SESSION["PagePosition"] = 1;
    }
    
}*/

#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table

$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
            </div>
        </div>
        <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms);?></p></div>
            </div>
        </div>
        <?php } ?>
        <?php 
        $arr_test = array_keys($arr_xml_list);
	
        if(trim($arr_test[0])!="ROOT_ITEM")
        {  ?>
            <form name="frmlink" action="">
            <?php
            $int_cnt=0;
            //print_r($arr_xml_list);
            foreach($arr_xml_list as $key => $val)         
            { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <?php				
                    if(trim($arr_xml_list[$key]["IMAGENAME"])!="") 
                    { 
                            $str_banner_ext = "";
                            $str_path="";
                            $image_embed_tag_textarea="";
                            $str_banner_ext = GetExtension($arr_xml_list[$key]["IMAGENAME"]);
                            $str_path = $str_img_path.$arr_xml_list[$key]["IMAGENAME"];
                            //print $str_path;
                            $title=MyHtmlEncode($arr_xml_list[$key]["LINKTEXT"]);
                            if(strtoupper($str_banner_ext)!="SWF")
                            {								
                                    $alt="Banner ".$int_cnt."";
                                    //$image_tag=ResizeImageTag($str_path,$arr_xml_list[$key]["HEIGHT"],$arr_xml_list[$key]["WIDTH"],$INT_BANNER,$alt,$title,0);
                                    $image_tag = "";
                                    $image_tag = "<img src='".$str_path."' class='img-responsive' alt='".$alt."' title='".$title."' />";
                                    ?>
                                        <a href="<?php print(MyHtmlEncode($arr_xml_list[$key]["LINKURL"]));?>" target="_blank"><?php print($image_tag);?></a>
                                        
                                    <?php
                                    $image_tag_textarea="";
                                    //$image_tag=ResizeImageTag($str_path,$arr_xml_list[$key]["HEIGHT"],$arr_xml_list[$key]["WIDTH"],$INT_BANNER,$alt,$title,1);
                                    $image_tag_textarea="<a href='".$arr_xml_list[$key]["LINKURL"]."' target='_blank'>".$image_tag."</a>";								
                            }
                            else
                            {
                            $swf_object_tag=DisplaySWFFile($str_path,MyHtmlEncode($arr_xml_list[$key]["LINKURL"]),$arr_xml_list[$key]["WIDTH"],$arr_xml_list[$key]["HEIGHT"],"_blank",$title,"#FFFFFF",0);
                            print($swf_object_tag);
                            $image_tag_textarea = DisplaySWFFile($str_path,MyHtmlEncode($arr_xml_list[$key]["LINKURL"]),$arr_xml_list[$key]["WIDTH"],$arr_xml_list[$key]["HEIGHT"],"_blank",$title,"#FFFFFF",1);;
                            $image_embed_tag_textarea = DisplaySWFFileWithEmbedTag($str_path,MyHtmlEncode($arr_xml_list[$key]["LINKURL"]),$arr_xml_list[$key]["WIDTH"],$arr_xml_list[$key]["HEIGHT"],"_blank",$title,"#FFFFFF",1);
                            }
                            if($arr_xml_list[$key]["DISPLAYASNEW"]=="YES")
                            { ?><div class=" text-top-left"><?php print $STR_ICON_PATH_NEW; ?></div><?php }
                    }
                    ?>
                            <br/>
                            <textarea  wrap="on" name="ta_link<?php print($int_cnt);?>" rows="3" class="form-control"><?php print($image_tag_textarea);?></textarea> 
                            <a href="javascript: void(0)" onClick="return HighlightAll('frmlink.ta_link<?php print($int_cnt);?>')" >Select text</a> 
                </div>
            </div><br/>
    <?php $int_cnt++;  }?>
            </form>
        <br/>
        <?php
        }?>
        
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/banner.js"></script>
</body>
</html>
