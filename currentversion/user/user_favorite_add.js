
$(function() {
   
    $("#frm_user_favorite input").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // something to have when submit produces an error ?
            // Not decided if I need it yet
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
//            alert("favorite");
            var userpkid = $("input#userpkid").val();
            var itempkid = $("input#itempkid").val();
            var flgfav = $("input#flgfav").val();
            
            
            $.ajax({
                
                url: "./user/user_favorite_item_add_p.php",
                type: "POST",
                data: {
                    
                    userpkid : userpkid,
                    itempkid : itempkid,
                    flgfav : flgfav
                },
                cache: false,
                success: function(data) 
                {
//                    alert(data);
                    var $responseText = JSON.parse(data);
                    if($responseText.status == 'SUC')
//                                        if(($responseText.status).equal("SUC"))
                    {
                        // Success message
//                        alert(data);
                        
                        $('#success').html("<div class='alert alert-success'>");
                        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                        $('#success > .alert-success').append("<strong> " + $responseText.message + " </strong>");
                        $('#success > .alert-success').append('</div>');
                        
//                        $('#success').html("<div class='alert alert-success'>");
//                        $('#success> .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
//                        .append("</button>");
//                        $('#success > .alert-success').append("<strong> " + $responseText.message + " </strong>");
//                        $('#success > .alert-success').append('</div>');
                            
                          setTimeout(function(){ location.reload() }, 2000);
                            setTimeout(function(){ window.location.href="./signin"; }, 5000);
                            //window.location.href="./signin";

//                            setTimeout(function(){ alert("Hello"); }, 3000);
//                        setTimeout(function(){ location.reload() }, 96866656);
                    }
                    else if($responseText.status == 'ERR')
                    {
                        // Fail message
                        $('#success').html("<div class='alert alert-danger'>");
                        $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                .append("</button>");
                        $('#success > .alert-danger').append("<strong> " + $responseText.message + " ");
                        $('#success > .alert-danger').append('</div>');
                        
                    }
                },
	
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});
/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
