<?php
//session_start();
include "./product_validatesession.php";
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../user/product_config.php";
//print_r($_SESSION);

#---------------------------------------------------------------------------------------------------------
$int_statepkid = 0;



// if (isset($_SESSION['userpkid']) && $_SESSION['userpkid'] != "") {
//     $str_query_select = "";
//     $str_query_select = "SELECT statepkid FROM t_user WHERE pkid=" . $_SESSION['userpkid'];
//     //print $str_query_select."<br/>"; 
//     $rs_list_user = GetRecordSet($str_query_select);

//     $int_statepkid = $rs_list_user->fields("statepkid"); // If user is logged in & has selected State from profile then it will be assigned here to display user's selected state
//     echo $int_statepkid;
// }

// get state id from product_address.js for preselected state of country 
if ($_POST['state']) {
    $int_statepkid = $_POST['state'];
}
#---------------------------------------------------------------------------------------------------------
$int_countrypkid = 0;
if (isset($_POST["countrypkid"])) {
    $int_countrypkid = trim($_POST["countrypkid"]);
}

$str_query_select = "";
$str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME_STATE . " WHERE masterpkid=" . $int_countrypkid . " AND visible='YES' ORDER BY title ASC";
//print $str_query_select."<br/>"; // exit;
$rs_list_state = GetRecordSet($str_query_select);

#---------------------------------------------------------------------------------------------------------
echo "<select class='form-control' name='cbo_state' id='cbo_state'>";

while (!$rs_list_state->EOF() == true) {
    //if(trim($rs_list_state->fields('pkid')) == trim($int_statepkid))  { $str_flag_selected = "selected"; } else { $str_flag_selected = ""; } 
    echo "<option value='" . $rs_list_state->fields('pkid') . "' " . CheckSelected($rs_list_state->fields('pkid'), $int_statepkid) . ">" . $rs_list_state->fields('title') . "</option>";

    $rs_list_state->MoveNext();
}
echo "</select>";
