<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_MT_BLOG";
$str_db_table_name_metatag = "t_page_metatag";

$str_img_path = "./mdm/blog/";

$str_db_table_name = "tr_blog";

$str_xml_file_name = "";
$str_xml_file_name_cat = "";

$str_xml_file_name_cms = "blog_cms.xml";
$int_records_per_page = 1;
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
/*$int_cat_pkid = "";
if(isset($_POST["catid"]) && trim($_POST["catid"]) != "" )
{   
    $int_cat_pkid = trim($_POST["catid"]);    
}

$str_name_key = "";
if(isset($_POST["key"]) && trim($_POST["key"]) != "" )
{ $str_name_key = trim($_POST["key"]); }


# get data for paging
$int_page = "";
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"]) != "" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"]) > 0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page = 1; }
//print_r($_POST);
$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";


if($int_cat_pkid > 0)
{
    $str_where = " AND a.subcatpkid=".$int_cat_pkid." ";
}*/
$str_where = "";    
#----------------------------------------------------------------------
# Select Query to get list
$str_query_select = "";
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$str_db_table_name." WHERE approved = 'YES' AND visible = 'YES' AND blogtype = 'B' ORDER BY submitdatetime DESC, title ASC";
//print $str_query_select; exit;
$rs_list = GetRecordSet($str_query_select);
//print $rs_list->Count();
#----------------------------------------------------------------------
$int_total_records = 0;
$int_total_records = 0;	
//print $int_total_records;
//print_r($_SESSION);
//print $int_total_records; exit;
#open cms xml file
$str_desc_cms = "";
$str_desc_cms2 = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatag. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
            </div>
        </div>
        <?php if($str_visible_cms == "YES") { ?>
            <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        <?php 
        $int_cnt = 0;
        //print $rs_list->Count();
        while($rs_list->eof() == false)
        {
        ?>
        <div class="row padding-10">
            <div class="col-md-1 text-center">
                <p class="text-primary"><i class="fa fa-file-text fa-3x"></i></p>
                <p><?php print date("M d, Y H:i:s", strtotime($rs_list->Fields("submitdatetime"))); ?> EST</p>
            </div>
            <?php if($rs_list->Fields("largeimagefilename") != "") { 
                    $str_div_first = "col-md-4";
                    $str_div_second = "col-md-7";
            } else { 
                    $str_div_first = "col-md-0";
                    $str_div_second = "col-md-11";	
            } ?>
            <div class="<?php print($str_div_first); ?>">
                <?php if($rs_list->Fields("largeimagefilename") != "") { ?>
                <img class="img-responsive" src="<?php print $str_img_path.$rs_list->Fields("largeimagefilename"); ?>" alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>">
                <?php } ?>
            </div>
            <div class="<?php print($str_div_second); ?>">
                <?php /* ?><?php if($rs_list->Fields("largeimagefilename") != "") { ?>
                <p><img class="img-responsive img-hover img-thumbnail" src="<?php print $BLOG_IMAGE_PATH_ABSOLUTE.$rs_list->Fields("largeimagefilename"); ?>" alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>"></p>
                <?php } ?><?php */ ?>
                <?php if($rs_list->Fields("title") != "") { ?>
                <h3 class="nopadding"><?php print $rs_list->Fields("title"); ?></h3>
                <?php } ?>


                <?php if($rs_list->fields("description") != "<br>" && $rs_list->fields("description") != "") {?>
                <p class=""><?php print $rs_list->fields("description"); ?></p>
                <?php } ?>
                    <?php
                    $str_img_alt = "";
                    if($rs_list->fields("url") != "")
                    {
                        if($rs_list->fields("url") != "") 
                            { $str_img_alt = $rs_list->fields("urltitle"); }
                        else
                            { $str_img_alt = $rs_list->fields("url"); } 
                    } 

                    if($rs_list->fields("url") != "") {
                    ?>    
                    <i class="fa fa-globe text-primary"></i>&nbsp;<?php print(DisplayWebsiteURL($rs_list->fields("url"),$rs_list->fields("urltitle"),$rs_list->fields("openinnewwindow"), $str_img_alt, '', '', ''));?>
                    <?php } ?>    
                </p>
                <?php
                $sel_qry_cnt_comments = "";
                $sel_qry_cnt_comments = "SELECT count(*) AS noofcomments FROM ".$str_db_table_name." WHERE blogpkid = ".$rs_list->Fields("blogpkid")." AND blogtype='C' AND visible='YES' AND approved='YES'";
                $rs_cnt_comments=GetRecordSet($sel_qry_cnt_comments);

                ?>
                <?php /* ?><a class="btn btn-success" href="./blog-<?php print $rs_list->Fields("blogpkid");?>" title="Click to view blog details">View Comments (<?php print $rs_cnt_comments->Fields("noofcomments"); ?>)&nbsp;&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></a><?php */ ?>
                <a class="btn btn-primary" href="./blog-<?php print $rs_list->Fields("blogpkid");?>#blogleavecomment" title="Click to leave a comment"><b>Leave a Comment&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></b></a>
            </div>
        </div>
        <br/><hr/><br/>
        <?php 
        $int_cnt++;
        $rs_list->MoveNext();
        } ?>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
</body>
</html>
