<?php
/*
	Module Name	:-  product payment
	File Name	:- product_payment_success_p.php
	Create Date	:- 10-01-2017
	Initially Created by :- 015
*/
#------------------------------------------------------------------------------------------
#	include files
//	session_start();
	include "./../user/store_validatesession.php";
        include "./../includes/configuration.php";
	include "./../includes/lib_data_access.php";
	include "./../includes/lib_common.php";
	include "./../includes/lib_email.php";
	include "./../includes/lib_xml.php";
	include "./../user/store_config.php";
        //include "./../includes/http_to_https.php";
#------------------------------------------------------------------------------------------

	//print_r($_POST); exit;

	$item_sessionid="";
	$item_uniqueid="";

	# Use below code only when doing testing on LOCAL server - START
	/*if(isset($_SESSION['sessionid']))
	{
		$item_sessionid=$_SESSION['sessionid'];
	}
	if(isset($_SESSION['uniqueid']))
	{
		$item_uniqueid=$_SESSION['uniqueid'];
	}*/
	# Use below code only when doing testing on LOCAL server - END


	# Use below code only when doing testing on ACTUAL server - START
	if(isset($_POST["itemsessionid"]) && trim($_POST["itemsessionid"])!="")
	{
		$item_sessionid=trim($_POST["itemsessionid"]);
	}

	if(isset($_POST["itemuniqueid"]) && trim($_POST["itemuniqueid"])!="")
	{
		$item_uniqueid=trim($_POST["itemuniqueid"]);
	}
	# Use below code only when doing testing on ACTUAL server - END


	/*$str_userid="";
	if(isset($_POST["userid"]) && trim($_POST["userid"])!="")
	{
		$str_userid=trim($_POST["userid"]);
	}

	$int_userpkid="";
	if(isset($_POST["userpkid"]) && trim($_POST["userpkid"])!="")
	{
		$int_userpkid=trim($_POST["userpkid"]);
	}*/
        
//        print "pkid" .$str_memberid;exit;
        
        $str_ship_add="";
	if(isset($_POST["ta_ship_add"]) && trim($_POST["ta_ship_add"])!="")
	{
		//$str_ship_add=RemoveQuote(trim($_POST["ta_ship_add"]));
		$str_ship_add=str_replace("'", "", RemoveQuote(trim($_POST["ta_ship_add"])));
	}
	

	$str_login_id="";
	$str_login_password="";

	$str_fname="";
	$str_lname="";
	$str_address="";
	$str_city="";
	$str_state="";
	$str_country="";
	$str_zipcode="";
	$str_phone="";	
	$str_email_id="";
	$str_ipaddress="";

	$str_response_digest="";
	$str_trans_id="";
	$str_salt="";
	$int_trans_status=""; // 1 means pament is process SUCCESSFULLY, 0 means payment is DENIED


	$var_extprice=0;
	if(isset($_POST["initialPrice"]))
	{
		$var_extprice=RemoveQuote(trim($_POST["initialPrice"]));	
	}

	if(isset($_POST["subscription_id"]) && trim($_POST["subscription_id"])!="")
	{
		$str_trans_id=RemoveQuote(trim($_POST["subscription_id"]));
	}
	//print $str_trans_id; exit;
		
	/*if(isset($_POST["formPrice"]))
	{
		$var_extprice=RemoveQuote(trim($_POST["formPrice"]));	
	}
	if(isset($_POST["subscriptionId"]) && trim($_POST["subscriptionId"])!="")
	{
		$str_trans_id=RemoveQuote(trim($_POST["subscriptionId"]));
	}*/


	if(isset($_POST["username"]) && trim($_POST["username"])!="")
	{
		$str_login_id=RemoveQuote(trim($_POST["username"]));
	}
	if(isset($_POST["password"]) && trim($_POST["password"])!="")
	{
		$str_login_password=RemoveQuote(trim($_POST["password"]));
	}	
	if(isset($_POST["email"]) && trim($_POST["email"])!="")
	{
		$str_email_id=RemoveQuote(trim($_POST["email"]));
	}
	if(isset($_POST["customer_fname"]) && trim($_POST["customer_fname"])!="")
	{
		//$str_fname=RemoveQuote(trim($_POST["customer_fname"]));
		$str_fname=str_replace("'", "", RemoveQuote(trim($_POST["customer_fname"])));
	}
	if(isset($_POST["customer_lname"]) && trim($_POST["customer_lname"])!="")
	{
		//$str_lname=RemoveQuote(trim($_POST["customer_lname"]));
		$str_lname=str_replace("'", "", RemoveQuote(trim($_POST["customer_lname"])));
		
	}
	if(isset($_POST["address1"]) && trim($_POST["address1"])!="")
	{
		//$str_address=RemoveQuote(trim($_POST["address1"]));
		$str_address=str_replace("'", "", RemoveQuote(trim($_POST["address1"])));
	}
	if(isset($_POST["state"]) && trim($_POST["state"])!="")
	{
		$str_state=RemoveQuote(trim($_POST["state"]));
	}
	if(isset($_POST["city"]) && trim($_POST["city"])!="")
	{
		$str_city=RemoveQuote(trim($_POST["city"]));
	}
	if(isset($_POST["country"]) && trim($_POST["country"])!="")
	{
		$str_country=RemoveQuote(trim($_POST["country"]));
	}
	if(isset($_POST["zipcode"]) && trim($_POST["zipcode"])!="")
	{
		$str_zipcode=RemoveQuote(trim($_POST["zipcode"]));
	}
	if(isset($_POST["ip_address"]) && trim($_POST["ip_address"])!="")
	{
		$str_ipaddress=RemoveQuote(trim($_POST["ip_address"]));
	}
	if(isset($_POST["phone_number"]) && trim($_POST["phone_number"])!="")
	{
		$str_phone=RemoveQuote(trim($_POST["phone_number"]));
	}
	
	$str_ship_add="";
	if(isset($_POST["ta_ship_add"]) && trim($_POST["ta_ship_add"])!="")
	{
		//$str_ship_add=RemoveQuote(trim($_POST["ta_ship_add"]));
		$str_ship_add=str_replace("'", "", RemoveQuote(trim($_POST["ta_ship_add"])));
	}

	//print $str_ship_add;exit;
	//sendmail("kra.rd.test@gmail.com", "MFI",$str_trans_id." - ".$item_sessionid." - ".$item_uniqueid, "kra.rd.test@gmail.com", 1); 
	#########	CHECK Member Exist Or Not And member id from the table
	/*$str_membername_name="";	
	$str_select="SELECT loginid FROM t_freemember WHERE memberpkid=".$int_memberpkid;
	
	// print $str_select; exit;
	
	$rs_member_list=GetRecordset($str_select);
	if($rs_member_list->eof()==false)
	{
		$str_membername_name=$rs_member_list->fields("loginid");
	}
	else
	{
		exit;
	}*/
	
	$str_purchase_date = date("Y-m-d H:i:s");
	$str_purchase_day = "";
	$str_purchase_month = "";
	$str_purchase_year = "";
	
	/// find the day, month and year from the transaction date
	$str_purchase_day=date("d",strtotime($str_purchase_date));
	$str_purchase_month=date("m",strtotime($str_purchase_date));
	$str_purchase_year=date("Y",strtotime($str_purchase_date));
	
	$str_download="YES";
	$str_addedby="YES";		
	
	
	#########	Get individual product data from t_sessioncart table
	$str_price_value="";
	$str_product_title="";
	$str_model_name="";
	
	# This variable is used to store purchased items. So we can send mail to model
	$str_items_list="";
	$int_modelpkid_for_mail=0;
	
	$str_select="SELECT * FROM ".$STR_DB_TABLE_NAME_SESSION_CART." WHERE sessionid='".$item_sessionid. "' AND uniqueid='".$item_uniqueid."'";
        //print $str_select;exit;
	//$str_select="SELECT * FROM t_sessioncart WHERE sessionid='".$item_sessionid. "' AND uniqueid='".$item_uniqueid."' AND freememberpkid=".$int_memberpkid;
	$rs_cart_list=GetRecordset($str_select);
	$int_ps_quantity=$rs_cart_list->Count(); // No. of records fetched in the recordset
	
	$expire_date=date("Y-m-d H:i:s",mktime(23, 59, 59, date("m")  , date("d")+$STR_CCBILL_STORE_DOWNLOAD_LIMIT_DAY, date("Y")));
	//print $expire_date; exit;	
	//print $str_select; exit;
	
	if($rs_cart_list->eof()==false)
	{
		
		//print "hello";exit;
		/*$sel_buyer_info="";
		$sel_buyer_info="SELECT loginid,password FROM t_buyer";
		$rs_buyer_info = GetRecordSet($sel_buyer_info);
		
		if($rs_buyer_info->fields("loginid") == $str_email_id){
			$str_member_password = 	$rs_buyer_info->fields("password");
		}
		else{*/
			//$str_member_loginid = GenerateRandomString();
			//$str_member_password = Generate_Random_Password();
			//print "password:  ". $str_member_password; exit;
		//}	
		
		while(!$rs_cart_list->eof())
		{	
                    $price = $rs_cart_list->fields("price"); 



                    $str_reg_date=date("Y-m-d H:i:s");

                    $str_query_insert = "";
                    $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_PURCHASE."(userpkid, username, productpkid, catpkid,subcatpkid, memberpkid,";
                    $str_query_insert.= "membername, producttitle,cattitle, subcattitle, color, size, type, sentdate, quantity, pricevalue, price,";
                    $str_query_insert.= "extendedprice, subscriptionid, firstname, lastname, emailid, address, city, state, country,";
                    $str_query_insert.= "zipcode,shippingaddress, phoneno, dshippingvalue, promocodepkid, finaldiscountedamount, shippinginfo,";
                    $str_query_insert.= "expiredate,purchasedatetime,ipaddress,inmonth,inyear) VALUES (";
                    $str_query_insert.= $rs_cart_list->fields("userpkid").",";
                    $str_query_insert.= "'".$rs_cart_list->fields("username")."', ";
                    $str_query_insert.= $rs_cart_list->fields("productpkid").", ";
                    $str_query_insert.= $rs_cart_list->fields("catpkid").", ";
                    $str_query_insert.= $rs_cart_list->fields("subcatpkid").", ";
                    $str_query_insert.= $rs_cart_list->fields("memberpkid").", ";
                    $str_query_insert.= "'".$rs_cart_list->fields("membername")."', ";
                    $str_query_insert.= "'".$rs_cart_list->fields("producttitle")."', ";
                    $str_query_insert.= "'".$rs_cart_list->fields("cattitle")."',";
                    $str_query_insert.= "'".$rs_cart_list->fields("subcattitle")."',";
                    $str_query_insert.= "'".$rs_cart_list->fields("color")."', ";
                    $str_query_insert.= "'".$rs_cart_list->fields("size")."', ";
                    $str_query_insert.= "'".$rs_cart_list->fields("type")."', ";
                    $str_query_insert.= "'".$rs_cart_list->fields("sentdate")."', ";
                    $str_query_insert.= $rs_cart_list->fields("quantity")." ,";
                    $str_query_insert.= "'".$rs_cart_list->fields("price")."', ";
                    $str_query_insert.= $rs_cart_list->fields("price").", ";
                    $str_query_insert.= $rs_cart_list->fields("extendedprice").", ";
                    $str_query_insert.= "'".$str_trans_id."', ";
                    $str_query_insert.= "'".ReplaceQuote($str_fname)."', ";
                    $str_query_insert.= "'".ReplaceQuote($str_lname)."', ";
                    $str_query_insert.= "'".ReplaceQuote($str_email_id)."', ";
                    $str_query_insert.= "'".ReplaceQuote($str_address)."',";
                    $str_query_insert.= "'".ReplaceQuote($str_city)."', ";
                    $str_query_insert.= "'".ReplaceQuote($str_state)."',";
                    $str_query_insert.= "'".ReplaceQuote($str_country)."', ";
                    $str_query_insert.= "'".ReplaceQuote($str_zipcode)."', ";
                    $str_query_insert.= "'".ReplaceQuote($str_ship_add)."',";
                    $str_query_insert.= "'".ReplaceQuote($str_phone)."', ";
                    $str_query_insert.= $rs_cart_list->fields("shippingvalue").", ";
                    $str_query_insert.= $rs_cart_list->fields("promocodepkid").", ";
                    $str_query_insert.= $rs_cart_list->fields("finaldiscountedamount").", ";
                    $str_query_insert.= "'".ReplaceQuote($rs_cart_list->fields("shippingaddress"))."' , ";
                    $str_query_insert.= "'".$expire_date."', ";
                    $str_query_insert.= "'".$str_reg_date."', ";
                    $str_query_insert.= "'".$_SERVER['REMOTE_ADDR']."', ";
                    $str_query_insert.= $str_purchase_month.",";
                    $str_query_insert.= $str_purchase_year.")";

                    //print $str_query_insert."<br/><br/>"; exit;
                    ExecuteQuery($str_query_insert);

                    #----------------------------------------------------------------------------------------------------
                    /*$fp=openXMLfile($UPLOAD_XML_CONTENT_PATH."siteconfiguration.xml");
                    $str_from = getTagValue($STR_FROM_DEFAULT,$fp);
                    $str_to = getTagValue($STR_FROM_DEFAULT,$fp);
                    closeXMLfile($fp);

                    $str_query_select = "SELECT shortenurlkey, getnotification FROM t_model WHERE modelpkid=".$rs_cart_list->fields("modelpkid")."";
                    //print $str_query_select;exit;
                    $rs_list = GetRecordSet($str_query_select);

                    if(!$rs_list->eof())
                    {
                        $str_subject="Donation received on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
                        $mailbody="Donation received. Below are details.<br/><br/>";
                        $mailbody.="<strong>Recipient Name:</strong> ".$rs_list->fields("shortenurlkey")."<br/>";
                        $mailbody.="<strong>Received Amount:</strong> ".$rs_cart_list->fields("price")."<br/>";
                        $mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL."";
                        //print "<br/>".$str_from; print "<br/>".$str_to; print "<br/>".$mailbody; exit;
                        sendmail($str_to,$str_subject,$mailbody,$str_from,1);  
                    }*/
                    #----------------------------------------------------------------------------------------------------

                    $str_items_list.="<br/><br/>".$rs_cart_list->fields("producttitle")." : $".$rs_cart_list->fields("extendedprice");
                    //$str_items_list.="<br/>quantity: ".$rs_cart_list->fields("quantity")." | color: ".$rs_cart_list->fields("color")." | size: ".$rs_cart_list->fields("size")." | type/model: ".$rs_cart_list->fields("typemodel");
                    //print $str_items_list;
                    

                    ###	Update No of Sold increamnt in tr_mdl_photoset table
                    //$str_update="UPDATE tr_mdl_photoset SET noofsold=noofsold+1 WHERE photosetpkid=".$rs_cart_list->fields("photosetpkid");
                    //print $str_update."<BR><BR><BR>";
                    //ExecuteQuery($str_update);
                    $rs_cart_list->MoveNext();
		}
	//exit;
	/*else
	{
		exit;
	}*/
			# Query to insert item list in t_product_purchase_temp2 table...
			//$str_query_insert_temp2="INSERT INTO t_product_purchase_temp2 (subid, sessionid, uniqueid, itemlist) VALUES ('".$str_trans_id."','".$item_sessionid."','".$item_uniqueid."','".str_replace("'", "|", $str_items_list)."')";
			//print $str_query_insert_temp2."<br/><br/>"; 
			//ExecuteQuery($str_query_insert_temp2);

 
	#Add Purchase Record In t_product_purchase_orderwise table
	/*$str_query_insert="INSERT INTO  t_product_purchase_orderwise (freememberpkid,quantity,extendedprice,purchasedatetime,ipaddress) VALUES (";
	$str_query_insert.="0,".$int_ps_quantity.",".$var_extprice.",'".ReplaceQuote($str_purchase_date)."','".ReplaceQuote($str_ipaddress)."')";
			
	print $str_query_insert."<BR><BR>"; exit;
	ExecuteQuery($str_query_insert);*/

	#########	Once product purchase date stored in t_product_purchase table, DELETE those data from t_sessioncart table
	$str_query_delete="DELETE FROM ".$STR_DB_TABLE_NAME_SESSION_CART." WHERE sessionid='".$item_sessionid. "' AND uniqueid='".$item_uniqueid."'";
//	$str_query_delete="DELETE FROM t_sessioncart WHERE sessionid='".$item_sessionid. "' AND uniqueid='".$item_uniqueid."' AND freememberpkid=".$int_memberpkid;
	ExecuteQuery($str_query_delete);
	//exit;
	}
        
        $str_query_select = "";
        $str_query_select="SELECT * ";
        $str_query_select.=" FROM ".$STR_DB_TABLE_NAME_PURCHASE." ";
        //$str_query_select.=" LEFT JOIN t_store a ON a.pkid=m.productpkid AND a.visible='YES'";
        //$str_query_select.=" LEFT JOIN tr_store_photo b ON a.pkid=b.masterpkid AND b.setasfront='YES' AND b.visible='YES'";
        //$str_query_select.=" LEFT JOIN t_store_price c ON a.pricepkid=c.pricepkid ";
        //$str_query_select.=" LEFT JOIN t_store_subcat d ON a.subcatpkid=d.subcatpkid ";
        $str_query_select.=" WHERE purchasedatetime='".ReplaceQuote($str_purchase_date)."'";
        $str_query_select.=" ORDER BY purchasedatetime desc";
        //print $str_query_select; exit;
        $rs_list = GetRecordSet($str_query_select);
            //print $rs_list->Fields("purchasepkid");exit;
        if($rs_list->Count() > 0)
        {
            /*## Below code to update purchasepkid in senders list which were selected while add to cart
            $str_query_update = "";
            $str_query_update = "UPDATE t_store_purchase_addressbook SET purchasepkid=".$rs_list->Fields("purchasepkid"). " WHERE masterpkid=".$rs_list->Fields("productpkid")." AND userpkid=".$rs_list->Fields("userpkid")." AND purchasepkid=0";
            ExecuteQuery($str_query_update);*/
            
        
        
        $str_item_mailbody="";
        
        $str_producttitle ="";
        $str_username ="";
       /* while($rs_list->eof()==false)
        {
            $int_purchasepkid = $rs_list->fields("purchasepkid");		
            $str_producttitle = $rs_list->fields("producttitle");
            
            $int_pur_pid=GetEncryptId($int_purchasepkid);
            $int_pur_eid=GetEncryptId($int_purchasepkid);
            $int_memberid=GetEncryptId($int_memberpkid);
            $str_username = "";
            $str_username = $rs_list->fields("username");
            
            
            $str_item_mailbody.="<a href=".$SITENAME_WITH_PROTOCOL."/mem_store_purchased_list.php?pid=".$int_pur_pid."&eid=".$int_pur_eid."&mid=".$int_memberid."'>".$str_producttitle."</a><br/><br/>";
//print $str_item_mailbody;
            $rs_list->MoveNext();
        }*/

        $str_query_select = "";
        $str_query_select = "SELECT * FROM t_user WHERE pkid=".$rs_list->Fields("userpkid");
        $rs_list_user = GetRecordSet($str_query_select);
        

        $str_mailbody_user = "";
        $str_subject_user = "";

        $str_mailbody_admin="";
        $str_subject_admin="";

        $fp=openXMLfile($STR_XML_FILE_PATH_MODULE."siteconfiguration.xml");
        $str_from=getTagValue($STR_FROM_DEFAULT,$fp);
        $str_to = $rs_list_user->fields("emailid");
        closeXMLfile($fp);

        $str_subject_user = $STR_MAIL_SUBJECT_PURCHASE_USER;
        $str_mailbody_user = $STR_MAIL_MAILBODY_PURCHASE_USER;
        $str_mailbody_user.= "<br/><br/> <strong>Purchase Date:</strong> " .ReplaceQuote($str_purchase_date). " ";
        $str_mailbody_user.= "<br/>".$str_items_list;
        //print $str_mailbody_user; exit;
        //$str_mailbody_user.= "<br/><br/> Click here to download: " . $str_item_mailbody;
        
//        print "<br/><br/> FOR BUYER: <br/>".$str_mailbody_user;
        sendmail($str_to,$str_subject_user,$str_mailbody_user,$str_from,1);
        
        $fp=openXMLfile($STR_XML_FILE_PATH_MODULE."siteconfiguration.xml");
        $str_to_admin = getTagValue($STR_FROM_DEFAULT,$fp);
        closeXMLfile($fp);

        $str_subject_admin = $STR_MAIL_SUBJECT_PURCHASE_ADMIN;
        $str_mailbody_admin = $STR_MAIL_MAILBODY_PURCHASE_ADMIN;
        $str_mailbody_admin.= "<br/><br/> <strong>Purchase Date:</strong> " .ReplaceQuote($str_purchase_date). " ";
        $str_mailbody_admin.= "<br/><br/> <strong>Item Title:</strong> " .ReplaceQuote($str_producttitle). " ";
        //$str_mailbody_admin.= "<br/><strong>Sold By:</strong> " .ReplaceQuote($str_modelname). " ";
        $str_mailbody_admin.= "<br/><strong>Purchased By:</strong> " .ReplaceQuote($str_memberid). " ";
        //$str_mailbody_user.= "<br/><br/> " . $str_item_mailbody;
        //print "<br/><br/> FOR ADMIN: <br/>".$str_mailbody_admin;
        sendmail($str_to_admin,$str_subject_admin,$str_mailbody_admin,$str_from,1);
        
        /*$str_mailbody_model = "";
        $str_subject_model = "";

        $str_subject_model = "Store purchase completed at " . $STR_SITE_URL . "";
        $str_mailbody_model = "Store details are as below.";
        $str_mailbody_model.= "<br/><br/><strong>Purchase Date:</strong> " .ReplaceQuote($str_purchase_date). " ";
        $str_mailbody_model.= "<br/><strong>Item Title:</strong> " .ReplaceQuote($str_producttitle). " ";
        $str_mailbody_model.= "<br/><strong>Purchased By:</strong> " .ReplaceQuote($str_memberid). " ";
        //$str_mailbody_user.= "<br/><br/> " . $str_item_mailbody;
        //print "<br/><br/> FOR MODEL: <br/>".$str_mailbody_admin;exit;
        sendmail($str_to,$str_subject_model,$str_mailbody_model,$str_from,1);*/
        //exit;
        }
#------------------------------------------------------------------------------------------
    //CloseConnection();
    Redirect("./store_checkout_msg.php?st=s");
    //Redirect("member_home.php");
    exit(); ?>
