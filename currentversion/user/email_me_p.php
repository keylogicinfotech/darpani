<?php
/*
File Name  :- contact_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------
#Include files
session_start();
include "./../includes/lib_common.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";

include "./../includes/lib_email.php";
include "./../includes/lib_xml.php";
include "./../includes/lib_file_upload.php";
include "./../includes/lib_image.php";
include "./../includes/unique_site_view_count.php";
#------------------------------------------------------------------------------
//print_r($_POST);exit;
#------------------------------------------------------------------------------
$str_db_table_name = "t_emailme";
$UPLOAD_PHOTO_PATH="../mdm/email_me/";
$str_dir=$UPLOAD_PHOTO_PATH;

$str_large_image="";
/*$int_subject="";	
if(isset($_POST['subject']))
{
    $int_subject = trim($_POST['subject']);
}*/	
//print_r($str_files);exit;
/*$str_fname="";
if(isset($_POST['txt_fname']))
{
	$str_fname=trim($_POST['txt_fname']);
}	

$str_lname="";
if(isset($_POST['txt_lname'])){
	$str_lname=trim($_POST['txt_lname']);
}*/
$str_name = "";
if(isset($_POST['name'])){
    $str_name = trim($_POST['name']);
}

$str_message = "";
if(isset($_POST['message'])){
    $str_message = trim($_POST['message']);
}	

$str_email = "";
if(isset($_POST['email'])){
    $str_email=trim($_POST['email']);
}	

/*$str_url="";
if(isset($_POST['url'])){
	$str_url=trim($_POST['url']);
}*/	

$str_phone="";
if(isset($_POST['phone'])){
	$str_phone=trim($_POST['phone']);
}

$str_secretcode = "";
if (isset($_POST["secretcode"])) { $str_secretcode = trim($_POST["secretcode"]); }

/*$str_country="";
if(isset($_POST['country'])){
	$str_country=trim($_POST['country']);
}
*/
/*$str_requirement="";
if(isset($_POST['rdo_service'])){
	$str_requirement=trim($_POST['rdo_service']);
}	
*/	

if( empty($str_name))
{
    $response['status']='ERR';
    $response['message']= "Please Enter Name!";
    echo json_encode($response); 
    return;
}

if(empty($str_message))
{
	$response['status']='ERR';
    $response['message']= "Please enter your message!";
    echo json_encode($response); 
    return;
}

/*if(empty($int_subject) || $int_subject == 0)
{
	$response['status']='ERR';
    $response['message']= "Please Select Subject!";
    echo json_encode($response); 
    return;
}*/
        
if( empty($str_email) || validateEmail($str_email)==false )
{
	$response['status']='ERR';
    $response['message']= "Invalid Email ID!";
    echo json_encode($response); 
    return;
}

if( empty($str_secretcode) || $_SESSION['image_secret_code'] != $str_secretcode )
{
	//$_SESSION['image_secret_code']="";
	$response['status']='ERR';
    $response['message']= "Invalid Secrete Code!";
    echo json_encode($response);
    return;
}
/*if($str_url !=""){
if(validateURL($str_url)==false )
		{
			$response['status']='ERR';
			$response['message']= "Invalid Url!";
			echo json_encode($response); 
			return;
		}
}*/
/*$requirement="";
$str_req_item="";
if($str_requirement != ""){
	$requirement=explode(",",$str_requirement);
	$int_count=0;
	$int_count=count($requirement) - 1;
	while($int_count>=0){
			$str_qry_select_item="SELECT catpkid,subcatpkid FROM t_contact_services WHERE pkid=".$requirement[$int_count];
			$rs_item_cat=GetRecordSet($str_qry_select_item);
			$int_catid=$rs_item_cat->fields('catpkid');
			$int_subcatid=$rs_item_cat->fields('subcatpkid');
			
			$str_req_item .= $int_catid."|".$int_subcatid."|".$requirement[$int_count]."-";
			$int_count--;
	}
}*/
//print $str_req_item;
//exit;


/*if( empty($str_secretcode) || $_SESSION['image_secret_code'] != $str_secretcode )
{
	//$_SESSION['image_secret_code']="";
	$response['status']='ERR';
    $response['message']= "Invalid Secrete Code!";
    echo json_encode($response);
    return;
}*/


#----------------------------------------------------------------------------------------------------
#upload image file.
/*$str_large_file_name="";
if($str_files != "")
{
	$str_dir="";
	$str_dir=$UPLOAD_PHOTO_PATH;
	//print $str_dir; exit;
	if(!file_exists($str_dir))
	{
		$response['status']='ERR';
		$response['message']= "Invalid file path!";
		echo json_encode($response);
		return;
	}
	#upload image file.	
	$str_large_path="";
	
	$str_large_file_name=$str_files;
	$str_large_path = trim($str_dir . $str_large_file_name);
	//print $str_large_path; exit;
	UploadFile($_FILES['uploadimages']['tmp_name'],$str_large_path);
	//ResizeImage($str_large_path,$INT_BIO_WIDTH);
}*/
//exit;
//$str_fax = $_POST['txt_phone'];
$str_ip_address = $_SERVER['REMOTE_ADDR'];
$str_datetime = date('Y-m-d H:i:s');
$str_unique_id = $str_datetime ."|". $str_email;
#----------------------------------------------------------------------------------------------------
$str_query_insert="INSERT INTO " .$str_db_table_name. " (name,emailid,phone,description,submitdatetime,uniquerowid,ipaddress,newmail)";
$str_query_insert.=" VALUES('".ReplaceQuote($str_name)."','".ReplaceQuote($str_email)."','".ReplaceQuote($str_phone)."', '".ReplaceQuote($str_message)."','".$str_datetime."','".ReplaceQuote($str_unique_id)."',";
$str_query_insert.="'".$str_ip_address."','YES')";	
//print($str_query_insert); exit;
ExecuteQuery($str_query_insert);	

$fp=openXMLfile($STR_XML_FILE_PATH_MODULE."siteconfiguration.xml");
$STR_SITE_URL=getTagValue($STR_WEBSITE_NAME,$fp);	
$str_from=getTagValue($STR_FROM_DEFAULT,$fp);
$str_to=getTagValue($STR_FROM_DEFAULT,$fp);
//$STR_SITE_URL=getTagValue("P_SITE_URL",$fp);	
closeXMLfile($fp);
//print "<br/>".$str_from; print "<br/>".$str_to; exit;
$str_subject="Contact inquiry submitted from ".$STR_SITENAME_WITHOUT_PROTOCOL."'contact section";
$mailbody="Contact inquiry submitted from contact section of " . $STR_SITE_URL . " <br> Following are the details: ";
$mailbody.="<BR><BR> <strong>Submit Date Time:</strong> ".date('Y-m-d H:i:s')." ";
$mailbody.="<br><br><strong>Name:</strong> " . MyHtmlEncode(RemoveQuote($str_name)) . " ";
$mailbody.="<br> <strong>E-mail Address:</strong> " . $str_email . " ";
$mailbody.="<br> <strong>Phone Number:</strong> " . ReplaceNullStringWithDash(RemoveQuote($str_phone),"--") . " ";
//$mailbody.="<br> <strong>Country:</strong> " . MyHtmlEncode(RemoveQuote($str_country),"--") . " ";
//$mailbody.="<br> <strong>Website Name of Company:</strong> " . $str_url . " ";
$mailbody.="<br> <strong>Message:</strong> " . RemoveQuote($str_message) ;
//print "<br/>".$str_from; print "<br/>".$str_to; print "<br/>".$mailbody; exit;
sendmail($str_to,$str_subject,$mailbody,$str_from,1);

//$_SESSION['image_secret_code']="";
$response['status']='SUC';
$response['message']="Your inquiry has been submitted successfully. A representative will contact you soon. Thank you.";
echo json_encode($response);
return;
?>
