<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
//print "HELLO"; exit;
#----------------------------------------------------------------------
#Include files
//session_start();
include "../user/store_validatesession.php";
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_xml.php";
include "../includes/lib_file_upload.php";
include "../includes/lib_image.php";
include "../includes/lib_datetimeyear.php";
include "../user/store_config.php";
//include "./../includes/http_to_https.php";

//print_r($_SESSION);exit;
#----------------------------------------------------------------------
$str_title_page = "";
$str_title_page = $STR_TITLE_PAGE_CART;
//print $str_title_page; exit;


$str_xml_file_name_cms = "";
$str_xml_file_name_cms = "store_cms.xml";

#------------------------------------------------------------------------------------------------
/*$int_userpkid = 0;
$int_userpkid = $_SESSION["userpkid"];

$str_username = "";
$str_username = $_SESSION["userid"];
*/
$int_prodpkid="";
if(isset($_GET["pkid"]))
{
        $int_prodpkid=RemoveQuote(trim($_GET["pkid"]));	
}
/*if($int_prodpkid=="" || !is_numeric($int_prodpkid) || $int_prodpkid<=0)
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/	

// PRODUCT CATEGORY PKID
$int_cpkid="";
if(isset($_GET["catpkid"]))
{
        $int_cpkid=RemoveQuote(trim($_GET["catpkid"]));	
}
/*if($int_cpkid=="" || !is_numeric($int_cpkid) || $int_cpkid<=0)
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/	

// PRICE PKID
/*$int_pricepkid="";
if(isset($_GET["pripkid"]))
{
        $int_pricepkid=RemoveQuote(trim($_GET["pripkid"]));	
}

if($int_pricepkid=="" || !is_numeric($int_pricepkid))
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/	

// PRICE VALUE
$int_priceval="";
if(isset($_GET["prival"]))
{
        $int_priceval=RemoveQuote(trim($_GET["prival"]));	
}
/*if($int_priceval=="" || !is_numeric($int_priceval))
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/	

// SHIPPING VALUE
$int_shippingval="";
if(isset($_GET["shippingval"]))
{
        $int_shippingval=RemoveQuote(trim($_GET["shippingval"]));	
}
/*if($int_shippingval=="" || !is_numeric($int_shippingval))
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/

// PRODUCT TITLE
$str_title="";
if(isset($_GET["title"]))
{
        $str_title=RemoveQuote(trim($_GET["title"]));	
}
/*if($str_title=="")
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/	

// PRODUCT CATEGORY TITLE
$str_ctitle="";
if(isset($_GET["ctitle"]))
{
        $str_ctitle=RemoveQuote(trim($_GET["ctitle"]));	
}
/*if($str_ctitle=="")
{
        CloseConnection();		
        Redirect("./shop.php");
        exit();
}*/	

$str_color="";
$str_size="";
if (isset($_GET["color"]))
{
        $str_color= trim($_GET["color"]);
}
if (isset($_GET["size"]))
{
        $str_size= trim($_GET["size"]);
}
$str_type="";
if (isset($_POST["os2"]))
{
        $str_type= trim($_GET["os2"]);
}

/*$int_modelpkid=0;
$str_modelname="";
if (isset($_GET["mid"]))
{
        $int_modelpkid= trim($_GET["mid"]);
}
if (isset($_GET["mname"]))
{
        $str_modelname= trim($_GET["mname"]);
}*/
#------------------------------------------------------------------------------------------------

# Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
	case("E"): $str_type = "E"; break;
	case("W"): $str_type = "W"; break;
    }
}
#	Get message text.

if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;        
    }
} 


$item_sessionid="";
$item_uniqueid="";

if(isset($_SESSION['sessionid']))
{
        $item_sessionid=$_SESSION['sessionid'];
}
if(isset($_SESSION['uniqueid']))
{
        $item_uniqueid=$_SESSION['uniqueid'];
}

if($item_sessionid=="" || $item_uniqueid=="")
{
        CloseConnection();
        Redirect("../online_store");
        exit();
}
#----------------------------------------------------------------------
#read main xml file
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = "";
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION3",$fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE3", $fp); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($str_title_page) ;?></title>
    <?php //print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet">      
</head>
<body>
<?php include $STR_USER_HEADER_PATH; ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="row padding-10">
                    <div class="col-md-12">
                        <h1 align="right"><?php print $str_title_page; ?></h1><hr/>           
                    </div>
                </div>
                <?php if($str_visible_cms == "YES") { ?>
                    <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
                    <div class="row padding-10">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms);?></p></div>
                        </div>
                    </div>
                    <?php } ?>
                <?php } ?>
                <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); } ?>
                <div class="row padding-10">
        <div class="col-md-12">
            <?php
		$var_gift_sku="FALSE";
		$var_promo_sku="FALSE";
		$var_extprice=0;
		$hid_options="";
		$hid_serialno="";
		
		#Below recordset fetches all purchased related data from t_store_sessioncart table for particular table.
		$strsql="SELECT * FROM t_store_sessioncart WHERE sessionid='".$item_sessionid."' AND uniqueid='".$item_uniqueid."' ORDER BY serialno,sku,producttitle";
                //print $strsql; exit;
		$cartrs=GetRecordSet($strsql); 
                //print $cartrs->Count();?>
                <?php if($cartrs->EOF()==true) { ?>
                    <div class="rowpadding-10" align="center">
                        <div class="col-md-12 alert alert-danger ">No items added into your shopping cart</div>
                    </div>
         <?php  }
         else   { ?>
                <?php 
                if($cartrs->EOF()==false)
		{
                    $int_highest_shipping_value=0;
                    while(!$cartrs->EOF()) 
                    { 
                        // Use below equation if want to update shipping charge according to no. of quantity for each item
                        //$int_highest_shipping_value = $int_highest_shipping_value + ($cartrs->fields("quantity") * $cartrs->fields("shippingvalue"));
                        //print $cartrs->fields("shippingvalue")."<br/>";
                        $int_highest_shipping_value = $int_highest_shipping_value + $cartrs->fields("shippingvalue");
                        $cartrs->MoveNext(); 
                    } 
                    $cartrs->MoveFirst(); 
		}
                if($cartrs->EOF()==false) 
                { ?>
		<form name="frmupdatecart" id="frmupdatecart">
		<br>
		<div class="row padding-10">
                    <div class="col-lg-12 ">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr class="cart_heade_bg">
                                        <th style="text-align:center" width="480">Details</th>
                                        <th style="text-align:center">Quantity</th>
                                        <th style="text-align:center">Unit Price</th>
                                        <th style="text-align:center">Total Price</th>
                                    </tr>
                                </thead>
                                <tbody>
				<?php
                                $int_cnt=1;
                                while(!$cartrs->EOF()==true)
                                {
                                    $var_cnt=0;
                                    $var_extprice=$var_extprice+$cartrs->fields("extendedprice");

                                    //print $var_extprice;
                                    if(substr($cartrs->fields("sku"),0,4)=="GIFT")
                                    {
                                        $hid_options=$hid_options.$cartrs->fields("serialno")."_cmb_";
                                        $var_gift_sku="TRUE";
                                    }
                                    else
                                    {
                                        $var_gift_sku="FALSE";
                                    }
                                    
                                    if(substr($cartrs->fields("sku"),0,9)=="PromoItem")
                                    {
                                        $var_promo_sku="TRUE";
                                    }
                                    else
                                    {
                                        $var_promo_sku="FALSE";
                                    }
                                    $int_pkid = 0;
                                    $int_pkid = GetEncryptId($cartrs->Fields("productpkid"));
                                    //print $int_pkid; exit;
                                ?>
                                <tr>
                                    <td>
                                        <h3 class="nopadding"><?php print(htmlentities($cartrs->fields("producttitle"))); ?></h3>
                                        
                                        &bull;&nbsp;<span class="text-help">category:</span> <?php /* ?><a href="<?php echo str_replace(' ','-',$cartrs->Fields("productcattitle")); ?>" title="Click to view product of this category" class="link"><?php print(htmlentities($cartrs->fields("subcattitle"))); ?></a><?php */ ?><?php print(htmlentities($cartrs->fields("subcattitle"))); ?>
                                 <?php /* ?>       &nbsp;&nbsp;&nbsp;&bull;&nbsp;<span class="text-help">sub category: </span><?php /* ?><a href="<?php echo str_replace(' ','-',$cartrs->Fields("productcattitle")); ?>" title="Click to view product of this category" class="link"><?php print(htmlentities($cartrs->fields("subcattitle"))); ?><?php /* ?></a><?php */ ?>
                                        <p>
                                        <?php if($cartrs->fields("color")!= "" || $cartrs->fields("color")!=NULL) { ?>&bull;<span class="text-help">&nbsp;color: </span><?php print(htmlentities($cartrs->fields("color"))); ?><?php } ?>
                                        <?php if($cartrs->fields("size")!="" || $cartrs->fields("size")!=NULL) { ?>| <span class="text-help">size: </span><?php print(htmlentities($cartrs->fields("size"))); ?><?php } ?>
                                        <?php if($cartrs->fields("type")!="" || $cartrs->fields("type")!=NULL) { ?>| <span class="text-help">type:</span> <?php print(htmlentities($cartrs->fields("type"))); ?><?php } ?>
                                        </p>
                                    </td>
                                    <td class="HighLightText01" style="vertical-align:middle" align="center"><input name="<?php print("quan_".$cartrs->fields("serialno")); ?>" type="text" class="form-control" value="<?php print($cartrs->fields("quantity")); ?>" size=5 maxlength="3" style="max-width:60px ;text-align: center;"></td>
                                    <td style="vertical-align:middle; text-align:right"><?php print($cartrs->fields("price")); ?><?php //print(GetPriceValue($var_extprice)); ?></span><input name="<?php print("price_".$cartrs->fields("serialno")); ?>" type="hidden" value="<?php print($cartrs->fields("price")); ?>"></td>                              
                                    <td class="" style="vertical-align:middle; text-align:right"><?php print(GetPriceValue($cartrs->fields("extendedprice"))); ?></td>
                                </tr>
				<?php 
                                if($var_cnt==0)
                                {
                                    $hid_options=$hid_options.$cartrs->fields("serialno")."_cmb_";
                                }
                                $hid_options=$hid_options."_.serialend._";
                                $hid_serialno=$hid_serialno."quan_".$cartrs->fields("serialno").";";
                                $cartrs->MoveNext();
                                $int_cnt=$int_cnt+1;		
				} 
				?>		
				<tr>
                                    
                                    <td align="center" colspan="4">
                                        <input name="hid_option" type="hidden" value="<?php print($hid_options); ?>">
					<input name="hid_serialno" type="hidden" value="<?php print($hid_serialno); ?>">
					<input name="extprice" type="hidden" value="<?php print($var_extprice); ?>">
					<?php /*?><input type="button" name="Submit3" value="Update" class="ButtonStyle" onClick="return formvalidation('<?php print($int_pkid);?>');"><?php */?>
                                        <button type="submit" name="Submit3" class="btn btn-primary" onClick="return formvalidation('<?php print($int_pkid);?>');"><i class="fa fa-cart-plus"></i>&nbsp;<b>Update Cart</b></button>
                                    </td>
                                </tr>
				<?php 
                                //------------------------------------------------------------------------------------------
                                //Initialization of variables used for message display.   
                                $str_type = "";
                                $str_message = "";
                                $str_minimum_purchase_required = "";
                                if(isset($_GET["mprm"]))
                                {   $str_minimum_purchase_required = trim($_GET["mprm"]); } 
                                //Get message type.
                                if(isset($_GET["type"]))
                                {
                                    switch(trim($_GET["type"]))
                                    {
                                        case("S"): $str_type = "S"; break;
                                        case("E"): $str_type = "E"; break;
                                        case("W"): $str_type = "W"; break;
                                    }
                                }
                                //Get Message Text.
                                if(isset($_GET["msg"]))
                                {
                                    switch(trim($_GET["msg"]))
                                    {				
                                        
                                        case("IVMP"): $str_message = "ERROR!!! Purchase minimum amount to apply promo code"; break;
                                        case("IVPC"): $str_message = "ERROR!!! Invalid promo code for this item"; break;
                                        
                                    }
                                }
                                //Diplay Message.
                                if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); }
                                ?>
                                <?php /*?><table>
                                <tr><td>Enter Promo Code (if any):&nbsp;&nbsp;</td><td><input name="txt_promocode" type="text" width="400px" class="form-control" value="<?php print($cartrs->fields("quantity")); ?>"></td></tr>
                                </table><?php */?>
                                <?php /*?>Enter Promo Code (if any):&nbsp;&nbsp;</td><td><?php */?>
				<tr>
                                    <td></td>
                                    <td colspan="2">Total Purchase Amount </td>
                                    <td align="right">
                                        <?php print(GetPriceValue($var_extprice)); ?>
                                    </td>
				</tr>
				<tr>
                                    <td>
                                        <div class="input-group" >
                                            <input name="txt_promocode" type="text" class="form-control" placeholder="Enter promo code if you have any" value="<?php print($cartrs->fields("quantity")); ?>">
                                            <span class="input-group-btn">
                                                <button type="submit" name="SubmitPromoCode" id="SubmitPromoCode" class="btn btn-primary" onClick="return applypromocodeform('<?php print($int_pkid);?>');"><b>Apply Code</b></button>
                                            </span>
                                        </div>
                                    </td>
                                    <td colspan="2" style="vertical-align:middle;">- Total Promotional Discount 
                                        <?php
                                        $str_sql_promocode = "";
                                        $str_sql_promocode="SELECT promocodepkid FROM ".$STR_DB_TABLE_NAME_SESSION_CART." WHERE sessionid='".$item_sessionid."' AND uniqueid='".$item_uniqueid."' ORDER BY serialno,sku,producttitle";
                                
                                        $rs_promocode=GetRecordSet($str_sql_promocode); 
                                        
                                        //print $rs_promocode->fields("promocodepkid"); exit;
                                        $int_promocodepkid = 0;
                                        $int_promocodepkid = $rs_promocode->fields("promocodepkid");
                                        //print $int_promocodepkid;exit;
                                        if($int_promocodepkid > 0)
                                        {
                                            $sel_qry_promocode = "";
                                            $sel_qry_promocode = "SELECT * FROM ".$STR_DB_TABLE_NAME_PROMOCODE." WHERE pkid = ".$int_promocodepkid;
                                            //print $sel_qry_promocode;
                                            $rs_promocode_details = GetRecordSet($sel_qry_promocode);
                                            //print $rs_promocode_details->Fields("title");exit;
                                        ?>
                                        [<?php
                                            if($rs_promocode_details->Fields("title") != "") {

                                                print "Used Promo Code: ".$rs_promocode_details->Fields("title");
                                            }
                                            if($rs_promocode_details->Fields("discountamount") != "" && $rs_promocode_details->Fields("discountamount") > 0) {

                                                print " - US$ ".$rs_promocode_details->Fields("discountamount");
                                            }
                                            if($rs_promocode_details->Fields("discountpercentage") != "" && $rs_promocode_details->Fields("discountpercentage") > 0) { 

                                                print " - ".$rs_promocode_details->Fields("discountpercentage")."%";
                                            }
                                            ?>]
                                        <?php }  ?>
                                    </td>
                                    <td align="right" style="vertical-align:middle;">
                                    <?php 
                                        $strsql="SELECT finaldiscountedamount FROM ".$STR_DB_TABLE_NAME_SESSION_CART." WHERE sessionid='".$item_sessionid."' AND uniqueid='".$item_uniqueid."'";
					$rs_disamount=GetRecordSet($strsql); ?>
					<?php print(GetPriceValue($rs_disamount->fields("finaldiscountedamount"))); ?>
                                    </td>
				</tr>
                                
				<tr>
                                    <td></td><td colspan="2">+ Shipping Charge</td>
                                    <td align="right">
                                    <?php print(GetPriceValue(number_format($int_highest_shipping_value,2))); ?>
                                    </td>
				</tr>
				<tr>
                                    <td></td><td colspan="2">= Total Payable Amount</td>
                                    <td align="right">
                                        <?php // $var_extprice = $var_extprice + $int_highest_shipping_value; ?>
                                        <?php //
                                        #With Promotional Discount
                                        $var_extprice = $var_extprice - $rs_disamount->fields("finaldiscountedamount") + $int_highest_shipping_value;
                                        #Without Promotional Discount
                                        //$var_extprice = $var_extprice + $int_highest_shipping_value;
                                        ?>
                                        <?php print(GetPriceValue($var_extprice)); ?>
                                    </td>
				</tr>
                    		
                                                        
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        
		
        <?php /*?><div class="row">
			<?php 
			//------------------------------------------------------------------------------------------
			//Initialization of variables used for message display.   
			$str_type = "";
			$str_message = "";
			$str_minimum_purchase_required = "";
			if(isset($_GET["mprm"]))
			{	$str_minimum_purchase_required = trim($_GET["mprm"]); } 
			//Get message type.
			if(isset($_GET["type"]))
			{
				switch(trim($_GET["type"]))
				{
					case("S"): $str_type = "S"; break;
					case("E"): $str_type = "E"; break;
					case("W"): $str_type = "W"; break;
				}
			}
			//Get Message Text.
			if(isset($_GET["msg"]))
			{
				switch(trim($_GET["msg"]))
				{				
					case("IVPC"): $str_message = " invalid promo code "; break;	
					case("IVMP"): $str_message = "purchase min. US$ ".$str_minimum_purchase_required." to apply promo code"; break;
				}
			}
			//Diplay Message.
			if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); }
			?>
		
			<div class="col-md-3">Enter Promo Code<br/>(if you have any) : </div>
			<div class="col-md-3"><input name="txt_promocode" type="text" class="form-control" value="<?php print($cartrs->fields("quantity")); ?>"></div>
			<div class="col-md-6">
			<?php /*?><input type="button" name="SubmitPromoCode" value="Apply" class="ButtonStyle" onClick="return applypromocodeform('<?php print($int_pkid);?>');">
			<button type="submit" name="SubmitPromoCode" id="SubmitPromoCode" class="btn content-bg-addtocart" onClick="return applypromocodeform('<?php print($int_pkid);?>');">Apply Code</button>
			</div>
		</div><?php */?>
		
		<?php /*?><div class="row breadcrumb">
            <div class="col-md-11" align="right">Total Purchase Amount :</div>
            <div class="col-md-1 nopadding" align="right"><?php print(GetPriceValue($var_extprice)); ?></div>
			<br/><br/>
			<div class="col-md-11" align="right">- Total Promotional Discount :</div>
            <div class="col-md-1 nopadding" align="right">
				<?php 
				$strsql="SELECT finaldiscountedamount FROM t_sessioncart WHERE sessionid='".$item_sessionid."' AND uniqueid='".$item_uniqueid."'";
				$rs_disamount=GetRecordSet($strsql); ?>
				<?php print(GetPriceValue($rs_disamount->fields("finaldiscountedamount"))); ?>
			</div>
			<br/><br/>
			<div class="col-md-11" align="right">+ Shipping Charge :</div>
            <div class="col-md-1 nopadding" align="right"><?php print(GetPriceValue($int_highest_shipping_value)); ?></span></div>
			<br/><br/>
            <div class="col-md-11" align="right">= Total Payable Amount :</div>
			<div class="col-md-1 nopadding" align="right">
				<?php // $var_extprice = $var_extprice + $int_highest_shipping_value; ?>
				<?php $var_extprice = $var_extprice - $rs_disamount->fields("finaldiscountedamount") + $int_highest_shipping_value; ?>
				<?php print(GetPriceValue($var_extprice)); ?>
			</div>
        </div><?php */?>
		
		
		
        <?php /*?><div class="row">
			<div class="col-md-12 breadcrumb">
	            <div class="col-md-4"></div>
            	<div class="col-md-8">
                	<div align="right">Total Purchase Amount :	<?php print(GetPriceValue($var_extprice)); ?></div>
                	<div align="right">- Total Promotional Discount :	
					<?php 
					$strsql="SELECT finaldiscountedamount FROM t_sessioncart WHERE sessionid='".$item_sessionid."' AND uniqueid='".$item_uniqueid."'";
					$rs_disamount=GetRecordSet($strsql); ?>
					<?php print(GetPriceValue($rs_disamount->fields("finaldiscountedamount"))); ?></span>&nbsp;
					</div>
					<div align="right">+ Shipping Charge : <?php print(GetPriceValue(number_format($int_highest_shipping_value))); ?></span></div>
                	<div align="right"> = Total Payable Amount :
					<?php // $var_extprice = $var_extprice + $int_highest_shipping_value; ?>
					<?php $var_extprice = $var_extprice - $rs_disamount->fields("finaldiscountedamount") + $int_highest_shipping_value; ?>
					<?php print(GetPriceValue($var_extprice)); ?>
					</div>
            	</div>
            </div>
        </div><?php */?>
        
            </form>

            <div class="row padding-10">
                <div class="col-md-12 breadcrumb"> 
           	<?php /*?><div><p class="text-help" align="left">*REQUIRED - Once you finish shopping and want to checkout, please enter your Name, Email, Shipping Address and any Special Instructions and/or Requests into the field displaying to the right :</p></div><?php */?>
                <?php 
                // MD5 Hash = formPrice formPeriod currencyCode salt
                $str_md5_hash = $var_extprice.$STR_CCBILL_STORE_DOWNLOAD_LIMIT_DAY.$STR_CCBILL_STORE_CHECK_CURRENCIES.$STR_CCBILL_STORE_SALT_VALUE;
                //$str1 = hash("md5",$str_md5_hash); // This function doesn't not work in PHP4
                $md5_hash_value = md5($str_md5_hash);
                //print "CCBILL ACT: ".$STR_CCBILL_CLIENT_ACC_NUM."<br>CCBILL SUB ACT: ".$STR_CCBILL_CLIENT_SUB_ACC_NUM_STORE."<br>FORM NAME: ".$STR_CCBILL_FORM_NAME_CREDIT_CARD_STORE."<br>TOTAL PRICE: ".$var_extprice."<br>MD5 HASH: ".$md5_hash_value;
                // print $str_memberpkid."<br>".$str_memberid;
                //print "ID: ".$int_modelpkid."<BR> NAME: ".$str_modelname; 
                //print_r($_SESSION);
                ?>
                    

                <form name="frmcheckout" action="<?php print($STR_BACKPROCESS_URL_STORE); ?>" method="POST">
                    <input type=hidden name=clientAccnum value='<?php print($STR_CCBILL_STORE_CLIENT_ACCOUNT_NUMBER);?>'>
                    <input type=hidden name=clientSubacc value='<?php print($STR_CCBILL_STORE_CLIENT_SUB_ACCOUNT_NUMBER);?>'>
                    <input type=hidden name=formName value='<?php print($STR_CCBILL_STORE_FORM_NAME_CREDIT_CARD);?>'>
                    <input type=hidden name=formPrice value='<?php print($var_extprice);?>'>
                    <input type=hidden name=formPeriod value='<?php print($STR_CCBILL_STORE_DOWNLOAD_LIMIT_DAY);?>'>
                    <input type=hidden name=currencyCode value='<?php print($STR_CCBILL_STORE_CHECK_CURRENCIES);?>'>
                    <input type=hidden name=formDigest value='<?php print($md5_hash_value);?>'>                    
                    <?php /* ?><input type=hidden name=freemeberpkid value='<?php print($int_memberpkid);?>'>
                    <input type=hidden name=freemeberid value='<?php print($str_membername);?>'><?php */ ?>
                    <input type=hidden name=itemsessionid value='<?php print($item_sessionid);?>'>
                    <input type=hidden name=itemuniqueid value='<?php print($item_uniqueid);?>'>
                    <?php /* ?><input type=hidden name=userpkid value='<?php print($int_userpkid);?>'>
                    <input type=hidden name=userid value='<?php print($str_username);?>'><?php */ ?>
                    <div> 
                        <textarea rows="5" cols="100" class="form-control" id="ta_ship_add" name="ta_ship_add" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>" style="resize:none"></textarea>
                    <?php /* ?><input type="button" name="Submit" value="Continue Shopping" class="ButtonStyle" onClick="updatecart('<?php print($var_red); ?>');">
                    </div><?php */
                    ?>
                        <p class="text-help"><b>*Optional</b> -  If ordering item(s) that require shipping please enter your Name, Email, Shipping Address and any Special Instructions and/or Requests into the box below.  You can also leave the model a message here.</p>
                    <?php /* ?><textarea  class="form-control" colspan="4" name="ta_ship_add" cols="136" rows="3"  placeholder="Enter shipping address"></textarea><br/><?php */ ?>
                    </div>    
                    <div align="right">
                    <?php //print($var_red); ?>
                    <?php /*?><button type="submit" class="btn content-bg-addtocart" onClick="updatecart('<?php print($var_red); ?>');">Continue Shopping</button>
                        <a href="<?php print htmlspecialchars($_SERVER['HTTP_REFERER']) ?>" class="btn btn-warning"><b><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;Continue Shopping</b></a><?php */?>
                        <button type="submit" name="submit" id="submit" class="btn btn-default"><b><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Check Out</b></button>
                            <?php /*?><input type="submit" name="Submit" value='Check Out' class='ButtonStyle'>&nbsp;<?php */?>
                    </div>
                    <br/>
                    <div align="right">
                        <p class="text-help" align="left"><?php print($STR_TRANS_LIMIT_MESSAGE); ?><br/><?php print($STR_CART_MESSAGE); ?></p>
                    </div>
		</form>	
	    
        </div>
	<?php 
	} ?>
    <?php  } ?>
    </div>        
    </div>
            </div>
        </div>
    </div>    
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include $STR_USER_FOOTER_PATH; CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js"></script>
    <script language="JavaScript" type="text/javascript">
    function formvalidation(arg)
    {
        
            var str_serialno=document.frmupdatecart.hid_serialno.value;
            //alert(str_serialno);
            var sr1=(str_serialno.split(";"));
            for(sr_count=0;sr_count<sr1.length-1;sr_count++)
            {
                var i=sr1[sr_count];
                if(!isDigit(eval('document.frmupdatecart.'+i).value))
                {
                    alert('Please enter positive numeric value for Quantity');
                    eval('document.frmupdatecart.'+i).select();
                    eval('document.frmupdatecart.'+i).focus();
                    return false;
                }
            }
            //updatecart('./store_item_updatecart_p.php?mid=<?php //print $int_userpkid; ?>&mname=<?php //print $str_username;  ?>&id='+arg);
            updatecart('../user/store_updatecart_p.php?id='+arg);
    }
    function updatecart(url1)
    {
            document.frmupdatecart.action=url1;
            document.frmupdatecart.method="post";
            document.frmupdatecart.submit();
    }


    function applypromocodeform(arg)
    {
            applypromocode('../user/store_promocode_p.php?id='+arg);
    }
    function applypromocode(url2)
    {
            document.frmupdatecart.action=url2;
            document.frmupdatecart.method="post";
            document.frmupdatecart.submit();
    }


    function checkccbillform()
    {
            with(document.ccbillform)
            {
                    /*if(trim(ta_ship_add.value)=="")
                    {
                            alert("Complete 'Required' field to continue.");
                            ta_ship_add.select();
                            ta_ship_add.focus();	
                            return false;
                    }*/
            }
            return true;
    }


    </script> 
</body>
</html>
