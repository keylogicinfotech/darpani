<?php
	session_start();
	if (!(isset($_SESSION['sessionid'])))
	{
		$_SESSION['sessionid'] = session_id();
	}
	if (!(isset($_SESSION['uniqueid'])))
	{
		$arr=getdate();
		$uid= $arr['year'] . substr("0" . $arr['mon'],-2) . substr("0" . $arr['mday'],-2) . substr("0" . $arr['hours'],-2) . substr("0" . $arr['minutes'],-2) . substr("0" . $arr['seconds'],-2) ;
		$_SESSION['uniqueid'] = $uid;
	}
?>