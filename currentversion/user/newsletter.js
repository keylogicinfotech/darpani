
$(function() {
//    alert("error");
    $("#frm_newletter input,#frm_newletter textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // something to have when submit produces an error ?
            // Not decided if I need it yet
        },
        submitSuccess: function($form, event) {
//            alert("error");
            event.preventDefault(); // prevent default submit behaviour
            var email = $("input#email").val();
          /*  if (email == "") {
                alert('Please enter email address.');
                return false;
            }*/
			
            $.ajax({
                url: "./user/newsletter_p.php",
                type: "POST",
                data: {
                    email: email,
                },
                cache: false,
                
                
                success: function(data) 
                {
//                    alert(data);
                    var $responseText=JSON.parse(data);
                    if($responseText.status == 'SUC')
                    {
                        $('#success').html("<div class='alert alert-success'>");
                        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                        $('#success > .alert-success').append("<strong> " + $responseText.message + " </strong>");
                        $('#success > .alert-success').append('</div>');

                        $("#resetFormNew").trigger("click");
                        setTimeout(function(){ location.reload() }, 2000);
                    }

                    else if($responseText.status == 'ERR')
                    {
                        // Fail message
                        $('#success').html("<div class='alert alert-danger'>");
                        $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                .append("</button>");
                        $('#success > .alert-danger').append("<strong> " + $responseText.message + " ");
                        $('#success > .alert-danger').append('</div>');
                    }
                },
				
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});
/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
