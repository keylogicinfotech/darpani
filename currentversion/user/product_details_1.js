/*
  Jquery Validation using jqBootstrapValidation
   example is taken from jqBootstrapValidation docs 
  */
$(function() {

    $("#frm_add input, #frm_add select").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // something to have when submit produces an error ?
            // Not decided if I need it yet
        },
        
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var hdn_prodpkid = $("input#hdn_prodpkid").val();
            var hdn_prodtitle = $("input#hdn_prodtitle").val();
            var hdn_catpkid = $("input#hdn_catpkid").val();
            var hdn_subcatpkid = $("input#hdn_subcatpkid").val();
            var hdn_memberpkid = $("input#hdn_memberpkid").val();
            var hdn_memberid = $("input#hdn_memberid").val();
            
            //var hdn_mdlpkid = $("input#hdn_mdlpkid").val();
            //var hdn_modelname = $("input#hdn_modelname").val();
            //var hdn_pricepkid = $("input#hdn_pricepkid").val();
            var hdn_price = $("input#hdn_price").val();
            var txt_quantity = $("input#txt_quantity").val();
            var cbo_color = $("select#cbo_color").val();
            var cbo_size = $("select#cbo_size").val();
            var cbo_type = $("select#cbo_type").val();
            //var hdn_ishippingvalue = $("input#hdn_ishippingvalue").val();
            //var hdn_dshippingvalue = $("input#hdn_dshippingvalue").val();
            //var hdn_ishippinginfo = $("input#hdn_ishippinginfo").val();
            //var hdn_dshippinginfo = $("input#hdn_dshippinginfo").val();
            var hdn_description = $("input#hdn_description").val();
            var specssku = $("input#specssku").val();
            //alert(arr_payment_method);
            var rdo_shipping = $("input[id='rdo_shipping']:checked").val();
            
			
            $.ajax({
                url: "../../user/store_addtocart_p.php",
                type: "POST",
                data: {
                    hdn_prodpkid: hdn_prodpkid,
                    hdn_prodtitle: hdn_prodtitle,
                    hdn_catpkid: hdn_catpkid,
                    hdn_subcatpkid: hdn_subcatpkid,
                    hdn_memberpkid: hdn_memberpkid,
                    hdn_memberid:hdn_memberid,
                    //hdn_mdlpkid: hdn_mdlpkid,
                    //hdn_modelname: hdn_modelname,
                    //hdn_pricepkid: hdn_pricepkid,
                    hdn_price: hdn_price,
                    txt_quantity: txt_quantity,
                    cbo_color: cbo_color,
                    cbo_size: cbo_size,
                    cbo_type: cbo_type,
                    //hdn_ishippingvalue: hdn_ishippingvalue,
                    //hdn_dshippingvalue: hdn_dshippingvalue,
                    hdn_description: hdn_description,
                    specssku: specssku,
                    rdo_shipping: rdo_shipping
                    //hdn_ishippinginfo: hdn_ishippinginfo,
                    //hdn_dshippinginfo: hdn_dshippinginfo
                },
                cache: false,
                
                success: function(data) 
                { 
                    alert(data);
                    var $responseText=JSON.parse(data);
                    if($responseText.status == 'SUC')
                    {
                        // Success message
                        $('#success').html("<div class='alert alert-success'>");
                        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                        $('#success > .alert-success').append("<strong> " + $responseText.message + " </strong>");
                        $('#success > .alert-success').append('</div>');

                        //clear all fields
                        $('#frm_add').trigger("reset");
                        
                        $("#success").show();
                        setTimeout(function() { $("#success").hide(); }, 2000);
                    }
                    else if($responseText.status == 'ERR')
                    {
                        // Fail message
                        $('#success').html("<div class='alert alert-danger'>");
                        $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                .append("</button>");
                        $('#success > .alert-danger').append("<strong> " + $responseText.message + " ");
                        $('#success > .alert-danger').append('</div>');
                    }
                },
				
				/*success: function() {
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Your contact inquiry has been submitted, we will contact you Soon. Thank you for communication.</strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + " it seems that my mail server is not responding...</strong> Could you please email me directly to <a href='mailto:info@websitename.com?Subject=Message_Me from websitename.com;>info@websitename.com</a> ? Sorry for the inconvenience!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },*/
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
