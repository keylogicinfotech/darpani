<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../user/store_config.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";

//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_MT_STORE";
$str_img_path = "../../".$UPLOAD_IMG_PATH;
$str_db_table_name_metatag = "t_page_metatag";
$str_xml_file_name = "";
$str_xml_file_name_cat = "";
$str_xml_file_name_cms = "";
$int_records_per_page = 10;

$str_button_view_details = "View This Product";
$str_hover_view_details = "Click To View This Product";
//print_r($_GET);exit;
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_cat_pkid = 0;
if(isset($_GET["cid"]) && trim($_GET["cid"]) != "" )
{   
    $int_cat_pkid = trim(GetDecryptId($_GET["cid"]));    
}

$int_pkid = 0;
if(isset($_GET["pid"]) && trim($_GET["pid"]) != "" )
{   
    $int_pkid = trim(GetDecryptId($_GET["pid"]));
}

# Select Query
$str_query_select = "";
$str_query_select = "SELECT a.*, b.subcattitle, c.thumbphotofilename, c.largephotofilename FROM " .$STR_DB_TABLE_NAME. " a ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." b ON a.subcatpkid=b.subcatpkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_PHOTO." c ON a.pkid=c.masterpkid WHERE a.pkid=".$int_pkid." AND a.subcatpkid=".$int_cat_pkid." AND b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' ";
$rs_list = GetRecordSet($str_query_select);

if($rs_list->EOF())
{
    CloseConnection();
    Redirect("./../online-store#ptop"); 
    exit();
}

$str_title = "";
$str_title = $rs_list->Fields("title");

# Select Query For Similar Products
$str_query_select = "";
$str_query_select = "SELECT a.*, b.subcattitle, c.thumbphotofilename, c.largephotofilename FROM " .$STR_DB_TABLE_NAME. " a ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." b ON a.subcatpkid=b.subcatpkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_PHOTO." c ON a.pkid=c.masterpkid WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.subcatpkid=".$int_cat_pkid." AND a.pkid != ".$int_pkid." ";
$str_query_select .= "ORDER BY a.createdatetime ";
//print $str_query_select; exit;
$rs_list_similar = GetRecordSet($str_query_select);
//print $rs_list_similar->Count();exit;

/*$str_name_key = "";
if(isset($_POST["key"]) && trim($_POST["key"]) != "" )
{ $str_name_key = trim($_POST["key"]); }

# get data for paging
$int_page = "";
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"]) != "" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"]) > 0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page = 1; }
//print_r($_POST);
$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";
*/



/*$int_page = 1;
if($int_total_records>0)
{
    $int_total_pages = ceil($int_total_records / $int_records_per_page); 
    //print $int_total_pages;
    if ($int_page > $int_total_pages)
    {
        $int_page = $int_total_pages;
    }
    if($int_page < 1)
    {
        $int_page=1;
    }
}*/

$str_member_flag = false;
$int_memberpkid = 0;
$str_memberid = "";
if((isset($_SESSION['vipmemberid']) && $_SESSION['vipmemberid'] != ""))
{
    $str_member_flag = true;
    $int_memberpkid = $_SESSION['vipmemberpkid'];
    $str_memberid = $_SESSION['vipmemberid'];
}
/*else
{
    CloseConnection();
    Redirect("./../vipmember-joinnow#ptop"); 
    exit();
}*/


#----------------------------------------------------------------------
#get metatag page title from table

$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatag. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_similar_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_similar_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_similar_mt->fields("titletag")) ;?><?php if($str_title != "") { print " [".$str_title."]"; }?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet">
</head>
<body>
<?php include("../includes/header.php"); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
                <ol class="breadcrumb thumbnail-margin">
                    <?php if($rs_list->Fields("subcattitle") != ""){ ?>
                        <li class="active"><?php print $rs_list->Fields("subcattitle"); ?></li>
                    <?php } ?>
                    <?php if($str_title != ""){ ?>
                        <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL."/online-store"; ?>"><?php print $str_title; ?></a></li>
                    <?php } ?>
                </ol>
            </div>
        </div>
        
        <div class="row padding-10">
            <div class="col-lg-4 col-md-4 col-xs-12 col-sm-5">
                <?php 
                $str_select_images = "";
                $str_select_images = "SELECT * FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE visible='YES' AND masterpkid= ".$rs_list->Fields("pkid")." ORDER BY setasfront DESC";
                $rs_list_image = GetRecordSet($str_select_images); 
                //print $rs_list_image->Count();
                ?>
                <ul id="glasscase" class="gc-start">
                    <?php                
                    while($rs_list_image->eof() != true) 
                    { ?>
                        <li>
                            <img src="<?php print $str_img_path.$rs_list->fields("pkid")."/".$rs_list_image->Fields("largephotofilename"); ?>" alt="<?php print($rs_list->fields("title")); ?>" data-gc-caption="<?php print($rs_list->fields("title")); ?>" />

                        </li><?php 
                        $rs_list_image->MoveNext();
                    } ?>
                </ul> 
            </div>
            <div class="col-lg-8 col-md-8 col-xs-12 col-sm-7">
                <div class="row padding-10">
                    <div class="col-md-12">
                        <?php if($rs_list->Fields("title") != "") { ?>
                        <h3 class=""><b><?php print $rs_list->Fields("title"); ?></b> 
                            <?php if($rs_list->Fields("subcattitle") != "") { ?>
                            / <?php print $rs_list->Fields("subcattitle"); ?></h3><hr/>
                            <?php } ?>
                        <?php } ?>
                        <?php if($rs_list->Fields("ourprice") != "" && $rs_list->Fields("ourprice") > 0) { ?>
                            <label><h5 class="nopadding">List Price :</h5></label>&nbsp;<label><h4 class="nopadding text-muted"><strike><?php print "<strike>US $".$rs_list->Fields("listprice")."</strike>"; ?></strike></h4></label>
                            <br/>
                            <label><h5 class="nopadding">Our Price :</h5></label>&nbsp;<label><h4 class="nopadding text-primary"><b><?php print "US $".$rs_list->Fields("ourprice").""; ?></b></h4></label>
                            <br/>
                        <?php } else { ?>
                            <label><h5 class="nopadding">Price :</h5></label>&nbsp;<label><h4 class="nopadding text-primary"><b><?php print "US $".$rs_list->Fields("listprice").""; ?></b></h4></label>
                        <?php } ?>
                        <?php if($rs_list->Fields("memberprice") != "" && $rs_list->Fields("memberprice") > 0) { ?>  
                            <label><h5 class="nopadding">Member Price :</h5></label>&nbsp;<label><h4 class="nopadding text-primary"><b><?php print "US $".$rs_list->Fields("memberprice").""; ?></b></h4></label>
                        <?php } ?>    
                            
                        <?php if($rs_list->Fields("description") != "") { ?>
                            <hr/>
                            <p class="" align="justify"><?php print $rs_list->Fields("description"); ?></p>
                        <?php } ?>
                    </div>
                </div>
                <div class="row padding-10">
                    <div class="col-md-12">
                        <hr/>
                        <form name="frm_add" id="frm_add" novalidate>
                            <div class="row padding-10">
                                <div class="col-md-6">
                                    <div class="control-group form-group">
                                        <div class="controls">
                                            <label>Quantity<span class="text-help">&nbsp;*</span></label>
                                            <input type="text" class="form-control" id="txt_quantity" name="txt_quantity" value="1" required="" data-validation-required-message="Please enter quantity." placeholder="Quantity" maxlength="3">
                                            <p class="help-block"></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <?php if($rs_list->Fields("color") != "") { ?>
                                    <div class="control-group form-group">
                                        <div class="controls">
                                            <label>Color<span class="text-help">&nbsp;*</span></label>
                                            <select class="form-control" id="cbo_color" name="cbo_color" required data-validation-required-message="Please select color.">
                                                <option value="">-- Select Color--</option>
                                                <?php 
                                                $arr_color = array();
                                                $arr_color = explode("|", trim($rs_list->Fields("color"))); 
                                                foreach($arr_color as $key => $str_color)
                                                { ?>
                                                    <option value="<?php print $str_color; ?>"><?php print $str_color; ?></option>
                                        <?php   } ?> 
                                                
                                            </select>
                                            <p class="help-block"></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="row padding-10">
                                <div class="col-md-6">
                                    <?php if($rs_list->Fields("size") != "") { ?>
                                    <div class="control-group form-group">
                                        <div class="controls">
                                            <label>Size<span class="text-help">&nbsp;*</span></label>
                                            <select class="form-control" id="cbo_size" name="cbo_size" required data-validation-required-message="Please select size.">
                                                <option value="">-- Select Size--</option>
                                                <?php 
                                                $arr_size = array();
                                                $arr_size = explode("|", trim($rs_list->Fields("size"))); 
                                                foreach($arr_size as $key => $str_size)
                                                { ?>
                                                    <option value="<?php print $str_size; ?>"><?php print $str_size; ?></option>
                                        <?php   } ?> 
                                                
                                            </select>
                                            <p class="help-block"></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-6">
                                    <?php if($rs_list->Fields("type") != "") { ?>
                                    <div class="control-group form-group">
                                        <div class="controls">
                                            <label>Type<span class="text-help">&nbsp;*</span></label>
                                            <select class="form-control" id="cbo_type" name="cbo_type" required data-validation-required-message="Please select type.">
                                                <option value="">-- Select Type --</option>
                                                <?php 
                                                $arr_type = array();
                                                $arr_type = explode("|", trim($rs_list->Fields("type"))); 
                                                foreach($arr_type as $key => $str_type)
                                                { ?>
                                                    <option value="<?php print $str_type; ?>"><?php print $str_type; ?></option>
                                        <?php   } ?> 
                                                
                                            </select>
                                            <p class="help-block"></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="row padding-10">
                                <div class="col-md-12">
                                    <hr/>
                                    <div class="control-group form-group">
                                        <div class="controls">
                                            <label>Select Shipping<span class="text-help">&nbsp;*</span></label>
                                                <div class="row padding-10">
                                                    <?php if($rs_list->Fields("dshippingvalue") != "" &&  $rs_list->fields("dshippingvalue") > 0) { ?>
                                                    <div class="col-md-6">
                                                        <input type="radio" id="rdo_shipping" checkone minchecked="1" data-validation-minchecked-message="Please select atleast one option." name="rdo_shipping" value="<?php print $rs_list->Fields("dshippingvalue"); ?>"/> <b>US $ <?php print $rs_list->Fields("dshippingvalue"); ?> Domestic Shipping</b>
                                                        <p class="text-help"><?php print $rs_list->Fields("dshippinginfo"); ?></p>
                                                    </div>
                                                    <?php } ?>
                                                    <?php if($rs_list->Fields("ishippingvalue") != "" &&  $rs_list->fields("ishippingvalue") > 0) { ?>
                                                    <div class="col-md-6">
                                                        <input type="radio" minchecked="1" data-validation-minchecked-message="Please select atleast one option." id="rdo_shipping" name="rdo_shipping" value="<?php print $rs_list->Fields("ishippingvalue"); ?>" /> <b>US $ <?php print $rs_list->Fields("ishippingvalue"); ?> International Shipping</b>
                                                        <p class="text-help"><?php print $rs_list->Fields("ishippinginfo"); ?></p>
                                                    </div>
                                                    <?php } ?>
                                                </div>


                                                <p class="help-block"></p>
                                        </div>
                                    </div>
                                    <div class="text-help">
                                        <?php /*if($rs_list->Fields("dshippingvalue") != "" &&  $rs_list->fields("dshippingvalue") > 0) { ?>
                                        Domestic Shipping Charge: <b class="text-primary"><?php print $rs_list->Fields("dshippingvalue"); ?></b><br>                            
                                        <?php } ?>
                                        <?php if($rs_list->Fields("ishippingvalue") != "" &&  $rs_list->fields("ishippingvalue") > 0) { ?>
                                        International Shipping Charge: <b class="text-primary"><?php print $rs_list->Fields("ishippingvalue"); ?></b><br/>                                          <?php } ?>
                                        <br/>
                                        <?php if($rs_list->Fields("dshippinginfo") != "") { ?>
                                            Domestic Shipping Info: <?php print $rs_list->Fields("dshippinginfo"); ?>
                                        <?php } ?>
                                        <?php if($rs_list->Fields("ishippinginfo") != "") { ?>
                                            International Shipping Info: <?php print $rs_list->Fields("ishippinginfo"); ?>
                                        <?php }*/ ?>
                                    </div>
                                    <input type="hidden" id="hdn_prodtitle" name="hdn_prodtitle" value="<?php print $rs_list->Fields("title"); ?>" />
                                    <input type="hidden" id="hdn_prodpkid" name="hdn_prodpkid" value="<?php print $int_pkid;  ?>" />
                                    <input type="hidden" id="hdn_catpkid" name="hdn_catpkid" value="<?php print $rs_list->Fields("catpkid");  ?>" />
                                    <input type="hidden" id="hdn_subcatpkid" name="hdn_subcatpkid" value="<?php print $int_cat_pkid;  ?>" />
                                    
                                    <?php
                                    if($str_member_flag == TRUE)
                                    {
                                        if($rs_list->Fields("memberprice") != "" && $rs_list->Fields("memberprice") > 0)
                                        { ?>
                                            <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $rs_list->Fields("memberprice");  ?>" />
                                <?php   }
                                        else
                                        {
                                            if($rs_list->Fields("ourprice") != "" && $rs_list->Fields("ourprice") > 0)
                                            { ?>
                                                <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $rs_list->Fields("ourprice");  ?>" />
                                    <?php   }
                                            else
                                            { ?>
                                                <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $rs_list->Fields("listprice");  ?>" />
                                    <?php   }
                                        }
                                    }
                                    else
                                    {
                                        if($rs_list->Fields("ourprice") != "" && $rs_list->Fields("ourprice") > 0)
                                            { ?>
                                                <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $rs_list->Fields("ourprice");  ?>" />
                                    <?php   }
                                            else
                                            { ?>
                                                <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $rs_list->Fields("listprice");  ?>" />
                                    <?php   }
                                    }
                                    ?>
                                    <input type="hidden" id="hdn_ishippingvalue" name="hdn_ishippingvalue" value="<?php print $rs_list->Fields("ishippingvalue");  ?>" />
                                    <input type="hidden" id="hdn_dshippingvalue" name="hdn_dshippingvalue" value="<?php print $rs_list->Fields("dshippingvalue");  ?>" />
                                    <input type="hidden" id="hdn_ishippinginfo" name="hdn_ishippinginfo" value="<?php print $rs_list->Fields("ishippinginfo");  ?>" />
                                    <input type="hidden" id="hdn_dshippinginfo" name="hdn_dshippinginfo" value="<?php print $rs_list->Fields("dshippinginfo");  ?>" />
                                    <input type="hidden" id="hdn_memberpkid" name="hdn_memberpkid" value="<?php print $int_memberpkid;  ?>" />
                                    <input type="hidden" id="hdn_memberid" name="hdn_memberid" value="<?php print $str_memberid;  ?>" />
                                    <?php $skuval=""; $skuval= GetEncryptId($int_pkid); ?> 
                                    <input type="hidden" id="specssku" name="specssku" value="<?php print $skuval;  ?>" />
                                    
                        
                                    <div id="success"></div>
                                    <button type="submit" class="btn btn-primary" title="Add To Cart"><i class="fa fa-cart-plus"></i>&nbsp;<b>Add To Cart</b></button>&nbsp;
                                    <a href="../../user/store_cart.php" class="btn btn-default" tabindex="6"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;<b>Check Out</b></a>&nbsp;
                                </div>
                            </div>
                            

                            
                        </form>    
                    </div> 
                </div>
            </div>
        </div>
        
        
        <?php 
        # Similar Products
        if($rs_list_similar->Count() > 0) { ?>
        <br/><br/>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h3 align="left">Similar Products</h3><hr/>
            </div>
        </div>
        <div class="row padding-10">
            <?php 
            $int_cnt = 0;
            while(!$rs_list_similar->EOF()) {
            ?>
            <div class="col-sm-4 col-md-3 col-xs-6 col-lg-3" align="center"> 
                <div class="thumbnail"> 
                    <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/online-store/<?php print(GetEncryptId($rs_list_similar->Fields("subcatpkid")));?>/<?php  print(GetEncryptId($rs_list_similar->Fields("pkid")));?>" title="<?php print $str_hover_view_details; ?>"><img alt="<?php print $rs_list_similar->Fields("title"); ?>" title="<?php print $rs_list_similar->Fields("title"); ?>" data-src="" src="<?php print $str_img_path.$rs_list_similar->Fields("pkid")."/".$rs_list_similar->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive"> </a>
                    <div class=" text-top-right">
                        <?php if($rs_list_similar->Fields("displayasnew") == "YES") { print $STR_ICON_PATH_NEW; } ?>
                        <?php if($rs_list_similar->Fields("displayashot") == "YES") { print $STR_ICON_PATH_HOT; } ?>
                        <?php if($rs_list_similar->Fields("displayasfeatured") == "YES") { print $STR_ICON_PATH_FEATURED; } ?>
                    </div>
                    <div class="caption">
                        <h4><a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/online-store/<?php print(GetEncryptId($rs_list_similar->Fields("subcatpkid")));?>/<?php  print(GetEncryptId($rs_list_similar->Fields("pkid")));?>" title="<?php print $str_hover_view_details; ?>"><b><?php print $rs_list_similar->Fields("title"); ?></b></a></h4>
                        <h4><?php print $rs_list_similar->Fields("subcattitle"); ?></h4>
                        <?php if($rs_list_similar->Fields("ourprice") > 0) { ?>
                            <label><h5 class="nopadding">List Price :</h5></label>&nbsp;<label><b class="text-muted"><?php print "<strike>US $".$rs_list_similar->Fields("listprice")."</strike>"; ?></b></label>
                            <br/>
                            <label><h5 class="nopadding">Our Price :</h5></label>&nbsp;<label><b class="text-primary"><?php print "US $".$rs_list_similar->Fields("ourprice").""; ?></b></label>
                        <?php } else { ?>
                            <label><h5 class="nopadding">Price :</h5></label>&nbsp;<label><b class="text-primary"><?php print "US $".$rs_list_similar->Fields("listprice").""; ?></b></label>
                        <?php }  ?>
                        <?php if($rs_list_similar->Fields("memberprice") > 0) { ?>
                        <label><h5 class="nopadding">Member Price :</h5></label>&nbsp;<label><b class="text-primary"><?php print "US $".$rs_list_similar->Fields("memberprice").""; ?></b></label>  <hr/>  
                        <?php } else { print "<hr/>"; }  ?>
                        <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/online-store/<?php print(GetEncryptId($rs_list_similar->Fields("subcatpkid")));?>/<?php  print(GetEncryptId($rs_list_similar->Fields("pkid")));?>" class="btn btn-primary btn-sm" title="<?php print $str_hover_view_details; ?>"><b><?php print $str_button_view_details; ?></b>&nbsp;<i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i></a>
                    </div> 
                </div> 
            </div>
            <?php
            $int_cnt++; if($int_cnt%4 == 0) { print "</div><div class='row padding-10'>"; }
            $rs_list_similar->MoveNext();
            }
            ?>
        </div>
        <?php } ?><br/>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include("../includes/footer.php");CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/store_details.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/glass.js"></script>
    
    <script type="text/javascript">
        $(document).ready( function () {
            $('#glasscase').glassCase({ 'thumbsPosition': 'bottom', 'widthDisplay' : 410});
        });
    </script>
</body>
</html>
