<?php
/*
Module Name:- USER
File Name  :- signup_p.php
Create Date:- 02-FEB-2019
Intially Create By :- 015
Update History:
*/
#--------------------------------------------------------------------------------------------------------
#Include Files
session_start();
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_xml.php";
include "../includes/lib_email.php";
include "./../includes/count_site_unique_view.php";
//header('Content-type: application/json');
//print_r($_POST);exit;
#--------------------------------------------------------------------------------------------------------

$str_db_table_name = "";
$str_db_table_name = "t_user";
// check if fields passed are empty
/*$str_usertype = "";
if (isset($_POST["rdo_usertype"])) { $str_usertype = trim($_POST["rdo_usertype"]); }*/
$str_loginid = "";
if (isset($_POST["l_loginid"])) { $str_loginid = trim($_POST["l_loginid"]); }
$str_password = "";
if (isset($_POST["l_password"])) { $str_password = trim($_POST["l_password"]); }
/*$str_l_secretcode = "";
if (isset($_POST["l_secretcode"])) { $str_l_secretcode = trim($_POST["l_secretcode"]); }*/
//print $str_loginid;exit;

/*if(empty($str_usertype))
{
    //$_SESSION['image_secret_code']="";
    $response['status']='ERR';
    $response['message']= "Select User Type!";
    echo json_encode($response); 
    return;
    //echo "Invalid Login ID!";
    //return false;
}*/

//if(empty($str_loginid) || is_valid_userid($str_loginid)==false )
if(empty($str_loginid))
{
    //$_SESSION['image_secret_code']="";
    $response['status']='ERR';
    $response['message']= "Invalid Login ID!";
    echo json_encode($response); 
    return;
    //echo "Invalid Login ID!";
    //return false;
}
//if(empty($str_password) || is_valid_userpassword($str_password)==false )
if(empty($str_password))
{
    //$_SESSION['image_secret_code']="";
    $response['status']='ERR';
    $response['message']= "Invalid Password!";
    echo json_encode($response); 
    return;
}
/*if( empty($str_l_secretcode) || $_SESSION['image_secret_code'] != $str_l_secretcode )
{
	//$_SESSION['image_secret_code']="";
	$response['status']='ERR';
    $response['message']= "Invalid Secrete Code!";
	
    echo json_encode($response);
    return;
}*/
$str_reg_date = "";
$str_reg_date = date("Y-m-d H:i:s");
$str_ip_address = "";
$str_ip_address = $_SERVER['REMOTE_ADDR'];
#----------------------------------------------------------------------------------------------------
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$str_db_table_name." WHERE loginid='".$str_loginid."' AND password='".$str_password."'";
//print $str_query_select; exit;
$rs_list = GetRecordSet($str_query_select);

if(!$rs_list->EOF())
{
    //print "MODEL".$str_query_select;
    if($rs_list->fields("allowlogin") == "NO")
    {
        $response['status']='ERR';
        $response['message']= "Sorry, you are not allowed to login!";
        echo json_encode($response);
        return;
    }    
}
else
{
    $response['status']='ERR';
    $response['message']= "Invalid loginid or password!";
    echo json_encode($response);
    return;
}

#----------------------------------------------------------------------------------------------------
# Update Query
$str_query_update = "";
$str_query_update = "UPDATE ".$str_db_table_name." SET lastlogindatetime='".date("Y-m-d H:i:s")."' WHERE pkid=".$rs_list->fields("pkid");
ExecuteQuery($str_query_update);

/*CloseConnection();
redirect("./about"); //Redirect Response To the called  Page or to the default page
exit();	*/

// Set Member's Session & Cookies Variables
$_SESSION["userid"] = $str_loginid;
$_SESSION["userpkid"] = $rs_list->fields("pkid");
//$_SESSION["username"] = $rs_list->fields("name"); // Commented on 12OCT2020
$_SESSION["username"] = $str_loginid;
$_SESSION["userapproved"] = $rs_list->fields("approved");	
//setcookie("modelid", $str_l_loginid, time() + (10 * 365 * 24 * 60 * 60)); // Cookie set for next 10 years
//setcookie("modelpkid", $rscheck->fields("modelpkid"), time() + (10 * 365 * 24 * 60 * 60)); // Cookie set for next 10 years
#----------------------------------------------------------------------------------------------------
//$_SESSION['image_secret_code']="";
$response['status']='SUC';
$response['message'] = "Logged In Successfully!";
echo json_encode($response);
return;
?>
