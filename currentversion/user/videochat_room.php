<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#-------------------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/http_to_https.php";	
include "./../includes/lib_image.php";
include "./../includes/lib_xml.php";
#-------------------------------------------------------------------------------
$str_page_title = "Video Chat Area";
$str_img_path = "./mdm/videochat/";
$str_xml_file_name = "phoneschedule.xml";
$str_xml_file_name_cms = "cam_cms.xml";

$str_title_page_metatag = "PG_VIDEOCHAT";
$str_db_table_name_metatag = "t_page_metatag";

#getting datas from module xmlfile.
$arr_xml_list="";
$arr_xml_list=readXml($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");
//print_r($arr_xml_list);

#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc1_cms = getTagValue("ITEMKEY_TA_LIVECONTENT", $fp);
$str_desc2_cms = getTagValue("ITEMKEY_TA_LEFTCONTENT", $fp);
$str_visible1_cms = getTagValue("ITEMKEY_VISIBLE", $fp);
$str_visible2_cms = getTagValue("ITEMKEY_VISIBLE1", $fp);
$str_status_cms = getTagValue("ITEMKEY_STATUS", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
# Get metatag page title
$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatag. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = "";
$str_title_page = $rs_list_mt->fields("titletag");
//print_r($arr_xml_list);
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>

<body>
<?php include($STR_USER_HEADER_PATH); ?>
<div class="container center-bg">
  <?php //   print_r($arr_xml_list); ?>
    <div class="row padding-10">
        <div class="col-lg-12 col-md-12">
            <h1 align="right"><?php print $str_page_title; ?></h1>
            <hr/>
        </div>
    </div>  
    <div class="row padding-10">
        <div class="col-md-12">
            <?php if($str_desc1_cms != "" && $str_desc1_cms != "<br>" && $str_visible1_cms == "YES") { ?>
                <div  class="breadcrumb"><p align="justify" class="nopadding"><?php print($str_desc1_cms);?></p></div>
            <?php } ?>
        </div>
    </div>
    <div class="row padding-10">
        <div class="col-md-12">
            <?php if($str_desc2_cms != "" && $str_desc2_cms != "<br>" && $str_visible2_cms == "YES") { ?>
                <div  class="breadcrumb"><p align="justify" class="nopadding"><?php print($str_desc2_cms);?></p></div>
            <?php } ?>
        </div>
    </div>
        
<!--------------------video chat section---------------------------------------->
  <?php /* ?>  <div class="row padding-10">
        <?php include "./player4/api/php/host_update.php"; ?>
        <div id="playerDiv2">
            <div id="header"></div>
            <div id="container">
                <div id="leftDiv">
                    <div id="mplayer"></div>
                    <div id="zoomBar">
                        <div id="zoom"></div>
                        <div id="fullscreen"></div>
                        <div id="slider"><div id="sliderKnob"></div></div>
                        <div id="mute"></div>		
                    </div>
                </div>
                <div id="fchat"></div>
            </div>
        </div>
    <script language="javascript">loadConfig()</script>
    </div>
        <?php */ ?>
<!----------------------------------------------------------------------------->        
    <?php 
//      print_r($arr_xml_list);
    $arr_test = array_keys($arr_xml_list);
    $int_cnt=0;
    if(trim($arr_test[0])!="ROOT_ITEM")
    { ?>
        <?php
        $int_cnt = 0;
//            print_r($str_xml_list);
        foreach($arr_xml_list as $key => $val)         
        {?>

    <?php } } ?>
    <?php 
    $arr_test=array_keys($arr_xml_list);

    $int_cnt = 0;
    if($arr_test[0]!= "ROOT_ITEM")
    { 
        while(list($key,$val) = each($arr_xml_list)) 
        {
            if(is_array($val))
            {  if(($arr_xml_list[$key]["TITLE"]!="") || ($arr_xml_list[$key]["DESCRIPTION"]!="")) { ?>
        <?php  ?>  <label><h3 align="left" class="nopadding"><?php print(MyHtmlEncode($arr_xml_list[$key]["TITLE"])) ?></h3></label>&nbsp;
        <p><?php print(($arr_xml_list[$key]["DESCRIPTION"])) ?></p>
         <?php  }
            }
        }
    } ?>

    <?php include($STR_USER_BANNER_PATH); ?>
</div>

    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    </body>
    <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
    </script>
    <div class="scrollup" style="display: block;"></div>
    <script type="text/javascript">
            $(document).ready(function(){ 

            $(window).scroll(function(){
                if ($(this).scrollTop() > 100) {
                    $('.scrollup').fadeIn();
                } else {
                    $('.scrollup').fadeOut();
                }
            }); 
            $('.scrollup').click(function(){
                $("html, body").animate({ scrollTop: 0 }, 600);
                return false;
            });
        });
    </script>
    
</html>
