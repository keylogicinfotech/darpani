/*  
Jquery Validation using jqBootstrapValidation
 example is taken from jqBootstrapValidation docs 
*/
$(function() {

    $("#frm_login input").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // something to have when submit produces an error ?
            // Not decided if I need it yet
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            //var rdo_usertype = $("input[name='rdo_usertype']:checked").val();
            var txt_loginid = $("input#txt_loginid").val();
            var txt_password = $("input#txt_password").val();
            //var l_usertype= $("select#l_usertype").val();
            var txt_secretcode = $("input#txt_secretcode").val();
            $.ajax({
                url: "./user/vipmember_joinnow_p.php",
                type: "POST",
		data: {
                   // rdo_usertype: rdo_usertype,
                    txt_loginid: txt_loginid,
                    txt_password: txt_password,
                    //l_usertype: l_usertype,
                    txt_secretcode: txt_secretcode
                },
				//dataType: "json",
                cache: false,
                success: function(data) 
                {
                     alert(data);
                    var $ResponseText_L=JSON.parse(data);
                    if($ResponseText_L.status == 'SUC')
                    {
                        // Success message
                        window.location.href="./user/vipmember_home.php";
                        
                        //window.location.href = "./user/postabill_home.php";
                        /*$('#successL').html("<div class='alert alert-success'>");
                        $('#successL > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                        $('#successL > .alert-success').append("<strong> " + $ResponseText_L.message + " </strong>");
                        $('#successL > .alert-success').append('</div>');

                        //clear all fields
                        $('#FormPLogin').trigger("reset");*/
                    }

                    else if($ResponseText_L.status == 'ERR')
                    {
                            // Fail message
                            $('#successL').html("<div class='alert alert-danger'>");
                            $('#successL > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                    .append("</button>");
                            $('#successL > .alert-danger').append("<strong> " + $ResponseText_L.message + " ");
                            $('#successL > .alert-danger').append('</div>');

                            // Auto refresh page after given miliseconds
                            //setTimeout(function(){ location.reload(); }, 500);

                    }
                    },
                })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});

/*When clicking on Full hide fail/success boxes */
$('#l_name').focus(function() {
    $('#success').html('');
});
