<?php
/*
	Module Name:- About
	File Name  :- resume_large_view.php
	Create Date:- 07-Mar-2006
	Intially Create By :- 0026
	Update History:
*/
#------------------------------------------------------------------------------
#	Include files
	include "./includes/configuration.php";
	include "./includes/lib_data_access.php";
	include "./includes/lib_image.php";
?>
<?php
#----------------------------------------------------------------------------------
# Get the variabl efro previous page
#----------------------------------------------------------------------------------

	$fileimage="";
	if (isset($_GET['imagefile']))
	{
			$fileimage=trim($_GET['imagefile']);
	}
#----------------------------------------------------------------------------------
# Code to validate necessory data has arrived proper or not
#----------------------------------------------------------------------------------
	if($fileimage == trim(""))
	{	
		CloseConnection();	
?>
		<script language="JavaScript">
			window.close()
		</script>
<?php 
	}
?>
<html>
<script language="JavaScript1.1" src="./includes/stoprightclick.js" type="text/javascript"></script>
<head>

<title>Large View</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="./includes/user.css" rel="stylesheet" type="text/css">
</head>

<body>
<?php include("./includes/popupheader.php")?>
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" background="./images/popup_center_bg.jpg">
  
  <tr>
    <td width="12"></td> 
    <td align="center" valign="top"> 
      <table border="0" cellpadding="0" cellspacing="1" background="./images/photo_border_bg.jpg">
		<tr>
			<td align="center" height="10" valign="bottom"></td>
		</tr>
        <tr>
          <td align="center" valign="top"><table border="0" cellspacing="1" cellpadding="0" background="./images/photo_border.jpg">
		  <tr>
			<td align="center" valign="top"><img src="<?php print($UPLOAD_PHOTO_PATH_USER."/".$fileimage);?>" border="0" alt="Large Image"></td>
		  </tr>
		  </table></td>
        </tr>
		<tr>
		<td height="30" align="center" valign="middle">
			<a href="javascript: void(0)" onClick="javascript: window.close();" class="Link8ptNormal">Close 
            Window</a></td>
		</tr>
    </table></td>
    
	<td width="12"></td>
  </tr>
</table>
</body>
</html>
