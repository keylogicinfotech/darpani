<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
//include "./../includes/http_to_https.php";	
#----------------------------------------------------------------------
$str_title_page = "";
$str_title_page = "PG_LOGIN";

$str_title_page_register = "";
$str_title_page_register = "PG_REGISTER";

$str_db_table_name = "t_page_metatag";
$str_img_path = "";
$str_xml_file_name = "";
$str_xml_file_name_cms = "user_cms.xml";
#----------------------------------------------------------------------
#read main xml file

#open cms xml file
$str_desc_cms = "";
$str_visible_cms = "";

$str_desc_cms_registration = "";
$str_visible_cms_registration = "";
        
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_LOGIN",$fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE",$fp);

$str_desc_cms_registration = getTagValue("ITEMKEYVALUE_REGISTRATION",$fp);
$str_visible_cms_registration = getTagValue("ITEMKEYVALUE_VISIBLE1",$fp);

CloseXmlFile($fp);

#----------------------------------------------------------------------
#get metatag page title
$str_querry_select = "";
$str_querry_select = "SELECT titletag FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page."' ";
//print $str_querry_select; exit;
$rs_list = GetRecordset($str_querry_select);
$str_title_page = $rs_list->fields("titletag");

#get metatag page title
$str_querry_select = "";
$str_querry_select = "SELECT titletag FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page_register."' ";
//print $str_querry_select; exit;
$rs_list_register = GetRecordset($str_querry_select);
$str_title_page_register = $rs_list_register->fields("titletag");
//print_r($_SESSION);
#---------------------------------------------------------------------- ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($str_title_page) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css"/>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css"/>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css"/>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/sidebar.css" rel="stylesheet" type="text/css"/>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" type="text/css"/>  
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-md-12">
                <h1 id="Login" align="right" class="text-default"><?php print($str_title_page); ?></h1>           
            </div>
        </div>
        <hr/>
        <?php if(strtoupper(trim($str_visible_cms)) == "YES"){ ?>
            <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
            <div class="row padding-10">
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div  class="well"><p align="justify"><?php print($str_desc_cms);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        <div class="row padding-10">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <form name="frm_login" id="frm_login" novalidate>
                    
                    
                    <div class="row padding-10">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="control-group form-group">
                                <label>Login ID</label><span class="text-help-form"> *</span>
                                <div class="controls input-group"> 
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="email" class="form-control" id="l_loginid" name="l_loginid" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_EMAIL_FORMAT; ?>" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?><?php //print $STR_PLACEHOLDER_LOGINID; ?>" >
                                </div>
                                <p class="help-block"></p>
                            </div>
                            <div class="control-group form-group">
                                <label>Password</label><span class="text-help-form"> *</span>
                                <div class="controls input-group">
                                    <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
                                    <input type="password" class="form-control" id="l_password" name="l_password" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_PASSWORD; ?>" placeholder="<?php //print $STR_PLACEHOLDER_PASSWORD; ?><?php print $STR_MSG_PASSWORD_FORMAT; ?>" minlength="6" maxlength="20">
                                </div>
                                <p class="help-block"></p> 
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                    
                    
                    
                   <?php /* ?> <div class="control-group form-group">
                        <label>Login ID</label><span class="text-help-form"> *</span>
                        <div class="controls input-group"> 
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input type="text" class="form-control" id="l_loginid" name="l_loginid" pattern="^[._a-zA-Z0-9]+$" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_LOGINID; ?>" value="" placeholder="<?php print $STR_PLACEHOLDER_LOGINID; ?>" minlength="4" maxlength="20">


                            <input type="email" class="form-control" id="l_loginid" name="l_loginid" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_EMAIL_FORMAT; ?>" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?><?php //print $STR_PLACEHOLDER_LOGINID; ?>" >
                            <p class="help-block"></p>
                        </div>
                    </div><?php */ ?>
                    
                    <div id="successL"></div>
                    <?php print DisplayFormButton("SIGNIN",0); ?>&nbsp;<?php print DisplayFormButton("RESET",0); ?>
                    &nbsp;&nbsp;&nbsp;<a href="./forgot-password">Forgot Password?</a><?php /*
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Don't have account <a href="./signup">Signup</a><br><br><?php */ ?>
                </form>
            </div>
        </div> 
        <br/>
        <br/>
        <div class="pink-line"></div>
        <br/>
        <div class="row padding-10">
            <div class="col-md-12">
                <h1 id="Register" align="right" class="text-default"><?php print($str_title_page_register); ?></h1>           
            </div>
        </div>
        <hr/>
        <?php if(strtoupper(trim($str_visible_cms_registration)) == "YES"){ ?>
            <?php if($str_desc_cms_registration != "" && $str_desc_cms_registration != "<br>") { ?>
            <div class="row padding-10">
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div  class="well"><p align="justify"><?php print($str_desc_cms_registration);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        <div class="row padding-10">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <form name="frm_signup" id="frm_signup" novalidate>
                    
                    <div class="row padding-10">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="control-group form-group">
                                <label>Enter Email ID As Your Login ID</label><span class="text-help-form"> *</span>
                                <div class="controls input-group">  
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="email" class="form-control" id="r_loginid" name="r_loginid" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_EMAIL_FORMAT; ?>" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?>" >
                                    <?php /* ?><input type="text" class="form-control" id="r_loginid" name="r_loginid" pattern="^[a-zA-Z0-9]+$" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_LOGINID; ?>" value="" placeholder="<?php print $STR_PLACEHOLDER_LOGINID; ?> | <?php print $STR_MSG_LOGINID_FORMAT;?>" minlength="4" maxlength="20"><?php */ ?>
                                </div>
                                <p class="help-block"></p>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-10">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="control-group form-group">
                                <label>Password</label><span class="text-help-form"> *</span>
                                <div class="controls input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" class="form-control" id="r_password" name="r_password" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_PASSWORD; ?>" placeholder="<?php //print $STR_PLACEHOLDER_PASSWORD; ?><?php print $STR_MSG_PASSWORD_FORMAT;?>" minlength="6" maxlength="20"> 
                                </div>
                                <p class="help-block"></p> 
                            </div>
                            <div class="control-group form-group">
                                <label>Confirm Password</label><span class="text-help-form"> *</span>
                                <div class="controls input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" class="form-control" id="r_conf_password" name="r_conf_password" required data-validation-required-message="<?php print $STR_MSG_ACTION_INVALID_CONFIRM_PASSWORD; ?>" data-validation-matches-message="<?php print $STR_MSG_ACTION_INVALID_CONFIRM_PASSWORD; ?>" data-validation-matches-match="r_password" placeholder="<?php //print $STR_PLACEHOLDER_CONFIRM_PASSWORD; ?><?php print $STR_MSG_PASSWORD_FORMAT;?>" minlength="6" maxlength="20">
                                </div>
                                <p class="help-block"></p>
                            </div>
                        </div>
                    </div>
                    <?php /* ?><div class="control-group form-group">
                        <div class="controls">
                            <label>Email</label><span class="text-help-form"> </span>
                            <input type="email" class="form-control" id="l_email" name="l_email" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?>" >
                            <p class="help-block"></p>
                        </div>
                    </div><?php */ ?>
                    <div id="success"></div>
                    <?php print DisplayFormButton("SIGNUP",0); ?>&nbsp;<?php print DisplayFormButton("RESET",0); ?>
                    <?php /* ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Already an account? <a href="./signin">Login</a><br><br><?php */ ?><br/><br/>
                    
                </form>
            </div>        
        </div>
        
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH);CloseConnection();?>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/signin.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/signup.js"></script>    
    <script>
    jQuery(function ($) {

    $(".sidebar-dropdown > a").click(function() {
  $(".sidebar-submenu").slideUp(200);
  if (
    $(this)
      .parent()
      .hasClass("active")
  ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .parent()
      .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .next(".sidebar-submenu")
      .slideDown(200);
    $(this)
      .parent()
      .addClass("active");
  }
});

$("#close-sidebar").click(function() {
  $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
  $(".page-wrapper").addClass("toggled");
});


   
   
});
</script>
</body>
</html>
