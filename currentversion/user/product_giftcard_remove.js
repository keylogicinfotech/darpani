/*
  Jquery Validation using jqBootstrapValidation
   example is taken from jqBootstrapValidation docs 
  */
$(function() {

    $("#frm_giftcard_remove input, #frm_giftcard_remove select").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // something to have when submit produces an error ?
            // Not decided if I need it yet
        },
        
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            
            var txt_giftcard_remove_amt = $("input#txt_giftcard_remove_amt").val();
            var hdn_userpkid = $("input#hdn_userpkid").val();
            var hdn_sessionid = $("input#hdn_sessionid").val();
            var hdn_uniqueid = $("input#hdn_uniqueid").val();
            
            $.ajax({
                url: "./../user/product_giftcard_remove_p.php",
                type: "POST",
                data: 
                {
                    txt_giftcard_remove_amt: txt_giftcard_remove_amt,
                    hdn_userpkid: hdn_userpkid,
                    hdn_sessionid: hdn_sessionid,
                    hdn_uniqueid: hdn_uniqueid
                    
                },
                cache: false,
                
                success: function(data) 
                { 
                   // alert(data);
                    var $responseText=JSON.parse(data);
                    if($responseText.status == 'SUC')
                    {
                        // Success message
                        $('#successgr').html("<div class='alert alert-success'>");
                        $('#successgr > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                        $('#successgr > .alert-success').append("<strong> " + $responseText.message + " </strong>");
                        $('#successgr > .alert-success').append('</div>');

                        //clear all fields
                        setTimeout(function() { location.reload(); }, 2000);
                    }
                    else if($responseText.status == 'ERR')
                    {
                        // Fail message
                        $('#successgr').html("<div class='alert alert-danger'>");
                        $('#successgr > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                .append("</button>");
                        $('#successgr > .alert-danger').append("<strong> " + $responseText.message + " ");
                        $('#successgr > .alert-danger').append('</div>');
                        setTimeout(function() { location.reload(); }, 2000);
                    }
                },
				
		/*success: function() {
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Your contact inquiry has been submitted, we will contact you Soon. Thank you for communication.</strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + " it seems that my mail server is not responding...</strong> Could you please email me directly to <a href='mailto:info@websitename.com?Subject=Message_Me from websitename.com;>info@websitename.com</a> ? Sorry for the inconvenience!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },*/
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#successg').html('');
});
