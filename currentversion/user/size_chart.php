<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/http_to_https.php";	
include "./../includes/unique_site_view_count.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------#----------------------------------------------------------------------
//$STR_XML_FILE_PATH_MODULE = "../../mdm/xmlmodulefiles/";
$str_title_page = " ";
$str_img_path = $STR_IMG_PATH_MEASURECHART;
$str_db_table_name = "t_page_metatag";
$str_xml_file_name_sizechart = "sizechart.xml";
$str_xml_file_name_cms_sizechart = "sizechart_cms.xml";

$str_xml_file_name_measurechart = "measurechart.xml";
$str_xml_file_name_cms_measurechart = "measurechart_cms.xml";
#----------------------------------------------------------------------
#read main xml file
$str_list_xml_sizechart = "";
$str_list_xml_sizechart = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name_sizechart,"ROOT_ITEM");

$str_list_xml_measurechart = "";
$str_list_xml_measurechart = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name_measurechart,"ROOT_ITEM");
//print_r($str_list_xml_sizechart);
#open cms xml file
$str_desc_cms_sizechart="";
$fp=OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms_sizechart);
$str_desc_cms_sizechart=getTagValue("ITEMKEYVALUE_DESCRIPTION",$fp);
$str_visible_cms_sizechart = getTagValue("ITEMKEYVALUE_VISIBLE",$fp);
CloseXmlFile($fp);

$str_desc_cms_measurechart="";
$fp=OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms_measurechart);
$str_desc_cms_measurechart=getTagValue("ITEMKEYVALUE_DESCRIPTION",$fp);
$str_visible_cms_measurechart = getTagValue("ITEMKEYVALUE_VISIBLE",$fp);
CloseXmlFile($fp);
//print_r($str_desc_cms_sizechart);exit;
#----------------------------------------------------------------------
#get metatag page title
$str_select_query = "";
$str_select_query = "SELECT title FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page."'";
//print $str_select_query;
$rs_list = GetRecordset($str_select_query);
$str_title_page = $rs_list->fields("title");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list->fields("title")) ;?></title>
    <?php print(Display_Page_Metatag('PG_FAQ')); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />        
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print "size chart"; ?></h1><hr/>           
            </div>
        </div>
      
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <p class="text-center"> <a href="" title="" data-toggle="modal" data-target="#myModal01" class="text-help"><b>View to Size Chart</b></a>&nbsp;&nbsp;&nbsp;
                    <a href="" title=""data-toggle="modal" data-target="#myModal03" class="text-help"><b>View to Measure</b></a>&nbsp;&nbsp;&nbsp;<br/>
                </p>
                <!-- Size Chart -->
                <div class="modal fade" id="myModal01" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php if($str_visible_cms_sizechart == 'YES'){ 
                            if($str_desc_cms_sizechart != "" | $str_desc_cms_sizechart != "<br/>"){
                                ?><p><?php print $str_desc_cms_sizechart;?></p><?php
                            }
                        }?>
                      </div>
                      <div class="modal-body">
                        <ul id="myTab" class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#service-one" data-toggle="tab"><h4 class="nopadding"><b>Size in cm</b></h4></a>
                            </li>
                            <li class=""><a href="#service-two" data-toggle="tab"><h4 class="nopadding"><b>Size in inches</b></h4></a>
                            </li>
                        </ul>

                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade active in" id="service-one">
                                <br/>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                            <tr><th>Size</th><th>Bust</th><th>Waist</th><th>Hip</th></tr>
                                            <?php 
                                            $arr_test=array_keys($str_list_xml_sizechart);
                                            if(trim($arr_test[0]) != "ROOT_ITEM"){
                                                foreach($str_list_xml_sizechart as $key => $val) 
                                                { 
                                                    if(is_array($val)){ ?> 
                                                <?php  if($str_list_xml_sizechart[$key]['VISIBLE'] == 'YES' ){?>

                                                     <?php // if($str_list_xml_sizechart[$key]['value1'] != 0.00 | $str_list_xml_sizechart[$key]['value2'] != 0.00 | $str_list_xml_sizechart[$key]['value3'] != 0.00 | $str_list_xml_sizechart[$key]['value4'] != 0.00){?>
                                                           <tr><td data-th="Size"><?php print($str_list_xml_sizechart[$key]['VALUE1']);?></td><td data-th="Bust"><?php print($str_list_xml_sizechart[$key]['VALUE2']);?></td><td data-th="Waist"><?php print($str_list_xml_sizechart[$key]['VALUE3']);?></td><td data-th="Hip"><?php print($str_list_xml_sizechart[$key]['VALUE4']);?></td></tr>

                                        <?php     }
                                                }
                                                }   
                                                }else{
                                                    ?><tr><td colspan="4" class="text-center"><?php print $STR_MSG_NO_DATA_AVAILABLE; ?></td></tr>
                                                <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="service-two">
                                <br/>
                                <div class="table-responsive">
                                 <table class="table table-striped table-bordered">
                                        <tbody><tr><th>Size</th><th>Bust</th><th>Waist</th><th>Hip</th></tr>
                                            <?php 
                                                $arr_test=array_keys($str_list_xml_sizechart);
                                                if(trim($arr_test[0]) != "ROOT_ITEM"){
                                                    foreach($str_list_xml_sizechart as $key => $val) 
                                                    { 
                                                        if(is_array($val)){ ?> 
                                            

                                                         <?php  if($str_list_xml_sizechart[$key]['VISIBLE'] == 'YES' ){?>
                                                          <?php 
                                                          $int_cm_value1 =$str_list_xml_sizechart[$key]['VALUE1'];
                                                          $int_inch_value1 = ceil($int_cm_value1/2.54);
                                                          
                                                          $int_cm_value2 =$str_list_xml_sizechart[$key]['VALUE2'];
                                                          $int_inch_value2 = ceil($int_cm_value2/2.54);
                                                          
                                                          $int_cm_value3 =$str_list_xml_sizechart[$key]['VALUE3'];
                                                          $int_inch_value3 = ceil($int_cm_value3/2.54);
                                                          
                                                          $int_cm_value4 =$str_list_xml_sizechart[$key]['VALUE4'];
                                                          $int_inch_value4 = ceil($int_cm_value4/2.54);?>
                                            
                                                        <tr><td data-th="Size"><?php print($int_inch_value1);?></td><td data-th="Bust"><?php print($int_inch_value2);?></td><td data-th="Waist"><?php print($int_inch_value3);?></td><td data-th="Hip"><?php print($int_inch_value4);?></td></tr>

                                        <?php           }else{
                                            ?><tr><td colspan="4" class="text-center"><?php print $STR_MSG_NO_DATA_AVAILABLE; ?></td></tr>
                                                        <?php }
                                                     }
                                            }  }else{
                                                ?><tr><td colspan="4" class="text-center"><?php print $STR_MSG_NO_DATA_AVAILABLE; ?></td></tr>
                                        <?php } ?></tbody>
                                    </table>
                                </div>    
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
     <!-------------end size chart------------------>  
     
      <!-- How To Measure -->
                <div class="modal fade" id="myModal03" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php if($str_visible_cms_measurechart == 'YES'){ 
                            if($str_desc_cms_measurechart != "" | $str_desc_cms_measurechart != "<br/>"){
                                ?><p><?php print $str_desc_cms_measurechart;?></p><?php
                            }
                        }?>
                      </div>
                      <div class="modal-body" align="center">
                        <?php 
                          $arr_test=array_keys($str_list_xml_measurechart);
                          if(trim($arr_test[0]) != "ROOT_ITEM"){
                              foreach($str_list_xml_measurechart as $key => $val) 
                              { 
                                  if(is_array($val)){ ?> 
                              <?php  if($str_list_xml_measurechart[$key]['VISIBLE'] == 'YES' ){?>
                                       <img class="img-responsive" src="<?php print($str_img_path.$str_list_xml_measurechart[$key]["IMAGEFILENAME"]); ?>" border="0" alt="<?php print $str_title_page;?> Image" title="<?php print $str_title_page;?> Image" />
                                       <p align="justify"><?php print($str_list_xml_measurechart[$key]['DESCRIPTION']);?></p>

                                   <?php // if($str_list_xml_measurechart[$key]['value1'] != 0.00 | $str_list_xml_measurechart[$key]['value2'] != 0.00 | $str_list_xml_measurechart[$key]['value3'] != 0.00 | $str_list_xml_measurechart[$key]['value4'] != 0.00){?>
                                         

                      <?php     }
                              }
                              }   
                          }else{
                              ?><p><?php print $STR_MSG_NO_DATA_AVAILABLE; ?></p>
                          <?php } ?>
                                                    
                                                    
                          
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End How To Measure -->
                                
           
            </div>
        </div>
       
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script> 
</body>
</html>
