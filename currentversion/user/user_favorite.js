//---------------------------------------------------------------------------------------------------------
///	Module Name:- Manage Change model Password
///	File Name  :- mdl_change_password.php
///	Create Date:- 17-JUL-2007
///	Intially Create By :- 
///	Update History:
//---------------------------------------------------------------------------------------------------------
/*  
ja validation for model profile delete confirmation..
*/

//
function confirm_delete()
{
	if(confirm("Are you sure you want to remove this item."))
        {   
            if(confirm("Confirm Remove:Click 'OK' to remove this profile or 'Cancel' to deletion."))
            {
                return true;
            }
	}
	return false;
}
