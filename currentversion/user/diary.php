<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_DIARY";
$str_img_path = $STR_SITENAME_WITH_PROTOCOL."/mdm/journal/";
$str_db_table_name = "t_page_metatag";
$str_xml_file_name = "journal.xml";
$str_xml_file_name_cms = "journal_cms.xml";
$int_records_per_page = 2;
$str_btn_title_view_more = "Read Full Story&nbsp;<i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i>";
#----------------------------------------------------------------------
#read main xml file
$str_xml_list = "";
$str_xml_list = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name,"ROOT_ITEM");

$int_total_records = 0;
$int_total_records = (count(array_keys($str_xml_list)));	
//print $int_total_records;
#----------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_cat_pkid = "";
if(isset($_POST["catid"]) && trim($_POST["catid"]) != "" )
{   
    $int_cat_pkid = trim($_POST["catid"]);    
}
else if(isset($_SESSION["catid"]) && trim($_SESSION["catid"]) != "" )
{   
    $int_cat_pkid = trim($_SESSION["catid"]);    
}

$str_name_key = "";
if(isset($_POST["key"]) && trim($_POST["key"]) != "" )
{ $str_name_key = trim($_POST["key"]); }
else if(isset($_SESSION["key"]) && trim($_SESSION["key"]) != "" )
{ $str_name_key = trim($_SESSION["key"]); }


# get data for paging
$int_page = "";
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"]) != "" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"]) > 0)
{ $int_page = $_POST["PagePosition"]; }
else if(isset($_SESSION["PagePosition"]) && trim($_SESSION["PagePosition"]) != "" && is_numeric($_SESSION["PagePosition"]) && trim($_SESSION["PagePosition"]) > 0)
{ $int_page = $_SESSION["PagePosition"]; }
else
{ $int_page = 1; }
//print_r($_POST);
$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";

$_SESSION["catid"] = $int_cat_pkid;
$_SESSION["key"] = $str_name_key;
$_SESSION["PagePosition"] = $int_page;

//print_r($_SESSION);
/*$int_page = 1;
if($int_total_records>0)
{
    $int_total_pages = ceil($int_total_records / $int_records_per_page); 
    //print $int_total_pages;
    if ($int_page > $int_total_pages)
    {
        $int_page = $int_total_pages;
    }
    if($int_page < 1)
    {
        $int_page=1;
    }
}*/
/*if($int_total_records > 0)
{
    $int_total_pages = ceil($int_total_records / $int_records_per_page); 
    //print $int_total_pages;exit;
    if ($int_page > $int_total_pages)
    {
        //$int_page = $int_total_pages;
        $_SESSION["catid"] = "";
        $_SESSION["key"] = "";
        $_SESSION["PagePosition"] = 1;
    }
    
}*/
if($int_total_records > 0)
{
    $int_total_pages = ceil($int_total_records / $int_records_per_page); 
    //print $int_total_pages;exit;
    if ($int_page > $int_total_pages)
    {
        //$int_page = $int_total_pages;
        $_SESSION["catid"] = "";
        $_SESSION["key"] = "";
        $_SESSION["PagePosition"] = 1;
    }
    else
    {
        $_SESSION["catid"] = $int_cat_pkid;
        $_SESSION["key"] = $str_name_key;
        $_SESSION["PagePosition"] = $int_page;

    }
    
}

$str_member_flag = false;
if(isset($_SESSION['vipmemberid']) && $_SESSION['vipmemberid'] != "")
{
    $str_member_flag = true;
}
//print_r($_SESSION);
//print $int_total_records; exit;
#open cms xml file
$str_desc_cms = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms);
$str_desc_cms = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_visible_cms = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);
#----------------------------------------------------------------------
#get metatag page title from table

$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_mt->fields("titletag");
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($rs_list_mt->fields("titletag")) ;?></title>
    <?php print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
            </div>
        </div>
        <?php if($str_visible_cms == "YES") { ?>
            <?php if($str_desc_cms != "" && $str_desc_cms != "<br>") { ?>
            <div class="row padding-10">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div  class="breadcrumb"><p align="justify"><?php print($str_desc_cms);?></p></div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
        <?php 
        $arr_test = array_keys($str_xml_list);
        $int_cnt=0;
        if(trim($arr_test[0])!="ROOT_ITEM")
        { 
            if($int_page != 1)
            {
                for($int_cnt;$int_cnt<($int_page-1)*($int_records_per_page);$int_cnt++)
		{
                    array_shift($str_xml_list);
		}	
            } ?>
        <div class="row padding-10">
            <?php
            $int_arr_lim=0;
            //print_r($str_xml_list);
            foreach($str_xml_list as $key => $val)         
            {
                    if($int_arr_lim < $int_records_per_page)
                    {
                        $int_arr_lim++;
			if(is_array($val))
			{
                            $str_video_access_flag=false;
                            $str_preview_flag=false;
                            if($str_member_flag)
                            {
                                $str_video_access_flag=true;
				$str_preview_flag=true;
                            }
                            else
                            {
                                //print $str_xml_list[$key]["ACCESSTOALL"] ;exit;
                                if($str_xml_list[$key]["ACCESSTOALL"] == "YES")
				{
                                    if($str_xml_list[$key]["PREVIEWTOALL"] == "YES")
                                    {
                                        $str_video_access_flag = true;
                                        $str_preview_flag = true;
                                    }										
				}
				else
				{
                                    if($str_xml_list[$key]["PREVIEWTOALL"] == "YES")
                                    {
                                        $str_preview_flag = true;
                                    }
				}									
                            } ?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="well">
                                    <div class="row padding-10">
                                        <?php if($str_xml_list[$key]["THUMBIMAGEFILENAME"] != "") { ?>
                                        <div class="col-md-6">
                                            <?php if($str_preview_flag == true || $str_member_flag) { ?>
                                                <image src="<?php print $str_img_path.$str_xml_list[$key]["PKID"]."/".$str_xml_list[$key]["THUMBIMAGEFILENAME"]; ?>" alt="<?php print $str_xml_list[$key]["TITLE"]; ?>" title="<?php print $str_xml_list[$key]["TITLE"]; ?>" class="img-responsive" />
                                            <?php } else { print("".MakeBlankTab(0,0,$STR_PREVIEW_NOT_AVAILABLE,"preview-danger")); }?>
                                            <div class="text-top-right">
                                                <?php if($str_xml_list[$key]["DISPLAYASNEW"] == "YES") { print $STR_ICON_PATH_NEW; } ?>
                                                <?php if($str_xml_list[$key]["PREVIEWTOALL"] == "YES" && $str_xml_list[$key]["ACCESSTOALL"] == "YES") { print $STR_ICON_PATH_FREE; } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <?php if($str_xml_list[$key]["TITLE"] != "") { ?>
                                                <h3 class="nopadding"><?php print $str_xml_list[$key]["TITLE"]; ?></a></h3>
                                            <?php } ?>
                                                
                                            <?php if($str_xml_list[$key]["DESCRIPTION"] != "") { ?>     
                                                <p><?php 
                                                    $str_desc_string = "";
                                                    $str_desc_string = $str_xml_list[$key]["DESCRIPTION"]; 
                    //                                $str_desc_string = "";
                                                    $str_desc_string = strip_tags($str_desc_string);
                                                    if (strlen($str_desc_string) > 64) {

                                                    $str_desc_stringCut = substr($str_desc_string, 0, 64);
                                                    $endPoint = strrpos($str_desc_stringCut, ' ');

                                                    //if the string doesn't contain any space then it will cut without word basis.
                                                    $str_desc_string = $endPoint? substr($str_desc_stringCut, 0, $endPoint) : substr($str_desc_stringCut, 0);
                                                    $str_desc_string .= '...';
                                                    }?>

                                                    <?php print $str_desc_string; ?></p>
                                            <?php } ?>
                                            <?php
                                            //print $str_member_flag;exit;
                                            if($str_member_flag == true) { ?>    
                                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/diary/<?php print(GetEncryptId($str_xml_list[$key]["PKID"]));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $str_xml_list[$key]["TITLE"])))); ?>" title="Click to view" class="btn btn-primary">
                                                    <b><?php print $str_btn_title_view_more; ?></b>
                                                </a>
                                            <?php } else if($str_video_access_flag == true && $str_preview_flag == true) { ?>    
                                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/diary/<?php print(GetEncryptId($str_xml_list[$key]["PKID"]));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $str_xml_list[$key]["TITLE"])))); ?>" title="Click to view" class="btn btn-primary">
                                                    <b><?php print $str_btn_title_view_more;?></b>
                                                    
                                                </a>
                                            <?php } else { ?>
                                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view" class="btn btn-primary"><b><b><?php print $str_btn_title_view_more;?></b></a>
                                            <?php } ?>      
                                        </div>
                                        <?php } else { ?>
                                        <div class="col-md-12">
                                            <?php if($str_xml_list[$key]["TITLE"] != "") { ?>
                                                <h3 class=""><?php print $str_xml_list[$key]["TITLE"]; ?></a></h3>
                                            <?php } ?>
                                            
                                            <?php if($str_xml_list[$key]["DESCRIPTION"] != "") { ?>    
                                                <p><?php 
                                                    $str_desc_string = "";
                                                    $str_desc_string = $str_xml_list[$key]["DESCRIPTION"]; 
                    //                                $str_desc_string = "";
                                                    $str_desc_string = strip_tags($str_desc_string);
                                                    if (strlen($str_desc_string) > 64) {

                                                    $str_desc_stringCut = substr($str_desc_string, 0, 64);
                                                    $endPoint = strrpos($str_desc_stringCut, ' ');

                                                    //if the string doesn't contain any space then it will cut without word basis.
                                                    $str_desc_string = $endPoint? substr($str_desc_stringCut, 0, $endPoint) : substr($str_desc_stringCut, 0);
                                                    $str_desc_string .= '...';
                                                    }?>

                                                    <?php print $str_desc_string; ?></p>
                                            <?php } ?>
                                            
                                            <?php
                                            //print $str_member_flag;exit;
                                            if($str_member_flag == true) { ?>    
                                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/diary/<?php  print(GetEncryptId($str_xml_list[$key]["PKID"]));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $str_xml_list[$key]["TITLE"])))); ?>" title="Click to view" class="btn btn-primary">
                                                    <b><?php print $str_btn_title_view_more; ?></b>
                                                </a>
                                            <?php } else if($str_video_access_flag == true && $str_preview_flag == true) { ?>    
                                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/diary/<?php  print(GetEncryptId($str_xml_list[$key]["PKID"]));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $str_xml_list[$key]["TITLE"])))); ?>" title="Click to view" class="btn btn-primary">
                                                    <b><?php print $str_btn_title_view_more;?></b>
                                                    
                                                </a>
                                            <?php } else { ?>
                                                <a href="<?php print($STR_SITENAME_WITH_PROTOCOL);?>/vipmember-joinnow#ptop" title="Click to view" class="btn btn-primary"><b><b><?php print $str_btn_title_view_more;?></b></a>
                                            <?php } ?>    
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } if($int_arr_lim % 2 == 0) { print "</div><div class='row padding-10'>"; }
                    }
                
            }?>
        </div>
        <div class="row padding-10">
            <div class="col-md-12 col-xs-12 col-sm-12" align="center">
                <nav aria-label="Page navigation">
                    <ul class="pagination nopadding">
                    <?php 
                    $int_margine = 2;
                    print(PagingWithMargineWithPost($int_total_records,$int_margine,$int_page,"diary",$int_records_per_page,"","key=".$str_name_key."&catid=".$int_cat_pkid."&#ptop"));?>
                    </ul>
                </nav>
            </div>
        </div><br/>
        <?php
        }?>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
</body>
</html>
