<?php
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
include "../user/product_validatesession.php";
include "./../includes/configuration.php";
include "./../user/product_config.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";

//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_STORE";
$str_img_path = $UPLOAD_IMG_PATH;
$str_img_path_measurement_chart = $UPLOAD_IMG_PATH_MEASUREMENT_CHART;
//print $str_img_path; 
$str_db_table_name_metatag = "t_page_metatag";
$str_xml_file_name = "";
$str_xml_file_name_cat = "";
$str_xml_file_name_cms = "";
$str_xml_file_name_sizechart = "sizechart.xml";
$str_xml_file_name_cms_sizechart = "sizechart_cms.xml";

$str_xml_file_name_measurechart = "measurechart.xml";
$str_xml_file_name_cms_measurechart = "measurechart_cms.xml";

$int_records_per_page = 10;

$str_button_view_details = "View This Product";
$str_hover_view_details = "Click To View This Product";
//print_r($_GET);exit;
#----------------------------------------------------------------------
##read main xml file for size chart
$str_list_xml_sizechart = "";
$str_list_xml_sizechart = readXML($STR_XML_FILE_PATH_MODULE . $str_xml_file_name_sizechart, "ROOT_ITEM");

$str_list_xml_measurechart = "";
$str_list_xml_measurechart = readXML($STR_XML_FILE_PATH_MODULE . $str_xml_file_name_measurechart, "ROOT_ITEM");

#Get values of all passed GET / POST variables
/*$int_cat_pkid = 0;
if(isset($_GET["cid"]) && trim($_GET["cid"]) != "" )
{   
    $int_cat_pkid = trim(GetDecryptId($_GET["cid"]));    
}*/

$int_pkid = 0;
if (isset($_GET["pid"]) && trim($_GET["pid"]) != "") {
    $int_pkid = trim($_GET["pid"]);
    //$int_pkid = trim(GetDecryptId($_GET["pid"]));
}

# Select Query
$str_query_select = "";
$str_query_select = "SELECT a.*, b.subcattitle, c.thumbphotofilename, c.largephotofilename, d.title as cattitle, d.description2 AS tailoringservicedesc, g.masterpkid AS itemfabricpkid FROM " . $STR_DB_TABLE_NAME . " a ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_CAT . " d ON a.catpkid=d.catpkid AND d.visible='YES' ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_SUBCAT . " b ON a.subcatpkid=b.subcatpkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TR_TABLE_NAME_FABRIC . " g ON a.pkid=g.itempkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_PHOTO . " c ON a.pkid=c.masterpkid WHERE a.pkid=" . $int_pkid . " AND b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' ";
//print $str_query_select;
$rs_list = GetRecordSet($str_query_select);
//print "FABRIC PKID : ".$rs_list->Fields("itemfabricpkid");
if ($rs_list->EOF()) {
    CloseConnection();
    Redirect($STR_SITENAME_WITH_PROTOCOL);
    exit();
}

#update noofview in master table
$str_query_update = "UPDATE " . $STR_DB_TABLE_NAME . " SET noofview=noofview+1 WHERE pkid=" . $int_pkid;
ExecuteQuery($str_query_update);

$str_title = "";
$str_title = $rs_list->Fields("title");

$int_sub_cat_pkid = 0;
$int_sub_cat_pkid = $rs_list->Fields("subcatpkid");


# Select Query For Similar Products
$str_query_select = "";

/*$str_query_select = "SELECT a.*, d.title as cattitle, b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl FROM " . $STR_DB_TABLE_NAME . " a ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_CAT . " d ON a.catpkid=d.catpkid AND d.visible='YES' ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_SUBCAT . " b ON a.subcatpkid=b.subcatpkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_PHOTO . " c ON a.pkid=c.masterpkid ";
$str_query_select .= "WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.pkid != " . $int_pkid . " AND a.subcatpkid=" . $int_sub_cat_pkid . " ";
$str_query_select .= "ORDER BY a.displayorder DESC, a.createdatetime DESC LIMIT 0,12";
*/

$str_query_select = "SELECT temp.pkid, temp.catpkid, temp.subcatpkid, temp.title, temp.itemcode, temp.listprice, temp.ourprice, temp.memberprice, temp.weight, ";
$str_query_select .= "temp.description, temp.createdatetime, temp.displayasnew, temp.displayashot, temp.displayasfeatured, temp.displayorder, ";
$str_query_select .= "temp.subcattitle, temp.thumbphotofilename, temp.largephotofilename, temp.imageurl, temp.cattitle, ";
$str_query_select .= "temp.colortitle, temp.sizetitle ";
$str_query_select .= "FROM ( ";
$str_query_select .= "SELECT a.pkid, a.catpkid, a.subcatpkid, a.title, a.itemcode, a.listprice, a.ourprice, a.memberprice, a.weight, ";
$str_query_select .= "a.description, a.createdatetime, a.displayasnew, a.displayashot, a.displayasfeatured, a.displayorder, ";
$str_query_select .= "b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title AS cattitle, ";
$str_query_select .= "f.title AS colortitle, ";
//$str_query_select .= "group_concat(n.title ) AS sizetitle "; // Use this line if want all sizes in same record
$str_query_select .= "min(n.title) AS sizetitle "; // Use this line if want first size only
//$str_query_select .= "max(n.title) AS sizetitle "; // Use this line if want last size only
$str_query_select .= " FROM " . $STR_DB_TABLE_NAME . " a ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_CAT . " d ON a.catpkid=d.catpkid AND d.visible='YES' ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_SUBCAT . " b ON a.subcatpkid=b.subcatpkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_PHOTO . " c ON a.pkid=c.masterpkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TR_TABLE_NAME_COLOR . " e ON a.pkid=e.itempkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_COLOR . " f ON e.masterpkid=f.pkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TR_TABLE_NAME_FABRIC . " g ON a.pkid=g.itempkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_FABRIC . " h ON g.masterpkid=h.pkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TR_TABLE_NAME_TYPE . " i ON a.pkid=i.itempkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_TYPE . " j ON i.masterpkid=j.pkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TR_TABLE_NAME_WORK . " k ON a.pkid=k.itempkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_WORK . " l ON k.masterpkid=l.pkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TR_TABLE_NAME_SIZE . " m ON a.pkid=m.itempkid ";
$str_query_select .= "LEFT JOIN " . $STR_DB_TABLE_NAME_SIZE . " n ON m.masterpkid=n.pkid ";
$str_query_select .= " WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.visible='YES' AND a.approved='YES' ";

//$str_query_select .= " AND a.pkid != " . $int_pkid . " AND a.subcatpkid=" . $int_sub_cat_pkid ." ";
$str_query_select .= " AND a.pkid != " . $int_pkid . " AND a.subcatpkid=" . $int_sub_cat_pkid . " AND g.masterpkid=" . $rs_list->Fields("itemfabricpkid");

$str_query_select .= " GROUP BY a.pkid, a.catpkid, a.subcatpkid, a.title, a.itemcode, a.listprice, a.ourprice, a.memberprice, a.weight, a.description, a.createdatetime, a.displayasnew, a.displayashot, a.displayasfeatured, b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl, d.title, f.title, a.displayorder DESC, a.createdatetime DESC) AS temp ";
$str_query_select .= " GROUP BY temp.pkid, temp.catpkid, temp.subcatpkid, temp.title, temp.itemcode, temp.listprice, temp.ourprice, temp.memberprice, temp.weight, temp.description, temp.createdatetime, temp.displayasnew, temp.displayashot, temp.displayasfeatured, temp.subcattitle, temp.thumbphotofilename, temp.largephotofilename, temp.imageurl, temp.cattitle, temp.title ";
$str_query_select .= " HAVING MAX(temp.createdatetime) ";
$str_query_select .= " ORDER BY temp.displayorder, temp.createdatetime DESC ";
$str_query_select .= " LIMIT 0,12";

//print $str_query_select; //exit;
$rs_list_similar = GetRecordSet($str_query_select);
//print $rs_list_similar->Count();exit;

#----------------------------------------------------------------------
$str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME_TAILORING_SERVICE . " WHERE visible='YES' AND price=0 LIMIT 0,1";
$rs_list_tailoring = GetRecordSet($str_query_select);
$int_tailoring_service_pkid = 0;
$int_tailoring_service_title = "Material Only";
if ($rs_list_tailoring->Count() > 0) {
    $int_tailoring_service_pkid = $rs_list_tailoring->Fields("pkid");
    $int_tailoring_service_title = $rs_list_tailoring->Fields("price");
}

#----------------------------------------------------------------------
/*$str_name_key = "";
if(isset($_POST["key"]) && trim($_POST["key"]) != "" )
{ $str_name_key = trim($_POST["key"]); }

# get data for paging
$int_page = "";
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"]) != "" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"]) > 0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page = 1; }
//print_r($_POST);
$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";
*/



/*$int_page = 1;
if($int_total_records>0)
{
    $int_total_pages = ceil($int_total_records / $int_records_per_page); 
    //print $int_total_pages;
    if ($int_page > $int_total_pages)
    {
        $int_page = $int_total_pages;
    }
    if($int_page < 1)
    {
        $int_page=1;
    }
}*/

/*$str_member_flag = false;
$int_memberpkid = 0;
$str_memberid = "";
if((isset($_SESSION['vipmemberid']) && $_SESSION['vipmemberid'] != ""))
{
    $str_member_flag = true;
    $int_memberpkid = $_SESSION['vipmemberpkid'];
    $str_memberid = $_SESSION['vipmemberid'];
}*/
/*else
{
    CloseConnection();
    Redirect("./../vipmember-joinnow#ptop"); 
    exit();
}*/
#open cms xml file
$str_desc_cms_sizechart = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS . $str_xml_file_name_cms_sizechart);
$str_desc_cms_sizechart = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
$str_visible_cms_sizechart = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);

$str_desc_cms_measurechart = "";
$fp = OpenXMLFile($STR_XML_FILE_PATH_CMS . $str_xml_file_name_cms_measurechart);
$str_desc_cms_measurechart = getTagValue("ITEMKEYVALUE_DESCRIPTION", $fp);
//print $str_desc_cms_measurechart; exit;
$str_visible_cms_measurechart = getTagValue("ITEMKEYVALUE_VISIBLE", $fp);
CloseXmlFile($fp);

#----------------------------------------------------------------------
#get metatag page title from table

$str_select_query = "";
$str_select_query = "SELECT titletag FROM " . $str_db_table_name_metatag . " WHERE visible='YES' AND pagekey='" . $str_title_page_metatag . "' ";
//print $str_select_query; exit;
$rs_list_similar_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_similar_mt->fields("titletag");
#----------------------------------------------------------------------
$str_member_flag = false;
$int_userpkid = 0;
$str_username = "";
$str_usertype = "";
if (isset($_SESSION['userpkid']) && $_SESSION['userpkid'] != "") {
    $str_member_flag = true;

    $str_query_select = "";
    $str_query_select = "SELECT isusertype FROM " . $STR_DB_TABLE_NAME_USER . " WHERE pkid=" . $_SESSION["userpkid"];
    $rs_list_user = GetRecordSet($str_query_select);


    $int_userpkid = $_SESSION['userpkid'];
    $str_username = $_SESSION['username'];
    $str_usertype = $rs_list_user->Fields("isusertype");
}
// print $str_usertype; exit;

$int_referredbypkid = 0;
$int_commission = 0.00;
$int_wholesaler_discount = 0;
$str_user_type = "";
if ($int_userpkid > 0) {
    $str_query_select = "";
    $str_query_select = "SELECT referredbypkid, discount, isusertype FROM " . $STR_DB_TABLE_NAME_USER . " WHERE pkid=" . $int_userpkid;
    $rs_list_user = GetRecordSet($str_query_select);
    if (!$rs_list_user->EOF()) {
        if (strtoupper($rs_list_user->Fields("isusertype")) == "WHOLESALER") {
            $str_user_type = "WHOLESALER";
            $int_referredbypkid = $rs_list_user->Fields("referredbypkid");
            $int_wholesaler_discount = $rs_list_user->Fields("discount");
            if ($int_referredbypkid > 0) {
                $str_query_select = "";
                $str_query_select = "SELECT discount, commission FROM " . $STR_DB_TABLE_NAME_USER . " WHERE pkid=" . $int_referredbypkid;
                $rs_list_commmision = GetRecordSet($str_query_select);

                $int_commission = $rs_list_commmision->Fields("commission");
            }
        }
    }
}
// print $int_wholesaler_discount;
// print_r($_SESSION);

$str_whatsapp_number = "8154044411";
#---------------------------------------------------------------------- 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=yes" />
    <meta name="author" content="">
    <title><?php //print($STR_SITE_TITLE);
            ?><?php print($rs_list->fields("seotitle")); ?></title>
    <meta name="description" content="<?php print($rs_list->fields("seodescription")); ?>">
    <meta name="keyword" content="<?php print($rs_list->fields("seokeyword")); ?>">
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/social-share-kit.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/glass.css" rel="stylesheet" type="text/css" />

    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <!--zoom assets-->
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/zoom/xzoom.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/zoom/setup.js"></script>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/zoom/xzoom.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/zoom/pinch-zoom.js"></script>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/zoom/xzoom-pinch.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .xzoom-preview {
            z-index: 999;
        }
    </style>
</head>

<body>
    <?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg"><br />
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <ol class="breadcrumb thumbnail-margin">
                    <?php //print_r($_GET); 
                    ?>
                    <?php if (isset($_GET["cname"]) && $_GET["cname"] != "" && $rs_list->Fields("cattitle") != "") { ?>
                        <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list->Fields("pkid"); 
                                                                                    ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle"))))); ?>/" title="<?php print $rs_list->Fields("cattitle"); ?>"><?php print $rs_list->Fields("cattitle"); ?></a></li>
                    <?php } ?>
                    <?php if (isset($_GET["sname"]) && $_GET["sname"] != "" && $rs_list->Fields("subcattitle") != "") { ?>
                        <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list->Fields("pkid"); 
                                                                                    ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle"))))); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle"))))); ?>"><?php print $rs_list->Fields("subcattitle"); ?></a></li>

                    <?php } ?>
                    <?php if (isset($_GET["pname"]) && $_GET["pname"] != "" && $rs_list->Fields("title") != "") { ?>
                        <li class=""><?php print $rs_list->Fields("title"); ?></li>
                    <?php } ?>
                </ol>
            </div>
        </div><br />
        <div class="row padding-10">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <?php
                $str_select_images = "";
                $str_select_images = "SELECT * FROM " . $STR_DB_TABLE_NAME_PHOTO . " WHERE visible='YES' AND masterpkid= " . $rs_list->Fields("pkid") . " ORDER BY setasfront DESC";
                //print $str_select_images; exit;
                $rs_list_image = GetRecordSet($str_select_images);
                if ($rs_list_image->Count() > 0) {
                    while ($rs_list_image->eof() != true) {
                        if ($rs_list_image->Fields("setasfront") == "YES") {
                ?>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php if ($rs_list_image->Fields("largephotofilename") != "") { ?>
                                    <div class="page">
                                        <div class="xzoom-container zoomHolder">
                                            <a href="#" data-toggle="modal" data-target=".product-<?php print($rs_list_image->fields("pkid")); ?>" rel="thumbnail">
                                                <img class="xzoom img-responsive pinch" data-elem="pinchzoomer" id="xzoom-default" style="width:100%" src="<?php print $str_img_path . $rs_list->fields("pkid") . "/" . $rs_list_image->Fields("largephotofilename"); ?>" alt="<?php print($rs_list->fields("title")); ?>" xoriginal="<?php print $str_img_path . $rs_list->fields("pkid") . "/" . $rs_list_image->Fields("largephotofilename"); ?>" />
                                            </a><br>
                                        </div>
                                    </div>
                                    <div class="modal fade product-<?php print($rs_list_image->fields("pkid")); ?>" align="center" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                    <img src="<?php print($str_img_path . $rs_list->fields("pkid") . "/" . $rs_list_image->fields("largephotofilename")); ?>" class="img-responsive img-rounded modal-img" id="modal-img" alt="<?php print($rs_list->fields("title")); ?>" title="Product Image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <?php } else {
                                ?>
                                    <div class="page pinch-zoom-parent">
                                        <div class="xzoom-container pinch-zoom">
                                            <a href="#" data-toggle="modal" data-target=".product-<?php print($rs_list_image->fields("pkid")); ?>" rel="thumbnail">
                                                <img id="expandedImg" class="xzoom img-responsive pinch" id="xzoom-default" xoriginal="<?php print $rs_list_image->Fields("imageurl"); ?>" style="width:100%" src="<?php print $rs_list_image->Fields("imageurl"); ?>">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="modal fade product-<?php print($rs_list_image->fields("pkid")); ?>" align="center" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                    <img src="<?php print $rs_list_image->Fields("imageurl"); ?>" id="modal-img-2" class="img-responsive img-rounded modal-img" alt="<?php print($rs_list->fields("title")); ?>" title="Product Image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>

                            <script type="text/javascript">
                                var el = document.querySelector('.pinch-zoom');
                                new PinchZoom.default(el, {});
                            </script>
                        <?php
                        } //else if ($rs_list_image->Fields("setasfront") == "NO") { 
                        ?>
                        <div class="xzoom-thumbs">
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <?php if ($rs_list_image->Fields("largephotofilename") != "") { ?>

                                    <a href="<?php print $str_img_path . $rs_list->fields("pkid") . "/" . $rs_list_image->Fields("largephotofilename"); ?>">
                                        <img class="xzoom-gallery" width="80" src="<?php print $str_img_path . $rs_list->fields("pkid") . "/" . $rs_list_image->Fields("largephotofilename"); ?>" xpreview="<?php print $str_img_path . $rs_list->fields("pkid") . "/" . $rs_list_image->Fields("largephotofilename"); ?>">
                                    </a>

                                <?php } else {
                                ?>
                                    <a href="<?php print $rs_list_image->Fields("imageurl"); ?>">
                                        <img class="xzoom-gallery" width="80" src="<?php print $rs_list_image->Fields("imageurl"); ?>" xpreview="<?php print $rs_list_image->Fields("imageurl"); ?>">
                                    </a>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    <?php

                        //}
                        $rs_list_image->MoveNext();
                    }
                    ?>

                    <script>
                        $(document).ready(function() {
                            var expandedImg = $('#xzoom-default').attr('src');
                            var modalImg = $('#modal-img').attr('src');
                            $('.xzoom-gallery').click(function() {
                                $('#modal-img').attr('src', this.src);
                                $('#modal-img-2').attr('src', this.src);
                            });
                        });
                    </script>
                    <!-- <?php if ($rs_list_image->Fields("largephotofilename") != "") { ?>

                                    <div class="xzoom-container">
                                        <a href="#" data-toggle="modal" data-target=".product-<?php print($rs_list_image->fields("pkid")); ?>" rel="thumbnail">
                                            <img class="xzoom img-responsive" id="xzoom-default" src="<?php print $str_img_path . $rs_list->fields("pkid") . "/" . $rs_list_image->Fields("largephotofilename"); ?>" alt="<?php print($rs_list->fields("title")); ?>" xoriginal="<?php print $str_img_path . $rs_list->fields("pkid") . "/" . $rs_list_image->Fields("largephotofilename"); ?>" width="100%" title="Click to view actual size image" border="0" align="top" />
                                        </a><br />
                                    </div>

                                    <?php //if($rs_list_image->Fields("setasfront") == "YES") { print "<br/>"; } 
                                    ?>

                                    <div class="modal fade product-<?php print($rs_list_image->fields("pkid")); ?>" align="center" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                    <img src="<?php print($str_img_path . $rs_list->fields("pkid") . "/" . $rs_list_image->fields("largephotofilename")); ?>" class="img-responsive img-rounded" alt="<?php print($rs_list->fields("title")); ?>" title="Product Image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <?php } else if ($rs_list_image->Fields("imageurl") != "") { ?>
                                    <a href="#" data-toggle="modal" data-target=".product-<?php print($rs_list_image->fields("pkid")); ?>" rel="thumbnail">
                                        <img src="<?php print $rs_list_image->Fields("imageurl"); ?>" alt="<?php print($rs_list->fields("title")); ?>" width="100%" class="img-responsive" title="Click to view actual size image" border="0" align="top" />
                                    </a>
                                    <br />
                                    <?php //if($rs_list_image->Fields("setasfront") == "YES") { print "<br/>"; } 
                                    ?>

                                    <div class="modal fade product-<?php print($rs_list_image->fields("pkid")); ?>" align="center" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                    <img src="<?php print $rs_list_image->Fields("imageurl"); ?>" class="img-responsive img-rounded" alt="<?php print($rs_list->fields("title")); ?>" title="Product Image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } // End of IF for image validation 
                                ?> 

                                </div>
                             <?php
                                $rs_list_image->MoveNext();
                            } ?> -->

                    <?php //} // End of IF count condition 
                    ?>
                    <br />
            </div>
            <div class="col-md-5 col-sm-12 col-xs-12">
                <div class="row padding-10">
                    <div class="col-md-12">
                        <h3 class="nopadding"><?php print($rs_list->fields("title")); ?>
                            <?php if ($rs_list->Fields("displayasnew") == 'YES') {
                                print "&nbsp;" . $STR_ICON_PATH_NEW;
                            } ?><?php if ($rs_list->Fields("displayasfeatured") == 'YES') {
                                    print "&nbsp;" . $STR_ICON_PATH_FEATURED;
                                } ?><?php if ($rs_list->Fields("displayashot") == 'YES') {
                                        print "&nbsp;" . $STR_ICON_PATH_HOT;
                                    } ?>
                        </h3>
                        <hr />
                    </div>
                </div>
                <div class="row padding-10">
                    <div class="col-md-12">
                        <p><?php if ($rs_list->fields("itemcode") != "") { ?><b>Code:</b> <?php print($rs_list->fields("itemcode")); ?><?php } ?>&nbsp;&nbsp;&nbsp;&nbsp;<?php if ($rs_list->fields("availability") == "YES") { ?><span class="btn btn-success btn-xs disabled pull-right"><i class="fa fa-check"></i>&nbsp;In Stock</span><?php } else { ?><span class="btn btn-danger btn-xs disabled pull-right pull-right"><i class="fa fa-close"></i>&nbsp;Out of Stock</span><?php } ?>
                        </p>
                    </div>
                </div>
                <hr />
                <div class="row padding-10">
                    <div class="col-md-12">
                        <label>
                            <h4 class="nopadding">
                                <?php
                                $int_per_discount = 0;
                                if ($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) {
                                    $int_member_price = "";
                                    $int_member_price = $rs_list->Fields("memberprice") / $int_conversionrate;
                                ?>
                                    <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_member_price), 0) . ""; ?></b>
                                <?php } else { ?>

                                    <?php
                                    if ($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                        $int_list_price = "";
                                        $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;

                                        $int_our_price = "";
                                        $int_our_price = $rs_list->Fields("ourprice") / $int_conversionrate; ?>
                                        <span class="text-muted"><strike><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 0) . "&nbsp;" . $str_currency_shortform; ?></strike></span>&nbsp;
                                        <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_our_price), 0) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;&nbsp;
                                        <?php

                                        $int_per_discount = (100 - ($rs_list->Fields("ourprice") / $rs_list->Fields("listprice")) * 100);  ?>
                                    <?php } else {
                                        $int_list_price = "";
                                        $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;
                                    ?>
                                        <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 0) . ""; ?></b>
                                    <?php } ?>
                                <?php } ?>
                            </h4>
                        </label>
                        <?php if ($int_per_discount > 0) { ?>
                            <span class="btn btn-xs btn-primary disabled pull-right"><?php print number_format($int_per_discount, 2); ?>% Off</span>
                        <?php } ?>
                    </div>
                </div>
                <hr />
                <form id="frm_add" name="frm_add" novalidate>

                    <?php
                    $str_query_select = "";
                    $str_query_select = "SELECT b.*, a.cmyk_code, a.title FROM " . $STR_DB_TABLE_NAME_COLOR . " a LEFT JOIN " . $STR_DB_TR_TABLE_NAME_COLOR . " b ON b.masterpkid=a.pkid AND a.visible='YES' WHERE b.itempkid=" . $rs_list->Fields("pkid");
                    $rs_list_color = GetRecordSet($str_query_select);
                    if ($rs_list_color->Count() > 0) { ?>
                        <div class="row padding-10">
                            <div class="col-md-12">
                                <div class="form-group control-group">
                                    <div class="controls">
                                        <label>Color <span class="text-help-form">*</span></label><br />
                                        <div class="product-configuration">
                                            <div class="product-color">
                                                <div class="color-choose">
                                                    <?php
                                                    $int_cnt = 1;
                                                    while (!$rs_list_color->EOF()) { ?>
                                                        <style>
                                                            /* Product Color */
                                                            .product-color {
                                                                margin-bottom: 0px;
                                                            }

                                                            .color-choose div {
                                                                display: inline-block;
                                                            }

                                                            .color-choose input[name="rdo_color"] {
                                                                display: none;
                                                            }

                                                            .color-choose input[name="rdo_color"]+label span {
                                                                display: inline-block;
                                                                width: 35px;
                                                                height: 35px;
                                                                margin: -1px 4px 0 0;
                                                                vertical-align: middle;
                                                                cursor: pointer;
                                                                border-radius: 0px;
                                                            }

                                                            .color-choose input[name="rdo_color"]+label span {
                                                                border: 2px solid #F5F5F5;
                                                            }

                                                            .color-choose input[name="rdo_color"]#rdo_color<?php print $int_cnt; ?>+label span {
                                                                background-color: <?php print $rs_list_color->Fields("cmyk_code"); ?>;
                                                            }

                                                            .color-choose input[name="rdo_color"]:checked+label span {
                                                                background-image: url(<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/images/check-icn.png);
                                                                background-repeat: no-repeat;
                                                                background-position: center;
                                                            }
                                                        </style>
                                                        <div>
                                                            <input data-image="rdo_color<?php print $int_cnt; ?>" type="radio" class="color" name="rdo_color" id="rdo_color<?php print $int_cnt; ?>" value="<?php print $rs_list_color->Fields("masterpkid"); ?>" required data-validation-required-message="Please select color." <?php if ($int_cnt == 1) {
                                                                                                                                                                                                                                                                                                                                        print "checked";
                                                                                                                                                                                                                                                                                                                                    } ?>>
                                                            <label for="rdo_color<?php print $int_cnt; ?>"><span></span></label>
                                                        </div>
                                                    <?php $int_cnt++;
                                                        $rs_list_color->MoveNext();
                                                    } ?>
                                                </div>

                                            </div>
                                        </div>
                                        <?php /*while(!$rs_list_color->EOF()) { ?>
                                    <style>
                                        .bg-check {position:absolute;display:none;margin:9px -25px 0px;}
                                        .circle {background-color:#f1f1f1;color:#FFFFFF;display: inline-block;width: 30px;height: 30px;margin: 0 2px; text-align: center;font-size: 18px;font-family: 'Kavoon', cursive;line-height: 28px;-webkit-transition: all 0.2s ease-in-out; -moz-transition: all 0.2s ease-in-out; -ms-transition: all 0.2s ease-in-out;
                                         -o-transition: all 0.2s ease-in-out; transition: all 0.2s ease-in-out; cursor:pointer;-webkit-border-radius: 50%;-moz-border-radius: 50%;border-radius: 50%;}
                                        input[id="rdo_color"] {visibility: hidden;position: absolute;}
                                        input[id="rdo_color"] + img { opacity: 1;filter: alpha(opacity=1); }
                                        input[id="rdo_color"]:checked + img { opacity: 0.4; }
                                        input[id="rdo_color"]:checked ~ span { display: inline-block; }
                                    </style>
                                    <label class="radio-inline" style="padding-left: 0px; color: #ddd;">
                                        <input type="radio" name="rdo_color" id="rdo_color" value="<?php print $rs_list_color->Fields("masterpkid"); ?>" required data-validation-required-message="Please select color." >
                                        <span class="circle img-responsive" id="bgcolor" style="background-color: <?php print $rs_list_color->Fields("cmyk_code"); ?>;"></span>
                                        <span class="fa fa-check bg-check"></span>
                                    </label>
                                    <?php
                                    $rs_list_color->MoveNext();
                                    } */ ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                    <?php } ?>
                    <?php
                    $str_query_select = "";
                    $str_query_select = "SELECT *, b.title FROM " . $STR_DB_TR_TABLE_NAME_SIZE . " a LEFT JOIN " . $STR_DB_TABLE_NAME_SIZE . " b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid=" . $rs_list->Fields("pkid");
                    //print $str_query_select;
                    $rs_list_size = GetRecordSet($str_query_select);

                    if ($rs_list_size->Count() > 0) { ?>
                        <div class="row padding-10">
                            <div class="col-md-12">
                                <div class="form-group control-group">
                                    <div class="controls">
                                        <label>Size <span class="text-help-form">*</span></label><br />
                                        <ul class="chec-radio">
                                            <?php $int_cnt = 1;
                                            while (!$rs_list_size->EOF()) { ?><?php //print $rs_list_color->Fields("cmyk_code")."<br/>"; 
                                                                                ?>
                                            <?php /* ?><style>
                                        .bg-check2 {position:absolute; display:none;background-color: #ef5f96; color: #fff; border:1px solid#c5c5c5;width: 50px;height: 50px; margin-left: -52px; padding: 10px; text-align: center;font-size: 13px; line-height: 28px;border-radius: 50%;}
                                        .circle2 {background-color: #FFFFFF; padding: 10px; color: #ef5f96; border:1px solid#c5c5c5;display: inline-block;width: 50px;height: 50px;margin: 0 2px; text-align: center;font-size: 13px; line-height: 28px;-webkit-transition: all 0.2s ease-in-out; -moz-transition: all 0.2s ease-in-out; -ms-transition: all 0.2s ease-in-out;
                                         -o-transition: all 0.2s ease-in-out; transition: all 0.2s ease-in-out; cursor:pointer;-webkit-border-radius: 50%;-moz-border-radius: 50%;border-radius: 50%;}

                                        input[id="rdo_size"] {visibility: hidden;position: absolute;}
                                        input[id="rdo_size"] + img { opacity: 1;filter: alpha(opacity=1); }
                                        input[id="rdo_size"]:checked + img { opacity: 0.4; }
                                        input[id="rdo_size"]:checked ~ span { display: inline-block; }
                                    </style>
                                    <label class="radio-inline" style="padding-left: 0px;">
                                        <input type="radio" name="rdo_size" id="rdo_size" value="<?php print $rs_list_size->Fields("masterpkid"); ?>" required data-validation-required-message="Please select size.">
                                        <span class="circle2 img-responsive" id="bgcolor" style="" ><?php print $rs_list_size->Fields("title"); ?></span>
                                        <span class="bg-check2"><?php print $rs_list_size->Fields("title"); ?></span>
                                    </label><?php */ ?>

                                            <li class="pz">
                                                <label class="radio-inline">
                                                    <input type="radio" name="rdo_size" id="rdo_size" value="<?php print $rs_list_size->Fields("masterpkid"); ?>" required data-validation-required-message="Please select size." <?php if ($int_cnt == 1) {
                                                                                                                                                                                                                                        print "checked";
                                                                                                                                                                                                                                    } ?> />
                                                    <div class="clab"><?php print $rs_list_size->Fields("title"); ?></div>
                                                </label>
                                            </li>
                                        <?php $int_cnt++;
                                                $rs_list_size->MoveNext();
                                            } ?>

                                        </ul>

                                    </div>
                                </div>
                                <?php /* ?><ul class="chec-radio">
                                <li class="pz">
                                        <label class="radio-inline">
                                                <input type="radio" checked="" id="pro-chx-residential" name="property_type" class="pro-chx" value="XL">
                                                <div class="clab">XL</div>
                                        </label>
                                </li>
                                <li class="pz">
                                        <label class="radio-inline">
                                                <input type="radio" id="pro-chx-commercial" name="property_type" class="pro-chx" value="XXL" checked>
                                                <div class="clab">XXL</div>
                                        </label>
                                </li>
                                <li class="pz">
                                        <label class="radio-inline">
                                                <input type="radio" id="pro-chx-open" name="property_type" class="pro-chx" value="XXXL">
                                                <div class="clab">XXXL</div>
                                        </label>
                                </li>
                            </ul><?php */ ?>



                            </div>
                        </div>
                        <hr />

                    <?php } ?>
                    <div class="row padding-10">
                        <div class="col-md-12">
                            <label>Quantity <span class="text-help-form">*</span></label>
                            <div class="form-group input-group" style="width:150px;">
                                <span class="input-group-btn" style="width: 0px;">
                                    <button id="qty_dec" class="btn btn-default" type="button" onclick="return QtyDecrement();"><i class="fa fa-minus"></i></button>
                                </span>
                                <input type="text" id="txt_quantity" name="txt_quantity" class="form-control text-center" value="1" required data-validation-required-message="Please enter quantity." readonly>

                                <span class="input-group-btn">
                                    <button id="qty_inc" class="btn btn-default" type="button" onclick="return QtyIncrement();"><i class="fa fa-plus"></i></button>
                                </span>
                            </div>
                        </div>

                    </div>
                    <?php if ($rs_list->Fields("availability_info") != "" || $rs_list->Fields("dshippinginfo") != "" || $rs_list->Fields("ishippinginfo") != "") { ?>
                        <hr />
                        <div class="row padding-10">
                            <div class="col-md-12">
                                <?php if ($rs_list->Fields("availability_info") != "") { ?>
                                    <p class="text-muted">
                                        <?php print($rs_list->Fields("availability_info")); ?>
                                        <?php /* ?><a href="" title="">Read More&nbsp;<i class="fa fa-caret-down"></i></a><?php */ ?>
                                    </p>
                                <?php } ?>
                                <?php if ($rs_list->Fields("dshippinginfo") != "") { ?>
                                    <p class="text-muted">
                                        <?php
                                        print $rs_list->Fields("dshippinginfo");
                                        ?>
                                        <?php /* ?><a href="" title="">Read More&nbsp;<i class="fa fa-caret-down"></i></a><?php */ ?>
                                    </p>
                                <?php } ?>
                                <?php if ($rs_list->Fields("ishippinginfo") != "") { ?>
                                    <p class="text-muted">
                                        <?php print $rs_list->Fields("ishippinginfo"); ?>
                                        <?php /* ?><a href="" title="">Read More&nbsp;<i class="fa fa-caret-down"></i></a><?php */ ?>
                                    </p>
                                <?php } ?>
                                <?php /* ?><p class="text-muted">
                                <span><i class="fa fa-clock-o"></i>&nbsp;Dispatch in 24 Hrs</span>&nbsp;&nbsp;
                                <span><i class="fa fa-plane"></i>&nbsp; Free Shipping</span><br/>
                            </p>
                            <p class="text-muted">
                                <span class="">+3 Days, if you opt for any stitching services.</span>
                            </p><?php */ ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php
                    $str_query_select = "";

                    ## Below given query set tailoring option based on product...
                    //$str_query_select = "SELECT a.*, b.title FROM ".$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE." a LEFT JOIN ".$STR_DB_TABLE_NAME_TAILORING_SERVICE." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.visible='YES' AND a.itempkid=".$rs_list->Fields("pkid")." ORDER BY b.displayorder DESC, b.title ASC" ;

                    ## Below given query set tailoring option based on category...
                    $str_query_select = "SELECT a.*, b.title FROM " . $STR_DB_TR_TABLE_NAME_TAILORING_SERVICE . " a LEFT JOIN " . $STR_DB_TABLE_NAME_TAILORING_SERVICE . " b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.visible='YES' AND a.catpkid=" . $rs_list->Fields("catpkid") . " ORDER BY a.displayorder DESC, b.title ASC";
                    $rs_list_tailoring = GetRecordSet($str_query_select);
                    if ($rs_list_tailoring->Count() > 0) { ?>
                        <hr />
                        <div class="row padding-10">
                            <div class="col-md-12">
                                <p class="small nopadding text-right">
                                    <a href="" title="" data-toggle="modal" data-target="#myModal01"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Size Chart</a>&nbsp;&nbsp;&nbsp;
                                    <a href="" title="" data-toggle="modal" data-target="#myModal03"><i class="fa fa-scissors" aria-hidden="true"></i>&nbsp;&nbsp;View How To Measure</a>&nbsp;&nbsp;&nbsp;<br />
                                </p>
                            </div>
                        </div>
                        <div class="row padding-10">
                            <div class="col-md-12">
                                <p class="nopadding"><b>Tailoring Services</b>
                                    <?php if ($rs_list->Fields("tailoringservicedesc") != "" && $rs_list->Fields("tailoringservicedesc") != "<br>") { ?>
                                        &nbsp;<a href="" data-toggle="modal" data-target="#myModal"><i class="fa fa-info-circle"></i></a></p>

                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close mdl-close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Stitching Options </h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row padding-10">
                                                    <div class="col-md-12">
                                                        <p align="justify">
                                                            <?php print $rs_list->Fields("tailoringservicedesc"); ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group control-group">
                                <div class="controls">
                                    <div class="radio">
                                        <?php $int_cnt = 1;
                                        while (!$rs_list_tailoring->EOF()) { ?>
                                            <label class="control control--radio label-normal">
                                                <input type="radio" name="rdo_tailoringoption" id="rdo_tailoringoption[]" value="<?php print $rs_list_tailoring->Fields("masterpkid"); ?>" <?php if ($int_cnt == 1) {
                                                                                                                                                                                                print "checked";
                                                                                                                                                                                            } ?> /> <?php print $rs_list_tailoring->Fields("title"); ?>&nbsp;(+<?php print "&nbsp;" . $str_currency_symbol . "&nbsp;"; ?><span id="tailoring_opt<?php print $rs_list_tailoring->Fields("masterpkid"); ?>"><?php print ceil($rs_list_tailoring->Fields("price") / $int_conversionrate); ?></span>)
                                                <div class="control__indicator" style="top: 3px;"></div>
                                            </label>
                                        <?php $int_cnt++;
                                            $rs_list_tailoring->MoveNext();
                                        } ?>
                                    </div>
                                </div>
                            </div>
                            <div id="rd_option2" style="display:none">
                                <?php
                                $arr_semi_stitched_type = array();
                                $arr_semi_stitched_type = explode(",", $STR_TAILORING_OPTION_SEMI_STITCHING_SIZE);
                                //print Count($arr_semi_stitched_type); 
                                ?>
                                <div class="form-group control-group">
                                    <div class="controls">
                                        <label>Semi Stitching <span class="text-help-form"> *</span></label>
                                        <select class="form-control input-sm" id="cbo_semistitched_stitchingsize" name="cbo_semistitched_stitchingsize" required required data-validation-required-message="Please select stitching size.">
                                            <option value="">-- SELECT SIZE --</option>
                                            <?php
                                            for ($i = 0; $i < (Count($arr_semi_stitched_type)); $i++) { ?>
                                                <option value="<?php print $arr_semi_stitched_type[$i]; ?>"><?php print $arr_semi_stitched_type[$i]; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="rd_option3" style="display:none;">
                                <?php
                                $str_query_select = "";
                                $str_query_select = "SELECT * FROM " . $STR_DB_TR_TABLE_NAME_CATWISE_TAILORING_OPTION . " a LEFT JOIN " . $STR_DB_TABLE_NAME_TAILORING_OPTION . " b ON a.optionpkid=b.pkid AND b.visible='YES' WHERE a.catpkid=" . $rs_list->Fields("catpkid") . " ORDER BY b.displayorder ASC";

                                //print $str_query_select; 
                                $rs_list_tailoring_option = GetRecordSet($str_query_select);

                                $arr_selected_option = array();
                                $str_tailoring_opt = "";
                                while (!$rs_list_tailoring_option->EOF()) {
                                    array_push($arr_selected_option, trim($rs_list_tailoring_option->Fields("title")));
                                    $rs_list_tailoring_option->MoveNext();
                                }

                                //print_r($arr_selected_option);
                                if (in_array("Blouse", $arr_selected_option)) {
                                ?>
                                    <div class="row padding-10">
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>1). Blouse Length</b></span><span class="text-help-form">*</span>
                                                    <input type="input" id="txt_blouse_length" name="txt_blouse_length" class="form-control input-sm" placeholder="Blouse Length" required required data-validation-required-message="Enter Blouse Length." />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>2). Sleeve Length</b></span><span class="text-help-form">*</span>
                                                    <input type="input" id="txt_blouse_sleeve_length" name="txt_blouse_sleeve_length" class="form-control input-sm" placeholder="Sleeve Length" required required data-validation-required-message="Enter Sleeve Length." />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>3). Around Bust</b></span><span class="text-help-form">*</span>
                                                    <select class="form-control input-sm" id="cbo_blouse_aroundbust" name="cbo_blouse_aroundbust" required data-validation-required-message="Select Around Bust.">
                                                        <?php
                                                        $arr_around_bust = array();
                                                        $arr_around_bust = explode(",", $STR_TAILORING_OPTION_CUSTOMIZED_AROUND_BUST);
                                                        ?>
                                                        <option value="0">-- SELECT SIZE --</option>
                                                        <?php
                                                        for ($i = 0; $i < (Count($arr_around_bust)); $i++) { ?>
                                                            <option value="<?php print $arr_around_bust[$i]; ?>"><?php print $arr_around_bust[$i]; ?></option>
                                                        <?php
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>4). Around Above Waist</b></span><span class="text-help-form">*</span>
                                                    <input type="input" id="txt_blouse_abovearoundwaist" name="txt_blouse_abovearoundwaist" class="form-control input-sm" placeholder="Around Above Waist" required required data-validation-required-message="Enter Around Above Waist." />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>5). Front Neck Depth</b></span><span class="text-help-form">*</span>
                                                    <input type="input" id="txt_blouse_frontneckdepth" name="txt_blouse_frontneckdepth" class="form-control input-sm" placeholder="Front Neck Depth" required required data-validation-required-message="Enter Front Neck Depth." />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>6). Back Neck Depth</b></span><span class="text-help-form"> *</span>
                                                    <input type="input" id="txt_blouse_backneckdepth" name="txt_blouse_backneckdepth" class="form-control input-sm" placeholder="Back Neck Depth" required required data-validation-required-message="Enter Back Neck Depth." />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php if (in_array("Blouse Pads", $arr_selected_option)) { ?>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>Want Blouse Pad?</b></span><span class="text-help-form">*</span>
                                                    <select class="form-control input-sm" id="cbo_blouse_pad" name="cbo_blouse_pad" required data-validation-required-message="Select Around Bust.">
                                                        <?php /* ?><?php
                                                    $arr_around_bust = array();
                                                    $arr_around_bust = explode(",", $STR_TAILORING_OPTION_CUSTOMIZED_AROUND_BUST);
                                                    ?>
                                                    <option value="0">-- SELECT SIZE --</option>
                                                    <?php 
                                                    for($i = 0; $i<(Count($arr_around_bust)); $i++)
                                                    { ?>
                                                        <option value="<?php print $arr_around_bust[$i]; ?>"><?php print $arr_around_bust[$i]; ?></option>
                                                    <?php 
                                                    } ?><?php */ ?>

                                                        <option value="NO">NO</option>
                                                        <option value="YES">YES</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <?php } ?>

                                <?php if (in_array("Lehenga Choli", $arr_selected_option)) { ?>
                                    <div class="row padding-10">
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>A). Around Waist</b></span><span class="text-help-form"> *</span>
                                                    <input type="input" id="txt_lehenga_aroundwaist" name="txt_lehenga_aroundwaist" class="form-control input-sm" placeholder="Around Waist" required required data-validation-required-message="Enter Around Waist." />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>B). Around Hips</b></span><span class="text-help-form"> *</span>
                                                    <input type="input" id="txt_lehenga_aroundhips" name="txt_lehenga_aroundhips" class="form-control input-sm" placeholder="Around Hips" required required data-validation-required-message="Enter Around Hips." />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>C). Lehenga Length</b></span><span class="text-help-form"> *</span>
                                                    <input type="input" id="txt_lehenga_length" name="txt_lehenga_length" class="form-control input-sm" placeholder="Lehenga Length" required required data-validation-required-message="Enter Lehenga Length." />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if (in_array("Salwar Suit", $arr_selected_option) || in_array("Salwar Suits", $arr_selected_option) || in_array("Salwar", $arr_selected_option)) { ?>
                                    <div class="row padding-10">
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>1). Your Height</b></span><span class="text-help-form">*</span>
                                                    <input type="input" id="txt_salwar_yourheight" name="txt_salwar_yourheight" class="form-control input-sm" placeholder="Your Height" required required data-validation-required-message="Enter Your Height." />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>2). Front Neck Depth</b></span><span class="text-help-form">*</span>
                                                    <input type="input" id="txt_salwar_frontneckdepth" name="txt_salwar_frontneckdepth" class="form-control input-sm" placeholder="Front Neck Depth" required required data-validation-required-message="Enter Front Neck Depth." />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>3). Bottom Length</b></span><span class="text-help-form">*</span>
                                                    <input type="input" id="txt_salwar_bottomlength" name="txt_salwar_bottomlength" class="form-control input-sm" placeholder="Bottom Length" required required data-validation-required-message="Enter Bottom Length." />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>4). Kameez Length</b></span><span class="text-help-form">*</span>
                                                    <input type="input" id="txt_salwar_kameezlength" name="txt_salwar_kameezlength" class="form-control input-sm" placeholder="Kameez Length" required required data-validation-required-message="Enter Kameez Length." />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>5). Sleeve Length</b></span><span class="text-help-form">*</span>
                                                    <input type="input" id="txt_salwar_sleevelength" name="txt_salwar_sleevelength" class="form-control input-sm" placeholder="Sleeve Length" required required data-validation-required-message="Enter Sleeve Length." />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>6). Sleeve Style</b></span><span class="text-help-form">*</span>
                                                    <select class="form-control input-sm" id="cbo_salwar_sleevestyle" name="cbo_salwar_sleevestyle" required required data-validation-required-message="Select Sleeve Style.">
                                                        <?php
                                                        $arr_sleeve_style = array();
                                                        $arr_sleeve_style = explode(",", $STR_TAILORING_OPTION_CUSTOMIZED_SLEEVE_STYLE);  ?>
                                                        <option value="0">-- SLEEVE STYLE --</option>
                                                        <?php
                                                        for ($i = 0; $i < (Count($arr_sleeve_style)); $i++) { ?>
                                                            <option value="<?php print $arr_sleeve_style[$i]; ?>"><?php print $arr_sleeve_style[$i]; ?></option>
                                                        <?php
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>7). Around Bust</b></span><span class="text-help-form">*</span>
                                                    <select class="form-control input-sm" id="cbo_salwar_aroundbust" name="cbo_salwar_aroundbust" required data-validation-required-message="Select Around Bust.">
                                                        <?php
                                                        $arr_around_bust = array();
                                                        $arr_around_bust = explode(",", $STR_TAILORING_OPTION_CUSTOMIZED_AROUND_BUST);
                                                        ?>
                                                        <option value="0">-- AROUND BUST --</option>
                                                        <?php
                                                        for ($i = 0; $i < (Count($arr_around_bust)); $i++) { ?>
                                                            <option value="<?php print $arr_around_bust[$i]; ?>"><?php print $arr_around_bust[$i]; ?></option>
                                                        <?php
                                                        } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>8). Around Above Waist</b></span><span class="text-help-form">*</span>
                                                    <input type="input" id="txt_salwar_abovearoundwaist" name="txt_salwar_abovearoundwaist" class="form-control input-sm" placeholder="Around Above Waist" required required data-validation-required-message="Enter Around Above Waist." />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>9). Around Waist</b></span><span class="text-help-form"> *</span>
                                                    <input type="input" id="txt_salwar_aroundwaist" name="txt_salwar_aroundwaist" class="form-control input-sm" placeholder="Around Waist" required required data-validation-required-message="Enter Around Waist." />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>10). Around Hips</b></span><span class="text-help-form">*</span>
                                                    <input type="input" id="txt_salwar_aroundhips" name="txt_salwar_aroundhips" class="form-control input-sm" placeholder="Around Hips" required required data-validation-required-message="Enter Around Hips." />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>11). Back Neck Depth</b></span><span class="text-help-form">*</span>
                                                    <input type="input" id="txt_salwar_backneckdepth" name="txt_salwar_backneckdepth" class="form-control input-sm" placeholder="Back Neck Depth" required required data-validation-required-message="Enter Back Neck Depth." />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>12). Around Thigh</b></span><span class="text-help-form"> *</span>
                                                    <input type="input" id="txt_salwar_aroundthigh" name="txt_salwar_aroundthigh" class="form-control input-sm" placeholder="Around Thigh" required required data-validation-required-message="Enter Around Thigh." />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>13). Around Knee</b></span><span class="text-help-form"> *</span>
                                                    <input type="input" id="txt_salwar_aroundknee" name="txt_salwar_aroundknee" class="form-control input-sm" placeholder="Around Knee" required required data-validation-required-message="Enter Around Knee." />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>14). Around Calf</b></span><span class="text-help-form">*</span>
                                                    <input type="input" id="txt_salwar_aroundcalf" name="txt_salwar_aroundcalf" class="form-control input-sm" placeholder="Around Calf" required required data-validation-required-message="Enter Around Calf." />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <span class="small"><b>15). Around Ankle</b></span><span class="text-help-form"> *</span>
                                                    <input type="input" id="txt_salwar_aroundankle" name="txt_salwar_aroundankle" class="form-control input-sm" placeholder="Around Ankle" required required data-validation-required-message="Enter Around Ankle." />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                <?php } ?>
                            </div>
                            </div>
                            <!-- Size Chart -->
                            <div class="modal fade" id="myModal01" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close mdl-close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <?php if ($str_visible_cms_sizechart == 'YES') {
                                                if ($str_desc_cms_sizechart != "" | $str_desc_cms_sizechart != "<br/>") {
                                            ?><p><?php print $str_desc_cms_sizechart; ?></p><?php
                                                                                        }
                                                                                    } ?>
                                        </div>
                                        <div class="modal-body">
                                            <ul id="myTab" class="nav nav-tabs nav-justified">

                                                <li class="active"><a href="#service-two" data-toggle="tab">
                                                        <h4 class="nopadding"><b>Size in inches</b></h4>
                                                    </a>
                                                </li>
                                                <li class=""><a href="#service-one" data-toggle="tab">
                                                        <h4 class="nopadding"><b>Size in cm</b></h4>
                                                    </a>
                                                </li>
                                            </ul>

                                            <div id="myTabContent" class="tab-content">
                                                <div class="tab-pane fade" id="service-one">
                                                    <br />
                                                    <div class="table-responsive">
                                                        <table class="table table-striped table-bordered">
                                                            <tbody>
                                                                <tr>
                                                                    <th>Size</th>
                                                                    <th>Bust</th>
                                                                    <th>Waist</th>
                                                                    <th>Hip</th>
                                                                </tr>
                                                                <?php
                                                                $arr_test = array_keys($str_list_xml_sizechart);
                                                                if (trim($arr_test[0]) != "ROOT_ITEM") {
                                                                    foreach ($str_list_xml_sizechart as $key => $val) {
                                                                        if (is_array($val)) { ?>
                                                                            <?php if ($str_list_xml_sizechart[$key]['VISIBLE'] == 'YES') { ?>

                                                                                <?php // if($str_list_xml_sizechart[$key]['value1'] != 0.00 | $str_list_xml_sizechart[$key]['value2'] != 0.00 | $str_list_xml_sizechart[$key]['value3'] != 0.00 | $str_list_xml_sizechart[$key]['value4'] != 0.00){
                                                                                ?>
                                                                                <tr>
                                                                                    <td data-th="Size"><?php print($str_list_xml_sizechart[$key]['VALUE1'] . " centimetres"); ?></td>
                                                                                    <td data-th="Bust"><?php print($str_list_xml_sizechart[$key]['VALUE2'] . " centimetres"); ?></td>
                                                                                    <td data-th="Waist"><?php print($str_list_xml_sizechart[$key]['VALUE3'] . " centimetres"); ?></td>
                                                                                    <td data-th="Hip"><?php print($str_list_xml_sizechart[$key]['VALUE4'] . " centimetres"); ?></td>
                                                                                </tr>

                                                                    <?php       }
                                                                        }
                                                                    }
                                                                } else { ?>
                                                                    <tr>
                                                                        <td colspan="4" class="text-center"><?php print $STR_MSG_NO_DATA_AVAILABLE; ?></td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade active in" id="service-two">
                                                    <br />
                                                    <div class="table-responsive">
                                                        <table class="table table-striped table-bordered">
                                                            <tbody>
                                                                <tr>
                                                                    <th>Size</th>
                                                                    <th>Bust</th>
                                                                    <th>Waist</th>
                                                                    <th>Hip</th>
                                                                </tr>
                                                                <?php
                                                                $arr_test = array_keys($str_list_xml_sizechart);
                                                                if (trim($arr_test[0]) != "ROOT_ITEM") {
                                                                    foreach ($str_list_xml_sizechart as $key => $val) {
                                                                        if (is_array($val)) { ?>


                                                                            <?php if ($str_list_xml_sizechart[$key]['VISIBLE'] == 'YES') { ?>
                                                                                <?php
                                                                                $int_cm_value1 = $str_list_xml_sizechart[$key]['VALUE1'];
                                                                                $int_inch_value1 = ceil($int_cm_value1 / 2.54);

                                                                                $int_cm_value2 = $str_list_xml_sizechart[$key]['VALUE2'];
                                                                                $int_inch_value2 = ceil($int_cm_value2 / 2.54);

                                                                                $int_cm_value3 = $str_list_xml_sizechart[$key]['VALUE3'];
                                                                                $int_inch_value3 = ceil($int_cm_value3 / 2.54);

                                                                                $int_cm_value4 = $str_list_xml_sizechart[$key]['VALUE4'];
                                                                                $int_inch_value4 = ceil($int_cm_value4 / 2.54); ?>

                                                                                <tr>
                                                                                    <td data-th="Size"><?php print($int_inch_value1 . " inches"); ?></td>
                                                                                    <td data-th="Bust"><?php print($int_inch_value2 . " inches"); ?></td>
                                                                                    <td data-th="Waist"><?php print($int_inch_value3 . " inches"); ?></td>
                                                                                    <td data-th="Hip"><?php print($int_inch_value4 . " inches"); ?></td>
                                                                                </tr>

                                                                            <?php           } else {
                                                                            ?><tr>
                                                                                    <td colspan="4" class="text-center"><?php print $STR_MSG_NO_DATA_AVAILABLE; ?></td>
                                                                                </tr>
                                                                    <?php }
                                                                        }
                                                                    }
                                                                } else {
                                                                    ?><tr>
                                                                        <td colspan="4" class="text-center"><?php print $STR_MSG_NO_DATA_AVAILABLE; ?></td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-------------end size chart------------------>
                            <!-- How To Measure -->
                            <div class="modal fade" id="myModal03" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close mdl-close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <?php if ($str_visible_cms_measurechart == 'YES') {
                                                if ($str_desc_cms_measurechart != "" | $str_desc_cms_measurechart != "<br/>") {
                                            ?><p><?php print $str_desc_cms_measurechart; ?></p><?php
                                                                                            }
                                                                                        } ?>
                                        </div>
                                        <div class="modal-body" align="center">
                                            <?php
                                            $arr_test = array_keys($str_list_xml_measurechart);
                                            if (trim($arr_test[0]) != "ROOT_ITEM") {
                                                foreach ($str_list_xml_measurechart as $key => $val) {
                                                    if (is_array($val)) { ?>
                                                        <?php if ($str_list_xml_measurechart[$key]['VISIBLE'] == 'YES') { ?>
                                                            <img class="img-responsive" src="<?php print($str_img_path_measurement_chart . $str_list_xml_measurechart[$key]["IMAGEFILENAME"]); ?>" border="0" alt="<?php print $str_title_page; ?> Image" title="<?php print $str_title_page; ?> Image" />
                                                            <p align="justify"><?php print($str_list_xml_measurechart[$key]['DESCRIPTION']); ?></p>

                                                            <?php // if($str_list_xml_measurechart[$key]['value1'] != 0.00 | $str_list_xml_measurechart[$key]['value2'] != 0.00 | $str_list_xml_measurechart[$key]['value3'] != 0.00 | $str_list_xml_measurechart[$key]['value4'] != 0.00){
                                                            ?>


                                                <?php     }
                                                    }
                                                }
                                            } else {
                                                ?><p><?php print $STR_MSG_NO_DATA_AVAILABLE; ?></p>
                                            <?php } ?>



                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End How To Measure -->
                        </div>
                    <?php } ?>
                    <div class="row padding-10">
                        <div class="col-md-12">
                            <hr />
                            <?php /* ?><input type="hidden" id="hdn_prodtitle" name="hdn_prodtitle" value="<?php print $rs_list->Fields("title")." ".$rs_list->Fields("itemcode"); ?>" /><?php */ ?>
                            <input type="hidden" id="hdn_prodtitle" name="hdn_prodtitle" value="<?php print $rs_list->Fields("title"); ?>" />
                            <input type="hidden" id="hdn_itemcode" name="hdn_itemcode" value="<?php print $rs_list->Fields("itemcode"); ?>" />
                            <input type="hidden" id="hdn_prodpkid" name="hdn_prodpkid" value="<?php print $rs_list->Fields("pkid"); ?>" />
                            <input type="hidden" id="hdn_catpkid" name="hdn_catpkid" value="<?php print $rs_list->Fields("catpkid");  ?>" />
                            <input type="hidden" id="hdn_subcatpkid" name="hdn_subcatpkid" value="<?php print $rs_list->Fields("subcatpkid"); ?>" />
                            <?php
                            if ($str_member_flag == TRUE) {
                                if ($rs_list->Fields("memberprice") != "" && $rs_list->Fields("memberprice") > 0 && $str_usertype == "WHOLESALER") { ?>
                                    <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $rs_list->Fields("memberprice");  ?>" />
                                    <?php   } else {
                                    if ($rs_list->Fields("ourprice") != "" && $rs_list->Fields("ourprice") > 0) { ?>
                                        <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $rs_list->Fields("ourprice");  ?>" />
                                    <?php   } else { ?>
                                        <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $rs_list->Fields("listprice");  ?>" />
                                    <?php   }
                                }
                            } else {

                                if ($rs_list->Fields("ourprice") != "" && $rs_list->Fields("ourprice") > 0) { ?>
                                    <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $rs_list->Fields("ourprice");  ?>" />
                                <?php   } else { ?>
                                    <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $rs_list->Fields("listprice");  ?>" />
                            <?php   }
                            } ?>

                            <input type="hidden" id="hdn_ishippingvalue" name="hdn_ishippingvalue" value="<?php print $rs_list->Fields("ishippingvalue");  ?>" />
                            <input type="hidden" id="hdn_dshippingvalue" name="hdn_dshippingvalue" value="<?php print $rs_list->Fields("dshippingvalue");  ?>" />
                            <input type="hidden" id="hdn_ishippinginfo" name="hdn_ishippinginfo" value="<?php print $rs_list->Fields("ishippinginfo");  ?>" />
                            <input type="hidden" id="hdn_dshippinginfo" name="hdn_dshippinginfo" value="<?php print $rs_list->Fields("dshippinginfo");  ?>" />
                            <?php /* ?><input type="hidden" id="hdn_memberpkid" name="hdn_memberpkid" value="<?php print $int_memberpkid;  ?>" />
                    <input type="hidden" id="hdn_memberid" name="hdn_memberid" value="<?php print $str_memberid;  ?>" /><?php */ ?>
                            <input type="hidden" id="hdn_userpkid" name="hdn_userpkid" value="<?php print $int_userpkid;  ?>" />
                            <input type="hidden" id="hdn_username" name="hdn_username" value="<?php print $str_username;  ?>" />

                            <input type="hidden" id="hdn_discount" name="hdn_discount" value="<?php print $int_wholesaler_discount;  ?>" />
                            <input type="hidden" id="hdn_referredbypkid" name="hdn_referredbypkid" value="<?php print $int_referredbypkid;  ?>" />
                            <input type="hidden" id="hdn_commission" name="hdn_commission" value="<?php print $int_commission;  ?>" />
                            <input type="hidden" id="hdn_weight" name="hdn_weight" value="<?php print $rs_list->Fields("weight");  ?>" />

                            <?php $skuval = "";
                            $skuval = GetEncryptId($int_pkid); ?>
                            <input type="hidden" id="specssku" name="specssku" value="<?php print $skuval;  ?>" />


                            <div id="success"></div>
                            <div class="row padding-10">

                                <?php if ($rs_list->fields("availability") == "YES") { ?>
                                    <div class="col-md-4 col-sm-4 col-xs-12" align="center">
                                        <button type="submit" class="btn btn-primary btn-block" id='btn_addtocart'><i class="fa fa-cart-plus"></i>&nbsp;<b>ADD TO CART</b></button>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12" align="center">
                                        <button type="submit" class="btn btn-primary btn-block" id='btn_buy_now'><i class="fa fa-bolt"></i>&nbsp;<b>BUY NOW</b></button>
                                        <?php /* ?><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_cart.php" class="btn btn-primary btn-block"><i class="fa fa-bolt"></i>&nbsp;<b>BUY NOW</b></a><?php */ ?>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12" align="center">
                                        <a href="https://wa.me/<?php print $str_whatsapp_number; ?>?text=<?php print "I am interested in buying this product " . $rs_list->fields("itemcode"); ?>" class="btn btn-default btn-block"><i class="fa fa-whatsapp"></i>&nbsp;<b>WHATSAPP</b></a>
                                    </div>
                                <?php } else {  ?>
                                    <div class="col-md-4 col-sm-4 col-xs-12" align="center">
                                        <a class="btn btn-danger btn-block disabled"><i class="fa fa-times-circle-o"></i>&nbsp;<b>Out Of Stock</b></a>
                                    <?php } ?>
                                    </div>

                            </div>
                </form>

            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="row padding-10">
                <div class="col-md-12">
                    <?php
                    ## --------------------------------------------- START : WISHLIST------------------------------------------
                    if ($str_member_flag) {
                        $str_query_select = "";
                        $str_query_select = "SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=" . $_SESSION["userpkid"] . " AND itempkid=" . $rs_list->fields("pkid");

                        $rs_list_fav = GetRecordSet($str_query_select);

                        $str_fav_class = "";
                        $str_fav_flag = "";
                        $str_fav_style = "";
                        if ($rs_list_fav->Count() == 0) {
                            $str_fav_class = "text-muted";
                            $str_fav_flag = "FAV";
                            $str_fav_style = "cursor: pointer;";
                        } else if ($rs_list_fav->Count() > 0) {
                            $str_fav_class = "text-primary";
                            $str_fav_flag = "UNFAV";
                            $str_fav_style = "cursor: pointer;";
                        }

                    ?>
                        <form name="frm_user_favorite_details" id="frm_user_favorite_details" novalidate>

                            <input type="hidden" name="userpkid" id="userpkid" value="<?php print($_SESSION["userpkid"]); ?>">
                            <input type="hidden" name="itempkid" id="itempkid" value="<?php print($rs_list->fields("pkid")) ?>">

                            <input type="hidden" name="flgfav" id="flgfav" value="<?php print($str_fav_flag); ?>">



                            <?php //print $rs_list_fav->Count(); 
                            ?>
                            <div id="successfav"></div>
                            <button id="frm_submit" name="frm_submit" type="submit" style="display: none;"></button>
                        </form>

                        <h4 align="center" class=""><a onclick="$('#frm_submit').click();" class="<?php print $str_fav_class; ?>" style="<?php print $str_fav_style; ?>"><i class="fa fa-heart"></i>&nbsp;
                                <?php if ($str_fav_flag == 'FAV') {
                                    print "Add To Favorite List";
                                } else if ($str_fav_flag == 'UNFAV') {
                                    print "Remove From Favorite List";
                                } ?>
                            </a></h4>

                        <script>
                            // $(document).ready(function() {
                            //     $('.color').change(function() {
                            //         $(this).attr('checked', true);
                            //     });
                            // });
                            $(function() {

                                $("#frm_user_favorite_details input").jqBootstrapValidation({
                                    preventSubmit: true,
                                    submitError: function($form, event, errors) {
                                        // something to have when submit produces an error ?
                                        // Not decided if I need it yet
                                    },
                                    submitSuccess: function($form, event) {
                                        event.preventDefault(); // prevent default submit behaviour
                                        // get values from FORM

                                        var userpkid = $("input#userpkid").val();
                                        //alert(userpkid);
                                        var itempkid = $("input#itempkid").val();
                                        var flgfav = $("input#flgfav").val();


                                        $.ajax({

                                            url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                            type: "POST",
                                            data: {

                                                userpkid: userpkid,
                                                itempkid: itempkid,
                                                flgfav: flgfav
                                            },
                                            cache: false,
                                            success: function(data) {
                                                //alert(data);
                                                var $responseText = JSON.parse(data);
                                                if ($responseText.status == 'SUC')
                                                //                                        if(($responseText.status).equal("SUC"))
                                                {
                                                    // Success message
                                                    //                        alert(data);

                                                    $('#successfav').html("<div class='alert alert-success small'>");
                                                    $('#successfav > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                        .append("</button>");
                                                    $('#successfav > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                    $('#successfav > .alert-success ').append('</div>');
                                                    //setTimeout(function(){ location.reload() }, 2000);
                                                    //setTimeout(function(){ location.reload(); }, 5000);
                                                    setTimeout(function() {
                                                        $("#successfav").hide();
                                                    }, 5000);

                                                } else if ($responseText.status == 'ERR') {
                                                    // Fail message
                                                    $('#successfav').html("<div class='alert alert-danger'>");
                                                    $('#successfav > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                        .append("</button>");
                                                    $('#successfav > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                    $('#successfav > .alert-danger').append('</div>');

                                                }
                                            },

                                        })
                                    },
                                    filter: function() {
                                        return $(this).is(":visible");
                                    },
                                });

                                $("a[data-toggle=\"tab\"]").click(function(e) {
                                    e.preventDefault();
                                    $(this).tab("show");
                                });
                            });

                            $('#name').focus(function() {
                                $('#success').html('');
                            });
                        </script>
                    <?php } else { ?>
                        <h4 align="center" class=""><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>"><i class="fa fa-heart"></i>&nbsp;Add to Favorite List</a></h4>
                    <?php }
                    ## --------------------------------------------- END : WISHLIST------------------------------------------ 
                    ?>








                </div>
            </div>
            <div class="row padding-10">
                <div class="col-md-12">
                    <div class="count-bg">
                        <p>Item Total:<span class="text-muted"></span></p>
                        <hr />
                        <h4 align="right"><span class="text-danger"><?php print $str_currency_symbol; ?>&nbsp;<span class="text-danger" id="list_price"></span></h4>
                        <h4 align="right"><span class="text-muted">(Tailoring)</span> + <span class="text-danger"><?php print $str_currency_symbol; ?>&nbsp;<span class="text-danger" id="tailoring_price">0.00</span><span id="hdn_tailoring_price" style="display: none;">0.00</span></h4>
                        <hr />
                        <h3 align="right">Item Total: <b class="text-success"><?php print $str_currency_symbol; ?></b>&nbsp;<b class="text-success" id="total_price"></b></h3>
                        <p align="right" class="nopadding">
                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_cart.php" type="submit" title="Click to proceed" class="btn btn-primary btn-lg"><i class="fa fa-check"></i>&nbsp;<b>CHECKOUT</b></a>
                        </p>
                        <hr />
                        <p align="center" class="text-help">Know our <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/return-policy" title="">Return Policy</a> <br />Any Questions? <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/contact" title="">Just Ask</a> </p>
                    </div>
                </div>
            </div>
            <?php /* ?><div class="row padding-10">
                    <div class="col-md-12" align="center">
                         <div class="ssk-group">
                             <a href="" class="ssk ssk-facebook"></a>
                             <a href="" class="ssk ssk-twitter"></a>
                             <a href="" class="ssk ssk-google-plus"></a>
                             <a href="" class="ssk ssk-pinterest"></a>
                             <a href="" class="ssk ssk-linkedin"></a>
                         </div>
                    </div>
                </div>
                <br/><?php */ ?>
            <div class="row padding-10">
                <div class="col-md-12">
                    <form id="frm_cod" name="frm_cod" novalidate>
                        <div class="control-group nopadding">
                            <div class="controls ">
                                <?php /* ?><label>Check Cash on Delivery *</label><?php */ ?>
                                <div class="input-group">
                                    <input type="text" id="txt_pincode" name="txt_pincode" class="form-control text-left" placeholder="Enter Pincode Here" required data-validation-required-message="Please Pincode here." />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" id="btn_cod" name="btn_cod" type="submit">Check</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div id="success_cod"></div>
                    </form>
                </div><br />
            </div>
        </div>
    </div>
    <div class="row padding-10">
        <div class="col-lg-12 col-md-12">
            <div id="integration-list">
                <ul>
                    <li>
                        <a class="expand">
                            <div class="right-arrow pull-right">+</div>
                            <h4 class="nopadding text-primary"><i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;Product Details</h4>
                        </a>

                        <div class="detail">
                            <div id="sup">
                                <div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered">
                                                <tbody>
                                                    <?php
                                                    $str_query_select = "";
                                                    $str_query_select = "SELECT * FROM " . $STR_DB_TR_TABLE_NAME_TYPE . " a LEFT JOIN " . $STR_DB_TABLE_NAME_TYPE . " b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid=" . $rs_list->Fields("pkid");
                                                    $rs_list_type = GetRecordSet($str_query_select);
                                                    if ($rs_list_type->Count() > 0) { ?>
                                                        <tr>
                                                            <td><b>Type</b></td>
                                                            <td>
                                                                <?php while (!$rs_list_type->EOF()) { ?>
                                                                    <p class="nopadding"><?php print $rs_list_type->Fields("title"); ?></p>
                                                                <?php
                                                                    $rs_list_type->MoveNext();
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    <?php
                                                    $str_query_select = "";
                                                    $str_query_select = "SELECT * FROM " . $STR_DB_TR_TABLE_NAME_FABRIC . " a LEFT JOIN " . $STR_DB_TABLE_NAME_FABRIC . " b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid=" . $rs_list->Fields("pkid");
                                                    $rs_list_fabric = GetRecordSet($str_query_select);
                                                    if ($rs_list_fabric->Count() > 0) { ?>
                                                        <tr>
                                                            <td><b>Fabric</b></td>
                                                            <td>
                                                                <?php while (!$rs_list_fabric->EOF()) { ?>
                                                                    <p class="nopadding"><?php print $rs_list_fabric->Fields("title"); ?></p>
                                                                <?php
                                                                    $rs_list_fabric->MoveNext();
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    <?php
                                                    $str_query_select = "";
                                                    $str_query_select = "SELECT * FROM " . $STR_DB_TR_TABLE_NAME_WORK . " a LEFT JOIN " . $STR_DB_TABLE_NAME_WORK . " b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid=" . $rs_list->Fields("pkid");
                                                    $rs_list_work = GetRecordSet($str_query_select);
                                                    if ($rs_list_work->Count() > 0) { ?>
                                                        <tr>
                                                            <td><b>Work</b></td>
                                                            <td>
                                                                <?php while (!$rs_list_work->EOF()) { ?>
                                                                    <p class="nopadding"><?php print $rs_list_work->Fields("title"); ?></p>
                                                                <?php
                                                                    $rs_list_work->MoveNext();
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>

                                                    <?php
                                                    $str_query_select = "";
                                                    $str_query_select = "SELECT * FROM " . $STR_DB_TR_TABLE_NAME_LENGTH . " a LEFT JOIN " . $STR_DB_TABLE_NAME_LENGTH . " b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid=" . $rs_list->Fields("pkid");
                                                    $rs_list_length = GetRecordSet($str_query_select);
                                                    if ($rs_list_length->Count() > 0) { ?>
                                                        <tr>
                                                            <td><b>Length</b></td>
                                                            <td>
                                                                <?php while (!$rs_list_length->EOF()) { ?>
                                                                    <p class="nopadding"><?php print $rs_list_length->Fields("title"); ?></p>
                                                                <?php
                                                                    $rs_list_length->MoveNext();
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    <?php
                                                    $str_query_select = "";
                                                    $str_query_select = "SELECT * FROM " . $STR_DB_TR_TABLE_NAME_OCCASION . " a LEFT JOIN " . $STR_DB_TABLE_NAME_OCCASION . " b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid=" . $rs_list->Fields("pkid");
                                                    $rs_list_occasion = GetRecordSet($str_query_select);
                                                    if ($rs_list_occasion->Count() > 0) { ?>
                                                        <tr>
                                                            <td><b>Occasion</b></td>
                                                            <td>
                                                                <?php while (!$rs_list_occasion->EOF()) { ?>
                                                                    <p class="nopadding"><?php print $rs_list_occasion->Fields("title"); ?></p>
                                                                <?php
                                                                    $rs_list_occasion->MoveNext();
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    <?php
                                                    $str_query_select = "";
                                                    $str_query_select = "SELECT * FROM " . $STR_DB_TR_TABLE_NAME_SHIPPING . " a LEFT JOIN " . $STR_DB_TABLE_NAME_SHIPPING . " b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid=" . $rs_list->Fields("pkid");
                                                    $rs_list_shipping = GetRecordSet($str_query_select);
                                                    if ($rs_list_shipping->Count() > 0) { ?>
                                                        <tr>
                                                            <td><b>Shipping Type</b></td>
                                                            <td>
                                                                <?php while (!$rs_list_shipping->EOF()) { ?>
                                                                    <p class="nopadding"><?php print $rs_list_shipping->Fields("title"); ?></p>
                                                                <?php
                                                                    $rs_list_shipping->MoveNext();
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    <?php if ($rs_list->Fields("weight") > 0) { ?>
                                                        <tr>
                                                            <td><b>Weight</b></td>
                                                            <td>
                                                                <p class="nopadding"><?php print $rs_list->Fields("weight") . " kg"; ?></p>

                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($rs_list->Fields("description") != "") { ?>
                                    <p class="more" align="justify">
                                        <?php print $rs_list->Fields("description"); ?>
                                    </p>
                                <?php } ?>
                            </div>
                        </div>
                    </li>
                    <?php
                    if ($rs_list->Fields("displayasadditionalinfo") == "YES") {
                        $str_query_select = "";
                        $str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME_ADDITIONAL_INFO . " WHERE visible='YES'" . $STR_DB_TABLE_NAME_ADDITIONAL_INFO_ORDER_BY . "";
                        $rs_list_additional_info = GetRecordSet($str_query_select);
                        if ($rs_list_additional_info->Count() > 0) { ?>
                            <?php while (!$rs_list_additional_info->EOF()) { ?>
                                <li>
                                    <a class="expand">
                                        <div class="right-arrow pull-right">+</div>
                                        <h4 class="nopadding text-primary"><i class="fa fa-angle-double-right" aria-hidden="true"></i>&nbsp;<?php print $rs_list_additional_info->Fields("title"); ?></h4>
                                    </a>
                                    <div class="detail">
                                        <div id="sup">
                                            <hr />
                                            <div class="row padding-10">
                                                <?php if ($rs_list_additional_info->Fields("imagefilename") != "") { ?>
                                                    <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                                                        <img src="<?php print $UPLOAD_IMG_PATH_ADDITIONAL_INFO . $rs_list_additional_info->Fields("imagefilename"); ?>" class="img-responsive" alt="<?php print $rs_list_additional_info->Fields("title"); ?>" title="<?php print $rs_list_additional_info->Fields("title"); ?>" />
                                                    </div>
                                                    <div class="col-md-9 col-lg-9 col-sm-8 col-xs-12">
                                                        <?php if ($rs_list_additional_info->Fields("description") != "") { ?>
                                                            <p align="justify"><?php print $rs_list_additional_info->Fields("description"); ?></p>
                                                        <?php } ?>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                        <?php if ($rs_list_additional_info->Fields("description") != "") { ?>
                                                            <p align="justify"><?php print $rs_list_additional_info->Fields("description"); ?></p>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>


                                        </div>
                                    </div>
                                </li>
                            <?php $rs_list_additional_info->MoveNext();
                            } ?>
                    <?php   }
                    } ?>

                </ul>
            </div>
        </div>
    </div>


    <?php
    # Similar Products
    if ($rs_list_similar->Count() > 0) { ?>
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <div class="section-title text-center border-line mb-10">
                    <h2 class="block_title">Similar Products</h2>
                </div>
            </div>
        </div><br />
        <div class="row padding-10 hidden-sm hidden-xs">
            <?php
            $int_cnt = 0;
            while (!$rs_list_similar->EOF()) {
            ?>
                <div class="col-md-3 col-lg-3">
                    <div class="product-box text-center">
                        <div class="row padding-10">
                            <div class="col-md-12 nopadding" align="center">
                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-",  str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))))); ?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>">
                                    <?php if ($rs_list_similar->Fields("thumbphotofilename") != "") { ?>
                                        <img alt="<?php print $rs_list_similar->Fields("title"); ?>" title="<?php print $rs_list_similar->Fields("title"); ?>" data-src="" src="<?php print $UPLOAD_IMG_PATH . $rs_list_similar->Fields("pkid") . "/" . $rs_list_similar->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                    <?php } else if ($rs_list_similar->Fields("imageurl") != "") { ?>
                                        <img alt="<?php print $rs_list_similar->Fields("title"); ?>" title="<?php print $rs_list_similar->Fields("title"); ?>" data-src="" src="<?php print $rs_list_similar->Fields("imageurl"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                    <?php } ?>
                                </a>

                                <?php
                                $str_query_select = "";
                                $str_query_select = "SELECT mainphotofilename FROM " . $STR_DB_TABLE_NAME_PHOTO . " WHERE masterpkid=" . $rs_list_similar->Fields("pkid") . " AND setasofferimage='LEFT' AND visible='YES' ORDER BY setasofferimage ASC";
                                $rs_list_offerimages_left = GetRecordSet($str_query_select);
                                //print $rs_list_offerimages->Count();
                                if ($rs_list_offerimages_left->Count() > 0) {
                                ?>

                                    <img src="<?php print $UPLOAD_IMG_PATH . $rs_list_similar->Fields("pkid") . "/" . $rs_list_offerimages_left->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-left" />
                                <?php }  ?>
                                <?php
                                $str_query_select = "";
                                $str_query_select = "SELECT mainphotofilename FROM " . $STR_DB_TABLE_NAME_PHOTO . " WHERE masterpkid=" . $rs_list_similar->Fields("pkid") . " AND setasofferimage='RIGHT' AND visible='YES' ORDER BY setasofferimage ASC";
                                $rs_list_offerimages_right = GetRecordSet($str_query_select);
                                //print $rs_list_offerimages->Count();
                                if ($rs_list_offerimages_right->Count() > 0) {
                                ?>
                                    <img src="<?php print $UPLOAD_IMG_PATH . $rs_list_similar->Fields("pkid") . "/" . $rs_list_offerimages_right->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-right" />
                                <?php }  ?>

                            </div>
                        </div>
                        <div class="row padding-10">
                            <div class="col-md-12 product-content-bg">
                                <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))))); ?>" title="<?php print $rs_list_similar->Fields("title"); //$str_hover_view_details; 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ?>"><b class="text-muted"><?php print MakeStringShort($rs_list_similar->Fields("title"), 23); ?></b></a></p>

                                <p>
                                    <?php if ($rs_list->Fields("displayasnew") == 'YES') {
                                        print $STR_ICON_PATH_NEW;
                                    } ?><?php if ($rs_list->Fields("displayasfeatured") == 'YES') {
                                            print "&nbsp;" . $STR_ICON_PATH_FEATURED;
                                        } ?><?php if ($rs_list->Fields("displayashot") == 'YES') {
                                                print "&nbsp;" . $STR_ICON_PATH_HOT;
                                            } ?> </p>

                                <p>
                                    <?php if ($str_user_type == "WHOLESALER" && $rs_list_similar->Fields("memberprice") > 0) {
                                        $int_wholesaler_price = "";
                                        $int_wholesaler_price = $rs_list_similar->Fields("memberprice") / $int_conversionrate;
                                        $int_hdn_price = $int_wholesaler_price;
                                    ?>
                                        <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_wholesaler_price), 2) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                    <?php } else { ?>

                                        <?php
                                        if ($rs_list_similar->Fields("ourprice") > 0 && ($rs_list_similar->Fields("ourprice") != $rs_list_similar->Fields("listprice"))) {
                                            $int_list_price = "";
                                            $int_list_price = $rs_list_similar->Fields("listprice") / $int_conversionrate;

                                            $int_our_price = "";
                                            $int_our_price = $rs_list_similar->Fields("ourprice") / $int_conversionrate;
                                            $int_hdn_price = $int_our_price;
                                        ?>
                                            <strike class='text-muted'><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 2) . "&nbsp;" . $str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                            <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_our_price), 2) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                            <?php
                                            $int_per_discount = 0;
                                            $int_per_discount = (100 - ($rs_list_similar->Fields("ourprice") / $rs_list_similar->Fields("listprice")) * 100);
                                            if ($int_per_discount > 0) { ?>
                                                <label class="label label-primary"><b><?php print number_format($int_per_discount, 2); ?>% OFF</b></label>
                                            <?php } ?>
                                        <?php } else {
                                            $int_list_price = "";
                                            $int_list_price = $rs_list_similar->Fields("listprice") / $int_conversionrate;
                                            $int_hdn_price = $int_list_price;
                                        ?>
                                            <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 2) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                        <?php }  ?>
                                    <?php }  ?>
                                </p>
                            </div>
                        </div>
                        <div id="success<?php print $rs_list_similar->fields("pkid"); ?>"></div>
                        <div class="row padding-10">
                            <div class="col-md-12">
                                <div class="row padding-10">
                                    <?php /*?><div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))));?>" type="button" class="product-button"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a> 
                                </div><?php */ ?>
                                    <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                        <?php /*?><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))))); ?>" type="button" class="product-buynow-button"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a><?php */ ?>

                                        <form class='myform' id="frm_add<?php print $rs_list_similar->Fields("pkid"); ?>" name="frm_add<?php print $rs_list_similar->Fields("pkid"); ?>" onsubmit="return addToCart('frm_add<?php print $rs_list_similar->Fields("pkid"); ?>')" novalidate>
                                            <input type="hidden" name="hdn_prodpkid" id="hdn_prodpkid" value="<?php print $rs_list_similar->Fields("pkid"); ?>">
                                            <input type="hidden" name="hdn_prodtitle" id="hdn_prodtitle" value="<?php print $rs_list_similar->Fields("title"); ?>">
                                            <input type="hidden" id="hdn_itemcode" name="hdn_itemcode" value="<?php print $rs_list_similar->Fields("itemcode"); ?>" />
                                            <?php /* ?>
						<input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $int_cat_pkid; ?>">
						<input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $int_sub_cat_pkid; ?>">
						<?php */ ?>
                                            <input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $rs_list_similar->Fields("catpkid"); ?>">
                                            <input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $rs_list_similar->Fields("subcatpkid"); ?>">

                                            <input type="hidden" name="hdn_weight" id="hdn_weight" value="<?php print $rs_list_similar->Fields("weight"); ?>">
                                            <input type="hidden" name="hdn_userpkid" id="hdn_userpkid" value="<?php print $int_userpkid; ?>">
                                            <input type="hidden" name="hdn_username" id="hdn_username" value="<?php print $str_username; ?>">
                                            <input type="hidden" name="hdn_discount" id="hdn_discount" value="<?php print $int_wholesaler_discount; ?>">
                                            <input type="hidden" name="hdn_referredbypkid" id="hdn_referredbypkid" value="<?php print $int_referredbypkid; ?>">
                                            <input type="hidden" name="hdn_commission" id="hdn_commission" value="<?php print $int_commission; ?>">
                                            <input type="hidden" name="rdo_color" id="rdo_color" value="<?php print $rs_list_similar->Fields("colortitle"); ?>">
                                            <input type="hidden" name="rdo_size" id="rdo_size" value="<?php print $rs_list_similar->Fields("sizetitle"); ?>">
                                            <input type="hidden" name="rdo_tailoringoption" id="rdo_tailoringoption" value="<?php print $int_tailoring_service_pkid; ?>">
                                            <input type="hidden" name="cbo_semistitched_stitchingsize" id="cbo_semistitched_stitchingsize" value="0">
                                            <input type="hidden" name="txt_blouse_length" id="txt_blouse_length" value="0">
                                            <input type="hidden" name="txt_blouse_sleeve_length" id="txt_blouse_sleeve_length" value="0">
                                            <input type="hidden" name="cbo_blouse_aroundbust" id="cbo_blouse_aroundbust" value="0">
                                            <input type="hidden" name="txt_blouse_abovearoundwaist" id="txt_blouse_abovearoundwaist" value="0">
                                            <input type="hidden" name="txt_blouse_frontneckdepth" id="txt_blouse_frontneckdepth" value="0">
                                            <input type="hidden" name="txt_blouse_backneckdepth" id="txt_blouse_backneckdepth" value="0">
                                            <input type="hidden" name="txt_lehenga_aroundwaist" id="txt_lehenga_aroundwaist" value="0">
                                            <input type="hidden" name="txt_lehenga_aroundhips" id="txt_lehenga_aroundhips" value="0">
                                            <input type="hidden" name="txt_lehenga_length" id="txt_lehenga_length" value="0">
                                            <input type="hidden" name="txt_salwar_yourheight" id="txt_salwar_yourheight" value="0">
                                            <input type="hidden" name="txt_salwar_frontneckdepth" id="txt_salwar_frontneckdepth" value="0">
                                            <input type="hidden" name="txt_salwar_bottomlength" id="txt_salwar_bottomlength" value="0">
                                            <input type="hidden" name="txt_salwar_kameezlength" id="txt_salwar_kameezlength" value="0">
                                            <input type="hidden" name="txt_salwar_sleevelength" id="txt_salwar_sleevelength" value="0">
                                            <input type="hidden" name="cbo_salwar_sleevestyle" id="cbo_salwar_sleevestyle" value="0">
                                            <input type="hidden" name="cbo_salwar_aroundbust" id="cbo_salwar_aroundbust" value="0">
                                            <input type="hidden" name="txt_salwar_abovearoundwaist" id="txt_salwar_abovearoundwaist" value="0">
                                            <input type="hidden" name="txt_salwar_aroundwaist" id="txt_salwar_aroundwaist" value="0">
                                            <input type="hidden" name="txt_salwar_aroundhips" id="txt_salwar_aroundhips" value="0">
                                            <input type="hidden" name="txt_salwar_backneckdepth" id="txt_salwar_backneckdepth" value="0">
                                            <input type="hidden" name="txt_salwar_aroundthigh" id="txt_salwar_aroundthigh" value="0">
                                            <input type="hidden" name="txt_salwar_aroundknee" id="txt_salwar_aroundknee" value="0">
                                            <input type="hidden" name="txt_salwar_aroundcalf" id="txt_salwar_aroundcalf" value="0">
                                            <input type="hidden" name="txt_salwar_aroundankle" id="txt_salwar_aroundankle" value="0">

                                            <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $int_hdn_price;  ?>" />
                                            <input type="hidden" id="txt_quantity" name="txt_quantity" value="1" />
                                            <input type="hidden" id="hdn_description" name="hdn_description" value="" />
                                            <input type="hidden" id="rdo_shipping" name="rdo_shipping" value="" />
                                            <?php
                                            $skuval = date("YmdHis");
                                            $skuval = GetEncryptId($rs_list_similar->Fields("pkid"));
                                            ?>
                                            <input type="hidden" id="specssku" name="specssku" value="<?php print $skuval;  ?>" />


                                            <button type="submit" class="product-buynow-button" name='btn_addtocart' id="btn_addtocart_<?php print $rs_list_similar->Fields("pkid"); ?>" style="border-bottom: 1px solid transparent; width: 100%;"><i class="fa fa-shopping-cart"></i></button>

                                        </form>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                        <?php
                                        ## --------------------------------------------- START : WISHLIST------------------------------------------
                                        if ($str_member_flag) {
                                            $str_query_select = "";
                                            $str_query_select = "SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=" . $_SESSION["userpkid"] . " AND itempkid=" . $rs_list_similar->fields("pkid");
                                            $rs_list_fav = GetRecordSet($str_query_select);

                                            $str_fav_class = "";
                                            $str_fav_flag = "";
                                            if ($rs_list_fav->Count() == 0) {
                                                $str_fav_class = "";
                                                $str_fav_flag = "FAV";
                                            } else if ($rs_list_fav->Count() > 0) {
                                                $str_fav_class = "text-primary";
                                                $str_fav_flag = "UNFAV";
                                            } ?>
                                            <form name="frm_user_favorite<?php print $rs_list_similar->fields("pkid"); ?>" id="frm_user_favorite<?php print $rs_list_similar->fields("pkid"); ?>" novalidate>

                                                <input type="hidden" name="userpkid" id="userpkid<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($_SESSION["userpkid"]); ?>">
                                                <input type="hidden" name="itempkid" id="itempkid<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($rs_list_similar->fields("pkid")) ?>">

                                                <input type="hidden" name="flgfav" id="flgfav<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($str_fav_flag); ?>">



                                                <?php //print $rs_list_fav->Count(); 
                                                ?>
                                                <button id="frm_submit<?php print($rs_list_similar->fields("pkid")) ?>" name="frm_submit<?php print($rs_list_similar->fields("pkid")) ?>" type="submit" style="display: none;"></button>
                                            </form>

                                            <a onclick="$('#frm_submit<?php print($rs_list_similar->fields("pkid")) ?>').click();" class="product-button <?php print $str_fav_class; ?>"><i class="fa fa-heart"></i></a>
                                            <script>
                                                $(function() {
                                                    $("#frm_user_favorite<?php print $rs_list_similar->fields("pkid"); ?> input").jqBootstrapValidation({
                                                        preventSubmit: true,
                                                        submitError: function($form, event, errors) {
                                                            // something to have when submit produces an error ?
                                                            // Not decided if I need it yet
                                                        },
                                                        submitSuccess: function($form, event) {
                                                            event.preventDefault(); // prevent default submit behaviour
                                                            // get values from FORM

                                                            var userpkid = $("input#userpkid<?php print($rs_list_similar->fields("pkid")) ?>").val();
                                                            //alert(userpkid);
                                                            var itempkid = $("input#itempkid<?php print($rs_list_similar->fields("pkid")) ?>").val();
                                                            var flgfav = $("input#flgfav<?php print($rs_list_similar->fields("pkid")) ?>").val();


                                                            $.ajax({

                                                                url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                                                type: "POST",
                                                                data: {

                                                                    userpkid: userpkid,
                                                                    itempkid: itempkid,
                                                                    flgfav: flgfav
                                                                },
                                                                cache: false,
                                                                success: function(data) {
                                                                    //alert(data);
                                                                    var $responseText = JSON.parse(data);
                                                                    if ($responseText.status == 'SUC')
                                                                    //                                        if(($responseText.status).equal("SUC"))
                                                                    {
                                                                        // Success message
                                                                        //                        alert(data);

                                                                        $('#success<?php print $rs_list_similar->fields("pkid"); ?>').html("<div class='alert alert-success small'>");
                                                                        $('#success<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                            .append("</button>");
                                                                        $('#success<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                                        $('#success<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').append('</div>');
                                                                        //setTimeout(function(){ location.reload() }, 2000);
                                                                        setTimeout(function() {
                                                                            location.reload();
                                                                        }, 5000);
                                                                    } else if ($responseText.status == 'ERR') {
                                                                        // Fail message
                                                                        $('#success<?php print $rs_list_similar->fields("pkid"); ?>').html("<div class='alert alert-danger'>");
                                                                        $('#success<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                            .append("</button>");
                                                                        $('#success<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                                        $('#success<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').append('</div>');

                                                                    }
                                                                },

                                                            })
                                                        },
                                                        filter: function() {
                                                            return $(this).is(":visible");
                                                        },
                                                    });

                                                    $("a[data-toggle=\"tab\"]").click(function(e) {
                                                        e.preventDefault();
                                                        $(this).tab("show");
                                                    });
                                                });
                                                /*When clicking on Full hide fail/success boxes */
                                                $('#name').focus(function() {
                                                    $('#success').html('');
                                                });
                                            </script>
                                        <?php } else { ?>
                                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>" class="product-button"><i class="fa fa-heart"></i></a>
                                        <?php }
                                        ## --------------------------------------------- END : WISHLIST------------------------------------------ 
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
                $int_cnt++;
                if ($int_cnt % 4 == 0) {
                    print "</div><div class='row padding-10 hidden-sm hidden-xs'>";
                }
                $rs_list_similar->MoveNext();
            }
            $rs_list_similar->MoveFirst();
            ?>
        </div>
        <div class="row padding-10 hidden-md hidden-lg hidden-xs">
            <?php
            $int_cnt = 0;
            while (!$rs_list_similar->EOF()) {
            ?>
                <div class="col-sm-4">
                    <div class="product-box text-center">
                        <div class="row padding-10">
                            <div class="col-md-12 nopadding" align="center">
                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))))); ?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>">
                                    <?php if ($rs_list_similar->Fields("thumbphotofilename") != "") { ?>
                                        <img alt="<?php print $rs_list_similar->Fields("title"); ?>" title="<?php print $rs_list_similar->Fields("title"); ?>" data-src="" src="<?php print $UPLOAD_IMG_PATH . $rs_list_similar->Fields("pkid") . "/" . $rs_list_similar->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                    <?php } else if ($rs_list_similar->Fields("imageurl") != "") { ?>
                                        <img alt="<?php print $rs_list_similar->Fields("title"); ?>" title="<?php print $rs_list_similar->Fields("title"); ?>" data-src="" src="<?php print $rs_list_similar->Fields("imageurl"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                    <?php } ?>
                                </a>

                                <?php
                                $str_query_select = "";
                                $str_query_select = "SELECT mainphotofilename FROM " . $STR_DB_TABLE_NAME_PHOTO . " WHERE masterpkid=" . $rs_list_similar->Fields("pkid") . " AND setasofferimage='LEFT' AND visible='YES' ORDER BY setasofferimage ASC";
                                $rs_list_offerimages_left = GetRecordSet($str_query_select);
                                //print $rs_list_offerimages->Count();
                                if ($rs_list_offerimages_left->Count() > 0) {
                                ?>

                                    <img src="<?php print $UPLOAD_IMG_PATH . $rs_list_similar->Fields("pkid") . "/" . $rs_list_offerimages_left->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-left" />
                                <?php }  ?>
                                <?php
                                $str_query_select = "";
                                $str_query_select = "SELECT mainphotofilename FROM " . $STR_DB_TABLE_NAME_PHOTO . " WHERE masterpkid=" . $rs_list_similar->Fields("pkid") . " AND setasofferimage='RIGHT' AND visible='YES' ORDER BY setasofferimage ASC";
                                $rs_list_offerimages_right = GetRecordSet($str_query_select);
                                //print $rs_list_offerimages->Count();
                                if ($rs_list_offerimages_right->Count() > 0) {
                                ?>
                                    <img src="<?php print $UPLOAD_IMG_PATH . $rs_list_similar->Fields("pkid") . "/" . $rs_list_offerimages_right->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-right" />
                                <?php }  ?>

                            </div>
                        </div>
                        <div class="row padding-10">
                            <div class="col-md-12 product-content-bg">
                                <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))))); ?>" title="<?php print $rs_list_similar->Fields("title"); //$str_hover_view_details; 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ?>"><b class="text-muted"><?php print MakeStringShort($rs_list_similar->Fields("title"), 23); ?></b></a></p>
                                <p>
                                    <?php if ($rs_list->Fields("displayasnew") == 'YES') {
                                        print $STR_ICON_PATH_NEW;
                                    } ?><?php if ($rs_list->Fields("displayasfeatured") == 'YES') {
                                            print "&nbsp;" . $STR_ICON_PATH_FEATURED;
                                        } ?><?php if ($rs_list->Fields("displayashot") == 'YES') {
                                                print "&nbsp;" . $STR_ICON_PATH_HOT;
                                            } ?> </p>
                                <?php /* ?><p>Product Code : 000000</p><?php */ ?>
                                <p>
                                    <?php if ($str_user_type == "WHOLESALER" && $rs_list_similar->Fields("memberprice") > 0) {
                                        $int_wholesaler_price = "";
                                        $int_wholesaler_price = $rs_list_similar->Fields("memberprice") / $int_conversionrate;
                                        $int_hdn_price = $int_wholesaler_price;
                                    ?>
                                        <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_wholesaler_price), 2) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                    <?php } else { ?>

                                        <?php
                                        if ($rs_list_similar->Fields("ourprice") > 0 && ($rs_list_similar->Fields("ourprice") != $rs_list_similar->Fields("listprice"))) {
                                            $int_list_price = "";
                                            $int_list_price = $rs_list_similar->Fields("listprice") / $int_conversionrate;

                                            $int_our_price = "";
                                            $int_our_price = $rs_list_similar->Fields("ourprice") / $int_conversionrate;
                                            $int_hdn_price = $int_our_price;
                                        ?>
                                            <strike class='text-muted'><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 2) . "&nbsp;" . $str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                            <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_our_price), 2) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                            <?php
                                            $int_per_discount = 0;
                                            $int_per_discount = (100 - ($rs_list_similar->Fields("ourprice") / $rs_list_similar->Fields("listprice")) * 100);
                                            if ($int_per_discount > 0) { ?>
                                                <label class="label label-primary"><b><?php print number_format($int_per_discount, 2); ?>% OFF</b></label>
                                            <?php } ?>
                                        <?php } else {
                                            $int_list_price = "";
                                            $int_list_price = $rs_list_similar->Fields("listprice") / $int_conversionrate;
                                            $int_hdn_price = $int_list_price;
                                        ?>
                                            <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 2) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                        <?php }  ?>
                                    <?php }  ?>
                                </p>
                            </div>
                        </div>
                        <div id="success2<?php print $rs_list_similar->fields("pkid"); ?>"></div>
                        <div class="row padding-10">
                            <div class="col-md-12">
                                <div class="row padding-10">
                                    <?php /*?><div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))));?>" type="button" class="product-button"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a> 
                                </div><?php */ ?>
                                    <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                        <?php /*?><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))))); ?>" type="button" class="product-buynow-button"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a><?php */ ?>

                                        <form class='myform' id="frm_add<?php print $rs_list_similar->Fields("pkid"); ?>" name="frm_add<?php print $rs_list_similar->Fields("pkid"); ?>" onsubmit="return addToCart('frm_add<?php print $rs_list_similar->Fields("pkid"); ?>')" novalidate>
                                            <input type="hidden" name="hdn_prodpkid" id="hdn_prodpkid" value="<?php print $rs_list_similar->Fields("pkid"); ?>">
                                            <input type="hidden" name="hdn_prodtitle" id="hdn_prodtitle" value="<?php print $rs_list_similar->Fields("title"); ?>">
                                            <input type="hidden" id="hdn_itemcode" name="hdn_itemcode" value="<?php print $rs_list_similar->Fields("itemcode"); ?>" />
                                            <?php /* ?>
						<input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $int_cat_pkid; ?>">
						<input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $int_sub_cat_pkid; ?>">
						<?php */ ?>
                                            <input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $rs_list_similar->Fields("catpkid"); ?>">
                                            <input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $rs_list_similar->Fields("subcatpkid"); ?>">

                                            <input type="hidden" name="hdn_weight" id="hdn_weight" value="<?php print $rs_list_similar->Fields("weight"); ?>">
                                            <input type="hidden" name="hdn_userpkid" id="hdn_userpkid" value="<?php print $int_userpkid; ?>">
                                            <input type="hidden" name="hdn_username" id="hdn_username" value="<?php print $str_username; ?>">
                                            <input type="hidden" name="hdn_discount" id="hdn_discount" value="<?php print $int_wholesaler_discount; ?>">
                                            <input type="hidden" name="hdn_referredbypkid" id="hdn_referredbypkid" value="<?php print $int_referredbypkid; ?>">
                                            <input type="hidden" name="hdn_commission" id="hdn_commission" value="<?php print $int_commission; ?>">
                                            <input type="hidden" name="rdo_color" id="rdo_color" value="<?php print $rs_list_similar->Fields("colortitle"); ?>">
                                            <input type="hidden" name="rdo_size" id="rdo_size" value="<?php print $rs_list_similar->Fields("sizetitle"); ?>">
                                            <input type="hidden" name="rdo_tailoringoption" id="rdo_tailoringoption" value="<?php print $int_tailoring_service_pkid; ?>">
                                            <input type="hidden" name="cbo_semistitched_stitchingsize" id="cbo_semistitched_stitchingsize" value="0">
                                            <input type="hidden" name="txt_blouse_length" id="txt_blouse_length" value="0">
                                            <input type="hidden" name="txt_blouse_sleeve_length" id="txt_blouse_sleeve_length" value="0">
                                            <input type="hidden" name="cbo_blouse_aroundbust" id="cbo_blouse_aroundbust" value="0">
                                            <input type="hidden" name="txt_blouse_abovearoundwaist" id="txt_blouse_abovearoundwaist" value="0">
                                            <input type="hidden" name="txt_blouse_frontneckdepth" id="txt_blouse_frontneckdepth" value="0">
                                            <input type="hidden" name="txt_blouse_backneckdepth" id="txt_blouse_backneckdepth" value="0">
                                            <input type="hidden" name="txt_lehenga_aroundwaist" id="txt_lehenga_aroundwaist" value="0">
                                            <input type="hidden" name="txt_lehenga_aroundhips" id="txt_lehenga_aroundhips" value="0">
                                            <input type="hidden" name="txt_lehenga_length" id="txt_lehenga_length" value="0">
                                            <input type="hidden" name="txt_salwar_yourheight" id="txt_salwar_yourheight" value="0">
                                            <input type="hidden" name="txt_salwar_frontneckdepth" id="txt_salwar_frontneckdepth" value="0">
                                            <input type="hidden" name="txt_salwar_bottomlength" id="txt_salwar_bottomlength" value="0">
                                            <input type="hidden" name="txt_salwar_kameezlength" id="txt_salwar_kameezlength" value="0">
                                            <input type="hidden" name="txt_salwar_sleevelength" id="txt_salwar_sleevelength" value="0">
                                            <input type="hidden" name="cbo_salwar_sleevestyle" id="cbo_salwar_sleevestyle" value="0">
                                            <input type="hidden" name="cbo_salwar_aroundbust" id="cbo_salwar_aroundbust" value="0">
                                            <input type="hidden" name="txt_salwar_abovearoundwaist" id="txt_salwar_abovearoundwaist" value="0">
                                            <input type="hidden" name="txt_salwar_aroundwaist" id="txt_salwar_aroundwaist" value="0">
                                            <input type="hidden" name="txt_salwar_aroundhips" id="txt_salwar_aroundhips" value="0">
                                            <input type="hidden" name="txt_salwar_backneckdepth" id="txt_salwar_backneckdepth" value="0">
                                            <input type="hidden" name="txt_salwar_aroundthigh" id="txt_salwar_aroundthigh" value="0">
                                            <input type="hidden" name="txt_salwar_aroundknee" id="txt_salwar_aroundknee" value="0">
                                            <input type="hidden" name="txt_salwar_aroundcalf" id="txt_salwar_aroundcalf" value="0">
                                            <input type="hidden" name="txt_salwar_aroundankle" id="txt_salwar_aroundankle" value="0">

                                            <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $int_hdn_price;  ?>" />
                                            <input type="hidden" id="txt_quantity" name="txt_quantity" value="1" />
                                            <input type="hidden" id="hdn_description" name="hdn_description" value="" />
                                            <input type="hidden" id="rdo_shipping" name="rdo_shipping" value="" />
                                            <?php
                                            $skuval = date("YmdHis");
                                            $skuval = GetEncryptId($rs_list_similar->Fields("pkid"));
                                            ?>
                                            <input type="hidden" id="specssku" name="specssku" value="<?php print $skuval;  ?>" />


                                            <button type="submit" class="product-buynow-button" name='btn_addtocart' id="btn_addtocart_<?php print $rs_list_similar->Fields("pkid"); ?>" style="border-bottom: 1px solid transparent; width: 100%;"><i class="fa fa-shopping-cart"></i></button>

                                        </form>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                        <?php
                                        ## --------------------------------------------- START : WISHLIST------------------------------------------
                                        if ($str_member_flag) {
                                            $str_query_select = "";
                                            $str_query_select = "SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=" . $_SESSION["userpkid"] . " AND itempkid=" . $rs_list_similar->fields("pkid");
                                            $rs_list_fav = GetRecordSet($str_query_select);

                                            $str_fav_class = "";
                                            $str_fav_flag = "";
                                            if ($rs_list_fav->Count() == 0) {
                                                $str_fav_class = "";
                                                $str_fav_flag = "FAV";
                                            } else if ($rs_list_fav->Count() > 0) {
                                                $str_fav_class = "text-primary";
                                                $str_fav_flag = "UNFAV";
                                            } ?>
                                            <form name="frm_user_favorite2<?php print $rs_list_similar->fields("pkid"); ?>" id="frm_user_favorite2<?php print $rs_list_similar->fields("pkid"); ?>" novalidate>

                                                <input type="hidden" name="userpkid" id="userpkid<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($_SESSION["userpkid"]); ?>">
                                                <input type="hidden" name="itempkid" id="itempkid<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($rs_list_similar->fields("pkid")) ?>">

                                                <input type="hidden" name="flgfav" id="flgfav<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($str_fav_flag); ?>">



                                                <?php //print $rs_list_fav->Count(); 
                                                ?>
                                                <button id="frm_submit2<?php print($rs_list_similar->fields("pkid")) ?>" name="frm_submit2<?php print($rs_list_similar->fields("pkid")) ?>" type="submit" style="display: none;"></button>
                                            </form>

                                            <a onclick="$('#frm_submit2<?php print($rs_list_similar->fields("pkid")) ?>').click();" class="product-button <?php print $str_fav_class; ?>"><i class="fa fa-heart"></i></a>
                                            <script>
                                                $(function() {
                                                    $("#frm_user_favorite2<?php print $rs_list_similar->fields("pkid"); ?> input").jqBootstrapValidation({
                                                        preventSubmit: true,
                                                        submitError: function($form, event, errors) {
                                                            // something to have when submit produces an error ?
                                                            // Not decided if I need it yet
                                                        },
                                                        submitSuccess: function($form, event) {
                                                            event.preventDefault(); // prevent default submit behaviour
                                                            // get values from FORM

                                                            var userpkid = $("input#userpkid<?php print($rs_list_similar->fields("pkid")) ?>").val();
                                                            //alert(userpkid);
                                                            var itempkid = $("input#itempkid<?php print($rs_list_similar->fields("pkid")) ?>").val();
                                                            var flgfav = $("input#flgfav<?php print($rs_list_similar->fields("pkid")) ?>").val();


                                                            $.ajax({

                                                                url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                                                type: "POST",
                                                                data: {

                                                                    userpkid: userpkid,
                                                                    itempkid: itempkid,
                                                                    flgfav: flgfav
                                                                },
                                                                cache: false,
                                                                success: function(data) {
                                                                    //alert(data);
                                                                    var $responseText = JSON.parse(data);
                                                                    if ($responseText.status == 'SUC')
                                                                    //                                        if(($responseText.status).equal("SUC"))
                                                                    {
                                                                        // Success message
                                                                        //                        alert(data);

                                                                        $('#success2<?php print $rs_list_similar->fields("pkid"); ?>').html("<div class='alert alert-success small'>");
                                                                        $('#success2<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                            .append("</button>");
                                                                        $('#success2<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                                        $('#success2<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').append('</div>');
                                                                        //setTimeout(function(){ location.reload() }, 2000);
                                                                        setTimeout(function() {
                                                                            location.reload();
                                                                        }, 5000);
                                                                    } else if ($responseText.status == 'ERR') {
                                                                        // Fail message
                                                                        $('#success2<?php print $rs_list_similar->fields("pkid"); ?>').html("<div class='alert alert-danger'>");
                                                                        $('#success2<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                            .append("</button>");
                                                                        $('#success2<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                                        $('#success2<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').append('</div>');

                                                                    }
                                                                },

                                                            })
                                                        },
                                                        filter: function() {
                                                            return $(this).is(":visible");
                                                        },
                                                    });

                                                    $("a[data-toggle=\"tab\"]").click(function(e) {
                                                        e.preventDefault();
                                                        $(this).tab("show");
                                                    });
                                                });
                                                /*When clicking on Full hide fail/success boxes */
                                                $('#name').focus(function() {
                                                    $('#success').html('');
                                                });
                                            </script>
                                        <?php } else { ?>
                                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>" class="product-button"><i class="fa fa-heart"></i></a>
                                        <?php }
                                        ## --------------------------------------------- END : WISHLIST------------------------------------------ 
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
                $int_cnt++;
                if ($int_cnt % 3 == 0) {
                    print "</div><div class='row padding-10 hidden-md hidden-lg hidden-xs'>";
                }
                $rs_list_similar->MoveNext();
            }
            $rs_list_similar->MoveFirst();
            ?>
        </div>
        <div class="row padding-10 hidden-sm hidden-md hidden-lg">
            <?php
            $int_cnt = 0;
            while (!$rs_list_similar->EOF()) {
            ?>
                <div class="col-xs-6">
                    <div class="product-box text-center">
                        <div class="row padding-10">
                            <div class="col-md-12 nopadding" align="center">
                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))))); ?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>">
                                    <?php if ($rs_list_similar->Fields("thumbphotofilename") != "") { ?>
                                        <img alt="<?php print $rs_list_similar->Fields("title"); ?>" title="<?php print $rs_list_similar->Fields("title"); ?>" data-src="" src="<?php print $UPLOAD_IMG_PATH . $rs_list_similar->Fields("pkid") . "/" . $rs_list_similar->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                    <?php } else if ($rs_list_similar->Fields("imageurl") != "") { ?>
                                        <img alt="<?php print $rs_list_similar->Fields("title"); ?>" title="<?php print $rs_list_similar->Fields("title"); ?>" data-src="" src="<?php print $rs_list_similar->Fields("imageurl"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                    <?php } ?>
                                </a>

                                <?php
                                $str_query_select = "";
                                $str_query_select = "SELECT mainphotofilename FROM " . $STR_DB_TABLE_NAME_PHOTO . " WHERE masterpkid=" . $rs_list_similar->Fields("pkid") . " AND setasofferimage='LEFT' AND visible='YES' ORDER BY setasofferimage ASC";
                                $rs_list_offerimages_left = GetRecordSet($str_query_select);
                                //print $rs_list_offerimages->Count();
                                if ($rs_list_offerimages_left->Count() > 0) {
                                ?>

                                    <img src="<?php print $UPLOAD_IMG_PATH . $rs_list_similar->Fields("pkid") . "/" . $rs_list_offerimages_left->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-left" />
                                <?php }  ?>
                                <?php
                                $str_query_select = "";
                                $str_query_select = "SELECT mainphotofilename FROM " . $STR_DB_TABLE_NAME_PHOTO . " WHERE masterpkid=" . $rs_list_similar->Fields("pkid") . " AND setasofferimage='RIGHT' AND visible='YES' ORDER BY setasofferimage ASC";
                                $rs_list_offerimages_right = GetRecordSet($str_query_select);
                                //print $rs_list_offerimages->Count();
                                if ($rs_list_offerimages_right->Count() > 0) {
                                ?>
                                    <img src="<?php print $UPLOAD_IMG_PATH . $rs_list_similar->Fields("pkid") . "/" . $rs_list_offerimages_right->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-right" />
                                <?php }  ?>

                            </div>
                        </div>
                        <div class="row padding-10">
                            <div class="col-md-12 product-content-bg">
                                <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))))); ?>" title="<?php print $rs_list_similar->Fields("title"); //$str_hover_view_details; 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ?>"><b class="text-muted"><?php print MakeStringShort($rs_list_similar->Fields("title"), 18); ?></b></a></p>
                                <p>
                                    <?php if ($rs_list->Fields("displayasnew") == 'YES') {
                                        print $STR_ICON_PATH_NEW;
                                    } ?><?php if ($rs_list->Fields("displayasfeatured") == 'YES') {
                                            print "&nbsp;" . $STR_ICON_PATH_FEATURED;
                                        } ?><?php if ($rs_list->Fields("displayashot") == 'YES') {
                                                print "&nbsp;" . $STR_ICON_PATH_HOT;
                                            } ?> </p>
                                <?php /* ?><p>Product Code : 000000</p><?php */ ?>
                                <p>
                                    <?php if ($str_user_type == "WHOLESALER" && $rs_list_similar->Fields("memberprice") > 0) {
                                        $int_wholesaler_price = "";
                                        $int_wholesaler_price = $rs_list_similar->Fields("memberprice") / $int_conversionrate;
                                        $int_hdn_price = $int_wholesaler_price;
                                    ?>
                                        <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_wholesaler_price), 2) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                    <?php } else { ?>

                                        <?php
                                        if ($rs_list_similar->Fields("ourprice") > 0 && ($rs_list_similar->Fields("ourprice") != $rs_list_similar->Fields("listprice"))) {
                                            $int_list_price = "";
                                            $int_list_price = $rs_list_similar->Fields("listprice") / $int_conversionrate;

                                            $int_our_price = "";
                                            $int_our_price = $rs_list_similar->Fields("ourprice") / $int_conversionrate;
                                            $int_hdn_price = $int_our_price;
                                        ?>
                                            <strike class='text-muted'><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 2) . "&nbsp;" . $str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                            <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_our_price), 2) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                            <?php
                                            $int_per_discount = 0;
                                            $int_per_discount = (100 - ($rs_list_similar->Fields("ourprice") / $rs_list_similar->Fields("listprice")) * 100);
                                            if ($int_per_discount > 0) { ?>
                                                <label class="label label-primary"><b><?php print number_format($int_per_discount, 2); ?>% OFF</b></label>
                                            <?php } ?>
                                        <?php } else {
                                            $int_list_price = "";
                                            $int_list_price = $rs_list_similar->Fields("listprice") / $int_conversionrate;
                                            $int_hdn_price = $int_list_price;
                                        ?>
                                            <b class="text-success"><?php print $str_currency_symbol . "&nbsp;" . number_format(ceil($int_list_price), 2) . "&nbsp;" . $str_currency_shortform; ?></b>&nbsp;
                                        <?php }  ?>
                                    <?php }  ?>
                                </p>
                            </div>
                        </div>
                        <div id="success3<?php print $rs_list_similar->fields("pkid"); ?>"></div>
                        <div class="row padding-10">
                            <div class="col-md-12">
                                <div class="row padding-10">
                                    <?php /*?><div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))));?>" type="button" class="product-button"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a> 
                                </div><?php */ ?>
                                    <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                        <?php /*?><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))))); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))))); ?>" type="button" class="product-buynow-button"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a><?php */ ?>

                                        <form class='myform' id="frm_add<?php print $rs_list_similar->Fields("pkid"); ?>" name="frm_add<?php print $rs_list_similar->Fields("pkid"); ?>" onsubmit="return addToCart('frm_add<?php print $rs_list_similar->Fields("pkid"); ?>')" novalidate>
                                            <input type="hidden" name="hdn_prodpkid" id="hdn_prodpkid" value="<?php print $rs_list_similar->Fields("pkid"); ?>">
                                            <input type="hidden" name="hdn_prodtitle" id="hdn_prodtitle" value="<?php print $rs_list_similar->Fields("title"); ?>">
                                            <input type="hidden" id="hdn_itemcode" name="hdn_itemcode" value="<?php print $rs_list_similar->Fields("itemcode"); ?>" />
                                            <?php /* ?>
						<input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $int_cat_pkid; ?>">
						<input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $int_sub_cat_pkid; ?>">
						<?php */ ?>
                                            <input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print $rs_list_similar->Fields("catpkid"); ?>">
                                            <input type="hidden" name="hdn_subcatpkid" id="hdn_subcatpkid" value="<?php print $rs_list_similar->Fields("subcatpkid"); ?>">

                                            <input type="hidden" name="hdn_weight" id="hdn_weight" value="<?php print $rs_list_similar->Fields("weight"); ?>">
                                            <input type="hidden" name="hdn_userpkid" id="hdn_userpkid" value="<?php print $int_userpkid; ?>">
                                            <input type="hidden" name="hdn_username" id="hdn_username" value="<?php print $str_username; ?>">
                                            <input type="hidden" name="hdn_discount" id="hdn_discount" value="<?php print $int_wholesaler_discount; ?>">
                                            <input type="hidden" name="hdn_referredbypkid" id="hdn_referredbypkid" value="<?php print $int_referredbypkid; ?>">
                                            <input type="hidden" name="hdn_commission" id="hdn_commission" value="<?php print $int_commission; ?>">
                                            <input type="hidden" name="rdo_color" id="rdo_color" value="<?php print $rs_list_similar->Fields("colortitle"); ?>">
                                            <input type="hidden" name="rdo_size" id="rdo_size" value="<?php print $rs_list_similar->Fields("sizetitle"); ?>">
                                            <input type="hidden" name="rdo_tailoringoption" id="rdo_tailoringoption" value="<?php print $int_tailoring_service_pkid; ?>">
                                            <input type="hidden" name="cbo_semistitched_stitchingsize" id="cbo_semistitched_stitchingsize" value="0">
                                            <input type="hidden" name="txt_blouse_length" id="txt_blouse_length" value="0">
                                            <input type="hidden" name="txt_blouse_sleeve_length" id="txt_blouse_sleeve_length" value="0">
                                            <input type="hidden" name="cbo_blouse_aroundbust" id="cbo_blouse_aroundbust" value="0">
                                            <input type="hidden" name="txt_blouse_abovearoundwaist" id="txt_blouse_abovearoundwaist" value="0">
                                            <input type="hidden" name="txt_blouse_frontneckdepth" id="txt_blouse_frontneckdepth" value="0">
                                            <input type="hidden" name="txt_blouse_backneckdepth" id="txt_blouse_backneckdepth" value="0">
                                            <input type="hidden" name="txt_lehenga_aroundwaist" id="txt_lehenga_aroundwaist" value="0">
                                            <input type="hidden" name="txt_lehenga_aroundhips" id="txt_lehenga_aroundhips" value="0">
                                            <input type="hidden" name="txt_lehenga_length" id="txt_lehenga_length" value="0">
                                            <input type="hidden" name="txt_salwar_yourheight" id="txt_salwar_yourheight" value="0">
                                            <input type="hidden" name="txt_salwar_frontneckdepth" id="txt_salwar_frontneckdepth" value="0">
                                            <input type="hidden" name="txt_salwar_bottomlength" id="txt_salwar_bottomlength" value="0">
                                            <input type="hidden" name="txt_salwar_kameezlength" id="txt_salwar_kameezlength" value="0">
                                            <input type="hidden" name="txt_salwar_sleevelength" id="txt_salwar_sleevelength" value="0">
                                            <input type="hidden" name="cbo_salwar_sleevestyle" id="cbo_salwar_sleevestyle" value="0">
                                            <input type="hidden" name="cbo_salwar_aroundbust" id="cbo_salwar_aroundbust" value="0">
                                            <input type="hidden" name="txt_salwar_abovearoundwaist" id="txt_salwar_abovearoundwaist" value="0">
                                            <input type="hidden" name="txt_salwar_aroundwaist" id="txt_salwar_aroundwaist" value="0">
                                            <input type="hidden" name="txt_salwar_aroundhips" id="txt_salwar_aroundhips" value="0">
                                            <input type="hidden" name="txt_salwar_backneckdepth" id="txt_salwar_backneckdepth" value="0">
                                            <input type="hidden" name="txt_salwar_aroundthigh" id="txt_salwar_aroundthigh" value="0">
                                            <input type="hidden" name="txt_salwar_aroundknee" id="txt_salwar_aroundknee" value="0">
                                            <input type="hidden" name="txt_salwar_aroundcalf" id="txt_salwar_aroundcalf" value="0">
                                            <input type="hidden" name="txt_salwar_aroundankle" id="txt_salwar_aroundankle" value="0">

                                            <input type="hidden" id="hdn_price" name="hdn_price" value="<?php print $int_hdn_price;  ?>" />
                                            <input type="hidden" id="txt_quantity" name="txt_quantity" value="1" />
                                            <input type="hidden" id="hdn_description" name="hdn_description" value="" />
                                            <input type="hidden" id="rdo_shipping" name="rdo_shipping" value="" />
                                            <?php
                                            $skuval = date("YmdHis");
                                            $skuval = GetEncryptId($rs_list_similar->Fields("pkid"));
                                            ?>
                                            <input type="hidden" id="specssku" name="specssku" value="<?php print $skuval;  ?>" />


                                            <button type="submit" class="product-buynow-button" name='btn_addtocart' id="btn_addtocart_<?php print $rs_list_similar->Fields("pkid"); ?>" style="border-bottom: 1px solid transparent; width: 100%;"><i class="fa fa-shopping-cart"></i></button>

                                        </form>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                        <?php
                                        ## --------------------------------------------- START : WISHLIST------------------------------------------
                                        if ($str_member_flag) {
                                            $str_query_select = "";
                                            $str_query_select = "SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=" . $_SESSION["userpkid"] . " AND itempkid=" . $rs_list_similar->fields("pkid");
                                            $rs_list_fav = GetRecordSet($str_query_select);

                                            $str_fav_class = "";
                                            $str_fav_flag = "";
                                            if ($rs_list_fav->Count() == 0) {
                                                $str_fav_class = "";
                                                $str_fav_flag = "FAV";
                                            } else if ($rs_list_fav->Count() > 0) {
                                                $str_fav_class = "text-primary";
                                                $str_fav_flag = "UNFAV";
                                            } ?>
                                            <form name="frm_user_favorite3<?php print $rs_list_similar->fields("pkid"); ?>" id="frm_user_favorite3<?php print $rs_list_similar->fields("pkid"); ?>" novalidate>

                                                <input type="hidden" name="userpkid" id="userpkid<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($_SESSION["userpkid"]); ?>">
                                                <input type="hidden" name="itempkid" id="itempkid<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($rs_list_similar->fields("pkid")) ?>">

                                                <input type="hidden" name="flgfav" id="flgfav<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($str_fav_flag); ?>">



                                                <?php //print $rs_list_fav->Count(); 
                                                ?>
                                                <button id="frm_submit3<?php print($rs_list_similar->fields("pkid")) ?>" name="frm_submit3<?php print($rs_list_similar->fields("pkid")) ?>" type="submit" style="display: none;"></button>
                                            </form>

                                            <a onclick="$('#frm_submit3<?php print($rs_list_similar->fields("pkid")) ?>').click();" class="product-button <?php print $str_fav_class; ?>"><i class="fa fa-heart"></i></a>
                                            <script>
                                                $(function() {
                                                    $("#frm_user_favorite3<?php print $rs_list_similar->fields("pkid"); ?> input").jqBootstrapValidation({
                                                        preventSubmit: true,
                                                        submitError: function($form, event, errors) {
                                                            // something to have when submit produces an error ?
                                                            // Not decided if I need it yet
                                                        },
                                                        submitSuccess: function($form, event) {
                                                            event.preventDefault(); // prevent default submit behaviour
                                                            // get values from FORM

                                                            var userpkid = $("input#userpkid<?php print($rs_list_similar->fields("pkid")) ?>").val();
                                                            //alert(userpkid);
                                                            var itempkid = $("input#itempkid<?php print($rs_list_similar->fields("pkid")) ?>").val();
                                                            var flgfav = $("input#flgfav<?php print($rs_list_similar->fields("pkid")) ?>").val();


                                                            $.ajax({

                                                                url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                                                type: "POST",
                                                                data: {

                                                                    userpkid: userpkid,
                                                                    itempkid: itempkid,
                                                                    flgfav: flgfav
                                                                },
                                                                cache: false,
                                                                success: function(data) {
                                                                    //alert(data);
                                                                    var $responseText = JSON.parse(data);
                                                                    if ($responseText.status == 'SUC')
                                                                    //                                        if(($responseText.status).equal("SUC"))
                                                                    {
                                                                        // Success message
                                                                        //                        alert(data);

                                                                        $('#success3<?php print $rs_list_similar->fields("pkid"); ?>').html("<div class='alert alert-success small'>");
                                                                        $('#success3<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                            .append("</button>");
                                                                        $('#success3<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                                        $('#success3<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').append('</div>');
                                                                        //setTimeout(function(){ location.reload() }, 2000);
                                                                        setTimeout(function() {
                                                                            location.reload();
                                                                        }, 5000);
                                                                    } else if ($responseText.status == 'ERR') {
                                                                        // Fail message
                                                                        $('#success3<?php print $rs_list_similar->fields("pkid"); ?>').html("<div class='alert alert-danger'>");
                                                                        $('#success3<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                            .append("</button>");
                                                                        $('#success3<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                                        $('#success3<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').append('</div>');

                                                                    }
                                                                },

                                                            })
                                                        },
                                                        filter: function() {
                                                            return $(this).is(":visible");
                                                        },
                                                    });

                                                    $("a[data-toggle=\"tab\"]").click(function(e) {
                                                        e.preventDefault();
                                                        $(this).tab("show");
                                                    });
                                                });
                                                /*When clicking on Full hide fail/success boxes */
                                                $('#name').focus(function() {
                                                    $('#success').html('');
                                                });
                                            </script>
                                        <?php } else { ?>
                                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>" class="product-button"><i class="fa fa-heart"></i></a>
                                        <?php }
                                        ## --------------------------------------------- END : WISHLIST------------------------------------------ 
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
                $int_cnt++;
                if ($int_cnt % 2 == 0) {
                    print "</div><div class='row padding-10 hidden-sm hidden-md hidden-lg'>";
                }
                $rs_list_similar->MoveNext();
            }
            $rs_list_similar->MoveFirst();
            ?>
        </div>
    <?php } ?><br />
    </div>

















    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/glass.js"></script>
    <div class="footer-bottom-margin">
        <?php include($STR_USER_FOOTER_PATH);
        CloseConnection(); ?>
    </div>
    <div class="container-fluid">
        <div class="row padding-10">
            <!--            <div class="col-md-12 text-center" align="center">
                <div class="price-and-checkout-btn-panel hidden-lg hidden-md">
                    <div class="btn-group" role="group" aria-label="...">
                        <button class="btn btn-primary"><i class="fa fa-cart-plus"></i>&nbsp;ADD TO CART</button>
                        <button class="btn btn-warning"><i class="fa fa-bolt"></i>&nbsp;BUY NOW</button>
                        <button class="btn btn-success"><i class="fa fa-whatsapp"></i></button>
                    </div>
                    
                </div>
            </div>-->

            <div class="price-and-checkout-btn-panel hidden-lg hidden-md">

                <div class="col-xs-4 text-center nopadding">
                    <a onclick="$('#btn_addtocart').click();">
                        <div class="add-to-cart-btn">
                            <i class="fa fa-cart-plus"></i>&nbsp;Add to Cart
                        </div>
                    </a>
                </div>
                <div class="col-xs-6 text-center nopadding">
                    <a onclick="$('#btn_buy_now').click();">
                        <div class="buy-now-btn">
                            <i class="fa fa-shopping-cart"></i>&nbsp;Buy Now at <?php print $str_currency_symbol ?>&nbsp;<span id="total_price2"></span>
                        </div>
                    </a>
                </div>
                <div class="col-xs-2 text-center nopadding">
                    <a href="https://wa.me/<?php print $str_whatsapp_number; ?>?text=<?php print "I am interested in buying this product " . $rs_list->fields("itemcode"); ?>">
                        <div class="whatsapp-btn">
                            <i class="fa fa-whatsapp"></i>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_details.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_details2.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_details_cod.js"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function() {
            $('#glasscase').glassCase({
                'thumbsPosition': 'bottom',
                'widthDisplay': 410
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            //alert($('#txt_quantity').val());

            //alert(tailoring_price);
            var temp = parseFloat(tailoring_price);

            var selected_tailoring_price = $("input[name='rdo_tailoringoption']:checked").val();
            var quantity = $('#txt_quantity').val();

            //alert(selected_tailoring_price);

            //alert(temp);
            document.getElementById('list_price').innerHTML = parseFloat(<?php
                                                                            $int_listprice_display = "";
                                                                            $int_listprice_display = $rs_list->Fields("listprice") / $int_conversionrate;

                                                                            $int_ourprice_display = "";
                                                                            $int_ourprice_display = $rs_list->Fields("ourprice") / $int_conversionrate;

                                                                            $int_memberprice_display = "";
                                                                            $int_memberprice_display = $rs_list->Fields("memberprice") / $int_conversionrate;

                                                                            if ($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) {
                                                                                print ceil($int_memberprice_display);
                                                                            } else if ($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                                                                print ceil($int_ourprice_display);
                                                                            } else {
                                                                                print ceil($int_listprice_display);
                                                                            } ?>).toFixed(2);
            document.getElementById('total_price').innerHTML = (parseFloat(<?php if ($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) {
                                                                                print ceil($int_memberprice_display);
                                                                            } else if ($rs_list->Fields('ourprice') > 0) {
                                                                                print ceil($int_ourprice_display);
                                                                            } else {
                                                                                print ceil($int_listprice_display);
                                                                            } ?>).toFixed(2))
            document.getElementById('total_price2').innerHTML = (parseFloat(<?php if ($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) {
                                                                                print ceil($int_memberprice_display);
                                                                            } else if ($rs_list->Fields('ourprice') > 0) {
                                                                                print ceil($int_ourprice_display);
                                                                            } else {
                                                                                print ceil($int_listprice_display);
                                                                            } ?>).toFixed(2))

            if (selected_tailoring_price == 1) {
                $("#rd_option3").hide();
                $("#rd_option2").hide();
                var temp = document.getElementById("tailoring_opt1").innerHTML;
                //alert(temp);
                document.getElementById('tailoring_price').innerHTML = "" + parseFloat(temp * quantity).toFixed(2);
                document.getElementById('hdn_tailoring_price').innerHTML = "" + parseFloat(temp).toFixed(2)

                var total_final_price = 0;
                var total_price = 0;



                //alert(tailoring_price);
                total_price = parseFloat(document.getElementById('total_price').innerHTML).toFixed(2);
                //alert(total_price);
                tailoring_price = parseFloat(document.getElementById('tailoring_price').innerHTML).toFixed(2);
                total_final_price = parseFloat(tailoring_price) + (<?php if ($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) {
                                                                        print ceil($int_memberprice_display);
                                                                    } else if ($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                                                        print ceil($int_ourprice_display);
                                                                    } else {
                                                                        print ceil($int_listprice_display);
                                                                    } ?> * quantity);
                document.getElementById('total_price').innerHTML = total_final_price.toFixed(2);
                document.getElementById('total_price2').innerHTML = total_final_price.toFixed(2);
                //alert(total_price);

            } else if (selected_tailoring_price == 2) {
                $("#rd_option3").hide();
                $("#rd_option2").show();
                var temp = document.getElementById("tailoring_opt2").innerHTML;
                //alert(temp);
                document.getElementById('tailoring_price').innerHTML = "" + parseFloat(temp * quantity).toFixed(2);
                document.getElementById('hdn_tailoring_price').innerHTML = "" + parseFloat(temp).toFixed(2)

                var total_final_price = 0;
                var total_price = 0;



                //alert(tailoring_price);
                total_price = parseFloat(document.getElementById('total_price').innerHTML).toFixed(2);
                //alert(total_price);
                tailoring_price = parseFloat(document.getElementById('tailoring_price').innerHTML).toFixed(2);
                total_final_price = parseFloat(tailoring_price) + (<?php if ($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) {
                                                                        print ceil($int_memberprice_display);
                                                                    } else if ($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                                                        print ceil($int_ourprice_display);
                                                                    } else {
                                                                        print ceil($int_listprice_display);
                                                                    } ?> * quantity);
                document.getElementById('total_price').innerHTML = total_final_price.toFixed(2);
                document.getElementById('total_price2').innerHTML = total_final_price.toFixed(2);
                //alert(total_price);

            } else if (selected_tailoring_price == 3) {
                $("#rd_option3").show();
                $("#rd_option2").hide();
                var temp = document.getElementById("tailoring_opt3").innerHTML;
                //alert(temp);
                document.getElementById('tailoring_price').innerHTML = "" + parseFloat(temp * quantity).toFixed(2);
                document.getElementById('hdn_tailoring_price').innerHTML = "" + parseFloat(temp).toFixed(2)

                var total_final_price = 0;
                var total_price = 0;



                //alert(tailoring_price);
                total_price = parseFloat(document.getElementById('total_price').innerHTML).toFixed(2);
                //alert(total_price);
                tailoring_price = parseFloat(document.getElementById('tailoring_price').innerHTML).toFixed(2);
                total_final_price = parseFloat(tailoring_price) + (<?php if ($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) {
                                                                        print ceil($int_memberprice_display);
                                                                    } else if ($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                                                        print ceil($int_ourprice_display);
                                                                    } else {
                                                                        print ceil($int_listprice_display);
                                                                    } ?> * quantity);
                document.getElementById('total_price').innerHTML = total_final_price.toFixed(2);
                document.getElementById('total_price2').innerHTML = total_final_price.toFixed(2);
                //alert(total_price);

            }
        });

        function QtyIncrement() {
            var quantity = $('#txt_quantity').val();
            var remainingqty = parseInt(quantity) + 1;
            //alert(remainingqty);
            var txt_qty = document.getElementById('txt_quantity');
            txt_qty.value = remainingqty;


            var list_price = parseFloat(<?php

                                        $int_listprice_display = "";
                                        $int_listprice_display = $rs_list->Fields("listprice") / $int_conversionrate;

                                        $int_ourprice_display = "";
                                        $int_ourprice_display = $rs_list->Fields("ourprice") / $int_conversionrate;


                                        if ($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) {
                                            print ceil($int_memberprice_display);
                                        } else if ($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                            print ceil($int_ourprice_display);
                                        } else {
                                            print ceil($int_listprice_display);
                                        } ?>).toFixed(2) * txt_qty.value;
            //alert(parseFloat(list_price).toFixed(2));
            document.getElementById('list_price').innerHTML = parseFloat(list_price).toFixed(2)
            //var our_price = <?php //print $rs_list->Fields('ourprice'); 
                                ?> * txt_qty.value;
            //document.getElementById('our_price').innerHTML = parseFloat(our_price).toFixed(2)
            //alert(list_price);



            var tailoring_price = parseFloat(document.getElementById('hdn_tailoring_price').innerHTML).toFixed(2);



            var increased_tailoring_price = tailoring_price * txt_qty.value;
            document.getElementById('tailoring_price').innerHTML = parseFloat(increased_tailoring_price).toFixed(2);

            document.getElementById('total_price').innerHTML = parseFloat(parseFloat(list_price) + parseFloat(increased_tailoring_price)).toFixed(2);
            document.getElementById('total_price2').innerHTML = parseFloat(parseFloat(list_price) + parseFloat(increased_tailoring_price)).toFixed(2);
        }

        function QtyDecrement() {
            //alert($('#txt_quantity').val());
            var quantity = $('#txt_quantity').val();
            var remainingqty = parseInt(quantity) - 1;
            //alert(remainingqty);
            var txt_qty = document.getElementById('txt_quantity');
            if (remainingqty == 0) {
                txt_qty.value = 1;
            } else {
                txt_qty.value = remainingqty;
            }

            var list_price = <?php if ($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) {
                                    print ceil($int_memberprice_display);
                                } else if ($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                    print ceil($int_ourprice_display);
                                } else {
                                    print ceil($int_listprice_display);
                                } ?> * txt_qty.value;
            //alert(parseFloat(list_price).toFixed(2));
            document.getElementById('list_price').innerHTML = "" + parseFloat(list_price).toFixed(2)

            document.getElementById('total_price').innerHTML = "" + parseFloat(list_price).toFixed(2);

            var tailoring_price = parseFloat(document.getElementById('hdn_tailoring_price').innerHTML).toFixed(2);



            var increased_tailoring_price = tailoring_price * txt_qty.value;
            document.getElementById('tailoring_price').innerHTML = parseFloat(increased_tailoring_price).toFixed(2);

            document.getElementById('total_price').innerHTML = parseFloat(parseFloat(list_price) + parseFloat(increased_tailoring_price)).toFixed(2);
            document.getElementById('total_price2').innerHTML = parseFloat(parseFloat(list_price) + parseFloat(increased_tailoring_price)).toFixed(2);
        }
    </script>
    <script type="text/javascript">
        function ShowHideDiv() {
            var rdooption = document.getElementById("rdo_tailoringoption");
            var div2 = document.getElementById("rdo_option2");
            var div3 = document.getElementById("rdo_option3");
            if (rdooption == 3) {
                div3.style.display = rdooption.checked ? "block" : "none";
            }
            //alert(rdooption.value);
            //dvPassport.style.display = chkYes.checked ? "block" : "none";
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('input[type="radio"]').click(function() {
                if ($(this).attr("name") == "rdo_tailoringoption") {
                    var val = $(this).val();
                    //alert(val);
                    var quantity = $('#txt_quantity').val();
                    //alert(quantity);

                    //var rd_optionTWO = document.getElementById("rd_option2");
                    if (val == 2) {
                        //alert("ho");
                        $("#rd_option2").show();
                        $("#rd_option3").hide();
                        var temp = document.getElementById("tailoring_opt2").innerHTML;

                        document.getElementById('tailoring_price').innerHTML = "" + parseFloat(temp * quantity).toFixed(2);

                        document.getElementById('hdn_tailoring_price').innerHTML = "" + parseFloat(temp).toFixed(2)


                        //rd_optionTWO.style.display =  $(this).checked ? "block" : "none";

                    } else if (val == 3) {
                        $("#rd_option3").show();
                        $("#rd_option2").hide();
                        var temp = document.getElementById("tailoring_opt3").innerHTML;
                        //alert(temp);
                        document.getElementById('tailoring_price').innerHTML = "" + parseFloat(temp * quantity).toFixed(2);
                        document.getElementById('hdn_tailoring_price').innerHTML = "" + parseFloat(temp).toFixed(2);
                        //alert(temp);
                    } else {
                        $("#rd_option3").hide();
                        $("#rd_option2").hide();
                        var temp = document.getElementById("tailoring_opt1").innerHTML;
                        document.getElementById('tailoring_price').innerHTML = "" + parseFloat(temp * quantity).toFixed(2);
                        document.getElementById('hdn_tailoring_price').innerHTML = "" + parseFloat(temp).toFixed(2)
                        //alert(temp);
                    }
                    //document.getElementById("output").innerHTML = val;
                    //document.getElementById("output1").innerHTML = val;

                    //document.getElementById("rawclr").addClass("alert alert-warning");
                    //temp=document.getElementById("temp");
                    //temp.className="alert alert-warning";
                    /*$('input[type="radio"]').click(function() {
                    //alert("hi");
                            var selected = $(this).hasClass("alert alert-warning");
                            $("#data tr").removeClass("alert alert-warning");
                            if(!selected)
                                            $("#data tr").addClass("alert alert-warning");
                    });*/
                }
            });

            $('input[type="radio"]').change(function() {
                if ($(this).attr("name") == "rdo_tailoringoption") {
                    var total_final_price = 0;
                    var tailoring_price = 0;
                    var total_price = 0;
                    var quantity = $('#txt_quantity').val();

                    tailoring_price = parseFloat(document.getElementById('tailoring_price').innerHTML).toFixed(2);
                    //alert(tailoring_price);
                    total_price = parseFloat(document.getElementById('total_price').innerHTML).toFixed(2);
                    total_final_price = parseFloat(tailoring_price) + (<?php if ($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) {
                                                                            print ceil($int_memberprice_display);
                                                                        } else if ($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                                                            print ceil($int_ourprice_display);
                                                                        } else {
                                                                            print ceil($int_listprice_display);
                                                                        } ?> * quantity);
                    document.getElementById('total_price').innerHTML = total_final_price.toFixed(2);
                    document.getElementById('total_price2').innerHTML = total_final_price.toFixed(2);
                }
            });

        });
    </script>

    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/social-share-kit.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        // Init Social Share Kit
        SocialShareKit.init({
            url: 'http://darpani.com',
            twitter: {
                title: 'Darpani',
                via: 'darpani'
            },
            onBeforeOpen: function(targetElement, network, paramsObj) {
                console.log(arguments);
            },
            onOpen: function(targetElement, network, url, popupWindow) {
                console.log(arguments);
            },
            onClose: function(targetElement, network, url, popupWindow) {
                console.log(arguments);
            }
        });

        $(function() {

            // Just to disable href for other example icons
            $('.ssk').on('click', function(e) {
                e.preventDefault();
            });

            // Navigation collapse on click
            $('.navbar-collapse ul li a:not(.dropdown-toggle)').bind('click', function() {
                $('.navbar-toggle:visible').click();
            });

            // Email protection
            $('.author-email').each(function() {
                var a = '@',
                    em = 'support' + a + 'social' + 'sharekit' + '.com',
                    t = $(this);
                t.attr('href', 'mai' + 'lt' + 'o' + ':' + em);
                !t.text() && t.text(em);
            });

            // Sticky icons changer
            $('.sticky-changer').click(function(e) {
                e.preventDefault();
                var $link = $(this);
                $('.ssk-sticky').removeClass($link.parent().children().map(function() {
                    return $(this).data('cls');
                }).get().join(' ')).addClass($link.data('cls'));
                $link.parent().find('.active').removeClass('active');
                $link.addClass('active').blur();
            });
        });

        // This code is required if you want to use Twitter callbacks
        window.twttr = (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0],
                t = window.twttr || {};
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);

            t._e = [];
            t.ready = function(f) {
                t._e.push(f);
            };

            return t;
        }(document, "script", "twitter-wjs"));

        // Demo callback
        function twitterDemoCallback(e) {
            $('#twitterEvents').append(e.type + ' ');
        }

        // Bind to Twitter events
        twttr.ready(function(tw) {
            tw.events.bind('click', twitterDemoCallback);
            tw.events.bind('tweet', twitterDemoCallback);
        });
    </script>
    <script type="text/javascript">
        /*$(document).ready(function(){
            var maxLength = 300;
            $(".show-read-more").each(function(){
                    var myStr = $(this).text();
                    //alert($(this).text());
                    if($.trim(myStr).length > maxLength){
                            var newStr = myStr.substring(0, maxLength);
                            //alert(newStr);
                            var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
                            //alert(removedStr);
                            $(this).empty().html(newStr);
                            $(this).append(' <a href="javascript:void(0);" class="read-more">Read More...</a>');
                            $(this).append('<span class="more-text">' + removedStr + '</span>');
                    }
            });
            $(".read-more").click(function(){
                    $(this).siblings(".more-text").contents().unwrap();
                    $(this).remove();
            });
    });*/
        $(document).ready(function() {
            var showChar = 390;
            var ellipsestext = "...";
            var moretext = "Read More&nbsp;<i class='fa fa-caret-down'></i>";
            var lesstext = "Read Less&nbsp;<i class='fa fa-caret-up'></i>";
            $('.more').each(function() {
                var content = $(this).html();

                if (content.length > showChar) {

                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar - 1, content.length - showChar);

                    var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                    $(this).html(html);
                }

            });

            $(".morelink").click(function() {
                if ($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
                } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
                }
                $(this).parent().prev().toggle();
                $(this).prev().toggle();
                return false;
            });
        });
    </script>
    <script>
        $(function() {
            $(".expand").on("click", function() {
                $(this).next().slideToggle(200);
                $expand = $(this).find(">:first-child");

                if ($expand.text() == "+") {
                    $expand.text("-");
                } else {
                    $expand.text("+");
                }
            });
        });
    </script>
</body>

</html>