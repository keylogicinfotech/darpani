<?php 

error_reporting(E_ALL^(E_NOTICE|E_WARNING));
#Include files
include "../user/product_validatesession.php";
include "./../includes/configuration.php";
include "./../user/product_config.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";



if(!empty($_POST["id"])){

include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_title_page_metatag = "PG_STORE";
$str_img_path = $UPLOAD_IMG_PATH;
$str_img_path_measurement_chart = $UPLOAD_IMG_PATH_MEASUREMENT_CHART;
//print $str_img_path; 
$str_db_table_name_metatag = "t_page_metatag";
$str_xml_file_name = "";
$str_xml_file_name_cat = "";
$str_xml_file_name_cms = "";
$str_xml_file_name_sizechart = "sizechart.xml";
$str_xml_file_name_cms_sizechart = "sizechart_cms.xml";

$str_xml_file_name_measurechart = "measurechart.xml";
$str_xml_file_name_cms_measurechart = "measurechart_cms.xml";

$int_records_per_page = 10;

$str_button_view_details = "View This Product";
$str_hover_view_details = "Click To View This Product";
//print_r($_GET);exit;
#----------------------------------------------------------------------
##read main xml file for size chart
$str_list_xml_sizechart = "";
$str_list_xml_sizechart = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name_sizechart,"ROOT_ITEM");

$str_list_xml_measurechart = "";
$str_list_xml_measurechart = readXML($STR_XML_FILE_PATH_MODULE.$str_xml_file_name_measurechart,"ROOT_ITEM");

#Get values of all passed GET / POST variables
/*$int_cat_pkid = 0;
if(isset($_GET["cid"]) && trim($_GET["cid"]) != "" )
{   
    $int_cat_pkid = trim(GetDecryptId($_GET["cid"]));    
}*/

$int_pkid = 0;
if(isset($_GET["pid"]) && trim($_GET["pid"]) != "" )
{   
    $int_pkid = trim($_GET["pid"]);
    //$int_pkid = trim(GetDecryptId($_GET["pid"]));
}

# Select Query
$str_query_select = "";
$str_query_select = "SELECT a.*, b.subcattitle, c.thumbphotofilename, c.largephotofilename, d.title as cattitle, d.description2 AS tailoringservicedesc FROM " .$STR_DB_TABLE_NAME. " a ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON a.catpkid=d.catpkid AND d.visible='YES' ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." b ON a.subcatpkid=b.subcatpkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_PHOTO." c ON a.pkid=c.masterpkid WHERE a.pkid=".$int_pkid." AND b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' ";
//print $str_query_select;
$rs_list = GetRecordSet($str_query_select);


if($rs_list->EOF())
{
    CloseConnection();
    Redirect($STR_SITENAME_WITH_PROTOCOL); 
    exit();
}

#update noofview in master table
$str_query_update="UPDATE ".$STR_DB_TABLE_NAME." SET noofview=noofview+1 WHERE pkid=".$int_pkid ;
ExecuteQuery($str_query_update);

$str_title = "";
$str_title = $rs_list->Fields("title");

$int_sub_cat_pkid = 0;
$int_sub_cat_pkid = $rs_list->Fields("subcatpkid");

# Select Query For Similar Products
$str_query_select= "SELECT COUNT(*) as num_rows FROM ".$STR_DB_TABLE_NAME. " WHERE a.pkid >".$_POST['id'];
    $rs_list_similar = GetRecordSet($str_query_select);
    $totalRowCount = $rs_list_similar['num_rows'];
    
    $showLimit = 4;



$str_query_select = "";
$str_query_select = "SELECT a.*, d.title as cattitle, b.subcattitle, c.thumbphotofilename, c.largephotofilename, c.imageurl FROM " .$STR_DB_TABLE_NAME. " a ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON a.catpkid=d.catpkid AND d.visible='YES' ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." b ON a.subcatpkid=b.subcatpkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_PHOTO." c ON a.pkid=c.masterpkid ";
$str_query_select .= "WHERE b.visible='YES' AND c.setasfront='YES' AND c.visible='YES' AND a.pkid != ".$int_pkid." AND a.subcatpkid=".$int_sub_cat_pkid." AND a.pkid >".$_POST['id'].;
$str_query_select .= "ORDER BY a.displayorder DESC, a.createdatetime DESC, a.pkid LIMIT ".$showLimit;  
//print $str_query_select; exit;
$rs_list_similar = GetRecordSet($str_query_select);

//print $rs_list_similar->Count();exit;



#open cms xml file
$str_desc_cms_sizechart="";
$fp=OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms_sizechart);
$str_desc_cms_sizechart=getTagValue("ITEMKEYVALUE_DESCRIPTION",$fp);
$str_visible_cms_sizechart = getTagValue("ITEMKEYVALUE_VISIBLE",$fp);
CloseXmlFile($fp);

$str_desc_cms_measurechart="";
$fp=OpenXMLFile($STR_XML_FILE_PATH_CMS.$str_xml_file_name_cms_measurechart);
$str_desc_cms_measurechart=getTagValue("ITEMKEYVALUE_DESCRIPTION",$fp);
//print $str_desc_cms_measurechart; exit;
$str_visible_cms_measurechart = getTagValue("ITEMKEYVALUE_VISIBLE",$fp);
CloseXmlFile($fp);

#----------------------------------------------------------------------
#get metatag page title from table

$str_select_query = "";
$str_select_query = "SELECT titletag FROM " .$str_db_table_name_metatag. " WHERE visible='YES' AND pagekey='".$str_title_page_metatag."' ";
//print $str_select_query; exit;
$rs_list_similar_mt = GetRecordset($str_select_query);
$str_title_page = $rs_list_similar_mt->fields("titletag");
#----------------------------------------------------------------------
$str_member_flag = false;
$int_userpkid = 0;
$str_username = "";
$str_usertype = "";
if(isset($_SESSION['userpkid']) && $_SESSION['userpkid'] != "")
{
    $str_member_flag = true;
    
    $str_query_select = "";
    $str_query_select = "SELECT isusertype FROM ".$STR_DB_TABLE_NAME_USER." WHERE pkid=".$_SESSION["userpkid"];
    $rs_list_user = GetRecordSet($str_query_select);

    
    $int_userpkid = $_SESSION['userpkid'];
    $str_username = $_SESSION['username'];
    $str_usertype = $rs_list_user->Fields("isusertype");
}
//print $str_usertype; exit;

$int_referredbypkid = 0;
$int_commission = 0.00;
$int_wholesaler_discount = 0;
$str_user_type = "";
if($int_userpkid > 0)
{
    $str_query_select = "";
    $str_query_select = "SELECT referredbypkid, discount, isusertype FROM ".$STR_DB_TABLE_NAME_USER." WHERE pkid=".$int_userpkid;
    $rs_list_user = GetRecordSet($str_query_select);
    if(!$rs_list_user->EOF())
    {
        if(strtoupper($rs_list_user->Fields("isusertype")) == "WHOLESALER") 
        {
            $str_user_type = "WHOLESALER";
            $int_referredbypkid = $rs_list_user->Fields("referredbypkid");
            $int_wholesaler_discount = $rs_list_user->Fields("discount");
            if($int_referredbypkid > 0)
            {
                $str_query_select = "";
                $str_query_select = "SELECT discount, commission FROM ".$STR_DB_TABLE_NAME_USER." WHERE pkid=".$int_referredbypkid;
                $rs_list_commmision = GetRecordSet($str_query_select);

                $int_commission = $rs_list_commmision->Fields("commission");
                
            }
        }
    }
}
//print $int_wholesaler_discount;
//print_r($_SESSION);

$str_whatsapp_number = "917041626343";
#---------------------------------------------------------------------- 

?>
<?php

        # Similar Products
        if($rs_list_similar->Count() > 0) {
				?>

    <!--similar products container-->
        <!--row1--><div class="row padding-10 hidden-sm hidden-xs">
		<div class="postList">
            <?php 
            $int_cnt = 0;
            while(!$rs_list_similar->EOF()) {
				$postID=$rs_list_similar->Fields("pkid");	/*product id*/
            ?>
            <!--product 1--><div class="col-md-3 col-lg-3">  <!--md and lg screen size-->
                <div class="product-box text-center">
          <!--row1-->          <div class="row padding-10"> 
                        <div class="col-md-12 nopadding" align="center">
                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-",  str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("cattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title"))))));?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>">
                                <?php if($rs_list_similar->Fields("thumbphotofilename") != "") { ?>
                                    <img alt="<?php print $rs_list_similar->Fields("title"); ?>" title="<?php print $rs_list_similar->Fields("title"); ?>" data-src="" src="<?php print $UPLOAD_IMG_PATH.$rs_list_similar->Fields("pkid")."/".$rs_list_similar->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                <?php } else if($rs_list_similar->Fields("imageurl") != "") { ?>
                                    <img alt="<?php print $rs_list_similar->Fields("title"); ?>" title="<?php print $rs_list_similar->Fields("title"); ?>" data-src="" src="<?php print $rs_list_similar->Fields("imageurl"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                <?php } ?>
                            </a>
                            
                            <?php 
                            $str_query_select = "";
                            $str_query_select = "SELECT mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list_similar->Fields("pkid")." AND setasofferimage='LEFT' AND visible='YES' ORDER BY setasofferimage ASC";
                            $rs_list_offerimages_left = GetRecordSet($str_query_select);
                            //print $rs_list_offerimages->Count();
                            if($rs_list_offerimages_left->Count() > 0) {
                            ?>
                            
                            <img src="<?php print $UPLOAD_IMG_PATH.$rs_list_similar->Fields("pkid")."/".$rs_list_offerimages_left->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-left"/>
                            <?php }  ?>
                            <?php 
                            $str_query_select = "";
                            $str_query_select = "SELECT mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list_similar->Fields("pkid")." AND setasofferimage='RIGHT' AND visible='YES' ORDER BY setasofferimage ASC";
                            $rs_list_offerimages_right = GetRecordSet($str_query_select);
                            //print $rs_list_offerimages->Count();
                            if($rs_list_offerimages_right->Count() > 0) {
                            ?>
                            <img src="<?php print $UPLOAD_IMG_PATH.$rs_list_similar->Fields("pkid")."/".$rs_list_offerimages_right->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-right"/>
                            <?php }  ?>
                            
                        </div>
                    </div>
        <!--row2-->            <div class="row padding-10">
                        <div class="col-md-12 product-content-bg">
                            <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("cattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title"))))));?>" title="<?php print $rs_list_similar->Fields("title");//$str_hover_view_details; ?>"><b class="text-muted"><?php print MakeStringShort($rs_list_similar->Fields("title"),23); ?></b></a></p>
                           
                            <p>
                                <?php if($rs_list->Fields("displayasnew") == 'YES') { print $STR_ICON_PATH_NEW; } ?><?php if($rs_list->Fields("displayasfeatured") == 'YES') { print "&nbsp;".$STR_ICON_PATH_FEATURED; } ?><?php if($rs_list->Fields("displayashot") == 'YES') { print "&nbsp;".$STR_ICON_PATH_HOT; } ?>                            </p>
                            
                            <p>
                                <?php if($str_user_type == "WHOLESALER" && $rs_list_similar->Fields("memberprice") > 0) { 
                                    $int_wholesaler_price = "";
                                    $int_wholesaler_price = $rs_list_similar->Fields("memberprice") / $int_conversionrate;
                                    ?>
                                <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_wholesaler_price), 2)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                <?php } else { ?>
                                
                                <?php 
                                if($rs_list_similar->Fields("ourprice") > 0 && ($rs_list_similar->Fields("ourprice") != $rs_list_similar->Fields("listprice"))) { 
                                    $int_list_price = "";
                                    $int_list_price = $rs_list_similar->Fields("listprice") / $int_conversionrate;
                                    
                                    $int_our_price = "";
                                    $int_our_price = $rs_list_similar->Fields("ourprice") / $int_conversionrate; ?>
                                    <strike class='text-muted'><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 2)."&nbsp;".$str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                    <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_our_price), 2)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                <?php
                                    $int_per_discount = 0;
                                    $int_per_discount = (100 - ($rs_list_similar->Fields("ourprice")/$rs_list_similar->Fields("listprice")) * 100); 
                                    if($int_per_discount > 0) { ?>
                                        <label class="label label-primary"><b><?php print number_format($int_per_discount, 2); ?>% OFF</b></label> 
                                    <?php } ?>
                                <?php } else { ?>
                                        <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 2)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                <?php }  ?>
                                <?php }  ?>
                            </p>
                        </div>
                    </div>
                    <div id="success<?php print $rs_list_similar->fields("pkid"); ?>"></div>
          <!--row3-->          <div class="row padding-10">
                        <div class="col-md-12">
                            <div class="row padding-10">
                                <?php /*?><div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))));?>" type="button" class="product-button"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a> 
                                </div><?php */?>
                                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title"))))));?>" type="button" class="product-buynow-button"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a> 
                                </div>
                                
                                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                <?php 
                                ## --------------------------------------------- START : WISHLIST------------------------------------------
                                if($str_member_flag) 
                                { 
                                    $str_query_select="";
                                    $str_query_select="SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=".$_SESSION["userpkid"]." AND itempkid=".$rs_list_similar->fields("pkid");
                                    $rs_list_fav=GetRecordSet($str_query_select);

                                    $str_fav_class = "";
                                    $str_fav_flag = "";
                                    if($rs_list_fav->Count() == 0) { 
                                        $str_fav_class = "";
                                        $str_fav_flag = "FAV"; 
                                    } else if($rs_list_fav->Count() > 0) { 
                                      $str_fav_class = "text-primary";
                                      $str_fav_flag = "UNFAV"; 
                                    } ?>
                                    <form name="frm_user_favorite<?php print $rs_list_similar->fields("pkid"); ?>" id="frm_user_favorite<?php print $rs_list_similar->fields("pkid"); ?>" novalidate>

                                        <input type="hidden" name="userpkid" id="userpkid<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($_SESSION["userpkid"]); ?>">
                                        <input type="hidden" name="itempkid" id="itempkid<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($rs_list_similar->fields("pkid")) ?>">

                                        <input type="hidden" name="flgfav" id="flgfav<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($str_fav_flag); ?>">



                                      <?php //print $rs_list_fav->Count(); ?>
                                        <button id="frm_submit<?php print($rs_list_similar->fields("pkid")) ?>" name="frm_submit<?php print($rs_list_similar->fields("pkid")) ?>" type="submit" style="display: none;" ></button>
                                    </form>

                                    <a onclick="$('#frm_submit<?php print($rs_list_similar->fields("pkid")) ?>').click();" class="product-button <?php print $str_fav_class; ?>"><i class="fa fa-heart"></i></a>
                                    <script>
                                        $(function() {
                                            $("#frm_user_favorite<?php print $rs_list_similar->fields("pkid"); ?> input").jqBootstrapValidation({
                                                preventSubmit: true,
                                                submitError: function($form, event, errors) {
                                                    // something to have when submit produces an error ?
                                                    // Not decided if I need it yet
                                                },
                                                submitSuccess: function($form, event) {
                                                    event.preventDefault(); // prevent default submit behaviour
                                                    // get values from FORM

                                                    var userpkid = $("input#userpkid<?php print($rs_list_similar->fields("pkid")) ?>").val();
                                                    //alert(userpkid);
                                                    var itempkid = $("input#itempkid<?php print($rs_list_similar->fields("pkid")) ?>").val();
                                                    var flgfav = $("input#flgfav<?php print($rs_list_similar->fields("pkid")) ?>").val();


                                                    $.ajax({

                                                        url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                                        type: "POST",
                                                        data: 
                                                        {

                                                            userpkid : userpkid,
                                                            itempkid : itempkid,
                                                            flgfav : flgfav
                                                        },
                                                        cache: false,
                                                        success: function(data) 
                                                        {
                                                        //alert(data);
                                                            var $responseText = JSON.parse(data);
                                                            if($responseText.status == 'SUC')
                                        //                                        if(($responseText.status).equal("SUC"))
                                                            {
                                                                // Success message
                                        //                        alert(data);

                                                                $('#success<?php print $rs_list_similar->fields("pkid"); ?>').html("<div class='alert alert-success small'>");
                                                                $('#success<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                .append("</button>");
                                                                $('#success<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                                $('#success<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').append('</div>');
                          //setTimeout(function(){ location.reload() }, 2000);
                                                                    setTimeout(function(){ location.reload(); }, 5000);
                                                            }
                                                            else if($responseText.status == 'ERR')
                                                            {
                                                                // Fail message
                                                                $('#success<?php print $rs_list_similar->fields("pkid"); ?>').html("<div class='alert alert-danger'>");
                                                                $('#success<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                        .append("</button>");
                                                                $('#success<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                                $('#success<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').append('</div>');

                                                            }
                                                        },

                                                    })
                                                },
                                                filter: function() {
                                                    return $(this).is(":visible");
                                                },
                                            });

                                            $("a[data-toggle=\"tab\"]").click(function(e) {
                                                e.preventDefault();
                                                $(this).tab("show");
                                            });
                                        });
                                        /*When clicking on Full hide fail/success boxes */
                                        $('#name').focus(function() {
                                            $('#success').html('');
                                        });

                                    </script>
                                <?php } else { ?>
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>" class="product-button"><i class="fa fa-heart"></i></a>
                                <?php } 
                                ## --------------------------------------------- END : WISHLIST------------------------------------------ ?>    
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
			</div>
			<!--view more-->
			<?php if($totalRowCount > $showLimit){ ?>
			<div class="show_more_main" id="show_more_main<?php echo $postID; ?>">
				<span id="<?php echo $postID; ?>" class="show_more" title="Load more posts">Show more</span>
				<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
			</div>
			<?php } ?>
            <?php
            $int_cnt++; if($int_cnt%4 == 0) { print "</div><div class='row padding-10 hidden-sm hidden-xs'>"; }
            $rs_list_similar->MoveNext();
            }$rs_list_similar->MoveFirst();
            ?>
        </div>
        <div class="row padding-10 hidden-md hidden-lg hidden-xs">
		<div class="postList">
            <?php 
            $int_cnt = 0;
            while(!$rs_list_similar->EOF()) {
				$postID=$rs_list_similar->Fields("pkid");	/* product id */
            ?>
            <div class="col-sm-4">  <!--sm screen size-->
                <div class="product-box text-center">
          <!--row4-->          <div class="row padding-10">
                        <div class="col-md-12 nopadding" align="center">
                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("cattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title"))))));?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>">
                                <?php if($rs_list_similar->Fields("thumbphotofilename") != "") { ?>
                                    <img alt="<?php print $rs_list_similar->Fields("title"); ?>" title="<?php print $rs_list_similar->Fields("title"); ?>" data-src="" src="<?php print $UPLOAD_IMG_PATH.$rs_list_similar->Fields("pkid")."/".$rs_list_similar->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                <?php } else if($rs_list_similar->Fields("imageurl") != "") { ?>
                                    <img alt="<?php print $rs_list_similar->Fields("title"); ?>" title="<?php print $rs_list_similar->Fields("title"); ?>" data-src="" src="<?php print $rs_list_similar->Fields("imageurl"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                <?php } ?>
                            </a>
                           
                            <?php 
                            $str_query_select = "";
                            $str_query_select = "SELECT mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list_similar->Fields("pkid")." AND setasofferimage='LEFT' AND visible='YES' ORDER BY setasofferimage ASC";
                            $rs_list_offerimages_left = GetRecordSet($str_query_select);
                            //print $rs_list_offerimages->Count();
                            if($rs_list_offerimages_left->Count() > 0) {
                            ?>
                            
                            <img src="<?php print $UPLOAD_IMG_PATH.$rs_list_similar->Fields("pkid")."/".$rs_list_offerimages_left->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-left"/>
                            <?php }  ?>
                            <?php 
                            $str_query_select = "";
                            $str_query_select = "SELECT mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list_similar->Fields("pkid")." AND setasofferimage='RIGHT' AND visible='YES' ORDER BY setasofferimage ASC";
                            $rs_list_offerimages_right = GetRecordSet($str_query_select);
                            //print $rs_list_offerimages->Count();
                            if($rs_list_offerimages_right->Count() > 0) {
                            ?>
                            <img src="<?php print $UPLOAD_IMG_PATH.$rs_list_similar->Fields("pkid")."/".$rs_list_offerimages_right->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-right"/>
                            <?php }  ?>
                            
                        </div>
                    </div>
      <!--row5-->              <div class="row padding-10">
                        <div class="col-md-12 product-content-bg">
                            <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("cattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title"))))));?>" title="<?php print $rs_list_similar->Fields("title");//$str_hover_view_details; ?>"><b class="text-muted"><?php print MakeStringShort($rs_list_similar->Fields("title"),23); ?></b></a></p>
                            <p>
                                <?php if($rs_list->Fields("displayasnew") == 'YES') { print $STR_ICON_PATH_NEW; } ?><?php if($rs_list->Fields("displayasfeatured") == 'YES') { print "&nbsp;".$STR_ICON_PATH_FEATURED; } ?><?php if($rs_list->Fields("displayashot") == 'YES') { print "&nbsp;".$STR_ICON_PATH_HOT; } ?>                            </p>
                            <?php /* ?><p>Product Code : 000000</p><?php */ ?>
                            <p>
                                <?php if($str_user_type == "WHOLESALER" && $rs_list_similar->Fields("memberprice") > 0) { 
                                    $int_wholesaler_price = "";
                                    $int_wholesaler_price = $rs_list_similar->Fields("memberprice") / $int_conversionrate;
                                    ?>
                                <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_wholesaler_price), 2)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                <?php } else { ?>
                                <?php if($rs_list_similar->Fields("ourprice") > 0 && ($rs_list_similar->Fields("ourprice") != $rs_list_similar->Fields("listprice"))) { 
                                    $int_list_price = "";
                                    $int_list_price = $rs_list_similar->Fields("listprice") / $int_conversionrate;
                                    
                                    $int_our_price = "";
                                    $int_our_price = $rs_list_similar->Fields("ourprice") / $int_conversionrate; ?>
                                    <strike class='text-muted'><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 2)."&nbsp;".$str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                    <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_our_price), 2)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                <?php
                                    $int_per_discount = 0;
                                    $int_per_discount = (100 - ($rs_list_similar->Fields("ourprice")/$rs_list_similar->Fields("listprice")) * 100); 
                                    if($int_per_discount > 0) { ?>
                                        <label class="label label-primary"><b><?php print number_format($int_per_discount, 2); ?>% OFF</b></label> 
                                    <?php } ?>
                                <?php } else { ?>
                                        <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 2)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                <?php }  ?>
                                <?php } ?>
                            </p>
                        </div>
                    </div>
                    <div id="success2<?php print $rs_list_similar->fields("pkid"); ?>"></div>
      <!--row6-->              <div class="row padding-10">
                        <div class="col-md-12">
                            <div class="row padding-10">
                                <?php /*?><div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))));?>" type="button" class="product-button"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a> 
                                </div><?php */?>
                                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title"))))));?>" type="button" class="product-buynow-button"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a> 
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                <?php 
                                ## --------------------------------------------- START : WISHLIST------------------------------------------
                                if($str_member_flag) 
                                { 
                                    $str_query_select="";
                                    $str_query_select="SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=".$_SESSION["userpkid"]." AND itempkid=".$rs_list_similar->fields("pkid");
                                    $rs_list_fav=GetRecordSet($str_query_select);

                                    $str_fav_class = "";
                                    $str_fav_flag = "";
                                    if($rs_list_fav->Count() == 0) { 
                                        $str_fav_class = "";
                                        $str_fav_flag = "FAV"; 
                                    } else if($rs_list_fav->Count() > 0) { 
                                      $str_fav_class = "text-primary";
                                      $str_fav_flag = "UNFAV"; 
                                    } ?>
                                    <form name="frm_user_favorite2<?php print $rs_list_similar->fields("pkid"); ?>" id="frm_user_favorite2<?php print $rs_list_similar->fields("pkid"); ?>" novalidate>

                                        <input type="hidden" name="userpkid" id="userpkid<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($_SESSION["userpkid"]); ?>">
                                        <input type="hidden" name="itempkid" id="itempkid<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($rs_list_similar->fields("pkid")) ?>">

                                        <input type="hidden" name="flgfav" id="flgfav<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($str_fav_flag); ?>">



                                      <?php //print $rs_list_fav->Count(); ?>
                                        <button id="frm_submit2<?php print($rs_list_similar->fields("pkid")) ?>" name="frm_submit2<?php print($rs_list_similar->fields("pkid")) ?>" type="submit" style="display: none;" ></button>
                                    </form>

                                    <a onclick="$('#frm_submit2<?php print($rs_list_similar->fields("pkid")) ?>').click();" class="product-button <?php print $str_fav_class; ?>"><i class="fa fa-heart"></i></a>
                                    <script>
                                        $(function() {
                                            $("#frm_user_favorite2<?php print $rs_list_similar->fields("pkid"); ?> input").jqBootstrapValidation({
                                                preventSubmit: true,
                                                submitError: function($form, event, errors) {
                                                    // something to have when submit produces an error ?
                                                    // Not decided if I need it yet
                                                },
                                                submitSuccess: function($form, event) {
                                                    event.preventDefault(); // prevent default submit behaviour
                                                    // get values from FORM

                                                    var userpkid = $("input#userpkid<?php print($rs_list_similar->fields("pkid")) ?>").val();
                                                    //alert(userpkid);
                                                    var itempkid = $("input#itempkid<?php print($rs_list_similar->fields("pkid")) ?>").val();
                                                    var flgfav = $("input#flgfav<?php print($rs_list_similar->fields("pkid")) ?>").val();


                                                    $.ajax({

                                                        url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                                        type: "POST",
                                                        data: 
                                                        {

                                                            userpkid : userpkid,
                                                            itempkid : itempkid,
                                                            flgfav : flgfav
                                                        },
                                                        cache: false,
                                                        success: function(data) 
                                                        {
                                                        //alert(data);
                                                            var $responseText = JSON.parse(data);
                                                            if($responseText.status == 'SUC')
                                        //                                        if(($responseText.status).equal("SUC"))
                                                            {
                                                                // Success message
                                        //                        alert(data);

                                                                $('#success2<?php print $rs_list_similar->fields("pkid"); ?>').html("<div class='alert alert-success small'>");
                                                                $('#success2<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                .append("</button>");
                                                                $('#success2<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                                $('#success2<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').append('</div>');
                          //setTimeout(function(){ location.reload() }, 2000);
                                                                    setTimeout(function(){ location.reload(); }, 5000);
                                                            }
                                                            else if($responseText.status == 'ERR')
                                                            {
                                                                // Fail message
                                                                $('#success2<?php print $rs_list_similar->fields("pkid"); ?>').html("<div class='alert alert-danger'>");
                                                                $('#success2<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                        .append("</button>");
                                                                $('#success2<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                                $('#success2<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').append('</div>');

                                                            }
                                                        },

                                                    })
                                                },
                                                filter: function() {
                                                    return $(this).is(":visible");
                                                },
                                            });

                                            $("a[data-toggle=\"tab\"]").click(function(e) {
                                                e.preventDefault();
                                                $(this).tab("show");
                                            });
                                        });
                                        /*When clicking on Full hide fail/success boxes */
                                        $('#name').focus(function() {
                                            $('#success').html('');
                                        });

                                    </script>
                                <?php } else { ?>
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>" class="product-button"><i class="fa fa-heart"></i></a>
                                <?php } 
                                ## --------------------------------------------- END : WISHLIST------------------------------------------ ?>    
                                </div>
                            </div>                            
                        </div>
                    </div>    
                </div>
            </div>
			</div>
			<!--view more-->
			<?php if($totalRowCount > $showLimit){ ?>
			<div class="show_more_main" id="show_more_main<?php echo $postID; ?>">
				<span id="<?php echo $postID; ?>" class="show_more" title="Load more posts">Show more</span>
				<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
			</div>
			<?php } ?>
            <?php
            $int_cnt++; if($int_cnt%3 == 0) { print "</div><div class='row padding-10 hidden-md hidden-lg hidden-xs'>"; }
            $rs_list_similar->MoveNext();
            }$rs_list_similar->MoveFirst();
            ?>
        </div>
        <div class="row padding-10 hidden-sm hidden-md hidden-lg">
		<div class="postList">
            <?php 
            $int_cnt = 0;
            while(!$rs_list_similar->EOF()) {
				$postID=$rs_list_similar->Fields("pkid");	/*product id*/
            ?>
            <div class="col-xs-6">  <!--for xs screen size-->
                <div class="product-box text-center">
         <!--row7-->           <div class="row padding-10">
                        <div class="col-md-12 nopadding" align="center">
                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("cattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title"))))));?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>">
                                <?php if($rs_list_similar->Fields("thumbphotofilename") != "") { ?>
                                    <img alt="<?php print $rs_list_similar->Fields("title"); ?>" title="<?php print $rs_list_similar->Fields("title"); ?>" data-src="" src="<?php print $UPLOAD_IMG_PATH.$rs_list_similar->Fields("pkid")."/".$rs_list_similar->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                <?php } else if($rs_list_similar->Fields("imageurl") != "") { ?>
                                    <img alt="<?php print $rs_list_similar->Fields("title"); ?>" title="<?php print $rs_list_similar->Fields("title"); ?>" data-src="" src="<?php print $rs_list_similar->Fields("imageurl"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                <?php } ?>
                            </a>
                            
                            <?php 
                            $str_query_select = "";
                            $str_query_select = "SELECT mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list_similar->Fields("pkid")." AND setasofferimage='LEFT' AND visible='YES' ORDER BY setasofferimage ASC";
                            $rs_list_offerimages_left = GetRecordSet($str_query_select);
                            //print $rs_list_offerimages->Count();
                            if($rs_list_offerimages_left->Count() > 0) {
                            ?>
                            
                            <img src="<?php print $UPLOAD_IMG_PATH.$rs_list_similar->Fields("pkid")."/".$rs_list_offerimages_left->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-left"/>
                            <?php }  ?>
                            <?php 
                            $str_query_select = "";
                            $str_query_select = "SELECT mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list_similar->Fields("pkid")." AND setasofferimage='RIGHT' AND visible='YES' ORDER BY setasofferimage ASC";
                            $rs_list_offerimages_right = GetRecordSet($str_query_select);
                            //print $rs_list_offerimages->Count();
                            if($rs_list_offerimages_right->Count() > 0) {
                            ?>
                            <img src="<?php print $UPLOAD_IMG_PATH.$rs_list_similar->Fields("pkid")."/".$rs_list_offerimages_right->Fields("mainphotofilename"); ?>" alt="" title="" class="img-top-right"/>
                            <?php }  ?>
                            
                        </div>
                    </div>
        <!--row8-->            <div class="row padding-10">
                        <div class="col-md-12 product-content-bg">
                            <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("cattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title"))))));?>" title="<?php print $rs_list_similar->Fields("title");//$str_hover_view_details; ?>"><b class="text-muted"><?php print MakeStringShort($rs_list_similar->Fields("title"),18); ?></b></a></p>
                            <p>
                                <?php if($rs_list->Fields("displayasnew") == 'YES') { print $STR_ICON_PATH_NEW; } ?><?php if($rs_list->Fields("displayasfeatured") == 'YES') { print "&nbsp;".$STR_ICON_PATH_FEATURED; } ?><?php if($rs_list->Fields("displayashot") == 'YES') { print "&nbsp;".$STR_ICON_PATH_HOT; } ?>                            </p>
                            <?php /* ?><p>Product Code : 000000</p><?php */ ?>
                            <p>
                                <?php if($str_user_type == "WHOLESALER" && $rs_list_similar->Fields("memberprice") > 0) { 
                                    $int_wholesaler_price = "";
                                    $int_wholesaler_price = $rs_list_similar->Fields("memberprice") / $int_conversionrate;
                                    ?>
                                <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_wholesaler_price), 2)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                <?php } else { ?>
                                
                                <?php if($rs_list_similar->Fields("ourprice") > 0 && ($rs_list_similar->Fields("ourprice") != $rs_list_similar->Fields("listprice"))) { 
                                    $int_list_price = "";
                                    $int_list_price = $rs_list_similar->Fields("listprice") / $int_conversionrate;
                                    
                                    $int_our_price = "";
                                    $int_our_price = $rs_list_similar->Fields("ourprice") / $int_conversionrate; ?>
                                    <strike class='text-muted'><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 2)."&nbsp;".$str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                    <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_our_price), 2)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                <?php
                                    $int_per_discount = 0;
                                    $int_per_discount = (100 - ($rs_list_similar->Fields("ourprice")/$rs_list_similar->Fields("listprice")) * 100); 
                                    if($int_per_discount > 0) { ?>
                                        <label class="label label-primary"><b><?php print number_format($int_per_discount, 2); ?>% OFF</b></label> 
                                    <?php } ?>
                                <?php } else { ?>
                                        <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 2)."&nbsp;".$str_currency_shortform;; ?></b>&nbsp;
                                <?php }  ?>
                                <?php } ?>
                            </p>
                        </div>
                    </div>
                    <div id="success3<?php print $rs_list_similar->fields("pkid"); ?>"></div>
        <!--row9-->            <div class="row padding-10">
                        <div class="col-md-12">
                            <div class="row padding-10">
                                <?php /*?><div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title")))));?>" type="button" class="product-button"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a> 
                                </div><?php */?>
                                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_similar->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("subcattitle"))))));?>/<?php print(str_replace(" ", "-", str_replace("'", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_similar->Fields("title"))))));?>" type="button" class="product-buynow-button"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a> 
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 nopadding">
                                <?php 
                                ## --------------------------------------------- START : WISHLIST------------------------------------------
                                if($str_member_flag) 
                                { 
                                    $str_query_select="";
                                    $str_query_select="SELECT itempkid FROM tr_user_favorite_item WHERE userpkid=".$_SESSION["userpkid"]." AND itempkid=".$rs_list_similar->fields("pkid");
                                    $rs_list_fav=GetRecordSet($str_query_select);

                                    $str_fav_class = "";
                                    $str_fav_flag = "";
                                    if($rs_list_fav->Count() == 0) { 
                                        $str_fav_class = "";
                                        $str_fav_flag = "FAV"; 
                                    } else if($rs_list_fav->Count() > 0) { 
                                      $str_fav_class = "text-primary";
                                      $str_fav_flag = "UNFAV"; 
                                    } ?>
                                    <form name="frm_user_favorite3<?php print $rs_list_similar->fields("pkid"); ?>" id="frm_user_favorite3<?php print $rs_list_similar->fields("pkid"); ?>" novalidate>

                                        <input type="hidden" name="userpkid" id="userpkid<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($_SESSION["userpkid"]); ?>">
                                        <input type="hidden" name="itempkid" id="itempkid<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($rs_list_similar->fields("pkid")) ?>">

                                        <input type="hidden" name="flgfav" id="flgfav<?php print($rs_list_similar->fields("pkid")) ?>" value="<?php print($str_fav_flag); ?>">



                                      <?php //print $rs_list_fav->Count(); ?>
                                        <button id="frm_submit3<?php print($rs_list_similar->fields("pkid")) ?>" name="frm_submit3<?php print($rs_list_similar->fields("pkid")) ?>" type="submit" style="display: none;" ></button>
                                    </form>

                                    <a onclick="$('#frm_submit3<?php print($rs_list_similar->fields("pkid")) ?>').click();" class="product-button <?php print $str_fav_class; ?>"><i class="fa fa-heart"></i></a>
                                    <script>
                                        $(function() {
                                            $("#frm_user_favorite3<?php print $rs_list_similar->fields("pkid"); ?> input").jqBootstrapValidation({
                                                preventSubmit: true,
                                                submitError: function($form, event, errors) {
                                                    // something to have when submit produces an error ?
                                                    // Not decided if I need it yet
                                                },
                                                submitSuccess: function($form, event) {
                                                    event.preventDefault(); // prevent default submit behaviour
                                                    // get values from FORM

                                                    var userpkid = $("input#userpkid<?php print($rs_list_similar->fields("pkid")) ?>").val();
                                                    //alert(userpkid);
                                                    var itempkid = $("input#itempkid<?php print($rs_list_similar->fields("pkid")) ?>").val();
                                                    var flgfav = $("input#flgfav<?php print($rs_list_similar->fields("pkid")) ?>").val();


                                                    $.ajax({

                                                        url: "<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite_item_add_p.php",
                                                        type: "POST",
                                                        data: 
                                                        {

                                                            userpkid : userpkid,
                                                            itempkid : itempkid,
                                                            flgfav : flgfav
                                                        },
                                                        cache: false,
                                                        success: function(data) 
                                                        {
                                                        //alert(data);
                                                            var $responseText = JSON.parse(data);
                                                            if($responseText.status == 'SUC')
                                        //                                        if(($responseText.status).equal("SUC"))
                                                            {
                                                                // Success message
                                        //                        alert(data);

                                                                $('#success3<?php print $rs_list_similar->fields("pkid"); ?>').html("<div class='alert alert-success small'>");
                                                                $('#success3<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                .append("</button>");
                                                                $('#success3<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').append("<strong> " + $responseText.message + " </strong>");
                                                                $('#success3<?php print $rs_list_similar->fields("pkid"); ?> > .alert-success ').append('</div>');
                          //setTimeout(function(){ location.reload() }, 2000);
                                                                    setTimeout(function(){ location.reload(); }, 5000);
                                                            }
                                                            else if($responseText.status == 'ERR')
                                                            {
                                                                // Fail message
                                                                $('#success3<?php print $rs_list_similar->fields("pkid"); ?>').html("<div class='alert alert-danger'>");
                                                                $('#success3<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                                                        .append("</button>");
                                                                $('#success3<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').append("<strong> " + $responseText.message + " ");
                                                                $('#success3<?php print $rs_list_similar->fields("pkid"); ?> > .alert-danger').append('</div>');

                                                            }
                                                        },

                                                    })
                                                },
                                                filter: function() {
                                                    return $(this).is(":visible");
                                                },
                                            });

                                            $("a[data-toggle=\"tab\"]").click(function(e) {
                                                e.preventDefault();
                                                $(this).tab("show");
                                            });
                                        });
                                        /*When clicking on Full hide fail/success boxes */
                                        $('#name').focus(function() {
                                            $('#success').html('');
                                        });

                                    </script>
                                <?php } else { ?>
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login" title="<?php print $STR_HOVER_FAVORITE; ?>" class="product-button"><i class="fa fa-heart"></i></a>
                                <?php } 
                                ## --------------------------------------------- END : WISHLIST------------------------------------------ ?>    
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
			</div>
			<!--view more-->
			<?php if($totalRowCount > $showLimit){ ?>
			<div class="show_more_main" id="show_more_main<?php echo $postID; ?>">
				<span id="<?php echo $postID; ?>" class="show_more" title="Load more posts">Show more</span>
				<span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
			</div>
			<?php } ?>
            <?php
            $int_cnt++; if($int_cnt%2 == 0) { print "</div><div class='row padding-10 hidden-sm hidden-md hidden-lg'>"; }
            $rs_list_similar->MoveNext();
            }$rs_list_similar->MoveFirst();
            ?>
        </div>    
        <?php } ?><br/>        
    </div>
    
   
    
       
    <?php } ?>
				
	
  
