<?php
#-------------------------------------------------------------------------------
#open xml file for content display
$fp=openXMLfile($STR_XML_FILE_PATH_CMS."webmaster_cms.xml");
$str_image_=getTagValue("WEBMASTER_HOME_PHOTO",$fp);
$str_desc_=getTagValue("WEBMASTER_HOME_DESCRIPTION",$fp);
//$STR_UPLOAD_WEBMASTER_PATH = "./mdm/webmaster/";
closeXMLfile($fp);
#-------------------------------------------------------------------------------
?>
<?php if($str_image_!="" && $str_desc_!="" && $str_desc_!="<br>") { ?>
<div class="row padding-10">
    <div class="col-md-4">
        <?php if($str_image_!="") { ?>
        <?php 
        $path=$STR_UPLOAD_WEBMASTER_PATH.$str_image_;
        ///print c;
        $image_tag=ResizeImageTag($path,0,0,$INT_WEBMASTER_HOME,"Webmaster_Image","",0);
//        print($image_tag);
         ?>
        <img src="<?php print $path; ?>" class="img-responsive" alt="Webmaster Image" title="Webmaster Image"/>
    <?php } ?>
    </div>
    <div class="col-md-8">
        
        <?php if($str_desc_!="") { ?>
        <?php	
        $arr_features_ = explode("\n",$str_desc_);
        for($i=0;$i<count($arr_features_);$i++)
        {
            if($arr_features_[$i]!=trim("")) { ?>	
                <?php print($arr_features_[$i]);?>
    <?php } }}?>
    </div>
</div><br/>
<?php } else if($str_image_=="" && $str_desc_!="") { ?>
<div class="row padding-10">
     <div class="col-md-12">
        
        <?php if($str_desc_!="") { ?>
        <?php	
        $arr_features_ = explode("\n",$str_desc_);
        for($i=0;$i<count($arr_features_);$i++)
        {
            if($arr_features_[$i]!=trim("")) { ?>	
                <?php print($arr_features_[$i]);?>
    <?php } }}?>
    </div>
</div><br/>
<?php } else if($str_image_!="" && $str_desc_=="" || $str_desc_=="<br>") { ?>
<div class="row padding-10">
    <div class="col-md-4"></div>
     <div class="col-md-4" align="center">
      <?php if($str_image_!="") { ?>
        <?php 
        $path=$STR_UPLOAD_WEBMASTER_PATH.$str_image_;
        ///print c;
//        $image_tag=ResizeImageTag($path,0,0,$INT_WEBMASTER_HOME,"Webmaster_Image","",0);
        $image_tag=$path;
//        print($image_tag);
         ?>
        <img src="<?php print $path; ?>" class="img-responsive" alt="Webmaster Image" title="Webmaster Image"/>
    <?php } ?>
    </div>
    <div class="col-md-4"></div>
</div><br/>
<?php } ?>
