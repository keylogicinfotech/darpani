<?php 
/*
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
session_start();
include("./user_validate_session.php");
include "./../includes/configuration.php";
include "./product_config.php";
include("./user_config.php");
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
include "./../includes/lib_datetimeyear.php";
include "./../includes/lib_file_upload.php";
//include "./../includes/lib_file_upload.php";
//include "./../includes/http_to_https.php";	
//include "./../includes/count_site_unique_view.php";
include "./../includes/lib_xml.php";
#----------------------------------------------------------------------
$str_img_path_store = "../mdm/store/";
$str_title_page = "Favorite";
//$STR_TITLE_USER_FAVORITE = "User Favorite List";
$INT_USER_SESSION_PKID=$_SESSION["userpkid"];
//print $INT_USER_SESSION_PKID;
$STR_USER_SESSION_LOGINID=$_SESSION["userpkid"];
$str_page_key_metatag = 'PG_STORE';
$str_db_table_name_store_photo = "tr_store_photo"; 
$str_db_table_name_store_cat = "t_store_cat"; 
$str_db_table_name_store_subcat = "t_store_subcat";
#----------------------------------------------------------------------
#get user details
$str_query_select = "SELECT loginid, name FROM t_user WHERE pkid=".$INT_USER_SESSION_PKID;
$rs_list_user = GetRecordSet($str_query_select);


/*$str_query_select="SELECT fm.*,m.* ";
$str_query_select.=" FROM  tr_model_favorite_freemem fm ";
$str_query_select.=" LEFT JOIN t_freemember m ON fm.memberpkid=m.pkid "; 
$str_query_select.=" WHERE m.visible='YES' AND fm.modelpkid=".$INT_MODEL_SESSION_PKID;
$str_query_select.=" ORDER BY fm.addeddate DESC"; */
//----------------------------------------------------------------------------------------------------
$str_query_select="SELECT DISTINCT fi.itempkid,fi.*,s.*,p.thumbphotofilename, p.imageurl,d.title AS cattitle,b.subcattitle";
$str_query_select.=" FROM  ".$STR_DB_TR_TABLE_NAME_FAVORITES." fi ";
$str_query_select.=" LEFT JOIN ".$STR_DB_TABLE_NAME." s ON fi.itempkid=s.pkid ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON s.catpkid=d.catpkid  AND d.visible='YES' ";
 $str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." b ON s.subcatpkid=b.subcatpkid AND b.visible='YES' ";
$str_query_select.=" LEFT JOIN ".$STR_DB_TABLE_NAME_PHOTO." p ON s.pkid=p.masterpkid AND p.setasfront='YES'";
$str_query_select.=" WHERE s.visible='YES' AND fi.userpkid=".$INT_USER_SESSION_PKID;
$str_query_select.=" ORDER BY fi.date DESC";
//print $str_query_select;
$rs_list=GetRecordSet($str_query_select);



$str_query_select = "SELECT title,titletag FROM t_page_metatag WHERE pagekey='PG_STORE'";
$rs_list_page_metatag = GetRecordSet($str_query_select);
#----------------------------------------------------------------------

$str_member_flag = false;
$int_userpkid = 0;
if(isset($_SESSION['userpkid']) && $_SESSION['userpkid'] != "")
{
    $str_member_flag = true;
    $int_userpkid = $_SESSION['userpkid'];
}

$str_user_type = "";
if($int_userpkid > 0)
{
    $str_query_select = "";
    $str_query_select = "SELECT isusertype FROM ".$STR_DB_TABLE_NAME_USER." WHERE pkid=".$int_userpkid;
    
    $rs_list_user = GetRecordSet($str_query_select);
    if(!$rs_list_user->EOF())
    {
        if($rs_list_user->Fields("isusertype") == "WHOLESALER")
        {
            $str_user_type = "WHOLESALER";
        }
    }
    //$str_where = " WHERE userpkid = ".$int_userpkid." ";
}
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php  print($rs_list_page_metatag->fields("titletag")) ;?><?php  if($str_title_page != "") { print " [".$str_title_page."]"; }?></title>
    <?php // print(Display_Page_Metatag($str_title_page_metatag)); ?>
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/main-sidebar.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/custom.css" rel="stylesheet" type="text/css" />
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/user.css" rel="stylesheet" />   
    <link href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> 
    
</head>
<body>
<?php include($STR_USER_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-lg-12 col-md-12">
                <h1 align="right"><?php print $str_title_page; ?></h1><hr/>
            </div>
        </div><br/>
        
       <div class="row padding-10">
            <div class="col-md-3 col-xs-12 col-sm-12">
            <?php include($STR_USER_PANEL_PATH); ?>
            </div>
            <div class="col-md-9 col-xs-12 col-sm-12">
                <!----------------------------favorite item list---------------------------->
                <?php if($rs_list->Count() <= 0) { ?>
                    <div class="row padding-10 hidden-sm hidden-xs">
                        <div class="col-lg-12 col-md-12 alert alert-danger text-center">
                            <?php print $STR_MSG_NO_DATA_AVAILABLE; ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row padding-10 hidden-sm hidden-xs">
                    <?php    
                    $int_cnt = 0;
                    while(!$rs_list->EOF()) { ?>
                        <div class="col-lg-4 col-md-4">
                            <div class="product-box text-center">
                                <div class="row padding-10">
                                    <div class="col-md-12 nopadding" align="center">
                                        <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>">
                                            
                                            <?php if($rs_list->Fields("thumbphotofilename") != "") { ?>
                                                <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $str_img_path_store.$rs_list->Fields("pkid")."/".$rs_list->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                            <?php } else if($rs_list->Fields("imageurl") != "") { ?>
                                                <img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $rs_list->Fields("imageurl"); ?>" data-holder-rendered="true" class="img-responsive img-products">
                                            <?php } ?>
                                        </a>

                                        <?php 
                                        $str_query_select = "";
                                        $str_query_select = "SELECT * FROM ".$str_db_table_name_store_photo." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='LEFT' ORDER BY setasofferimage ASC";
                                        $rs_list_offerimages_left = GetRecordSet($str_query_select);
                                        //print $rs_list_offerimages->Count();
                                        if($rs_list_offerimages_left->Count() > 0) {
                                        ?>

                                        <img src="<?php print $str_img_path_store.$rs_list->Fields("pkid")."/".$rs_list_offerimages_left->Fields("thumbphotofilename"); ?>" alt="" title="" class="img-top-left"/>
                                        <?php }  ?>
                                        <?php 
                                        $str_query_select = "";
                                        $str_query_select = "SELECT * FROM ".$str_db_table_name_store_photo." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='RIGHT' ORDER BY setasofferimage ASC";
                                        $rs_list_offerimages_right = GetRecordSet($str_query_select);
                                        //print $rs_list_offerimages->Count();
                                        if($rs_list_offerimages_right->Count() > 0) {
                                        ?>
                                        <img src="<?php print $str_img_path_store.$rs_list->Fields("pkid")."/".$rs_list_offerimages_right->Fields("thumbphotofilename"); ?>" alt="" title="" class="img-top-right"/>
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-12 product-content-bg">
                                        <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" title="<?php print $str_hover_view_details; ?>"><b class="text-default"><?php print MakeStringShort($rs_list->Fields("title"), 30); ?></b></a></p>
                                        <?php /* ?><p>Product Code : 000000</p><?php */ ?>
                                        <p>
                                            <?php if($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) { ?>
                                                <?php $int_member_price = "";
                                                $int_member_price = $rs_list->Fields("memberprice") / $int_conversionrate; ?>

                                                    <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_member_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                                    
                                            <?php } else { ?>
                                            <?php if($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                                $int_list_price = "";
                                                $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;

                                                $int_our_price = "";
                                                $int_our_price = $rs_list->Fields("ourprice") / $int_conversionrate;
                                                ?>
                                                <strike class="text-muted"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 0)."&nbsp;".$str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                                <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_our_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                                <?php
                                                $int_per_discount = 0;
                                                $int_per_discount = (100 - ($rs_list->Fields("ourprice")/$rs_list->Fields("listprice")) * 100); 
                                                if($int_per_discount > 0) { ?>
                                                <label class="label label-primary"><b><?php print number_format($int_per_discount, 0); ?>% OFF</b></label> 
                                                <?php } ?>
                                            <?php } else {
                                                $int_list_price = "";
                                                $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate; ?>

                                                    <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                            <?php }  ?> 
                                        <?php }  ?>                
                                        </p>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="row padding-10">
                                            <div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                                <a href="./cart.html" type="button" class="product-button"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a> 
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-buynow-button"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a> 
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_unfavorite_p.php?itempkid=<?php print($rs_list->fields("itempkid")); ?>" onClick="return confirm_delete();" type="button" title="click to remove this item" class="product-button" onClick="return confirm_favourites();" data-toggle="" data-target="#"><i class="fa fa-remove"></i></a>
                                            </div> 
                                       </div>
                                    </div>
                                </div>

                            </div>                
                        </div>
                    <?php
                    $int_cnt++; 
                    if($int_cnt%3 == 0) { print "</div><div class='row padding-10 hidden-sm hidden-xs'>"; } 

                    $rs_list->MoveNext();
                } $rs_list->MoveFirst(); ?>
                </div>
                <?php } ?>
                <!--------------------------------end favorite section-----------------------------------> 
                
                <!----------------------------favorite item list---------------------------->
                <?php if($rs_list->Count() <= 0) { ?>
                    <div class="row padding-10 hidden-md hidden-lg hidden-xs">
                        <div class="col-sm-12 alert alert-danger text-center">
                            <?php print $STR_MSG_NO_DATA_AVAILABLE; ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row padding-10 hidden-md hidden-lg hidden-xs">
                    <?php    
                    $int_cnt = 0;
                    while(!$rs_list->EOF()) { ?>
                        <div class="col-sm-4">
                            <div class="product-box text-center">
                                <div class="row padding-10">
                                    <div class="col-md-12 nopadding" align="center">
                                        <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>"><img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $str_img_path_store.$rs_list->Fields("pkid")."/".$rs_list->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products"></a>

                                        <?php 
                                        $str_query_select = "";
                                        $str_query_select = "SELECT * FROM ".$str_db_table_name_store_photo." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='LEFT' ORDER BY setasofferimage ASC";
                                        $rs_list_offerimages_left = GetRecordSet($str_query_select);
                                        //print $rs_list_offerimages->Count();
                                        if($rs_list_offerimages_left->Count() > 0) {
                                        ?>

                                        <img src="<?php print $str_img_path_store.$rs_list->Fields("pkid")."/".$rs_list_offerimages_left->Fields("thumbphotofilename"); ?>" alt="" title="" class="img-top-left"/>
                                        <?php }  ?>
                                        <?php 
                                        $str_query_select = "";
                                        $str_query_select = "SELECT * FROM ".$str_db_table_name_store_photo." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='RIGHT' ORDER BY setasofferimage ASC";
                                        $rs_list_offerimages_right = GetRecordSet($str_query_select);
                                        //print $rs_list_offerimages->Count();
                                        if($rs_list_offerimages_right->Count() > 0) {
                                        ?>
                                        <img src="<?php print $str_img_path_store.$rs_list->Fields("pkid")."/".$rs_list_offerimages_right->Fields("thumbphotofilename"); ?>" alt="" title="" class="img-top-right"/>
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-12 product-content-bg">
                                        <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" title="<?php print $str_hover_view_details; ?>"><b class="text-default"><?php print MakeStringShort($rs_list->Fields("title"), 30); ?></b></a></p>
                                        <?php /* ?><p>Product Code : 000000</p><?php */ ?>
                                        <p>
                                            <?php if($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) { ?>
                                                <?php $int_member_price = "";
                                                $int_member_price = $rs_list->Fields("memberprice") / $int_conversionrate; ?>

                                                    <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_member_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                                    
                                            <?php } else { ?>
                                            <?php if($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                                $int_list_price = "";
                                                $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;

                                                $int_our_price = "";
                                                $int_our_price = $rs_list->Fields("ourprice") / $int_conversionrate;
                                                ?>
                                                <strike class="text-muted"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 0)."&nbsp;".$str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                                <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_our_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                                <?php
                                                $int_per_discount = 0;
                                                $int_per_discount = (100 - ($rs_list->Fields("ourprice")/$rs_list->Fields("listprice")) * 100); 
                                                if($int_per_discount > 0) { ?>
                                                <label class="label label-primary"><b><?php print number_format($int_per_discount, 0); ?>% OFF</b></label> 
                                                <?php } ?>
                                            <?php } else {
                                                $int_list_price = "";
                                                $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate; ?>

                                                    <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                            <?php }  ?> 
                                        <?php }  ?>

                                        </p>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="row padding-10">
                                            <div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                                <a href="./cart.html" type="button" class="product-button"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a> 
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-buynow-button"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a> 
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_unfavorite_p.php?itempkid=<?php print($rs_list->fields("itempkid")); ?>" onClick="return confirm_delete();" type="button" title="click to remove this item" class="product-button" onClick="return confirm_favourites();" data-toggle="" data-target="#"><i class="fa fa-remove"></i></a>
                                            </div> 
                                       </div>
                                    </div>
                                </div>

                            </div>                
                        </div>
                    <?php
                    $int_cnt++; 
                    if($int_cnt%3 == 0) { print "</div><div class='row padding-10 hidden-md hidden-lg hidden-xs'>"; } 

                    $rs_list->MoveNext();
                } $rs_list->MoveFirst(); ?>
                </div>
                <?php } ?>
                <!--------------------------------end favorite section-----------------------------------> 
                
                
                <!----------------------------favorite item list---------------------------->
                <?php if($rs_list->Count() <= 0) { ?>
                    <div class="row padding-10 hidden-md hidden-lg hidden-sm">
                        <div class="col-xs-12 alert alert-danger text-center">
                            <?php print $STR_MSG_NO_DATA_AVAILABLE; ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row padding-10 hidden-md hidden-lg hidden-sm">
                    <?php    
                    $int_cnt = 0;
                    while(!$rs_list->EOF()) { ?>
                        <div class="col-xs-6">
                            <div class="product-box text-center">
                                <div class="row padding-10">
                                    <div class="col-md-12 nopadding" align="center">
                                        <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" title="<?php print $STR_HOVER_VIEW_DETAILS; ?>"><img alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" data-src="" src="<?php print $str_img_path_store.$rs_list->Fields("pkid")."/".$rs_list->Fields("thumbphotofilename"); ?>" data-holder-rendered="true" class="img-responsive img-products"></a>

                                        <?php 
                                        $str_query_select = "";
                                        $str_query_select = "SELECT * FROM ".$str_db_table_name_store_photo." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='LEFT' ORDER BY setasofferimage ASC";
                                        $rs_list_offerimages_left = GetRecordSet($str_query_select);
                                        //print $rs_list_offerimages->Count();
                                        if($rs_list_offerimages_left->Count() > 0) {
                                        ?>

                                        <img src="<?php print $str_img_path_store.$rs_list->Fields("pkid")."/".$rs_list_offerimages_left->Fields("thumbphotofilename"); ?>" alt="" title="" class="img-top-left"/>
                                        <?php }  ?>
                                        <?php 
                                        $str_query_select = "";
                                        $str_query_select = "SELECT * FROM ".$str_db_table_name_store_photo." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='RIGHT' ORDER BY setasofferimage ASC";
                                        $rs_list_offerimages_right = GetRecordSet($str_query_select);
                                        //print $rs_list_offerimages->Count();
                                        if($rs_list_offerimages_right->Count() > 0) {
                                        ?>
                                        <img src="<?php print $str_img_path_store.$rs_list->Fields("pkid")."/".$rs_list_offerimages_right->Fields("thumbphotofilename"); ?>" alt="" title="" class="img-top-right"/>
                                        <?php }  ?>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-12 product-content-bg">
                                        <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" title="<?php print $str_hover_view_details; ?>"><b class="text-default"><?php print MakeStringShort($rs_list->Fields("title"), 30); ?></b></a></p>
                                        <?php /* ?><p>Product Code : 000000</p><?php */ ?>
                                        <p>
                                            <?php if($str_user_type == "WHOLESALER" && $rs_list->Fields("memberprice") > 0) { ?>
                                                <?php $int_member_price = "";
                                                $int_member_price = $rs_list->Fields("memberprice") / $int_conversionrate; ?>

                                                    <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_member_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                                    
                                            <?php } else { ?>
                                            <?php if($rs_list->Fields("ourprice") > 0 && ($rs_list->Fields("ourprice") != $rs_list->Fields("listprice"))) {
                                                $int_list_price = "";
                                                $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate;

                                                $int_our_price = "";
                                                $int_our_price = $rs_list->Fields("ourprice") / $int_conversionrate;
                                                ?>
                                                <strike class="text-muted"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 0)."&nbsp;".$str_currency_shortform; ?></strike>&nbsp;&nbsp;
                                                <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_our_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                                <?php
                                                $int_per_discount = 0;
                                                $int_per_discount = (100 - ($rs_list->Fields("ourprice")/$rs_list->Fields("listprice")) * 100); 
                                                if($int_per_discount > 0) { ?>
                                                <label class="label label-primary"><b><?php print number_format($int_per_discount, 0); ?>% OFF</b></label> 
                                                <?php } ?>
                                            <?php } else {
                                                $int_list_price = "";
                                                $int_list_price = $rs_list->Fields("listprice") / $int_conversionrate; ?>

                                                    <b class="text-success"><?php print $str_currency_symbol."&nbsp;".number_format(ceil($int_list_price), 0)."&nbsp;".$str_currency_shortform; ?></b>&nbsp;
                                            <?php }  ?> 
                                        <?php }  ?> 

                                        </p>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="row padding-10">
                                            <div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                                <a href="./cart.html" type="button" class="product-button"><b><i class="fa fa-cart-plus" aria-hidden="true"></i></b></a> 
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list->Fields("pkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("cattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("subcattitle")))));?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list->Fields("title")))));?>" type="button" class="product-buynow-button"><b><i class="fa fa-shopping-cart" aria-hidden="true"></i></b></a> 
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4 nopadding">
                                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_unfavorite_p.php?itempkid=<?php print($rs_list->fields("itempkid")); ?>" onClick="return confirm_delete();" type="button" title="click to remove this item" class="product-button" onClick="return confirm_favourites();" data-toggle="" data-target="#"><i class="fa fa-remove"></i></a>
                                            </div> 
                                       </div>
                                    </div>
                                </div>

                            </div>                
                        </div>
                    <?php
                    $int_cnt++; 
                    if($int_cnt%2 == 0) { print "</div><div class='row padding-10 hidden-md hidden-lg hidden-sm'>"; } 

                    $rs_list->MoveNext();
                } $rs_list->MoveFirst(); ?>
                </div>
                <?php } ?>
                <!--------------------------------end favorite section-----------------------------------> 
            </div>
        </div>
    </div>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jquery.min.js"></script>
    <?php include($STR_USER_FOOTER_PATH); CloseConnection();?>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/bootstrap.min.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/js-plugin.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/main-sidebar.js" type="text/javascript"></script>
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/base.js" type="text/javascript"></script>
    
    <script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_favorite.js"></script>
</body>
</html>
