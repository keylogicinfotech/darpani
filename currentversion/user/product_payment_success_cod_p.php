<?php
/*
	Module Name	:-  product payment
	File Name	:- product_payment_success_p.php
	Create Date	:- 10-01-2017
	Initially Created by :- 015
*/
#------------------------------------------------------------------------------------------
#	include files
//	session_start();
include "./../user/product_validatesession.php";
include "./../includes/configuration.php";
include "./../includes/lib_data_access.php";
include "./../includes/lib_common.php";
//include "./../includes/lib_email.php";
include "./../includes/lib_xml.php";
include "./../user/product_config.php";
//include "./../includes/http_to_https.php";
#------------------------------------------------------------------------------------------

//print_r($_POST); exit;

$item_sessionid = "";
$item_uniqueid = "";
$int_paymentoptionpkid = 0;
$str_paymentoption_title = "";
# Use below code only when doing testing on LOCAL server - START
/*if(isset($_SESSION['sessionid']))
	{
		$item_sessionid=$_SESSION['sessionid'];
	}
	if(isset($_SESSION['uniqueid']))
	{
		$item_uniqueid=$_SESSION['uniqueid'];
	}*/
# Use below code only when doing testing on LOCAL server - END


# Use below code only when doing testing on ACTUAL server - START
if (isset($_POST["hdn_sessionid"]) && trim($_POST["hdn_sessionid"]) != "") {
        $item_sessionid = trim($_POST["hdn_sessionid"]);
}

if (isset($_POST["hdn_uniqueid"]) && trim($_POST["hdn_uniqueid"]) != "") {
        $item_uniqueid = trim($_POST["hdn_uniqueid"]);
}

if (isset($_POST["hdn_paymentoptionpkid"]) && trim($_POST["hdn_paymentoptionpkid"]) != "") {
        $int_paymentoptionpkid = trim($_POST["hdn_paymentoptionpkid"]);
}
if ($int_paymentoptionpkid > 0) {
        $str_query_select = "";
        $str_query_select = "SELECT title FROM " . $STR_DB_TABLE_NAME_PAYMENT_OPTION . " WHERE pkid=" . $int_paymentoptionpkid;
        $rs_list_paymentoption = GetRecordSet($str_query_select);
        $str_paymentoption_title = $rs_list_paymentoption->Fields("title");
}
// print $str_paymentoption_title; exit;
# Use below code only when doing testing on ACTUAL server - END

$int_order_total = 0;
if (isset($_POST["hdn_int_order_total"]) && trim($_POST["hdn_int_order_total"]) != "") {
        $int_order_total = trim($_POST["hdn_int_order_total"]);
}



$str_purchase_date = date("Y-m-d H:i:s");
$str_purchase_day = "";
$str_purchase_month = "";
$str_purchase_year = "";

/// find the day, month and year from the transaction date
$str_purchase_day = date("d", strtotime($str_purchase_date));
$str_purchase_month = date("m", strtotime($str_purchase_date));
$str_purchase_year = date("Y", strtotime($str_purchase_date));

$str_download = "YES";
$str_addedby = "YES";


#########	Get individual product data from t_sessioncart table
# This variable is used to store purchased items. So we can send mail to model
$str_query_select = "";
$str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME_SESSION_CART . " WHERE sessionid='" . $item_sessionid . "' AND uniqueid='" . $item_uniqueid . "'";
$rs_cart_list = GetRecordset($str_query_select);
$int_ps_quantity = $rs_cart_list->Count(); // No. of records fetched in the recordset


if ($rs_cart_list->eof() == false) {

        //print "hello";exit;
        /*$sel_buyer_info="";
		$sel_buyer_info="SELECT loginid,password FROM t_buyer";
		$rs_buyer_info = GetRecordSet($sel_buyer_info);
		
		if($rs_buyer_info->fields("loginid") == $str_email_id){
			$str_member_password = 	$rs_buyer_info->fields("password");
		}
		else{*/
        //$str_member_loginid = GenerateRandomString();
        //$str_member_password = Generate_Random_Password();
        //print "password:  ". $str_member_password; exit;
        //}	


        $str_cod_txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        $str_mailbody_product_list = "";
        $str_mailbody_shipping_address = "";
        //$int_extended_price = 0;
        $str_email = "";

        while (!$rs_cart_list->eof()) {
                //$price = $rs_cart_list->fields("price"); 

                //$str_reg_date=date("Y-m-d H:i:s");

                $str_query_insert = "";
                $str_query_insert = "INSERT INTO " . $STR_DB_TABLE_NAME_PURCHASE . "(userpkid, username, productpkid, catpkid,subcatpkid,paymentoptionpkid, paymentoptiontitle, memberpkid, membername, subscriptionid, ";
                $str_query_insert .= " referredbypkid, commission, wholesalerdiscount, producttitle,cattitle, subcattitle,itemcode, tailoringoptionpkid, tailoringoptiontitle, tailoringprice,tailoringmeasurements, color, size, type, sentdate, quantity, pricevalue, price,";
                $str_query_insert .= "extendedprice,giftcard, firstname, emailid, address, city, state, countrypkid, country,";
                $str_query_insert .= "zipcode, phoneno, shippingvalue, weight, promocodepkid, finaldiscountedamount, ";
                $str_query_insert .= "purchasedatetime,ipaddress,inmonth,inyear) VALUES (";
                $str_query_insert .= $rs_cart_list->fields("userpkid") . ",";
                $str_query_insert .= "'" . $rs_cart_list->fields("username") . "', ";
                $str_query_insert .= $rs_cart_list->fields("productpkid") . ", ";
                $str_query_insert .= $rs_cart_list->fields("catpkid") . ", ";
                $str_query_insert .= $rs_cart_list->fields("subcatpkid") . ", ";
                $str_query_insert .= $int_paymentoptionpkid . ", ";
                $str_query_insert .= "'" . $str_paymentoption_title . "', ";
                $str_query_insert .= $rs_cart_list->fields("memberpkid") . ", ";
                $str_query_insert .= "'" . $rs_cart_list->fields("membername") . "', ";
                $str_query_insert .= "'" . $str_cod_txnid . "', ";

                $str_query_insert .= $rs_cart_list->fields("referredbypkid") . ",";
                $str_query_insert .= $rs_cart_list->fields("commission") . ",";
                $str_query_insert .= $rs_cart_list->fields("wholesalerdiscount") . ",";

                $str_query_insert .= "'" . $rs_cart_list->fields("producttitle") . "', ";
                $str_query_insert .= "'" . $rs_cart_list->fields("cattitle") . "',";
                $str_query_insert .= "'" . $rs_cart_list->fields("subcattitle") . "',";
                $str_query_insert .= "'" . $rs_cart_list->fields("itemcode") . "',";

                $str_query_insert .= "" . $rs_cart_list->fields("tailoringoptionpkid") . ", ";
                $str_query_insert .= "'" . $rs_cart_list->fields("tailoringoptiontitle") . "',";
                $str_query_insert .= "" . $rs_cart_list->fields("tailoringprice") . ",";
                $str_query_insert .= "'" . $rs_cart_list->fields("tailoringmeasurements") . "',";

                $str_query_insert .= "'" . $rs_cart_list->fields("color") . "', ";
                $str_query_insert .= "'" . $rs_cart_list->fields("size") . "', ";
                $str_query_insert .= "'" . $rs_cart_list->fields("type") . "', ";
                $str_query_insert .= "'" . $rs_cart_list->fields("sentdate") . "', ";
                $str_query_insert .= $rs_cart_list->fields("quantity") . " ,";
                $str_query_insert .= "'" . $rs_cart_list->fields("price") . "', ";
                $str_query_insert .= $rs_cart_list->fields("price") . ", ";
                $str_query_insert .= $rs_cart_list->fields("extendedprice") . ", ";
                $str_query_insert .= $rs_cart_list->fields("giftcard") . ", ";
                //$str_query_insert.= "'".$str_trans_id."', ";
                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("name")) . "', ";
                //$str_query_insert.= "'".ReplaceQuote($str_lname)."', ";
                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("emailid")) . "', ";
                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("address")) . "',";
                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("city")) . "', ";
                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("state")) . "',";
                $str_query_insert .= "" . $rs_cart_list->fields("countrypkid") . ", ";
                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("country")) . "', ";
                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("zipcode")) . "', ";
                //$str_query_insert.= "'".ReplaceQuote($str_ship_add)."',";
                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("phone")) . "', ";
                $str_query_insert .= $rs_cart_list->fields("shippingvalue") . ", ";
                $str_query_insert .= $rs_cart_list->fields("weight") . ", ";
                $str_query_insert .= $rs_cart_list->fields("promocodepkid") . ", ";
                $str_query_insert .= $rs_cart_list->fields("finaldiscountedamount") . ", ";
                //$str_query_insert.= "'".ReplaceQuote($rs_cart_list->fields("shippingaddress"))."' , ";
                //$str_query_insert.= "'".$expire_date."', ";
                $str_query_insert .= "'" . $str_purchase_date . "', ";
                $str_query_insert .= "'" . $_SERVER['REMOTE_ADDR'] . "', ";
                $str_query_insert .= $str_purchase_month . ",";
                $str_query_insert .= $str_purchase_year . ")";
                //print $str_query_insert;exit;
                ExecuteQuery($str_query_insert);

                $str_tailoring_options_list = "";
                if ($rs_cart_list->fields("tailoringoptiontitle") != "") {
                        $str_tailoring_options_list = "";
                        if ($rs_cart_list->Fields("tailoringoptiontitle") != "") {
                                $str_tailoring_options_list .= " | Tailoring Option : " . $rs_cart_list->Fields("tailoringoptiontitle") . "";
                        }
                        if ($rs_cart_list->Fields("tailoringmeasurements") != "") {
                                $str_tailoring_options_list .= " | Tailoring Measurements : " . $rs_cart_list->Fields("tailoringmeasurements") . "<br/>";
                        }
                }

                $str_item_size = "";
                if ($rs_cart_list->Fields("size") != "") {
                        $str_item_size = " | Size : " . $rs_cart_list->Fields("size");
                }

                //$str_mailbody_product_list .= $rs_cart_list->Fields("producttitle")."<br/>";
                $str_mailbody_product_list .= "Item Name : " . $rs_cart_list->Fields("producttitle") . " | Itemcode : " . $rs_cart_list->Fields("itemcode") . " | Qty : " . $rs_cart_list->Fields("quantity") . " | Price : " . $rs_cart_list->Fields("price") . " | Color : " . $rs_cart_list->Fields("color") . $str_item_size . "" . $str_tailoring_options_list . " | Shipping Charge : " . $rs_cart_list->Fields("shippingvalue") . "<br/>";

                //$int_extended_price = $int_extended_price + $rs_cart_list->fields("extendedprice") + $rs_cart_list->fields("shippingvalue") + $rs_cart_list->fields("tailoringprice");
                $str_email = $rs_cart_list->fields("emailid");

                $str_mailbody_shipping_address = $rs_cart_list->Fields("address") . ", " . $rs_cart_list->Fields("city") . ", " . $rs_cart_list->Fields("zipcode") . ", " . $rs_cart_list->Fields("state") . ", " . $rs_cart_list->Fields("country") . "";
                $str_product_title = $rs_cart_list->fields("producttitle");
                $str_customer_name = $rs_cart_list->fields("name");
                $str_customer_email = $rs_cart_list->fields("emailid");
                $str_customer_phone = $rs_cart_list->fields("phone");
                $str_customer_address = $rs_cart_list->fields("address") . " " . $rs_cart_list->fields("city") . " " . $rs_cart_list->fields("zipcode") . " " . $rs_cart_list->fields("state") . " " . $rs_cart_list->fields("country");
                $str_price = $rs_cart_list->fields("price"); // for price of item
                $str_customer_ten_digit_number = substr($str_customer_phone, -10); // get last 10 digit from mobile number
                $rs_cart_list->MoveNext();
        }
        //exit;
        /*else
	{
		exit;
	}*/
        # Query to insert item list in t_product_purchase_temp2 table...
        //$str_query_insert_temp2="INSERT INTO t_product_purchase_temp2 (subid, sessionid, uniqueid, itemlist) VALUES ('".$str_trans_id."','".$item_sessionid."','".$item_uniqueid."','".str_replace("'", "|", $str_items_list)."')";
        //print $str_query_insert_temp2."<br/><br/>"; 
        //ExecuteQuery($str_query_insert_temp2);


        #Add Purchase Record In t_product_purchase_orderwise table
        /*$str_query_insert="INSERT INTO  t_product_purchase_orderwise (freememberpkid,quantity,extendedprice,purchasedatetime,ipaddress) VALUES (";
	$str_query_insert.="0,".$int_ps_quantity.",".$var_extprice.",'".ReplaceQuote($str_purchase_date)."','".ReplaceQuote($str_ipaddress)."')";
			
	print $str_query_insert."<BR><BR>"; exit;
	ExecuteQuery($str_query_insert);*/
        //print "Hello"; exit;
        #########	Once product purchase date stored in t_product_purchase table, DELETE those data from t_sessioncart table
        $str_query_delete = "DELETE FROM " . $STR_DB_TABLE_NAME_SESSION_CART . " WHERE sessionid='" . $item_sessionid . "' AND uniqueid='" . $item_uniqueid . "'";
        //	$str_query_delete="DELETE FROM t_sessioncart WHERE sessionid='".$item_sessionid. "' AND uniqueid='".$item_uniqueid."' AND freememberpkid=".$int_memberpkid;
        ExecuteQuery($str_query_delete);
        //exit;
}

// ============================== send sms to user mobile =============================================================
// die($STR_SITENAME_WITHOUT_PROTOCOL);
$msg = urlencode("RECEIVED: Your order id " . $str_cod_txnid . " for INR " . $str_price . " is confirmed. DARPANI will inform you when it ship. Thank you From: " . $STR_SITENAME_WITHOUT_PROTOCOL . " DARPNI");
// die($msg);
sms($str_customer_ten_digit_number, $msg); // call function which is write in lib_common.php


$fp = openXMLfile($STR_XML_FILE_PATH_MODULE . "siteconfiguration.xml");
$STR_SITE_URL = getTagValue("P_SITE_URL", $fp);
$str_from = getTagValue("P_SITE_EMAIL_ADDRESS", $fp);
$str_to = getTagValue("P_SITE_EMAIL_ADDRESS", $fp);
$str_to_buyer = $str_email;
closeXMLfile($fp);

$str_subject = "New purchase order placed on " . $STR_SITENAME_WITHOUT_PROTOCOL . "";
$mailbody = "Transaction ID for this order is." . $str_cod_txnid . "<br/><br/>";
$mailbody .= "<strong>Order Date : </strong> " . $str_purchase_date . "<br/>";
$mailbody .= "<strong>Ordered Items : </strong> " . $str_mailbody_product_list . "<br/>";
$mailbody .= "<strong>Payment Mode :</strong> COD (Cash On Delivery)<br/>";
$mailbody .= "<strong>Amount : </strong> " . $int_order_total . "<br/><br/>";
//$mailbody .= "<strong>Shipping Address : </strong> ".$str_mailbody_shipping_address."<br/>";
$mailbody .= "<strong>Customer Name : </strong> " . $str_customer_name . "<br/>";
$mailbody .= "<strong>Customer Email : </strong> " . $str_customer_email . "<br/>";
$mailbody .= "<strong>Customer Phone : </strong> " . $str_customer_phone . "<br/>";
$mailbody .= "<strong>Shipping Address : </strong> " . $str_customer_address . "<br/>";
$mailbody .= "<br/>Thank You,<br/>" . $STR_SITENAME_WITHOUT_PROTOCOL . "";

$str_subject_buyer = "Your purchase order placed on " . $STR_SITENAME_WITHOUT_PROTOCOL . "";
$mailbody_buyer = "Your Transaction ID for this order is." . $str_cod_txnid . ". Please keep this for any future reference.<br/><br/>";
$mailbody_buyer .= "<strong>Order Date : </strong> " . $str_purchase_date . "<br/>";
$mailbody_buyer .= "<strong>Ordered Items :</strong> " . $str_mailbody_product_list . "<br/>";
$mailbody_buyer .= "<strong>Payment Mode :</strong> COD (Cash On Delivery)<br/>";
$mailbody_buyer .= "<strong>Amount :</strong> " . $int_order_total . "<br/><br/>";
//$mailbody .= "<strong>Shipping Address : </strong> ".$str_mailbody_shipping_address."<br/>";
$mailbody_buyer .= "<strong>Customer Name : </strong> " . $str_customer_name . "<br/>";
$mailbody_buyer .= "<strong>Customer Email : </strong> " . $str_customer_email . "<br/>";
$mailbody_buyer .= "<strong>Customer Phone : </strong> " . $str_customer_phone . "<br/>";
$mailbody_buyer .= "<strong>Shipping Address : </strong> " . $str_customer_address . "<br/>";
//$mailbody_buyer .= "<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL."";
$mailbody_buyer .= "<br/><a href='" . $STR_SITENAME_WITHOUT_PROTOCOL . "/user/user_order_status_list.php' target='_blank'>Track Your Order</a>";

//print "<br/>ADMIN ".$str_from; print "<br/>".$str_to; print "<br/>".$mailbody."<br/>"; 
//print "<br/>BUYER ".$str_from; print "<br/>".$str_to_buyer; print "<br/>".$mailbody_buyer; 
//exit;

//sendmail($str_to,$str_subject,$mailbody,$str_from,1);
//sendmail($str_to_buyer,$str_subject_buyer,$mailbody_buyer,$str_from,1);

$headers .= "Reply-To: Darpani.com <" . $str_from . ">" . "\r\n";
$headers .= "Return-Path: Darpani.com <" . $str_from . ">" . "\r\n";
$headers .= "From: Darpani.com <" . $str_from . ">" . "\r\n";
//$headers .= "Cc: <".$str_from.">" . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
//$headers .= "X-Priority: 3\r\n";
//$headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;

mail($str_to, $str_subject, $mailbody, $headers);
mail($STR_COMPANY_EMAIL_ADDRESS, $str_subject, $mailbody, $headers);
//mail($str_to_buyer,$str_subject_buyer,$mailbody_buyer,$headers);
mail($str_to_buyer, $str_subject_buyer, $mailbody_buyer . $str_query_insert, $headers);

#------------------------------------------------------------------------------------------




//CloseConnection();
Redirect("./product_msg.php?pmode=cod&txnid=" . $str_cod_txnid);
//Redirect("member_home.php");
exit();
