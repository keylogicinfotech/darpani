<?php
/*
	Module Name	:-  product payment
	File Name	:- product_payment_success_p.php
	Create Date	:- 10-01-2017
	Initially Created by :- 015
*/
#------------------------------------------------------------------------------------------
#	include files
//	session_start();
include "./product_validatesession.php";
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_email.php";
include "../includes/lib_xml.php";
include "../includes/lib_datetimeyear.php";
include "./product_config.php";
//include "./../includes/http_to_https.php";
#------------------------------------------------------------------------------------------
//print_r($_POST); exit;

require_once("../PaytmKit/lib/config_paytm.php");
require_once("../PaytmKit/lib/encdec_paytm.php");

$item_sessionid = "";
if (isset($_SESSION['sessionid'])) {
        $item_sessionid = $_SESSION['sessionid'];
}

$item_uniqueid = "";
if (isset($_SESSION['uniqueid'])) {
        $item_uniqueid = $_SESSION['uniqueid'];
}
//print $_SESSION['sessionid']."<br/>".$_SESSION['uniqueid']; exit;

$str_query_select = "";
$str_query_select = "SELECT title FROM " . $STR_DB_TABLE_NAME_PAYMENT_OPTION . " WHERE pkid=4";
//print $str_query_select."<br/>";
$rs_list_paymentoption = GetRecordSet($str_query_select);
$str_paymentoption_title = $rs_list_paymentoption->Fields("title");
$int_paymentoptionpkid = 4;


$paytmChecksum = "";
$paramList = array();
$isValidChecksum = "FALSE";

$paramList = $_POST;

$str_txnid = "";
if (isset($_POST["ORDERID"]) && trim($_POST["ORDERID"]) != "") {
        $str_txnid = trim($_POST["ORDERID"]);
}

$int_final_subtotal = 0.00;
if (isset($_POST["TXNAMOUNT"]) && trim($_POST["TXNAMOUNT"]) != "") {
        $int_final_subtotal = trim($_POST["TXNAMOUNT"]);
}

$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg

//Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application�s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
$isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.

if ($isValidChecksum == "TRUE") {
        echo "<b>Checksum matched and following are the transaction details:</b>" . "<br/>";
        if ($_POST["STATUS"] == "TXN_SUCCESS") {
                //echo "<b>Transaction status is success</b>" . "<br/>";
                //Process your transaction here as success transaction.
                //Verify amount & order id received from Payment gateway with your application's order id and amount.

                $str_purchase_date = date("Y-m-d H:i:s");
                $str_purchase_day = "";
                $str_purchase_month = "";
                $str_purchase_year = "";

                /// find the day, month and year from the transaction date
                $str_purchase_day = date("d", strtotime($str_purchase_date));
                $str_purchase_month = date("m", strtotime($str_purchase_date));
                $str_purchase_year = date("Y", strtotime($str_purchase_date));

                $str_download = "YES";
                $str_addedby = "YES";

                #########	Get individual product data from t_sessioncart table
                # This variable is used to store purchased items. So we can send mail to model
                $str_query_select = "";
                //$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_SESSION_CART." WHERE sessionid='".$item_sessionid. "' AND uniqueid='".$item_uniqueid."'";
                $str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME_SESSION_CART . " WHERE validation01='" . $str_txnid . "'";
                //print $str_query_select."<br/><br/>";
                $rs_cart_list = GetRecordset($str_query_select);

                $int_ps_quantity = $rs_cart_list->Count(); // No. of records fetched in the recordset

                if ($rs_cart_list->eof() == false) {

                        //print "hello";exit;
                        /*$sel_buyer_info="";
                        $sel_buyer_info="SELECT loginid,password FROM t_buyer";
                        $rs_buyer_info = GetRecordSet($sel_buyer_info);

                        if($rs_buyer_info->fields("loginid") == $str_email_id){
                                $str_member_password = 	$rs_buyer_info->fields("password");
                        }
                        else{*/
                        //$str_member_loginid = GenerateRandomString();
                        //$str_member_password = Generate_Random_Password();
                        //print "password:  ". $str_member_password; exit;
                        //}	

                        $str_mailbody_product_list = "";
                        while (!$rs_cart_list->eof()) {
                                //$price = $rs_cart_list->fields("price"); 
                                //$str_reg_date=date("Y-m-d H:i:s");

                                $str_query_insert = "";
                                $str_query_insert = "INSERT INTO " . $STR_DB_TABLE_NAME_PURCHASE . "(userpkid, username, productpkid, catpkid,subcatpkid,paymentoptionpkid, paymentoptiontitle, memberpkid, membername, subscriptionid, ";
                                $str_query_insert .= " referredbypkid, commission, wholesalerdiscount, producttitle,cattitle, subcattitle,tailoringoptionpkid, tailoringoptiontitle, tailoringprice,tailoringmeasurements, color, size, type, sentdate, quantity, pricevalue, price,";
                                $str_query_insert .= "extendedprice,giftcard, firstname, emailid, address, city, state, countrypkid, country,";
                                $str_query_insert .= "zipcode, phoneno, shippingvalue, weight, promocodepkid, finaldiscountedamount, ";
                                $str_query_insert .= "purchasedatetime,ipaddress,inmonth,inyear, bankname, paymentmode ";
                                $str_query_insert .= " ) VALUES ( ";
                                $str_query_insert .= $rs_cart_list->fields("userpkid") . ",";
                                $str_query_insert .= "'" . $rs_cart_list->fields("username") . "', ";
                                $str_query_insert .= $rs_cart_list->fields("productpkid") . ", ";
                                $str_query_insert .= $rs_cart_list->fields("catpkid") . ", ";
                                $str_query_insert .= $rs_cart_list->fields("subcatpkid") . ", ";
                                $str_query_insert .= $int_paymentoptionpkid . ", ";
                                $str_query_insert .= "'" . $str_paymentoption_title . "', ";
                                $str_query_insert .= $rs_cart_list->fields("memberpkid") . ", ";
                                $str_query_insert .= "'" . $rs_cart_list->fields("membername") . "', ";
                                $str_query_insert .= "'" . $str_txnid . "', ";

                                $str_query_insert .= $rs_cart_list->fields("referredbypkid") . ",";
                                $str_query_insert .= $rs_cart_list->fields("commission") . ",";
                                $str_query_insert .= $rs_cart_list->fields("wholesalerdiscount") . ",";

                                $str_query_insert .= "'" . $rs_cart_list->fields("producttitle") . "', ";
                                $str_query_insert .= "'" . $rs_cart_list->fields("cattitle") . "',";
                                $str_query_insert .= "'" . $rs_cart_list->fields("subcattitle") . "',";

                                $str_query_insert .= "" . $rs_cart_list->fields("tailoringoptionpkid") . ", ";
                                $str_query_insert .= "'" . $rs_cart_list->fields("tailoringoptiontitle") . "',";
                                $str_query_insert .= "" . $rs_cart_list->fields("tailoringprice") . ",";
                                $str_query_insert .= "'" . $rs_cart_list->fields("tailoringmeasurements") . "',";

                                $str_query_insert .= "'" . $rs_cart_list->fields("color") . "', ";
                                $str_query_insert .= "'" . $rs_cart_list->fields("size") . "', ";
                                $str_query_insert .= "'" . $rs_cart_list->fields("type") . "', ";
                                $str_query_insert .= "'" . $rs_cart_list->fields("sentdate") . "', ";
                                $str_query_insert .= $rs_cart_list->fields("quantity") . " ,";
                                $str_query_insert .= "'" . $rs_cart_list->fields("price") . "', ";
                                $str_query_insert .= $rs_cart_list->fields("price") . ", ";
                                $str_query_insert .= $rs_cart_list->fields("extendedprice") . ", ";
                                $str_query_insert .= $rs_cart_list->fields("giftcard") . ", ";
                                //$str_query_insert.= "'".$str_trans_id."', ";
                                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("name")) . "', ";
                                //$str_query_insert.= "'".ReplaceQuote($str_lname)."', ";
                                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("emailid")) . "', ";
                                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("address")) . "',";
                                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("city")) . "', ";
                                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("state")) . "',";
                                $str_query_insert .= "" . $rs_cart_list->fields("countrypkid") . ", ";
                                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("country")) . "', ";
                                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("zipcode")) . "', ";
                                //$str_query_insert.= "'".ReplaceQuote($str_ship_add)."',";
                                $str_query_insert .= "'" . ReplaceQuote($rs_cart_list->fields("phone")) . "', ";
                                $str_query_insert .= $rs_cart_list->fields("shippingvalue") . ", ";
                                $str_query_insert .= $rs_cart_list->fields("weight") . ", ";
                                $str_query_insert .= $rs_cart_list->fields("promocodepkid") . ", ";
                                $str_query_insert .= $rs_cart_list->fields("finaldiscountedamount") . ", ";
                                //$str_query_insert.= "'".ReplaceQuote($rs_cart_list->fields("shippingaddress"))."' , ";
                                //$str_query_insert.= "'".$expire_date."', ";
                                $str_query_insert .= "'" . $str_purchase_date . "', ";
                                $str_query_insert .= "'" . $_SERVER['REMOTE_ADDR'] . "', ";
                                $str_query_insert .= $str_purchase_month . ",";
                                $str_query_insert .= $str_purchase_year . ", ";
                                $str_query_insert .= "'" . $_POST["BANKNAME"] . "',";
                                $str_query_insert .= "'" . $_POST["PAYMENTMODE"] . "' ";
                                $str_query_insert .= " )";

                                //print $str_query_insert; //exit;
                                ExecuteQuery($str_query_insert);

                                $str_mailbody_product_list .= $rs_cart_list->Fields("producttitle") . "|" . $rs_cart_list->Fields("color") . "|" . $rs_cart_list->Fields("size") . "|" . $rs_cart_list->Fields("type") . "|" . $rs_cart_list->Fields("quantity") . "|" . $rs_cart_list->Fields("price") . "<br/>";

                                $str_price = $rs_cart_list->fields("price"); // for price of item
                                $str_customer_ten_digit_number = substr($str_customer_phone, -10); // get last 10 digit from mobile number
                                $rs_cart_list->MoveNext();
                        }
                        //exit;
                        /*else
                    {
                            exit;
                    }*/
                        # Query to insert item list in t_product_purchase_temp2 table...
                        //$str_query_insert_temp2="INSERT INTO t_product_purchase_temp2 (subid, sessionid, uniqueid, itemlist) VALUES ('".$str_trans_id."','".$item_sessionid."','".$item_uniqueid."','".str_replace("'", "|", $str_items_list)."')";
                        //print $str_query_insert_temp2."<br/><br/>"; 
                        //ExecuteQuery($str_query_insert_temp2);


                        #Add Purchase Record In t_product_purchase_orderwise table
                        /*$str_query_insert="INSERT INTO  t_product_purchase_orderwise (freememberpkid,quantity,extendedprice,purchasedatetime,ipaddress) VALUES (";
                    $str_query_insert.="0,".$int_ps_quantity.",".$var_extprice.",'".ReplaceQuote($str_purchase_date)."','".ReplaceQuote($str_ipaddress)."')";

                    print $str_query_insert."<BR><BR>"; exit;
                    ExecuteQuery($str_query_insert);*/
                        //print "Hello"; exit;
                        #########	Once product purchase date stored in t_product_purchase table, DELETE those data from t_sessioncart table
                        // $str_query_delete="DELETE FROM t_sessioncart WHERE sessionid='".$item_sessionid. "' AND uniqueid='".$item_uniqueid."' AND freememberpkid=".$int_memberpkid;                    
                        // $str_query_delete="DELETE FROM ".$STR_DB_TABLE_NAME_SESSION_CART." WHERE sessionid='".$item_sessionid. "' AND uniqueid='".$item_uniqueid."'";
                        $str_query_delete = "DELETE FROM " . $STR_DB_TABLE_NAME_SESSION_CART . " WHERE validation01='" . $str_txnid . "'";
                        ExecuteQuery($str_query_delete);
                        //exit;
                }
                // ============================== send sms to user mobile =============================================================
                $msg = urlencode("RECEIVED: Your order id " . $str_txnid . " for INR " . $str_price . " is confirmed. DARPANI will inform you when it ship. Thank you From: " . $STR_SITENAME_WITHOUT_PROTOCOL . " DARPNI");
                sms($str_customer_ten_digit_number, $msg); // call function which is write in lib_common.php

                $fp = openXMLfile($STR_XML_FILE_PATH_MODULE . "siteconfiguration.xml");
                $STR_SITE_URL = getTagValue("P_SITE_URL", $fp);
                $str_from = getTagValue("P_SITE_EMAIL_ADDRESS", $fp);
                $str_to = getTagValue("P_SITE_EMAIL_ADDRESS", $fp);
                $str_to_buyer = $rs_cart_list->fields("emailid");
                closeXMLfile($fp);

                $str_subject = "New purchase order placed on " . $STR_SITENAME_WITHOUT_PROTOCOL . "";
                $mailbody = "Transaction ID for this order is." . $str_txnid . "<br/><br/>";
                $mailbody .= "<strong>Order Date : </strong> " . $str_purchase_date . "<br/>";
                $mailbody .= "<strong>Ordered Items : </strong><br/> " . $str_mailbody_product_list . "<br/>";
                $mailbody .= "<strong>Payment Mode :</strong> Paytm<br/>";
                $mailbody .= "<strong>Amount : </strong> " . $int_final_subtotal . "<br/>";
                $mailbody .= "<br/>Thank You,<br/>" . $STR_SITENAME_WITHOUT_PROTOCOL . "";

                $str_subject_buyer = "Your purchase order placed on " . $STR_SITENAME_WITHOUT_PROTOCOL . "";
                $mailbody_buyer = "Your Transaction ID for this order is." . $str_txnid . ". Please keep this for any future reference.<br/><br/>";
                $mailbody_buyer .= "<strong>Order Date : </strong> " . $str_purchase_date . "<br/>";
                $mailbody_buyer .= "<strong>Ordered Items :</strong><br/> " . $str_mailbody_product_list . "<br/>";
                $mailbody_buyer .= "<strong>Payment Mode :</strong> Paytm<br/>";
                $mailbody_buyer .= "<strong>Amount :</strong> " . $int_final_subtotal . "<br/><br/>";
                $mailbody_buyer .= "<br/>Thank You,<br/>" . $STR_SITENAME_WITHOUT_PROTOCOL . "";

                //print "<br/>".$str_from; print "<br/>".$str_to; print "<br/>".$mailbody; 
                //print "<br/>".$str_from; print "<br/>".$str_to_buyer; print "<br/>".$mailbody_buyer; 
                //exit;

                sendmail($str_to, $str_subject, $mailbody, $str_from, 1);
                sendmail($str_to_buyer, $str_subject_buyer, $mailbody_buyer, $str_from, 1);
        }
        /*else {
                    echo "<b>Transaction status is failure</b>" . "<br/>";
            }*/

        /*if (isset($_POST) && count($_POST)>0 )
            { 
                    foreach($_POST as $paramName => $paramValue) {
                                    echo "<br/>" . $paramName . " = " . $paramValue;
                    }
            }*/
} else {
        echo "<b>Checksum mismatched.</b>";
        //Process transaction as suspicious.
}

#------------------------------------------------------------------------------------------
CloseConnection();
Redirect("./product_msg.php?pmode=paytm");
//Redirect("member_home.php");
exit();
