<?php
#	Select query to get records from table
$str_query_select = "SELECT * FROM t_camprice";
$rs_camprice = GetRecordSet($str_query_select); 
/*****************************************************************************************************
* host_update API example, mirrors our Admin panel functions
* Connects to our gateway and updates host 
*****************************************************************************************************/
$host_id = '22414211133200';			//from you local db, you should have stored this when creating host
$new_username = 'RubyRenegade';			//include if updating host's username , 2-20 chars a-zA-Z0-9_-
$new_password = 'renegade123';			//include if updating host's password, 2-20 chars a-zA-Z0-9

//$host_id = $rs_seller->fields("host_id");			//from you local db, you should have stored this when creating host
//$new_username = '';			//include if updating host's username , 2-20 chars a-zA-Z0-9_-
//$new_password = '';			//include if updating host's password, 2-20 chars a-zA-Z0-9
/****************************************************************************************************/
include('./player4/api/php/global.php');
$params = 'action=host_update&account_id='.$account_id.'&gateway_pass='.$gateway_pass;
$params.= '&host_id='.$host_id;	
//$params.= $new_username ? '&new_username='.$new_username : '';
//$params.= $new_password ? '&pass='.$new_password : '';
$params.= '&max_time='.$rs_camprice->fields("freechattime");    //  3 minute free room timer
$params.= '&max_time_total='.$rs_camprice->fields("freechattime");   // 3 minutes per period below
$params.= '&max_time_total_period=1440';// 3 minutes per 24 hours allowed for free rooms

$params.= '&mode1=1';					// enable free chat
//$params.= '&mode2=1';					// enable group chat
//$params.= '&mode3=1';					// enable private chat
//$params.= '&enabled=1';					// enable host
//$params.= '&allow_guest=1';				// allow users to enter free chat with guestpass (requires guestpass to be on for your account)
//$params.= '&pay2=199';					// group pay per minute rate, 1.99 per minute
//$params.= '&pay3=299';					// private pay per minute rate, 2.99 per minute
$params.= '&pay2='.$rs_camprice->fields("groupchatprice");	// group pay per minute rate, 1.99 per minute
$params.= '&pay3='.$rs_camprice->fields("privatechatprice");	// private pay per minute rate, 2.99 per minute

//$params.= '&com2=50';					// group chat commission rate, 50%
//$params.= '&com3=60';					// private chat commission rate, 60%
//$params.= '&tip_com2=35';				// group chat tip commission rate, 35%
//$params.= '&tip_com3=55';				// private chat tip commission rate, 55%
//$params.= '&max_streams=5';				// max number of simultaneous free room viewers 
//$params.= '&view_user_bal=1';			// allow host to view member's balance via JustCamIt, 1=true, 0=false
//$params.= '&profile=1';					// allow host to update her profile via JustCamIt
//$params.= '&media=1';					// allow host to update, upload images via JustCamIt
//$params.= '&stats=1';					// allow host to view her stats via JustCamIt
//$params.= '&aim_user=test';				// aim username for aim buddy notification
//$params.= '&aim_pass=me';				// aim password for aim buddy notification	
//$params.= '&first_name=firstn';			// host first name
//$params.= '&last_name=lastn';			// host last name
//$params.= '&email=asdf@test.com';		// host email	
//$params.= '&pos_bias=5';				// camlist position bias , 0-100 with 0 being first in list
//$params.= '&allow_ban=0';				// allow host to ban (disable) user account via JustCamIt, not recommended
//$params.= '&aux_link1_url=www.1.com';	// these links are returned on a host_info_request api call
//$params.= '&aux_link1_txt=www111';
//$params.= '&aux_link2_url=http://easdf.com';
//$params.= '&aux_link2_txt=easdf';
//$params.= '&aux_link3_url=ww.cnn.com';
//$params.= '&aux_link3_txt=cnn';
//$params.= '&chat_only=0';				// text chat only no video, requires JustCamIt 1.1 and higher

$result = sendPost($gateway_ip,$params,true);//use ssl
//print_r($result);
if(stristr($result['message'],'ok:Host updated'))
{ 
	//host updated 
}
else if(stristr($result['message'],'error:'))
{ 
	// // error, handle it somehow, all errors start with "error:"
}
//print $params;
//print_r($result);	
?>