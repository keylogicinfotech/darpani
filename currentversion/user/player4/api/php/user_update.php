<?php
/*****************************************************************************************************
* user_update API example
* Connects to our gateway and updates user
*****************************************************************************************************/
$user_id = '22345644998693';//originally returned during the user_create api call and stored in your db			
/****************************************************************************************************/

include('global.php');

$params = '&action=user_update';
$params .= '&account_id='.$account_id;
$params .= '&gateway_pass='.$gateway_pass;
$params .= '&user_id='.$user_id;

// optional data
$params .= '&add_amount=20.00';				// add $20 to user's virtual wallet, -20.00 would decrease balance
//$params .= '&new_username=mynewusername';	//2-20 chars a-zA-Z0-9_-
//$params .= '&pass=mynewpassword';			//2-20 chars a-zA-Z0-9_-	
//$params .= '&first_name=john';
//$params .= '&last_name=doe';
//$params .= '&address1=1234 Hudson Lane';
//$params .= '&address2=Suite 11';
//$params .= '&city=San Diego';
//$params .= '&state=Ca';
//$params .= '&zip=92345';
//$params .= '&country=USA';
//$params .= '&email=johndoe@hotmail.com';

$result = sendPost($gateway_ip,$params,true);		//use ssl

if(stristr($result['message'],'ok:User Updated')){//good update
	
}else if(stristr($result['message'],'error:')){// // error, handle it somehow, all errors start with "error:"

}

print_r($result);	

?>