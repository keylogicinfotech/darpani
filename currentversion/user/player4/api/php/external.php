<?php
/*
 External Balance API:
 This feature allows you to provide our server with periodic approvals for pay per minute access.
 It consists of 3 stages, the Initial, Periodic, and Final. Our server will POST to your server a request
 which is based on one of these three stages. Your server will answer with a confirmation / amount or a denial.
 The POST to URLs must be entered in our Admin panel and External Balance must be enabled by us first. You will 
 find it under MyAccount->GatewayInfo.
 
 The POST string contains placeholder keywords that our server will replace with the actual value.
 Allowed keywords:
	USER_ID  		id returned during user_create API call
	SESSION_ID 		unique 16 char alpha numeric string for this ppm session, store this in your db in a SessionTBL
	IP 				IP address of the User
	HOST_ID 		id of the Host for this session
 	PPM 			pay per minute rate of this session in form 1.99 (1.99 credits per minute)
 	MODE 			2 = group mode, 3 = private mode
	TOTAL_TIME 		total time in 1.75 minutes format (105 seconds)
	AMOUNT			total amount in 1.99 format of the session 
	CHAT_ONLY		this session is in chat_only mode, no video
 
 Initial POST : 
 Starts prior to entering ppm video chat when the User opens a ppm session. Typical POST string in our Admin panel looks like:
 	http://mysite.com/gateway/external.php?action=initial&user_id=USER_ID&session_id=SESSION_ID&ip=IP&host_id=HOST_ID&ppm=PPM&mode=MODE
	

 Periodic POST :
 Continous during ppm session when the User's virtual wallet has fallen below the amount set in our Admin panel.
 Typcially is 60 seconds worth of time is the threshold. Typical POST string in our Admin panel looks like:
 	http://mysite.com/gateway/external.php?action=periodic&user_id=USER_ID&session_id=SESSION_ID&ip=IP&host_id=HOST_ID&ppm=PPM&mode=MODE
 
  
 Final POST :
 Completes the session as our server will POST this final amount of the total charge for this session. 
 Typical POST string in our Admin panel looks like:
 	http://mysite.com/gateway/external.php?action=final&user_id=USER_ID&session_id=SESSION_ID&ip=IP&host_id=HOST_ID&ppm=PPM&mode=MODE&total_time=TOTAL_TIME&amount=AMOUNT


********* Example Gateway for your server **************/
extract($_POST);

/////////// Initial ///////////////////
if($action == 'initial'){ 
	
	$user_has_money = true;
	
	if($user_has_money){
		echo('&balance=2.00'); 	// sets the User's virtual wallet to this initial amount
	}else{
		echo('&balance=0');		// don't let User into ppm
	}
	
////////// Periodic ////////////////////	
}else if($action == 'periodic'){
	
	$user_has_money = true;
	
	if($user_has_money){	
		echo('&add_to_balance=2.00');	// adds this amount to User's virutal wallet
	}else{
		echo('&add_to_balance=0');		// User has no more money, he will finish out session when virtual wallet drops to zero
	}

////////// Final ////////////////////////
}else if($action == 'final'){
	
	/// do processing like reconcile amount back to User's account in your database
	
	echo('&accepted=1');				// let our server know you have received the Final POST
										// otherwise we'll keep trying for 8 hours
}

///// Test Log /////////////////////////
if($action && ($fp = fopen('log.txt','a'))){
	fwrite($fp, date('m-d-Y H:i:s').' '.$action.' '.$session_id.' '.$user_id.' '.$ip.' '.$host_id.' '.$ppm.' '.$mode.' '.$total_time.' '.$amount.' '.$chat_only.chr(13).chr(10));
	fclose($fp);
}

?>