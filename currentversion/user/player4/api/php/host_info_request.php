<?php
/*****************************************************************************************************
* host_info_request API example, identical to code used to make the Bio page in our Player
* Connects to our gateway and retrieves data on host
*****************************************************************************************************/
$host_id = '22414211133200';			//from you local db, you should have stored this when creating host
/****************************************************************************************************/

include('global.php');

$params = 'action=host_info_request&account_id='.$account_id;
$params.= '&host_id='.$host_id;	
//$params.= '&host_name='.$host_name;	//can be used instead of host_id


$result = sendPost($gateway_ip,$params,false);//no ssl
	
if(isset($result['host_id'])){// request returned with no error

}else if(stristr($result['message'],'error:')){// // error, handle it somehow, all errors start with "error:"

}

print_r($result);	

?>