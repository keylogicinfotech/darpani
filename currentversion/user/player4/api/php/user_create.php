<?php
/*****************************************************************************************************
user_create API example
Connects to our gateway and creates user if not already in our database. Returns the user_id you store 
in your local database for future api calls.
Sets the four session cookies for the Player to function.
Users with a zero balance have a limited lifetime in our system, so always use this on login.

----------------------------
PHP / MySQL User Management :
If your site already has an existing user management system utilizing PHP / MySQL then integrate this script
into your login.php page right after the user:pass of the member has been verified with your local database. 
Because the Player relies on session cookies to function, you will need to have this script inserted before 
the <html> tag and above any server side scripting that prints to the browser.

-----------------------------
.htaccess User Management (CCBill account with monthly recurring billing)
1. Place the Player folder into your .htaccess protected member's folder
2. Point your Login button to this script
3. Edit global.php with your account info.
4. Edit global.php and set $use_htaccess = true;
5. Edit global.php and change $htaccess_member_homepage to point to the member homepage. (optional)

*****************************************************************************************************/
$member_user = $strmemberid;				//user you want to create, 2-20 chars a-zA-Z0-9_-, get from $_POST[]
$member_pass = $strpassword;			//password for this user, 2-20 chars a-zA-Z0-9_-, get from $_POST[], non MD5
/****************************************************************************************************/

include('global.php');
$setCookies = false;
$error = '';

if($use_htaccess){
	$member_user = $_SERVER['REMOTE_USER'];
	//create 8 character random password for istreaming database for this session a-zA-Z0-9
	for($len=8,$member_pass='';strlen($member_pass)<$len;$member_pass.=chr(!mt_rand(0,2)?mt_rand(48,57):(!mt_rand(0,1)?mt_rand(65,90):mt_rand(97,122))));
}

$params = '&action=user_info_request';
$params .= '&account_id='.$account_id;
$params .= '&gateway_pass='.$gateway_pass;
$params .= '&user='.$member_user;
$result = sendPost($gateway_ip,$params,true);//use ssl	

// Check if user already is in our database
if(stristr($result['message'],'error:Username Not Found')){
	// Not found so create this user
	$params = '&action=user_create';
	$params .= '&account_id='.$account_id;
	$params .= '&gateway_pass='.$gateway_pass;
	$params .= '&user='.$member_user;
	$params .= '&pass='.$member_pass;
	
	// optional data	
	//$params .= '&first_name=john';
	//$params .= '&last_name=doe';
	//$params .= '&address1=1234 Hudson Lane';
	//$params .= '&address2=Suite 11';
	//$params .= '&city=San Diego';
	//$params .= '&state=Ca';
	//$params .= '&zip=92345';
	//$params .= '&country=USA';
	//$params .= '&email=johndoe@hotmail.com';
			
	$result = sendPost($gateway_ip,$params,true);//use ssl
	
	if(stristr($result['message'],'ok:User Created')){//user created
		$setCookies = true;
		$user_id = $result['user_id'];//store in your local db for future use

	}else{
		$error = 'error_creating_user';
	}

// Check user and pass 
}else if($result['user'] == $member_user){
	if($result['pass'] == $member_pass){
		$setCookies = true;//already in our database
	
	}else{//password needs updating
		$params = '&action=user_update';
		$params .= '&account_id='.$account_id;
		$params .= '&gateway_pass='.$gateway_pass;
		$params .= '&user='.$member_user;
		$params .= '&pass='.$member_pass;
		$result = sendPost($gateway_ip,$params,true);		//use ssl
		
		if(stristr($result['message'],'ok:User Updated')){
			$setCookies = true;
		}else{
			$error = 'error_updating_user';
		}
	}	
}else{
	$error = 'gateway_error';
}

if($setCookies){
	setcookie('account_id',$account_id,0,'/');
	setcookie('gateway_ip',$gateway_ip,0,'/');
	setcookie('member_user',$member_user,0,'/');
	setcookie('member_pass',$member_pass,0,'/');
	
	if(isset($user_id)){
		//// Store user_id in your local db for future user api calls (user_info_request & user_update accept user_id)		
	}	

}else{// error, handle somehow
	//print $error;
}

//// .htaccess user management
if($use_htaccess){
	if($error)
		$error = '?error='.$error;
		
	header('Location:'.$htaccess_member_homepage.$error);// bounce to member's home page
}
?>