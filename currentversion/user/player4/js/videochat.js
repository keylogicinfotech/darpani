/*
This code has been written to work around many browser issues, alter at your own risk

Compatible Browsers in decending order of player mode preference:

Flash H.264 mode (All features work) ***********************
PC - IE, Firefox, Opera, AOL, Netscape, Safari, Chrome
Mac - Firefox, Opera, Safari
Linux - Firefox, Mozilla, SeaMonkey 

Silverlight 3 enhanced  mode *******************************
PC - IE6+, FireFox, Opera
Mac - Safari, FireFox

WMP plugin mode *********************************************
PC - IE
Mac - Safari, FireFox, requires Flip4Mac Quicktime plugin


Note: (Silverlight & WMP mode) 
FullScreen on PC only, zoom feature does not work on Opera, FireFox(pc) needs new WMP plugin or older Mozilla activex wrapper to zoom
Firefox users should use the IE Tab extension for best video results during zoom
*/
var account_id = new SessionVar("account_id",'');
var gateway_ip = new SessionVar("gateway_ip",'');
var member_user = new SessionVar("member_user",'');
var member_pass = new SessionVar("member_pass",'');
var mode_request = new SessionVar("mode_request",'');
var host_name = new SessionVar("host_name",'');

var zoomIndex = new SessionVar("zoomIndex",-1);
var volumeSession = new SessionVar("volumeSession",-1);
var chatWidthMinimum = new SessionVar("chatWidthMinimum",0);
var chatWidthNominal = new SessionVar("chatWidthNominal",0);
var chatWindowMode = new SessionVar("chatWindowMode",-1);
var hideChat = new SessionVar("hideChat",-1);
var port = new SessionVar("port",3000);//default port is 3000
var alerts = new SessionVar("alerts",-1);//keep track if user turned off alerts
var bypassPPMconfirm = new SessionVar("bypassPPMconfirm",0);//show pop up in flash for user to confirm he understands the PPM price

var videoWidth;
var videoHeight;
var clientWidth, clientHeight;
var mocxPlugin;//original mozilla activex ff plugin
var configXML;
var updateSizeTimerID = null;
var lightBoxOpacity;
var zoomArray = Array();
var mutedVolume = -1;//not muted
var volume = 0;
var mouseStartX = 0;
var volumeStartX = 0;
var dragging = false;
var sliderTrackLeft = 0;

var containerCSS;
var mplayerCSS;
var zoomBarCSS;
var zoomImgCSS;
var fullscreenCSS;
var muteCSS;
var sliderCSS;
var sliderKnobCSS;
var fchatCSS;

var agt = navigator.userAgent.toLowerCase();
var ie = ((agt.indexOf("msie")!=-1) && (agt.indexOf("opera")==-1));
var ff = (agt.indexOf("firefox")!=-1);
var mac = (agt.indexOf("mac")!=-1);
var safari = (agt.indexOf("safari")!=-1);
var windows = (agt.indexOf("windows")!=-1);
var opera = (agt.indexOf("opera")!=-1);
var allowSilverlight = false;
var zoomEvent = false;
var allowH264 = false;
var initialZoom = true;
var chatOnly = false;

/*
loadConfig()
It all starts here, parse the config.xml file and build the player 
*/
function loadConfig()
{
	parseURLquery();// check for host_name on query string if direct link
	
	loadXML("./player4/config/config.xml",configCallBack,false);//non async
	function configCallBack(xmlDoc)
	{
		configXML = xmlDoc
		chatWidthMinimum.set(getXMLvalue(configXML,"chatWidthMinimum","value"));	
		chatWidthNominal.set(getXMLvalue(configXML,"chatWidthNominal","value"));
		lightBoxOpacity = parseInt(getXMLvalue(configXML,"lightBoxOpacity","value"));
		bypassPPMconfirm.set(getXMLvalue(configXML,"bypassPPMconfirm","value"));
		
		if(chatWindowMode.geti() == -1)
			chatWindowMode.set(getXMLvalue(configXML,"chatWindowMode","value"));//1=lightbox, 2=pop up or tabbed 
		
		if(hideChat.geti() == -1)//not be set by an optional cookie
			hideChat.set(getXMLvalue(configXML,"hideChat","value"));//use config file setting
		
		if(zoomIndex.geti() == -1)
			zoomIndex.set(getXMLvalue(configXML,"zoomIndexDefault","value"));
		
		if(volumeSession.geti() == -1)
			volume = parseInt(getXMLvalue(configXML,"initialVolume","value"));//no previous volume setting, take initial
		else
			volume = volumeSession.geti();
	
		var sizes = getXMLvalue(configXML,"zoomSizes","value");
		var tmpZoomSizes = sizes.split(",");
		
		for(var x=0;x<tmpZoomSizes.length;x++)
			zoomArray.push(parseFloat(tmpZoomSizes[x]));

		containerCSS = getCSS("container");	
		mplayerCSS = getCSS("mplayer");
		zoomBarCSS = getCSS("zoomBar");
		zoomImgCSS = getCSS("zoom");
		fullscreenCSS = getCSS("fullscreen");
		muteCSS = getCSS("mute");
		sliderCSS = getCSS("slider");
		sliderKnobCSS = getCSS("sliderKnob");
		fchatCSS = getCSS("fchat");
		
		allowSilverlight = getXMLvalue(configXML,"allowSilverlight","value") == "1" ? true:false;//get config value
		allowSilverlight &= Silverlight.isInstalled("2.0.31005");		// AND it with browser plugin status, silverlight 2.0+ needed for this player
		
		if(window.addEventListener){ // ff
			parent.addEventListener('resize',updateSize,false);
		} else { // ie
			parent.attachEvent('onresize',updateSize);
		}				
		loadFlash();
	}	
}

function loadFlash()
{
	var url = "./player4/flash/preloader.swf?account_id=" + account_id.get();
	url += "&user=" + member_user.get();
	url += "&pass=" + member_pass.get();
	
	var gatewayIP = gateway_ip.get();
	if(gatewayIP.indexOf("http://") == -1)
		gatewayIP = "http://"+gatewayIP;
	if(gatewayIP.indexOf("/gateway") == -1)
		gatewayIP += "/gateway/";
	
	url += "&gateway_ip=" + gatewayIP;
	url += "&host_name=" + host_name.get();
	url += "&mode_request=" + mode_request.get();
	url += "&port=" + port.get();
	url += "&alerts=" + alerts.get();
	url += "&bypass=" + bypassPPMconfirm.get();
	url += "&chat_swf=./player4/flash/chat.swf";
	url += "&config_file=./player4/config/flash.xml";
	
	videoWidth = 320;//initial setting
	videoHeight = 240;//initial setting
	zoom(zoomIndex.geti());//init
	
	$("fchat").innerHTML = ' \
	<object id="fplayer" name="fplayer" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" \
	codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="100%" height="100%"> \
	<param name="movie" value="'+ url +'"> \
	<param name="quality" value="high"> \
	<param name="loop" value="false"> \
	<param name="scale" value="exactfit"> \
	<param name="allowscriptaccess" value="always"> \
	<param name="allowfullscreen" value="true"> \
	<param name="bgcolor" value="#FFFFFF"> \
	<embed name="fplayer" src="'+ url +'" width="100%" height="100%" loop="false" type="application/x-shockwave-flash" \
	pluginspage="http://www.macromedia.com/go/getflashplayer" bgcolor="#FFFFFF" quality="high" scale="exactfit" swLiveConnect="true" allowScriptAccess="always" allowFullScreen="true"></embed> \
	</object>';	
	
	//// make player visible now
	if(chatWindowMode.geti()==1){//lightbox mode
		resizeMask();
		parent.document.getElementById("playerDiv").style.visibility = "visible";
		var player2Obj = $("playerDiv2");//this div is along for the ride in this mode
		if(player2Obj){
			player2Obj.style.visibility = "visible";
			player2Obj.style.border = "0px";//kill border since the floating outer div will already have one when chatWindowMode=1
		}	
	}else if(chatWindowMode.geti() == 2){
		parent.document.getElementById("playerDiv2").style.visibility = "visible";
	}
}

/*
StartMediaPlayer(url)
Called by the Flash player once it receives the host's url from the gateway
*/
function StartMediaPlayer(url)
{	
	if(chatOnly){
		if(initialZoom){//no need to zoom a second time when going from free to ppm, fixes screen flashing
			zoom(zoomIndex.geti());
			initialZoom = false;			
		}
		return;
	}
	if(allowH264){
		if(initialZoom){//no need to zoom a second time when going from free to ppm, fixes screen flashing
			zoom(zoomIndex.geti());
			initialZoom = false;
		}
		
		createFlashH264(url);		
		return;
	}
	
	if(allowSilverlight){
		if(initialZoom){//no need to zoom a second time when going from free to ppm, fixes screen flashing
			zoom(zoomIndex.geti());
			initialZoom = false;
		}
			
		setTimeout("createSilverlight('"+ url +"')",100);//let screen catch up before init silverlight
		return;
	}
	
	//// continue with good old wmp plugin
	mocxPlugin = false;//original mozilla activex ff plugin
	var mswmpPlugin = false;//new microsoft ff plugin
	var flip4macPlugin = false;//qt plugin for mac safari, ff	
	
	$("leftDiv").style.backgroundColor = "transparent";
	$("mplayer").style.visibility = "visible";
	$("zoomBar").style.visibility = "visible";
	
	if(!ie){
		var len = navigator.plugins.length;
		for(var x=0;x<len;x++){
			if(navigator.plugins[x].name.indexOf("Flip4Mac") != -1){
				url = "mms" + url.substr(4);//flip4mac only likes mms://		
				flip4macPlugin = true;
				break;
			}
			if(!ff)
				continue;
			var sublen = navigator.plugins[x].length;
			for(var y=0;y<sublen;y++){					
				if(navigator.plugins[x][y].type.indexOf("application/x-ms-wmp") != -1){
					mswmpPlugin = true;//they have the new microsoft wmp firefox plugin, shows np-mswmp in description in about:plugins
				}
				if(navigator.plugins[x][y].type.indexOf("application/x-oleobject") != -1){
					mocxPlugin = true;//they have the original mozilla activex plugin
				}
			}			
		}			
	}
	
	var classidType;
	var autostart;
	var windowlessvideo;
	var forceEmbedTag = false;

	if(opera || (ff && !mswmpPlugin && !mocxPlugin && !mac) )
		forceEmbedTag = true;
	if(ie || mocxPlugin || forceEmbedTag){
		classidType = 'classid="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6"';
		autostart = 'false';
		windowlessvideo = 'false';
		
	}else if(mswmpPlugin){//firefox with new wmp plugin															
		classidType = 'type="application/x-ms-wmp"';
		autostart = 'true';
		windowlessvideo = 'true';
	}else{
		classidType = 'type="application/x-mplayer2"';
		autostart = 'true';
		windowlessvideo = 'false';
	}		
	
	var mpStr = '<object id="MediaPlayer" '+ classidType +'  width="'+ videoWidth +'" height="'+ videoHeight +'">';
	mpStr += '<param name="URL" value="'+url+'" />';
	mpStr += '<param name="src" value="'+url+'" />';
	mpStr += '<param name="autoStart" value="'+autostart+'" />';
	mpStr += '<param name="uiMode" value="none" />';
	mpStr += '<param name="showcontrols" value="false" />';
	mpStr += '<param name="showtracker" value="false" />';
	mpStr += '<param name="showstatusbar" value="false" />';
	mpStr += '<param name="stretchToFit" value="true" />';
	mpStr += '<param name="volume" value="'+volume+'"/>';
	mpStr += '<param name="windowlessVideo" value="'+windowlessvideo+'" />';
	if(forceEmbedTag){//problem browser support that work better with the old embed tag... for now
		mpStr += '<embed type="application/x-mplayer2" name="MediaPlayer" src="' + url + '" width="'+ videoWidth +'" height="'+ videoHeight +'" ';	
		mpStr += 'autoStart="1" showcontrols="0" showtracker="0 showstatusbar="0" stretchToFit="1" bufferingtime="1" volume="'+volume+'"></embed>';
	}	
	mpStr +='</object>';
	$("mplayer").innerHTML = mpStr;

	var mp = $("MediaPlayer");				
	if(ie || mocxPlugin){	//ie or mozilla activex plugin (ff users with older activex wrapper)
		mp.network.bufferingTime = 1000;// set to 1 second, although it seems the min is 2 seconds
		mp.controls.play();//now start the feed after we set the buffer time
	}

	if(initialZoom){//no need to zoom a second time when going from free to ppm, fixes screen flashing
		zoom(zoomIndex.geti());
		initialZoom = false;
	}
		
	$("fplayer").focus();
}
function fvideo_DoFSCommand(command,args)// callback from fvideo flash
{
	if(command == "zoom"){
		onZoom();
	}else if(command == "volume"){
		volumeSession.set(args);	//0 - 100
	}
}

function fplayer_DoFSCommand(command, args)//callback from flash chat, video size and silverlight capable, before StartMediaPlayer()
{
	if(command == "hostVideoInfo"){//wxh sl_capable video_codec
		var pieces = args.split(" ");
		var sepIndex = pieces[0].indexOf("x");//320x240, wxh
		videoWidth = pieces[0].substr(0,sepIndex);
		videoHeight = pieces[0].substr(sepIndex+1);
		allowSilverlight &= pieces[1] == "1" ? true:false;//AND it with current config permission and browser plugin status
		allowH264 = pieces[2] == "H264" ? true:false;
		chatOnly = pieces[3] == "1" ? true:false;
		
		if(chatOnly){//text chat mode
			$("leftDiv").innerHTML = "";
			
		}else if(allowH264){//host is using h.264 video
			$("leftDiv").innerHTML = "";
			$("leftDiv").style.backgroundColor = getXMLvalue(configXML,"videoBackgroundColor","value");//tmp color while fl loads
		
		}else if(allowSilverlight){//Use Silverlight if allowed and host is using wmv/wma enocder
			$("leftDiv").innerHTML = "";
			$("leftDiv").style.backgroundColor = getXMLvalue(configXML,"videoBackgroundColor","value");//tmp color while sl loads
		
		}else{//default to wmp plugin
		
			var obj;
			if(obj=$("zoom")){		
				obj.className = "zoom";
				obj.onclick = onZoom;
				obj.title = getXMLvalue(configXML,"zoomToolTip","value");
			}
			if(obj=$("fullscreen")){
				obj.className = "fullscreen";
				obj.onclick = function(){try{$("MediaPlayer").fullScreen=true;}catch(e){}}
				obj.title = getXMLvalue(configXML,"fullscreenToolTip","value");
			}
			if(obj=$("mute")){
				obj.className = "mute";
				obj.onclick = function(){if(mutedVolume > -1){volume=mutedVolume;adjustVolume();mutedVolume=-1;}else{mutedVolume=volume;volume=0;adjustVolume();}}
				obj.title = getXMLvalue(configXML,"muteToolTip","value");
			
			}
			if(obj=$("sliderKnob")){
				obj.onmousedown = function(){mouseStartX = 0;$("sliderKnob").onmousemove = dragVolume;}
				obj.onmouseup = function(){$("sliderKnob").onmousemove = null; volumeSession.set(volume);}
				obj.onmouseout = function(){$("sliderKnob").onmousemove = null; volumeSession.set(volume);}
				obj.title = getXMLvalue(configXML,"volumeToolTip","value");
				adjustVolume();
			}
	
		}	
	}else if(command == "port"){//port probe found this port so use it next time to speed up connection
		port.set(args);
	}else if(command == "alerts"){//keep track of alerts settings in flash with session cookie
		alerts.set(args);
	}
}

function message(mess)//message callback from flash chat
{	
	try{$("fchat").innerHTML = ""}catch(e){}
	try{$("mplayer").style.visibility = "hidden"}catch(e){}
	RemoveMediaPlayer();
	
	var xmlTagName = "msg" + mess;
	var messageStr = getXMLvalue(configXML,xmlTagName,"value");	
	var containerH = $("container").style.height;	
 	var messStr = '<div class="messages"><span style="line-height:' + containerH + '" >' + messageStr + '</span></div>'
	$("container").innerHTML = messStr;	
}

function RemoveMediaPlayer()
{
	try{$("mplayer").innerHTML = ""}catch(e){try{$("leftDiv").innerHTML=""}catch(e){}}
}

function delayedZoom(size,trigCnt)
{
	if(trigCnt == 1){
		$("mplayer").style.width = size + "px";
		setTimeout("delayedZoom("+(size+1)+",0)",1);
	}else{
		$("mplayer").style.width = size + "px";
	}
}

function zoom(index)
{
	var sizeMultiplier = zoomArray[index];//1 to 4, but size 2 could point to multiplier 1.5
	var mp = $("MediaPlayer");
	var doc = window.parent.document;
	var playerDiv = chatWindowMode.geti() == 1 ? "playerDiv" : "playerDiv2";
	
	var playerDivPBMwidth = getPaddingBorderMargin(playerDiv,"w",parent);//outer most div 
	var playerDivPBMheight = getPaddingBorderMargin(playerDiv,"h",parent);
	
	var containerPBMwidth = containerCSS.marginLeft + containerCSS.borderLeftWidth + containerCSS.paddingLeft + containerCSS.marginRight + containerCSS.borderRightWidth + containerCSS.paddingRight;  
	var containerPBMheight = containerCSS.marginTop + containerCSS.borderTopWidth + containerCSS.paddingTop + containerCSS.marginBottom + containerCSS.borderBottomWidth + containerCSS.paddingBottom;
	
	var videoPBMwidth = mplayerCSS.marginLeft + mplayerCSS.borderLeftWidth + mplayerCSS.paddingLeft + mplayerCSS.marginRight + mplayerCSS.borderRightWidth + mplayerCSS.paddingRight;
	var videoPBMheight = mplayerCSS.marginTop + mplayerCSS.borderTopWidth + mplayerCSS.paddingTop + mplayerCSS.marginBottom + mplayerCSS.borderBottomWidth + mplayerCSS.paddingBottom;
	
	var chatPBMwidth = fchatCSS.marginLeft + fchatCSS.borderLeftWidth + fchatCSS.paddingLeft + fchatCSS.marginRight + fchatCSS.borderRightWidth + fchatCSS.paddingRight;
	var chatPBMheight = fchatCSS.marginTop + fchatCSS.borderTopWidth + fchatCSS.paddingTop + fchatCSS.marginBottom + fchatCSS.borderBottomWidth + fchatCSS.paddingBottom;
	
	var zoomPBMwidth = zoomBarCSS.marginLeft + zoomBarCSS.borderLeftWidth + zoomBarCSS.paddingLeft + zoomBarCSS.marginRight + zoomBarCSS.borderRightWidth + zoomBarCSS.paddingRight;
	var zoomBarHeight = zoomBarCSS.marginTop + zoomBarCSS.borderTopWidth + zoomBarCSS.paddingTop + zoomBarCSS.height + zoomBarCSS.marginBottom + zoomBarCSS.borderBottomWidth + zoomBarCSS.paddingBottom;
	
	var videoW = videoWidth * sizeMultiplier;
	var videoH = videoHeight * sizeMultiplier;
	var chatW = chatWidthNominal.geti() - index - chatPBMwidth;//make sure flash stage event is triggered by using size as a +- pixel between resizes
	var chatH = videoH + videoPBMheight + zoomBarHeight - chatPBMheight;
	if(hideChat.geti()==1){
		chatW = 0;
		chatH = 0;
		chatPBMwidth = 0;
		chatPBMheight = 0;
		chatWidthMinimum.set(0);
		chatWidthNominal.set(0);
	}
	
	if(chatOnly){
		chatW += (videoW + videoPBMwidth);//expand to fill space where video was 
		videoW = 0;
		videoPBMwidth = 0;	
		$("fchat").style.marginLeft = Math.round(fchatCSS.marginLeft/2) + "px";
	}	
	
	var headerH = getPaddingBorderMargin("header","h");
	headerH += parseInt(getStyle("header","height"));
	
	if(headerH > 0){
		$("header").innerHTML = '<div id="logoDiv"></div><div class="closeButton" onClick="closePlayer()" onMouseOver="this.className=\'closeButtonOver\'" onMouseOut="this.className=\'closeButton\'"></div>';
	}
	var containerW, containerH, playerDivW, playerDivH, left, top;
	top = parseInt(getXMLvalue(configXML,"floatingPlayerDivY","value"));
	
	clientWidth = (doc.documentElement && doc.documentElement.clientWidth) ? doc.documentElement.clientWidth : parent.innerWidth;
	clientHeight = (doc.documentElement && doc.documentElement.clientHeight) ? doc.documentElement.clientHeight : parent.innerHeight;
	
	for(var x=0;x<800;x++){//adjust player to fit in browser window without scrollbars
		containerW = videoW + videoPBMwidth + chatW + chatPBMwidth;
		containerH = videoH + videoPBMheight + zoomBarHeight;
		
		playerDivW = containerW;
		playerDivH = headerH + containerH;
		
		playerDivW += containerPBMwidth;
		playerDivH += containerPBMheight;
		
		playerDivW += playerDivPBMwidth;//add to for test
		playerDivH += playerDivPBMheight;
		//test to make sure player < browser window
		if(clientWidth > playerDivW && clientHeight > playerDivH){
			break;
		}else{
			if(zoomEvent)//if manual click, not needed for window resize events
				index = zoomArray.length - 1;	//hitting window boundaries so reset zoom index to lowest zoom for next onZoom call		
			
			if(playerDivW > clientWidth){
				chatW-=1;
				
				if(chatW < chatWidthMinimum.geti()){
					if(!chatOnly)
						chatW = chatWidthMinimum.geti();						
					
					videoW-=1;
					videoH = Math.round(videoW * (videoHeight/videoWidth));
					chatH = videoH + videoPBMheight + zoomBarHeight - chatPBMheight;					
				}
			}
			
			if(playerDivH > clientHeight){
				videoH-=1;
				if(!chatOnly)
					videoW = Math.round(videoH * (videoWidth/videoHeight));
					
				chatH = videoH + videoPBMheight + zoomBarHeight - chatPBMheight;
			}
		}
	}
	
	left = Math.round((clientWidth/2) - (playerDivW/2));// center player horizontally
	if(playerDivH + top > clientHeight){	//need to center player vertically
		top = Math.round((clientHeight/2) - (playerDivH/2));
	}
	
	if(hideChat.geti()==1){
		chatW = 0;
		chatH = 0;
	}
	
	playerDivW -= playerDivPBMwidth;//remove after test
	playerDivH -= playerDivPBMheight;
	/// set sizes of everything
	var playerDiv = doc.getElementById(playerDiv);//this is outer most div
	playerDiv.style.left = left + "px";
	playerDiv.style.top = top + "px";
	playerDiv.style.width = playerDivW + "px";
	playerDiv.style.height = playerDivH + "px";
	
	var playerFrame = doc.getElementById("playerFrame");//iframe
	if(playerFrame){
		playerFrame.style.width = playerDivW + "px";
		playerFrame.style.overflow = "visible";
		playerFrame.style.height = playerDivH + "px";
	}
	$("header").style.width = playerDivW + "px";
	$("container").style.width = containerW + "px";
	$("container").style.height = containerH + "px";
	$("leftDiv").style.height = (containerH) + "px";
	$("leftDiv").style.width = (videoW + videoPBMwidth) + "px";		
	
	if(!allowSilverlight && $("mplayer")){
		$("mplayer").style.width = videoW + "px";
		$("mplayer").style.height = videoH + "px";
		$("zoomBar").style.width = (videoW + videoPBMwidth - zoomPBMwidth) + "px";
	}
	
	$("fchat").style.width = chatW + "px";
	$("fchat").style.height = chatH + "px";		
	
	if(mp){
		mp.style.width = videoW + "px";
		mp.style.height = videoH + "px";
	}
	
	if(mocxPlugin)
		setTimeout("delayedZoom("+ (videoW-1) +",1)",20);//trigger older ff mozilla activex wrapper to zoom
	
	zoomIndex.set(index);//remember zoom size
	zoomEvent = false;
}

function resizeMask()
{
	if(chatWindowMode.geti() != 1)//only for lightbox mode
		return;
		
	var doc = window.parent.document;
	var maskObj = doc.getElementById("mask"); 
	if(!maskObj){
		var dabody = doc.getElementsByTagName('body')[0];//dynamically create mask on parent window under floating player div
		maskObj = doc.createElement("div");
		maskObj.id = "mask";
		maskObj.style.position = "absolute";
		maskObj.style.top = "0px";
		maskObj.style.left = "0px";
		maskObj.style.width = "100%";
		maskObj.style.zIndex = "10";
		maskObj.style.visibility = "hidden";
		
		var userAgent = navigator.userAgent.toLowerCase();
		if (userAgent.indexOf('mac') != -1 && userAgent.indexOf('firefox')!=-1) {//mac ff bug :/
			maskObj.style.backgroundRepeat = "repeat";
			maskObj.style.backgroundImage = "url(themes/images/mask70.png)";// 70% opacity png
			maskObj.innerHTML = '<iframe src="about:blank" style="position:absolute;top:0;left:0;z-index:-1;width:100%;height:99%;"></iframe>';
			
		}else{
			maskObj.style.backgroundColor = "#000000";
			maskObj.style.MozOpacity = lightBoxOpacity/100;
			maskObj.style.opacity = lightBoxOpacity/100;
    		maskObj.style.filter = "alpha(opacity="+ lightBoxOpacity +")";			
			if(userAgent.indexOf("msie 6") != -1)//use iframe to maskout selects in ie 6
				maskObj.innerHTML = '<iframe src="about:blank" style="position:absolute;top:0;left:0;z-index:-1;filter:mask();width:3000px;height:3000px;"></iframe>';
		}		
		dabody.appendChild(maskObj);
	}
	
	var clientHeight = parent.innerHeight ? parent.innerHeight : parent.document.documentElement.clientHeight;
	
	var scrollHeight; 
	if(window.innerHeight && window.scrollMaxY){//ff
		scrollHeight = parent.innerHeight + parent.scrollMaxY;
	}else if(doc.documentElement && doc.documentElement.scrollHeight > doc.documentElement.offsetHeight){ // ie strict mode
		scrollHeight = doc.documentElement.scrollHeight - (doc.documentElement.scrollHeight-doc.body.offsetHeight);
	}else{
		scrollHeight = doc.body.offsetHeight;
		if(!ie)
			scrollHeight += 20;
	}
	
	clientHeight = scrollHeight > clientHeight ? scrollHeight : clientHeight;
	maskObj.style.height = clientHeight + "px";
	maskObj.style.visibility = "visible";	
}

function updateSize()
{
	if(ie){
		clearTimeout(updateSizeTimerID);//ie constantly fires the onresize event so dont resize tell 500 millisecs after last event
		updateSizeTimerID = setTimeout( function(){try{resizeMask()}catch(e){} zoom(zoomIndex.geti())},500);
	}else{//everyone else
		try{resizeMask()}catch(e){} 
		zoom(zoomIndex.geti())
		clearTimeout(updateSizeTimerID);//cleanup vertical scroll bar issue on ff after resize to smaller width
		updateSizeTimerID = setTimeout( function(){try{resizeMask()}catch(e){} zoom(zoomIndex.geti())},1);
	}
}

function closePlayer()
{	
	if(chatWindowMode.geti() == 2){//tabbed / pop up window
		// window.close();
		window.location = "http://www.MyRubyR.com/user/home.php"
		return;
	}
	//teardown looks messy, hide our mess
	try{parent.document.getElementById("playerDiv").style.visibility = "hidden";}catch(e){}
	try{parent.document.getElementById("mask").style.visibility = "hidden";}catch(e){}
	
	//delay teardown to allow player to hide
	setTimeout(function(){						
		var dabody = parent.document.getElementsByTagName('body')[0];
		var playerObj = parent.document.getElementById("playerDiv");
		var maskObj = parent.document.getElementById("mask");	
		
		if(agt.indexOf("windows nt 5.1")!= -1)
			try{$("MediaPlayer").URL = "mms://127.0.0.1";}catch(e){}//reduces player shutdown issues in xp
		
		//remove resize event handler from parent window
		if(window.removeEventListener){ // ff
			try{parent.removeEventListener('resize',updateSize,false);}catch(e){}
		} else { // ie
			try{parent.detachEvent('onresize',updateSize);}catch(e){}
		}
			
		if(ie){
			var playerFrame = parent.document.getElementById("playerFrame");
			if(playerFrame)
				playerFrame.src = "about:blank";//force ie to relase memory holding wmp
		}
		
		if(playerObj){//remove playerDiv
			while(playerObj.hasChildNodes()) {playerObj.removeChild(playerObj.lastChild );}
			dabody.removeChild(playerObj);
		}
		
		if(maskObj){//remove mask
			while(maskObj.hasChildNodes()) {maskObj.removeChild(maskObj.lastChild );}
			dabody.removeChild(maskObj);
		}
	},1);
}

function onZoom()//also called by silverlight so dont change function name
{
	var size=zoomIndex.geti();
	if(size+1>=zoomArray.length)
		zoomIndex.set(0);
	else 
		zoomIndex.set(size+1);
		
	zoomEvent = true;//user clicked zoom button
	zoom(zoomIndex.geti());	
}

function dragVolume(e)
{
	e = e || window.event;
	var mouseX;
	if(e.pageX){//ff
		mouseX = e.pageX;
	}else{//ie
		mouseX = e.clientX + document.body.scrollLeft - document.body.clientLeft;
	}
	
	if(mouseStartX == 0){//initial mousedown
		mouseStartX = mouseX;
		volumeStartX = Math.round(volume/2);
	}
	
	var volumeX = volumeStartX + (mouseX - mouseStartX);
	if(volumeX < 0)
		volumeX = 0;
	if(volumeX > 50)
		volumeX = 50;		
	volume = volumeX * 2;	
	
	adjustVolume();
	return false;
}
function adjustVolume()
{	
	$("sliderKnob").style.left = Math.round(volume/2) + sliderKnobCSS.left + "px";
	try{$("MediaPlayer").settings.volume = volume;}catch(e){}
	if(volume > 0)
		mutedVolume = -1;//they are manually over-riding the mute button by moving the slider so not mute anymore
}

function getCSS(cssName)
{
	var cssObj = {};
	var styles = Array("margin-top","border-top-width","padding-top","padding-bottom","border-bottom-width","margin-bottom",
	"margin-left","border-left-width","padding-left","padding-right","border-right-width","margin-right","height","width",
	"border-bottom-color","background-image","background-color","left","line-height");

	for(var x=0; x<styles.length;x++){ 
		var styleName = styles[x].replace(/\-(\w)/g, function (str, p1){return p1.toUpperCase();});      
		var val = getStyle(cssName,styles[x]);
		if(val.indexOf("px") != -1){
			cssObj[styleName] = parseInt(val);
		}else if(val.indexOf("rgb") != -1){//rgb(144,144,144) convert to #909090
			var colors = val.substr(4).split(",");
			cssObj[styleName] = "#" + parseInt(colors[0]).toString(16) + parseInt(colors[1]).toString(16) + parseInt(colors[2].substr(0)).toString(16);
		}else if(val.indexOf("#") != -1){
			cssObj[styleName] = val;
		}else if(val.indexOf("url(") != -1){
			cssObj[styleName] = val.substr(4).replace(/'|"|\)/g,"");								  
		}else{
			cssObj[styleName] = 0;	
		}		
	}
	return cssObj;
}

function parseURLquery()
{
	//parse href for query
	location.url = /\?.+/.exec(location.href);
	location.url = location.url?(location.url[0].substring(1).split('&')):[];
		
	for (var i=0,len=location.url.length;i<len;i++){
		var nameValuePair = location.url[i].split('=');
		location.url[nameValuePair[0]]=unescape(nameValuePair[1]);
	}
	
	if(location.url['host_name'])
		host_name.set(location.url['host_name']);//set session cookie
	
	if(location.url['mode_request'])		
		mode_request.set(location.url['mode_request']);//set session cookie	
}

////////////////////////////////////////////////////////////////
///// Silverlight Init /////////////////////////////////////
function createSilverlight(url)
{	
	var initParams = "configFile=./player4/config/config.xml";
	
	initParams += ",mediaSource="+url;	
	initParams += ",mediaElementWidth=" + videoWidth;
	initParams += ",mediaElementHeight=" + videoHeight;
	initParams += ",mediaElementBorderStrokeThickness=" + mplayerCSS.borderBottomWidth;
	initParams += ",mediaElementBorderStroke=" + mplayerCSS.borderBottomColor;

	initParams += ",zoomBarMarginTop=" + (mplayerCSS.marginBottom + zoomBarCSS.marginTop);
	initParams += ",zoomBarStrokeThickness=" + zoomBarCSS.borderBottomWidth;
	initParams += ",zoomBarStroke=" + zoomBarCSS.borderBottomColor;	
	initParams += ",zoomBarHeight=" + zoomBarCSS.height;
	initParams += ",zoomBarImg=" + zoomBarCSS.backgroundImage;
	
	initParams += ",zoomImg=" + zoomImgCSS.backgroundImage;
	initParams += ",zoomImgWidth=" + zoomImgCSS.width;
	
	initParams += ",fullscreenImg=" + fullscreenCSS.backgroundImage;
	initParams += ",fullscreenImgWidth=" + fullscreenCSS.width;
	
	initParams += ",muteImg=" + muteCSS.backgroundImage;
	initParams += ",muteImgWidth=" + muteCSS.width;
	
	initParams += ",sliderTrackImg=" + sliderCSS.backgroundImage;
	initParams += ",sliderTrackImgWidth=" + sliderCSS.width;
	
	initParams += ",sliderKnobImg=" + sliderKnobCSS.backgroundImage;
	initParams += ",sliderKnobImgWidth=" + sliderKnobCSS.width;
	
	initParams += ",backgroundColor=" + containerCSS.backgroundColor;
	
	initParams += ",volume=" + volume;
	
	$("leftDiv").innerHTML = '<object data="data:application/x-silverlight," type="application/x-silverlight-2" width="100%" height="100%"> \
			<param name="source" value="silverlight2/SilverlightPlayer.xap"/> \
			<param name="isWindowless" value="false"/> \
			<param name="background" value="#000000" /> \
			<param name="initParams" value="'+ initParams +'" /> \
		</object>';
		
	$("fplayer").focus();	
}
////////////////////////////////////////////////////
//// H.264 Init /////////////////////////////////////////
function createFlashH264(url)
{	
	var movie = "./player4/flash/video.swf";	
	movie += "?configFile=./player4/config/config.xml";
	movie += "&mediaElementWidth=" + videoWidth;
	movie += "&mediaElementHeight=" + videoHeight;
	movie += "&mediaElementBorderStrokeThickness=" + mplayerCSS.borderBottomWidth;
	movie += "&mediaElementBorderStroke=" + mplayerCSS.borderBottomColor;
	
	movie += "&zoomBarMarginTop=" + (mplayerCSS.marginBottom + zoomBarCSS.marginTop);
	movie += "&zoomBarStrokeThickness=" + zoomBarCSS.borderBottomWidth;
	movie += "&zoomBarStroke=" + zoomBarCSS.borderBottomColor;	
	movie += "&zoomBarHeight=" + zoomBarCSS.lineHeight;
	movie += "&zoomBarImg=" + zoomBarCSS.backgroundImage;
	movie += "&zoomImg=" + zoomImgCSS.backgroundImage;
	movie += "&zoomImgWidth=" + zoomImgCSS.width;
	
	movie += "&fullscreenImg=" + fullscreenCSS.backgroundImage;
	movie += "&fullscreenImgWidth=" + fullscreenCSS.width;
	
	movie += "&muteImg=" + muteCSS.backgroundImage;
	movie += "&muteImgWidth=" + muteCSS.width;
	
	movie += "&sliderTrackImg=" + sliderCSS.backgroundImage;
	movie += "&sliderTrackImgWidth=" + sliderCSS.width;
	
	movie += "&sliderKnobImg=" + sliderKnobCSS.backgroundImage;
	movie += "&sliderKnobImgWidth=" + sliderKnobCSS.width;
	movie += "&backgroundColor=" + containerCSS.backgroundColor;
	
	movie += "&volume=" + volume;
	movie += "&url=" + url;
	
	var str = new String(movie);
	movie = str.replace(/#/g,"0x");// no # signs

	$("leftDiv").innerHTML = ' \
	<object id="fvideo" name="fvideo" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" \
	codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,115,0" width="100%" height="100%"> \
	<param name="movie" value="'+ movie +'"> \
	<param name="quality" value="high"> \
	<param name="loop" value="false"> \
	<param name="scale" value="exactfit"> \
	<param name="allowscriptaccess" value="always"> \
	<param name="allowfullscreen" value="true"> \
	<param name="bgcolor" value="#000000"> \
	<embed name="fvideo" src="'+ movie +'" width="100%" height="100%" loop="false" type="application/x-shockwave-flash" \
	pluginspage="http://www.macromedia.com/go/getflashplayer" bgcolor="#000000" quality="high" scale="exactfit" swLiveConnect="true" allowScriptAccess="always" allowFullScreen="true"></embed> \
	</object>';	
}
if(ie && !opera){
	document.write('<script FOR="fvideo" EVENT="FSCommand" LANGUAGE="Jscript">fvideo_DoFSCommand(arguments[0],arguments[1]);</script>');
	document.write('<script FOR="fplayer" EVENT="FSCommand" LANGUAGE="Jscript">fplayer_DoFSCommand(arguments[0],arguments[1]);</script>');
}
