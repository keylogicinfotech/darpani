/*
	Module Name:- Login
	File Name  :- login.js
	Create Date:- 09-12-2016
	Intially Create By :- 0022
	Update History:
*/
function validate_frm_login()
{
	with(document.frm_login)
	{
		if(isEmpty(txt_adminid.value))	
		{
			alert("Please enter admin ID");
			txt_adminid.select();
			txt_adminid.focus();
			return false;
		}
		if(isEmpty(pas_admin_password.value))	
		{
			alert("Please enter password ");
			pas_admin_password.select();
			pas_admin_password.focus();
			return false;
		}
		else
		{
			if(pas_admin_password.value.length < 8)	
			{
				alert("Too short: Minimum of '8' characters in Password.");
				pas_admin_password.select();
				pas_admin_password.focus();
				return false;
			}
			if(pas_admin_password.value.length > 20)	
			{
				alert("Too long: Maximum of '20' characters in Password.");
				pas_admin_password.select();
				pas_admin_password.focus();
				return false;
			}
			
		}
		if(isEmpty(txt_code.value))	
		{
			alert("Please enter secret code. It's case sensitive.");
			txt_code.select();
			txt_code.focus();
			return false;
		}
		if(isEmpty(txt_spamfilterq.value))	
		{
			alert("Please enter answer of sum in words.");
			txt_spamfilterq.select();
			txt_spamfilterq.focus();
			return false;
		}
	}
return true;
}