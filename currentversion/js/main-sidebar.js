(function($) {
    "use strict";
    $('[data-toggle="tooltip"]').tooltip({
        animated: 'fade',
        placement: 'top',
        container: 'body'
    });
    $(window).scroll(function() {
        if ($('#help-page').length == 0) {
            if ($(this).scrollTop() > 20) {
                $('.header-top-bar').addClass("sticky");
                $('.product-category-page-container').addClass("filter_sticky");
            } else {
                $('.header-top-bar').removeClass("sticky");
                $('.product-category-page-container').removeClass("filter_sticky");
            }
        }
    });
    new WOW().init();
    $("#homepage-offer-carosel").owlCarousel({
        autoPlay: true,
        slideSpeed: 2000,
        pagination: false,
        navigation: false,
        items: 1,
        navigationText: ["", ""],
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [980, 1],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
    });
    $(".product-carousel").owlCarousel({
        autoPlay: true,
        pagination: true,
        navigation: false,
        items: 4,
        navigationText: ["<span class='fa fa-angle-left fa-4x'></span>", "<span class='fa fa-angle-right fa-4x'></span>"],
        itemsDesktop: [1200, 4],
        itemsDesktopSmall: [1100, 3],
        itemsTablet: [900, 2],
        itemsMobile: [479, 1],
    });
    $(".product-detail-images-carousel").owlCarousel({
        autoPlay: false,
        slideSpeed: 500,
        pagination: true,
        navigation: true,
        items: 4,
        navigationText: ["<span class='fa fa-angle-left fa-4x'></span>", "<span class='fa fa-angle-right fa-4x'></span>"],
        itemsDesktop: [1600, 4],
        itemsDesktopSmall: [1199, 3],
        itemsTablet: [991, 1],
        itemsMobile: [479, 1],
    });
    $(".product-carousel-box").owlCarousel({
        autoPlay: false,
        pagination: false,
        navigation: true,
        items: 4,
        navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
        itemsDesktop: [1300, 4],
        itemsDesktopSmall: [1200, 3],
        itemsTablet: [991, 3],
        itemsMobile: [479, 2],
    });
    $(".product-carousel-most-view").owlCarousel({
        autoPlay: false,
        slideSpeed: 2000,
        pagination: false,
        navigation: true,
        items: 3,
        navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [980, 2],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
    });
    $("#blog-curosel").owlCarousel({
        autoPlay: true,
        slideSpeed: 500,
        pagination: true,
        navigation: true,
        items: 3,
        navigationText: ["<span class='fa fa-angle-left fa-4x'></span>", "<span class='fa fa-angle-right fa-4x'></span>"],
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [980, 2],
        itemsTablet: [768, 2],
        itemsMobile: [479, 1],
    });
    $("#latest-post-carousel").owlCarousel({
        autoPlay: true,
        slideSpeed: 500,
        pagination: true,
        navigation: true,
        items: 3,
        navigationText: ["<span class='fa fa-angle-left fa-4x'></span>", "<span class='fa fa-angle-right fa-4x'></span>"],
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [980, 2],
        itemsTablet: [768, 2],
        itemsMobile: [479, 1],
    });
    $("#blog-curosel-4").owlCarousel({
        autoPlay: false,
        slideSpeed: 2000,
        pagination: false,
        navigation: true,
        items: 2,
        navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
        itemsDesktop: [1199, 2],
        itemsDesktopSmall: [980, 2],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
    });
    $("#blog-curosel-5").owlCarousel({
        autoPlay: false,
        slideSpeed: 2000,
        pagination: false,
        navigation: true,
        items: 2,
        navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
        itemsDesktop: [1199, 2],
        itemsDesktopSmall: [980, 1],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
    });
    $("#blog-curosel-duble").owlCarousel({
        autoPlay: false,
        slideSpeed: 2000,
        pagination: false,
        navigation: true,
        items: 1,
        navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [980, 1],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
    });
    $("#blog-curosel-6").owlCarousel({
        autoPlay: false,
        slideSpeed: 2000,
        pagination: false,
        navigation: true,
        items: 3,
        navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [980, 2],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
    });
    $("#brand-curosel").owlCarousel({
        autoPlay: false,
        slideSpeed: 2000,
        pagination: false,
        navigation: false,
        items: 6,
        navigationText: ["<span></span>", "<span></span>"],
        itemsDesktop: [1199, 5],
        itemsDesktopSmall: [980, 4],
        itemsTablet: [768, 2],
        itemsMobile: [479, 1],
    });
    $(".slider-product-carousel").owlCarousel({
        autoPlay: false,
        slideSpeed: 2000,
        pagination: true,
        navigation: false,
        items: 4,
        navigationText: ["<span></span>", "<span></span>"],
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [980, 2],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
    });
    $(".banner-carousel").owlCarousel({
        autoPlay: false,
        slideSpeed: 2000,
        pagination: true,
        navigation: false,
        items: 1,
        navigationText: ["<span></span>", "<span></span>"],
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [980, 1],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
    });
    var sync2 = $("#sync2");
    sync2.owlCarousel({
        items: 3,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [980, 3],
        itemsTablet: [768, 3],
        itemsTabletSmall: false,
        itemsMobile: [479, 2],
        pagination: false,
        responsiveRefreshRate: 100,
        navigation: true,
        navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
    });
    $(".qtybutton").on("click", function() {
        var max_qty = max_quantity($(this));
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();
        if ($button.text() == "+") {
            var newVal = parseFloat(oldValue) + 1;
            if (newVal > max_qty) {
                newVal = oldValue
            }
        } else {
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
            }
        }
        $button.parent().find("input").val(newVal);
    });
    $(".icon-search").on('click', function() {
        $(".top-form").show(500);
        $(".logo").hide();
    });
    $(".closebtn").on('click', function() {
        $(".top-form").hide(500);
        $(".logo").show();
    });
    $(".showlogin").on('click', function() {
        $(".login").slideToggle();
    })
    $(".show-coupon").on('click', function() {
        $(".checkout_coupon").slideToggle();
    });
    $(".showaccount").on('click', function() {
        $(".account-box-hide").slideToggle();
    });
    $(".showship").on('click', function() {
        $(".ship-box-hide").slideToggle();
    });
    $("#blog-curosel-shortcode").owlCarousel({
        autoPlay: false,
        slideSpeed: 2000,
        pagination: true,
        navigation: false,
        items: 3,
        navigationText: ["<span></span>", "<span></span>"],
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [980, 2],
        itemsTablet: [768, 1],
        itemsMobile: [479, 1],
    });
})(jQuery);
$(document).ready(function() {
    if ($(window).width() > 767) {
        $("#zoom_01").elevateZoom({
            gallery: 'gal1',
            cursor: 'pointer',
            galleryActiveClass: "active"
        });
        $("#zoom_01").bind("click", function(e) {
            var ez = $('#zoom_01').data('elevateZoom');
            ez.closeAll();
            $.fancybox(ez.getGalleryList(), {
                width: 500,
                height: 500,
                minWidth: 100,
                minHeight: 100,
                maxWidth: 500,
                maxHeight: 500,
                loop: false
            });
            return false;
        });
    };
});
var winWidth = $(window).width();
$(document).ready(function() {
    function setModalHgt() {
        var winHgt = document.documentElement.clientHeight;
        if (winWidth < 768) {
            $("#giftcardModal .modal-dialog .modal-content, .fullwidth_modal .modal-dialog .modal-content").css('min-height', winHgt);
        } else {}
    }
    setModalHgt();
    $(window).resize(function() {
        setModalHgt();
    });
    $(window).scroll(function() {
        setModalHgt();
    });
    if ($(window).width() > 767) {
        $('.mob-filter-jquery').removeClass('modal');
        $('.mob-filter-jquery').removeClass('fade');
        $('.mob-filter-jquery-1').removeClass('modal-dialog');
        $('.mob-filter-jquery-2').removeClass('modal-content');
        $('.mob-filter-jquery-3').removeClass('modal-header');
        $('.mob-filter-jquery-4').removeClass('modal-body');
    }
});

function round_off(value) {
    var val = parseFloat(parseFloat(value).toFixed())
    if (val == 0 && value > 0) {
        return "1.00";
    } else {
        return val.toFixed(2);
    }
}

function round_off_val(value) {
    var val = parseFloat(parseFloat(value).toFixed())
    if (val == 0 && value > 0) {
        return 1.00;
    } else {
        return val;
    }
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + JSON.stringify(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function showHideAJAXLoader() {
    $('.ajax-loader').toggle();
    $('#scrollUp').toggle();
    $("body").toggleClass('disable-screen');
}

function set_cart_count() {
    var cart_json = JSON.parse(readCookie("cart-data"));
    var cart_count1 = 0
    var cart_count2 = 0
    var cart_count = 0
    if (cart_json) {
        if (cart_json.hasOwnProperty('cartitems') && cart_json.cartitems.length) {
            cart_count1 = cart_json.cartitems.length;
        }
        if (cart_json.hasOwnProperty('gcart') && cart_json.gcart.length) {
            cart_count2 = cart_json.gcart.length;
        }
    }
    cart_count = cart_count1 + cart_count2
    if ($(".cart_i_count").length > 0 && !$(".cart_i_count").hasClass("logedin_cart")) {
        $(".cart_i_count").text(cart_count);
        createCookie("cart-item-count", cart_count, 7);
    }
    return cart_count
}

function set_cookie(cartitem_json) {
    var cartitem = false
    cart_json = JSON.parse(readCookie("cart-data"));
    item_status = ""
    if (cart_json) {
        if (cart_json.hasOwnProperty('cartitems') && cart_json.cartitems.length) {
            for (i = 0; i < cart_json.cartitems.length; i++) {
                if (JSON.stringify(cart_json.cartitems[i]) === JSON.stringify(cartitem_json) || cart_json.cartitems[i].product_id == cartitem_json.product_id && cart_json.cartitems[i].combopack_id == cartitem_json.combopack_id && cart_json.cartitems[i].size == cartitem_json.size) {
                    cart_json.cartitems.splice(i, 1);
                }
            }
            if (!cartitem) {
                cart_json.cartitems.push(cartitem_json);
                eraseCookie("cart-data")
                createCookie("cart-data", cart_json, 7)
            }
        } else {
            cart_json.cartitems = [cartitem_json]
            createCookie("cart-data", cart_json, 7)
        }
    } else {
        cart_json = {
            'cartitems': [cartitem_json]
        }
        createCookie("cart-data", cart_json, 7)
    }
    cart_json = JSON.parse(readCookie("cart-data"));
    return [cart_json, item_status];
}

function remove_cartitem(cartitem_json) {
    cart_json = JSON.parse(readCookie("cart-data"));
    if (cart_json) {
        if (cart_json.hasOwnProperty('cartitems') && cart_json.cartitems.length) {
            for (i = 0; i < cart_json.cartitems.length; i++) {
                if (JSON.stringify(cart_json.cartitems[i]) === JSON.stringify(cartitem_json)) {
                    cart_json.cartitems.splice(i, 1);
                    eraseCookie("cart-data")
                    createCookie("cart-data", cart_json, 7)
                } else {
                    if (cartitem_json.product_id && cart_json.cartitems[i].product_id == cartitem_json.product_id && cart_json.cartitems[i].size == cartitem_json.size) {
                        cart_json.cartitems.splice(i, 1);
                        eraseCookie("cart-data")
                        createCookie("cart-data", cart_json, 7)
                    } else if (cartitem_json.combopack_id && cart_json.cartitems[i].combopack_id == cartitem_json.combopack_id && cart_json.cartitems[i].size == cartitem_json.size) {
                        cart_json.cartitems.splice(i, 1);
                        eraseCookie("cart-data")
                        createCookie("cart-data", cart_json, 7)
                    } else {
                        if (cart_json.hasOwnProperty('gcart') && cart_json.gcart.length) {
                            for (j = 0; j < cart_json.gcart.length; j++) {
                                if (cart_json.hasOwnProperty('gcart') && cartitem_json.giftcard_id && cart_json.gcart[j].gstyle == cartitem_json.giftcard_id) {
                                    cart_json.gcart.splice(j, 1);
                                    eraseCookie("cart-data")
                                    createCookie("cart-data", cart_json, 7)
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (cart_json.hasOwnProperty('gcart') && cart_json.gcart.length) {
                for (j = 0; j < cart_json.gcart.length; j++) {
                    if (cart_json.hasOwnProperty('gcart') && cartitem_json.giftcard_id && cart_json.gcart[j].gstyle == cartitem_json.giftcard_id) {
                        cart_json.gcart.splice(j, 1);
                        eraseCookie("cart-data")
                        createCookie("cart-data", cart_json, 7)
                    }
                }
            }
        }
    }
    return cart_json
}

function update_cartitem(cartitem_json, cartId, qty_update) {
    cart_json = JSON.parse(readCookie("cart-data"));
    only_size_updated = true
    if (cart_json) {
        for (i = 0; i < cart_json.cartitems.length; i++) {
            if (cart_json.cartitems[i].product_id == cartitem_json.product_id && cart_json.cartitems[i].combo_flag == cartitem_json.combo_flag && cart_json.cartitems[i].size == cartitem_json.size) {
                if (!qty_update) {
                    cart_json.cartitems.splice(i, 1);
                    cart_json.cartitems.splice(i, 0, cartitem_json);
                    cart_json.cartitems.splice(cartId - 1, 1);
                    eraseCookie("cart-data")
                    createCookie("cart-data", cart_json, 7)
                    only_size_updated = false
                }
            }
        }
        if (only_size_updated) {
            cart_json.cartitems.splice(cartId - 1, 1);
            cart_json.cartitems.splice(cartId - 1, 0, cartitem_json);
            eraseCookie("cart-data")
            createCookie("cart-data", cart_json, 7)
        }
    }
    return cart_json
}

function max_quantity(instance) {
    var stock = instance.closest('.cart-item').find('.cart-plus-minus-box').attr('data_qty');
    var pSize = instance.closest('.cart-item').find('.website-text-theme').data('size');
    var max_qty = 1;
    if (stock) {
        stock = eval(stock);
        for (var i = 0; i < stock.length; i++) {
            if (stock[i].size != "None") {
                if (stock[i].size == pSize) {
                    if (stock[i].manage_stock == 'No') {
                        max_qty = 50;
                    } else {
                        max_qty = stock[i].qty;
                    }
                }
            } else {
                if (stock[i].manage_stock == 'No') {
                    max_qty = 50;
                } else {
                    max_qty = stock[i].qty;
                }
            }
        }
    }
    return max_qty;
}

function post_form(path, params, method) {
    method = method || "post";
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
        }
    }
    document.body.appendChild(form);
    form.submit();
}

function getWhiteListSearchParameters() {
    var queryString = document.location.search;
    if (queryString == '') {
        return queryString;
    } else {
        var filter_keys = ["color", "material", "product_type", "size", "price", "stitched_type"];
        var queryParameters = document.location.search.split('?')[1].split('&');
        var filterString = queryParameters.reduce(function(query, value, index) {
            if (filter_keys.indexOf(value.split('=')[0]) > -1) {
                if (query != '') {
                    query += "&";
                }
                return query += value;
            } else {
                return query += '';
            }
        }, '');
        if (filterString) {
            return '?' + filterString;
        } else {
            return filterString;
        }
    }
}

function ValidateEmail(email) {
    var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return !regex.test(email);
}

function ValidatePhone(phone, country) {
    if (country && country.toLowerCase() !== 'india') {
        return (String(phone).length > 13);
    } else {
        return !(String(phone).length == 10);
    }
}

function validatePincode(pincode, country) {
    if (country && country.toLowerCase() !== 'india') {
        return pincode.length > 13;
    } else {
        var regex = /^[1-9][0-9]{5}$/;
        return !regex.test(pincode);
    }
}

function validateCity(city) {
    var cityRegex = /[!$%^*+=\[\]{}:"\\|<>\?]/;
    if (typeof city == 'string' && city.length <= 30 && city.trim().length >= 1 && !cityRegex.test(city)) {
        return false;
    }
    return true;
}

function ValidateName(name) {
    var nameRegex = /[!@#$%^*+\=\[\]{}:"\\|<>\/?]/;
    if (name.length > 60 || name.length == 0 || nameRegex.test(name)) {
        return true;
    }
    return false;
}

function ValidateStreetAddress(address) {
    var addressRegex = /[!$%^*+=\[\]{}:"\\|<>\?]/;
    if (address.length == 0 || addressRegex.test(address)) {
        return true;
    }
    return false;
}

function ValidateAddressLength(address) {
    if (address.length < 15) {
        return true;
    }
    return false;
}

function ValidateLandmark(landmark) {
    var landmarkRegex = /[!$%^*+=\[\]{}:"\\|<>\?]/;
    if (landmark.length > 150 || landmarkRegex.test(landmark)) {
        return true;
    }
    return false;
}

function ValidateSelect(select_name) {
    if (select_name.length == 0) {
        return false;
    }
    return true;
}

function ValidateCvv(number) {
    if (number.length < 3) {
        return false;
    }
    return true;
}

function ValidateCards(number) {
    var regex = new RegExp("^[0-9]{15}$");
    if (!regex.test(number))
        return false;
    return luhnCheck(number);
}

function validate_vpa(number) {
    var regex = new RegExp("^.+@.+$");
    if (!regex.test(number)) {
        return false;
    } else {
        return true;
    }
}

function luhnCheck(val) {
    var sum = 0;
    for (var i = 0; i < val.length; i++) {
        var intVal = parseInt(val.substr(i, 1));
        if (i % 2 == 0) {
            intVal *= 2;
            if (intVal > 9) {
                intVal = 1 + (intVal % 10);
            }
        }
        sum += intVal;
    }
    return (sum % 10) == 0;
}

function removePhoneCountryCode(phone) {
    return String(phone).replace("+91", "");
}
COOKIE_ADDRESS_LIST_EXPIRY_TIME = 10;
COOKIE_COUPON_AND_CREDITS_EXPIRY_TIME = 100;
COOKIE_ADDRESS_COUNTER_EXPIRY_TIME = 100;

function remove_address_from_cookie(address_id, $this) {
    var address_list = JSON.parse(readCookie('customer_info'));
    var address_id_counter = parseInt(readCookie('address-id-counter'));
    for (var index = 0; index < address_list.length; index++) {
        var address_obj = address_list[index];
        if (address_obj.address_id == address_id) {
            address_list.splice(index, 1);
            eraseCookie('customer_info');
            eraseCookie('address-id-counter');
            createCookie('customer_info', address_list, COOKIE_ADDRESS_LIST_EXPIRY_TIME);
            createCookie('address-id-counter', address_id_counter, COOKIE_ADDRESS_COUNTER_EXPIRY_TIME);
            location.reload();
        }
    }
}

function is_existing_address(address_item, address_id) {
    var address_list = JSON.parse(readCookie('customer_info'));
    var isDuplicateAddress = false;
    if (address_list != null && address_list.length > 0) {
        if (address_id == '') {
            for (var index = 0; index < address_list.length; index++) {
                var address_obj = address_list[index];
                if (address_obj.street_address == address_item.street_address && address_obj.id != address_id) {
                    isDuplicateAddress = true;
                    break;
                }
            }
        }
        return isDuplicateAddress;
    } else {
        return isDuplicateAddress;
    }
}

function save_or_modify_address_in_cookie(address_item, address_id) {
    var address_list = JSON.parse(readCookie('customer_info'));
    if (address_list != null && address_list.length > 0) {
        if (address_id != '') {
            for (var index = 0; index < address_list.length; index++) {
                var address_obj = address_list[index];
                if (address_obj.address_id == address_id) {
                    address_list.splice(index, 1);
                    break;
                }
            }
        }
        if (address_item.default_address == true) {
            for (var index = 0; index < address_list.length; index++) {
                var address_obj = address_list[index];
                address_obj.default_address = false;
            }
        }
        var address_id_counter = parseInt(readCookie('address-id-counter'));
        address_id_counter++;
        address_item['address_id'] = address_id_counter;
        address_list.push(address_item);
        eraseCookie('customer_info');
        eraseCookie('address-id-counter');
        createCookie('customer_info', address_list, COOKIE_ADDRESS_LIST_EXPIRY_TIME);
        createCookie('address-id-counter', address_id_counter, COOKIE_ADDRESS_COUNTER_EXPIRY_TIME);
    } else {
        address_item['address_id'] = 1;
        eraseCookie('customer_info');
        eraseCookie('address-id-counter');
        var address_array = [];
        address_array.push(address_item);
        createCookie('customer_info', address_array, COOKIE_ADDRESS_LIST_EXPIRY_TIME);
        createCookie('address-id-counter', 1, COOKIE_ADDRESS_COUNTER_EXPIRY_TIME);
    }
}

function sendUserDataToClevertap(name, email, phone_no) {
    clevertap.profile.push({
        "Site": {
            "Name": name,
            "Email": email,
            "Phone": phone_no
        }
    });
    if (Boolean(readCookie('user_just_logedin'))) {
        $.ajax({
            url: "/set_ip_addr/",
            type: "POST",
            data: {
                'email': email
            }
        })
    }
}

function productViewedAnalyticsEvents(sku, name, category, price, currency, date) {
    fbq('track', 'ViewContent', {
        content_name: name,
        content_ids: [sku],
        content_type: 'product',
        value: price,
        currency: currency
    });
    dataLayer.push({
        'ecommerce': {
            'detail': {
                'products': [{
                    'name': name,
                    'id': sku,
                    'price': price,
                }]
            }
        }
    });
    clevertap.event.push("Product viewed", {
        "Product SKU": sku,
        "Product name": name,
        "Category": category,
        "Price": price,
        "Currency": currency,
        "Date": date
    });
}

function giftcardViewedAnalyticsEvents(sku, name, category, price, currency, date) {
    fbq('track', 'ViewContent', {
        content_name: name,
        content_ids: [sku],
        content_type: 'giftcard',
        value: price,
        currency: currency
    });
    dataLayer.push({
        'ecommerce': {
            'detail': {
                'products': [{
                    'name': name,
                    'id': "giftcard",
                    'price': price,
                }]
            }
        }
    });
    clevertap.event.push("Product viewed", {
        "Product SKU": sku,
        "Product name": name,
        "Category": category,
        "Price": price,
        "Currency": currency,
        "Date": date,
        "Type": 'giftcard'
    });
}

function addToCartAnalyticsEvents(sku, name, category, price, qunatity, currency, date, image, productSlug, content_type) {
    var cont_type = '';
    var type = '';
    if (content_type) {
        cont_type = content_type;
        type = 'giftcard';
    } else {
        cont_type = 'product';
    }
    fbq('track', 'AddToCart', {
        content_name: name,
        content_ids: [sku],
        content_type: cont_type,
        value: price,
        currency: currency
    });
    clevertap.event.push("Added To Cart", {
        "Product SKU": sku,
        "Product name": name,
        "Category": category,
        "Price": price,
        "Quantity": qunatity,
        "Currency": currency,
        "Image URI": image,
        "Slug": productSlug,
        "Date": date,
        "Type": type
    });
    window._izq.push(["event", "add-to-cart", {}]);
}

function addToWishListAnalyticsEvents(product) {
    fbq('track', 'AddToWishlist', {
        content_name: product.name,
        content_ids: [product.sku],
        content_type: 'product',
        value: product.price,
        currency: product.currency
    });
    clevertap.event.push("Added To Wishlist", {
        "Product SKU": product.sku,
        "Product name": product.name,
        "Category": product.category,
        "Price": product.price,
        "Currency": product.currency,
        "Date": product.date
    });
}

function removeFromWishlistAnalyticsEvents(product) {
    clevertap.event.push("Remove From Wishlist", {
        "Product SKU": product.sku,
        "Product name": product.name,
        "Category": product.category,
        "Price": product.price,
        "Currency": product.currency,
        "Date": product.date
    });
}

function removeFromCartAnalyticsEvents(product) {
    dataLayer.push({
        'event': 'removeFromCart',
        'ecommerce': {
            'remove': {
                'products': [{
                    'name': product.name,
                    'id': product.sku,
                    'price': product.price,
                }]
            }
        }
    });
    clevertap.event.push("Remove From Cart", {
        "Product SKU": product.sku,
        "Product name": product.name,
        "Category": product.category,
        "Price": product.price,
        "Currency": product.currency,
        "Date": product.date
    });
}

function categoryViewedAnalyticsEvents(category) {
    clevertap.event.push("Category Viewed", {
        "Category name": category.name,
        "Date": category.date
    });
}

function searchTermAnalyticsEvents(category) {
    clevertap.event.push("Search", {
        "Category name": category.name,
        "Date": category.date
    });
}

function notificationBanner(params) {
    var offset_params;
    if ($(window).width() <= 991) {
        offset_params = {
            x: 20,
            y: 20
        }
    } else {
        offset_params = {
            x: 20,
            y: 120
        }
    }
    $.notify({
        icon: params.icon,
        message: params.message,
        url: params.clickUrl,
        target: params.target
    }, {
        element: 'body',
        position: null,
        type: params.type,
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        offset: offset_params,
        spacing: 10,
        z_index: 1031,
        delay: 5000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated bounceInDown',
            exit: 'animated bounceOutUp'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template: '<div data-notify="container" class="col-xs-11 col-sm-11 col-md-4 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">Ã—</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message" style="padding-left: 2px;">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
    });
}
$(function() {
    $(".serach-products").autocomplete({
        source: function(request, response) {
            var postData = {
                "search_string": request.term
            };
            jQuery.support.cors = true;
            $.ajax({
                url: "/api/autosuggest/",
                headers: {
                    "Authorization": "Token " + $("#es_token").data("token")
                },
                type: "POST",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify(postData),
                success: function(data) {
                    arr_hits = data;
                    var uniq_text = []
                    var final_arr = [];
                    $.each(arr_hits, function(i, el) {
                        if ($.inArray(el, final_arr) === -1) {
                            final_arr.push(el);
                        }
                    });
                    response($.map(final_arr, function(item) {
                        return {
                            "label": item,
                            "id": item
                        }
                    }));
                }
            });
        },
        minLength: 3,
        select: function(event, ui) {
            $("#company_id").val(ui.item.id);
            searchProducts(ui.item.value);
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    })
});

/*function searchProducts(query) {
    query = query.replace('&', '%26')
    window.history.pushState("", "product search", "/search/?query=" + query);
    window.location.reload();
}
$(".search-box-input-panel button[type=submit]").on("click", function(event) {
    event.preventDefault();
    if ($(this).closest('form').find(".serach-products").val().length >= 1) {
        searchProducts($(this).closest('form').find(".serach-products").val());
    }
});
$(".search-box-input-panel .serach-products").on("keypress", function(event) {
    if ($(this).val().length >= 1 && event.keyCode == 13) {
        searchProducts($(this).val());
    }
});*/
$("#showmenu").click(function(e) {
    e.preventDefault();
    $("#menu").toggleClass("show");
    $("#scrollUp").css({
        'visibility': 'hidden'
    });
    $('.price-and-checkout-btn-panel').hide();
    $('.add-and-go-to-cart-btn').hide();
    if ($("#menu").hasClass('show')) {
        $(".site-overlay").addClass('site-overlay-active');
    } else {
        $(".site-overlay").removeClass('site-overlay-active');
    }
});

function hideDisplayMenus($this) {
    if ($this.children('ul').length) {
        event.preventDefault();
        $this.children('ul').toggle('fast');
        $this.children('a').children('i:last-child').toggleClass('fa-caret-down fa-caret-left');
    }
}
$("#menu").on('click', 'li.main-category.submenu > a > i.fa', function(event) {
    var $this = $(this).closest('.submenu');
    hideDisplayMenus($this);
});
$("#menu").on('click', 'li.submenu.sub-category > a, li.submenu.sub-category > a > i.fa', function(event) {
    if ($(this).is('i.fa')) {
        event.stopPropagation();
    }
    var $this = $(this).closest('.submenu.sub-category');
    hideDisplayMenus($this);
});
$("body").on('click', '.site-overlay-active, .mobile-menu-panel .heading span', function() {
    $("#menu").removeClass("show");
    $(".site-overlay").removeClass('site-overlay-active');
    $("#scrollUp").css({
        'visibility': 'visible'
    });
    $('.price-and-checkout-btn-panel').show();
    $('.add-and-go-to-cart-btn').show();
});
$(document).ready(function() {
    $(".giftcard_delivery_date").datepicker({
        minDate: 'today'
    });
    $(".giftcard_delivery_date").datepicker('setDate', 'defaultDate');
    $(".ui-datepicker-trigger").click(function(e) {
        $(".giftcard_delivery_date").datepicker("show");
    });
});
$(document).ready(function() {
    $(document).on('click', '#subscribe_button', function(e) {
        email = $('#subscribe_email').val();
        if (email) {
            if (!ValidateEmail(email)) {
                $.ajax({
                    type: "POST",
                    url: "/subscribe_email/",
                    headers: {
                        "Authorization": Website.BaseJSUtils.auth_token
                    },
                    data: {
                        'email': email,
                    },
                    success: function(data) {
                        if (data.status == "Success") {
                            $("#subscribe_errors").text(data.message);
                            $("#subscribe_email").val("");
                        }
                    }
                });
            } else {
                $("#subscribe_errors").text("Please enter valid email address");
            }
        } else {
            $("#subscribe_errors").text("Please enter valid email address");
            e.preventDefault();
        }
    })
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function getUrlParam(parameter, defaultvalue) {
    var urlparameter = defaultvalue;
    if (window.location.href.indexOf(parameter) > -1) {
        urlparameter = getUrlVars()[parameter];
    }
    return urlparameter;
}

function createUtmCookie() {
    var utm_source = getUrlParam('utm_source', 'null');
    var utm_medium = getUrlParam('utm_medium', 'null');
    var utm_campaign = getUrlParam('utm_campaign', 'null');
    var utms = "utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign;
    createCookie("utm_params", utms, 1);
}
if (readCookie("utm_params")) {
    var utm_source = getUrlParam('utm_source', 'null');
    var utm_medium = getUrlParam('utm_medium', 'null');
    var utm_campaign = getUrlParam('utm_campaign', 'null');
    var utm1 = "utm_source=" + utm_source + "&utm_medium=" + utm_medium + "&utm_campaign=" + utm_campaign;
    var utm2 = JSON.parse(readCookie("utm_params"));
    if (utm1 != utm2 && (window.location.search.indexOf('utm_source') > 0 || window.location.search.indexOf('utm_medium') > 0 || window.location.search.indexOf('utm_campaign') > 0)) {
        createUtmCookie();
    }
} else {
    if (window.location.search.indexOf('utm_source') > 0 || window.location.search.indexOf('utm_medium') > 0 || window.location.search.indexOf('utm_campaign') > 0) {
        createUtmCookie();
    }
}