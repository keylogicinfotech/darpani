/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Function name getLength()
// Purpose: To print the number of characters in a control like text box or text area.
//			which is user to know how many characters are currently entered and hence accordingly can enter data
// Arguements: 2
//				1) ctlpnt is a readonly text box placed before the control whose length is to be known
//				2) ctllen is the name of the control whose length is to be known
// Suggestions: Call this function on keyup event or key down event instead of onChange event as onChange is
//				generally fired onBlur.
// Format of call: javascript:return getLength('txtaddresslen','taAddress');
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	function getLength(ctlpnt,ctllen)
//	{
//		document.getElementById(ctlpnt).value = document.getElementById(ctllen).value.length;	
//		return false;
//	}

	function getLength(ctlpnt,ctllen)
	{
		eval(ctlpnt).value = eval(ctllen).value.length;	
		return false;
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*	Function Name : noBlankSpaces(string)
	Input Parameter : String
	return value : String
	Purpose : find and removes blank space(s)
*/	

	function noBlankSpaces(t)
	{
		if(t.value.match(/\s/g))
		{
			alert('Sorry, you are not allowed to enter any space(s)');
			t.value=t.value.replace(/\s/g,'');
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
	Function Name : trim
	Input Parameter : String
	return value : String
	Purpose : truncate the leading and trailing the whitespace and return the string
*/
function trim(inputString) {
 	   if (typeof inputString != "string") { return inputString; }
		  var retValue = inputString;
		  var ch = retValue.substring(0, 1);
		   while (ch == " ") { // Check for spaces at the beginning of the string
      			retValue = retValue.substring(1, retValue.length);
			     ch = retValue.substring(0, 1);
   			}
		   ch = retValue.substring(retValue.length-1, retValue.length);
		  while (ch == " ") { // Check for spaces at the end of the string
		      retValue = retValue.substring(0, retValue.length-1);
      			ch = retValue.substring(retValue.length-1, retValue.length);
		   }
   		while (retValue.indexOf("  ") != -1) { // Note that there are two spaces in the string - look for multiple spaces within the string
		      retValue = retValue.substring(0, retValue.indexOf("  ")) + retValue.substring(retValue.indexOf("  ")+1, retValue.length); // Again, there are two spaces in each of the strings
   			}
   return retValue; // Return the trimmed string back to the user
}

/*
	function name : isEmpty
	Input Parmeter : strValue(Contains String Value)
	return value : False -->  If String Is Empty
	               True -->   If String Is Not Empty
	Purpose : Check For An Empty String
*/
function isEmpty(strValue)
{
	var smRegExpr=new RegExp('^[\r\n ]*$')
	if(smRegExpr.test(strValue))
		return true;
	return false
}

function isInteger(strValue)
{
	var smRegExpr=new RegExp('^[0-9]*$')
	if(smRegExpr.test(strValue))
		return true;
	return false
}


function sValidatePrice()
{
	var smRegExpr = new RegExp('^[0-9]*.?[0-9]*$');

	if (arguments.length != 1)
		return false;

	if (smRegExpr.test(arguments[0]))
	{	
		if ( (isNaN(arguments[0])==true) || arguments[0] < 0 )
		{
			return false;
		}
	}
	else
	{
		return false;
	}

	return true;
}

/*
	function name : CompareDate
	Input Parmeter : To ( string contains the valid date)
					 From (String contains the valid date)
	return value : False -->  if From date is greter then To date
	               True -->   if From date is less then or equal to To date
	Purpose : compare the two dates(string) with the format DD-mon-yyyy and return the result
*/
function CompareDate(From,To)
{
		RegDateArray=To.split("-");
		strToDate=RegDateArray[0] + " " + RegDateArray[1] + " " + RegDateArray[2]
		RegDateArray=From.split("-");
		strFromDate=RegDateArray[0] + " " + RegDateArray[1] + " " + RegDateArray[2]
		if(Date.parse(strToDate) < Date.parse(strFromDate))
		{
			return false;
		}
		return true;
}
/*function CompareDate(To,From)
	{
		RegDateArray=To.split("-");
		strToDate=RegDateArray[0] + " " + RegDateArray[1] + " " + RegDateArray[2]
		RegDateArray=From.split("-");
		strFromDate=RegDateArray[0] + " " + RegDateArray[1] + " " + RegDateArray[2]
		if(Date.parse(strToDate) > Date.parse(strFromDate))
		{
			return false;
		}
		return true;
	}
*/


/*
	function name : validatepassword($password)
	Input Parmeter : (string contains the password)
	return value : False -->  if password contains invalid characters
	               True -->   if password contains valid characters
	Purpose : validate password
*/

function ValidatePassword(Pass)
{
	var smRegExpr=new RegExp('^[0-9,a-z,A-Z,_]*$')
	if(smRegExpr.test(Pass))
		return true;
	return false
}

/*
	function name : validateDuration
	Input Parmeter : To ( string contains the valid date)
					 From (String contains the valid date)
	return value : False -->  if From date is greter then To date
	               True -->   if From date is less then or equal to To date
	Purpose : compare the two dates(string) with the format dd-mm-yyyy and return the result
*/
function validateDuration(From,To)
{
		var monthname=new Array("","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
		
		RegDateArray1=To.split("-");
		RegDateArray1[1] =monthname[RegDateArray1[1]];
		strToDate=RegDateArray1[2] + " " + RegDateArray1[1] + " " + RegDateArray1[0];
		RegDateArray2=From.split("-");
		RegDateArray2[1] =monthname[RegDateArray2[1]];
		strFromDate=RegDateArray2[2] + " " + RegDateArray2[1] + " " + RegDateArray2[0];
		if(Date.parse(strToDate) < Date.parse(strFromDate))
		{
			return false;
		}
		return true;
}


function ValidateYear()
{
	var smRegExpr=new RegExp('[1-2][0-9][0-9][0-9]');
	if (smRegExpr.test(arguments[0]))
		return true;
	return false;
}
function sValidateMailAddress()
{
	var smRegExp = new RegExp('^[^@. ][^@ ]\*@[^@. ][^@ ]\*[.][^.@ ]\*[^@ ]\*[^@. ]$');

	if (arguments.length != 1) 
		return false;
	if (smRegExp.test(arguments[0]))
		return true;
	return false;
}

function sValidateVideoFileTitle()
{
	var smRegExp = new RegExp('^([a-zA-Z0-9 ]*)$');

	if (arguments.length != 1) 
		return false;
	if (smRegExp.test(arguments[0]))
		return true;
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////	
//	Function Name : checkExt
//	Prototype : checkExt(fileName)
//	Input Parameter : fileName
// 	Return Value : True (if  entered fileName contains valid extension)
//				   False (if entered fileName does not contain valid extension)
//	Purpose : It will check if filename entered contains valid extension or not.
// 	Usage : checkExt(fileName)

	function checkExt(fileName)
	{
		if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "JPG" ) 
			return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "JPEG" )
		    return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "PNG" ) 			 
		    return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "GIF" ) 			 
		    return true;
		alert("Please select valid photograph file type. You can only upload 'JPG' , 'JPEG' , 'PNG', 'GIF' file type.");
		return false;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////	
//	Function Name : checkExtWithSWF
//	Prototype : checkExtWithSWF(fileName)
//	Input Parameter : fileName
// 	Return Value : True (if  entered fileName contains valid extension)
//				   False (if entered fileName does not contain valid extension)
//	Purpose : It will check if filename entered contains valid extension or not.
// 	Usage : checkExtWithSWF(fileName)

	function checkExtWithSWF(fileName)
	{
		if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "JPG" ) 
			return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "JPEG" )
		    return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "PNG" ) 			 
		    return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "GIF" ) 			 
		    return true;	
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "SWF" ) 			 
		    return true;		
		alert("Please select valid photograph file type. You can only upload 'JPG', 'JPEG', 'PNG', 'GIF', 'SWF' file type.");
		return false;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////	
//	Function Name : checkZipExt
//	Prototype : checkZipExt(fileName)
//	Input Parameter : fileName
// 	Return Value : True (if  entered fileName contains valid extension)
//				   False (if entered fileName does not contain valid extension)
//	Purpose : It will check if filename entered contains valid extension or not.
// 	Usage : checkZipExt(fileName)

	function checkZipExt(fileName)
	{
		if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "ZIP" ) 
			return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "RAR" )
		    return true;
		alert("Please select valid file type. You can only upload 'ZIP' , 'RAR' file type.");
		return false;
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////	
//	Function Name : checkBannerExt
//	Prototype : checkBannerExt(fileName)
//	Input Parameter : fileName
// 	Return Value : True (if  entered fileName contains valid extension)
//				   False (if entered fileName does not contain valid extension)
//	Purpose : It will check if filename entered contains valid extension or not.
// 	Usage : checkBannerExt(fileName)

	function checkBannerExt(fileName)
	{
		if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "JPG" ) 
			return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "JPEG" )
		    return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "PNG" ) 			 
		    return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "GIF" )
		    return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "BMP" ) 			 
		    return true;
		alert("Please select valid image file type. You can only upload 'JPG' , 'JPEG' , 'PNG' , 'GIF' , 'BMP' file type.");
		return false;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////	
//	Function Name : checkFlashExt
//	Prototype : checkFlashExt(fileName)
//	Input Parameter : fileName
// 	Return Value : True (if  entered fileName contains valid extension)
//				   False (if entered fileName does not contain valid extension)
//	Purpose : It will check if filename entered contains valid extension or not.
// 	Usage : checkFlashExt(fileName)

	function checkFlashExt(fileName)
	{
		if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "SWF" ) 
			return true;		
		alert("Please select valid flash file type. You can only upload 'SWF' file type.");
		return false;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////	
//	Function Name : checkJpgExt
//	Prototype : checkJpgExt(fileName)
//	Input Parameter : fileName
// 	Return Value : True (if  entered fileName contains valid extension)
//				   False (if entered fileName does not contain valid extension)
//	Purpose : It will check if filename entered contains valid extension or not.
// 	Usage : checkJpgExt(fileName)

	function checkJpgExt(fileName)
	{
		if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "JPG" ) 
			return true;		
		alert("Invalid Image File Type. You can only Upload 'JPG' file Type");
		return false;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////	
//	Function Name : checkExeExt
//	Prototype : checkExeExt(fileName)
//	Input Parameter : fileName
// 	Return Value : True (if  entered fileName contains valid extension)
//				   False (if entered fileName does not contain valid extension)
//	Purpose : It will check if filename entered contains valid extension or not.
// 	Usage : checkExeExt(fileName)

	function checkExeExt(fileName)
	{
		if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "EXE" ) 
			return false;		
	}	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////	

function chkDate(month,day,year) {
	    months = new Array("","January","February","March","April","May","June","July","August","September","October","November","December");
	    //Convert values to integer types
	    month = month * 1;
	    day = day * 1;
	    year = year * 1;
	    var month_string = months[month];
	  var leapYear = false;
	  if(year % 400 == 0) {
	    leapYear = true;
	  } else if(year % 100 == 0) {
	    leapYear = false;
	  } else if(year % 4 == 0) {
	    leapYear = true;
	  }
	  var days_in_feb = 28;
	  if(leapYear) {
	    days_in_feb = 29;
	  }
	  days_in_month = new Array(0,31,days_in_feb,31,30,31,30,31,31,30,31,30,31);

	    if (day > days_in_month[month]) {
	        alert(month_string + " does not have " + day + " days.");
	        return false;
	    }
	    return true;
	}	
	

// occurs: return no of occurrences of strVal within strSrc
function occurs(strVal, strSrc)
{
	
	var nCnt = 0, strBuffer=strSrc;

	while (strBuffer.indexOf(strVal) >= 0)
	{
		strBuffer = replace(strBuffer, strVal, "");
		nCnt++;
	}
		
	return (nCnt);
}
// isDigit: check if val has digits (0-9)
function isDigit(val)
{
	var strBuffer = new String(val);
	var nPos = 0;

	if (isEmpty(strBuffer))
		return false;

	for (nPos = 0; nPos < strBuffer.length; nPos++)
		if (strBuffer.charAt(nPos) < '0' || strBuffer.charAt(nPos) > '9')
			return false;

	return true;
}	
	
/*	
function isDate(dVal)
{
	var strBuffer= new String(dVal);
	var cDelimiter='';
	var strMonth=0, strDay=0, strYear=0;
	var nPos=-1;
	
	// Get the delimiter used
	if (occurs('/', strBuffer) == 2)
		cDelimiter = '/';
	else if (occurs('-', strBuffer) == 2)
		cDelimiter = '-';

	// If no '/' or '-' found return false
	if (cDelimiter == '')
		return false;

	// validate month, date, and year (Y, YY, YYYY are valid year formats)
	nPos = strBuffer.indexOf(cDelimiter);
	strMonth = strBuffer.substring(0, nPos);
	if (strMonth.length > 2 || !isDigit(strMonth))
		return false;
	strBuffer = strBuffer.substring(nPos+1);
	nPos = strBuffer.indexOf(cDelimiter);
	strDay = strBuffer.substring(0, nPos);
	if (strDay.length > 2 || !isDigit(strDay))
		return false;
	strBuffer = strBuffer.substring(nPos+1);
	strYear = strBuffer;
	if ((strYear.length > 4) || (strYear.length == 3) || !isDigit(strYear))
		return false;

	// if YY < 50 then YYYY=20YY, else if YY >= 50 then YYYY=19YY
	var iYear = parseInt(strYear);
	if (iYear < 50)
		strYear = "20" + (strYear < 10 ? '0' + strYear:strYear);
	else if (iYear >= 50 && iYear < 100)
		strYear = "19" + strYear;

	strBuffer = strMonth + cDelimiter + strDay + cDelimiter + strYear;

	// validate date
	var dBuffer = new Date(strBuffer);
	if (dBuffer.getDate() != parseInt(strDay) || 
				dBuffer.getMonth()+1 != parseInt(strMonth) || 
				dBuffer.getFullYear() != parseInt(strYear))
		return false;

	return true;
}
*/

	function isDate(Format,dVal)
	{
		var strBuffer;
		var cDelimeter='';
		var strMonth=0, strDay=0, strYear=0;
		var nPos=-1;
	
		// Get the delimiter used
		if (occurs('/', dVal) == 2)
			cDelimeter = '/';
		else if (occurs('-', dVal) == 2)
			cDelimeter = '-';
		else if (occurs('.', dVal) == 2)
			cDelimeter = '.';
		else if (occurs(' ', dVal) == 2)
			cDelimeter = ' ';

		// If no '/' or '-' found return false
		if (cDelimeter == '')
			return false;

		RegTempArray=dVal.split(cDelimeter);
		if(Format=="mdy")
		{
			RegDateArray = new makeArray(RegTempArray[0],RegTempArray[1],RegTempArray[2]);
		}
		else if(Format=="dmy")
		{
			RegDateArray = new makeArray(RegTempArray[1],RegTempArray[0],RegTempArray[2]);
		}
		else if(Format=="ymd")
		{
			RegDateArray = new makeArray(RegTempArray[1],RegTempArray[2],RegTempArray[0]);
		}
		
		dVal=RegDateArray[0]+cDelimeter+RegDateArray[1]+cDelimeter+RegDateArray[2]
		
		strBuffer = new String(dVal);
		// validate month, date, and year (Y, YY, YYYY are valid year formats)
		nPos = strBuffer.indexOf(cDelimeter);
		strMonth = strBuffer.substring(0, nPos);
		if (strMonth.length > 2 || !isDigit(strMonth))
			return false;
		
		strBuffer = strBuffer.substring(nPos+1);
		nPos = strBuffer.indexOf(cDelimeter);
		strDay = strBuffer.substring(0, nPos);
		if (strDay.length > 2 || !isDigit(strDay))
			return false;
		
		strBuffer = strBuffer.substring(nPos+1);
		strYear = strBuffer;
		if ((strYear.length > 4) || (strYear.length == 3) || !isDigit(strYear) || strYear==0)
			return false;
	
		// if YY < 50 then YYYY=20YY, else if YY >= 50 then YYYY=19YY
		var iYear = parseInt(strYear);
		
		if (iYear < 50)
			strYear = "20" + (strYear < 10 ? '0' + strYear:strYear);
		else if (iYear >= 50 && iYear < 100)
			strYear = "19" + strYear;

		strBuffer = strMonth + cDelimeter + strDay + cDelimeter + strYear;

		// validate date
		var dBuffer = new Date(strBuffer);
		if(strDay.substr(0,1) == "0")
		{
			strDay = strDay.substr(1);
		}
		if(strMonth.substr(0,1) == "0")
		{
			strMonth = strMonth.substr(1);
		}
	
		if (dBuffer.getDate() != parseInt(strDay) || 
				dBuffer.getMonth()+1 != parseInt(strMonth) || 
				dBuffer.getFullYear() != parseInt(strYear))
		return false;

	return true;
	}



	

function replace(strSrc, strVal, strWith)
{
	var nPos = 0, strLeft="", strRight="";

	// check if empty (or) no string is found to replace
	if (isEmpty(strSrc) || (strSrc.indexOf(strVal) < 0))
		return strSrc;

	nPos = strSrc.indexOf(strVal);
	strLeft = strSrc.substring(0, nPos);
	nPos += strVal.length;
	strRight = strSrc.substring(nPos);

	return (strLeft + strWith + strRight);
}

// Function Name :: isValidUrl
// Purpose       :: Check validity of url
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function isValidUrl(txtstring)
	{
		var txtval;
		txtval = txtstring.toLowerCase();
		if(trim(txtval.substring(0,7))!='http://' && trim(txtval.substring(0, 8)) != trim("https://"))
		{	
			alert("Please enter valid URL. Format: http://www.domainname.com");
			return false;
		}
		if(txtval.length <= 8)
		{
			alert("Please enter valid URL. Format: http://www.domainname.com");
			return false;
		}
		return true;
	}
	
//	Function Name : makeArray
//	Prototype : makeArray()
//	Input Parameter : Array elements separated with commas
// 	Return Value : Array
//	Purpose : It will create required array
// 	Usage : makeArray(x,y,...). 

	function makeArray() 
	{
		for (i = 0; i<makeArray.arguments.length; i++)
		this[i] = makeArray.arguments[i];
	}
	
function sValidateInt()
{
	var smRegExpr = new RegExp('^[0-9][0-9]*$');

	if (arguments.length != 1)
		return false;

	if (smRegExpr.test(arguments[0]))
		return true;

	if ((isNaN(arguments[0])) | (isNaN(parseInt(arguments[0],10))))
		return false;

	return false;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Function Name : checkBroucherExt
//	Prototype : checkBroucherExt(fileName)
//	Input Parameter : fileName
// 	Return Value : True (if entered fileName contains valid extension)
//				   False (if entered fileName does not contain valid extension)
//	Purpose : It will check if filename entered contains valid extension or not.
// 	Usage : checkBroucherExt(fileName)

	function checkBroucherExt(fileName)
	{
		var lenbeforedot,lenafterdot
		lenbeforedot = fileName.substring(0,fileName.lastIndexOf(".")).length;
		lenafterdot = fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).length;
		if((lenbeforedot==0) || (lenafterdot==0) || (fileName.lastIndexOf("."))==0)
		{
			alert("Enter valid file name")
			return false
		}
		return true;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Function Name : checkVideoExt
//	Prototype : checkVideoExt(fileName)
//	Input Parameter : fileName
// 	Return Value : True (if  entered fileName contains valid extension)
//				   False (if entered fileName does not contain valid extension)
//	Purpose : It will check if filename entered contains valid extension or not.
// 	Usage : checkVideoExt(fileName)

	function checkVideoExt(fileName)
	{
		if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "MPEG" ) 
			return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "MPG" )
		    return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "AVI" ) 			 
		    return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "WAV" ) 			 
		    return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "ASF" )
		    return true;
		alert("Invalid Video Clip File Type. You can only Upload 'MPEG' , 'MPG' , 'AVI' , 'WAV' , 'ASF' file Type");
		return false;
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Function Name : checkMPGVideoExt
//	Prototype : checkMPGVideoExt(fileName)
//	Input Parameter : fileName
// 	Return Value : True (if  entered fileName contains valid extension)
//				   False (if entered fileName does not contain valid extension)
//	Purpose : It will check if filename entered contains valid extension or not.
// 	Usage : checkVideoExt(fileName)

	function checkMPGVideoExt(fileName)
	{
		if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "MPEG" ) 
			return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "MPG" )
		    return true;
		else if(fileName.substring(fileName.lastIndexOf(".")+1,fileName.length).toUpperCase() == "WMV" )
		    return true;
		alert("Invalid Video Clip File Type. You can only Upload 'WMV' , 'MPEG' , 'MPG' file Type");
		return false;
	}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//	Function Name : isValidPassword
//	Prototype : isValidPassword(strPassword)
//	Input Parameter : strPassword
// 	Return Value : Boolean
//	Purpose : It will Check for valid password string 
// 	Usage : isValidPassword(strPassword)
   function isValidPassword(strPassword)
   {
   		var PasswordString;
		PasswordString = trim(strPassword)
		if(isEmpty(PasswordString))
		{
			alert("Password can not be Blank.");				
			return false;
		}	
		if(PasswordString.indexOf(" ")>=0)
		{
			alert("Space is not allowed in password.");				
			return false;
		}			
		if(PasswordString.length < 6)
		{
			alert("Password should be of minimum 6 characters.");			
			return false;
		}   			
		return true;
   }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//	Function Name : checkSpace
//	Prototype : checkSpace(strString)
//	Input Parameter : strString
// 	Return Value : Boolean
//	Purpose : It will Check for space in input string 
// 	Usage : checkSpace(strString)
   function checkSpace(inputString)
   {
   		var tmpString;
		tmpString = trim(inputString)
		if(isEmpty(tmpString))
		{		
			return true;
		}	
		if(tmpString.indexOf(" ")>=0)
		{			
			return true;
		}					   			
		return false;
   }
   
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Function Name : checkbannerSource   
//	Prototype    : checkbannerSource(strString) 
// Input Parameter : strString
// Return value : Boolean
// Purpose : It will check both http and banner file extension
// Usage : checkbannerSource(strString) 

	/*function checkBannerSource(strString)
	{
		var txtval
		txtval = strString.toLowerCase()
		if(trim(txtval.substring(0,7))!='http://' && trim(txtval.substring(0, 8)) != trim("https://"))
		{	
			alert("Please enter valid banner remote url format. \n Format: http://www.modelingsquare.com/banner/banner1.jpg");
			return false;
		}
		if(txtval.length <= 10)
		{
			alert("Please enter valid banner remote url format. \n Format: http://www.modelingsquare.com/banner/banner1.jpg");
			return false;
		}
		if(txtval.substring(txtval.lastIndexOf(".")+1,txtval.length).toUpperCase() == "JPG" ) 
			return true;
		else if(txtval.substring(txtval.lastIndexOf(".")+1,txtval.length).toUpperCase() == "JPEG" )
		    return true;
		else if(txtval.substring(txtval.lastIndexOf(".")+1,txtval.length).toUpperCase() == "PNG" ) 			 
		    return true;
		else if(txtval.substring(txtval.lastIndexOf(".")+1,txtval.length).toUpperCase() == "GIF" )
		    return true;
		else if(txtval.substring(txtval.lastIndexOf(".")+1,txtval.length).toUpperCase() == "BMP" ) 			 
		    return true;
		alert("Please enter valid banner remote url format. \n Format: http://www.modelingsquare.com/banner/banner1.jpg");
		return false;
	}*/
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Function Name : isPositiveNumber   
//	Prototype    : isPositiveNumber(no) 
// Input Parameter : number
// Return value : Boolean
// Purpose : It will check for positive number
// Usage : isPositiveNumber(no)
 
function isPositiveNumber(no)
{
	if(trim(no) != "")
	{
		if(trim(no) <0 || isNaN(trim(no))==true)
		{			
			return false;
		}
		return true;
	}
	else
	{
		return false;
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Function Name : Convert12HourTo24Hour(hh,mm,ampm)
//	Prototype    : Convert12HourTo24Hour(hh,mm,ampm)
// Input Parameter : numbers and string
// Return value : time in 24 hour format
// Purpose : It will convert 12 hour format of time to 24 hour format of time
// Usage :  Convert12HourTo24Hour(hh,mm,ampm)


function  Convert12HourTo24Hour(hh,mm,ampm)
{
		if(ampm.toUpperCase()=='AM')
		{
				if(hh==12)
				{
						hh="00";
				}
		}
		if(ampm.toUpperCase()=='PM')
		{
				if(hh==12)
				{
						hh=12;
				}
				else
				{
						count=hh;
						hh=12+eval(count);
				}
		}
		if(hh.length==1)
		{
				hh="0" + hh;
		}
		if(mm.length==1)
		{
				mm="0" + mm;
		}
		return hh + ":" + mm;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Function Name : Compare24HourTime(starttime,endtime)
//	Prototype    : Compare24HourTime(starttime,endtime)
// Input Parameter : time in hr and min in 24 hr format
// Return value : boolean
// Purpose : It will convert compare time
// Usage :  Compare24HourTime(starttime,endtime)

function Compare24HourTime(starttime,endtime)
{
		
		hh=starttime.substring(0,2);
		mm=starttime.substring(3,5);
		hh1=endtime.substring(0,2);
		mm1=endtime.substring(3,5);
		if(eval(hh1)<eval(hh))
		{
			return false;
		}
		if(eval(hh)==eval(hh1))
		{
			if(eval(mm1)<eval(mm))
			{
				return false;
			}
		}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
//	Function Name : is_valid_userid
//	Prototype : is_valid_userid(str_userid)
//	Input Parameter : str_userid
// 	Return Value : Boolean
//	Purpose : It will Check for valid password string 
// 	Usage : is_valid_userid(str_userid)
   function is_valid_userid(str_userid)
   {
   		var userid_string="";
		userid_string = trim(str_userid);
		var userid_reg_expr = new RegExp('^[a-zA-Z0-9][a-zA-Z0-9_]*$');
		if(trim(userid_string)=="")
		{
			alert("Please enter login id.");
			return false;
		}	
		if(userid_string.indexOf(" ")!=-1)
		{			
			alert("Please do not enter blank space in login id.");
			return false;
		}		
		if(userid_reg_expr.test(str_userid)==false)
		{
			alert("Login id should contain only a-z, A-Z, 0-9 characters and underscore.");
			return false;
		}
		return true;
   }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
