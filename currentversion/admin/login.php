<?php
/*
	Module Name:- Login
	File Name  :- login.php
	Create Date:- 09-12-2016
	Intially Create By :- 0022
	Update History:
*/
?>
<?php
session_start();
include "../includes/configuration.php";
include "../includes/lib_common.php";
?>
<html>
<head>
    <link rel="shortcut icon" href="../../currentversion/favicon.ico">    
<title><?php print($STR_SITE_TITLE); ?> : <?php print($STR_TITLE_ADMIN_LOGIN); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/admin.css" rel="stylesheet">
</head>

<?php
#Get message type.
$str_type = "";
$str_message = "";	
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
	case("E"): $str_type = "E"; break;
	case("W"): $str_type = "W"; break;
    }
}
#	Display message.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
	case("IS"): $str_message = $STR_MSG_ACTION_INVALID_SCODE;break;
	case("IP"): $str_message ="ERROR !!! Invalid User name and / or Password."; break;
	case("SP"): $str_message ="ERROR !!! Too short: Minimum of '8' characters in Password."; break;
	case("LP"): $str_message ="ERROR !!! Too Long: Maximum of '20' characters in Password."; break;
        case("CP"): $str_message = "ERROR !!! Admin panel login is restricted at present."; break;
        case("SU"): $str_message = "ERROR !!! Invalid sum. Please enter answer of sum in words."; break;
        case("RUH"): $str_message = "ERROR !!! Are you Human? Please try again after some time."; break;
    }
}
?>
<body style="background-color:#f2f2f2">
<div class="container" >
<br/>
	<?php /*?><header><?php include "./includes/adminheaderroot.php"; ?></header><?php */?>
		<div class="row">
			<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
				
				<?php	if($str_type != "" && $str_message != "") { print(DisplayMessage("99%",$str_message,$str_type)); } ?>
				<div class="login-panel panel panel-default"><br/>
                                    <div align="center" class="login-pg-logo"><img src="../images/logo.png" class="img-responsive img-padding" alt="" title=""/></div>
                                    
				<h4 align="center" ><b><?php print($STR_TITLE_ADMIN_LOGIN); ?></b></h4><hr/>
					<?php /*?><div class="panel-heading">
						<h3 class="panel-title"><b>&nbsp;&nbsp;Enter Login Details</b></h3>
					</div><?php */?>
					<div class="panel-body">
						<form name="frm_login" action="login_p.php" method="post" onSubmit="return validate_frm_login();">
							<div class="form-group text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
							<fieldset>
								<div class="form-group input-group">
                                      <span class="input-group-addon"><i class=" fa fa-user text-primary"></i></span>
                                       <input name="txt_adminid" type="text" class="form-control input-sm" value="" tabindex="1" placeholder="Admin ID*" >
                               </div>
							   <div class="form-group input-group">
                                      <span class="input-group-addon"><i class=" fa fa-lock text-primary"></i></span>
									   <input name="pas_admin_password" type="password" class="form-control input-sm"  value="" tabindex="2"  placeholder="Password*" >
                               </div>
							   	<?php /*?> To hide Secret code and Security Question on local server <?php */?>
							   <?php /*?><?php if($APP_RUN_MODE == "SERVER") { ?>
							   <p class="HelpText">(<?php print($STR_SECERT_CODE);?>)</p>
							   <div class="form-group input-group">
                                      <span class="input-group-addon"><i class="fa fa-pencil-square-o"> </i></span>
									   <input type="text" name="txt_code" class="form-control input-sm" tabindex="3" minlength="7" maxlength="7"  placeholder="Secret Code*">
									   <span class="input-group-addon input-group-addon-no-padding"><img src="../includes/get_unique_image.php" border="0" alt="Secret Code Image" align="top"/></span>
                               </div>
								 <p class="HelpText">(enter answer in words)</p>
								  <div class="form-group input-group">
                                      <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
									   <input type="text" name="txt_spamfilterq" class="form-control input-sm" tabindex="4" placeholder="What is sum of 5 + 4 + 3 ? *" ><input type="hidden" value="twelve" name="hdn_spamfiltera"  >
                               </div>
							   <?php }?>	<?php */?>						   
							   
                                                                 <button name="btn_submit" title="Submit" value="Login" class="btn btn-primary btn-block" type="submit" tabindex="5"><i class="fa fa-sign-in"></i>&nbsp;<b>Login</b></button>
							</fieldset>
						</form>
					</div>
				</div>
		</div>
	</div>
	<footer> <?php //include "./includes/adminfooterroot.php"; ?></footer>
</div>
<?php //include "../includes/footer_admin.php"; ?>
<script src="../js/bootstrap.min.js"></script> 
<script src="../js/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="../js/functions.js"></script>
<script language="javascript" type="text/javascript" src="../js/login.js"></script>
</body></html>
