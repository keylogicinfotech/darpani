<?php 
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_app_specific.php";
#-------------------------------------------------------------------------------------------------------------------------------------------------
$int_counter = "";
#-------------------------------------------------------------------------------------------------------------------------------------------------
# To check wheather values are passed properly or not.
if (isset($_POST["hdn_counter"]))
{
    $int_counter=trim($_POST["hdn_counter"]);
}

//print $int_counter; exit;
if($int_counter == "" || $int_counter < 0 || !is_numeric($int_counter))
{
    CloseConnection();		
    Redirect("item_list.php?msg=F&type=E#ptop");
    exit();
}

#-------------------------------------------------------------------------------------------------------------------------------------------------
# Set display orders
for($i=1;$i<$int_counter;$i++)
{

    if (trim($_POST["hdn_pkid" .$i]) !=""  && is_numeric(trim($_POST["hdn_pkid" .$i]))==true  && trim($_POST["hdn_pkid" .$i])>0 && trim($_POST["txt_displayorder" . $i])!= "" && is_numeric(trim($_POST["txt_displayorder" . $i]))==true && trim($_POST["txt_displayorder" . $i])>=0)
        {
            $str_query_update = "UPDATE " .$STR_DB_TABLE_NAME. " SET displayorder=" . trim($_POST["txt_displayorder" . $i]) . " WHERE pkid=" . trim($_POST["hdn_pkid" . $i]);
            ExecuteQuery($str_query_update);		
        }
    }

#------------------------------------------------------------------------------------------------------------------------------------------------
#write xml file
WriteXML();		
#------------------------------------------------------------------------------------------------------------------------------------------------
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_list.php?msg=O&type=S&#ptop");
exit();
?>