<?php
/*
	Module Name:- moddomain
	File Name  :- domain_visible_p.php
	Create Date:- 3-jun-2013
	Intially Create By :- 0014
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
	include "./cm_home_content_app_specific.php";

#get the data
#----------------------------------------------------------------------------------------------------
	# filter variables
	$int_page="";
	$int_cat_pkid="";
	$str_mname="";
#----------------------------------------------------------------------------------------------------
#	get filter data


//print_r($_POST);exit;
if(isset($_POST["catid"]) && trim($_POST["catid"])!="" )
{
	$int_cat_pkid=trim($_POST["catid"]);
}
if(isset($_POST["key"]) && trim($_POST["key"])!="" )
{
	$str_mname=trim($_GET["key"]);
}
#	get paging data
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"])!="" && is_numeric($_POST["PagePosition"])==true && trim($_POST["PagePosition"])>0)
{
	$int_page= $_POST["PagePosition"];
}
else
{
	$int_page=1;
}
$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_mname."&PagePosition=".$int_page;

$counter="";
if (isset($_POST["hdn_counter"]))
{
	$counter=$_POST["hdn_counter"];
}
#To check required parameters are passed properly or not
if($counter <=0 || is_numeric($counter) == false)
{
	CloseConnection();
	Redirect("cm_home_content.php?msg=F&type=E&".$str_filter."#ptop");
	exit();
}

$strpkid="0,";$int_total=0;
$strpkid_notset="0,";
for ($i=1;$i<$counter;$i++)
{
	if (isset($_POST["chk_displayontop" . $i]))
	{
		if (($_POST["chk_displayontop" . $i])=="on")
		{
			$strpkid=$strpkid . $_POST["hdn_pkid" . $i] . ",";
			$int_total++;
		}
	}else
	{
		$strpkid_notset=$strpkid_notset.$_POST["hdn_pkid".$i].",";
	}

}
$strpkid=substr($strpkid,0,strrpos($strpkid,","));
$strpkid_notset=substr($strpkid_notset,0,strrpos($strpkid_notset,","));
//print $int_total;exit;
if($int_total > 3){
		CloseConnection();
		Redirect("cm_home_content.php?msg=NV&type=E&".$str_filter."#ptop");
		exit();
}
#------------------------------------------------------------------------------------------------
#update t_domainname table;
$strqry="";
$strqry="UPDATE cm_home_content SET displayontop='NO' WHERE pkid in (".$strpkid_notset.")";
ExecuteQuery($strqry);

$strqry2="";
$strqry2="UPDATE cm_home_content SET displayontop='YES' WHERE pkid in (" . $strpkid . ")";
ExecuteQuery($strqry2);

#-----------------------------------------------------------------------------------------------------------
#write xml file
WriteXml();		
#------------------------------------------------------------------------------------------------------------
#Close connection and redirect to prod_list.php page	
CloseConnection();
Redirect("cm_home_content.php?type=S&msg=SOT&".$str_filter."#ptop");
exit();
?>