<?php
/*
File Name  :- item_order_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_cnt="";
if (isset($_POST["hdn_counter"]))
{
        $int_cnt=trim($_POST["hdn_counter"]);
}	
if($int_cnt=="" || $int_cnt<0 || !is_numeric($int_cnt))
{
        CloseConnection();
        Redirect("item_list.php?msg=F&type=E&#ptop");
        exit();
}
#----------------------------------------------------------------------------------------------------
#Update data in table
for($i=1;$i<$int_cnt;$i++)
{
        if(trim($_POST["txt_price".$i]) != "" && trim($_POST["txt_price".$i]) >=0 && is_numeric(trim($_POST["txt_price".$i]))==true  && trim($_POST["hdn_pkid".$i])!="" && trim($_POST["hdn_pkid".$i])>0 && is_numeric(trim($_POST["hdn_pkid".$i]))==true)
        {
            $str_query_update="UPDATE ".$STR_DB_TABLE_NAME." SET price='".trim($_POST["txt_price".$i])."' WHERE pkid=".trim($_POST["hdn_pkid".$i]);
            ExecuteQuery($str_query_update);
        }
}
#----------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#----------------------------------------------------------------------------------------------------
# Update price in tr (transaction) table
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME." ".$STR_DB_TABLE_NAME_ORDER_BY." ";
$rs_list=GetRecordset($str_query_select);

while(!$rs_list->EOF()==true) 
{
	while(!$rs_list->EOF()==true) 
	{
		$str_query_update  = "";
		$str_query_update  = "UPDATE ".$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE." SET ";
		$str_query_update .= "price=".$rs_list->fields("price")." ";
		$str_query_update .= " WHERE masterpkid=".$rs_list->fields("pkid");
		//print $str_query_update."<br/>"; 
		ExecuteQuery($str_query_update);
                    
	$rs_list->MoveNext(); 
	}
$rs_list->MoveNext(); 
}
//exit;
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to listing page	
CloseConnection();
Redirect("item_list.php?type=S&msg=O&#ptop");
exit();
#----------------------------------------------------------------------------------------------------
?>
