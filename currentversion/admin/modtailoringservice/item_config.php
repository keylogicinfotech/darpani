<?php
$STR_TITLE_PAGE = "Tailoring Service List";
$UPLOAD_IMG_PATH = "../../mdm//";
$INT_IMG_WIDTH = 400;
$INT_IMG_HEIGHT = 200;

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "t_store_tailoringservice"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY displayorder DESC, title ASC "; 

# For Tailoring Services    
global $STR_DB_TR_TABLE_NAME_TAILORING_SERVICE;
global $STR_DB_TR_TABLE_NAME_ORDER_BY_TAILORING_SERVICE;
$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE = "tr_store_tailoringservice"; 
$STR_DB_TR_TABLE_NAME_ORDER_BY_TAILORING_SERVICE = " ORDER BY title ASC ";

/*global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "cms_occasion";

global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/occasion.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/occasion_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";	*/
?>
