<?php
$STR_TITLE_PAGE = "Privacy List";
$UPLOAD_IMG_PATH = "../../mdm/privacypolicy/";
$INT_IMG_WIDTH = 400;
$INT_IMG_HEIGHT = 200;

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "t_privacy_policy"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY displayorder DESC, title ASC "; 

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "cms_privacy_policy";

global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/privacy.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/privacy_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";	
?>
