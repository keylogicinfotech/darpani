function frm_edit_validateform()
{
    with(document.frm_edit)
    {
        
        if(trim(txt_title.value) == "")
        {
            alert("Please enter title.");
            txt_title.select();
            txt_title.focus();
            return false;
        }
        if(trim(ta_desc.value) == "" && trim(ta_desc.value) == "<br>")
        {
            alert("Please enter description.");
            ta_desc.select();
            ta_desc.focus();
            return false;
        } 
        /*if(trim(fileimage.value) != "")
        {
            if(checkExt(trim(fileimage.value))==false)
            {
                fileimage.select();
                fileimage.focus();
                return false;
            }
        }
        if(cbo_visible.value == "")
        {
            alert("Please set value for visible.");
            cbo_visible.focus();			
            return false;
        }*/
        
    }
    return true;
}
// Delete confirmation message
function Confirm_Deletion()
{
    if(confirm("Are you sure you want to delete this image permanently."))
    {
        if(confirm("Confirm Deletion:Click 'OK' to delete this image or 'Cancel' to deletion."))
        {
            return true;
        }
    }
    return false;
}
