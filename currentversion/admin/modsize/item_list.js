// Add FORM validation
function frm_add_validateform()
{
    with(document.frm_add)
    {
        if(trim(txt_title.value) == "")
        {
            alert("Please enter title.");
            txt_title.select();
            txt_title.focus();
            return false;
        }
//        if(trim(ta_desc.value) == "" && trim(ta_desc.value) == "<br>")
//        {
//            alert("Please enter description.");
//            ta_desc.select();
//            ta_desc.focus();
//            return false;
//        } 
        if(cbo_visible.value == "")
        {
            alert("Please set value for visible.");
            cbo_visible.focus();			
            return false;
        }        
    }
	return true;
}
// Display Order validation
function frm_list_check_displayorder()
{
    with(document.frm_list)
    {
        cnt=hdn_counter.value;

        for(i=1;i<cnt;i++)
        {
            if(isEmpty(eval("txt_displayorder" + i).value))
            {
                alert("Please enter display order.");
                eval("txt_displayorder" + i).focus();
                return false;
            }
            if(isNaN(eval("txt_displayorder" + i).value))
            {
                alert("Please enter numeric value for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }
            if(eval("txt_displayorder" + i).value<0)
            {
                alert("Please enter positive integer for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }
        }

    }
	return true;
}
// Delete confirmation message
function confirm_delete()
{
    if(confirm("Are you sure you want to delete this details."))
    {
        if(confirm("Confirm Deletion:Click 'OK' to delete this details or 'Cancel' to deletion."))
        {
            return true;
        }
    }
    return false;
}
