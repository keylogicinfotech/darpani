<?php
/*
	Module Name:- admmodule
	File Name  :- adm_module_add_p.php
	Create Date:- 18-APR-2006
	Intially Create By :- 0025
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "./validate_adm_login.php";
	include "../../includes/configuration_admin.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
#get post data
$str_title="";
$str_type="";
$str_url="";
$str_tooltip="";
$str_instruction="";
$str_pos="";
$str_cbo_display="";
$str_visible="";
$str_new="";
if(isset($_POST["txt_title"]))
{
	$str_title = trim($_POST["txt_title"]);
}
if(isset($_POST["cbo_visible"]))
{
	$str_visible = trim($_POST["cbo_visible"]);
}
if(isset($_POST["cbo_newwin"]))
{
	$str_new = trim($_POST["cbo_newwin"]);
}
if(isset($_POST["cbo_type"]))
{
	$str_type = trim($_POST["cbo_type"]);
}	
if(isset($_POST["txt_url"]))
{
	$str_url = trim($_POST["txt_url"]);
}
if(isset($_POST["txt_tooltip"]))
{
	$str_tooltip = trim($_POST["txt_tooltip"]);
}
if(isset($_POST["ta_instruction"]))
{
	$str_instruction = trim($_POST["ta_instruction"]);
}
if(isset($_POST["cbo_position"]))
{
	$str_pos = trim($_POST["cbo_position"]);
}
if(isset($_POST["cbo_display"]))
{
	$str_cbo_display = trim($_POST["cbo_display"]);
}
#----------------------------------------------------------------------------------------------------
#Create String For Redirect

$str_redirect="";
$str_redirect .= "&modtitle=". urlencode(RemoveQuote($str_title));
$str_redirect .= "&cattitle=". urlencode(RemoveQuote($str_title));
$str_redirect .= "&modtype=". urlencode(RemoveQuote($str_type));
$str_redirect .= "&url=". urlencode(RemoveQuote($str_url));
$str_redirect .= "&tooltip=". urlencode(RemoveQuote($str_tooltip));
$str_redirect .= "&instruction=". urlencode(RemoveQuote($str_instruction));
$str_redirect .= "&position=". urlencode(RemoveQuote($str_pos));
$str_redirect .= "&display=". urlencode(RemoveQuote($str_cbo_display));
$str_redirect .= "&vis=". urlencode(RemoveQuote($str_visible));
$str_redirect .= "&nwin=". urlencode(RemoveQuote($str_new));

#----------------------------------------------------------------------------------------------------
#To check required parameters are passed properly or not

if($str_title =="" || $str_type=="")
{
	CloseConnection();
	Redirect("adm_module_list.php?msg=F&type=E". $str_redirect."&#ptop");
	exit();
}

if($str_type!=0)
{
	if($str_url=="")
	{
		CloseConnection();
		Redirect("adm_module_list.php?msg=F&type=E". $str_redirect."&#ptop");
		exit();
	}
}
if($str_type==0)
{
	if($str_pos=="")
	{
		CloseConnection();
		Redirect("adm_module_list.php?msg=F&type=E". $str_redirect."&#ptop");
		exit();
	}
	if($str_cbo_display=="")
	{
		CloseConnection();
		Redirect("adm_module_list.php?msg=F&type=E". $str_redirect."&#ptop");
		exit();	
	}
}	
#----------------------------------------------------------------------------------------------------
/* #Duplicate Checking for category Title field.

$str_query_dup="select moduletitle from t_module where modulepkid=".$str_type." and moduletitle= '" . ReplaceQuote($str_title) . "'";
$rs_duplicate_check=GetRecordSet($str_query_dup);

if($rs_duplicate_check->eof() !=true)
{
	CloseConnection();
	$msg="DU";
	if($str_type==0)
	{
		$msg="DU";
	}
	else
	{
		$msg="SDU";
	}
	Redirect("module_list.php?msg=".$msg."&type=E&". $str_redirect."&#ptop");
	exit();
}
 */#----------------------------------------------------------------------------------------------------
#Insert Record In a t_module table
	if($str_cbo_display==0 && $str_type==0)
	{
		$int_max=0;
		$int_max=GetSubcatMaxValue("t_module","moduleparentpkid",$str_type,"displayorder");
	}	
	else
	{
		$int_max=($str_cbo_display+1);
		$str_update="update t_module set displayorder=displayorder+1 where moduleparentpkid=0 and displayorder >".$str_cbo_display;
		ExecuteQuery($str_update);
	}
$int_level="";
$msg='S';
if($str_type==0)
{
	$int_parentpkid=0;
	if($str_pos=="")
	{
		$str_pos="LEFT";
	}
	$msg="S";

}
else
{
	$int_parentpkid=$str_type;
	$str_pos="";
	$msg="SS";
}

	$str_query_insert = "insert into t_module(moduleparentpkid,moduletitle,displayorder,url,tooltip,instructiontext,position,visible,openinnewwindow)";
	$str_query_insert .= " values(" . $int_parentpkid . ",";
	$str_query_insert .= "'" . ReplaceQuote($str_title) . "',";
	$str_query_insert .= $int_max .",";
	$str_query_insert .= "'" . ReplaceQuote($str_url) . "',";
	$str_query_insert .= "'" . ReplaceQuote($str_tooltip) . "',";
	$str_query_insert .= "'" . ReplaceQuote($str_instruction) . "',";
	$str_query_insert .= "'" . ReplaceQuote(strtoupper($str_pos)) . "','".ReplaceQuote($str_visible)."','".ReplaceQuote($str_new)."')";

ExecuteQuery($str_query_insert);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to adm_module_list.php page	
	CloseConnection();
	Redirect("adm_module_list.php?type=S&msg=".$msg."&title=".urlencode(RemoveQuote($str_title))."&#ptop");
	exit();
#------------------------------------------------------------------------------------------------------------
?>