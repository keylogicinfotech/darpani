<?php
/*
	Module Name:- Login
	File Name  :- login_p.php
	Create Date:- 22-Jun-2006
	Intially Create By :-0015
	Update History:
*/	
#---------------------------------------------------------------------------------------------	
	session_start();
	include "../../includes/configuration.php";
	include "./adm_module_config.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";        
#---------------------------------------------------------------------------------------------	
#------------------------------------------------------------------------------------------------------------------------------------------------
#	to get values from login page.	
	if(isset($_POST["txt_adminid"]))
	{
		$str_adminid = trim($_POST["txt_adminid"]);
	}
	if(isset($_POST["pas_admin_password"]))
	{
		$str_password = trim($_POST["pas_admin_password"]);
	}
#---------------------------------------------------------------------------------------------	
#	to check whether both values are passed properly or not.
	if($str_adminid == "" || $str_password == "")
	{
		CloseConnection();
		Redirect("adm_module_login.php?msg=F&type=E");
		exit();
	}
#---------------------------------------------------------------------------------------------		
#	select query from t_siteadmin to get the siteadminusername contain admin value.
	$str_query_select = "SELECT * FROM t_module_login WHERE loginid='" . $str_adminid . "'";

#	to open recordset using query
	$rs_admin = GetRecordSet($str_query_select);
		//print $str_query_select;exit;

#	to check whether recordset is empty or not
   if($rs_admin->fields("allowlogin") == 'NO') //Record Not Found For Given UserName And Password
    {  
	  CloseConnection();
	  Redirect('adm_module_login.php?msg=AN&type=E'); //Redirect Response Back To Default Page
	  exit();
	}
	
	
#---------------------------------------------------------------------------------------------		
	$enc_text = $rs_admin->fields("loginpassword");
	$pkid=$rs_admin->fields("pkid");
	$admname=$rs_admin->fields("loginid");	
	//print $pkid;
	//print $admname;exit;
	$enc_text = md5_decrypt($enc_text, $STR_MODULE_ENCRYPTION_KEY);
	//echo "encrypted text is: ".$enc_text."<br>";
	//print $enc_text;exit;
		$_SESSION['adminpkid'] = $pkid;
		$_SESSION['adminname'] = $admname;
	if($str_password==$enc_text)
	{
		//Record Found For Given UserName And Password
		$_SESSION['admloginid'] = $rs_admin->fields("loginid"); //Create Session Variable For Session Validation
		if($rs_admin->fields("issuperadmin") == "YES")
	{
		$_SESSION['superadmin'] = 'YES';
	}
	else
	{
		$_SESSION['superadmin'] = 'NO';
	}
	}
	else
	{
		CloseConnection();
		Redirect('adm_module_login.php?msg=IP&type=E'); //Redirect Response Back To Default Page
		exit();
	}
#---------------------------------------------------------------------------------------------		
	//close the connection
	CloseConnection();
	redirect('./adm_home.php');
	exit();	
?>
