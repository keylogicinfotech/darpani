<?php
/*
	Module Name:- admin module
	File Name  :- adm_module_visible_p.php
	Create Date:- 21-Jun-2006
	Intially Create By :- 0015
	Update History:
*/

#----------------------------------------------------------------------------------------------------
#Include Files
	include "./validate_adm_login.php";
	include "../includes/configuration.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
	include "../includes/lib_datetimeyear.php";
#----------------------------------------------------------------------------------------------------------------------------	

#get the data
$int_cnt=0;
	$int_pkid=0;$int_modid=0;
	if(isset($_POST['hdn_parent']))
	{
		$int_pkid=$_POST['hdn_parent'];
	}
	if(isset($_POST['hdn_module']))
	{
		$int_modid=$_POST['hdn_module'];
	}
	if(isset($_POST['hdn_counter']))
	{
		$int_cnt=$_POST['hdn_counter'];
	}
	//print($int_modid);exit;
#----------------------------------------------------------------------------------------------------
#get post data
$str_empname="";
$str_work_type="";
$str_description="";
$str_pos="";
$str_cbo_display="";
$str_empname=$_SESSION['adminname'];
/*$str_new="";
if(isset($_POST["txt_empname"]))
{
	$str_empname = trim($_POST["txt_empname"]);
}*/
if(isset($_POST["cbo_work_type"]))
{
	$str_work_type = trim($_POST["cbo_work_type"]);
}
if(isset($_POST["ta_desc"]))
{
	$str_description = trim($_POST["ta_desc"]);
}


$str_disable="";
$str_query_select="SELECT * FROM t_work_history where modulepkid=".$int_modid;
$rs_list=GetRecordSet($str_query_select);
for($i=0;$i<=$int_cnt;$i++)
{
	if($rs_list->fields("worktype")=="CREATED")
	 { $str_disable="disabled"; } 
}
#Create String For Redirect

$str_redirect="";
$str_redirect .= "&empname=". urlencode(RemoveQuote($str_empname));
$str_redirect .= "&worktype=". urlencode(RemoveQuote($str_work_type));
$str_redirect .= "&description=". urlencode(RemoveQuote($str_description));
#----------------------------------------------------------------------------------------------------------------------------
if($str_empname == "" || $str_work_type == "" || $str_description=="")
	{
		CloseConnection();
		Redirect("adm_module_update_history.php?type=E&msg=F&parentpkid=".$int_pkid."&modpkid=".$int_modid);
		exit();
	}

#----------------------------------------------------------------------------------------------------------------------------	
	$str_query_insert = "insert into t_work_history(employeename,moduleparentpkid,modulepkid,worktype,updatedate,description)";
	$str_query_insert .= " values('" . ReplaceQuote($str_empname) . "',";
	$str_query_insert .= $int_pkid.",";
	$str_query_insert .= $int_modid .",";
	$str_query_insert .= "'" . ReplaceQuote($str_work_type) . "',";
	$str_query_insert .="'". ReplaceQuote(date("Y-m-d h:i:s")) ."',";
	$str_query_insert .= "'" . ReplaceQuote($str_description) . "')";
$select=ExecuteQuery($str_query_insert);
//print ($str_query_insert);exit;
#----------------------------------------------------------------------------------------------------------------------------	
	CloseConnection();
	/*Redirect("adm_module_update_history.php?type=S&msg=V&mode=".urlencode(RemoveQuote($str_visible))."&title=".urlencode(RemoveQuote($str_title)));*/
	Redirect("adm_module_update_history.php?type=S&msg=S&parentpkid=".$int_pkid."&modpkid=".$int_modid);
	exit();
?>