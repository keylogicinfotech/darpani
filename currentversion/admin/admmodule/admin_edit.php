<?php
/*
	Module Name:- modsiteadmin
	File Name  :- sa_edit.php
	Create Date:- 01-02-2006
	Intially Create By :- 0022
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	
	include "../../includes/configuration.php";
	include "./validate_adm_login.php";
	include "./adm_module_config.php";
	//include "./sa_config.php";
	include "./adm_module_app_specific.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_datetimeyear.php";
	include "../../includes/lib_common.php";
/*	if (strtoupper($_SESSION['superadmin'])=="NO")
	{ 
		CloseConnection();
		Redirect("../admin_home.php");
		exit();
	}*/
#	to get value of adminid...
	
	$int_pkid="";
	if(isset($_GET["id"])) { $int_pkid = $_GET["id"]; }
#	to check whether values are passed properly or not
	if($int_pkid == "" || !is_numeric($int_pkid) || $int_pkid<=0)
	{	CloseConnection();
		Redirect("./adm_module_admin_list.php?type=E&msg=F");
		exit();
	}

	# 	Select query from t_siteadmin to get record.
	$str_query_select = "select * from t_module_login where pkid=" . $int_pkid;
	$rs_admin_list = GetRecordSet($str_query_select);	
	
	$str_type = "";
	$str_message = "";
#	Get message type.
	if(isset($_GET["type"]))
	{
		switch(trim($_GET["type"]))
		{
			case("S"): $str_type = "S"; break;
			case("E"): $str_type = "E"; break;
			case("W"): $str_type = "W"; break;
		}
	}
#	Get message text.
	if(isset($_GET["msg"]))
	{
		switch($_GET["msg"])
		{
			case("F"): 	$str_message = $STR_MSG_ACTION_INFO_MISSING; break;
			case("EMAIL"):	$str_message = $STR_MSG_ACTION_INVALID_EMAIL; break;	
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE_MODULE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
	<?php include "./adm_module_header.php"; ?>
	<div class="row">
		<div class="col-lg-3 col-sm-6 col-xs-12 button_space">
			<div class="btn-group" role="group" aria-label="...">
				<a href="./adm_module_admin_list.php" class="btn btn-default" title="<?php print($STR_HREF_TITLE_LINK);?> <?php print($STR_TITLE_PAGE_MODULE); ?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_MODULE); ?></a>
			</div>
		</div>
		<div class="col-lg-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE_MODULE); ?></h3></div>
	</div><hr> 
	<?php #	Diplay message.
		if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); }?>
	<div class="row">
    	<div class="col-lg-12">
        	<div class="panel-group" id="accordion">
            	<div class="panel panel-default">
                	<div class="panel-heading">
                    	<h4 class="panel-title">
							<div class="row">
								<div class="col-md-12  padding-10">
									<div class="col-md-6 col-sm-6 col-xs-8"> 
                                                                            <i class="glyphicon  glyphicon-edit "></i>&nbsp;&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b> </a>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>												
								</div>
							</div>
						</h4>
                    </div>
                    <div id="collapse1" class="panel-collapse">
						<div class="panel-body">
							<form name="frm_admin_edit" action="admin_edit_p.php" method="post">
								<div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
								<div class="form-group">
									<label>Admin ID</label><br>
									<span class="text-info"><strong><?php print(MyHtmlEncode(RemoveQuote($rs_admin_list->fields("loginid")))); ?></strong></span>
								  <input name="hdn_admin_name" type="hidden" value="<?php print(MyHtmlEncode(RemoveQuote($rs_admin_list->fields("loginid"))))?>">
								</div>
								<div class="form-group">
									<label>Password</label><span class="text-help-form"> *</span>
									<input type="text" tabindex="1" placeholder="<?php print($STR_PASSWORD);?>" name="txt_password" value="<?php print(md5_decrypt(RemoveQuote($rs_admin_list->fields("loginpassword")), $STR_MODULE_ENCRYPTION_KEY))?>"  size="50" maxlength="20" class="form-control input-sm">
								</div>
								<?php
								if($_SESSION['superadmin'] == "YES"){?>
								<div class="form-group">
									<label>Is Super Admin</label><span class="text-help-form"> *</span>
									<span class="text-help-form">(<?php print($STR_ADMIN_MSG)?>)</span>
									 <select name="cbo_super" class="form-control input-sm" tabindex="5">
									 <option value="NO" <?php print(CheckSelected("NO",strtoupper($rs_admin_list->fields("issuperadmin")))); ?>>NO</option>
									 <option value="YES" <?php print(CheckSelected("YES",strtoupper($rs_admin_list->fields("issuperadmin")))); ?>>YES</option>
									 </select>
								</div>
								<?php }?>
								<input name="hdn_id" type="hidden" value="<?php print($int_pkid) ?>"><input type="hidden" name="hdn_alwlgn" value="<?php print($rs_admin_list->fields("allowlogin"));?>">
								<button type="submit" class="btn btn-success" id="btn_submit_add2" title="<?php print($STR_BUTTON_TITLE_FORM_EDIT); ?>" onClick="return frm_admin_edit_validation();" tabindex="6"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</button>&nbsp;
								<input name="btn_reset" id="btn_reset" title="<?php print($STR_BUTTON_TITLE_FORM_RESET); ?>" value="Reset" class="btn btn-danger" type="reset" tabindex="7">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <a name="help"></a>
	<?php include "../../includes/help_for_edit.php"; ?>
</div>
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/admin.css" rel="stylesheet">
<script language="JavaScript" src="../../js/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="./admin_edit.js" type="text/javascript"></script>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
</body></html>