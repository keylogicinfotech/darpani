/*
	Module Name:- modsiteadmin
	File Name  :- admin_add.js
	Create Date:- 06-02-2017
	Intially Create By :- 0022
	Update History:
*/

function frm_admin_add_validation()
{
	with(document.frm_admin_add)
	{
		if(isEmpty(txt_admin_name.value))
		{
			alert("Please enter Admin ID.");
			txt_admin_name.select();
			txt_admin_name.focus();
			return false;
		}
		if(is_valid_userid(trim(txt_admin_name.value)) == false)
		{
			txt_admin_name.select();
			txt_admin_name.focus();
			return false;
		}
		if(isEmpty(pas_password.value))
		{
			alert("Please enter password.");
			pas_password.select();
			pas_password.focus();
			return false;
		}
		
		if(isValidPassword(trim(pas_password.value))==false)
		{
			return false;
		}
		if (checkSpace(pas_password.value))
		{
			alert("Please do not enter blank space in Password.");
			pas_password.select();
			pas_password.focus();
			return false;
		}
		if(pas_password.value.length < 6)
		{
			alert("Please enter at least 6 characters in password.");
			pas_password.select();
			pas_password.focus();
			return false;
		}
		
	}
	return true;
}

function confirm_delete()
{	if(confirm("Are you sure you want to delete this admin details?"))
	{
		if(confirm("Click 'Ok' to delete this admin details or 'Cancel' to deletion."))
		{
			return true;
		}
	}
	return false;
}



