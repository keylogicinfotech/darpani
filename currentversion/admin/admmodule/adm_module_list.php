<?php
/*
	Module Name:- admmodule
	File Name  :- module_list.php
	Create Date:- 18-APR-2006
	Intially Create By :- 0025
	Update History:
*/
#----------------------------------------------------------------------------------------------------
# Include Files
	include "./validate_adm_login.php";
	include "../../includes/configuration_admin.php";
	include "./adm_module_config.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------

/*$int_pkid="";
	if(isset($_GET["id"])) { $int_pkid = $_GET["id"]; }
#	to check whether values are passed properly or not
	if($int_pkid == "" || !is_numeric($int_pkid) || $int_pkid<=0)
	{	CloseConnection();
		Redirect("./adm_module_list.php?type=E&msg=F");
		exit();
	}*/

# Select Query to display list
	$str_query_select = "SELECT a.visible AS pvisible,b.visible AS cvisible,a.openinnewwindow,a.modulepkid modid,a.moduleparentpkid modparentid,a.moduletitle modname,a.displayorder modorder,a.position modpos,";
	$str_query_select .= "a.url modurl,a.tooltip modtip,a.instructiontext modinstruction,";
	$str_query_select .= "b.modulepkid subid,b.moduleparentpkid subparentid,b.moduletitle subname,b.position subpos,b.displayorder suborder,";
	$str_query_select .= "b.url suburl,b.tooltip subtip,b.instructiontext subinstruction";
	$str_query_select .= " FROM t_module a LEFT JOIN t_module b";
	$str_query_select .= " ON a.modulepkid=b.moduleparentpkid WHERE a.moduleparentpkid=0";
	$str_query_select .= " ORDER BY pvisible DESC, a.position,a.moduleparentpkid,a.displayorder ASC,a.modulepkid,b.displayorder ASC,b.moduletitle";
	$rs_list=GetRecordSet($str_query_select);
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	$str_query_select="SELECT moduletitle,position,moduleparentpkid FROM t_module";
	$rs_position=GetRecordSet($str_query_select);

# 	Select query from t_siteadmin to get record.
$str_query_select = "select * from t_module_login where loginid='" .$_SESSION['adminname']."'";
$rs_admin_list = GetRecordSet($str_query_select);	
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******************************************/
/*$plain_text = 'password';
$password = 'ASSIDUOUS';
//$password = '111';
echo "plain text is: [${plain_text}]<br />\n";
echo "password is: [${password}]<br />\n";

$enc_text = md5_encrypt($plain_text, $password);
echo "encrypted text is: [${enc_text}]<br />\n";

$plain_text2 = md5_decrypt($enc_text, $password);
echo "decrypted text is: [${plain_text2}]<br />\n";	//print "<br>".md5($c);*/
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# get Query String Data
	$str_title="";
	$str_title1="";
	$str_mode="";
	$str_modname="";
	$str_modtype="";
	$str_url="";
	$str_tooltip="";
	$str_instruction="";
	$str_pos="";
	$str_display="";
	$str_visible="";
	$str_pos="";
	if (isset($_GET["title"]))
	{
		$str_title = trim($_GET["title"]);
	}
	if (isset($_GET["mode"]))
	{
		$str_mode = trim($_GET["mode"]);
	}
	if($str_mode=="YES")
	{
		$str_visible="Visible";
	}
	else
	{
		$str_visible="Invisible";
	}
	if (isset($_GET["title1"]))
	{
		$str_title1 = trim($_GET["title1"]);
	}
	if (isset($_GET["modname"]))
	{
		$str_modname = trim($_GET["modname"]);
	}
	if (isset($_GET["modtype"]))
	{
		$str_modtype = trim($_GET["modtype"]);
	}
	if (isset($_GET["url"]))
	{
		$str_url = trim($_GET["url"]);
	}
	if (isset($_GET["tooltip"]))
	{
		$str_tooltip = trim($_GET["tooltip"]);
	}
	if (isset($_GET["position"]))
	{
		$str_pos = trim($_GET["position"]);
	}
	if (isset($_GET["display"]))
	{
		$str_display = trim($_GET["display"]);
	}
	
	$str_cattitle="";
	if (isset($_GET["cattitle"]))
	{
		$str_cattitle = trim($_GET["cattitle"]);
	}
	if (isset($_GET["po_mode"]))
	{
		$str_pos = trim($_GET["po_mode"]);
	}
#-----------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
   	$str_type = "";
	$str_message = "";
#	Get message type.
   		if(isset($_GET["type"]))
		{
			switch(trim($_GET["type"]))
			{
				case("S"): $str_type = "S"; break;
				case("E"): $str_type = "E"; break;
				case("W"): $str_type = "W"; break;
			}
		}
#	Get message text.
		if(isset($_GET["msg"]))
		{
			switch(trim($_GET["msg"]))
			{
				case("F"): $str_message = "Some information(s) missing.Please try again."; break;
				case("FMI"): $str_message = "Master module '". RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) ."' is invisible. So you can not make '". RemoveQuote(MyHtmlEncode(MakeStringShort($str_title1,30))) ."' visible."; break;
				case("S"): $str_message = "Module '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' details added successfully."; break;
				case("V"): $str_message = "Module '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' changed to '".$str_visible."' successfully."; break;
				case("P"): $str_message = "Module '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' changed to '".$str_pos."' successfully."; break;
				case("SS"): $str_message = "Module sub category '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' details added successfully."; break;
				case("D"): $str_message = "Module details deleted successfully."; break;
				case("U"): $str_message = "Module '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' details updated successfully."; break;
				case("SU"): $str_message = "Module sub category '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' details updated successfully."; break;
				case("O"): $str_message = "Display order updated successfully."; break;
				case("DU"): $str_message = "Module title '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_cattitle,30))) . "' already exists. Please try with another title."; break;
				case("SDU"):$str_message = "Module sub category title '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_cattitle,30))) . "' already exists. Please try with another title."; break;
			}
		}

?>
	  
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_MODULE_LIST);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
	<?php include "./adm_module_header.php";?>
	<div class="row padding-10">
		<div class="col-md-6 col-sm-6 col-xs-12 button_space">
			<div class="btn-group" role="group" aria-label="...">
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_MODULE_LIST);?> </h3></div>
	</div><hr> 
	<?php if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
	<div class="row padding-10">
    	<div class="col-md-12">
        	<div class="panel-group" id="accordion">
            	<div class="panel panel-default">
                	<div class="panel-heading">
                    	<h4 class="panel-title">
							<div class="row padding-10">
                                                            <div class="col-md-6 col-sm-6 col-xs-8"><a class="accordion-toggle collapsed link" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><i class="fa  fa-plus "></i>&nbsp;&nbsp;<b><?php print($STR_TITLE_FORM_ADD); ?></b> </a></div>
									<div class="col-md-6 col-md-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>												
							</div>
						</h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
						<div class="panel-body">
							 <form name="frm_module_add" action="adm_module_add_p.php" method="post" onSubmit="return frm_module_add_validateform();">
								<div class="text-help-form" align="right"><?php print($STR_MANDATORY); ?></div>
								<div class="form-group">
									<label>Module Title</label><span class="text-help-form"> *</span> <span class="text-help-form">(<?php print($STR_MAX_255_MSG);?>)</span>
										<input type="text" id="txt_title"  name="txt_title"  value="<?php print(MyHtmlEncode($str_modname)); ?>"  maxlength="255" class="form-control input-sm" placeholder="Enter module title here">
								</div>
								 <?php if($rs_list->count()>0) { $rs_list->movefirst(); } $str_temp="";?>
								<div class="form-group">
									<label>Module Type</label><span class="text-help-form"> *</span>
									<select name="cbo_type" onChange="return change_position();"  class="form-control input-sm" >
	   									<option value="" selected>---- Select Category Type ----</option>
	   									<option value="0" <?php print(CheckSelected("0",strtoupper($str_modtype)));?>>Create As Main Category</option>
	   									<option value="">-------------- OR --------------</option>
	   									<?php while($rs_list->eof()==false) { 
		 									 if($str_temp!=$rs_list->fields("modid"))
												{	$str_temp=$rs_list->fields("modid");?>
	   									<option value="<?php print($rs_list->fields("modid")); ?>" <?php print(CheckSelected($rs_list->fields("modid"),$str_modtype));?>>
										<?php print($rs_list->fields("modname"));?></option>
	   										<?php } $rs_list->movenext(); } ?>
	   								</select>	   
								</div>
								<div class="form-group">
									<label>Url</label><span class="text-help-form">* (<?php print($STR_URL_EXAMPLE);?>)</span>
									<input type="text" name="txt_url" value="<?php print(MyHtmlEncode($str_url)); ?>" maxlength="255" class="form-control input-sm" placeholder="Enter URL here">
								</div>
								<div class="form-group">
									<label>Tooltip</label> <span class="text-help-form">(<?php print($STR_MAX_100_MSG);?>)</span>
									<input type="text" name="txt_tooltip" value="<?php print(MyHtmlEncode($str_tooltip)); ?>" maxlength="100" class="form-control input-sm" placeholder="Enter tooltip here">
								</div>
								<div class="form-group">
									<label>Instruction Text</label><span class="text-help-form"></span>
									<textarea class="form-control input-sm" name="ta_instruction" cols="100" rows="10" placeholder="Enter instruction text here"><?php print(MyHtmlEncode($str_instruction)); ?></textarea>
								</div>
								<div class="form-group">
									<label>Position</label><span class="text-help-form"> *</span>
									<select class="form-control input-sm" name="cbo_position" disabled >
										<option value="">---- Select Position ----</option>
										<option value="LEFT" <?php print(CheckSelected("LEFT",strtoupper($str_pos)));?>>Left</option>
										<option value="MIDDLE" <?php print(CheckSelected("MIDDLE",strtoupper($str_pos)));?>>Middle</option>
										<option value="RIGHT" <?php print(CheckSelected("RIGHT",strtoupper($str_pos)));?>>Right</option>
									</select>
								</div>
								<?php if($rs_list->count()>0) { $rs_list->movefirst(); } $str_temp="";?>
								<div class="form-group">
									<label>Display After Module</label><span class="text-help-form"> * (<?php print($STR_DISPLAY_AFTER_MSG);?>)</span>
										<select name="cbo_display" class="form-control input-sm" disabled >
	  										 <option value="0">---- Default Order ----</option>
	   											<?php while($rs_list->eof()==false) { 
														if($str_temp!=$rs_list->fields("modid")) { $str_temp=$rs_list->fields("modid"); ?>
	   										<option value="<?php print($rs_list->fields("modorder")); ?>" <?php print(CheckSelected($rs_list->fields("modorder"),$str_display));?>><?php print($rs_list->fields("modname")); ?> (<?php print($rs_list->fields("modorder")); ?>) (<?php print($rs_list->fields("modpos")); ?>)</option>
	  											<?php } $rs_list->movenext(); } ?>
	   									</select>	
								</div>
								<?php if($rs_list->count()>0) { $rs_list->movefirst(); } $str_temp="";?>
								<div class="form-group">
									<?php $str_visible="YES";
										if(isset($_GET["vis"]) && trim($_GET["vis"])!="" ) { $str_visible=trim($_GET["vis"]);}?>
									<label>Visible</label>&nbsp;<span class="text-help-form">* (<?php print($STR_VISIBLE)?>)</span><br/>
										<select name="cbo_visible" class="form-control input-sm" >
												<option value="YES" <?php print(CheckSelected($str_visible,"YES")); ?>>YES</option>
												<option value="NO" <?php print(CheckSelected($str_visible,"NO")); ?>>NO</option>
										</select>
								</div>
								<div class="form-group">
										 <?php $str_new_win="NO";
											if(isset($_GET["nwin"]) && trim($_GET["nwin"])!="" ) { $str_new_win=trim($_GET["nwin"]); }?>
									<label>Open Link In New Window</label>
										<select name="cbo_newwin" class="form-control input-sm" >
												<option value="YES" <?php print(CheckSelected($str_new_win,"YES")); ?>>YES</option>
												<option value="NO" <?php print(CheckSelected($str_new_win,"NO")); ?>>NO</option>
										</select>
								</div>	
								<input type="hidden" name="hdn_level" value="<?php print($rs_position->fields("moduleparentpkid")); ?>">	
								<button type="submit"  class="btn btn-success" name="btn_submit_add"  id="btn_submit_add" title="<?php print($STR_BUTTON_TITLE_FORM_ADD); ?>"><span class="fa fa-plus-sign" aria-hidden="true" ></span> Add</button>&nbsp;
								<input name="btn_reset" id="btn_reset" title="<?php print($STR_BUTTON_TITLE_FORM_RESET); ?>" value="Reset" class="btn btn-danger" type="reset">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="table-responsive">
	 <form name="frm_list" action="adm_module_order_p.php" method="post" onSubmit="return frm_list_check_displayorder();">
	 <table class="table table-striped table-bordered">
        	<thead>
                <tr>
                	<th width="4%">Sr. #</th>
					<th width="">Module Title</th>
					<th width="10%">Work History</th>
					<th width="7%">Actual Position</th>
					<th width="12%">Required Position</th>
					<th width="6%" align="center">Display  Order</th>
          			<th width="8%" align="center">Action</th>
					<?php if($_SESSION['superadmin'] == "YES"){?>	
         			<th width="6%">Delete</th>
					<?php }?>
				</tr>
			</thead>
			<tbody>
            <?php if($rs_list->EOF()==true)  {  ?>
				<tr><td colspan="8" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
				<?php } else {?>
				<?php 
				$int_cnt=1; 
				$int_mod_cnt=0; 
				$int_sub_cnt=0;
				$pkid=0; 
				$cnt=""; 
				$title=""; 
				$order="";
				$modpkid=""; 
				$flag=0; 
				$delete_flag=0;
				$disp_class="";
				$mod_pos=""; 
				$str_visible="";
					while($rs_list->eof()==false) { ?>		
					 <?php
					  if($pkid != $rs_list->fields("modid") && $flag==0)
						{	
						$int_sub_cnt = 0; 
						$flag=1; 
						$delete_flag=0;
						$pkid = $rs_list->fields("modid");
						$int_mod_cnt = $int_mod_cnt + 1;
						$cnt="<strong>".$int_mod_cnt."</strong>"; 
						$title= "<strong>".MyHtmlEncode($rs_list->fields("modname"))."</strong>"; 
						$order=$rs_list->fields("modorder"); 
                                                $modpkid=$rs_list->fields("modid");
						$modparentpkid=$rs_list->fields("modparentid"); 
						$url=$rs_list->fields("modurl");
						$instruction=$rs_list->fields("modinstruction"); 
						$tooltip=$rs_list->fields("modtip");
						//$url=DisplayWebSiteUrl($url,$url,'',$tooltip,'MenuLink8ptNormal','','');
						$disp_class=""; 
						$mod_pos=$rs_list->fields("modpos");
						$highlightclass="class='alert-warning'"; 
						$str_visible=$rs_list->fields("pvisible");
						if(trim($rs_list->fields("subname"))=="") { $delete_flag=1; }
						}
						else
						{	
						$flag=0; 
						$delete_flag=1; 
						$int_sub_cnt = $int_sub_cnt + 1;
						$cnt=$int_sub_cnt; 
						$str_visible=$rs_list->fields("cvisible");
						$title= ReplaceRecordWithCharacter($rs_list->fields("subname"),"<b>".MyHtmlEncode($rs_list->fields("subname"))."</b>"); 
						$order=$rs_list->fields("suborder"); 
						$modpkid=$rs_list->fields("subid");
						$modparentpkid=$rs_list->fields("subparentid"); 
						$mod_pos="";
						$url=ReplaceRecordWithCharacter($rs_list->fields("suburl"),"".$rs_list->fields("suburl"));
						$instruction=$rs_list->fields("subinstruction"); 
						$tooltip=$rs_list->fields("subtip");
						//$url=DisplayWebSiteUrl($url,$url,'',$tooltip,'MenuLink8ptNormal','','');
						$disp_class=""; $highlightclass="";
							//$str_title_class="class='FieldCaptionText8ptNormal'";
						 } ?>
				<?php if(trim($title)!="") { ?>
                                    <tr>
					<td align="center" <?php print($highlightclass);?>><?php print($cnt);?>
						<input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($modpkid);?>">
						<input type="hidden" name="hdn_parentpkid<?php print($int_cnt);?>" value="<?php print($modparentpkid);?>">
						<input type="hidden" name="current_cnt<?php print($int_cnt);?>" value="<?php print($int_cnt);?>">
						<?php //print($modpkid);?>
						<?php //print($modparentpkid);?>
					</td>
					<td <?php print($highlightclass);?>>
						<table width="100%" class="" border="0" cellspacing="0" cellpadding="0" >
						   <tr><td align="left"><?php print($title); ?><?php //print($modparentpkid);?></td></tr>
						   <?php if ($url!="") { ?><tr><td align="left" valign="top"><?php print($url); ?></td></tr><?php }?>
						   <?php if ($instruction!="") { ?><tr><td align="left" valign="top"><?php print($instruction); ?></td></tr><?php }?>
						</table>
					</td>
					<td align="center" valign="middle" <?php print($highlightclass);?>>
					
					<?php
					$str_total_work = "SELECT count(DISTINCT pkid) AS total_update FROM  t_work_history WHERE modulepkid=".$modpkid;
					$rs_work=GetRecordSet($str_total_work);
					//print($rs_work->fields("total_update")); 
	?>
					<a href="adm_module_update_history.php?parentpkid=<?php print($modparentpkid);?>&modpkid=<?php print($modpkid);?>" class="<?php print($str_class);?>" title="Click view work history"><span class="fa fa-list-ul" aria-hidden="true" ></span></a>
                                        <span class="text-help"><?php print("( ".$rs_work->fields("total_update")." )");  ?></span>
					</td>
					
					<?php if($mod_pos=="LEFT")
							{	$str_class="SetVisibleLink";
								$str_td_class="class='Setvisible'";
								$str_tip="Click to make position 'LEFT'";
							}
							elseif($mod_pos=="MIDDLE")
							{	$str_class="SetInvisibleLink";
								$str_td_class="class='SetInvisible'";
								$str_tip="Click to make position 'MIDDLE'";
							}
							elseif($mod_pos=="RIGHT")
							{	$str_class="SetInvisibleLink";
								$str_td_class="class='SetInvisible'";
								$str_tip="Click to make position 'RIGHT'";
							}
							else { $str_td_class=""; }
							if($highlightclass=="") { $str_chkbox_class="TDRowColor".($int_cnt%2); }	
							else { $str_chkbox_class="HighlightChkbox"; } 
							
							$str_tip_left="Click to make position 'LEFT'";
							$str_tip_midle="Click to make position 'MIDDLE'";
							$str_tip_right="Click to make position 'RIGHT'";
							?>
							
					<td align="center" valign="middle" <?php print($highlightclass);?>><strong><?php print $mod_pos; ?></strong></td>
				  	<td align="center" valign="middle" <?php print($highlightclass);?>>
						
						 <?php
						 if ($rs_list->fields("modpos")=="LEFT") { 
						$display_left="display:none";
						$display_right="";
						$display_middle="";
						$space_left_side="";
						$space_middle_side="&nbsp;&nbsp;";
						 } 
						 if ($rs_list->fields("modpos") == "MIDDLE") { 
						$display_left="";
						$display_right="";
						$display_middle="display:none";
						$space_left_side="&nbsp;&nbsp;";
						$space_middle_side="";
						 }
						 if ($rs_list->fields("modpos") == "RIGHT") {
						$display_left="";
						$display_right="display:none";
						$display_middle="";
						$space_left_side="&nbsp;&nbsp;";
						$space_middle_side="";
						 }					
						 if($modparentpkid==0) { ?>
						 <a href="adm_module_position_p.php?pkid=<?php print($modpkid);?>&reqpos=LEFT" name="pos_left"  style=" <?php print($display_left); ?>" class="<?php print($str_class);?>" title="<?php print($str_tip_left);?>"><?php //print($mod_pos);?>LEFT</a><?php print($space_left_side); ?>
						 <a href="adm_module_position_p.php?pkid=<?php print($modpkid);?>&reqpos=MIDDLE" name="pos_middle" class="<?php print($str_class);?>"  style=" <?php print($display_middle); ?>" title="<?php print($str_tip_midle);?>"><?php //print($mod_pos);?>MIDDLE</a><?php print($space_middle_side); ?>
						 <a href="adm_module_position_p.php?pkid=<?php print($modpkid);?>&reqpos=RIGHT" name="pos_right" style=" <?php print($display_right); ?>" class="<?php print($str_class);?>" title="<?php print($str_tip_right);?>"><?php //print($mod_pos);?>RIGHT</a>
						 <?php } ?>
					</td>
					<td <?php print($highlightclass);?> align="center">
							<input type="text" name="txt_displayorder<?php print($int_cnt);?>" size="5" maxlength="5" value="<?php print($order);?>" class="form-control input-sm" style="text-align:center">
							
							
					</td>	
					<td align="center" <?php print($highlightclass);?>>
						<?php $str_image="";  $str_visible_class="";  $str_title="";
								if(strtoupper($str_visible)=="YES") { $str_image="<i class='fa fa-eye'></i>";
									$str_visible_class="btn btn-warning btn-xs"; $str_title=$STR_BUTTON_TITLE_FORM_VISIBLE_ICON; }
								else { $str_image="<i class='fa fa-eye-slash'></i>";
									$str_visible_class="btn btn-default active btn-xs"; $str_title=$STR_BUTTON_TITLE_FORM_INVISIBLE_ICON; } ?>		  
								<a href="adm_module_visible_p.php?pkid=<?php print($modpkid);?>" class="<?php print($str_visible_class);?>" title="<?php print($str_title); ?>"><?php print($str_image);?></a> &nbsp;
								<a href="adm_module_edit.php?pkid=<?php print($modpkid);?>" class="btn btn-success btn-xs " title="<?php print($STR_BUTTON_TITLE_FORM_EDIT_ICON);?>"><i class="fa fa-pencil"></i></a>
					</td>
					<?php if($_SESSION['superadmin'] == "YES"){?>	
					<td align="center" <?php print($highlightclass);?>>
						<input type="checkbox" class="<?php print($str_chkbox_class);?>" name="chk_show<?php print($int_cnt);?>"  onClick="return check_sub(this,current_cnt<?php print($int_cnt);?>);" >
					</td>	
					<?php }?>						
				</tr>
				<?php $int_cnt++; }
						if($flag!=1) { $rs_list->MoveNext(); } } ?>
				<tr>
					<td  colspan="5">
					<?php /*?><input type="hidden" name="hdn_static" value="static_value"><?php */?>
					<input type="hidden" name="hdn_counter" id="hdn_counter" value="<?php print($int_cnt);?>">   
					<input type="hidden" name="hdn_disp_pkid<?php print($int_cnt);?>" value="<?php print($pkid);?>">
					</td>
					<td align="center"><button name="btn_submit_save" type="submit" title="<?php print($STR_BUTTON_TITTLE_FORM_DISPLAY_ORDER);?>" class="btn btn-success">Save</button></td>
					<td></td>
					<?php if($_SESSION['superadmin'] == "YES"){?>	
					<td><input name="btn_submit_delete" type="button" onClick="return frm_module_list_confirmdelete();" title="<?php print($STR_BUTTON_TITLE_FORM_DELETE_ICON);?>" value="Delete" class="btn btn-danger"></td>
					<?php }?>
				</tr>
		<?php } ?>
        </tbody>
	</table> 
	 </form>
    </div>	
	<?php /*?><?php include "../../includes/help_for_list.php"; ?><?php */?>
	
</div>
<?php include "../../includes/include_files_admin.php"; ?>
<script language="JavaScript" src="adm_module_list.js" type="text/javascript"></script>
</body></html>