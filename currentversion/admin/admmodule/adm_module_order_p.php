<?php
/*
	Module Name:- admproduct
	File Name  :- adm_module_order_p.php
	Create Date:- 18-APR-2006
	Intially Create By :- 0025
	Update History:
*/


//print $_POST["hdn_counter"]; exit;
#----------------------------------------------------------------------------------------------------
#Include Files
	include "./validate_adm_login.php";
	include "../../includes/configuration_admin.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
//print_r($_POST);exit;
#get post data
$int_cnt = 0;
//print $_POST["hdn_counter"]; exit;
if (isset($_POST["hdn_counter"]))
{
	$int_cnt=trim($_POST["hdn_counter"]);
}	
//print($int_cnt);exit;
if($int_cnt=="" || $int_cnt<0 || is_numeric($int_cnt)==false)
{
	CloseConnection();
	Redirect("adm_module_list.php?msg=F&type=E&#ptop");
	exit();
}
#----------------------------------------------------------------------------------------------------
#update display order field in table t_module.
for($i=1;$i<$int_cnt;$i++)
{
	if(trim($_POST["txt_displayorder". $i]) != "" && trim($_POST["txt_displayorder". $i]) >=0 && is_numeric(trim($_POST["txt_displayorder". $i]))==true  && trim($_POST["hdn_pkid". $i])!="" && trim($_POST["hdn_pkid". $i])>0 && is_numeric(trim($_POST["hdn_pkid". $i]))==true)
	{
            $str_query_update="UPDATE t_module SET displayorder='" . trim($_POST["txt_displayorder" . $i]) . "' WHERE modulepkid=" . trim($_POST["hdn_pkid" . $i]);
            //print $str_query_update."<br/>";
            ExecuteQuery($str_query_update);
	}
} //exit;
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to adm_module_list.php page	
    CloseConnection();
    Redirect("adm_module_list.php?type=S&msg=O&#ptop");
    exit();
#------------------------------------------------------------------------------------------------------------ ?>