<?php
/*
	Module Name:- control panel home
	File Name  :- adm_home.php
	Create Date:- 23-Jun-2006
	Intially Create By :- 0015
	Update History:
*/

#--------------------------------------------------------------------------------------------------
 	include "validate_adm_login.php";
	include "../../includes/configuration_admin.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
	//print_r($_SESSION);
#--------------------------------------------------------------------------------------------------
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><?php print($STR_SITE_TITLE); ?> : <?php print($STR_TITLE_MODULE_ADMIN_MAIN_MENU); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<div class="container">
  <?php include "./adm_module_header_home.php"; ?><!-- need to update admin header file as admin main menu -->
	<div class="row">
		<h3 align="center" ><b class="text-primary"><i class="glyphicon glyphicon-home"></i>&nbsp;<?php print($STR_TITLE_MODULE_ADMIN_MAIN_MENU); ?></b></h3>
		<div class="col-md-12 col-sm-12 col-xs-12" >
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-default">
						<p class="panel-heading"><b>Module Management</b></p>
						<div class="panel-body">
							<a href="./adm_module_list.php" class="link"  title="Click to view module management">Manage Modules</a>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-default">
						<p class="panel-heading"><b>Module Administrator Management</b></p>
						<div class="panel-body">
							<a href="./adm_module_admin_list.php" class="link"  title="Click to view site admin details">Manage Module Admin</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr/>
	</div>
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/admin.css" rel="stylesheet" type="text/css">
<link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script language="JavaScript" src="../../js/functions.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
function openchatwindow()
{
	window.open('./modchat/chat_with_users.php','','left=50,resizable=no,top=20,scrollbars=yes,width=700, height=560');	
}
</script>
</body>
</html>