<?php
/*
	Module Name:- Content Management Module
	File Name  :-cm_home_content.php
	Create Date:- 09-DEC-2016
	Intially Create By :- 015
	Update History:
*/
#------------------------------------------------------------------------------
#	Include files
	include "./validate_adm_login.php";
	include "../includes/configuration.php";
	include "./adm_module_config.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
#------------------------------------------------------------------------------------------
# get Query String Data
	$modparentpkid="";
	$modpkid="";
	if (isset($_GET["parentpkid"]))
	{
		$modparentpkid = trim($_GET["parentpkid"]);
	}
	if (isset($_GET["modpkid"]))
	{
		$modpkid = trim($_GET["modpkid"]);
	}
#-------------------------------------------------------------------------------------------------------------------
	
		$str_query="";
		$str_query="SELECT a.modulepkid,a.moduleparentpkid,a.moduletitle submodtitle,b.modulepkid,b.moduleparentpkid, b.moduletitle pmodtitle ";
		$str_query.="FROM t_module a LEFT JOIN t_module b ";
		$str_query.="ON a.moduleparentpkid=b.modulepkid ";
		$str_query.="where a.modulepkid=".$modpkid." and a.moduleparentpkid=".$modparentpkid;
		$rs_title=GetRecordSet($str_query);
	//print $str_query; 
	
#------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.
		
		$str_query_select="";
		$str_query_select="SELECT a.employeename,a.moduleparentpkid,a.modulepkid,a.worktype,a.updatedate,a.description,";
		$str_query_select.="b.modulepkid,b.moduleparentpkid,b.moduletitle pmodtitle ";
		$str_query_select.="FROM t_work_history a LEFT JOIN t_module b ";
		$str_query_select.="ON a.modulepkid=b.modulepkid ";
		$str_query_select.="where a.modulepkid=".$modpkid." and a.moduleparentpkid=".$modparentpkid." ORDER BY worktype ASC, updatedate ASC";

  
   
	# Select Query to display list
	/*$str_query_select="SELECT * FROM t_update_history where modulepkid=".$modpkid." ORDER BY worktype ASC, updatedate DESC";*/
	//print($str_query_select); //exit;
	$rs_list=GetRecordSet($str_query_select);
	
#-------------------------------------------------------------------------------------------------------------------
#	Get message type.
	$str_type = "";
	$str_message = "";	
	if(isset($_GET["type"]))
	{
		switch(trim($_GET["type"]))
		{
			case("S"): $str_type = "S"; break;
			case("E"): $str_type = "E"; break;
			case("W"): $str_type = "W"; break;
		}
	}
#	Display message.
if(isset($_GET["msg"]))
{
	switch(trim($_GET["msg"]))
	{
		case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
		case("V"):	$str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_visible_value; break;	
		case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;	
		case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
		case("E"): $str_message = $STR_MSG_ACTION_UPDATE; break;
		case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_WORK_HISTORY);?></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<!-- Page Content -->
<a name="ptop"></a>
<div class="container">
<?php //include "../includes/adminheader.php"; ?>
<?php include "./adm_module_header.php"; ?>
	<div class="row">
		<div class="col-lg-3 col-sm-6 col-xs-12 button_space">
			<div class="btn-group" role="group" aria-label="...">
				<a href="./adm_module_list.php" title="<?php print($STR_HREF_TITLE_LINK); ?> <?php print($STR_TITLE_MODULE_LIST); ?>" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_MODULE_LIST); ?></a>
			</div>
		</div>
		<div class="col-lg-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_WORK_HISTORY); ?>
		<?php print(" for ".$rs_title->fields("pmodtitle"));?>
		<?php if($rs_title->fields("pmodtitle")!=""){print(" >> ");}print($rs_title->fields("submodtitle")); ?></h3></div>
	</div><hr>
	<?php	if($str_type != "" && $str_message != "") { print(DisplayMessage("99%",$str_message,$str_type)); } ?>
	<div class="row">
    	<div class="col-lg-12">
        	<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
                    	<h4 class="panel-title">
							<div class="row">
								<div class="col-md-12  padding-10">
									<div class="col-md-6 col-sm-6 col-xs-8"><a class="accordion-toggle collapsed link" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><i class="glyphicon  glyphicon-plus "></i>&nbsp;&nbsp;<?php print($STR_TITLE_FORM_ADD); ?></a></div>
									<div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>
								</div>
							</div>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
						<div class="panel-body">
                        	<div class="panel-body">
     							<form name="frm_add_work" action="adm_module_update_history_worktype_p.php" method="post" onSubmit="return validate_frm_add_work();" enctype="multipart/form-data" role="form">
									<div class="form-group HelpText" align="right"><?php print($STR_MANDATORY); ?></div>
									
									<input type="hidden" name="hdn_parent" value="<?php print($modparentpkid); ?>">
									<input type="hidden" name="hdn_module" value="<?php print($modpkid); ?>">	
									<?php //print($modparentpkid); ?>
									<?php /*?><div class="form-group">
										<label>Employee Id</label><span class="HelpText"> *</span>
										<input id="txt_empname" name="txt_empname" size="90" class="form-control input-sm" maxlength="512" placeholder="Enter id here" type="text" tabindex="1">
									</div><?php */?>
									<div class="form-group">
									<label>Work Type</label>&nbsp;<span class="HelpText">* </span><br/>
									<?php 
									$str_updated="UPDATED";
									 $str_disable="";
									
									?>
										<select class="form-control input-sm" name="cbo_work_type" tabindex="2">
										<?php if($rs_list->fields("worktype")!="CREATED") { ?> 
										<option value="CREATED" <?php print(CheckSelected("CREATED",strtoupper($str_updated)));?>>Created</option><?php } ?>
										<option value="UPDATED" <?php print(CheckSelected("UPDATED",strtoupper($str_updated)));?>>Updated</option>
									</select>
								</div>
									<div class="form-group">
										<label>Description</label><span class="HelpText"> *</span>
											<textarea name="ta_desc" id="ta_desc" cols="100" rows="10" class="form-control input-sm" placeholder="Enter description here" tabindex="3"></textarea>
									</div>
									
									<button type="submit" tabindex="4" class="btn btn-success" title="<?php print($STR_BUTTON_TITLE_FORM_ADD)?>"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span> Add</button>&nbsp;
									<input name="btn_reset" id="btn_reset" title="<?php print($STR_BUTTON_TITLE_FORM_RESET)?>" value="Reset" class="btn btn-danger" type="reset" tabindex="5">
										
										<?php //print($modpkid); ?>		
     							</form>
            				</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="table-responsive">
	<form name="frm_list" action="" method="post" onSubmit="">
		<table class="table table-striped table-bordered table-hover">
        	<thead>
                <tr>
                	<th width="4%">Sr. #</th>
					<th width="10%">Added Date</th>
					<th width="12%">Employee Id</th>
					<th width="">Description</th>
					<?php /*?><th width="10%">Parent Module</th>
					<th width="10%">Sub Module</th><?php */?>
					<th width="8%">Work Type</th>
				</tr>
			</thead>
			<tbody>
			<?php if($rs_list->EOF()==true)  {  ?>
					<tr><td colspan="5" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
				<?php }
				else {
				
				$int_cnt=1;
				while(!$rs_list->EOF()==true) { ?>
				<tr>
					<td align="center"><?php print($int_cnt); ?></td>
					<td align="center"><?php print($rs_list->fields("updatedate"));?></td>
					<td align="center"> 
						<?php print($rs_list->fields("employeename"));?>
					</td>
					<td align="">
						<?php print($rs_list->fields("description"));?>
					</td>
					<?php /*?><td align="">
						<?php print($rs_list->fields("modname"));?>
					</td>
					<td align="">
						<?php  print($rs_list->fields("subname"));?>
					</td><?php */?>
					<td align="center">
						<?php
						 if($rs_list->fields("worktype")=="CREATED")
						{?>
						<strong class="text-info">
						<?php print($rs_list->fields("worktype"));?>
						</strong>
						<?php } else {
							 print($rs_list->fields("worktype"));
							 } ?>
					</td>
				</tr>
		<?php
			$int_cnt++;
			$rs_list->MoveNext();
			} 
		}  ?>
		<input type="hidden" name="hdn_counter" value="<?php print($int_cnt)?>">
       	</tbody>
	</table>
	</form>
    		
    </div>
	
	
	<?php include "../includes/help_for_list.php"; ?>
    <!-- /.container -->											
</div>
<!-- Bootstrap Core CSS -->
<link href="../includes/bootstrap.min.css" rel="stylesheet">
<link href="../includes/admin.css" rel="stylesheet">
<!-- jQuery -->
<script src="../includes/jquery.min.js"></script>
<script src="includes/jqBootstrapValidation.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="../includes/bootstrap.min.js"></script>
<script language="JavaScript" src="../includes/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="adm_module_update_history.js" type="text/javascript"></script>
</body></html>