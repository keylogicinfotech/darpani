/*
	Module Name:- admmodule
	File Name  :- adm_module_list.js
	Create Date:- 18-APR-2006
	Intially Create By :- 0025
	Update History:
*/

function frm_list_check_displayorder()
{
	with(document.frm_list)
	{
		cnt=hdn_counter.value;
                //alert(cnt);
		for(i=1;i<cnt;i++)
		{
			if(trim(eval("txt_displayorder" + i).value) =="")
			{
				alert("Please enter display order.");
				eval("txt_displayorder" + i).select();
				eval("txt_displayorder" + i).focus();
				return false;
			}
			
			if(eval("txt_displayorder" + i).value <0)
			{
				alert("Please enter positive integer for display order.");
				eval("txt_displayorder" + i).select();
				eval("txt_displayorder" + i).focus();
				return false;
			}
			
			if(isNaN(eval("txt_displayorder" + i).value))
			{
				alert("Please enter numeric value for display order.");
				eval("txt_displayorder" + i).select();
				eval("txt_displayorder" + i).focus();
				return false;
			}
		}
	}
	return true;
}

function frm_module_list_confirmdelete()
{
	
	with(document.frm_list)
	{
		counter=hdn_counter.value;
		cnt=0;

		for (i=1;i<counter;i++)
		{
			if (eval("chk_show" + i).checked==true)
			{
				cnt++;
			}
		}
		if (cnt==0)
		{
			alert("Please select module(s) which you want to delete.");
			return false;
		}
		else	
		{
			if(confirm("Are you sure you want to delete checked module(s) details?"))
			{
				if(confirm("Confirm Deletion:Click 'Ok' to delete checked module(s) details or 'Cancel' to deletion."))
				{
					action="adm_module_del_p.php";
					method="post";
					submit();	
		
				}
			}
			return false;
		}
	}
	return false;
}

function show_details(Url)
{
		window.open(Url,'Module','left=50,top=20,scrollbars=yes,resizable=yes,width=570,height=490');
		return false;
}

function frm_module_add_validateform()
{
	with(document.frm_module_add)
	{
		if(trim(txt_title.value) == "")
		{
			alert("Please enter module title.");
			txt_title.select();
			txt_title.focus();			
			return false;
		}

		if(cbo_type.value == "")
		{
			alert("Please select module type.");
			cbo_type.focus();			
			return false;
		}
		
		if(cbo_type.value!=0 )
		{
			if(txt_url.value == "")
		{
			alert("Please enter url.");
			txt_url.focus();			
			return false;
		}
		}
		
		if(cbo_type.value==0 )
		{
			if(cbo_position.value == "")
			{
				alert("Please select position.");
				cbo_position.focus();			
				return false;
			}
		}
				
	}
	return true;
}
function change_position()
{
	with(document.frm_module_add)
	{
		if(cbo_type.value != "0")
		{
			cbo_position.value="";
			eval(cbo_position).selected=true;
			eval(cbo_position).disabled=true;
			
			cbo_display.value="0";
			eval(cbo_display).selected=true;
			eval(cbo_display).disabled=true;

			return true;
		}
		else
		{
			eval(cbo_position).disabled=false;
			eval(cbo_display).disabled=false;
			return true;
		}
	}
	return true;

}
function check_sub(chk,curr)
{
	var count,i,
	i=curr.value;
	count=document.frm_list.hdn_counter.value;
	with(document.frm_list)
	{
		if (chk.checked==true)
		{
			if(eval("hdn_parentpkid" + i).value==0)
			{
				j=++i;
				i=curr.value;
				for(j;j<count;j++)
				{
					if(eval("hdn_parentpkid" + j).value==eval("hdn_pkid" + i).value)
					{
						eval("chk_show" + j).checked=true;
					}
				}
			}
			/*
			else
			{
				for(j=1;j<count;j++)
				{
					if(eval("hdn_parentpkid" + i).value==eval("hdn_pkid" + j).value)
					{
						eval("chk_show" + j).checked=true;
					}
				}
			}*/
		}
		if (chk.checked==false)
		{
			if(eval("hdn_parentpkid" + i).value==0)
			{
				j=++i;
				i=curr.value;
				for(j;j<count;j++)
				{
					if(eval("hdn_parentpkid" + j).value==eval("hdn_pkid" + i).value)
					{
						eval("chk_show" + j).checked=false;
					}
				}
			}
			
			else
			{
				for(j=1;j<count;j++)
				{
					if(eval("hdn_parentpkid" + i).value==eval("hdn_pkid" + j).value)
					{
						pcnt=j;
						cnt=j;
						break;
					}
				}
				eval("chk_show" + pcnt).checked=false;

				/*
				flag=false;
				for(cnt;cnt<count;cnt++)
				{
					if(eval("hdn_parentpkid" + cnt).value==eval("hdn_pkid" + pcnt).value)
					{
						if(eval("chk_show" + cnt).checked==true)
						{
							flag=true;
							break;
						}
					}
				}
				if(flag==true)
				{
						eval("chk_show" + pcnt).checked=false;
				}*/

			}

		}
	}
}
