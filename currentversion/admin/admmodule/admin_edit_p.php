<?php

/*
	Module Name:- modsiteadmin
	File Name  :- sa_edit_p.php
	Create Date:- 01-02-2006
	Intially Create By :- 0022
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	//include "../includes/validatesession.php";
	include "./validate_adm_login.php";
	include "../../includes/configuration.php";
	include "./adm_module_config.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_datetimeyear.php";
	include "../../includes/lib_common.php";
#-------------------------------------------------------------------------------------------	
#initializing vriables
$int_pkid ="";
$str_admin_name="";
$str_password="";
$str_super="NO";
$str_allowlogin="";

#	To check whether values are passed properly or not.
	if(isset($_POST["hdn_id"]))
	{
		$int_pkid = trim($_POST["hdn_id"]);
	}
	if(isset($_POST["hdn_admin_name"]))
	{
		$str_admin_name = trim($_POST["hdn_admin_name"]);
	}
	if(isset($_POST["txt_password"]))
	{
		$str_password = trim($_POST["txt_password"]);
	}
	if(isset($_POST["cbo_super"]))
	{
		$str_super = trim($_POST["cbo_super"]);
	}
	if(isset($_POST['hdn_alwlgn']))
	{
		$str_allowlogin=trim($_POST['hdn_alwlgn']);
	}
	if($str_super=='YES')
	{
		$str_allowlogin='YES';
	}
	//print $int_pkid;exit;
#---------------------------------------------------------------------------------------------------------------------------------------------------]
#check validation
	if( $int_pkid == "" || $str_admin_name == "" || $str_password == "" || $str_super=="")
	{
		CloseConnection();
		Redirect("./admin_edit.php?type=E&msg=F&id=".urlencode($int_pkid));
		exit();
	}
	
#-------------------------------------------------------------------------------------------	
#	Update query in t_siteadmin.
	$str_query_update = "update t_module_login set loginpassword='" . md5_encrypt(ReplaceQuote($str_password), $STR_MODULE_ENCRYPTION_KEY );
	$str_query_update .= "',allowlogin='".ReplaceQuote($str_allowlogin)."'";
	$str_query_update .= ",issuperadmin='"  .ReplaceQuote($str_super) ."' where pkid=" . $int_pkid ;
	
	ExecuteQuery($str_query_update);
	//print $str_query_update;exit;
#--------------------------------------------------------------------------------------------------------
	CloseConnection();
	Redirect("./adm_module_admin_list.php?type=S&msg=E&admin=".$str_admin_name."&#ptop");
	exit();
?>