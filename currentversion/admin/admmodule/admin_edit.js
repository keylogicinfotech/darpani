/*
	Module Name:- modsiteadmin
	File Name  :- admin_edit.js
	Create Date:- 01-02-2006
	Intially Create By :- 0022
	Update History:
*/

function frm_admin_edit_validation()
{
	with(document.frm_admin_edit)
	{
		if(hdn_admin_name.value == "")
		{
			alert("Please enter admin name.");
			hdn_admin_name.select();
			hdn_admin_name.focus();
			return false;
		}
		if(txt_password.value == "")
		{
			alert("Please enter password.");
			txt_password.select();
			txt_password.focus();
			return false;
		}
		if (checkSpace(txt_password.value))
		{
			alert("Please do not enter blank space in password.");
			txt_password.select();
			txt_password.focus();
			return false;
		}

		if(txt_password.value.length < 6)
		{
			alert("Please enter at least 6 characters for password.");
			txt_password.select();
			txt_password.focus();
			return false;
		}
		
	}
	return true;
}
