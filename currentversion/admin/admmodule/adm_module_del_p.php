<?php
/*
	Module Name:- admmodule
	File Name  :- adm_module_del_p.php
	Create Date:- 18-APR-2006
	Intially Create By :- 0025
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "./validate_adm_login.php";
	include "../../includes/configuration.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
#get Query String Data

//print ($_POST["hdn_static"]);

$int_pkid="";
$counter="";
if (isset($_POST["hdn_counter"]))
{
	$counter=$_POST["hdn_counter"];
}
$arr_pkid=array();
$strpkid="";
$str_flag=false;

for ($i=1;$i<$counter;$i++)
{
	if (isset($_POST["chk_show" . $i]))
	{
		if (($_POST["chk_show" . $i])=="on")
		{
			$strpkid=$strpkid . $_POST["hdn_pkid" . $i] . ",";
		}
	}
}
//print ($strpkid);exit;
$strpkid=substr($strpkid,0,strrpos($strpkid,","));
$strqry="";
$strqry="delete from t_module where modulepkid in (" . $strpkid . ")";
ExecuteQuery($strqry);

$str_select="select modulepkid from t_module where moduleparentpkid=0 order by displayorder";

$rs_list=GetRecordset($str_select);
$i=0;
while(!$rs_list->EOF())
{
	$arr_pkid[$i]=$rs_list->fields("modulepkid");
	$i++;
	$rs_list->MoveNext();
}
for($i=0;$i<count($arr_pkid);$i++)
{
	 $str_update="update t_module set displayorder=".($i+1)." where modulepkid=".$arr_pkid[$i];
	 ExecuteQuery($str_update);
}
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to module_list.php page	
	CloseConnection();
	Redirect("adm_module_list.php?type=S&msg=D");
	exit();
#------------------------------------------------------------------------------------------------------------
?>