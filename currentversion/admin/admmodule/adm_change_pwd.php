<?php
/*
	Module Name:- admmodule
	File Name  :- adm_change_pwd.php
	Create Date:- 26-06-2006
	Intially Create By :- 0015
	Update History:
*/
#-------------------------------------------------------------------------------------------------------
	include "./validate_adm_login.php";
	include "../includes/configuration.php";
	include "./adm_module_config.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_datetimeyear.php";
	include "../includes/lib_common.php";
#--------------------------------------------------------------------------------------------------------
#select query to get details from t_siteadmin table
	$str_select_query="";
	$str_old_password="";
	$str_select_query="SELECT loginpassword from t_module_login where loginid='".$_SESSION['admloginid']."'";
	$rs_admin_password=GetRecordset($str_select_query);
	$str_old_password=md5_decrypt($rs_admin_password->fields("loginpassword"), $STR_MODULE_ENCRYPTION_KEY);	
#------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
   	$str_type = "";
	$str_message = "";
 #	Get message type.  
   		if(isset($_GET["type"]))
		{
			switch(trim($_GET["type"]))
			{
				case("S"): $str_type = "S"; break;
				case("E"): $str_type = "E"; break;
				case("W"): $str_type = "W"; break;
			}
		}
#	Get message text.
   		if(isset($_GET["msg"]))
		{
			switch($_GET["msg"])
			{
				case("S"): $str_message = $STR_MSG_ACTION_UPDATE; break;
				case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
				case("P"): $str_message = $STR_INVALID_OLD_PASSWORD; break;
				case("FP"): $str_message = $STR_MSG_ACTION_INVALID_PASSWORD; break;
				case("FU"): $str_message = $STR_NEW_CONFIRM_PASSWORD_SAME; break;
			}
		}?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><?php print($STR_SITE_TITLE); ?> : <?php print($STR_TITLE_CHANGE_MODULE_ADMIN_MAIN_MENU_PWD); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<!-- Page Content -->
<a name="ptop"></a>
<div class="container">
<?php include "./adm_module_header.php";?>
	<div class="row">
		<div class="col-lg-3 button_space"></div>
		<div class="col-lg-9" align="right" ><h3><?php print($STR_TITLE_CHANGE_MODULE_ADMIN_MAIN_MENU_PWD); ?></h3></div>
	</div><hr>
	<?php	if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); } ?>
	<div class="row">
    	<div class="col-lg-12">
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<div class="row">
								<div class="col-md-12  padding-10">
									<div class="col-md-6 col-sm-6 col-xs-8"><i class="glyphicon  glyphicon-edit "></i>&nbsp;&nbsp;<?php print($STR_TITLE_FORM_ENTER_PASSWORD); ?></div>
									<div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>	
								</div>
							</div>
						</h4>
					</div>
					<div id="collapse01" class="panel-collapse">
						<div class="panel-body">
							<form name="frm_change_password" action="./adm_change_pwd_p.php" method="post" onSubmit="return frm_change_password_validation();">
								<div class="form-group HelpText" align="right"><?php print($STR_MANDATORY); ?></div>
								<div class="form-group">
									<label>Admin Name</label><br/>
									<span class="text-info"><strong><?php print(MyHtmlEncode(RemoveQuote($_SESSION['admloginid']))); ?></strong></span>
								</div>
								<div class="form-group">
									<label>Old Password</label><span class="HelpText"> * (<?php print($STR_PASSWORD);?>)</span>
									<input id="pas_old_password" type="password" name="pas_old_password" size="25" maxlength="20" class="form-control input-sm" tabindex="1">
								</div>
								<div class="form-group">
									<label>New Password</label><span class="HelpText"> * (<?php print($STR_PASSWORD);?>)</span>
									<input id="pas_new_password" type="password" name="pas_new_password" size="25" maxlength="20" class="form-control input-sm" tabindex="2">
								</div>
								<div class="form-group">
									<label>Confirm Password</label><span class="HelpText"> * (<?php print($STR_PASSWORD);?>)</span>
									<input id="pas_confirm_password" type="password" name="pas_confirm_password" size="25" maxlength="20" class="form-control input-sm" tabindex="3">
								</div>
								<button type="submit" class="btn btn-success" name="btn_submit" id="btn_submit" title="Click to save Module Admin Main Menu password" value="Change Password " tabindex="4"><span class="glyphicon  glyphicon-edit " aria-hidden="true"></span>&nbsp;Edit</button>&nbsp;&nbsp;<input name="btn_reset" id="btn_reset2" title="<?php print($STR_BUTTON_TITLE_FORM_RESET); ?>" value="Reset" class="btn btn-danger" type="reset" tabindex="5">
								<input type="hidden" name="hdn_old_pwd" value="<?php print($str_old_password);?>">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include "../includes/help_for_edit.php"; ?>
    <!-- /.container -->											
</div>
<link href="../includes/bootstrap.min.css" rel="stylesheet">
<link href="../includes/admin.css" rel="stylesheet">
<!-- jQuery -->
<script src="../includes/jquery.min.js"></script>
<script src="../includes/bootstrap.min.js"></script>
<script language="JavaScript" src="./sc_edit.js" type="text/javascript"></script>
<script language="JavaScript" src="../includes/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="./adm_change_pwd.js" type="text/javascript"></script>
</body></html>