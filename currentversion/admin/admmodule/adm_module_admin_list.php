<?php
/*
	Module Name:- modsiteadmin
	File Name  :- sa_list.php
	Create Date:- 01-02-2006
	Intially Create By :- 0022
	Update History:
*/


	include "validate_adm_login.php";
	include "../../includes/configuration.php";
	include "./adm_module_config.php";
	include "./adm_module_app_specific.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_datetimeyear.php";
	include "../../includes/lib_common.php";
	
	/*if (strtoupper($_SESSION['superadmin'])=="NO")
	{ 
		CloseConnection();
		Redirect("../admin_home.php");
		exit();
	}*/
	//print_r($_SESSION);
#initializing vriables
	$str_name="";
	$str_fname="";
	$str_lname="";
	$str_addlen="";
	$str_url="";
	$str_super="";
	$str_add="";
	#getting query string variables	
	if(isset($_GET["stradminname"])) { $str_name=trim(MyHtmlEncode(RemoveQuote($_GET["stradminname"]))); }
	if(isset($_GET["super"])) { $str_super=trim($_GET["super"]); }

#	Select query to get records from t_siteadmin.
#-------------------------------------------------------------------------------------------------------------	
$str_flag_query = "";
if (strtoupper($_SESSION['superadmin'])=="NO")
	{ $str_flag_query = "WHERE issuperadmin='NO' AND pkid=".$_SESSION['adminpkid']." AND loginid='".$_SESSION['adminname']."'"; }
else if(strtoupper($_SESSION['superadmin'])=="YES") { $str_flag_query = ""; }

$str_query_select = "SELECT * FROM t_module_login ".$str_flag_query." ORDER BY pkid, registrationdate desc";
$rs_admin_list = GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
$str_varchar="";

if(isset($_GET['varchange'])) { $str_varchar=trim($_GET['varchange']); }
	
#	Get message type.
   		if(isset($_GET["type"]))
		{
			switch(trim($_GET["type"]))
			{
				case("S"): $str_type = "S"; break;
				case("E"): $str_type = "E"; break;
				case("W"): $str_type = "W"; break;
			}
		}

		if (isset($_GET["admin"]))
		{
			$str_admin_name = trim(MyHtmlEncode(RemoveQuote($_GET["admin"])));
		}
		else
		{
			$str_admin_name = "";
		}
#	Get message text.
   		if(isset($_GET["msg"]))
		{
			switch($_GET["msg"])
			{
				case("A"): $str_message = "Allow module login mode changed to '" . $str_varchar . "' successfully."; break;
				case("DU"): $str_message = "Admin '" . $str_admin_name . "' already exists. Please try again with another name."; break;
				case("E"): 	$str_message = $STR_MSG_ACTION_EDIT; break;
				case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
				case("S"):	$str_message = $STR_MSG_ACTION_ADD; break;
				case("F"): 	$str_message = $STR_MSG_ACTION_INFO_MISSING; break;		
				case("N"): $str_message = $STR_MSG_ACTION_INVALID_USERID; break;
				case("P"): $str_message = $STR_MSG_ACTION_INVALID_PASSWORD; break;		
			}
		}
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE_MODULE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
	<?php include "./adm_module_header.php"; ?>
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12 button_space">
			<div class="btn-group" role="group" aria-label="...">
				<?php /*?>
				<a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;Back</a>
				<a href="./country_list.php" class="btn btn-default" title="Click to go country list" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_COUNTRY); ?></a>
				<a href="./co_state_list.php?pkid=<?php print($rs_state->fields("countrypkid")); ?>" class="btn btn-default" title="Click to go country list" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_STATE); ?></a><?php */?>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE_MODULE);?> </h3></div>
	</div><hr> 
	<?php if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
	
	<div class="row">
    	<div class="col-lg-12">
        	<div class="panel-group" id="accordion">
            	<div class="panel panel-default">
                	<div class="panel-heading">
                    	<h4 class="panel-title">
							<div class="row">
								<div class="col-md-12  padding-10">
								
									<div class="col-md-6 col-sm-6 col-xs-8">
									<?php if(strtoupper($_SESSION['superadmin'])=="YES") {?><a class="accordion-toggle collapsed link" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><i class="glyphicon  glyphicon-plus "></i>&nbsp;&nbsp;<?php print($STR_TITLE_ADD); ?></a><?php }?></div>
									<div class="col-md-6 col-md-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>												
								</div>
							</div>
						</h4>
                    </div>
					<?php if(strtoupper($_SESSION['superadmin'])=="YES") {?>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
						<div class="panel-body">
							<form name="frm_admin_add" action="./admin_add_p.php" method="post">
								<div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
								<div class="form-group">
									<label>Admin ID</label><span class="text-help-form"> *</span>
									<input type="text" placeholder="<?php print($STR_UNIQUE_ADMIN);?>" id="txt_admin_name"  name="txt_admin_name"  value="<?php print($str_name); ?>" tabindex="1" maxlength="20" class="form-control input-sm">
								</div>
								<div class="form-group">
									<label>Password</label><span class="text-help-form"> *</span>
									<input type="text" name="pas_password" placeholder="<?php print($STR_PASSWORD)?>" maxlength="20" tabindex="2"  class="form-control input-sm">
								</div>
								<div class="form-group">
									<label>Is Super Admin</label><span class="text-help-form"> *</span><span class="text-help-form"> (<?php print($STR_ADMIN_MSG)?>)</span>
									<select name="cbo_super" class="form-control input-sm" tabindex="3">
										<option value="NO" <?php print(CheckSelected("NO",$str_super)); ?>>NO</option>
										 <option value="YES" <?php print(CheckSelected("YES",$str_super)); ?>>YES</option>
								   </select>
								</div>
								<button type="submit" tabindex="4" class="btn btn-success" title="<?php print($STR_BUTTON_TITLE_FORM_ADD); ?>" onClick="return frm_admin_add_validation();" ><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span> Add</button>&nbsp;
								<input tabindex="5" name="btn_reset" id="btn_reset" title="<?php print($STR_BUTTON_TITLE_FORM_RESET); ?>" value="Reset" class="btn btn-danger" type="reset">
							</form>
						</div>
					</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="table-responsive">
    	<table class="table table-striped table-bordered table-hover">
        	<thead>
                <tr>
                	<th width="4%">Sr. #</th>
					<th width="9%">Registration Date</th>
					<th width="">Module Admin ID</th>
					<th width="6%">Is Super Admin?</th>
					<th width="9%">Allow Module Admin Login</th>
                    <th width="7%">Action</th>
				</tr>
			</thead>
			<tbody>
			<?php if($rs_admin_list->EOF()==true)  {  ?>
					<tr><td colspan="9" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
				<?php } else { $int_cnt=1; while(!$rs_admin_list->EOF()==true) { ?>
					<tr>
                    	<td align="center"><?php print($int_cnt)?></td>
						<td align="center"><?php print(DDMMMYYYYFormat($rs_admin_list->fields("registrationdate"))); ?></td>
						<td><span class="text-info"><strong><?php print(MyHtmlEncode(RemoveQuote($rs_admin_list->fields("loginid")))); ?></strong></span><strong> (<?php print(md5_decrypt(RemoveQuote($rs_admin_list->fields("loginpassword")), $STR_MODULE_ENCRYPTION_KEY))?></strong>)</td>
						<?php if($rs_admin_list->fields("issuperadmin") == "YES") {
								$str_adm="YES"; $str_adm_class="alert-success"; }
							else { $str_adm="NO"; $str_adm_class="alert-danger"; } ?>
						<td align="center" class="<?php print $str_adm_class; ?>"><?php print($str_adm); ?></td>
						
						<?php if($rs_admin_list->fields("allowlogin") == "YES") {
								$str_visible = "SetVisibleLink";
								$str_class="SetVisible";
								$str_bg_class="alert-success";
								$str_change = "NO";
							} else {
								$str_visible = "SetInvisibleLink";
								$str_class="SetInVisible";
								$str_bg_class="alert-danger";
								$str_change = "YES";
							} ?>
						<td align="center" class="<?php print($str_bg_class); ?>">
							<?php if($rs_admin_list->fields("issuperadmin") == "YES") { 
								print($rs_admin_list->fields("allowlogin")); }
							else if($rs_admin_list->fields("issuperadmin") == "NO" && $_SESSION['superadmin'] == "YES") {?>
								<a href="admin_status_p.php?id=<?php print($rs_admin_list->fields("pkid"))?>" title="Click to change allow login mode to '<?php print($str_change) ?>'" class="<?php print($str_visible) ?> alert-link">
							<?php print($rs_admin_list->fields("allowlogin"))?></a>
							<?php } else if($rs_admin_list->fields("issuperadmin") == "NO" && $_SESSION['superadmin'] == "NO") { print($rs_admin_list->fields("allowlogin")); } ?>
					</td>
					<td align="center">
						<a class="btn btn-success btn-xs" href="admin_edit.php?id=<?php print($rs_admin_list->fields("pkid"))?>" title="<?php print($STR_HOVER_EDIT); ?>"><i class="glyphicon glyphicon-pencil"></i></a>
						<?php  if($rs_admin_list->fields("issuperadmin") =="NO" && $_SESSION['superadmin'] == "NO") 
						{ ?><button class="btn btn-default active btn-xs help-btn disabled"><i class="glyphicon glyphicon-remove"></i></button><?php }
			 			else if($rs_admin_list->fields("issuperadmin") == "YES" && $_SESSION['superadmin'] == "YES") {?><button class="btn btn-default active btn-xs help-btn disabled"><i class="glyphicon glyphicon-remove"></i></button><?php }
								else if($rs_admin_list->fields("issuperadmin") == "NO" && $_SESSION['superadmin'] == "YES")  {  ?><a class="btn btn-danger btn-xs" href="admin_delete_p.php?id=<?php print($rs_admin_list->fields("pkid"))?>" title="<?php print($STR_HOVER_DELETE); ?>" onClick="return confirm_delete();"><i class="glyphicon glyphicon-remove"></i></a>
<?php } ?>	
					</td>
				</tr>
		<?php $int_cnt++; $rs_admin_list->MoveNext(); } ?>
		<?php }  ?>
       	</tbody>
	</table>
    </div>
	<?php include "../../includes/help_for_list.php"; ?>
</div>
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/admin.css" rel="stylesheet">
<script language="JavaScript" src="../includes/functions.js" type="text/javascript"></script>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script language="JavaScript" src="./admin_add.js" type="text/javascript"></script>
</body></html>