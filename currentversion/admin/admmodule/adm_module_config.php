<?php
#------------------------------------------------------------------------------------------------------------------------------------------------
$STR_TITLE_PAGE_MODULE = "Module Admin List";

#msgs.
	$STR_MODULE_ENCRYPTION_KEY="30110Lori_1001";
	$STR_DISPLAY_AFTER_MSG="Format of Display is : Module Name (Display Order) (Position)";
	//$STR_URL_EXAMPLE="If you have selected Module Type as \"Create As Main Category\" enter url like: <b>../user/aboutme.php</b> And If you have selected some other subcategory, enter url like: <b>./modbiography/bio_list.php</b>";
	$STR_URL_EXAMPLE="If module type \"Create As Main Category\" selected, enter url like this: <b>../user/index</b>. For all other options, enter url like: <b>./modsiteadmin/sa_list.php</b>";
	$STR_PASSWORD="Minimum 6 characters should be entered.";
	$STR_UNIQUE_PASSWORD="New Password and confirm password should be same.";
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# variables used for rss module
	$STR_RSS_TITLE_MSG="Note: Enter Title for RSS file. It will be displayed on title bar of the browser.";
	$STR_RSS_LINK_MSG="Note: Enter URL of website.";
	$STR_RSS_EMAIL_FORMAT="Format: emailid@doaminname.com";
	$STR_RSS_DESCRIPTION_MSG="Note: Enter description of site and brief introduction about its functionalities.";
	$STR_RSS_COPYRIGHT_MSG="Note: Enter site copyright information.";
	$STR_RSS_WEB_EMAIL_MSG="Note: Enter email id of the technical person responsible for the RSS file.";
	$STR_PUB_DATE_MSG="Note: Select publish date-time of RSS file.";
	$STR_RSS_VISIBLE="Note: If set NO, It will be Invisible at user site.";
	$STR_INVALID_OLD_PASSWORD="ERROR !!! Invalid old password. Please try again.";
	$STR_MSG_ACTION_INVALID_PASSWORD = "ERROR !!! Password should be minimum 6 characters long.";
	$STR_NEW_CONFIRM_PASSWORD_SAME="ERROR !!! New password and confirm password should be same.";
	$STR_RSS_FILE_MSG="Note: Enter name of rss file here.<br/>e.g. rss_photo<br/> File name can not contain /:\?*<>|\" characters.";
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# variables used for manage rss images module
	$STR_IMAGE_TITLE_MSG="Note: It will be used if image will not be found on source url or will be used as tooltip of the image.";
	$STR_IMAGE_SOURCE_URL_MSG="Note: This URL will be used as source where image is stored.";
	$STR_IMAGE_SOURCE_URL_EX_MSG="e.g. http://www.doaminname.com/user/images/image1.jpg";
	$STR_IMAGE_CLICK_URL_MSG="Note: This URL will be used as a click URL of the image.";
	$STR_IMAGE_HEIGHT_WIDTH_MSG="Note: Keep it blank or zero if you don't want to specify.";
	$STR_COMMON_DISPLAY_ORDER="Column named \"Display Order\" is used to manage display order in an ascending order on user side.";
	$INT_IMG_SIZE=778;
	
	# upload path
	$UPLOAD_IMAGE_PATH="../../mdm/rssimage/";
	$UPLOAD_IMAGE_USER_PATH="../mdm/rssimage/";
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# variables used for manage rss items module
	$STR_ITEM_LINK_MSG="Note: Enter URL where detailed description of the item is shown.";
	$STR_QUERY_MSG="Note: Enter query to get count of the item.";
	$STR_QUERY_MSG.="<br/>e.g. SELECT count(*) AS count FROM t_rss";
	$STR_QUERY_MSG.="<br/>Note: Field named 'count' must be present here.";
	$STR_QUERY_RESULT_MSG="Note: Enter text to be followed by result of above query.<br/>e.g. 10 Photo -- '10' will be result of the query and 'Photo' is the text entered here.";
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
?>