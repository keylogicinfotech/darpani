<?php
/*
	Module Name:- admin module
	File Name  :- adm_module_visible_p.php
	Create Date:- 21-Jun-2006
	Intially Create By :- 0015
	Update History:
*/

#----------------------------------------------------------------------------------------------------
#Include Files
	include "./validate_adm_login.php";
	include "../../includes/configuration.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------------------------------	
#get the data
	$int_pkid=0;
	if(isset($_GET['pkid']))
	{
		$int_pkid=$_GET['pkid'];
	}
	if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
	{
		CloseConnection();
		Redirect("adm_module_list.php?type=E&msg=F");
		exit();
	}
#----------------------------------------------------------------------------------------------------------------------------	
	#select query to get the details of pkid from t_module table
	$str_select_query="SELECT moduletitle,visible FROM t_module where modulepkid=".$int_pkid;
	$rs_visible=GetRecordset($str_select_query);
	$str_title=$rs_visible->Fields("moduletitle");
	if($rs_visible->eof())
	{
		CloseConnection();
		Redirect("adm_module_list.php?type=E&msg=F");
		exit();
	}
	
	$str_visible=$rs_visible->Fields("visible");
	if(strtoupper($str_visible)=='YES')
	{
		$str_visible="NO";
		$str_visible_title="Invisible";
	}
	else if(strtoupper($str_visible)=='NO')
	{
		$str_visible="YES";
		$str_visible_title="Visible";
	}
#-----------------------------------------------------------------------------------------------------------------------------
#update query to change the mode
	$str_select_update="UPDATE t_module SET visible='" .ReplaceQuote($str_visible). "' where modulepkid=".$int_pkid;
	ExecuteQuery($str_select_update);
	//print $str_select_update;exit;
#----------------------------------------------------------------------------------------------------------------------------	
	CloseConnection();
	Redirect("adm_module_list.php?type=S&msg=V&mode=".urlencode(RemoveQuote($str_visible))."&title=".urlencode(RemoveQuote($str_title)));
	exit();
?>