<?php
/*
	Module Name:- admmodule
	File Name  :- adm_module_edit_p.php
	Create Date:- 18-APR-2006
	Intially Create By :- 0025
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "./validate_adm_login.php";
	include "../../includes/configuration_admin.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
#get post data

$int_pkid="";
if (isset($_POST["hdn_pkid"]))
{
	$int_pkid=trim($_POST["hdn_pkid"]);
}

$str_level="";
if (isset($_POST["hdn_level"]))
{
	$str_level = trim($_POST["hdn_level"]);
}	

if($int_pkid<0 || is_numeric($int_pkid)==false || $str_level<0 || is_numeric($str_level)==false)
{
	CloseConnection();
	Redirect("adm_module_list.php?msg=F&type=E&#ptop");
	exit();
}

$str_title="";
$str_type=0;
$str_url="";
$str_tooltip="";
$str_instruction="";
$str_pos="";
$str_cbo_display="";
$str_new="";
$str_visible="";

if (isset($_POST["txt_title"]))
{
	$str_title = trim($_POST["txt_title"]);
}
if (isset($_POST["cbo_visible"]))
{
	$str_visible = trim($_POST["cbo_visible"]);
}
if (isset($_POST["cbo_newwin"]))
{
	$str_new = trim($_POST["cbo_newwin"]);
}
if($str_level!=0)
{
	if (isset($_POST["cbo_type"]))
	{
		$str_type = trim($_POST["cbo_type"]);
	}	
}
if(isset($_POST["txt_url"]))
{
	$str_url = trim($_POST["txt_url"]);
}
if(isset($_POST["txt_tooltip"]))
{
	$str_tooltip = trim($_POST["txt_tooltip"]);
}
if(isset($_POST["ta_instruction"]))
{
	$str_instruction = trim($_POST["ta_instruction"]);
}
if(isset($_POST["cbo_position"]))
{
	$str_pos = trim($_POST["cbo_position"]);
}
if($str_level==0)
{
	if(isset($_POST["cbo_display"]))
	{
		$str_cbo_display = trim($_POST["cbo_display"]);
	}
}
#----------------------------------------------------------------------------------------------------
#To check required parameters are passed properly or not

if($str_title == "")
{
	CloseConnection();
	Redirect("adm_module_edit.php?msg=F&type=E&pkid=". $int_pkid."&#ptop");
	exit();
}


if($str_type!=0)
{
	if($str_url=="")
	{
		CloseConnection();
		Redirect("adm_module_list.php?msg=F&type=E". $str_redirect."&#ptop");
		exit();
	}
}

if($str_type <0 || is_numeric($str_type)==false)
{
	CloseConnection();
	Redirect("adm_module_list.php?msg=F&type=E&#ptop");
	exit();
}

 #----------------------------------------------------------------------------------------------------
/* #Duplicate Checking for category Title field.

$str_query_dup="select moduletitle from t_module where modulepkid!=".$int_pkid." and moduleparentpkid=".$str_type." and moduletitle= '" . ReplaceQuote($str_title) . "'";
$rs_duplicate_check=GetRecordSet($str_query_dup);

if($rs_duplicate_check->eof() !=true)
{
	CloseConnection();
	$msg="DU";
	if($str_type==0)
	{
		$msg="DU";
	}
	else
	{
		$msg="SDU";
	}
	Redirect("module_edit.php?msg=".$msg."&type=E&pkid=". $int_pkid."&title=".urlencode(RemoveQuote($str_title))."&#ptop");
	exit();
}
 */#----------------------------------------------------------------------------------------------------
$msg="U";
	if($str_level==0)
	{
		$int_parnentpkid=$str_level;
		$msg="U";
		if($str_cbo_display!=0)
		{
			$str_dislayorder="displayorder=". ($str_cbo_display+1) .",";
		}
		else
		{
			$str_dislayorder="";
		}
	}
	else
	{
		$int_parnentpkid=$str_type;
		$str_pos="";
		$msg="SU";
		$str_dislayorder="";
	}

#Update record in t_module table 
$str_query_update = "update t_module set moduletitle='" . ReplaceQuote($str_title) . "',";
$str_query_update .= "moduleparentpkid=" . $int_parnentpkid . ",";
$str_query_update .= "url='" . ReplaceQuote($str_url) . "',";
$str_query_update .= "tooltip='" . ReplaceQuote($str_tooltip) . "',"; 
$str_query_update .= $str_dislayorder;
$str_query_update .= "instructiontext='" . ReplaceQuote($str_instruction) . "',";
$str_query_update .= "visible='" . ReplaceQuote($str_visible) . "',";
$str_query_update .= "openinnewwindow='" . ReplaceQuote($str_new) . "',";
$str_query_update .= "position='" . ReplaceQuote(strtoupper($str_pos)) . "' where modulepkid=". $int_pkid;
/* print($str_query_update);
exit();
 */ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
	if($str_cbo_display!=0)
	{
		$str_update="update t_module set displayorder=displayorder+1 where moduleparentpkid=0 and displayorder >".$str_cbo_display+1;
		ExecuteQuery($str_update);
	}	

#Close connection and redirect to adm_module_list.php page	
	CloseConnection();
	Redirect("adm_module_list.php?type=S&msg=".$msg."&title=".urlencode(RemoveQuote($str_title))."&#ptop");
	exit();
#------------------------------------------------------------------------------------------------------------
?>