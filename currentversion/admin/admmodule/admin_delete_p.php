<?php
/*
	Module Name:- modsiteadmin
	File Name  :- sa_add.php
	Create Date:- 01-02-2006
	Intially Create By :- 0022
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "./validate_adm_login.php";
	include "../../includes/configuration.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_datetimeyear.php";
	include "../../includes/lib_common.php";
	
	if (strtoupper($_SESSION['superadmin'])=="NO")
	{ 
		CloseConnection();
		Redirect("../adm_home.php");
		exit();
	}


#	To get values passed from previous page.
	
	$int_pkid="";
	if(isset($_GET["id"]))
	{
		$int_pkid = trim($_GET["id"]);
	}
	
#	To check whether values are passed properly or not.
	if($int_pkid == "" || $int_pkid<=0 || !is_numeric($int_pkid))
	{
		CloseConnection();
		Redirect("./adm_module_admin_list.php?type=E&msg=F");
		exit();
	}
	
	
#	select qurey to get the admin name to be deleted.
	$str_query_select = "select loginid,issuperadmin from t_module_login where pkid=" . $int_pkid;
	$rs_admin_name = GetRecordset($str_query_select);
	$str_admin_name = $rs_admin_name->fields("loginid");	
	$str_admin_super = $rs_admin_name->fields("issuperadmin");	
	if(strtoupper($str_admin_super)=='YES')
	{
		CloseConnection();
		Redirect("adm_module_admin_list.php");
		exit();
	}

#	Delete query from t_siteadmin to delete record from database.
	$str_query_delete = "delete from t_module_login where pkid=" . $int_pkid;
	ExecuteQuery($str_query_delete);
	
#---------------------------------------------------------------------------------------------------------------	
	CloseConnection();
	Redirect("./adm_module_admin_list.php?type=S&msg=D&admin=".RemoveQuote($str_admin_name)."&#ptop");
	exit();	
?>
