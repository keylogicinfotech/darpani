<?php
/*
	Module Name:- admmodule
	File Name  :- adm_module_edit.php
	Create Date:- 18-APR-2006
	Intially Create By :- 0025
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "./validate_adm_login.php";
	include "../../includes/configuration_admin.php";
	include "adm_module_config.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
#get Query String Data
$int_pkid="";
if(isset($_GET["pkid"])==true)
{
	$int_pkid=trim($_GET["pkid"]);
}
	
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
	CloseConnection();
	Redirect("adm_module_list.php?msg=F&type=E&#ptop");
	exit();
}

# get Query String Data
$str_title="";
if (isset($_GET["title"]))
{
	$str_title = trim($_GET["title"]);
}
#----------------------------------------------------------------------------------------------------
#select record from t_module table

$str_query_select="select * from t_module where modulepkid=". $int_pkid;
$rs_edit=GetRecordSet($str_query_select);

if($rs_edit->eof()==true)
{
	CloseConnection();
	Redirect("adm_module_list.php?msg=F&type=E&#ptop");
	exit();
}
#------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
   	$str_type = "";
	$str_message = "";
#	Get message type.
   		if(isset($_GET["type"]))
		{
			switch(trim($_GET["type"]))
			{
				case("S"): $str_type = "S"; break;
				case("E"): $str_type = "E"; break;
				case("W"): $str_type = "W"; break;
			}
		}
#	Get message text.
		if(isset($_GET["msg"]))
		{
			switch(trim($_GET["msg"]))
			{
				case("F"): $str_message = "Some information(s) missing.Please try again."; break;				
				case("DU"): $str_message = "Module title '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' already exists. Please try with another title."; break;
				case("SDU"): $str_message = "Module sub category title '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' already exists. Please try with another title."; break;
			}
		}
#----------------------------------------------------------------------------------------------------
?>
<?php /*?><html>
<head>
<title>:: Edit Module ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="778" border="0" cellpadding="0" cellspacing="0" align="center">
 <tr> 
  <td><?php include "./adm_module_header.php";?></td>
 </tr>
</table>
<a name="ptop"></a>
<table width="778" align="center" cellpadding="0" cellspacing="0">
 <tr> 
  <td background="../images/tablebg.gif" height="390" valign="top"><br>
   <table width="740" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr> 
     <td align="center" height="25" class="PageHeader">
	 Edit Module: <?php print(MyHtmlEncode(MakeStringShort($rs_edit->fields("moduletitle"),45)));?></td>
    </tr>
	<tr> 
     <td height="2" align="right" class="TableHeader8ptBold"></td>
   </tr>
   </table>
   <table width="740" border="0" align="center" cellpadding="2" cellspacing="0">
    <tr> 
     <td width="95%" height="20" class="DescriptionText8ptNormal"><a href="./adm_home.php" class="NavigationLink" title="Go to control panel main menu">Main Menu</a> &gt;&gt; <a href="adm_module_list.php" title="Go to Module list" 
	 class="NavigationLink">Module List</a>&nbsp;&gt;&gt;&nbsp;Edit Module 
	 Category: <?php print(MyHtmlEncode(MakeStringShort($rs_edit->fields("moduletitle"),50)));?>
	 </td>
     <td width="5%" align="right" class="DescriptionText8ptNormal">&lt;&lt;&nbsp;<a href="adm_module_list.php" class="NavigationLink" title="Back to Module list">Back</a></td>
	</tr>
   </table>
   <?php
#------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
   	$str_type = "";
	$str_message = "";
#	Get message type.
   		if(isset($_GET["type"]))
		{
			switch(trim($_GET["type"]))
			{
				case("S"): $str_type = "S"; break;
				case("E"): $str_type = "E"; break;
				case("W"): $str_type = "W"; break;
			}
		}
#	Get message text.
		if(isset($_GET["msg"]))
		{
			switch(trim($_GET["msg"]))
			{
				case("F"): $str_message = "Some information(s) missing.Please try again."; break;				
				case("DU"): $str_message = "Module title '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' already exists. Please try with another title."; break;
				case("SDU"): $str_message = "Module sub category title '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' already exists. Please try with another title."; break;
			}
		}
#	Diplay message.
		if($str_type != "" && $str_message != "") { print(DisplayMessage(740,$str_message,$str_type)); }
?>
   <br>
      <a name="ptop"></a> 
      <table width="740" border="0" cellspacing="0" cellpadding="0" align="center">
   <tr>
    <td width="100%"> 
     <table width="100%" height="23" border="0" cellspacing="0" cellpadding="0">
      <tr class="TableHeader10ptBold">
       	<td width="95%">Edit Module Details</td>
       	<td align="right"><a href="#help" class="NavigationLink" title="Click to view help">Help</a></td>
      </tr>
     </table> 
	</td>
   </tr>
   <tr>
    <td>
	 <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0" class="tbl">
     <form name="frm_module_edit" action="adm_module_edit_p.php" method="post" onSubmit="return frm_module_edit_validateform();">
      <tr><td width="28%" height="10"></td><td></td></tr>
	  <tr>
	   <td align="right" valign="top" class="FieldCaptionText8ptNormal">Module Title:<span class="text-help-form">*</span></td>
	   <td align="left" valign="top"><input type="text" name="txt_title" value="<?php print(MyHtmlEncode($rs_edit->fields("moduletitle"))); ?>" maxlength="255" size="70" class="clsTextBox"><br><span class="text-help-form"><?php print($STR_MAX_255_MSG);?></span></td>
	  </tr>
	   <?php $str_query_select="select modulepkid,moduletitle,displayorder,position from t_module where moduleparentpkid=0 order by moduletitle";
			$rs_list=GetRecordSet($str_query_select);
			$str_temp="";
	   		if($rs_edit->fields("moduleparentpkid")!=0) { ?>
	   <tr>
	   <td align="right" valign="top" class="FieldCaptionText8ptNormal">Module Type:<span class="text-help-form">*</span></td>
	   <td align="left" valign="top"><select name="cbo_type">
	   		<?php while($rs_list->eof()==false) { ?>
	   		<option value="<?php print($rs_list->fields("modulepkid")); ?>" <?php print(CheckSelected($rs_list->fields("modulepkid"),$rs_edit->fields("moduleparentpkid")));?>><?php print($rs_list->fields("moduletitle")); 
			 ?></option>
		   <?php $rs_list->movenext(); } ?>
	   </select>	   
	   </td>
	   </tr>	
	   <?php } ?>  
      <tr> 
       <td align="right" valign="top" class="FieldCaptionText8ptNormal">Url:</td>
            <td align="left" valign="top"><input type="text" name="txt_url" value="<?php print(MyHtmlEncode($rs_edit->fields("url"))); ?>" maxlength="255" size="70" class="clsTextBox"><br><span class="text-help-form"><?php print($STR_MAX_255_MSG);?></span></td>
      </tr>
      <tr> 
       <td align="right" valign="top" class="FieldCaptionText8ptNormal">Tooltip:</td>
            <td align="left" valign="top"><input type="text" name="txt_tooltip" value="<?php print(MyHtmlEncode($rs_edit->fields("tooltip"))); ?>" maxlength="100" size="70" class="clsTextBox"><br><span class="text-help-form"><?php print($STR_MAX_100_MSG);?></span></td>
      </tr>
	        <tr> 
       <td align="right" valign="top" class="FieldCaptionText8ptNormal">Instruction text:</td>
		<td align="left" valign="top"><textarea class="clsTextArea" name="ta_instruction" cols="50" rows="5"><?php print(MyHtmlEncode($rs_edit->fields("instructiontext"))); ?></textarea></td>
      </tr>
		<?php if($rs_edit->fields("moduleparentpkid")==0) { ?>
		<tr> 
       <td align="right" valign="top" class="FieldCaptionText8ptNormal">Position:</td>
		<td align="left" valign="top">
		<select name="cbo_position" <?php print($str_show);?>>
			<option value="LEFT" <?php print(CheckSelected("LEFT",strtoupper($rs_edit->fields("position"))));?>>Left</option>
			<option value="RIGHT" <?php print(CheckSelected("RIGHT",strtoupper($rs_edit->fields("position"))));?>>Right</option>
		</select></td>
      </tr>
	   <tr>
	   <td align="right" valign="top" class="FieldCaptionText8ptNormal">Display After Module:<span class="text-help-form">*</span></td>
	   <td align="left" valign="top" class="text-help-form"><select name="cbo_display" >
	   <option value="0"><-- Keep Display Order As It Is --></option>
	   <?php while($rs_list->eof()==false) { ?>
	   		<option value="<?php print($rs_list->fields("displayorder")); ?>" ><?php print($rs_list->fields("moduletitle")); ?> (<?php print($rs_list->fields("displayorder")); ?>) (<?php print($rs_list->fields("position")); ?>)</option>
	   <?php  $rs_list->movenext(); } ?>
	   </select><br>
			<?php print($STR_DISPLAY_AFTER_MSG);?>   
	   </td>
	   </tr>
	     <?php }?>
	   <tr>
	     <td align="right" valign="top" class="FieldCaptionText8ptNormal">Visible:</td>
	     <td align="left" valign="top" class="text-help-form">
           <select name="cbo_visible">
             <option value="YES" <?php print(CheckSelected($rs_edit->fields("visible"),"YES")); ?>>YES</option>
             <option value="NO" <?php print(CheckSelected($rs_edit->fields("visible"),"NO")); ?>>NO</option>
           </select><br><?php print($STR_VISIBLE);?> </td>
	     </tr>
	   <tr>
	     <td align="right" valign="top" class="FieldCaptionText8ptNormal">Open In New Window: </td>
	     <td align="left" valign="top" class="text-help-form">
           <select name="cbo_newwin">
             <option value="YES" <?php print(CheckSelected($rs_edit->fields("openinnewwindow"),"YES")); ?>>YES</option>
             <option value="NO" <?php print(CheckSelected($rs_edit->fields("openinnewwindow"),"NO")); ?>>NO</option>
           </select><br><?php print($STR_NEW_WINDOW);?> 
		  	 </td>
	     </tr>
	  <tr> 
       <td height="5"></td>
       <td></td>
      </tr>
      <tr> 
       <td height="26">
			<input type="hidden" name="hdn_pkid" value="<?php print($int_pkid);?>">
			<input type="hidden" name="hdn_level" value="<?php print($rs_edit->fields("moduleparentpkid")); ?>">
	   </td>
       <td align="left">
			<input class="ButtonStyle" name="btn_submit" type="submit" id="btn_submit" title="Click to save Module details" value="Save Changes">
                   &nbsp;&nbsp;
			<input name="btn_reset" class="ButtonStyle" type="reset" id="btn_reset" title="Click to reset the form " value="Reset"></td>			
      </tr>
      <tr> 
       <td height="10"></td>
       <td></td>
      </tr>
     </form>
     </table>
	</td>
   </tr>
  </table>
  <br><a name="help"></a>
  <table width="740" border="0" cellpadding="2" cellspacing="0" align="center">
   <tr class="TableHeader8ptBold"> 
    <td height="20" class="TableHeader">
	 <table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr class="TableHeader10ptBold"> 
       <td>Help Section</td>
       <td align="right"><a href="#ptop" class="NavigationLink" title="Go to top">Top</a></td>
      </tr>
     </table>
	</td>
   </tr>
   <tr valign="top"> 
    <td>
	 <table width="100%" border="0" cellpadding="2" cellspacing="0">
              <tr> 
                <td width="20" valign="baseline" class="text-help-form">&#8226;</td>
                <td valign="baseline" class="text-help-form"><?php print($STR_MANDATORY); ?></td>
              </tr>
            </table>
	</td>
   </tr>
  </table>
  <br>
 </td>
</tr>
</table>
	<?php
	include "../includes/adminfooter.php";
	CloseConnection();
	?>
<link href="../includes/admin.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="../includes/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="adm_module_edit.js" type="text/javascript"></script>
</body>
</html>

<?php */?>


<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_FORM_UPDATE); ?> : <?php print($STR_TITLE_MODULE_LIST);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../includes/bootstrap.min.css" rel="stylesheet">
<link href="../../includes/admin.css" rel="stylesheet">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
	<?php include "./adm_module_header.php";?>
	<div class="row padding-10">
		<div class="col-md-3 col-sm-6 col-xs-12 button_space">
			<div class="btn-group" role="group" aria-label="...">
				<a href="./adm_module_list.php" class="btn btn-default" title="<?php print($STR_HREF_TITLE_LINK);?> <?php print($STR_TITLE_MODULE_LIST); ?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_MODULE_LIST); ?></a>
			</div>
		</div>
		<div class="col-md-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_FORM_UPDATE); ?> : <?php print(MyHtmlEncode(MakeStringShort($rs_edit->fields("moduletitle"),45)));?></h3>
		
		</div>
	</div><hr> 
	<?php #	Diplay message.
		if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); }?>
	<div class="row padding-10">
    	<div class="col-md-12">
        	<div class="panel-group" id="accordion">
            	<div class="panel panel-default">
                	<div class="panel-heading">
                    	<h4 class="panel-title">
							<div class="row padding-10">
									<div class="col-md-6 col-sm-6 col-xs-8"> 
                                                                            <i class="glyphicon  glyphicon-edit "></i>&nbsp;&nbsp;<b><?php print($STR_TITLE_FORM_UPDATE); ?></b> </a>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>												
							</div>
						</h4>
                    </div>
                    <div id="collapse1" class="panel-collapse">
						<div class="panel-body">
							 <form name="frm_module_edit" action="adm_module_edit_p.php" method="post" onSubmit="return frm_module_edit_validateform();">
								<div class="text-help-form" align="right"><?php print($STR_MANDATORY); ?></div>
								<div class="form-group">
									<label>Module Title</label> <span class="text-help-form">* (<?php print($STR_MAX_255_MSG);?>)</span>
								  <input type="text" tabindex="1" placeholder="<?php print($STR_PASSWORD);?>" name="txt_title" value="<?php print(MyHtmlEncode($rs_edit->fields("moduletitle"))); ?>"  size="50" maxlength="255" class="form-control input-sm">
								</div>
								<?php 
								$str_query_select="select modulepkid,moduletitle,displayorder,position from t_module where moduleparentpkid=0 order by moduletitle";
								$rs_list=GetRecordSet($str_query_select);
								$str_temp="";
	   							if($rs_edit->fields("moduleparentpkid")!=0) { ?>
								<div class="form-group">
									<label>Module Type</label> <span class="text-help-form">*</span>
									<select name="cbo_type"  class="form-control input-sm" tabindex="2">
	   									<?php while($rs_list->eof()==false) { ?>
	   									<option value="<?php print($rs_list->fields("modulepkid")); ?>" <?php print(CheckSelected($rs_list->fields("modulepkid"),$rs_edit->fields("moduleparentpkid")));?>><?php print($rs_list->fields("moduletitle")); ?></option>
		   								<?php $rs_list->movenext(); } ?>
	   								</select>	  
								</div>
								<?php } ?>
								<div class="form-group">
									<label>Url</label><?php if($rs_edit->fields("moduleparentpkid")!=0) { ?> <span class="text-help-form">* (<?php print($STR_URL_EXAMPLE);?>)</span><?php } else { ?> <span class="text-help-form"> (<?php print($STR_URL_EXAMPLE);?>)</span><?php } ?>
									<input type="text" tabindex="3" name="txt_url" value="<?php print(MyHtmlEncode($rs_edit->fields("url"))); ?>" size="50" maxlength="255" class="form-control input-sm"> 
								</div>		
								<div class="form-group">
									<label>Tooltip</label> <span class="text-help-form"> (<?php print($STR_MAX_100_MSG);?>)</span> 
									<input type="text" name="txt_tooltip" tabindex="4" value="<?php print(MyHtmlEncode($rs_edit->fields("tooltip"))); ?>" maxlength="100" size="50" class="form-control input-sm">
								</div>
								<div class="form-group">
									<label>Instruction text</label><span class="'text-help-form"> </span>
									<textarea class="form-control input-sm" tabindex="5" name="ta_instruction" cols="100" rows="10"><?php print(MyHtmlEncode($rs_edit->fields("instructiontext"))); ?></textarea> 
								</div>
								<?php if($rs_edit->fields("moduleparentpkid")==0) { ?>								
								<div class="form-group">
									<label>Position</label><span class="'text-help-form"> *</span>
									<select  class="form-control input-sm" name="cbo_position" <?php print($str_show);?> tabindex="6" >
										<option value="LEFT" <?php print(CheckSelected("LEFT",strtoupper($rs_edit->fields("position"))));?>>Left</option>
										<option value="MIDDLE" <?php print(CheckSelected("MIDDLE",strtoupper($rs_edit->fields("position"))));?>>Middle</option>
										<option value="RIGHT" <?php print(CheckSelected("RIGHT",strtoupper($rs_edit->fields("position"))));?>>Right</option>
									</select>
								</div>
								<div class="form-group">
									<label>Display After Module</label><span class="text-help-form"> * (<?php print($STR_DISPLAY_AFTER_MSG);?>)</span>
									<select name="cbo_display" tabindex="7" class="form-control input-sm">
	   <option value="0"><--- Keep Display Order As It Is ---></option>
	   <?php while($rs_list->eof()==false) { ?>
	   		<option value="<?php print($rs_list->fields("displayorder")); ?>" ><?php print($rs_list->fields("moduletitle")); ?> (<?php print($rs_list->fields("displayorder")); ?>) (<?php print($rs_list->fields("position")); ?>)</option>
	   <?php  $rs_list->movenext(); } ?>
	   </select>
								</div>
								<?php }?>	
								<div class="form-group">
									<label>Visible</label><span class="text-help-form"> * (<?php print($STR_VISIBLE);?>)</span>
									<select name="cbo_visible"  tabindex="8" class="form-control input-sm">
             							<option value="YES" <?php print(CheckSelected($rs_edit->fields("visible"),"YES")); ?>>YES</option>
             							<option value="NO" <?php print(CheckSelected($rs_edit->fields("visible"),"NO")); ?>>NO</option>
           							</select>
								</div>
								<div class="form-group">
									<label>Open Link In New Window</label><span class="text-help-form"> * </span>
									<select name="cbo_newwin"  tabindex="9" class="form-control input-sm">
             							<option value="YES" <?php print(CheckSelected($rs_edit->fields("openinnewwindow"),"YES")); ?>>YES</option>
             							<option value="NO" <?php print(CheckSelected($rs_edit->fields("openinnewwindow"),"NO")); ?>>NO</option>
           </select>
								</div>
								
								
								<input name="hdn_pkid" type="hidden" value="<?php print($int_pkid) ?>">
								<input type="hidden" name="hdn_level" value="<?php print($rs_edit->fields("moduleparentpkid")); ?>">
								<button type="submit" class="btn btn-success" name="btn_submit" id="btn_submit" title="<?php print($STR_BUTTON_TITLE_FORM_EDIT); ?>"tabindex="10"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</button>&nbsp;
								<input name="btn_reset" id="btn_reset" title="<?php print($STR_BUTTON_TITLE_FORM_RESET); ?>" value="Reset" class="btn btn-danger" type="reset" tabindex="11">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <a name="help"></a>
    <?php /*?><?php include "../../includes/help_for_edit.php"; ?><?php */?>
</div>
<?php include "../../includes/include_files_admin.php"; ?>
</body></html>