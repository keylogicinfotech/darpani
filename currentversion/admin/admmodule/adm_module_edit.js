/*
	Module Name:- admmodule
	File Name  :- adm_module_edit.js
	Create Date:- 18-APR-2006
	Intially Create By :- 0025
	Update History:
*/

function frm_module_edit_validateform()
{
	with(document.frm_module_edit)
	{
		if(trim(txt_title.value) == "")
		{
			alert("Please enter module title.");
			txt_title.select();
			txt_title.focus();			
			return false;
		}
		if(cbo_type.value!=0 )
		{
			if(txt_url.value == "")
		{
			alert("Please enter url.");
			txt_url.focus();			
			return false;
		}
		}
		
		if(hdn_level.value!=0)
		{			
			if(cbo_type.value == "")
			{
				alert("Please select module type.");
				cbo_type.focus();			
				return false;
			}
		}
						
	}
	return true;
}