<?php
$STR_ADMIN_MSG="Select YES if you want to display as super admin.";
$STR_UNIQUE_ADMIN="Use only letters, numbers and underscore ( _ ). Admin ID can't be updated once created.";
$STR_PASSWORD="Use only letters and numbers. Password length must be minimum 6 characters.";

# -------------------------------------------------------------------------------------------------------------------
# function to write rss file
# -------------------------------------------------------------------------------------------------------------------

	function publish_rss_file($pkid,$image_file_path)
	{
		$arr_main=array();
		$arr_image=array();
		$arr_item=array();
		$str_file_path="../../user/";
		$str_file_name="";
		
		# select query to get data from t_rss
		$str_query_select="SELECT * FROM t_rss WHERE rsspkid=".$pkid;
		$rs_rss=GetRecordSet($str_query_select);

		# select query to get data from tr_rss_image
		$str_query_select="SELECT * FROM tr_rss_image WHERE rsspkid=".$pkid." AND visible='YES' ORDER BY displayorder";
		$rs_image=GetRecordSet($str_query_select);

		# select query to get data from tr_rss_item
		$str_query_select="SELECT * FROM tr_rss_item WHERE rsspkid=".$pkid." AND visible='YES' ORDER BY displayorder";
		$rs_item=GetRecordSet($str_query_select);
		
		# make array of main tags
		if($rs_rss->EOF()==false)
		{
			$arr_main["title"]=$rs_rss->fields("title");
			$arr_main["link"]=$rs_rss->fields("siteurl");
			$arr_main["description"]=$rs_rss->fields("description");
			$arr_main["pubDate"]=date("d-M-Y h:i A",strtotime($rs_rss->fields("publishdate")));
			$arr_main["lastBuildDate"]=date("d-M-Y h:i A");
			
			if($rs_rss->fields("webmasteremail")!="")
			{
				$arr_main["webMaster"]=$rs_rss->fields("webmasteremail");
			}
			
			if($rs_rss->fields("copyright")!="")
			{
				$arr_main["copyright"]=$rs_rss->fields("copyright");
			}
			
			$str_file_name=$rs_rss->fields("rssfilename");
		}
		
		# make array of images
		$i=0;
		while($rs_image->EOF()==false)
		{
			$str_source="";
			
			if($rs_image->fields("imagefilename")!="")
			{
				$str_source=$image_file_path.$rs_image->fields("imagefilename");
			}
			else
			{
				$str_source=$rs_image->fields("sourceurl");
			}
			
			$arr_image[$i]["url"]=$str_source;
			$arr_image[$i]["title"]=$rs_image->fields("title");
			$arr_image[$i]["link"]=$rs_image->fields("clickurl");
			if($rs_image->fields("height")!=0)
			{
				$arr_image[$i]["height"]=$rs_image->fields("height");
			}
			if($rs_image->fields("width")!=0)
			{
				$arr_image[$i]["width"]=$rs_image->fields("width");			
			}
			
			$i=$i+1;
			$rs_image->MoveNext();
		}

		# make array of items
		$i=0;
		while($rs_item->EOF()==false)
		{
			$str_res="";
			if($rs_item->fields("query")!="")
			{
				$arr_temp=explode("|",$rs_item->fields("query"));
				$rs_temp=GetRecordSet($arr_temp[0]);
				if($rs_temp->EOF()==false)
				{
					$str_res=$rs_temp->fields("count");
				}	
				$str_res=" [ ". $str_res." ". $arr_temp[1] . " ] ";
			}
			
			$arr_item[$i]["title"]=$rs_item->fields("title").$str_res;
			$arr_item[$i]["description"]=$rs_item->fields("description");
			$arr_item[$i]["link"]=$rs_item->fields("link");
			///$arr_item[$i]["content:encoded"]=$rs_item->fields("description");
			
			$i=$i+1;
			$rs_item->MoveNext();
		}
		
		write_rss_file($str_file_name,$arr_main,$arr_image,$arr_item);
		
	}
# -------------------------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------
	function write_rss_file($file_name,$arr_rmain,$arr_rimage,$arr_ritem)
	{
		$file_path="../../user/".$file_name.".xml";
		
		$xml_string="<?xml version=\"1.0\" encoding=\"ISO8859-1\"?>\r\n";
		$xml_string.="<?xml-stylesheet type='text/xsl' href='".$file_name.".xslt' version='1.0'?>\r\n";
		/*$xml_string.="<rss version=\"2.0\"\r\n";
		$xml_string.="xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\r\n";
		$xml_string.="xmlns:sy=\"http://purl.org/rss/1.0/modules/syndication/\"";
		xmlns:admin="http://webns.net/mvcb/"
					  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
					  xmlns:content="http://purl.org/rss/1.0/modules/content/">*/
		$xml_string.="<rss version=\"2.0\">\r\n";
		$xml_string.="<channel>\r\n";
		
		if(is_array($arr_rmain)==true)
		{
			foreach($arr_rmain as $key => $value )
			{
				if($key=="description")
				{
					$xml_string.="<".$key."><![CDATA[".$value."]]></".$key.">\r\n";
				}
				else
				{
					$xml_string.="<".$key.">".$value."</".$key.">\r\n";
				}
			}
			$xml_string.="<generator>Radio UserLand v8.2.1</generator>\r\n";
		}


		if(is_array($arr_rimage)==true)
		{
			for($i=0;$i< count($arr_rimage) ;$i++)
			{
				$xml_string.="<image>\r\n";
					foreach($arr_rimage[$i] as $key => $value )
					{
						$xml_string.="<".$key.">".$value."</".$key.">\r\n";
					}
					//$xml_string.="<pubdate>".date("Y-m-d")."</pubdate>\r\n";
				$xml_string.="</image>\r\n";
			}
		}
		
		$xml_string.="\r\n\r\n";
		
		if(is_array($arr_ritem)==true)
		{
			for($i=0;$i< count($arr_ritem) ;$i++)
			{
				$xml_string.="<item>\r\n";
					foreach($arr_ritem[$i] as $key => $value )
					{
						if($key=="description")
						{
							$xml_string.="<".$key."><![CDATA[".$value."]]></".$key.">\r\n";
							//$xml_string.="<".$key.":encoded>".$value."</".$key.":encoded>\r\n";
						}
						else
						{
							$xml_string.="<".$key.">".$value."</".$key.">\r\n";
						}
					}
					
					$xml_string.="<guid>".$i."</guid>\r\n";
					
					if($arr_rmain["webMaster"]!="")
					{
						$xml_string.="<author>".$arr_rmain["webMaster"]."</author>\r\n";
					}
					
					$xml_string.="<pubDate>".date("Y-m-d h:i A")."</pubDate>\r\n";
				$xml_string.="</item>\r\n";
			}
		}
		
		$xml_string.="</channel>\r\n";
		$xml_string.="</rss>";

		$file=fopen( $file_path ,"w")	or die("Error in creating file!!!");
		fwrite($file,$xml_string);
		if($file)
		{
			fclose($file);
		}
	
	}
# -------------------------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------
?>