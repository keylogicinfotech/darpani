<?php
/*
	Module Name:- admmodule
	File Name  :- adm_module_position_p.php
	Create Date:- 18-APR-2006
	Intially Create By :- 0025
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "./validate_adm_login.php";
	include "../../includes/configuration_admin.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
#get Query String Data
$int_pkid="";
$req_pos="";
if(isset($_GET["pkid"])==true)
{
	$int_pkid=trim($_GET["pkid"]);
}	
if(isset($_POST["txt_url"]))
{
	$str_url = trim($_POST["txt_url"]);
}

if(isset($_GET["reqpos"])==true)
{
	$req_pos=trim($_GET["reqpos"]);
}	
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
	CloseConnection();
	Redirect("adm_module_list.php?msg=F&type=E&#ptop");
	exit();
}
#----------------------------------------------------------------------------------------------------
#select query to get product details from t_module table
$str_query_select="SELECT moduletitle,position,moduleparentpkid FROM t_module WHERE modulepkid=". $int_pkid;
$rs_position=GetRecordSet($str_query_select);
//print $str_query_select; exit;

if($rs_position->eof()==true)
{
	CloseConnection();
	Redirect("adm_module_list.php?msg=F&type=E&#ptop");
	exit();
}
$str_existing_mode="";
$str_title="";
$str_existing_mode=$rs_position->fields("position");
$str_title=trim($rs_position->fields("moduletitle"));

$msg="P";
if($rs_position->fields("moduleparentpkid")==0)
{
	$msg="P";
}
else
{
	$msg="SV";
}
#----------------------------------------------------------------------------------------------------
#change position mode
$str_mode_title="";
$str_req_mode="";
if($int_pkid!="" || $req_pos!="")
{
	if(strtoupper($str_existing_mode)=="LEFT")
	{
		$str_req_mode=$req_pos;
		$str_mode_title=$req_pos;
	}
	else if(strtoupper($str_existing_mode)=="MIDDLE")
	{
		$str_req_mode=$req_pos;
		$str_mode_title=$req_pos;
	}
	else if(strtoupper($str_existing_mode)=="RIGHT")
	{
		$str_req_mode=$req_pos;
		$str_mode_title=$req_pos;
	}
}
#----------------------------------------------------------------------------------------------------
#update status in t_module table
$str_query_update="UPDATE t_module SET position='" . ReplaceQuote($str_req_mode) . "' WHERE modulepkid=".$int_pkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to prod_cat_list.php page	
	CloseConnection();
	Redirect("adm_module_list.php?type=S&msg=".$msg."&title=".urlencode(RemoveQuote($str_title))."&po_mode=". urlencode(RemoveQuote($str_mode_title)) ."&#ptop");
	exit();
#------------------------------------------------------------------------------------------------------------
?>