<?php
/*
	Module Name:- modsiteadmin
	File Name  :- sa_add_p.php
	Create Date:- 12-DEC-2016
	Intially Create By :- 015
	Update History:
*/
#--------------------------------------------------------------------------------------------------------------------------------------
#Include Files
	//include "../includes/validatesession.php";
	include "./validate_adm_login.php";
	include "../../includes/configuration.php";
	include "./adm_module_config.php";
//include "./sa_config.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_datetimeyear.php";
	include "../../includes/lib_common.php";
#--------------------------------------------------------------------------------------------------------------------------------------
	$str_super="";

#	To check whether values are passed properly or not.	
	if(isset($_POST["txt_admin_name"]))
	{
		$str_admin_name = trim($_POST["txt_admin_name"]);
	}
	if(isset($_POST["pas_password"]))
	{
		$str_password = trim($_POST["pas_password"]);
	}

	if(isset($_POST["cbo_super"]))
	{
		$str_super = trim($_POST["cbo_super"]);
	}
#-------------------------------------------------------------------------------------------------------------------------------
	$str_redirect="&stradminname=".urlencode(RemoveQuote($str_admin_name))."";
	$str_redirect=$str_redirect."&admin=".urlencode(RemoveQuote($str_admin_name))."&super=".urlencode(RemoveQuote($str_super));
#-------------------------------------------------------------------------------------------------------------------------------
# To check required parameters are passed properly or not
	
	if($str_admin_name == "" || $str_password == "" || $str_super=="")
	{
		CloseConnection();
		Redirect("./adm_module_admin_list.php?type=E&msg=F".$str_redirect."&#ptop");
		exit();
	}
	if(strstr($str_admin_name," ")!=false || strstr($str_password," ")!=false)
	{
		CloseConnection();
		Redirect("./adm_module_admin_list.php?type=E&msg=N".$str_redirect."&#ptop");
		exit();
	}
	
	if (strlen($str_password) < 6)
	{	
		CloseConnection();
		Redirect("./adm_module_admin_list.php?type=E&msg=P".$str_redirect."&#ptop");
		exit();		
	}

#-------------------------------------------------------------------------------------------------------------------------------	
#	select query from t_siteadmin to check whether particular admin already exists or not.
	$str_query_select = "select count(*) as NoOfRecords from t_module_login where loginid='" . ReplaceQuote($str_admin_name) . "'";
	$rs_duplicate_check = GetRecordset($str_query_select);
	if($rs_duplicate_check->fields("NoOfRecords") > 0)
	{
		CloseConnection();
		Redirect("./adm_module_admin_list.php?type=E&msg=DU".$str_redirect."&#ptop");
		exit();
	}
#-------------------------------------------------------------------------------------------------------------------------------	
#	insert query into t_siteadmin table to insert record.

	$str_query_insert = "INSERT INTO t_module_login(loginid,loginpassword,allowlogin,registrationdate,";
	$str_query_insert .= "issuperadmin)";
	//$str_query_insert .= "
	$str_query_insert .= " VALUES ('" . ReplaceQuote($str_admin_name) . "','" . md5_encrypt(ReplaceQuote($str_password), $STR_MODULE_ENCRYPTION_KEY ) . "','YES','";
	$str_query_insert .= ReplaceQuote(YYYYMMDDFormat(date('Y-m-d'))) . "','" . ReplaceQuote($str_super) ."')";
	
	//print ($str_query_insert); exit;
	
	ExecuteQuery($str_query_insert);
#-------------------------------------------------------------------------------------------------------------------------------
#Close connection and redirect to siteadmin_add.php page	
	CloseConnection();
	Redirect("./adm_module_admin_list.php?type=S&msg=S&admin=".urlencode(RemoveQuote($str_admin_name))."&#ptop");
	exit();
?>