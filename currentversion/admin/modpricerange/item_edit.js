function frm_edit_validateform()
{
    with(document.frm_edit)
    {
        
        if(trim(txt_title.value) == "")
        {
            alert("Please enter title.");
            txt_title.select();
            txt_title.focus();
            return false;
        }
        if(trim(txt_from_amt.value) == ""  || txt_from_amt.value < 0)
        {
            alert("Please enter from amount.");
            txt_from_amt.select();
            txt_from_amt.focus();
            return false;
        }
        if(trim(txt_to_amt.value) == ""  || txt_to_amt.value < 0)
        {
            alert("Please enter to amount.");
            txt_to_amt.select();
            txt_to_amt.focus();
            return false;
        }
        /*if(trim(fileimage.value) != "")
        {
            if(checkExt(trim(fileimage.value))==false)
            {
                fileimage.select();
                fileimage.focus();
                return false;
            }
        }
        if(cbo_visible.value == "")
        {
            alert("Please set value for visible.");
            cbo_visible.focus();			
            return false;
        }*/
        
    }
    return true;
}
// Delete confirmation message
function Confirm_Deletion()
{
    if(confirm("Are you sure you want to delete this image permanently."))
    {
        if(confirm("Confirm Deletion:Click 'OK' to delete this image or 'Cancel' to deletion."))
        {
            return true;
        }
    }
    return false;
}
