<?php
/*
File Name  :-item_edit_p.php
Create Date:- Jan-2019
Initially Create By:-014
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_file_system.php";
include "item_app_specific.php";
#--------------------------------------------------------------------------------------------------------------------------------
# get all the passed values
$str_desc="";
$int_pkid="";
$str_open="YES";
$str_title="";
//$str_visible="YES";
if(isset($_POST['txt_title']))
{ $str_title=trim($_POST['txt_title']); }
if(isset($_POST['ta_desc']))
{
    $str_desc=trim($_POST['ta_desc']);
}
if(isset($_POST['hdn_pkid']))
{
    $int_pkid=trim($_POST['hdn_pkid']);
}
/*$str_image_visible="YES";
if (isset($_POST["cbo_image_visible"]))
{
    $str_image_visible = trim($_POST["cbo_image_visible"]);
}*/
$int_displayorder=0;
if (isset($_POST["txt_displayorder"]))
{
    $int_displayorder = trim($_POST["txt_displayorder"]);
}
#------------------------------------------------------------------------------------------------------------
#check all validation
if($str_title=="")
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Duplicate Checking for Title field.
$str_query_select="SELECT title FROM ".$STR_DB_TABLE_NAME. " WHERE title= '" . ReplaceQuote($str_title) . "' AND pkid!=".$int_pkid;
$rs_list_check_duplicate=GetRecordSet($str_query_select);
if(!$rs_list_check_duplicate->eof())
{
    CloseConnection();
    Redirect("item_edit.php?msg=DU&type=E&pkid=".urlencode($int_pkid));
    exit();
}
#--------------------------------------------------------------------------------------------------------------------------------#update query to set values in table
$int_max_do=GetMaxValue("cm_home_feature","displayorder");
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME. " SET ";
$str_query_update .= "title='".ReplaceQuote($str_title)."'";
//$str_query_update .= "description='".ReplaceQuote($str_desc)."'";
//$str_query_update .= "displayorder=".$int_max_do." "; //has to change this
$str_query_update .= " WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#--------------------------------------------------------------------------------------------------------------------------------#Writing data of table into XML file
WriteXml();
#--------------------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=E&type=S");
exit();
?>
