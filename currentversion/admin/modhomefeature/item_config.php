<?php
$STR_TITLE_PAGE = "Home Page Feature List";
$UPLOAD_IMG_PATH = "../../mdm//";
$INT_IMG_WIDTH = 400;
$INT_IMG_HEIGHT = 200;

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "cm_home_feature"; 
$STR_DB_TABLE_NAME_ORDER_BY = "ORDER BY displayorder"; 

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "";

global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/home_feature.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlmodulefiles/home_feature_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";	
?>
