function frm_edit_validate()
{
    with(document.frm_edit)
    {
        if(trim(txt_title.value) == "")
        {
            alert("Please enter title.");
            txt_title.select();
            txt_title.focus();
            return false;
        }
    }
    return true;
}

function confirm_delete()
{
    if(confirm("Are you sure you want to delete this image?"))
    {
        if(confirm("Confirm Deletion: Click 'OK' to delete this image or click 'Cancel' to cancel deletion."))
        {
            return true;
        }
    }
    return false;
}

// JavaScript Document
