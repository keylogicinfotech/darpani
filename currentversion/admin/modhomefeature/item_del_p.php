<?php
/*
File Name  :- item_del_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#--------------------------------------------------------------------------------------------------------------------------------#Include files
include "../includes/validatesession.php";
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_file_system.php";
include "./cm_home_content_app_specific.php";

$int_pkid="";
$str_title="";
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#To check whether all values are passed properly or not.	
if(isset($_GET["pkid"]))
{
        $int_pkid=trim($_GET["pkid"]);
}
if($int_pkid=="")
{
        CloseConnection();
        Redirect("cm_home_content_edit.php?msg=F&type=E");
        exit();
}
if (!is_numeric($int_pkid))
{
        CloseConnection();
        Redirect("cm_home_content_edit.php?msg=F&type=E");
        exit();
}

$str_select_query="";
$str_select_query="SELECT photofilename FROM cm_home_content where pkid=".$int_pkid;
//print $str_select_query; 
$rs_visible=GetRecordset($str_select_query);
$str_large_image=$rs_visible->Fields("photofilename");
#---------------------------------------------------------------------------------------------------------------------
#Delete query to delete record from table.
$str_query_update = "";
$str_query_update="UPDATE cm_home_content SET photofilename='' WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------
#write xml file
WriteXML();			
#-----------------------------------------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_edit.php?msg=DEL&type=S&pkid=".urlencode($int_pkid)."&tit=".urlencode(RemoveQuote($str_highlight)));
exit();
?>
