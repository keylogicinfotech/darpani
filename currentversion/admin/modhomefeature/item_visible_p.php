<?php
/*
File Name  :- item_visible_p.php
Create Date:- Jan-2019
Initially Create By:-014
Update History:
*/
#-------------------------------------------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_app_specific.php";
#------------------------------------------------------------------------------
#Get values of all passed GET / POST variables				
$int_pkid="";
$str_mode="";

if(isset($_GET["pkid"]))
{
    $int_pkid=trim($_GET["pkid"]);
}
if(!is_numeric($int_pkid) || $int_pkid=="" || $int_pkid<0 )
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#potp");
    exit();
}
#---------------------------------------------------------------------------------------------------------
# Select Query
$str_query_select="SELECT title, visible FROM ".$STR_DB_TABLE_NAME. " WHERE pkid=".trim($int_pkid);
$rs_list=GetRecordSet($str_query_select);
if($rs_list->EOF()==true)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}	

$str_title=$rs_list->Fields("title");
$str_mode=$rs_list->Fields("visible");
#---------------------------------------------------------------------------------------------------------
#Set the value for variable to be stored in database.
if(strtoupper($str_mode)=="YES")
{
    $str_set_text="NO";
}
else
{
    $str_set_text="YES";
}
#---------------------------------------------------------------------------------------------------------
#Update query to update.	
$str_query_update="UPDATE ".$STR_DB_TABLE_NAME. " SET visible='" . $str_set_text . "' WHERE pkid=" . $int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------
#write xml file
WriteXML();	
#---------------------------------------------------------------------------------------------------------
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_list.php?msg=V&type=S&mode=".urlencode(RemoveQuote($str_set_text))."&#ptop");
exit();
?>
