<?php
/*
File Name  :-item_list.php
Create Date:- Jan-2019
Initially Create By:-014
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";	
include "item_config.php";
#------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME. " ORDER BY displayorder";
$rs_list = GetRecordSet($str_query_select); 

$str_query_select = "";
$str_query_select = "SELECT COUNT(*) totalrec FROM ".$STR_DB_TABLE_NAME. "";
$rs_list_total = GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_visible_value="";
if(isset($_GET["mode"]))
{
    if($_GET["mode"] == "YES") { $str_visible_value = "Visible"; }
    elseif($_GET["mode"] == "NO") { $str_visible_value = "Invisible"; }
}
	
#Get message type.
$str_type = "";
$str_message = "";	
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Display message.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_visible_value; break;	
	case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;	
	case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
	case("E"): $str_message = $STR_MSG_ACTION_EDIT; break;
	case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
	case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
	case("DU"): $str_message = $STR_MSG_ACTION_TITLE_ALREADY_EXIST; break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<body>
<!-- Page Content -->
<a name="ptop"></a>
<div class="container">
<?php include "../../includes/adminheader.php"; ?>
	<div class="row padding-10">
            <div class="col-md-6 col-sm-6 col-xs-12 button_space">
                <div class="btn-group" role="group" aria-label="..."></div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE);?></h3></div>
	</div>
        <hr>
	<?php if($str_type != "" && $str_message != "") { print(DisplayMessage("99%",$str_message,$str_type)); } ?>
	<div class="row padding-10">
            <div class="col-md-12">
        	<div class="panel-group" id="accordion">
                    <div class="panel panel-default">
			<div class="panel-heading">
                            <h4 class="panel-title">
                                <span>
                                    <a class="accordion-toggle collapsed link" data-toggle="collapse" title="<?php print($STR_HOVER_ADD); ?>" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><i class="fa  fa-plus "></i>&nbsp;<b><?php print($STR_HOVER_ADD); ?></b></a>
                                </span>
                                <?php print($STR_LINK_HELP); ?>
                            </h4>
			</div>
                            <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">
                                    <form name="frm_add" action="item_add_p.php" method="post" onSubmit="return frm_add_validate();" enctype="multipart/form-data" role="form">
                                         <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                            <div class="form-group">
                                                <label>Title</label><span class="text-help"> </span>
                                                <input id="txt_title" name="txt_title" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_TITLE ?>" type="text" >
                                            </div>
                                            <?php /* ?><div class="form-group">
                                                    <label>Description</label><span class="text-help"></span>
                                                            <textarea name="ta_desc" id="ta_desc" cols="100" rows="20" class="form-control input-sm" placeholder="Enter description here" ></textarea><span class="text-help"><?php //print($STR_MAX_255_MSG);?></span>
                                            </div><?php */ ?>

                                            <div class="form-group">
                                                    <label>Visible</label>&nbsp;<span class="text-help-form">(<?php print($STR_MSG_VISIBLE)?>)</span><br/>
                                                    <select name="cbo_window" class="form-control input-sm" >
                                                    <option value="YES">YES</option><option value="NO">NO</option></select>
                                            </div>
                                            <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                            <input type="hidden" name="hdn_pkid" value="<?php print($rs_list->fields("pkid")); ?>" >
                                            <input type="hidden" name="txt_header" class="clsTextBox" maxlength="100" value="">									
     					</form>
                                    </div>
				</div>
                            </div>
			</div>
                    </div>
                </div>
            	<div class="table-responsive">
                    <table class="table table-striped table-bordered ">
			<form name="frm_list" action="item_order_p.php" method="post" onSubmit="return frm_list_check_displayorder();">
                            <thead>
				<tr>
                                    <th width="4%">Sr. #</th>
                                    <th>Details</th>
                                    <th width="5%">Display Order</th>
                                    <th width="6%">Action</th>
				</tr>
                            </thead>
                            <tbody>		
                                <?php if($rs_list->EOF()==true) { ?>
                                <tr><td colspan="7" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                                <?php } else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
                                <tr>
                                    <td align="center"><?php print($int_cnt)?></td>
                                    <td style="vertical-align:top">
                                        <?php if($rs_list->fields("title") != "")  {?>
                                        <h4><b class=""><?php print(RemoveQuote($rs_list->fields("title"))); ?></b><?php } ?>
                                        <?php
                                            if($rs_list->fields("pkid") == 13)
                                            {
                                                $str_query_select = "";
                                                $str_query_select = "SELECT noofview FROM cm_home_keyword";
                                                $rs_list_visitor = GetRecordSet($str_query_select);
                                                $int_overall_records = $rs_list_visitor->fields("noofview");
                                                ?>
                                                <b class="text-success">(US$&nbsp;<?php print(trim($int_overall_records));?>)</b>
                                        <?php  }
                                            if($rs_list->fields("pkid") == 14)
                                            { 
                                                $int_total_brag_card = 0;
                                                $str_query_select="";
                                                $str_query_select="SELECT COUNT(purchasepkid) AS total_record FROM t_product_purchase ";
//                                                print $str_query_select;exit;
                                                $rs_list_purchase=GetRecordSet($str_query_select);
                                                $int_total_brag_card = $rs_list_purchase->fields("total_record");
//                                                $int_total_brag_cards = number_format($rs_list_purchase->fields("total_record") * 1, 2);
//                                                $int_brag_card_sent = $rs_list->fields("value");
                                            ?>
                                                <b class="text-success">(<?php print $int_total_brag_card; ?>)</b>
                                        <?php  }
                                        if($rs_list->fields("pkid") == 15)
                                            { 
                                                $int_total_people = 0;
                                                $str_query_select="";
                                                $str_query_select="SELECT COUNT(pkid) AS total_people FROM t_user ";
                                                $rs_list_user=GetRecordSet($str_query_select);
                                                $int_total_people = $rs_list_user->fields("total_people");
                                            ?>
                                            <b class="text-success">(<?php print $int_total_people; ?>)</b></h4>
                                        <?php  }
                                            ?>
                                        <?php if($rs_list->fields("description") != "")  {?><p align="justify" <?php /*?>class="readmore"<?php */?>><?php print(RemoveQuote($rs_list->fields("description")));?></p><?php } ?>
                                        
                                    </td>
                                    <td align="center">
                                        <input type="text" name="txt_displayorder<?php print($int_cnt);?>" value="<?php print($rs_list->fields("displayorder"));?>" class="form-control input-sm text-center"><input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>">
                                    </td>
                                    <?php $str_image="";
                                    if(strtoupper($rs_list->fields("visible"))=="YES")
                                    {	$str_image="<i class='fa fa-eye'></i>";
                                        $str_class="btn btn-warning btn-xs";
                                        $str_title=$STR_HOVER_VISIBLE;
                                    }
                                    else
                                    {	$str_image="<i class='fa fa-eye-slash'></i>";
                                        $str_class="btn btn-default active btn-xs";
                                        $str_title=$STR_HOVER_INVISIBLE;
                                    } ?>	
                                    <td align="center" >
                                        <a href="item_visible_p.php?pkid=<?php print($rs_list->fields("pkid"))?>" class="<?php print($str_class);?>" title="<?php print($str_title); ?>"><?php print($str_image)?></a>
                                        <a href="item_edit.php?pkid=<?php print($rs_list->fields("pkid"))?>" class="btn btn-success btn-xs " title="<?php print($STR_HOVER_EDIT)?>"><i class="fa fa-pencil"></i></a>
                                        <?php /* ?><a href="item_del_p.php?pkid=<?php print($rs_list->fields("pkid"))?>" onClick="return confirm_delete() ;" class="btn btn-danger btn-xs" title="<?php print($STR_BUTTON_TITLE_FORM_DELETE_ICON)?>"><i class="fa fa-trash"></i></a><?php */ ?>
                                    </td>
                                    </tr>
          				<?php $int_cnt++; $rs_list->MoveNext(); } ?>
					<input type="hidden" name="hdn_counter" value="<?php print($int_cnt)?>">
          				<tr class="<?php print($int_cnt%2); ?>">
                                            <td></td>
                                            <td></td>
                                            <td align="center" valign="middle"><?php print DisplayFormButton("SAVE",0); ?></td>
          					<td align="center"></td>
					</tr>
						<?php } ?>	
					</tbody>
				</form>
			</table>
		</div>
	
	<?php include "../../includes/help_for_list.php"; ?>
    <!-- /.container -->											
</div>

<script language="JavaScript" src="item_list.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>
</body></html>
