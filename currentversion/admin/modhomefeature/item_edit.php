<?php
/*
File Name  :-item_edit.php
Create Date:- Jan-2019
Initially Create By:-014
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";	
include "item_config.php";
#------------------------------------------------------------------------------
#Select query to get records from table
$int_pkid="";
if(isset($_GET['pkid']))
{
    $int_pkid=trim($_GET['pkid']);
}

$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
$rs_list = GetRecordSet($str_query_select); 
	
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Diplay message.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("E"): $str_message = $STR_MSG_ACTION_UPDATE; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("DU"): $str_message = "Title already exist. please try with another title."; break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_EDIT); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<!-- Page Content -->
<a name="ptop"></a>
<div class="container">
<?php include "../../includes/adminheader.php"; ?>
	<div class="row padding-10">
            <div class="col-md-3 col-sm-6 col-xs-12 button_space">
		<div class="btn-group" role="group" aria-label="...">
                    <?php /*?><a href="#help"  class="btn btn-info " title="Click to view help"><i class="fa fa-info-sign  "></i>&nbsp; Help</a> <?php */?>
                        <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Back to State List"><i class="fa fa-chevron-left"></i>&nbsp;Back</a><?php */?>
                    <a href="item_list.php" title="<?php print($STR_TITLE_GO_TO); ?> <?php print($STR_TITLE_PAGE); ?>" class="btn btn-default"><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>  
                </div>
            </div>
        <div class="col-md-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE); ?></h3></div>
	</div><hr>
	<?php	if($str_type != "" && $str_message != "") { print(DisplayMessage("99%",$str_message,$str_type)); } ?>
	<div class="row padding-10">
            <div class="col-md-12">
		<div class="panel-group" id="accordion">
                    <div class="panel panel-default">
			<div class="panel-heading">
                            <h4 class="panel-title">
                                <i class="fa  fa-edit "></i>&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b>
                                <?php print($STR_LINK_HELP); ?>	
                            </h4>
			</div>
                            <div id="collapse01" class="panel-collapse">
                                <div class="panel-body">
                                    <form name="frm_edit" action="item_edit_p.php" method="post" onSubmit="return frm_edit_validate();" enctype="multipart/form-data" role="form">
                                        <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                                <div class="form-group">
                                                    <label>Title</label><span class="text-help"></span>
                                                        <input id="txt_title" name="txt_title" class="form-control input-sm" value="<?php print(RemoveQuote($rs_list->fields("title"))); ?>" placeholder="<?php print $STR_PLACEHOLDER_TITLE ?>" type="text" >
                                                </div>
                                                <?php /* ?><div class="form-group">
                                                        <label>Description</label><span class="text-help"></span>
                                                                <textarea name="ta_desc" id="ta_desc" rows="10" class="form-control input-sm" placeholder="Enter description here" ><?php print(RemoveQuote($rs_list->fields("description"))); ?></textarea><span class="text-help"><?php //print($STR_MAX_255_MSG);?></span>	
                                                </div><?php */ ?>

                                                <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                                <input type="hidden" name="hdn_pkid" value="<?php print($rs_list->fields("pkid")); ?>" >
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	<?php include "../../includes/help_for_edit.php"; ?>
    <!-- /.container -->											
</div>

<script language="JavaScript" src="item_edit.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>
</body></html>
