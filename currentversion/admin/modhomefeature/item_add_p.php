<?php
/*
File Name  :-item_add_p.php
Create Date:- Jan-2019
Initially Create By:-014
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_file_system.php";
include "item_app_specific.php";
#--------------------------------------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$str_desc="";
$int_pkid="";
$str_open="YES";
$str_title="";
$str_visible="YES";
if(isset($_POST['txt_title']))
{ 
    $str_title=trim($_POST['txt_title']); 
}
//if(isset($_POST['ta_desc']))
//{
//        $str_desc=trim($_POST['ta_desc']);
//}
if(isset($_POST['hdn_pkid']))
{
    $int_pkid=trim($_POST['hdn_pkid']);
}
/*$str_image_visible="YES";
if (isset($_POST["cbo_image_visible"]))
{
    $str_image_visible = trim($_POST["cbo_image_visible"]);
}*/
if(isset($_POST['cbo_window']))
{
    $str_visible=trim($_POST['cbo_window']);
}
#------------------------------------------------------------------------------------------------------------
#Prepare query string to pass variables back
$str_redirect = "";
$str_redirect = "&title=".urlencode(RemoveQuote($str_title))."&desc=".urlencode(RemoveQuote($str_desc))."";
$str_redirect=$str_redirect."&visible=".RemoveQuote(urlencode($str_visible));
#check all validation
if($str_title=="")
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
#--------------------------------------------------------------------------------------------------------------------------------#insert query 
$int_max_do=GetMaxValue("cm_home_feature","displayorder");
$str_query_insert="";
$str_query_insert="INSERT INTO ".$STR_DB_TABLE_NAME. " (title,visible,displayorder) VALUES(";
$str_query_insert=$str_query_insert."'".ReplaceQuote($str_title)."',";
//$str_query_insert=$str_query_insert."'".ReplaceQuote($str_desc)."',";
//$str_query_insert=$str_query_insert."'".ReplaceQuote($str_thumb_file_name)."',";
$str_query_insert=$str_query_insert."'".ReplaceQuote($str_visible)."','".$int_max_do."')";
//print $str_query_insert;exit;
ExecuteQuery($str_query_insert);
#--------------------------------------------------------------------------------------------------------------------------------#Writing data of table into XML file
WriteXml();
#--------------------------------------------------------------------------------------------------------------------------------
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_list.php?msg=S&type=S");
exit();
?>
