<?php
/*
Module Name:- Login
File Name  :- admin_home.php
Create Date:- 29-SEP-2005
Intially Create By :- 0022
Update History:
*/
##--------------------------------------------------------------------------------------------------
 	include "../includes/validatesessionroot.php";
	include "../includes/configuration.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
	
	
	if(strtoupper($_SESSION["superadmin"])=="NO")
	{
		if(isset($_SESSION["adminname"]))
		{
			if ($_SESSION["adminname"] != "")
			{
				$str_admin=$_SESSION["adminname"];
			}
		}	 

		$str_admin_select="select pkid from t_siteadmin where username='".$str_admin."'";
		$rs_admin=GetRecordSet($str_admin_select);
		
		$str_query_select="SELECT a.openinnewwindow as mwin,b.openinnewwindow as swin,a.position as modpos, a.moduletitle AS modname,a.url modurl, a.tooltip modtip, a.instructiontext modinstruction,";
		$str_query_select .= " b.moduletitle AS subname,b.url suburl,b.tooltip subtip,b.instructiontext subinstruction";
		$str_query_select .= " FROM t_module AS a, t_module AS b, tr_site_module AS c";
		$str_query_select .= " WHERE a.modulepkid = b.moduleparentpkid AND b.modulepkid = c.modulepkid AND a.moduleparentpkid = 0 AND pkid =".$rs_admin->fields("pkid")." AND a.visible='YES' AND b.visible='YES'";
		$str_query_select .= " ORDER BY a.position,a.moduleparentpkid, a.displayorder,a.modulepkid, b.displayorder";

/*  		$str_query_select="SELECT a.modulepkid modid, a.moduleparentpkid modparentid, a.moduletitle modname, a.position modpos, a.url modurl, a.tooltip modtip, a.instructiontext modinstruction";
		$str_query_select .= " FROM tr_site_module c, t_module a";
		$str_query_select .= " WHERE c.modulepkid = a.modulepkid AND pkid =".$rs_admin->fields("pkid");
		$str_query_select .= " ORDER  BY a.position, a.modulepkid, a.displayorder ";		
		# Select Query to display list
  		 $str_query_select = "select a.modulepkid modid,a.moduleparentpkid modparentid,a.moduletitle modname,a.position modpos,";
		$str_query_select .= "a.url modurl,a.tooltip modtip,a.instructiontext modinstruction,";
		$str_query_select .= "b.modulepkid subid,b.moduletitle subname,b.position subpos,";
		$str_query_select .= "b.url suburl,b.tooltip subtip,b.instructiontext subinstruction,c.modulepkid";
		$str_query_select .= " from tr_site_module c left join t_module a on c.modulepkid=a.modulepkid and pkid=".$rs_admin->fields("pkid");
		$str_query_select .= " left join t_module b";
		$str_query_select .= " on a.modulepkid=b.moduleparentpkid" ;
		$str_query_select .= " where a.moduleparentpkid=0";
		$str_query_select .= " order by a.position,a.moduleparentpkid,a.displayorder,a.modulepkid,b.displayorder,b.moduletitle";
   		   print($str_query_select);
		exit();
 */		$rs_list=GetRecordSet($str_query_select);
	}
	elseif(strtoupper($_SESSION["superadmin"])=="YES")
	{
		# Select Query to display list
		$str_query_select="SELECT a.openinnewwindow as mwin,b.openinnewwindow as swin,a.position as modpos, a.moduletitle AS modname,a.url modurl, a.tooltip modtip, a.instructiontext modinstruction,";
		$str_query_select .= " b.moduletitle AS subname,b.url suburl,b.tooltip subtip,b.instructiontext subinstruction";
		$str_query_select .= " FROM t_module AS a, t_module AS b";
		$str_query_select .= " WHERE a.modulepkid = b.moduleparentpkid AND a.moduleparentpkid = 0 AND a.visible='YES' AND b.visible='YES'";
		$str_query_select .= " ORDER BY a.position,a.moduleparentpkid, a.displayorder,a.modulepkid, b.displayorder";
		$rs_list=GetRecordSet($str_query_select);
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : Admin Main Menu</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="shortcut icon" href="./../favicon.ico">
</head>
<body>
<div class="container">
  <?php include("../includes/header_admin.php"); ?>
	<div class="row padding-10">
		<h3 align="center" ><b class=""><i class="fa fa-home"></i>&nbsp;Main Menu</b></h3><hr/>
		<div class="col-md-4 col-sm-6 col-xs-12">
                      <?php if(!$rs_list->EOF()) { 
                                while(!$rs_list->EOF() && $rs_list->fields("modpos")=="LEFT") 
                                { 
                                    $str_main_title=$rs_list->fields("modname"); ?>
                                    <div class="row padding-10">
                                            <div class="col-md-12">
                                                    <div class="panel panel-default">
                                                            <div class="panel-heading"><b><?php print(DisplayWebSiteurl($rs_list->fields("modurl"),$rs_list->fields("modname"),$rs_list->fields("mwin"),$rs_list->fields("modtip"),'','',''));?></b></div>
                                                            <div class="panel-body">
                                                                    <?php while($str_main_title==$rs_list->fields("modname")) 
                                                                            {
                                                                                    print(DisplayWebSiteurl($rs_list->fields("suburl"),$rs_list->fields("subname"),$rs_list->fields("swin"),$rs_list->fields("subtip"),'link','',''));
                                                                                    if($rs_list->fields("subinstruction")!="") { print($rs_list->fields("subinstruction")); }
                                                                                    print "<br/>";
                                                                                    $rs_list->MoveNext(); 
                                                                            } ?>
                                                                
                                                                <?php 
                                                               /* $str_sel_active_module = "";
                                                                $str_sel_active_module = "SELECT * FROM t_active_module";
                                                                $rs_active_module = GetRecordSet($str_sel_active_module);
                                                                
                                                                while(!$rs_active_module->eof()) 
                                                                {
                                                                    if(strtoupper($rs_active_module->fields("active")) == "YES") 
                                                                    {
                                                                        //print (DisplayWebSiteurl('https://test.com','Activated: '.$rs_active_module->fields("moduletitle"),'','','link','','')); 
                                                                        print "Activated: ".$rs_active_module->fields("moduletitle");
                                                                        ?><br>
                                                                    
                                                                <?php
                                                                    }
                                                                    else
                                                                    {
                                                                        print (DisplayWebSiteurl('./admin_module_active_p.php?pkid='.$rs_active_module->fields("pkid"),'Click to Active: '.$rs_active_module->fields("moduletitle"),'','','link','',''))."<br>"; 
                                                                    }
                                                                $rs_active_module->MoveNext();
                                                                }*/ ?>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>
			<?php   } 
                            } ?>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<?php if(!$rs_list->EOF()) { while(!$rs_list->EOF() && $rs_list->fields("modpos")=="MIDDLE") { $str_main_title=$rs_list->fields("modname"); ?>
			<div class="row padding-10">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><b><?php print(DisplayWebSiteurl($rs_list->fields("modurl"),$rs_list->fields("modname"),$rs_list->fields("mwin"),$rs_list->fields("modtip"),'','',''));?></b></div>
						<div class="panel-body">
							<?php while($str_main_title==$rs_list->fields("modname")) {
								print(DisplayWebSiteurl($rs_list->fields("suburl"),$rs_list->fields("subname"),$rs_list->fields("swin"),$rs_list->fields("subtip"),'link','',''));
								if($rs_list->fields("subinstruction")!="") { print($rs_list->fields("subinstruction")); }
								print "<br/>";
								$rs_list->MoveNext(); } ?>	
						</div>
					</div>
				</div>
			</div>
				<?php } } ?>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="row padding-10">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading heading_bold"><b>Site Administrator Management</b></div>
							<div class="panel-body">
								<?php /*?><?php
								if(isset($_SESSION['superadmin'])) {
					 				if (strtoupper($_SESSION['superadmin'])=="YES") { ?>
									<?php } } ?><?php */?>
								<a href="./modsiteadmin/item_list.php" class="link"  title="Click to view site admin details">Manage Site Admin</a><br/>
							</div>
						</div>
					</div>
				</div>
			<?php if(!$rs_list->EOF()) { while(!$rs_list->EOF() && $rs_list->fields("modpos")=="RIGHT") { $str_main_title=$rs_list->fields("modname"); ?>
			<div class="row padding-10">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><b><?php print(DisplayWebSiteurl($rs_list->fields("modurl"),$rs_list->fields("modname"),$rs_list->fields("mwin"),$rs_list->fields("modtip"),'','',''));?></b></div>
						<div class="panel-body">
							<?php while($str_main_title==$rs_list->fields("modname")) {
								print(DisplayWebSiteurl($rs_list->fields("suburl"),$rs_list->fields("subname"),$rs_list->fields("swin"),$rs_list->fields("subtip"),'link','',''));
								if($rs_list->fields("subinstruction")!="") { print($rs_list->fields("subinstruction")); }
								print "<br/>";
								$rs_list->MoveNext(); } ?>	
						</div>
					</div>
				</div>
			</div>
			<?php } } ?>
			<?php if($APP_RUN_MODE=="LOCAL") { ?>
			<div class="row padding-10">
                                <div class="col-md-12">
                                        <div class="panel panel-default">
                                                <div class="panel-heading heading_bold"><b>Module Management</b></div>
                                                <div class="panel-body">
                                                        <?php /*?><?php
                                                        if(isset($_SESSION['superadmin'])) {
                                                                if (strtoupper($_SESSION['superadmin'])=="YES") { ?>
                                                                <?php } } ?><?php */?>
                                                        <a href="./admmodule/adm_module_login.php" class="link"  title="Click to view module management">Manage Modules</a><br/>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <?php }  ?>
		</div>
	</div>
	<hr/>
	</div>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/admin.css" rel="stylesheet" type="text/css">
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<?php /*?><script language="JavaScript" src="./includes/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="./admin_home.js" type="text/javascript"></script><?php */?>
<?php //include "../includes/include_files_admin.php"; ?>
<?php include("../includes/footer_admin.php"); ?>
</body>
</html>
