<?php
/*
Module Name:- Content Management Module for Home page keywords
File Name  :-cm_banner_code_p.php
Create Date:- 27-DEC-2006
Intially Create By :-
Update History:
*/
#------------------------------------------------------------------------------
# Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_xml.php";
#----------------------------------------------------------------------------------------------------------------------------------------
# get all the passed values
$int_pkid = "";
$str_title = "";
$str_metakeyword = "";
$str_metadesc = "";

$str_foot_desc = "";
$str_head_banner_code = "";
$str_foot_top_banner_code = "";
$str_foot_banner_code = "";
$int_noofview = 0;
$str_noofview_visible = "NO";
//print_r ($_POST); exit;
if(isset($_POST['ta_title']))
{
    $str_title = trim($_POST['ta_title']);
}
if(isset($_POST['ta_meta_keyword']))
{
    $str_metakeyword = trim($_POST['ta_meta_keyword']);
}
if(isset($_POST['ta_desc']))
{
    $str_metadesc = trim($_POST['ta_desc']);
}	
if(isset($_POST['ta_desc1']))
{
    $str_foot_desc = trim($_POST['ta_desc1']);
}
if(isset($_POST['ta_desc3']))
{
    $str_head_banner_code = trim($_POST['ta_desc3']);
}
if(isset($_POST['ta_desc4']))
{
    $str_foot_top_banner_code = trim($_POST['ta_desc4']);
}
if(isset($_POST['ta_desc2']))
{
    $str_foot_banner_code = trim($_POST['ta_desc2']);
}	
if(isset($_POST['txt_view']))
{
    $int_noofview = trim($_POST['txt_view']);
}
if(isset($_POST['cbo_visible']))
{
    $str_noofview_visible = trim($_POST['cbo_visible']);
}
if(isset($_POST['hdn_pkid']))
{
    $int_pkid = trim($_POST['hdn_pkid']);
}
#----------------------------------------------------------------------------------------------------------------------------------------
#update query to set values in home table
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME. " SET description='".ReplaceQuote($str_foot_desc)."', ";
$str_query_update = $str_query_update ."bannercode='".ReplaceQuote($str_foot_banner_code)."', ";
$str_query_update = $str_query_update . "topbannercode='".ReplaceQuote($str_foot_top_banner_code)."', ";
$str_query_update = $str_query_update . "noofview='".ReplaceQuote($int_noofview)."', ";
$str_query_update = $str_query_update . "noofview_visible='".ReplaceQuote($str_noofview_visible)."' ";
$str_query_update = $str_query_update . " WHERE pkid=".$int_pkid;
//print($str_query_update); exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------------------------------------------	
# Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME. " ";
$rs_list = GetRecordSet($str_query_select);

# Generating XML File
if(!$rs_list->eof())
{
    $arr_title=array();
    $arr1_title=array();
    $i=0;
    while(!$rs_list->eof())
    {

        $arr_title[$i]=$rs_list->fields("itemkey")."_description";
        $arr1_title[$i]=$rs_list->fields("description");
        $i=$i+1;

        $arr_title[$i]=$rs_list->fields("itemkey")."_bannercode";
        $arr1_title[$i]=$rs_list->fields("bannercode");
        $i=$i+1;

        $arr_title[$i]=$rs_list->fields("itemkey")."_topbannercode";
        $arr1_title[$i]=$rs_list->fields("topbannercode");
        $i=$i+1;

        $arr_title[$i]=$rs_list->fields("itemkey")."_noofview";
        $arr1_title[$i]=$rs_list->fields("noofview");
        $i=$i+1;

        $arr_title[$i]=$rs_list->fields("itemkey")."_noofview_visible";
        $arr1_title[$i]=$rs_list->fields("noofview_visible");
        $i=$i+1;

        $rs_list->MoveNext();
    }		
    #writting data to the file
    writeXmlFile($XML_FILE_PATH_CMS,$XML_ROOT_TAG_CMS,$arr_title,$arr1_title);
}	

#----------------------------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=E&type=S&#ptop");
exit();
?>
