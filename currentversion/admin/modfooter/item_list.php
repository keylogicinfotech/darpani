<?php
/*	
File Name  :-item_list.php
Create Date:- Jan-2019
Intially Create By :- 
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files


include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
#------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME. "";
$rs_list = GetRecordSet($str_query_select);

$str_visible = "";
$str_visible = $rs_list->fields("noofview_visible");
#------------------------------------------------------------------------------------------
# Initialization of GET / POST variables.   
$str_type = "";
$str_message = "";	
# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#	Diplay message.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("E"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("DEL"): $str_message = $STR_MSG_ACTION_DELETE_IMAGE; break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE_BANNER_FOOTER); ?> : <?php print($STR_TITLE_EDIT); ?> </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body onLoad="set_counter();">
<!-- Page Content -->
<a name="ptop"></a>
<div class="container">
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 button_space">
            <div class="btn-group" role="group" aria-label="...">
            <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Back to State List"><i class="fa fa-chevron-left"></i>&nbsp;Back</a><?php */?>
            </div>
        </div>
        <div class="col-md-9" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE_BANNER_FOOTER); ?></h3></div>
    </div><hr>
    <?php if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); } ?>
    <div class="row padding-10">
    	<div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <b><i class="fa  fa-edit "></i>&nbsp;&nbsp;<?php print($STR_TITLE_EDIT); ?></b>
                            <?php print($STR_LINK_HELP); ?>
			</h4>
                    </div>
                    <div id="collapse01" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <form name="frm_edit" action="item_list_p.php" method="post" onSubmit="return join_vaidate();" enctype="multipart/form-data" role="form">
                                <?php if($_SESSION['superadmin'] == 'YES') { ?>	
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="form-group">
                                        <label>Footer Description</label>&nbsp;<span class="text-help-form"><?php print($STR_MSG_ADMIN); ?></span>
                                        
                                         
                                         
                                          <textarea id="ta_desc1" name="ta_desc1" type="text" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>" ><?php print(RemoveQuote($rs_list->fields("description"))); ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Footer Description 2</label>&nbsp;<span class="text-help-form"><?php print($STR_MSG_ADMIN); ?></span>
                                            <textarea id="ta_desc2" name="ta_desc2"  type="text" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>" ><?php print(RemoveQuote($rs_list->fields("bannercode"))); ?></textarea>
                                        </div>
                                        <?php /* ?><div class="form-group">
                                                <label>Google Analytics Code </label>&nbsp;<span class="text-help"></span>
                                                        <textarea name="ta_desc3" rows="10" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>" ><?php print(RemoveQuote($rs_list->fields("headerbannercode"))); ?></textarea>
                                        </div><?php */ ?>
                                        <?php /* ?><div class="row padding-10">
                                            <div class="col-md-6 col-xs-6">
                                                <div class="form-group">
                                                    <label> View Counter</label>&nbsp;<span class="text-help-form"><?php print($STR_MSG_ADMIN); ?></span>
                                                        <input type="text" name="txt_view" placeholder="<?php print $STR_PLACEHOLDER_INT; ?>" class="form-control input-sm" value="<?php print($rs_list->fields("noofview"));?>">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-6">
                                                <div class="form-group">
                                                    <label>Counter Visible</label>&nbsp;<span class="text-help-form"><?php print($STR_MSG_ADMIN); ?></span>
                                                        <select name="cbo_visible" class="form-control input-sm" >
                                                            <option value="YES" <?php print(CheckSelected("YES",strtoupper($str_visible)));?>>YES</option>
                                                            <option value="NO" <?php print(CheckSelected("NO",strtoupper($str_visible)));?>>NO</option>
                                                        </select>
                                                </div>
                                            </div>
                                        </div><?php */ ?>
                                <?php } else { ?>
                                    <?php /* ?>    <input type="hidden" name="ta_desc" id="ta_desc" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("description")))); ?>" >
                                        <input type="hidden" name="ta_desc2" id="ta_desc2" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("footerbannercode")))); ?>" >
                                        <?php /*?><input type="hidden" name="ta_desc4" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("topbannercode")))); ?>" >	<?php */?>
                                   <?php /* ?>     <input type="hidden" name="ta_desc3" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("headerbannercode")))); ?>" >
                                        <input type="hidden" name="txt_view" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("noofview")))); ?>" >
                                        <input type="hidden" name="cbo_visible" value="<?php print($str_visible); ?>" ><?php */ ?>
                                <?php } ?>
                                        <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                        <input type="hidden" name="hdn_pkid" value="<?php print($rs_list->fields("pkid")); ?>" >
                                        <input type="hidden" name="ta_title" value="" >
                                        <input type="hidden" name="ta_meta_keyword" value="" >
                                        <input type="hidden" name="ta_desc" value="" >
     							</form>
            				</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<?php include "../../includes/help_for_edit.php"; ?>
    <!-- /.container -->
</div>

<?php include($STR_ADMIN_FOOTER_PATH); ?>

<?php include "../../includes/include_files_admin.php"; ?>
<script type="text/javascript" src="../../js/richetxteditor.js"></script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_desc1');
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_desc2');
		//new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
  });
		// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
		// convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_desc1');
             new nicEditor({fullPanel : true}).panelInstance('ta_desc2');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>

</body>
</html>
