<?php
$STR_TITLE_PAGE = "Footer Page Banner List";
$UPLOAD_IMG_PATH = "../../mdm//";
$INT_IMG_WIDTH = 0;
$INT_IMG_HEIGHT = 0;

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "cms_footer"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY displayorder DESC, title ASC "; 

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "";

global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/footer.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/footer_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";	

$STR_MSG_ACTION_DISPLAY_ON_HOME="'Display In Home Page Panel' settings updated successfully for selected records.";
$STR_MSG_ACTION_DISPLAY_ON_TOP="'Display On Top of Home Page' settings updated successfully for selected records.";
$STR_ADD_EDIT_MSG = "You can enter maximum one detail here. Please edit details by clicking on edit icon OR just delete existing one to upload completely new detail.";
?>
