function showdetails(url)
{
    window.open(url,'latestupdates','left=50,top=20,resizable=yes,scrollbars=yes,width=570,height=490');
    return false;
}

function confirm_delete()
{
    if(confirm("Are you sure you want to delete this image permanently.?"))
    {
        if(confirm("Confirm Deletion:Click 'OK' to delete this details or 'Cancel' to deletion."))
        {
                return true;
        }
    }
    return false;
}

function frm_edit_validate()
{
    with(document.frm_edit)
    {
        if(isDate('mdy',cbo_month.value + "/" + cbo_day.value + "/" + cbo_year.value)==false)
        {
            alert("Please enter valid date.");
            return false;
        }

        if(trim(cbo_news_type.value)=="" || trim(cbo_news_type.value)==0)
        {
            alert("Please select type for news.");
            cbo_news_type.focus();	
            return false;
        }

        if(isEmpty(txt_heading.value))
        {
            alert("Please enter latest updates heading.");
            txt_heading.focus();
            return false;
        }

        if(trim(txt_newsurl.value)!="")
        {
            if(!isValidUrl(txt_newsurl.value.toLowerCase()))
            {
                txt_newsurl.select();
                txt_newsurl.focus();
                return false;			
            }
        }

        if(file_news_image.value!="")
        {
            if(checkExt(file_news_image.value)==false)
            {
                file_news_image.select();
                file_news_image.focus();
                return false;
            }
        }

        if (isEmpty(ta_desc.value))
        {
            alert("Please enter latest updates description.");
            ta_desc.select();
            ta_desc.focus();
            return false;
        }
    }
    return true;
}