<?php
$STR_TITLE_PAGE = "News / Announcement List";
$UPLOAD_IMG_PATH = "../../mdm/news/";
$INT_IMG_WIDTH = "900";
$INT_IMG_WIDTH_THUMB = "300";

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = " t_news"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY typefornews desc,date desc,heading";

global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/news.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

$str_user1 = "Regular User";
$str_user2 = "Wholesaler";

$STR_TITLE_RESTORE_DETAILS="Click to restore this Latest Updates detail";
# Error Messages will be displayed on page
$STR_MSG_NEWS_CATAGORY = "Error... Please select category for whom this news is.";
//    $STR_MSG_NEWS_CATAGORY = "ERROR !!! Invalid date format."
?>