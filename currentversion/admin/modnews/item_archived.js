function showdetails(url)
{
    window.open(url,'News','left=50,top=20,resizable=yes,scrollbars=yes,width=570,height=490');
    return false;
}

function restore_confirm()
{
    if(confirm("Are you sure you want to restore this details?"))
    {
        if(confirm("Click 'Ok' to restore this details or click 'Cancel' to cancel."))
        {
            return true;
        }
        return false;
    }
    return false;
}

function confirm_delete()
{
    if(confirm("Are you sure you want delete this details?"))
    {
        if(confirm("Click 'Ok' to delete this details or click 'Cancel' to cancel."))
        {
            return true;
        }
        return false;
    }
    return false;
}
