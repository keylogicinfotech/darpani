<?php
/*
File Name  :- item_order_p.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#-------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
//include "../../includes/lib_file_system.php";	
include "./item_app_specific.php";
#-------------------------------------------------------------------------------------
#get Query String Data
$int_cnt="";
if (isset($_POST["hdn_counter"]))
{
    $int_cnt=trim($_POST["hdn_counter"]);
}	
if($int_cnt=="" || $int_cnt<0 || !is_numeric($int_cnt))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#update display order
for($i=1;$i<$int_cnt;$i++)
{
    if(trim($_POST["txt_displayorder".$i]) != "" && trim($_POST["txt_displayorder".$i]) >=0 && is_numeric(trim($_POST["txt_displayorder".$i]))==true  && trim($_POST["hdn_pkid".$i])!="" && trim($_POST["hdn_pkid".$i])>0 && is_numeric(trim($_POST["hdn_pkid".$i]))==true)
    {
        $str_query_update="UPDATE " .$STR_DB_TABLE_NAME. " SET displayorder='".trim($_POST["txt_displayorder".$i])."' WHERE pkid=".trim($_POST["hdn_pkid".$i]);
        //print $str_query_update; exit;
        ExecuteQuery($str_query_update);
    }
}
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to page	
CloseConnection();
Redirect("item_list.php?type=S&msg=O&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------
?>
