<?php
/*
File Name  :- item_del_img_p.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";	
include "./item_app_specific.php";
#------------------------------------------------------------------------------------------------
#get Query String Data
$int_pkid="";
if(isset($_GET["pkid"])==true)
    $int_pkid=trim($_GET["pkid"]);
	
#To check required parameters are passed properly or not
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#select query
$str_query_select="SELECT imagefilename,imagefilenamelarge FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=". $int_pkid;
$rs_list=GetRecordSet($str_query_select);

if($rs_list->eof()==true)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Delete Images
if($rs_list->Fields("imagefilename")!="")
    DeleteFile($UPLOAD_IMG_PATH.$rs_list->Fields("imagefilename"));

if($rs_list->Fields("imagefilenamelarge")!="")
    DeleteFile($UPLOAD_IMG_PATH.$rs_list->Fields("imagefilenamelarge"));
#----------------------------------------------------------------------------------------------------
#update table
$str_query_update="UPDATE " .$STR_DB_TABLE_NAME. " SET imagefilenamelarge='',imagefilename='' WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to page	
CloseConnection();
Redirect("item_edit.php?type=S&msg=PD&pkid=".$int_pkid."&#ptop");
exit();
#----------------------------------------------------------------------------------------------------
?>