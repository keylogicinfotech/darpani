<?php
/*
File Name  :- item_add_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_image.php";	
include "./item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#get post data
$str_heading="";
$str_url="";
$str_description="";
$str_type="";
$dt_day="";
$dt_month="";
$dt_year="";
$str_openurl="YES";
$str_large_image="";
//$int_photosetpkid=0;

if(isset($_POST["txt_heading"]))
{
    $str_heading = trim($_POST["txt_heading"]);
}
	
if(isset($_POST["txt_newsurl"]))
{
    $str_url = trim($_POST["txt_newsurl"]);
}

if(isset($_POST["ta_desc"]))
{
    $str_description = trim($_POST["ta_desc"]);
}

if(isset($_POST["cbo_news_type"]))
{
    $str_type=trim($_POST["cbo_news_type"]);
}

if(isset($_POST["cbo_day"]))
{
    $dt_day = trim($_POST["cbo_day"]);
}

if(isset($_POST["cbo_year"]))
{
    $dt_year = trim($_POST["cbo_year"]);
}	

if(isset($_POST["cbo_month"]))
{
    $dt_month = trim($_POST["cbo_month"]);
}

if(isset($_POST["cbo_openurl"]))
{
    $str_openurl = trim($_POST["cbo_openurl"]);
}

if(isset($_FILES['file_news_image']))
{
    $str_large_image=trim($_FILES['file_news_image']['name']);
}
#------------------------------------------------------------------------------------------------
#Create String For Redirect
$str_redirect="";
$str_redirect="&heading=". urlencode(RemoveQuote($str_heading));
$str_redirect=$str_redirect . "&description=". urlencode(RemoveQuote($str_description));
$str_redirect=$str_redirect . "&url=". urlencode(RemoveQuote($str_url));
$str_redirect=$str_redirect . "&day=". urlencode(RemoveQuote($dt_day));
$str_redirect=$str_redirect . "&month=". urlencode(RemoveQuote($dt_month));
$str_redirect=$str_redirect . "&year=". urlencode(RemoveQuote($dt_year));
$str_redirect=$str_redirect . "&openurl=". urlencode(RemoveQuote($str_openurl));
$str_redirect=$str_redirect . "&newstype=". urlencode(RemoveQuote($str_type));

#----------------------------------------------------------------------------------------------------
#To check required parameters are passed properly or not

if($dt_day == 0 || $dt_month == 0 || $dt_year == 0)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E". $str_redirect."&#ptop");
    exit();
}

if(!checkdate($dt_month,$dt_day,$dt_year))
{
    CloseConnection();
    Redirect("item_list.php?msg=ID&type=E". $str_redirect."&#ptop");
    exit();
}

if($str_type == "" || $str_type == "0")
{
    CloseConnection();
    Redirect("item_list.php?msg=NT&type=E". $str_redirect."&#ptop");
    exit();
}

if($str_heading =="")
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E". $str_redirect."&#ptop");
    exit();
}

if( $str_url!="" )
{
    if( validateURL(strtolower($str_url))==false )
    {
        CloseConnection();
        Redirect("item_list.php?msg=IU&type=E". $str_redirect."&#ptop");
        exit();
    }
}

if( $str_large_image!="" )
{
//here we cannot use directly name of image file as first argument.
// instead we have to specify $_FILES['filename']['tmp_name'].

    if(ValidateImageExtension($_FILES['file_news_image']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
        CloseConnection();
        Redirect("item_list.php?msg=IE&type=E". $str_redirect."&#ptop");
        exit();
    }
}

if($str_description =="")
{
	CloseConnection();
	Redirect("item_list.php?msg=F&type=E". $str_redirect."&#ptop");
	exit();
}
#----------------------------------------------------------------------------------------------------
#duplicate checking
$dt_date="";
$dt_date=$dt_year."/".$dt_month."/".$dt_day;
$str_query_select="";
$str_query_select="SELECT heading FROM " .$STR_DB_TABLE_NAME. " WHERE date='".$dt_date."' AND heading='".ReplaceQuote($str_heading)."'";
$rs_list_check_duplicate=GetRecordSet($str_query_select);
if($rs_list_check_duplicate->eof()==false)
{
    //CloseConnection();
    //Redirect("item_list.php?msg=DU&type=E". $str_redirect."&#ptop");
    //exit();
}
#----------------------------------------------------------------------------------------------------
#Upload Images


$str_large_file_name="";
$str_thumb_file_name="";
$str_large_path="";
$str_thumb_path="";

if($str_large_image!="")
{
	
    $str_large_file_name=GetUniqueFileName()."_l.".getextension($str_large_image);
    $str_thumb_file_name=GetUniqueFileName()."_t.".getextension($str_large_image);

    $str_large_path = trim($UPLOAD_IMG_PATH . $str_large_file_name);
    $str_thumb_path = trim($UPLOAD_IMG_PATH . $str_thumb_file_name);
    UploadFile($_FILES['file_news_image']['tmp_name'],$str_large_path);
    ResizeImage($str_large_path,$INT_IMG_WIDTH);
    SaveAsThumbImage($str_large_path,$str_thumb_path,$INT_IMG_WIDTH_THUMB);
}	
#----------------------------------------------------------------------------------------------------
#Insert Record In a t_news table

$int_max_displayorder = 0;
$int_max_displayorder=GetMaxValue($STR_DB_TABLE_NAME,"displayorder");

$str_query_insert= "INSERT INTO ".$STR_DB_TABLE_NAME."(date,heading,url,openinnewwindow,";
$str_query_insert=$str_query_insert . "description,typefornews,imagefilenamelarge,imagefilename,";
$str_query_insert=$str_query_insert . "archive,visible,displayorder)";
$str_query_insert=$str_query_insert . " values('" . ReplaceQuote($dt_date) . "',";
$str_query_insert=$str_query_insert . "'" . ReplaceQuote($str_heading) . "',";
$str_query_insert=$str_query_insert . "'" . ReplaceQuote($str_url) . "',";
$str_query_insert=$str_query_insert . "'" . ReplaceQuote($str_openurl) . "',";
$str_query_insert=$str_query_insert . "'" . ReplaceQuote($str_description) . "',";
$str_query_insert=$str_query_insert . "'" . ReplaceQuote($str_type) . "',";
$str_query_insert=$str_query_insert . "'" . ReplaceQuote($str_large_file_name) . "',";
$str_query_insert=$str_query_insert . "'" . ReplaceQuote($str_thumb_file_name) . "',";
$str_query_insert=$str_query_insert . "'NO',";
$str_query_insert=$str_query_insert . "'YES',".$int_max_displayorder.")";

ExecuteQuery($str_query_insert);
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to page	
CloseConnection();
Redirect("item_list.php?type=S&msg=S&heading=".urlencode(RemoveQuote($str_heading))."&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------
?>
