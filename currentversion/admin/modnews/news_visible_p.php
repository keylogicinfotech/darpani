<?php
/*
	Module Name:- modNews
	File Name  :- news_visible_p.php
	Create Date:- 10-FEB-2006
	Intially Create By :- 0023
	Update History:
*/
#----------------------------------------------------------------------------------------------------
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
	include "news_config.php";
	include "news_app_specific.php";
#----------------------------------------------------------------------------------------------------
#get Query String Data
$int_pkid="";
if(isset($_GET["pkid"])==true)
	$int_pkid=trim($_GET["pkid"]);
	
#To check required parameters are passed properly or not
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
	CloseConnection();
	Redirect("news_list.php?msg=F&type=E&#ptop");
	exit();
}
$gopage=1;
if(isset($_GET["gopage"])==true)
	$gopage=trim($_GET["gopage"]);
#----------------------------------------------------------------------------------------------------
#select query to get news details from table

$str_query_select="select newsheading,visible from t_news where newspkid=". $int_pkid;
$rs_visible=GetRecordSet($str_query_select);

if($rs_visible->eof()==true)
{
	CloseConnection();
	Redirect("news_list.php?msg=F&type=E");
	exit();
}
$str_mode=$rs_visible->fields("visible");

$str_title=trim($rs_visible->fields("newsheading"));
#----------------------------------------------------------------------------------------------------
#change visible mode

if(strtoupper($str_mode)=="YES")
{
		$str_mode="NO";
		$str_mode_title="Invisible";
}
else if(strtoupper($str_mode)=="NO")
{
		$str_mode="YES";
		$str_mode_title="Visible";
}
#----------------------------------------------------------------------------------------------------
#update status in t_news table
$str_query_update="update t_news set visible='" . ReplaceQuote($str_mode) . "' where newspkid=".$int_pkid;
ExecuteQuery("$str_query_update");
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file

WriteXml();

#----------------------------------------------------------------------------------------------------
#Close connection and redirect to news_list.php page	
	CloseConnection();
	if ($gopage==1)
		Redirect("news_list.php?type=S&msg=V&heading=".urlencode(RemoveQuote($str_title))."&visible=". urlencode(RemoveQuote($str_mode_title)) ."&#ptop");
	elseif ($gopage==2)
		Redirect("news_archived.php?type=S&msg=V&heading=".urlencode(RemoveQuote($str_title))."&visible=". urlencode(RemoveQuote($str_mode_title)) ."&#ptop");
	exit();
#------------------------------------------------------------------------------------------------------------

?>