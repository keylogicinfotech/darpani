function showdetails(url)
{
    window.open(url,'latestupdate','left=50,top=20,resizable=yes,scrollbars=yes,width=570,height=490');
    return false;
}

function confirm_archive()
{
    if(confirm("Are you sure you want to send this details to archived list?"))
    {
        if(confirm("Click 'Ok' to send this details to archived list or click 'Cancel' to cancel."))
        {
            return true;
        }
        return false;
    }
    return false;
}

function confirm_restore()
{
    if(confirm("Are you sure you want to restore this details?"))
    {
        if(confirm("Click 'Ok' to restore this details or click 'Cancel' to cancel."))
        {
            return true;
        }
        return false;
    }
    return false;
}

function confirm_delete()
{
    if(confirm("Are you sure you want delete this details?"))
    {
        if(confirm("Click 'Ok' to delete this details or click 'Cancel' to cancel."))
        {
            return true;
        }
        return false;
    }
    return false;
}

function frm_add_validate()
{
    with(document.frm_add)
    {
        if(isDate('mdy',cbo_month.value + "/" + cbo_day.value + "/" + cbo_year.value)==false)
        {
            alert("Please enter valid date.");
            return false;
        }

        if(trim(cbo_news_type.value)=="" || trim(cbo_news_type.value)==0)
        {
            alert("Please select type for news.");
            cbo_news_type.focus();	
            return false;
        }

        if(isEmpty(txt_heading.value))
        {
            alert("Please enter title.");
            txt_heading.focus();
            return false;
        }
        if(trim(txt_newsurl).value!="")
        {
            if(!isValidUrl(txt_newsurl.value))
            {
                txt_newsurl.select();
                txt_newsurl.focus();
                return false;			
            }
        }
        if(file_news_image.value!="")
        {
            if(checkExt(file_news_image.value)==false)
            {
                file_news_image.select();
                file_news_image.focus();
                return false;
            }
        }
        if (isEmpty(ta_desc.value))
        {
            alert("Please enter description.");
            ta_desc.select();
            ta_desc.focus();
            return false;
        }
    }
    return true;
}