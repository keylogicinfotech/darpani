<?php
/*
File Name  :- item_del_p.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";	
include "./item_app_specific.php";
#------------------------------------------------------------------------------------------------
#get Query String Data
$int_pkid="";
if(isset($_GET["pkid"])==true)
    $int_pkid=trim($_GET["pkid"]);
#To check required parameters are passed properly or not
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
$int_gopage=1;
if(isset($_GET["gopage"])==true)
    $int_gopage=trim($_GET["gopage"]);
#----------------------------------------------------------------------------------------------------
#select query
$str_query_select="SELECT heading,imagefilename,imagefilenamelarge FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=". $int_pkid;
$rs_list=GetRecordSet($str_query_select);

if($rs_list->eof()==true)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
$str_title="";
$str_title=trim($rs_list->fields("newsheading"));
#----------------------------------------------------------------------------------------------------
#Delete Images
if($rs_list->Fields("imagefilename")!="")
    DeleteFile($UPLOAD_IMG_PATH.$rs_list->Fields("imagefilename"));

if($rs_list->Fields("imagefilenamelarge")!="")
    DeleteFile($UPLOAD_IMG_PATH.$rs_list->Fields("imagefilenamelarge"));
	
#----------------------------------------------------------------------------------------------------
#Delete query
$str_query_delete="DELETE FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=" .$int_pkid;
ExecuteQuery($str_query_delete);
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to page	
CloseConnection();
    Redirect("item_list.php?type=S&msg=D&heading=".urlencode(RemoveQuote($str_title))."&gopage=".$int_gopage."&#ptop");
exit();
#----------------------------------------------------------------------------------------------------
?>