<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#-------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";	
include "./item_app_specific.php";
#-------------------------------------------------------------------------------------
#get Query String Data
$int_pkid="";
if(isset($_GET["pkid"])==true)
    $int_pkid=trim($_GET["pkid"]);
	
#To check required parameters are passed properly or not
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
$gopage=1;
if(isset($_GET["gopage"])==true)
    $gopage=trim($_GET["gopage"]);
#--------------------------------------------------------------------------------------
#select query
$str_query_select="SELECT heading,visible FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=". $int_pkid;
$rs_list=GetRecordSet($str_query_select);
if($rs_list->eof()==true)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
$str_mode=$rs_list->fields("visible");
$str_title=trim($rs_list->fields("heading"));
#--------------------------------------------------------------------------------------
#change visible mode
if(strtoupper($str_mode)=="YES")
{
    $str_mode="NO";
    $str_mode_title="Invisible";
}
else if(strtoupper($str_mode)=="NO")
{
    $str_mode="YES";
    $str_mode_title="Visible";
}
#--------------------------------------------------------------------------------------
#update status in table
$str_query_update="UPDATE " .$STR_DB_TABLE_NAME. " SET visible='" . ReplaceQuote($str_mode) . "' WHERE pkid=".$int_pkid;
ExecuteQuery("$str_query_update");
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#---------------------------------------------------------------------------------------
#Close connection and redirect to page	
CloseConnection();
if ($gopage==1)
    Redirect("item_list.php?type=S&msg=V&heading=".urlencode(RemoveQuote($str_title))."&visible=". urlencode(RemoveQuote($str_mode_title)) ."&gopage=1");
elseif ($gopage==2)
    Redirect("item_list.php?type=S&msg=V&heading=".urlencode(RemoveQuote($str_title))."&visible=". urlencode(RemoveQuote($str_mode_title)) ."&gopage=2");
exit();
#-----------------------------------------------------------------------------------------
?>