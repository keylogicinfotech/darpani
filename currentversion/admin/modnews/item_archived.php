<?php
/*
File Name  :- item_archived.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_image.php";
#------------------------------------------------------------------------------------------------
# Select Query
/*$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME. "  WHERE archive='NO'";
$str_query_select= $str_query_select. "ORDER BY typefornews desc,date desc,heading";

$rs_list=GetRecordSet($str_query_select);

$str_query_select="";
$str_query_select="SELECT count(*) noofrec FROM " .$STR_DB_TABLE_NAME. " WHERE archive='YES'";
$rs_list_archived=GetRecordSet($str_query_select);
$int_total=0;
if($rs_list_archived->eof()==false)
    $int_total=$rs_list->fields("noofrec"); */
#------------------------------------------------------------------------------------------------
# Select Query to display list
$str_query_select="";
$str_query_select="select * from t_news where archive='NO'";
$str_query_select= $str_query_select. "ORDER BY typefornews desc,date desc,heading";
$rs_list=GetRecordSet($str_query_select);

$str_query_select="";
$str_query_select="select count(*) noofrec from t_news where archive='YES'";
$rs_group=GetRecordSet($str_query_select);
$int_total=0;
if($rs_group->eof()==false)
	$int_total=$rs_group->fields("noofrec");
# Select Query to display  Archived list
$str_query_select="";
$str_query_select="select * from t_news  where archive='YES'";
$str_query_select= $str_query_select. "order by typefornews desc, heading";
$rs_list_archived=GetRecordSet($str_query_select);
$str_query_select="";
$str_query_select="select count(*) noofrec from t_news where archive='NO'";
$rs_group_archived=GetRecordSet($str_query_select);
$int_total=0;
if($rs_group_archived->eof()==false)
	$int_total=$rs_group_archived->fields("noofrec"); 
#------------------------------------------------------------------------------------------------
#Getting query string data
$str_visible="";
$str_heading="";
if (isset($_GET["visible"]))
    $str_visible = trim($_GET["visible"]);

if (isset($_GET["heading"]))
    $str_heading = trim($_GET["heading"]);
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#	Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = "Some information(s) missing.Please try again."; break;
        case("D"): $str_message = "Latest Updates '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_heading,30))) . "' details deleted sucessfully."; break;
        case("U"): $str_message = "Latest Updates '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_heading,30))) . "' details updated successfully."; break;
        case("V"): $str_message = "Latest Updates '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_heading,30))) . "' changed to '". $str_visible . "' successfully."; break;
        case("R"): $str_message = "Latest Updates '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_heading,30))) . "' details restored successfully."; break; 
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<!-- Page Content -->
<a name="ptop"></a>
<div class="container">
<?php include "../../includes/adminheader.php"; ?>
    <div class="row">
        <div class="col-lg-3 col-sm-6 col-md-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
            <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Back to State List"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;Back</a><?php */?>
            </div>
        </div>
        <div class="col-lg-9 col-sm-6 col-md-12" align="right" ><h3><?php print($STR_TITLE_PAGE); ?></h3></div>
    </div>
    <hr>
    <?php   if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); } ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <div class="row">
                        <div class="col-md-12  padding-10">
                            <div class="col-md-6 col-sm-6 col-xs-8"></div>
                            <div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>
                        </div>
                    </div>
                </h4>
            </div>
            <div class="table-responsive">
                <div class="tab-pane" id="news_archived">
                    <form name="frm_list" method="post" action="subscriber_setstatus_p.php">
			<table class="table table-striped table-bordered table-hover">
                            <thead>	
                                <tr>					
                                    <th width="4%">Sr. #</th>
                                    <th width="8%">Create Date..</th>
                                    <th width="">Latest Updates Heading</th>
                                    <th width="3%" >Restore..</th>	
                                    <th width="8%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if ($rs_list_archived->eof() == true) { ?>
                                <tr><td colspan="5" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                            <?php }  else { $str_type=""; $int_cnt=1; while($rs_list_archived->eof()==false) { ?>
                            <?php if($str_type != $rs_list_archived->fields("typefornews")) { ?>
                                <tr><td bgcolor="#87CEEB" colspan="6" align="left" >For <span>[ <?php print(RemoveQuote(MyhtmlEncode($rs_list_archived->fields("typefornews"))));?> ]</span></td></tr>
                            <?php } ?>
                                <tr class="<?php print($int_cnt%2); ?>">
                                    
                                    <td align="center" valign="middle" ><?php print($int_cnt);?>
                                        <input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list_archived->fields("pkid"));?>">
                                    </td>
                                    <td align="center"><?php print(DDMMMYYYYFormat($rs_list_archived->fields("date")));?></td>
                                    <td>
                                    <?php if($rs_list_archived->fields("imagefilename")!="") { ?>
                                                    <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($UPLOAD_IMG_PATH.$rs_list_archived->fields("imagefilenamelarge"));?>" rel="thumbnail"><img class="img-responsive" src="<?php print($UPLOAD_IMG_PATH.$rs_list_archived->fields("imagefilename"));?>"  border="0" align="absmiddle" alt="News image" title="<?php print($STR_HOVER_IMAGE); ?>" alt="<?php print($STR_HOVER_IMAGE); ?>"></a>
                                    <div class="modal fade f-pop-up-<?php print($rs_list_archived->fields("pkid")); ?> " align="center" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                <img src="<?php print($UPLOAD_IMG_PATH.$rs_list_archived->fields("imagefilenamelarge")."/".$rs_list_archived->fields("imagefilenamelarge"));?>" class="img-responsive img-rounded" title="<?php print $rs_list_archived->fields("imagefilenamelarge"); ?>" alt="<?php print $rs_list_archived->fields("imagefilenamelarge"); ?>"></div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                <?php } ?>
                                    <p></p>			
                                    <div align="justify" class="DescriptionText">
                                        <span class="text-primary"><b><?php print(MyHtmlEncode($rs_list_archived->fields("heading")));?></b></span>
                                        <?php if($rs_list_archived->fields("url")!="") { ?>
                                            <br><span class="DescriptionText"><strong>Top Title:</strong> </span><?php print(DisplayWebSiteURL(trim(MyHtmlEncode($rs_list_archived->fields("url"))),"",trim(strtoupper($rs_list_archived->fields("openinnewwindow"))),"Click to view the website","MenuLink10ptNormal","",60));?>	
                                        <?php } ?>
                                        <?php if($rs_list_archived->fields("description")!="") {?>
                                            <br><span class="DescriptionText"><span class=""><strong>Bottom Title:</strong> </span><?php print(MyHtmlEncode($rs_list_archived->fields("description")));?></span><?php }?>
                                    </div>
                                    <?php $str_image="";
                                    if(strtoupper($rs_list_archived->fields("visible"))=="YES")
                                        { $str_image = "<img src='../images/visible.gif' border='0' title=\"Click to set 'Invisible'\">"; }
                                    else { $str_image = "<img src='../images/invisible.gif' border='0' title=\"Click to set 'Visible'\">"; }
                                    ?>				
                                    </td>
                                    <td align="center"><a href="item_archived_p.php?pkid=<?php print($rs_list_archived->fields("pkid"))?>&gopage=2" onClick="return archive_confirm();" title="Click to restore this Latest Updates detail"><i class="glyphicon glyphicon-retweet"></i></a></td>	 
                                    <td align="center">
                                    <a class="btn btn-warning btn-xs" href="item_visible_p.php?pkid=<?php print($rs_list_archived->fields("pkid"));?>&gopage=1" title="Click to make it invisible"><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a class="btn btn-success btn-xs" href="item_edit.php?pkid=<?php print($rs_list_archived->fields("pkid"))?>" title="Click To edit Latest News / Announcement details"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a class="btn btn-danger btn-xs" href="item_del_p.php?pkid=<?php print($rs_list_archived->fields("pkid"))?>&gopage=1" onClick="return confirm_delete();" title="Click To delete Latest News / Announcement details parmanently"><i class="glyphicon glyphicon-remove"></i></a></td>
				</tr>					
				<?php $str_type = $rs_list_archived->fields("typefornews");
                                $rs_list_archived->movenext(); $int_cnt = $int_cnt +1; } //while end
                                }//else and ?>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
    <!-- /.container -->											
</div>
<!-- Bootstrap Core CSS -->
<script language="JavaScript" src="./item_archived.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>
<?php /*?><script src="../includes/thumbnailviewer.js" type="text/javascript"></script><?php */?>
</body></html>