<?php
/*
File Name  :- item_edit.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_image.php";
#------------------------------------------------------------------------------------------------
#Getting query string data
$int_pkid="";
if(isset($_GET["pkid"])==true)
{
    $int_pkid=trim($_GET["pkid"]);
}

$int_gopage = "";
if(isset($_GET["gopage"])==true)
{
    $int_gopage = trim($_GET["gopage"]);
}


$str_return_page="";
if(isset($_GET["retpage"]))
{
    $str_return_page=$_GET["retpage"];
}
$str_news_type="";
if(isset($_GET["type"]))
{
    $str_news_type=trim($_GET["type"]);
}
	
#To check required parameters are passed properly or not
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}

$str_heading = "";
if (isset($_GET["heading"]))
    $str_heading = trim($_GET["heading"]);
#------------------------------------------------------------------------------------------------
#Select query to get all details from table
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
$rs_list=GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("PD"): $str_message= $STR_MSG_ACTION_DELETE_IMAGE; break;
        case("IE"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("DU"): $str_message = "Latest Updates with heading '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_heading,14))) . "' already exists in selected date. Please try with another Latest Updated heading."; break;
        case("ID"): $str_message = $STR_MSG_NEWS_CATAGORY; break;
        case("NT"): $str_message = $STR_MSG_NEWS_CATAGORY; break;
        case("IU"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;	
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_EDIT); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> 
<div class="container">
    <?php include $STR_ADMIN_HEADER_PATH; ?>
    <a name="ptop" id="ptop"></a>
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="fa fa-chevron-left "></i>&nbsp;Back</a><?php */?>
                <a href="./item_list.php" class="btn btn-default" title="Click to go News / Announcement list" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE);?></a>
            </div>
        </div>
        <div class="col-md-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE);?></h3></div>
	</div>
        <hr> 
	<?php if($str_type != "" && $str_message != "")	{ print(DisplayMessage(0,$str_message,$str_type)); }	?>
	<div class="row padding-10">
            <div class="col-md-12">
        	<div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                	<div class="panel-heading">
                            <h4 class="panel-title">
                                <i class="fa fa-edit"></i>&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b> </a>
                                <?php print($STR_LINK_HELP); ?>                                                          
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse">
                            <div class="panel-body">
                                <form name="frm_edit" action="item_edit_p.php" method="post" onSubmit="return frm_edit_validate()" enctype="multipart/form-data">
                                    <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="form-group">
                                        <label>News / Announcement Date <span class="text-help-form"> *</span></label><br/>
                                        <?php $date=explode("-",$rs_list->fields("date")); 
                                        print(DisplayDate("cbo_day","cbo_month","cbo_year",$date[2],$date[1],$date[0]));
                                        ?>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                           <div class="form-group">
                                                <label>News / Announcement For <span class="text-help-form"> *</span></label>
                                                <select name="cbo_news_type" class="form-control input-sm" >
                                                    <?php /*?><option value="">-- Select Type --</option><?php */?>
                                                    <option value="REGULAR" <?php print(CheckSelected("REGULAR",$rs_list->fields("typefornews")))?>><?php print $str_user1; ?></option>
                                                    <option value="WHOLESALER" <?php  print(CheckSelected("WHOLESALER",$rs_list->fields("typefornews")))?>><?php  print $str_user2; ?></option>
                                               </select>
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Title</label><span class="text-help-form"> *</span>
                                                <input type="text" name="txt_heading" size="93" maxlength="255" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("heading"))));?>" class="form-control input-sm" >
                                        </div> 
                                        </div>
                                    </div>
									
								
                                    <div class="form-group">
                                        <label>Description <span class="text-help-form"> *</span></label>
                                        <textarea name="ta_desc" id="ta_desc" rows="10" wrap="virtual" class="form-control input-sm"><?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("description"))));?></textarea>
                                    </div>
                                    <div>
                                        <input type="hidden" name="txt_newsurl" value="">
                                        <input type="hidden" name="cbo_openurl" value="YES">
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Existing Image</label>
                                                    
                                            <?php if($rs_list->fields("imagefilenamelarge")!="") { ?>
                                                <a href="./item_del_img_p.php?pkid=<?php print($int_pkid);?>&flag=1" title="<?php print($STR_HOVER_DELETE_IMAGE)?>" onClick="return confirm_delete();" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a><br>


                                                <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list->fields("pkid"));?>" rel="thumbnail"><img class="img-responsive" src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilenamelarge"));?>"  border="0" align="absmiddle" alt="<?php print $rs_list->fields("imagefilenamelarge")?>" title="<?php print $rs_list->fields("imagefilenamelarge")?>"></a>
                                                <div class="modal fade f-pop-up-<?php print($rs_list->fields("pkid")); ?> " align="center"  role="dialog" aria-labelledby="" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                                <img src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilenamelarge"));?>" class="img-responsive img-rounded" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image">
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div>															
                                            <?php } else { print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); }?>
                                                            <?php // else { print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); } ?>
                                        <!--</div>-->
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Upload New Image</label>
                                                <span class="text-help-form"> (<?php print($STR_MSG_IMG_FILE_TYPE);?></span>&nbsp;<span class="text-help-form"> | <?php print($STR_MSG_IMG_UPLOAD);?>)</span>
                                                <input type="file" name="file_news_image" maxlength="255" >
                                            </div>
                                        </div>
                                    </div>

                                    
                                        <input type="hidden" name="hdn_pkid" value="<?php print($int_pkid); ?>">
                                        <input type="hidden" name="hdn_gopage" value="<?php print($int_gopage); ?>">
                                        
                                    <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <a name="help"></a>
    <?php include "../../includes/help_for_edit.php"; ?>
</div>
<script language="JavaScript" src="./item_edit.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>
</body></html>