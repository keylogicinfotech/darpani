<?php
/*
File Name  :- item_archived_p.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
include "./item_app_specific.php";
#------------------------------------------------------------------------------------------------
#get Query String Data
$int_pkid="";
if(isset($_GET["pkid"])==true)
    $int_pkid=trim($_GET["pkid"]);
#To check required parameters are passed properly or not
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
$gopage=1;
if(isset($_GET["gopage"])==true)
    $gopage=trim($_GET["gopage"]);
#----------------------------------------------------------------------------------------------------
#select query
$str_query_select="SELECT heading FROM " .$STR_DB_TABLE_NAME. "  WHERE pkid=". $int_pkid;
$rs_list=GetRecordSet($str_query_select);
if($rs_list->eof()==true)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
$str_title="";
$str_title=trim($rs_list->fields("heading"));
#----------------------------------------------------------------------------------------------------
if ($gopage==1)
{
    $strqry="UPDATE " .$STR_DB_TABLE_NAME. " SET archive='YES' WHERE pkid=" . $int_pkid;
    ExecuteQuery($strqry);
    WriteXml();
    CloseConnection();
    Redirect("item_list.php?msg=R&type=S&heading=".urlencode(RemoveQuote($str_title))."&gopage=".$gopage."&#ptop");
    exit();
}
elseif ($gopage==2)
{
    $strqry="UPDATE " .$STR_DB_TABLE_NAME. " SET archive='NO' WHERE pkid=" . $int_pkid;
    ExecuteQuery($strqry);
    WriteXml();
    CloseConnection();
    Redirect("item_list.php?msg=R&type=S&heading=".urlencode(RemoveQuote($str_title))."&gopage=".$gopage."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
?>