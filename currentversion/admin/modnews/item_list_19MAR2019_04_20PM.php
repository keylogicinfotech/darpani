<?php
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_image.php";
#------------------------------------------------------------------------------------------------
# Select Query to display list
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE archive='NO'";
$str_query_select= $str_query_select. "ORDER BY typefornews DESC, displayorder DESC, date DESC,pkid,heading";
$rs_list=GetRecordSet($str_query_select);

# Select Query to display  Archived list
$str_query_select="";
$str_query_select="SELECT count(*) noofrec FROM " .$STR_DB_TABLE_NAME. " WHERE archive='YES'";
$rs_list_count_record=GetRecordSet($str_query_select);
$int_total=0;
if($rs_list_count_record->eof()==false)
	$int_total=$rs_list_count_record->fields("noofrec");

# Select Query to display  Archived list
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE archive='YES'";
$str_query_select= $str_query_select. "" .$STR_DB_TABLE_NAME_ORDER_BY;
$rs_list_archived=GetRecordSet($str_query_select);

$str_query_select="";
$str_query_select="SELECT count(*) noofrec FROM " .$STR_DB_TABLE_NAME. " WHERE archive='NO'";
$rs_list_count=GetRecordSet($str_query_select);
$int_total=0;
if($rs_list_count->eof()==false)
	$int_total=$rs_list_count->fields("noofrec");
#------------------------------------------------------------------------------------------------
# get Query String Data
$str_newsheading="";
$str_url="";
$str_description="";
$str_news_type="";
$dt_day="";
$dt_month="";
$dt_year="";
$str_openurl="YES";
$str_visible="";
$str_heading="";
$str_news_for="";

if(isset($_GET["heading"]))
{
    $str_newsheading = trim($_GET["heading"]);
}
	
if(isset($_GET["url"]))
{
    $str_url = trim($_GET["url"]);
}

if(isset($_GET["description"]))
{
    $str_description = trim($_GET["description"]);
}

if(isset($_GET["type"]))
{
    $str_news_type=trim($_GET["type"]);
}

if(isset($_GET["day"]))
{
    $dt_day = trim($_GET["day"]);
}

if(isset($_GET["year"]))
{
    $dt_year = trim($_GET["year"]);
}	
if(isset($_GET["month"]))
{
    $dt_month = trim($_GET["month"]);
}
if(isset($_GET["openurl"]))
{
    $str_openurl = trim($_GET["openurl"]);
}
if(isset($_GET["visible"]))
{
    $str_visible = trim($_GET["visible"]);
}
if(isset($_GET["heading"]))
{
    $str_heading = trim($_GET["heading"]);
}

$int_go_page = 0;
$str_active_news_tab = "";
$str_active_archived_tab = "";

$str_active_news_tab_link = "";
$str_active_archived_tab_link = "";
if(isset($_GET["gopage"]))
{
    $int_go_page = trim($_GET["gopage"]);
}
if($int_go_page == 1)
{
    $str_active_news_tab = "active";
    $str_active_archived_tab = "";
    
    $str_active_news_tab_link = "in active";
    $str_active_archived_tab_link = "";
}
else if($int_go_page == 2)
{
    $str_active_news_tab = "";
    $str_active_archived_tab = "active";
    
    $str_active_news_tab_link = "";
    $str_active_archived_tab_link = "in active";

} else {
    $str_active_news_tab = "active";
    $str_active_archived_tab = "";
    
    $str_active_news_tab_link = "in active";
    $str_active_archived_tab_link = "";
}
# get Query String Data (Archived)
$str_visible="";
$str_heading="";

if (isset($_GET["visible"]))
	$str_visible = trim($_GET["visible"]);

if (isset($_GET["heading"]))
	$str_heading = trim($_GET["heading"]);
#------------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;

    }
}
#Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("IE"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("U"): $str_message = "'" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_heading,30))) . "' SUCCESS !!! Details updated."; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_visible; break;
        case("DU"): $str_message = "Latest News / Announcement with title '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_newsheading,14))) . "' already exists in selected date. Please try with another title."; break;
        case("R"): $str_message = "'" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_heading,30))) . "' SUCCESS !!! Details sent to archive list."; break;
        case("ID"): $str_message = $STR_MSG_NEWS_CATAGORY; break;
        case("NT"): $str_message = $STR_MSG_NEWS_CATAGORY; break;
        case("IU"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
    }
}
#Initialization of variables used for message display.   
$str_type_archived = "";
$str_message_archived = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type_archived = "S"; break;
        case("E"): $str_type_archived = "E"; break;
        case("W"): $str_type_archived = "W"; break;
    }
}
#Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message_archived = $STR_MSG_ACTION_INFO_MISSING; break;
        case("D"): $str_message_archived = $STR_MSG_ACTION_DELETE; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_visible; break;
        case("U"): $str_message_archived = "'" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_heading,30))) . "' SUCCESS !!! Details updated."; break;
        case("R"): $str_message_archived = "'" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_heading,30))) . "' SUCCESS !!! Details restored."; break; 
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript">
function tabText(text,headtext){
	document.title = text;
	myHeader.innerText=headtext;
}
</script>
</head>
<body>
<!-- Page Content -->
<a name="ptop"></a>
<div class="container">
<?php include $STR_ADMIN_HEADER_PATH; ?>
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-md-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                    <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Back to State List"><i class="fa fa-chevron-left"></i>&nbsp;Back</a><?php */?>
            </div>
        </div>
        <div class="col-md-9 col-sm-6 col-md-12" align="right"><h3 id="myHeader"><?php print($STR_TITLE_PAGE); ?></h3></div>
        </div>
        <hr>
        <?php if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); } ?>
        <div class="row padding-10">
            <div class="col-md-12">
                <div class="panel-heading">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="<?php print $str_active_news_tab; ?>"><a href="#news" data-toggle="tab" title="<?php print($STR_TITLE_PAGE);?>" onClick="tabText('<?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?>', '<?php print($STR_TITLE_PAGE); ?>')"><i class="fa fa-bullhorn" aria-hidden="true"></i>&nbsp;&nbsp;<b>News / Announcement  List</b></a></li> 
                        <li class="<?php print $str_active_archived_tab; ?>"><a href="#archived" onClick="tabText('<?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_ARCHIVED_NEWS_ANNOUNCEMENT);?>', '<?php print($STR_TITLE_ARCHIVED_NEWS_ANNOUNCEMENT);?>')" data-toggle="tab" title="Click to see archived list."><i class="fa fa-bullhorn" aria-hidden="true"></i>&nbsp;&nbsp;<b>View Archived News / Announcement List</b></a></li>
                    </ul>
                </div><br/>
            </div>
        </div>
        
    
	<?php /*?><?php	if($str_type_archived != "" && $str_message_archived != "") { print(DisplayMessage(0,$str_message_archived,$str_type_archived)); } ?><?php */?>
        <div class="row padding-10">
            <div class="col-md-12">
                <div id='content' class="tab-content">
                    <div class="tab-content">
    
                        <div class="tab-pane <?php print $str_active_news_tab_link; ?>" id="news">
                            <div class="row padding-10">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <span>
                                                        <a class="accordion-toggle collapsed link" title="<?php print($STR_TITLE_ADD); ?>" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>
                                                    </span>
                                                    <?php print($STR_LINK_HELP); ?>
                                                </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
                                            <div class="panel-body">
                                                <form name="frm_add" action="item_add_p.php" method="post" onSubmit="return frm_add_validate();" enctype="multipart/form-data" role="form">
                                                    <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                                    <div class="form-group">
                                                        <label>News / Announcement Date<span class="text-help"> *</span></label><br/>
                                                        <?php if($dt_day!="" && $dt_month!="" && $dt_year!="") { DisplayDate("cbo_day","cbo_month","cbo_year",$dt_day,$dt_month,$dt_year); }
                                                        else { DisplayDate("cbo_day","cbo_month","cbo_year",date("d"),date("m"),date("Y")); } ?>
                                                    </div>
                                                    <div class="row padding-10">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>News / Announcement For<span class="text-help"> *</span></label>
                                                                <select name="cbo_news_type" class="form-control input-sm" >
                                                                    <?php /*?><option value="">-- Select Type --</option><?php */?>
                                                                    <option value="MODEL" <?php print(CheckSelected("MODEL",$str_news_type))?>><?php print $str_user1; ?></option>
                                                                    <!--<option value="MEMBER" <?php // print(CheckSelected("MEMBER",$str_news_type))?>><?php // print $str_user2; ?></option>-->
                                                               </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Title<span class="text-help"> *</span></label>&nbsp;<span class="text-help-form">(<?php print($STR_MSG_TITLE)?>)</span>
                                                                <input type="text" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" name="txt_heading" maxlength="255" value="<?php print(MyHtmlEncode(RemoveQuote($str_newsheading)));?>" class="form-control input-sm" >  		
                                                            </div>	
                                                        </div>                                                
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description<span class="text-help"> *</span></label>
                                                        <textarea name="ta_desc" id="ta_desc" rows="10" wrap="virtual" class="form-control input-sm" placeholder="Enter description here" ><?php print(MyHtmlEncode(RemoveQuote($str_description)));?></textarea>
                                                    </div>
                                                    <div>
                                                        <input type="hidden" name="txt_url" value="">
                                                        <input type="hidden" name="cbo_openurl" value="YES">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Upload Photograph</label>&nbsp;<span class="text-help-form">(<?php print($STR_MSG_IMG_FILE_TYPE."&nbsp;|&nbsp;".$STR_MSG_IMG_UPLOAD);?><?php //print($STR_IMG_FILE_TYPE."&nbsp;|&nbsp;".$STR_NEWS_IMG_MSG."&nbsp;|&nbsp;".$STR_IMG_UPLOAD_MSG);?>)</span>
                                                        <input type="file" name="file_news_image" >
                                                    </div>	
                                                    <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>						
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            
                        <div class="row padding-10">
                            <div class="col-md-12">    
                        <div class="table-responsive">
                            <form name="frm_list" method="post" action="item_order_p.php" onClick="return active_tab();">
                                <table class="table table-striped table-bordered ">
                                    <thead>	
                                        <tr>					
                                            <th width="4%">Sr. #</th>
                                            <th width="10%">Create Date</th>
                                            <th width="">Details</th>
                                            <th width="4%">Display Order</th>	
                                            <th width="3%" >Archive</th>	
                                            <th width="5%">Action</th></tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($rs_list->eof() == true) { ?><tr><td colspan="6" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                                    <?php }  else { $str_news_type=""; $int_cnt=1; while($rs_list->eof()==false) { ?>
                                    <?php if($str_news_type != $rs_list->fields("typefornews")) { ?>
                                        <tr><td colspan="6" align="left" class="alert-warning">
                                                <h5><b>&nbsp;<?php if(RemoveQuote(MyhtmlEncode($rs_list->fields("typefornews")))=="MEMBER") { print("MEMBER"); } else { print(RemoveQuote(MyhtmlEncode($rs_list->fields("typefornews")))); } ?></b></h5></td></tr>
                                    <?php } ?>
                                        <tr>
                                            <td align="center" valign="middle" class="<?php print($int_cnt%2); ?>"><?php print($int_cnt);?>
                                                <input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>">
                                            </td>
                                            <td align="center" class="text-help"><i><?php print(DDMMMYYYYFormat($rs_list->fields("date")));?></i></td>
                                            <td>


                                          <?php if($rs_list->fields("imagefilename")!="") { ?>
                                            <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list->fields("pkid"));?>" rel="thumbnail"><img  class="img-responsive" src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>"  border="0" align="absmiddle" alt="<?php print $rs_list->fields("imagefilename")?>" title="<?php print $rs_list->fields("imagefilename")?>"></a>
                                                            <div class="modal fade f-pop-up-<?php print($rs_list->fields("pkid")); ?> " align="center" role="dialog" aria-labelledby="" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-body">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                                            <img src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilenamelarge"));?>" class="img-responsive img-rounded" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image">
                                                                        </div>
                                                                    </div><!-- /.modal-content -->
                                                                </div><!-- /.modal-dialog -->
                                                            </div>															
                                                    <?php }  else { print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); }?>
                                                    <p></p>					
                                                    <div align="justify" class="">
                                                        <h4 class="nopadding"><b><?php print(MyHtmlEncode($rs_list->fields("heading")));?></b></h4>
                                                        <?php if($rs_list->fields("url")!="") { ?>
                                                            <span class=""><?php print(DisplayWebSiteURL(trim(MyHtmlEncode($rs_list->fields("url"))),"",trim(strtoupper($rs_list->fields("openinnewwindow"))),"Click to view the website","","",60));?></span>
                                                        <?php } ?>
                                                        <?php if($rs_list->fields("description")!="") {?>
                                                            <span class=""><?php print(RemoveQuote($rs_list->fields("description")));?></span><?php }?>
                                                    </div>
                                                    <?php $str_image=""; $str_class=""; $str_title="";
                                                    if(strtoupper($rs_list->fields("visible"))=="YES")
                                                    {   $str_image="<i class='fa fa-eye'></i>";
                                                        $str_class="btn btn-warning btn-xs";
                                                        $str_title=$STR_HOVER_VISIBLE;
                                                    }
                                                    else
                                                    {   $str_image="<i class='fa fa-eye-slash'></i>";
                                                        $str_class="btn btn-default active btn-xs";
                                                        $str_title=$STR_HOVER_INVISIBLE;
                                                    }
                                                    ?>		
                                                </td>

                                                <td align="center">
                                                    <input type="text" name="txt_displayorder<?php print($int_cnt);?>" maxlength="5" value="<?php print($rs_list->fields("displayorder"));?>" class="form-control input-sm text-center">
                                                    <input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>">
                                                </td>
                                                <td align="center"><h4 class="nopadding"><a href="item_archived_p.php?pkid=<?php print($rs_list->fields("pkid"))?>&gopage=1" onClick="return confirm_archive();" title="<?php print($rs_list->fields("pkid"))?>" class=""><i class="fa fa-folder-open"></i></a></h4> </td>	 
                                                <td align="center">
                                                    <p><a class="<?php print($str_class); ?>" href="./item_visible_p.php?pkid=<?php print($rs_list->fields("pkid"));?>&gopage=1" title="<?php print($str_title); ?>"><?php print($str_image);?></a></p>
                                                <p><a class="btn btn-success btn-xs" href="./item_edit.php?pkid=<?php print($rs_list->fields("pkid"))?>&gopage=1" title="<?php print($STR_HOVER_EDIT);?>"><i class="fa fa-pencil"></i></a></p>
                                                <a class="btn btn-danger btn-xs" href="./item_del_p.php?pkid=<?php print($rs_list->fields("pkid"))?>&gopage=1" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE);?>"><i class="fa fa-trash"></i></a></td>
                        </tr>					
                                                <?php $str_news_type = $rs_list->fields("typefornews");
                                                $rs_list->movenext(); $int_cnt = $int_cnt +1;		}  //while end ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td align="center"><input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>">
                                                    <?php print DisplayFormButton("SAVE",0); ?>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        <?php }//else and ?>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                            </div>
                        </div>
                        </div>
    <!-- News list end -->	
    <!--Archive list start-->						
                        <div class="tab-pane <?php print $str_active_archived_tab_link; ?>" id="archived">				
                            <div class="table-responsive">
                                <form name="frm_list_subscriber" method="post" action="#" onClick="return active_tab()">
                                    <table class="table table-striped table-bordered ">
                                        <thead>	
                                            <tr>					
                                                <th width="4%">Sr. #</th>
                                                <th width="10%">Create Date</th>
                                                <th width="">Details</th>
                                                <th width="3%" >Restore</th>	
                                                <th width="5%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if ($rs_list_archived->eof() == true) { ?><tr><td colspan="5" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                                            <?php }  else { $str_news_type=""; $int_cnt=1; while($rs_list_archived->eof()==false) { ?>
                                            <?php if($str_news_type != $rs_list_archived->fields("typefornews")) { ?>
                                            <tr><td colspan="6" align="left" class="alert-warning">
                                                    <h5><b>&nbsp;<?php if(RemoveQuote(MyhtmlEncode($rs_list_archived->fields("typefornews")))=="MEMBER") { print("MEMBER"); } else { print(RemoveQuote(MyhtmlEncode($rs_list_archived->fields("typefornews")))); } ?></b></h5></td></tr>
                                            <?php } ?>
                                            <tr class="<?php print($int_cnt%2); ?>">
                                                <td align="center" valign="middle" ><?php print($int_cnt);?>
                                                    <input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list_archived->fields("pkid"));?>">
                                                </td>
                                                <td align="center" class="text-help"><?php print(DDMMMYYYYFormat($rs_list_archived->fields("date")));?></td>
                                                <td>
                                                    <?php if($rs_list_archived->fields("imagefilenamelarge")!="") { ?>
                                                    <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list_archived->fields("pkid"));?>" rel="thumbnail"><img class="img-responsive" src="<?php print($UPLOAD_IMG_PATH.$rs_list_archived->fields("imagefilenamelarge"));?>"  border="0" align="absmiddle" alt="<?php print $rs_list_archived->fields("imagefilenamelarge")?>" title="<?php print $rs_list_archived->fields("imagefilenamelarge")?>"></a>
                                                    <div class="modal fade f-pop-up-<?php print($rs_list_archived->fields("pkid")); ?> " align="center"  role="dialog" aria-labelledby="" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                                    <img src="<?php print($UPLOAD_IMG_PATH.$rs_list_archived->fields("imagefilenamelarge"));?>" class="img-responsive img-rounded" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image">
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div>															
                                            <?php } ?>
                                            <p></p>			
                                            <div align="justify" class="">
                                            <h4 class=""><b><?php print(MyHtmlEncode($rs_list_archived->fields("heading")));?></b></h4>
                                            <?php if($rs_list_archived->fields("url")!="") { ?>
                                                <span class=""><?php print(DisplayWebSiteURL(trim(MyHtmlEncode($rs_list_archived->fields("url"))),"",trim(strtoupper($rs_list_archived->fields("openinnewwindow"))),"Click to view the website","","",60));?>	</span>
                                            <?php } ?>
                                            <?php if($rs_list_archived->fields("description")!="") {?>
                                                <span class=""><?php print(RemoveQuote($rs_list_archived->fields("description")));?></span><?php }?>
                                            </div>
                                            <?php $str_image=""; $str_class=""; $str_title="";
                                                if(strtoupper($rs_list_archived->fields("visible"))=="YES")
                                                    {   $str_image="<i class='fa fa-eye'></i>";
                                                        $str_class="btn btn-warning btn-xs";
                                                        $str_title=$STR_HOVER_VISIBLE;
                                                    }
                                                    else
                                                    {   $str_image="<i class='fa fa-eye-slash'></i>";
                                                        $str_class="btn btn-default active btn-xs";
                                                        $str_title=$STR_HOVER_INVISIBLE;
                                                    }
                                                    ?>			
                                                    </td>
                                                    <td align="center"><h4 class="nopadding"><a href="./item_archived_p.php?pkid=<?php print($rs_list_archived->fields("pkid"))?>&gopage=2" onClick="return confirm_restore();" title="<?php print ($STR_TITLE_RESTORE_DETAILS); ?>"><i class="fa fa-retweet"></i></a></h4></td>	 
                                                    <td align="center">
                                                        <p><a class="<?php print($str_class); ?>" href="./item_visible_p.php?pkid=<?php print($rs_list_archived->fields("pkid"));?>&gopage=2" title="<?php print($str_title); ?>"><?php print($str_image);?></a></p>
                                                    <p><a class="btn btn-success btn-xs" href="./item_edit.php?pkid=<?php print($rs_list_archived->fields("pkid"))?>&gopage=2" title="<?php print($STR_HOVER_EDIT);?>"><i class="fa fa-pencil"></i></a></p>
                                                    <a class="btn btn-danger btn-xs" href="./item_del_p.php?pkid=<?php print($rs_list_archived->fields("pkid"))?>&gopage=2" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE);?>"><i class="fa fa-trash"></i></a></td>
                                                    </tr>					
                                                    <?php $str_news_type = $rs_list_archived->fields("typefornews");
                                                        $rs_list_archived->movenext(); $int_cnt = $int_cnt +1;	 } //while end
                                                        }//else end ?>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>						
    <!--Archive list end-->
                <?php /*?></div><?php */?>
                    </div>
                </div>
            </div>
        </div>
	<?php include "../../includes/help_for_list.php"; ?>
    <!-- /.container -->											
</div>
<!-- Bootstrap Core CSS -->
<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>

<?php /*?><script src="../includes/thumbnailviewer.js" type="text/javascript"></script><?php */?>
</body></html>
