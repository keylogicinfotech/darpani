<?php
/*
File Name  :- item_edit_p.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_file_system.php";	
include "./item_app_specific.php";
#------------------------------------------------------------------------------------------------
#get post data
$int_pkid="";
if(isset($_POST["hdn_pkid"])==true)
{
    $int_pkid=trim($_POST["hdn_pkid"]);
}	
#To check required parameters are passed properly or not
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}

$int_gopkid="";
if(isset($_POST["hdn_gopage"])==true)
{
    $int_gopkid=trim($_POST["hdn_gopage"]);
}	


$str_heading="";
$str_url="";
$str_description="";
$str_type="";
$dt_day="";
$dt_month="";
$dt_year="";
$str_openurl="YES";
$str_large_image="";

if(isset($_POST["txt_heading"]))
{
    $str_heading = trim($_POST["txt_heading"]);
}
	
if(isset($_POST["txt_newsurl"]))
{
    $str_url = trim($_POST["txt_newsurl"]);
}

if(isset($_POST["ta_desc"]))
{
    $str_description = trim($_POST["ta_desc"]);
}

if(isset($_POST["cbo_news_type"]))
{
    $str_type=trim($_POST["cbo_news_type"]);
}

if(isset($_POST["cbo_day"]))
{
    $dt_day = trim($_POST["cbo_day"]);
}

if(isset($_POST["cbo_year"]))
{
    $dt_year = trim($_POST["cbo_year"]);
}
	
if(isset($_POST["cbo_month"]))
{
    $dt_month = trim($_POST["cbo_month"]);
}

if(isset($_POST["cbo_openurl"]))
{
    $str_openurl = trim($_POST["cbo_openurl"]);
}

if(isset($_FILES['file_news_image']))
{
    $str_large_image=trim($_FILES['file_news_image']['name']);
}

$str_return_page="";
if(isset($_POST["retpage"]))
{
    $str_return_page=trim($_POST["retpage"]);
}
#------------------------------------------------------------------------------------------------
#To check required parameters are passed properly or not

if($dt_day == 0 || $dt_month == 0 || $dt_year == 0)
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&pkid=". $int_pkid."&gopage=".$int_gopkid."&#ptop");
    exit();
}

if(!checkdate($dt_month,$dt_day,$dt_year))
{
    CloseConnection();
    Redirect("item_edit.php?msg=ID&type=E&pkid=". $int_pkid."&gopage=".$int_gopkid."&#ptop");
    exit();
}

if($str_type == "" || $str_type == "0")
{
    CloseConnection();
    Redirect("item_edit.php?msg=NT&type=E&pkid=". $int_pkid."&gopage=".$int_gopkid."&#ptop");
    exit();
}

if($str_heading =="")
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&pkid=". $int_pkid."&gopage=".$int_gopkid."&#ptop");
    exit();
}

if( $str_url!="" )
{
    if( validateURL(strtolower($str_url))==false )
    {
        CloseConnection();
        Redirect("item_edit.php?msg=IU&type=E&pkid=". $int_pkid."&gopage=".$int_gopkid."&#ptop");
        exit();
    }
}

if( $str_large_image!="" )
{
    //here we cannot use directly name of image file as first argument.
    // instead we have to specify $_FILES['filename']['tmp_name'].

    if(ValidateImageExtension($_FILES['file_news_image']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
        CloseConnection();
        Redirect("item_edit.php?msg=IE&type=E&pkid=". $int_pkid."&gopage=".$int_gopkid."&#ptop");
        exit();
    }

/* 	if( CheckFileExtension($str_large_image,$STR_IMG_FILE_TYPE_VALIDATION)==0 )
        {
        CloseConnection();
        Redirect("item_edit.php?msg=IE&type=E&pkid=". $int_pkid."&#ptop");
        exit();
        }
 */}

if($str_description =="")
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&pkid=". $int_pkid."&gopage=".$int_gopkid."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#duplicate checking
$newsdate = "";
$newsdate = $dt_year."-".$dt_month."-".$dt_day;
$str_query_select="";
$str_query_select="SELECT heading FROM ".$STR_DB_TABLE_NAME." WHERE date='".$newsdate."' AND heading='".ReplaceQuote($str_heading)."' AND pkid!=".$int_pkid;
$rs_list_check_duplicate=GetRecordSet($str_query_select);
if($rs_list_check_duplicate->eof()==false)
{
    //CloseConnection();
    //Redirect("item_edit.php?msg=DU&type=E&pkid=". $int_pkid."&heading=".urlencode(RemoveQuote($str_heading))."&#ptop");
    //exit();
}
#----------------------------------------------------------------------------------------------------
#select query to get image details from t_news table

$str_query_select="SELECT imagefilename,imagefilenamelarge FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
$rs_list=GetRecordSet($str_query_select);
if($rs_list->eof() == true)
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&pkid=". $int_pkid."&gopage=".$int_gopkid."&#ptop");
    exit();
}
$str_new_thumb_image="";
$str_new_large_image="";
$str_db_thumb_image=$rs_list->fields("imagefilename");
$str_db_large_image=$rs_list->fields("imagefilenamelarge");
$str_large_path="";
$str_thumb_path="";

#delete old file if exists and new file name is given.
if($str_large_image !="")
{
    if($str_db_large_image != "")
    {
        DeleteFile($UPLOAD_IMG_PATH.$str_db_large_image);
    }
    if($str_db_thumb_image != "")
    {
        DeleteFile($UPLOAD_IMG_PATH.$str_db_thumb_image);
    }

    #upload new file
    $str_new_large_image=GetUniqueFileName()."_l.".getextension($str_large_image);
    $str_new_thumb_image=GetUniqueFileName()."_t.".getextension($str_large_image);

    $str_large_path=trim($UPLOAD_IMG_PATH . $str_new_large_image);
    $str_thumb_path=trim($UPLOAD_IMG_PATH . $str_new_thumb_image);

    UploadFile($_FILES['file_news_image']['tmp_name'],$str_large_path);
    ResizeImage($str_large_path,$INT_IMG_WIDTH);

    SaveAsThumbImage($str_large_path,$str_thumb_path,$INT_IMG_WIDTH_THUMB);
} 
else
{
    $str_new_large_image=$str_db_large_image;
    $str_new_thumb_image=$str_db_thumb_image;
}

#----------------------------------------------------------------------------------------------------
#Update record
$str_query_update="UPDATE " .$STR_DB_TABLE_NAME. " SET date='" .  ReplaceQuote($newsdate) . "',";
$str_query_update=$str_query_update . "heading='" . ReplaceQuote($str_heading) . "',";
$str_query_update=$str_query_update . "imagefilename='" . ReplaceQuote($str_new_thumb_image) . "',";
$str_query_update=$str_query_update . "imagefilenamelarge='" . ReplaceQuote($str_new_large_image) . "',";
$str_query_update=$str_query_update . "url='" . ReplaceQuote($str_url) . "',";
$str_query_update=$str_query_update . "openinnewwindow='" . ReplaceQuote($str_openurl) . "',";
$str_query_update=$str_query_update . "description='" . ReplaceQuote($str_description) . "',";
$str_query_update=$str_query_update . "typefornews='" . ReplaceQuote($str_type) . "'";
$str_query_update=$str_query_update . " where pkid=". $int_pkid;
//print $str_query_update;exit;

ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file

WriteXml();
#------------------------------------------------------------------------------------------------------------
#Close connection and redirect to page	
/*if($str_return_page=="a")
{
    CloseConnection();
    Redirect("item_archived.php?type=S&msg=U&heading=".urlencode(RemoveQuote($str_heading))."&gopage=".$int_gopkid."&#ptop");
    exit();
}*/

CloseConnection();
Redirect("item_list.php?type=S&msg=U&heading=".urlencode(RemoveQuote($str_heading))."&gopage=".$int_gopkid."&#ptop");
exit();

#------------------------------------------------------------------------------------------------------------
?>
