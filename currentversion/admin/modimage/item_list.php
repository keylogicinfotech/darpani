<?php 
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
#----------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME. " $STR_DB_TABLE_NAME_ORDER_BY ";
$rs_list = GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------------
# get Query String Data
$str_mode="";
if (isset($_GET["mode"]))
{
    $str_mode = trim($_GET["mode"]);
}
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
$str_iname = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"])) {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING;break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; 	break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
        case("IE"): $str_message = $STR_MSG_ACTION_INVALID_FILE_EXTENSION; break;		
        case("IED"): $str_message = $STR_MSG_ACTION_INVALID_DOC_EXT; break;				
    }
}
#------------------------------------------------------------------------------------------------
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-12 col-sm-12 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE);?> </h3></div>
    </div><hr> 
        <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <span>
                                <b><a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" title="<?php print($STR_HOVER_ADD); ?>" href="#collapseOne" aria-expanded="false"><?php print($STR_TITLE_ADD); ?> </a></b>
                            </span>
                            <?php print($STR_LINK_HELP); ?>   
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <form name="frm_add" action="item_add_p.php" method="post" onSubmit="return frm_add_validate();" enctype="multipart/form-data">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="form-group">
                                    <label>Upload File</label><span class="text-help-form"> *</span>
                                    <span class="text-help-form"> (<?php print($STR_MSG_IMG_FILE_TYPE);?> | <?php print($STR_MSG_DOC_FILE_TYPE);?>)</span>
                                    <input type="file" name="filename" maxlength="255">
                                </div>
 <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-10">
        <div class="col-md-4"></div><div class="col-md-4"><div id="snackbar">Link copied to clipboard...</div></div><div class="col-md-4"></div>
    </div>
    
    <div class="table-responsive">
        <form name="frm_list" action="" method="post" onSubmit="return frm_list_check_displayorder();">
            <table class="table table-striped table-bordered ">
                <thead>
                    <tr>
                        
                        <?php // print $STR_TABLE_COLUMN_NAME_ALLOW_LOGIN; ?>
                        
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                        <th width="5%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($rs_list->EOF()==true) { ?> 
                        <tr><td colspan="4" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                    <?php } else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
                    <tr>
                        <td align="center" class="align-middle"><?php print($int_cnt); ?><input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>">
                        </td>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                    <td align="left" valign="top">
                                        <table align="left" cellpadding="1" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" align="left">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td valign="top" align="left">  
                                                                <?php if($rs_list->fields("filename")!="") {
                                                                        $file_ext = getextension($rs_list->fields("filename"));
                                                                        $str_host_url01 ="";
                                                                        $str_img_src01 = "";
                                                                        $str_host_url01 = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
                                                                        $str_img_src01 = substr($str_host_url01,0,strpos($str_host_url01,"admin")-1);												
                                                                        $str_img_src01 = $str_img_src01."/".substr(trim($UPLOAD_IMG_PATH),6).$rs_list->fields("filename");
                                                                        if($file_ext == "pdf" || $file_ext == "doc" || $file_ext == "docx" || $file_ext == "odt") { 
                                                                                print "<h4 class='nopadding'><b><a href=".$str_img_src01." class='' target='_blank'>".$rs_list->fields("title")."</a></b></h4>"; 
                                                                    } else { ?>
                                                                        <?php //print($UPLOAD_IMG_PATH);?>
                                                                        <?php //print($rs_list->fields("filename"));?>
                                                                    <a href="#" data-toggle="modal" data-target=".f-pop-up<?php print $rs_list->fields("pkid");?>" rel="thumbnail"><img border="0" alt="Image" src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("filename"));?>" class="img-responsive" title="<?php print $rs_list->fields("title"); ?>" alt="<?php print $STR_TITLE_PAGE_IMG; ?>"></a>
                                                                    <div class="modal fade f-pop-up<?php print $rs_list->fields("pkid");?>" align="center" role="dialog" aria-labelledby="" aria-hidden="true">
                                                                        <div class="modal-dialog modal-lg">
                                                                            <div class="modal-content">
                                                                                <div class="modal-body">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                                                    <img border="0" alt="Image" src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("filename"));?>" class="img-responsive" title="<?php print $rs_list->fields("title"); ?>" alt="<?php print $STR_TITLE_PAGE_IMG; ?>">
                                                                                </div>
                                                                            </div><!-- /.modal-content -->
                                                                        </div><!-- /.modal-dialog -->
                                                                        </div><br/>
                                                                        <b>Image Code:</b> <span class="text-help-form">(<?php print $STR_MSG_IMG_CODE_TEXT; ?>)<br></span>
                                                                    <?php 
                                                                        $str_host_url ="";
                                                                        $str_img_src = "";
                                                                        $str_host_url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
                                                                        $str_img_src = substr($str_host_url,0,strpos($str_host_url,"admin")-1);												
                                                                        $str_img_src = $str_img_src."/".substr(trim($UPLOAD_IMG_PATH),6).$rs_list->fields("filename"); ?>
                                                                        <table width="100%" >
                                                                            <td>
                                                                                <textarea rows="2" class="form-control input-sm" wrap="off" name="ta_image<?php print($int_cnt); ?>" id="ta_image<?php print($int_cnt); ?>" ><img src="<?php print($str_img_src);?>" class="img-responsive"  border="0"  alt="<?php print($STR_SITENAME_WITHOUT_PROTOCOL);?>" title="<?php print($STR_SITENAME_WITHOUT_PROTOCOL);?>" ></textarea></td><td>&nbsp;<a href="javascript: void(0)" title="<?php print $STR_HOVER_SELECT_CODE; ?>" onClick="copyToClipboard('#ta_image<?php print($int_cnt); ?>');myFunction();" class="link">Copy Text</a></td><br/>
                                                                        </table><br/>
                                                                    <?php 
 
                                                                    } 
                                                                } else {  print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); } ?>
                                                                        <b>Link Code:</b> 
                                                                    <span class="text-help-form">(<?php print $STR_MSG_LINK_CODE_TEXT; ?> | <?php print($STR_MSG_LINK_TEXT);?> )</span>
                                                                        <?php $str_host_url ="";
                                                                            $str_img_src = "";
                                                                            $str_host_url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
                                                                            $str_img_src = substr($str_host_url,0,strpos($str_host_url,"admin")-1);												
                                                                            $str_img_src = $str_img_src."/".substr(trim($UPLOAD_IMG_PATH),6).$rs_list->fields("filename"); ?>
                                                                            <table width="100%" border="0">
                                                                                <td>
                                                                                    <textarea rows="2" wrap="off" id="ta_image_link<?php print($int_cnt); ?>" name="ta_image_link<?php print($int_cnt); ?>" class="form-control input-sm" >
<a href="<?php print($str_img_src);?>" border="0" class="" target='_blank'>write your text here</a>
                                                                                    
                                                                                    </textarea>
                                                                                </td>
                                                                                <td>&nbsp;
                                                                                    <a href="javascript: void(0)" title="<?php print $STR_TITLE_HOVER_SELECT_CODE; ?>" onClick="copyToClipboard('#ta_image_link<?php print($int_cnt); ?>');myFunction();" class="">Copy Text</a>
                                                                                </td>
                                                                            </table><br>
                                                                    <?php /* ?><span class="text-help-form">( <?php print($STR_MSG_SELECT_CODE);?> )</span><?php */ ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>												
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>								
                        </td>
                        <td align="center" class="align-middle">
                            <p><a class="btn btn-success btn-xs" href="item_edit.php?pkid=<?php print($rs_list->fields("pkid"));?>" title="<?php print($STR_HOVER_EDIT);?>" ><i class="fa fa-pencil"></i></a></p>
                            <p><a class="btn btn-danger btn-xs" href="item_del_p.php?pkid=<?php print($rs_list->fields("pkid"));?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE);?>"><i class="fa fa-trash"></i></a>	</p>
                        </td>
                    </tr>
		<?php $int_cnt++; $rs_list->MoveNext(); } ?>
		<?php /*?><tr><td colspan="3"></td></tr><?php */?>
		<?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<script>
function copyToClipboard(element) 
{
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}
function myFunction() 
{
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}
</script>
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>

</body></html>