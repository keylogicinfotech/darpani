<?php
/*
	Module Name:- modImage
	File Name  :- img_add_p.php
	Create Date:- 21-Jun-2006
	Intially Create By :- 0023
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "../../includes/validatesession.php";
	include "../../includes/configuration.php";
	include "./img_config.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
	include "../../includes/lib_file_upload.php";
	include "../../includes/lib_image.php";	
#----------------------------------------------------------------------------------------------------
#get post data
$str_large_image="";
$str_image_name="";

if(isset($_FILES['file_large_image']))
{
	$str_large_image=trim($_FILES['file_large_image']['name']);
}	

//if(isset($_FILES['txt_iname']))
//{
//	$str_image_name=trim($_FILES['txt_iname']['name']);
//}	

print($str_large_image);  exit;
#----------------------------------------------------------------------------------------------------
#To check required parameters are passed properly or not
if($str_large_image!="")
{
	if( CheckFileExtension($str_large_image,$STR_IMG_FILE_TYPE_VALIDATION)==0 &&  CheckFileExtension($str_large_image,$STR_DOC_FILE_TYPE_VALIDATION)==0 )
	{
		CloseConnection();
		Redirect("img_edit.php?msg=IE&type=E&#ptop");
		exit();
	}	
}
else
{
	CloseConnection();
	Redirect("img_edit.php?msg=F&type=E&#ptop");
	exit();
}
#----------------------------------------------------------------------------------------------------
#upload image file.
$str_large_file_name="";
if($str_large_image != "")
{
	$str_dir="";
	$str_dir=$UPLOAD_IMAGE_PATH;
	if(!file_exists($str_dir))
	{
		CloseConnection();
		Redirect("img_list.php?msg=F&type=E&#ptop");
		exit();
	}
	
	#upload image file.	
	$str_large_path="";
	
	$str_large_file_name=GetUniqueFileName().".".getextension($str_large_image);
	$str_large_path = trim($str_dir . $str_large_file_name);
	
	UploadFile($_FILES['file_large_image']['tmp_name'],$str_large_path);
	CompressImage($str_large_path, $str_large_path, 60);	
}
#----------------------------------------------------------------------------------------------------
#Insert Record In a t_image table
$str_query_insert = "";
$str_query_insert = "INSERT INTO t_image(imagefilename,imagename) VALUES('" . ReplaceQuote($str_large_file_name) . "','".$str_large_image."')";
//print $str_query_insert; exit;
ExecuteQuery($str_query_insert);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to img_list.php page	
	CloseConnection();
	Redirect("img_list.php?type=S&msg=S&#ptop");
	exit();
#------------------------------------------------------------------------------------------------------------
?>
