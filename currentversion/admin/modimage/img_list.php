<?php
/*
	Module Name:- modImage
	File Name  :- img_list.php
	Create Date:- 24-JAN-2017
	Intially Create By :- 015
	Update History: 
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "../../includes/validatesession.php";
	include "../../includes/configuration.php";
	include "./img_config.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
	include "../../includes/lib_image.php";
	include "../../includes/lib_file_upload.php";
#----------------------------------------------------------------------------------------------------
# Select Query to display list
$str_query_select="select * from t_image order by imagepkid DESC";
$rs_list=GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------------
# get Query String Data
$str_mode="";
if (isset($_GET["mode"]))
{
	$str_mode = trim($_GET["mode"]);
}
#------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
   	$str_type = "";
	$str_message = "";
	$str_iname = "";
#	Get message type.
   		if(isset($_GET["type"]))
		{
			switch(trim($_GET["type"])) {
				case("S"): $str_type = "S"; break;
				case("E"): $str_type = "E"; break;
				case("W"): $str_type = "W"; 	break;
			}
		}
#	Get message text.
		if(isset($_GET["msg"]))
		{
			switch(trim($_GET["msg"]))
			{
				case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING;break;
				case("S"): 	$str_message = $STR_MSG_ACTION_ADD; 	break;
				case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
				case("U"): $str_message = $STR_MSG_ACTION_UPDATE; break;
				case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
				case("IE"): $str_message = $STR_MSG_ACTION_INVALID_FILE_EXTENSION; break;		
				case("IED"): $str_message = $STR_MSG_ACTION_INVALID_DOC_EXT; break;				
			}
		}
#------------------------------------------------------------------------------------------------
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_IMG_PDF_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
	<?php include($STR_ADMIN_HEADER_PATH); ?>
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12 button_space">
			<div class="btn-group" role="group" aria-label="...">
				<?php /*?>
				<a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;Back</a>
				<?php */?>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_IMG_PDF_PAGE);?> </h3></div>
	</div><hr> 
	<?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
	<div class="row">
    	<div class="col-lg-12">
        	<div class="panel-group" id="accordion">
            	<div class="panel panel-default">
                	<div class="panel-heading">
                    	<h4 class="panel-title">
							<div class="row">
								<div class="col-md-12  padding-10">
									<div class="col-md-6 col-sm-6 col-xs-8"><a class="accordion-toggle collapsed link" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?php print($STR_TITLE_FORM_ADD); ?> </a></div>
									<div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>												
								</div>
							</div>
						</h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    	<div class="panel-body">
								<form name="frm_img_add" action="img_add_p.php" method="post" onSubmit="return frm_img_add_validateform();" enctype="multipart/form-data">
									<div class="text-help" align="right"><?php print($STR_MANDATORY); ?></div>
									<div class="form-group">
										<label>Upload Image / PDF</label><span class="text-help"> *</span>
										<span class="text-help"> (<?php print($STR_IMG_FILE_TYPE);?> | <?php print($STR_DOC_FILE_TYPE);?>)</span>
										<input type="file"  name="file_large_image" size="90" maxlength="255" class="clsCatTextBox">
									</div>
									<button type="submit"  class="btn btn-success" title="<?php print($STR_BUTTON_TITLE_FORM_ADD);?>" onClick="return frmAdd_Validate(2);" ><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span> Add</button>&nbsp;
									<input  name="btn_reset" id="btn_reset" title="<?php print($STR_BUTTON_TITLE_FORM_RESET);?>" value="Reset" class="btn btn-danger" type="reset">
								</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="table-responsive">
	 <form name="frm_list" action="bio_order_p.php" method="post" onSubmit="return frm_list_check_displayorder();">
		<table class="table table-striped table-bordered ">
        	<thead>
                <tr>
                	<th width="4%">Sr. #</th>
					<th width="">Details</th>
					<th width="8%">Action</th>
				</tr>
			</thead>
			<tbody>
			<?php if($rs_list->EOF()==true) { ?> <tr><td colspan="4" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
				<?php } else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
				<tr>
                                    <td align="center" style="vertical-align: middle"><?php print($int_cnt); ?><input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("imagepkid"));?>"></td>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> 
								<td align="left" valign="top">
									<table align="left" cellpadding="1" cellspacing="0">
										<tbody>
											<tr>
												<td valign="top" align="left">
													<table border="0" cellpadding="0" cellspacing="0">
														<tr>
															<td valign="top" align="left">  
															<?php if($rs_list->fields("imagefilename")!="") {
																	$file_ext = getextension($rs_list->fields("imagefilename"));
																	$str_host_url01 ="";
																	$str_img_src01 = "";
																	$str_host_url01 = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
																	$str_img_src01 = substr($str_host_url01,0,strpos($str_host_url01,"admin")-1);												
																	$str_img_src01 = $str_img_src01."/".substr(trim($UPLOAD_IMAGE_PATH),6).$rs_list->fields("imagefilename");
																	if($file_ext == "pdf" || $file_ext == "doc") { 
																		print "<h3><a href=".$str_img_src01." class='MenuLink10ptBold' target='_blank'>".$rs_list->fields("imagename")."</a></h3><br/>"; 
																	} else { ?>
																	<?php //print($UPLOAD_IMAGE_PATH);?>
																	<?php //print($rs_list->fields("imagefilename"));?>
																	
																		<img border="0" alt="Image" src="<?php print($UPLOAD_IMAGE_PATH.$rs_list->fields("imagefilename"));?>" class="img-responsive"><br/>
																		<b>Image Code:</b> use this code to display image on this or any other website.<br>
																<?php $str_host_url ="";
																		$str_img_src = "";
																		$str_host_url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
																		$str_img_src = substr($str_host_url,0,strpos($str_host_url,"admin")-1);												
																		$str_img_src = $str_img_src."/".substr(trim($UPLOAD_IMAGE_PATH),6).$rs_list->fields("imagefilename"); ?>
																<table width="100%" >
																	<td>
																	<textarea rows="2" class="form-control input-sm" cols="100" wrap="off" name="ta_image<?php print($int_cnt); ?>" ><img src="<?php print($str_img_src);?>" class="img-responsive"  border="0" alt="<?php print($STR_SITENAME_WITHOUT_PROTOCOL);?>" title="<?php print($STR_SITENAME_WITHOUT_PROTOCOL);?>" ></textarea></td><td>&nbsp;<a href="javascript: void(0)" onClick="return HighlightAll('frm_list.ta_image<?php print($int_cnt); ?>');" >Select Code</a></td><br/>
																</table><br/>
																<?php } } else {  print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); } ?>
																<b>Link Code:</b> use this code to display Image/PDF file as a link on this or any other website.<br>
																<span class="text-help">( <?php print($STR_MSG1);?> )</span>
																<?php $str_host_url ="";
																	$str_img_src = "";
																	$str_host_url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
																	$str_img_src = substr($str_host_url,0,strpos($str_host_url,"admin")-1);												
																	$str_img_src = $str_img_src."/".substr(trim($UPLOAD_IMAGE_PATH),6).$rs_list->fields("imagefilename");						
																?>
															<table width="100%" border="0">
																<td>
																<textarea rows="2" cols="100" wrap="off" name="ta_image_link<?php print($int_cnt); ?>" class="form-control input-sm" ><a href="<?php print($str_img_src);?>" border="0"  target='_blank'>write your text here</a></textarea></td><td>&nbsp;<a href="javascript: void(0)" onClick="return HighlightAll('frm_list.ta_image_link<?php print($int_cnt); ?>');" >Select Code</a></td>
															</table><br>
																<span class="text-help">( <?php print($STR_MSG2);?> )</span>
															</td>
														</tr>
													</table>
												</td>												
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</table>								
					</td>
					<td align="center" style="vertical-align: middle">
						<a class="btn btn-success btn-xs" href="img_edit.php?pkid=<?php print($rs_list->fields("imagepkid"));?>" title="<?php print($STR_BUTTON_TITLE_FORM_EDIT_ICON);?>" ><?php print $STR_LINK_ICON_PATH_EDIT; ?></a>&nbsp;
						<a class="btn btn-danger btn-xs" href="img_del_p.php?pkid=<?php print($rs_list->fields("imagepkid"));?>" onClick="return frm_img_list_confirmdelete();" title="<?php print($STR_BUTTON_TITLE_FORM_DELETE_ICON);?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>	
					</td>
				</tr>
		<?php $int_cnt++; $rs_list->MoveNext(); } ?>
		<?php /*?><tr><td colspan="3"></td></tr><?php */?>
		<?php }  ?>
       	</tbody>
	</table>
	</form>
    </div>
	<?php include "../../includes/help_for_list.php"; ?>
</div>
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/admin.css" rel="stylesheet">
<link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script language="JavaScript" src="../../js/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="./img_list.js" type="text/javascript"></script>
</body></html>