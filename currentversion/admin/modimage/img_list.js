/*
	Module Name:- modImage
	File Name  :- img_list.js
	Create Date:- 21-Jun-2006
	Intially Create By :- 0023
	Update History:
*/
function HighlightAll(theField) 
{
	var tempval=eval("document."+theField)
	tempval.focus() 
	tempval.select()	
}

function frm_img_list_confirmdelete()
{
if(confirm("Are you sure you want to delete this image details?"))
	{
		if(confirm("Confirm Deletion:Click 'Ok' to delete this image details or 'Cancel' to deletion."))
		{
			return true;
		}
	}
	return false;
}

function frm_img_add_validateform()
{
	with(document.frm_img_add)
	{
		
		if(trim(file_large_image.value) != "")
		{
			if(!checkImageDocsExt(trim(file_large_image.value)))
			{
			file_large_image.select();
			file_large_image.focus();
			return false;
			}
		}
		else
		{
			alert("Please select image / pdf file.");
			file_large_image.select();
			file_large_image.focus();
			return false;
		}
	}
	return true;
}
