<?php 
/*
File Name  :- item_add_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_image.php";	
#----------------------------------------------------------------------------------------------------
#get post data
$str_filename="";
//$str_image_name="";
if(isset($_FILES['filename']))
{
    $str_filename=trim($_FILES['filename']['name']);
}	
//if(isset($_FILES['txt_iname']))
//{
//	$str_image_name=trim($_FILES['txt_iname']['name']);
//}	
#----------------------------------------------------------------------------------------------------
#To check required parameters are passed properly or not
if($str_filename!="")
{
    
    if( CheckFileExtension($str_filename,$STR_IMG_FILE_TYPE_VALIDATION)==0 &&  CheckFileExtension($str_filename,$STR_DOC_FILE_TYPE_VALIDATION)==0 )
    {
        CloseConnection();
        Redirect("item_edit.php?msg=IE&type=E&#ptop");
        exit();
    }	
}
else
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#upload file.
$str_large_file_name="";
if($str_filename != "")
{
//    print "error3";exit;
    $str_dir="";
    $str_dir=$UPLOAD_IMG_PATH;
    if(!file_exists($str_dir))
    {
        CloseConnection();
        Redirect("item_list.php?msg=F&type=E&#ptop");
        exit();
    }

    #upload file.	
    $str_large_path="";

    $str_large_file_name=GetUniqueFileName().".".getextension($str_filename);
    $str_large_path = trim($str_dir . $str_large_file_name);
	
    UploadFile($_FILES['filename']['tmp_name'],$str_large_path);	
}
#----------------------------------------------------------------------------------------------------
#Insert Record In table
$str_query_insert = "";
$str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME."(filename,title) VALUES('" . ReplaceQuote($str_large_file_name) . "','".$str_filename."')";
//print $str_query_insert;exit; 
ExecuteQuery($str_query_insert);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to page	
CloseConnection();
Redirect("item_list.php?type=S&msg=S&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------
?>