<?php
/*
	Module Name:- modimage
	File Name  :- img_edit.php
	Create Date:- 05-APRIL-2008
	Intially Create By :- KRA
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "../../includes/validatesession.php";
	include "../../includes/configuration.php";
	include "./img_config.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
	include "../../includes/lib_image.php";
	include "../../includes/lib_file_upload.php";
#getting query string data
	$int_pkid="";
	if(isset($_GET['pkid'])) { $int_pkid=trim($_GET['pkid']); }
	if($int_pkid=="" || $int_pkid<=0 || !is_numeric($int_pkid))
	{
		CloseConnection();
		Redirect("img_list.php?msg=F&type=E");
		exit();
	}
#select query to get datas from t_portfolio table	
	$str_select_query="";
	$str_select_query="SELECT * FROM t_image where imagepkid=".$int_pkid;
	$rs_edit=GetRecordset($str_select_query);
	if($rs_edit->eof())
	{
		CloseConnection();
		Redirect("img_list.php?msg=F&type=E");
		exit();
	}
#------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
   	$str_type="";
	$str_message="";
	$str_title="";
			
#	Get message type.
   		if(isset($_GET['type']))
		{
			switch(trim($_GET['type']))
			{
				case("S"): $str_type = "S"; break;
				case("E"):	$str_type = "E"; break;
				case("W"): $str_type = "W"; 	break;
			}
		}
#	Get message text.
		if(isset($_GET['msg']))
		{
			switch(trim($_GET['msg']))
			{
				case("F"): 	$str_message = $STR_MSG_ACTION_INFO_MISSING; break;
				case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
				case("DEL"): $str_message = $STR_MSG_ACTION_DELETE_IMAGE;break; 
				case("LINK"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;
				case("IE"): $str_message = $STR_MSG_ACTION_INVALID_FILE_EXTENSION; break;
				case("IDE"): $str_message = "Please Select Image or PDF.";	break;
			}
		}		
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_FORM_UPDATE); ?> : <?php print($STR_TITLE_IMG_PDF_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
	<?php include($STR_ADMIN_HEADER_PATH); ?>
	<div class="row">
		<div class="col-lg-3 button_space">
			<div class="btn-group" role="group" aria-label="...">
				<?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;Back</a><?php */?>
				<a href="./img_list.php" class="btn btn-default" title="<?php print($STR_HREF_TITLE_LINK);?> <?php print($STR_TITLE_IMG_PDF_PAGE);?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_IMG_PDF_PAGE);?></a>
			</div>
		</div>
		<div class="col-lg-9" align="right" ><h3><?php print($STR_TITLE_FORM_UPDATE); ?> : <?php print($STR_TITLE_IMG_PDF_PAGE);?></h3></div>
	</div><hr> 
	<?php if($str_type != "" && $str_message != "")	{ print(DisplayMessage(0,$str_message,$str_type)); }	?>
	<div class="row">
    	<div class="col-lg-12">
        	<div class="panel-group" id="accordion">
            	<div class="panel panel-default">
                	<div class="panel-heading">
                    	<h4 class="panel-title">
							<div class="row">
								<div class="col-md-12  padding-10">
									<div class="col-md-6 col-xs-8"> 
										<i class="glyphicon  glyphicon-edit "></i>&nbsp;&nbsp;<?php print($STR_TITLE_FORM_UPDATE); ?> </a>
									</div>
									<div class="col-md-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>												
								</div>
							</div>
						</h4>
                    </div>
                    <div id="collapse1" class="panel-collapse">
						<div class="panel-body">
							<form name="frm_portfolio_edit" action="img_edit_p.php" method="post" onSubmit="return frm_portfolio_edit_validateform()" enctype="multipart/form-data">
								<div class="HelpText" align="right"><?php print($STR_MANDATORY); ?></div>
								<div class="form-group">
									<div>
										<label>Existing Image / PDF</label>
									</div>
									<div align="left" valign="top">
										
										<?php if($rs_edit->fields("imagefilename")!="") { 
												$file_ext = getextension($rs_edit->fields("imagefilename"));
												$str_host_url01 ="";
												$str_img_src01 = "";
												$str_host_url01 = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
												$str_img_src01 = substr($str_host_url01,0,strpos($str_host_url01,"admin")-1);												
												$str_img_src01 = $str_img_src01."/".substr(trim($UPLOAD_IMAGE_PATH),6).$rs_edit->fields("imagefilename");
												if($file_ext == "pdf" || $file_ext == "doc") { print "<h3><a href=".$str_img_src01." class='MenuLink10ptBold' target='_blank'>".$rs_edit->fields("imagename")."</a></h3>"; }
												else { 
												$path=$UPLOAD_IMAGE_PATH.$rs_edit->fields("imagefilename"); ?>
												<img src="<?php print $path; ?>" alt="Image or PDF not uploaded" title="Existing Image or PDF" border="0" class="img-responsive">
									<?php } }//else { print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); } ?>	
									</div>
								</div>
								<div class="form-group">
									<label>Upload New  Image / PDF</label>
									<span class="HelpText">(<?php print($STR_IMG_FILE_TYPE);?> | <?php print($STR_DOC_FILE_TYPE);?>)</span>
									<input type="file" name="file_large_image" maxlength="255" class="clsCatTextBox">
									<input name="hdn_existing_file" type="hidden" id="hdn_existing_file" value="<?php print($rs_edit->fields("imagefilename")); ?>">
								</div>
								<button  type="submit" class="btn btn-success" name="btn_submit_edit" title="<?php print($STR_BUTTON_TITLE_FORM_EDIT);?>" ><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</button>&nbsp;
								<input name="btn_reset" id="btn_reset" title="<?php print($STR_BUTTON_TITLE_FORM_RESET);?>" value="Reset" class="btn btn-danger" type="reset">
								<input type="hidden" name="hdn_pkid" value="<?php print($int_pkid);?>">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <a name="help"></a>
	<?php include "../../includes/help_for_edit.php"; ?>
</div>
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/bootstrap-theme.min.css" rel="stylesheet">
<link href="../../css/admin.css" rel="stylesheet">
<script language="JavaScript" src="../../js/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="./img_edit.js" type="text/javascript"></script>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
</body></html>