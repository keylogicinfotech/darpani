<?php 
/*
File Name  :- item_edit.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_image.php";	
#----------------------------------------------------------------------------------------------------
#Retrieve data using GET / POST method
$int_pkid = 0;
if(isset($_GET['pkid'])) { $int_pkid = trim($_GET['pkid']); }
if($int_pkid == "" || $int_pkid <= 0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
# Select Query to display list
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
$rs_list = GetRecordSet($str_query_select);

if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type="";
$str_message="";
$str_title="";

#Get message type.
if(isset($_GET['type']))
{
    switch(trim($_GET['type']))
    {
        case("S"): $str_type = "S"; break;
        case("E"):	$str_type = "E"; break;
        case("W"): $str_type = "W"; 	break;
    }
}
#Get message text.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("DEL"): $str_message = $STR_MSG_ACTION_DELETE_IMAGE;break; 
        case("LINK"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;
        case("IE"): $str_message = $STR_MSG_ACTION_INVALID_FILE_EXTENSION; break;
        case("IDE"): $str_message = "Please Select Image or PDF.";	break;
    }
}		
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_EDIT); ?> </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 button_space">
            <div class="btn-group" role="group" aria-label="...">
            <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="fa fa-chevron-left "></i>&nbsp;Back</a><?php */?>
                <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE);?></a>
            </div>
	</div>
	<div class="col-md-9" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE);?></h3></div>
    </div><hr> 
    <?php if($str_type != "" && $str_message != "")	{ print(DisplayMessage(0,$str_message,$str_type)); }	?>
    <div class="row padding-10">
    	<div class="col-md-12">
            <div class="panel-group" id="accordion">
            	<div class="panel panel-default">
                    <div class="panel-heading">
                    	<h4 class="panel-title">
                            <i class="fa  fa-edit "></i>&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b> </a>
                            <?php print($STR_LINK_HELP); ?>
			</h4>
                    </div>
                    <div id="collapse1" class="panel-collapse">
			<div class="panel-body">
                            <form name="frm_edit" action="item_edit_p.php" method="post" onSubmit="return frm_edit_validateform()" enctype="multipart/form-data">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Existing File</label>
                                                <div align="left" valign="top">
                                                <?php if($rs_list->fields("filename")!="") { 
                                                    $file_ext = getextension($rs_list->fields("filename"));
                                                    $str_host_url01 ="";
                                                    $str_img_src01 = "";
                                                    $str_host_url01 = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
                                                    $str_img_src01 = substr($str_host_url01,0,strpos($str_host_url01,"admin")-1);                                                                             $str_img_src01 = $str_img_src01."/".substr(trim($UPLOAD_IMG_PATH),6).$rs_list->fields("filename");
                                                    if($file_ext == "pdf" || $file_ext == "doc" || $file_ext == "docx" || $file_ext == "odt") { print "<h4 class='nopadding'><b><a href=".$str_img_src01." target='_blank'>".$rs_list->fields("title")."</a></b></h4>"; }
                                                    else { 
                                                        $str_path = $UPLOAD_IMG_PATH.$rs_list->fields("filename"); ?>
                                                        <a href="#" data-toggle="modal" data-target=".f-pop-up" rel="thumbnail"><img src="<?php print $str_path; ?>" alt="<?php print $rs_list->fields("title"); ?>" title="<?php print $rs_list->fields("title"); ?>" border="0" class="img-responsive"></a>
                                                        <div class="modal fade f-pop-up" align="center" role="dialog" aria-labelledby="" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                                    <img src="<?php print $str_path; ?>" alt="<?php print $rs_list->fields("title"); ?>" title="<?php print $rs_list->fields("title"); ?>" border="0" class="img-responsive">
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                        </div>
                                                <?php } }//else { print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); } ?>	
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Upload File</label>
                                            <span class="text-help-form">(<?php print($STR_MSG_IMG_FILE_TYPE);?> | <?php print($STR_MSG_DOC_FILE_TYPE);?>)</span>
                                                <input type="file" name="filename" maxlength="255">
                                                <input name="hdn_existing_file" type="hidden" id="hdn_existing_file" value="<?php print($rs_list->fields("filename")); ?>" >
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                <input type="hidden" name="hdn_pkid" value="<?php print($int_pkid);?>">
                            </form>
			</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a name="help"></a>
    <?php include "../../includes/help_for_edit.php"; ?>
</div>
<?php include "../../includes/include_files_admin.php"; ?>    
    <?php include($STR_ADMIN_FOOTER_PATH); ?>
<script language="JavaScript" src="./item_edit.js" type="text/javascript"></script>
</body></html>