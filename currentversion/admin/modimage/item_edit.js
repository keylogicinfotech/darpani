
function frm_edit_validateform()
{
    with(document.frm_edit_validateform)
    {
        if(trim(filename.value) == "" && trim(hdn_existing_file.value) == "")
        {
            if(!checkImageDocsExt(trim(filename.value)))
            {
                alert("Please select file.");
                filename.select();
                filename.focus();
                return false;
            }
        }

        if(trim(filename.value) != "")
        {
            if(checkImageDocsExt(trim(filename.value))==false)
            {
                fileimage.select();
                fileimage.focus();
                return false;
            }
        }

    }
    return true;
}

function show_details(Url)
{
    window.open(Url,'Portfolio','left=50,top=20,scrollbars=yes,resizable=yes,width=570,height=490');
    return false;
}

function confirm_delete()
{
    if(confirm("Are you sure you want to delete this detail?"))
    {
        if(confirm("Confirm Deletion: Click 'OK' to delete this detail or click 'Cancel' to cancel deletion."))
        {
            return true;
        }
    }
    return false;
}