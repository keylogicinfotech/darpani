<?php 
/*
File Name  :- item_del_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
//include "../../includes/lib_image.php";	
#----------------------------------------------------------------------------------------------------
#get Query String Data
$int_pkid="";
if(isset($_GET["pkid"])==true)
{
    $int_pkid=trim($_GET["pkid"]);
}	
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#select query to get image details
$str_query_select="";
$str_query_select="SELECT filename FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=". $int_pkid;
$rs_list=GetRecordSet($str_query_select);
$str_large_image=$rs_list->Fields("filename");

if($rs_list->eof()==true)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}

#----------------------------------------------------------------------------------------------------
#Delete Image.
if(trim($str_large_image)!="")
{
    DeleteFile($UPLOAD_IMG_PATH.trim($str_large_image));
}
#----------------------------------------------------------------------------------------------------
#Delete from t_image table
$str_query_update="";
$str_query_update="DELETE FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to page	
CloseConnection();
Redirect("item_list.php?type=S&msg=D&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------
?>
	