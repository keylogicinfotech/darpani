<?php
/*
	Module Name:- modImage
	File Name  :- img_del_p.php
	Create Date:- 21-Jun-2006
	Intially Create By :- 0023
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "../../includes/validatesession.php";
	include "../../includes/configuration.php";
	include "./img_config.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
	include "../../includes/lib_file_system.php";
#----------------------------------------------------------------------------------------------------
#get Query String Data
$int_pkid="";
if(isset($_GET["pkid"])==true)
{
	$int_pkid=trim($_GET["pkid"]);
}	
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
	CloseConnection();
	Redirect("img_list.php?msg=F&type=E&#ptop");
	exit();
}
#----------------------------------------------------------------------------------------------------
#select query to get image details
$str_query_select="";
$str_query_select="SELECT imagefilename FROM t_image WHERE imagepkid=". $int_pkid;
$rs_delete=GetRecordSet($str_query_select);
$str_large_image=$rs_delete->Fields("imagefilename");

if($rs_delete->eof()==true)
{
	CloseConnection();
	Redirect("img_list.php?msg=F&type=E&#ptop");
	exit();
}

#----------------------------------------------------------------------------------------------------
#Delete Image.
if(trim($str_large_image)!="")
{
	DeleteFile($UPLOAD_IMAGE_PATH.trim($str_large_image));
}
#----------------------------------------------------------------------------------------------------
#Delete from t_image table
$str_query_update="";
$str_query_update="DELETE FROM t_image WHERE imagepkid=".$int_pkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to img_list.php page	
	CloseConnection();
	Redirect("img_list.php?type=S&msg=D&#ptop");
	exit();
#------------------------------------------------------------------------------------------------------------
?>
	