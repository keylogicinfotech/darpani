/*
	Module Name:- modportfolio
	File Name  :- pf_edit.js
	Create Date:- 17-FEB-2006
	Intially Create By :- 0022
	Update History:
*/

function frm_portfolio_edit_validateform()
{
	with(document.frm_portfolio_edit)
	{	
	
		/*if(trim(file_large_image.value) != "" && trim(hdn_existing_file.value) != "")
		{
			if(!checkImageDocsExt(trim(file_large_image.value)))
			{
			file_large_image.select();
			file_large_image.focus();
			return false;
			}
		}
		else
		{
			alert("Please select image / pdf file.");
			file_large_image.select();
			file_large_image.focus();
			return false;
		}*/
		
		if(trim(file_large_image.value) == "" && trim(hdn_existing_file.value) == "")
		{
			if(!checkImageDocsExt(trim(file_large_image.value)))
			{
				alert("Please select image / pdf file.");
				file_large_image.select();
				file_large_image.focus();
				return false;
			}
		}
		
		if(trim(file_large_image.value) != "")
		{
			if(checkImageDocsExt(trim(file_large_image.value))==false)
			{
				fileimage.select();
				fileimage.focus();
				return false;
			}
		}
		
	}
	return true;
}

function show_details(Url)
{
		window.open(Url,'Portfolio','left=50,top=20,scrollbars=yes,resizable=yes,width=570,height=490');
		return false;
}

function confirm_delete()
{
	if(confirm("Are you sure you want to delete this image?"))
	{
		if(confirm("Confirm Deletion: Click 'OK' to delete this image or click 'Cancel' to cancel deletion."))
		{
			return true;
		}
	}
	return false;
}