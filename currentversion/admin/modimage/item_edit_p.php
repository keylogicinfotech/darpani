<?php 
/*
File Name  :- item_edit_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_image.php";	
include "../../includes/lib_file_system.php";
#----------------------------------------------------------------------------------------------------
#get post data
$str_large_image="";
if(isset($_FILES['filename']))
{
    $str_large_image=trim($_FILES['filename']['name']);
}
if(isset($_POST['hdn_pkid']))
{
    $int_pkid=trim($_POST['hdn_pkid']);
}
$str_existing_file="";
if(isset($_POST['hdn_existing_file']))
{
    $str_existing_file=trim($_POST['hdn_existing_file']);
}	
//print("original uploaded image name: ". $str_large_image . "<br>"); print("PKID of image in database: ". $int_pkid . "<br>"); exit;
#----------------------------------------------------------------------------------------------------
#To check required parameters are passed properly or not
if($str_large_image!="")
{
    if( CheckFileExtension($str_large_image,$STR_IMG_FILE_TYPE_VALIDATION)==0 &&  CheckFileExtension($str_large_image,$STR_DOC_FILE_TYPE_VALIDATION)==0 )
    {
        CloseConnection();
        Redirect("item_edit.php?msg=IE&type=E&#ptop");
        exit();
    }	
}
/*else
{
    CloseConnection();
    Redirect("img_edit.php?msg=F&type=E&#ptop");
    exit();
}


if($str_image!="" )
{
    if(ValidateImageExtension($_FILES['fileimage']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
            CloseConnection();
            Redirect("cm_home_image_edit.php?msg=I&type=E");
            exit();
    }
}*/
if( ($str_existing_file) == "" && ($str_large_image) == "")
{
    CloseConnection();
    Redirect("item_edit.php?msg=IDE&type=E&#ptop");
    exit();
}
//print $str_large_image; exit;
#--------------------------------------------------------------------------------------------------------------------------------------
#upload image
$str_large_file_name="";
$str_select="";

$str_query_select="SELECT filename,title FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
$rs_list=GetRecordSet($str_query_select);

if($rs_list->eof())
{
	CloseConnection();
	Redirect("item_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid));
	exit();
}



$str_large_file_name = "";
$str_large_file_name = $rs_list->fields("filename");
//$str_large_image= $rs_list->fields("imagename");
//print $str_large_file_name; exit;
//print ("Image name in mdm folder: ". $str_large_file_name . "<br>"); print ("image path in mdm folder: ". $UPLOAD_IMG_PATH . "<br>"); exit;

if($str_large_image!="")
{
    #delete old file
    if($str_large_file_name!="")
    {
        DeleteFile($UPLOAD_IMG_PATH.trim($str_large_file_name));
    }
    //print ("<br>Image name in mdm folder After Delete: ". $str_large_file_name . "<br><br>");  exit;

    $str_large_file_name=GetUniqueFileName().".".getextension($str_large_image);
    $str_large_path = trim($UPLOAD_IMG_PATH.$str_large_file_name);
    //print ("Fianl Image Path: " . $str_large_path . "");
    UploadFile($_FILES['filename']['tmp_name'],$str_large_path);
    CompressImage($str_large_path, $str_large_path, 60);	
}
else
{
    $str_large_image = $rs_list->fields("title");
}
	
/*if($str_large_image == ""){
		$str_large_image=$rs_list->fields("filename");
}*/
#---------------------------------------------------------------------------------------
#Insert Record In  table
//$str_query_insert = "";
//$str_query_insert = "insert into t_image(imagefilename)";
//$str_query_insert .= " values('" . ReplaceQuote($str_large_file_name) . "')";
//ExecuteQuery($str_query_insert);

#update query in t_image table
// IMAGE PKID & NAME WILL SAME AFTER UPDATING EXISTING IMAGE SO NO NEED TO FIRE UPDATE QUERY

//$str_update="";
//$str_update="update t_image set imagefilename='".ReplaceQuote($str_large_file_name)."',";

// ExecuteQuery($str_update);

#update query to set values
$str_query_update = "UPDATE " .$STR_DB_TABLE_NAME. " SET filename='" . ReplaceQuote($str_large_file_name) . "',";
$str_query_update .= "title='" . $str_large_image. "' ";
$str_query_update .= " WHERE pkid=". $int_pkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to page	
CloseConnection();
Redirect("item_list.php?type=S&msg=U&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------
?>
