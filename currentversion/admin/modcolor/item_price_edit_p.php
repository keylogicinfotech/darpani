<?php
/*
File Name  :- item_cms_p.php
Create Date:- MARCH2019
Intially Create By :- 0014
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#------------------------------------------------------------------------------
# get all the passed values
$int_groupchatprice=0;
$int_privatechatprice=0;
$int_freechattime=0;		
if(isset($_POST['txt_groupchatprice']))
{
    $int_groupchatprice=trim($_POST['txt_groupchatprice']);
}
if(isset($_POST['txt_privatechatprice']))
{
    $int_privatechatprice=trim($_POST['txt_privatechatprice']);
}
if(isset($_POST['txt_freechattime']))
{
    $int_freechattime=trim($_POST['txt_freechattime']);
}

$flag_allowguest_chat=0;
if(isset($_POST["cbo_allowguest_chat"]))
{ $flag_allowguest_chat=trim($_POST["cbo_allowguest_chat"]); }

#------------------------------------------------------------------------------
if($int_groupchatprice=="" || $int_groupchatprice<=0 || $int_privatechatprice=="" || $int_privatechatprice<=0 || $int_freechattime=="" || $int_freechattime<=0 )
{
    CloseConnection();
    Redirect("item_price_edit.php?msg=F&type=E&#ptop");
    exit();
}
#------------------------------------------------------------------------------
#update query to set values in cm_joinnow table
$str_query_update="UPDATE " .$STR_DB_TABLE_NAME_PRICE. " SET groupchatprice=".ReplaceQuote($int_groupchatprice).",privatechatprice=".ReplaceQuote($int_privatechatprice).",freechattime=".ReplaceQuote($int_freechattime).",allowguests=".$flag_allowguest_chat;
//print $str_query_update;exit;
ExecuteQuery($str_query_update);
#------------------------------------------------------------------------------
CloseConnection();
Redirect("item_price_edit.php?msg=E&type=S&#ptop");
exit();
?>