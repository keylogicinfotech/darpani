<?php
/*
File Name  :- item_cms.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------
# Select query
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME_PRICE. " ";
$rs_list = GetRecordSet($str_query_select); 
#----------------------------------------------------------------------------------------------------------------------------------------------------
# filter variables
$int_page="";
$int_cat_pkid="";
$str_key="";
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#get filter data
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{
    $int_cat_pkid=trim($_GET["catid"]);
}
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{
    $str_key=trim($_GET["key"]);
}
#get paging data
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"])==true && trim($_GET["PagePosition"])>0)
{
    $int_page= $_GET["PagePosition"];
}
else
{
    $int_page=1;
}
$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_key."&PagePosition=".$int_page;
#--------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Diplay message.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("E"): $str_message = $STR_MSG_ACTION_EDIT; break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?>  : <?php print($STR_TITLE_PAGE_PRICE); ?></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body onLoad="myFunction()">
<!-- Page Content -->

<div class="container">
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-6 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <a href="./item_list.php?<?php print($str_filter); ?>" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
                <a href="./item_cms.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)">
                    <i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)
                </a> 
            </div>
        </div>
        <div class="col-md-6" align="right" ><h3><?php print($STR_TITLE_PAGE_PRICE); ?></h3></div>
    </div>
    <hr>
        <?php if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); } ?>
        <div class="row padding-10">
            <a name="ptop"></a>
            <div class="col-md-12">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <i class="fa  fa-edit "></i>&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b>
                                <?php print($STR_LINK_HELP); ?>
                            </h4>
                        </div>
                        <div id="collapse01" class="panel-collapse collapse in">
                            <form name="frm_edit" action="item_price_edit_p.php?&#ptop"  method="post">
                                <div class="panel-body">
                                    <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>

                                    <?php /* ?>      <div class="form-group">
                                              <label>Profile Page Button Description </label>
                                              <input type="text" id="ta_bdesc" name="ta_bdesc" value="<?php print(RemoveQuote($rs_list->fields("buttondesc"))); ?>" rows="10" cols="90" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>">
                                          </div> <?php */ ?>


                                     <?php /* ?>       <label>Description </label>
                                            <textarea id="ta_ldesc" name="ta_ldesc"  class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>"><?php print(RemoveQuote($rs_list->fields("description"))); ?></textarea>
                                        </div> <?php */ ?>

                                        <div class="form-group">
                                            <label>Group Chat Price</label><span class="text-help-form"> *</span>
                                            <span class="text-help-form"> (<?php print($STR_MSG_HELP_PRICE);?>)</span>
                                            <input id="txt_groupchatprice" name="txt_groupchatprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE; ?>" value="<?php print(RemoveQuote($rs_list->fields("groupchatprice"))); ?>">
                                        </div>

                                        <div class="form-group">
                                            <label>Private Chat Price</label><span class="text-help-form"> *</span>
                                            <span class="text-help-form"> (<?php print($STR_MSG_HELP_PRICE);?>)</span>
                                            <input id="txt_privatechatprice" name="txt_privatechatprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE; ?>" value="<?php print(RemoveQuote($rs_list->fields("privatechatprice"))); ?>" >
                                        </div>
                                    
                                        <div class="form-group">
                                            <label>Free Chat Time for Visitor in 1 day</label><span class="text-help-form"> *</span>
                                             <span class="text-help-form"> (<?php print($STR_MSG_HELP_TIME);?>)</span>
                                            <input id="txt_freechattime" name="txt_freechattime" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE; ?>" value="<?php print(RemoveQuote($rs_list->fields("freechattime"))); ?>" >
                                        </div>
                                    
                                        <div class="form-group">
                                            <label>Enable Guest Room:</label>
                                            <span class="text-help-form"> (<?php print($STR_MSG_HELP_ALLOW_GUEST);?>)</span>
                                            <select name="cbo_allowguest_chat" class="form-control input-sm" >
                                                <option value="1" <?php print(CheckSelected(1,$rs_list->fields("allowguests"))); ?>>YES</option>
                                                <option value="0" <?php print(CheckSelected(0,$rs_list->fields("allowguests"))); ?>>NO</option>
                                            </select>
<!--                                            <select name="cbo_allowguest_chat" class="form-control input-sm"><option value="YES" <?php // print(CheckSelected("YES",$str_visible));?>>YES</option><option value="NO" <?php // print(CheckSelected("NO",$str_visible)); ?>>NO</option></select>-->
                                        </div>

                                        <input type="hidden" name="hdn_pkid" value="<?php print($rs_list->fields("pkid")); ?>" >
                                        <?php print DisplayFormButton("EDIT",0); ?>&nbsp;<?php print DisplayFormButton("RESET",0); ?>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>
        <?php include "../../includes/help_for_edit.php"; ?>
        </div>
</div>
    <!-- /.container -->											

<?php include "../../includes/include_files_admin.php"; ?>
    <?php include($STR_ADMIN_FOOTER_PATH); ?>
<!--<script language="JavaScript" src="./item_cms.js" type="text/javascript"></script>-->
<script language="javascript" type="text/javascript">
function myFunction() {
    ta_desc.focus();
}
</script>
<script type="text/javascript" src="../includes/richetxteditor.js"></script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('txt_groupchatprice');
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('txt_privatechatprice');
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('txt_freechattime');
//        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_photoid_desc');
//        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_autosign_desc');
  });
	// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
	// convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('txt_groupchatprice');
             new nicEditor({fullPanel : true}).panelInstance('txt_privatechatprice');
             new nicEditor({fullPanel : true}).panelInstance('txt_freechattime');
//             new nicEditor({fullPanel : true}).panelInstance('ta_photoid_desc');
//             new nicEditor({fullPanel : true}).panelInstance('ta_autosign_desc');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>
</body></html>
