<?php
/*
File Name  :- item_cms.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------
# Select query
//$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME. " ";
//$rs_list = GetRecordSet($str_query_select); 
#----------------------------------------------------------------------------------------------------------------------------------------------------
# filter variables
$int_page="";
$int_cat_pkid="";
$str_key="";
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#get filter data
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{
    $int_cat_pkid=trim($_GET["catid"]);
}
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{
    $str_key=trim($_GET["key"]);
}
#get paging data
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"])==true && trim($_GET["PagePosition"])>0)
{
    $int_page= $_GET["PagePosition"];
}
else
{
    $int_page=1;
}
$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_key."&PagePosition=".$int_page;
#--------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Diplay message.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("E"): $str_message = $STR_MSG_ACTION_EDIT; break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?>  : <?php print($STR_TITLE_PAGE); ?></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body onLoad="myFunction()">
<!-- Page Content -->

<div class="container">
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-6 button_space">
            <div class="btn-group" role="group" aria-label="...">
               <?php /* ?> <a href="./item_list.php?<?php print($str_filter); ?>" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a><?php */ ?>
            </div>
        </div>
        <div class="col-md-6" align="right" ><h3>Cam Software Download and Use Instructions </h3></div>
    </div>
    <hr>
        <?php if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); } ?>
        <div class="row padding-10">
            <a name="ptop"></a>
            <div class="col-md-12">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <i class="fa fa-video-camera"></i>&nbsp;<b>JustCamIt - Cam Software Download and Use Instructions </b>
                                <?php print($STR_LINK_HELP); ?>
                            </h4>
                        </div>
                        <div id="collapse01" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <h4><b>Your Computer / System Requirements</b></h4>
                                <p>For our software to work properly you will need a recent multimedia computer system with a 1.5 Ghz processor or faster running Windows 2000/XP. Also needed are a video capture device or firewire (iLink) camcorder, a working sound card, and broadband Internet connection such as cable or DSL.</p>
                                <hr/>
                                <h4><b>JustCamIt Installation and Login</b></h4>
                                <p>Install the downloaded setup file of JustCamIt on your computer by doing double click on setup file and follow the simple instructions given during setup.<br/>

After complete installation, double click on JustCamIt icon either from Desktop or from Start Menu, click "Login->Login To Server" from top navigation menu and enter following details that is provided to you by RT Web Designs.</p>
                                <ul>
                                    <li><strong>Server :</strong> production1</li>
                                    <li><strong>Account ID :</strong> 224142</li>
                                    <li><strong>Username :</strong> Your Login ID / User ID</li>
                                    <li><strong>Password :</strong> Your Password</li>
                                </ul>
                                <a href="http://www.justcamit.com/download.php" title="Download JustCamIt Software" class="btn btn-success" target="blank"><i class="fa fa-download"></i>&nbsp;<b>Download JustCamIt Software</b></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>
    <?php include "../../includes/help_for_edit.php"; ?>
    </div>
    
    <!-- /.container -->											

<?php include "../../includes/include_files_admin.php"; ?>
    <?php include($STR_ADMIN_FOOTER_PATH); ?>
<!--<script language="JavaScript" src="./item_cms.js" type="text/javascript"></script>-->
<script language="javascript" type="text/javascript">
function myFunction() {
    ta_desc.focus();
}
</script>
<script type="text/javascript" src="../includes/richetxteditor.js"></script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('txt_groupchatprice');
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('txt_privatechatprice');
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('txt_freechattime');
//        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_photoid_desc');
//        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_autosign_desc');
  });
	// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
	// convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('txt_groupchatprice');
             new nicEditor({fullPanel : true}).panelInstance('txt_privatechatprice');
             new nicEditor({fullPanel : true}).panelInstance('txt_freechattime');
//             new nicEditor({fullPanel : true}).panelInstance('ta_photoid_desc');
//             new nicEditor({fullPanel : true}).panelInstance('ta_autosign_desc');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>
</body></html>
