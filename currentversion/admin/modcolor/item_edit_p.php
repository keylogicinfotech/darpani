<?php
/*
File Name  :- item_edit_p.php
Create Date:- 18-JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_file_system.php";
include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Initializing varialbles
$int_pkid="";
$str_sub="";
$str_desc="";
$str_image="";

#Get values of all passed GET / POST variables
if(isset($_POST['hdn_pkid']))
{
    $int_pkid=trim($_POST['hdn_pkid']);
}
if($int_pkid=="" || $int_pkid<0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
if(isset($_POST['txt_title']))
{
    $str_sub=trim($_POST['txt_title']);
}
if(isset($_POST['ta_desc']))
{
    $str_desc=trim($_POST['ta_desc']);
}
if(isset($_POST['txt_cmykcode']))
{
    $str_code=trim($_POST['txt_cmykcode']);
}
if(isset($_FILES['fileimage']))
{
    $str_image=trim($_FILES['fileimage']['name']);
}
#----------------------------------------------------------------------------------------------------
#Check all validation
if($str_sub=="" )
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid));
    exit();
}
//if($str_image!="" )
//{
//    #Here we cannot use directly name of image file as first argument. Instead we have to specify $_FILES['filename']['tmp_name'].
//    if(ValidateImageExtension($_FILES['fileimage']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
//    {
//        CloseConnection();
//        Redirect("item_edit.php?msg=I&type=E&pkid=".urlencode($int_pkid));
//        exit();
//    }
///* if(CheckFileExtension($str_image,$STR_IMG_FILE_TYPE_VALIDATION)==0 )
//    {
//            CloseConnection();
//            Redirect("item_edit.php?msg=I&type=E&pkid=".urlencode($int_pkid));
//            exit();
//    }*/
//}	
#----------------------------------------------------------------------------------------------------	
//$str_query_select="";
//$str_query_select="SELECT imagefilename FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
//$rs_list=GetRecordset($str_query_select);
//if($rs_list->eof())
//{
//    CloseConnection();
//    Redirect("item_edit.php?msg=I&type=E");
//    exit();
//}
//
//$str_thumb_file_name="";
//$str_thumb_path="";
//$str_thumb_file_name=$rs_list->fields("imagefilename");
//if($str_image!="")
//{
//    #Delete old image
//    if($str_thumb_file_name!="")
//    {
//        DeleteFile($UPLOAD_IMG_PATH.trim($str_thumb_file_name));
//    }
//    #Upload new image
//    $str_thumb_file_name=GetUniqueFileName()."_t.".getextension($str_image);
//    $str_thumb_path = trim($UPLOAD_IMG_PATH."/".$str_thumb_file_name);
//    UploadFile($_FILES['fileimage']['tmp_name'],$str_thumb_path);
//    //ResizeImage($str_thumb_path,$INT_IMG_THUMB_WIDTH);
//}	
#----------------------------------------------------------------------------------------------------
#Update query in table
$str_query_update="UPDATE ".$STR_DB_TABLE_NAME." SET title='".ReplaceQuote($str_sub)."',";
//$str_query_update.="description='".ReplaceQuote($str_desc)."',";
$str_query_update.="cmyk_code='".ReplaceQuote($str_code)."'";
//$str_query_update.="imagefilename='".ReplaceQuote($str_thumb_file_name)."'";
$str_query_update.=" WHERE pkid=". $int_pkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=U&type=S&");
exit();
#----------------------------------------------------------------------------------------------------
?>
