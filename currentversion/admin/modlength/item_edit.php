<?php
/*
File Name  :- item_edit.php
Create Date:- 18-JAN-2019
Intially Create By :- 0022
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
#Getting query string data
$int_pkid="";
if(isset($_GET['pkid'])) { $int_pkid=trim($_GET['pkid']); }
if($int_pkid=="" || $int_pkid<=0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Select query to get all details from table	
$str_query_select="";
$str_query_select="SELECT * FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
$rs_list=GetRecordset($str_query_select);
if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type="";
$str_message="";
$str_title="";
if(isset($_GET['tit'])) { $str_title=trim($_GET['tit']); }
#Get message type.
if(isset($_GET['type']))
{
    switch(trim($_GET['type']))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Get message text.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("DEL"): $str_message = $STR_MSG_ACTION_DELETE_IMAGE; break;
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_EDIT); ?> </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE);?></a>
            </div>
        </div>
        <div class="col-md-9 col-sm-6 col-xs-12 " align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE);?></h3>       </div>
    </div>
    <hr> 
    <?php if($str_type != "" && $str_message != "")	{ print(DisplayMessage(0,$str_message,$str_type)); }	?>
    <div class="row padding-10">
    <div class="col-md-12">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <h4 class="panel-title">
                        <span>
                            <i class="fa fa-edit "></i>&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b> 
                        </span>
                         <?php print($STR_LINK_HELP); ?>
                    </h4>
                </div>
            <div id="collapse1" class="panel-collapse">
                <div class="panel-body">
                    <form name="frm_edit" action="./item_edit_p.php" method="post" onSubmit="return frm_edit_validateform()" enctype="multipart/form-data">
                        <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                             <div class="form-group"><label>Title</label><span class="text-help-form"> *</span>
                               <input type="text" name="txt_title" id="txt_title" class="form-control input-sm" maxlength="255" value="<?php print(MyHtmlEncode($rs_list->fields("title"))); ?>" placeholder="<?php print $STR_PLACEHOLDER_TITLE ?>">
                             </div>
                        <div class="form-group"><label>Description</label><span class="text-help-form"> </span><strong></strong>
                            <textarea name="ta_desc" id="ta_desc" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>"><?php print(RemoveQuote($rs_list->fields("description"))); ?></textarea>
                        </div>
                        
                        <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                        <input type="hidden" name="hdn_pkid" value="<?php print($int_pkid);?>">
            	</form>
            </div>
        </div>
        </div>
        </div>
        </div>
    </div>
    
    <?php include "../../includes/help_for_edit.php"; ?>
</div>
<script language="JavaScript" src="./item_edit.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>
