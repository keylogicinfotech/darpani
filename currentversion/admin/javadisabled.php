<?php 
	include "../includes/validatesessionroot.php";
?>
<script language="javascript">
	window.location="./admin_broadcast.php";
</script>
<html>
<head>
<title>:: Javascript Error ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="../css/admin.css" rel="stylesheet" type="text/css">
<body>
<NOSCRIPT>
<table width="1008" border="0" cellpadding="0" cellspacing="0" align="center">
 <tr> 
  <td><?php include "../includes/adminheaderroot.php";?></td>
 </tr>
</table>
<a name="ptop"></a>
<table width="1008" align="center" cellpadding="0" cellspacing="0" height="465">
 <tr> 
  <td background="../images/tablebg.gif" align="center" valign="top"><br>
   <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr> 
     <td align="center" height="25" class="PageHeader">Javascript Error </td>
    </tr>
	<tr> 
     <td height="2" align="right" class="TableHeader8ptBold"></td>
   </tr>
   </table>
   <br>
   <a name="ptop"></a>
      <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr> 
          <td height="20" align="center" valign="middle" class="ErrorMessage"> 
            Your browser does not  support JavaScript currently.</td>
        </tr>
      </table><br>
  <table width="99%" border="0" cellpadding="2" cellspacing="0" align="center">
   <tr  class="TableHeader8ptBold"> 
    <td height="20" >
	 <table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr  class="TableHeader10ptBold"> 
       <td >Help</td>
       <td align="right" >&nbsp;</td>
      </tr>
     </table>
	</td>
   </tr>
   <tr valign="top"> 
    <td>
	 <table width="100%" border="0" cellpadding="2" cellspacing="0">
      <tr> 
       <td width="2%" valign="baseline" class="HelpText">&#8226;</td>
       <td width="98%" valign="baseline" class="HelpText"><p>If your are using Netscape 2 or later then you can enable JavaScript.<br> 
           <strong>Version 2 or 3:</strong><br> 
  Choose Options &gt;&gt;Network Preferences &gt;&gt; Choose the Languages tab &gt;&gt; Click Enable Javascript &gt;&gt; Press OK.<br>
             <strong>Version 4:</strong><br> 
             Choose Edit &gt;&gt; Preferences &gt;&gt; Advanced &gt;&gt; Click Enable JavaScript &gt;&gt; Press OK.</p>
         </td>
      </tr>
	  <tr> 
       <td valign="baseline" class="HelpText">&#8226;</td>
       <td valign="baseline" class="HelpText">With Internet Explorer, please apply following steps:<br>
         Go to Tools Menu &gt;&gt; Select Internet Options &gt;&gt; Choose Security Tab &gt;&gt; Click on Custom Level &gt;&gt; Scripting &gt;&gt;<br>
          Select Enable option in Active Scripting &gt;&gt; Press Ok &gt;&gt; Press Yes on Alert message.</td>
      </tr>
	  <tr> 
       <td valign="baseline" class="HelpText">&#8226;</td>
       <td valign="baseline" class="HelpText">With Mozilla Firefox, please apply following steps:<br>
         Go to Tools Menu &gt;&gt; Select Options &gt;&gt; Choose Content Tab &gt;&gt; Check Enable Javascript checkbox &gt;&gt; Press OK.</td>
      </tr>
	  <tr> 
       <td valign="baseline" class="HelpText">&#8226;</td>
       <td valign="baseline" class="HelpText">Please 'Refresh / Reload' this page after perform setting.
       </td>
      </tr>
	  <tr> 
       <td height="50" colspan="2" align="right" valign="bottom" class="HelpText"><a href="./admin_broadcast.php" class="MenuLink8ptBold"><font size="4">Click Here to Continue without enabling JavaScript...</font></a></td>
      </tr>
     </table>
	</td>
   </tr>
  </table>
  <br>
 </td>
</tr>
</table>
</NOSCRIPT>	
</body>
</html>



