<?php
/*
Module Name:- modstore
File Name  :- item_cat_add_p.php
Create Date:- 04-FEB-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
include "./item_app_specific.php";	
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_size = "";
$str_type = "";
$str_visible = "YES";
if (isset($_POST["cbo_visible"]))
{
    $str_visible = trim($_POST["cbo_visible"]);
}	
if (isset($_POST["txt_value1"]))
{
    $int_value1 = trim($_POST["txt_value1"]);
}
if (isset($_POST["txt_value2"]))
{
    $int_value2 = trim($_POST["txt_value2"]);
}
if (isset($_POST["txt_value3"]))
{
    $int_value3 = trim($_POST["txt_value3"]);
}
if (isset($_POST["txt_value4"]))
{
    $int_value4 = trim($_POST["txt_value4"]);
}
//print $str_type;exit;
#----------------------------------------------------------------------------------------------------
#Redirect URL

$str_redirect = "";
$str_redirect .= "&visible=". urlencode(RemoveQuote($str_visible));
$str_redirect .= "&size=". urlencode(RemoveQuote($int_value1));

#----------------------------------------------------------------------------------------------------
#To check required parameters are passed properly or not

if($int_value1 == "" || $str_visible == "")
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E". $str_redirect."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Duplication Check
/*$str_query_select = "";
$str_query_select = "SELECT title from ".$STR_DB_TABLE_NAME." WHERE title= '" . ReplaceQuote($int_size) . "'";
$rs_list_check_duplicate=GetRecordSet($str_query_select);

if(!$rs_list_check_duplicate->eof())
{
    CloseConnection();
    Redirect("item_list.php?msg=DU&type=E&". $str_redirect."&#ptop");
    exit();
}*/
#----------------------------------------------------------------------------------------------------
#Insert Record In a t_workskill_category table
$int_max_displayorder = 0;
$int_max_displayorder = GetMaxValue($STR_DB_TABLE_NAME,"displayorder");	
$str_query_insert = "";
$str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME." (value1,value2,value3,value4,visible,displayorder)";
$str_query_insert .= " VALUES('" . ReplaceQuote($int_value1) . "',";
$str_query_insert .= "'" . ReplaceQuote($int_value2) . "',";
$str_query_insert .= "'" . ReplaceQuote($int_value3) . "',";
$str_query_insert .= "'" . ReplaceQuote($int_value4) . "',";
$str_query_insert .= "'" . ReplaceQuote($str_visible) . "',".$int_max_displayorder.")";
//print $str_query_insert;exit;
ExecuteQuery($str_query_insert);
#----------------------------------------------------------------------------------------------------------------------------------------	
# Writing data of table into XML file	
WriteXml();
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_list.php?type=S&msg=S&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------

?>
