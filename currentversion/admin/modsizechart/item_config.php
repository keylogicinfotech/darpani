<?php 
$STR_TITLE_PAGE = "Size Chart List";

$UPLOAD_IMG_PATH = "";

# For Main List
global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "t_sizechart"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY displayorder DESC";

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "cms_size_chart";
# XML Path
global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/sizechart.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/sizechart_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";

?>
