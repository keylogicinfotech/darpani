<?php
/*
Module Name:- modstore
File Name  :- item_cat_visible_p.php
Create Date:- 04-FEB-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_GET); exit;
$int_pkid = 0;
if(isset($_GET["pkid"]) == true)
{
    $int_pkid = trim($_GET["pkid"]);
}	
if($int_pkid <=0 || is_numeric($int_pkid) == false || $int_pkid=="")
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E&#ptop");
    exit();
}

#----------------------------------------------------------------------------------------------------
## For Category
# Select Query
$str_query_select = "";
$str_query_select = "SELECT title,visible FROM ".$STR_DB_TABLE_NAME_CAT." WHERE catpkid=". $int_pkid;
//print $str_query_select;exit;
$rs_list=GetRecordSet($str_query_select);

if($rs_list->eof())
{
        CloseConnection();
        Redirect("item_cat_list.php?msg=F&type=E&#ptop");
        exit();
}

$str_mode = "";
$str_title = "";
$str_mode = strtoupper(trim($rs_list->fields("visible")));
//print $str_mode; exit;
$str_title = trim($rs_list->fields("title"));

#----------------------------------------------------------------------------------------------------
#change visible mode
$str_mode_title = "";
if(strtoupper($str_mode) == "YES")
{
    $str_mode = "NO";
    $str_mode_title = "Invisible";
}
else if(strtoupper($str_mode)=="NO")
{
    $str_mode="YES";
    $str_mode_title="Visible";
}
#----------------------------------------------------------------------------------------------------
# Update Query
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_CAT." SET visible='" . ReplaceQuote($str_mode) . "' WHERE catpkid=".$int_pkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_cat_list.php?type=S&msg=V&mode=". urlencode(RemoveQuote($str_mode_title)) ."&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------

?>