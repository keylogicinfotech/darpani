<?php
/*
Module Name:- modstore
File Name  :- item_cat_edit.php
Create Date:- 4-MARCH-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
//include "./item_cat_config.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
#get Query String Data
$int_pkid = 0;
if(isset($_GET["pkid"])==true)
{
    $int_pkid = trim($_GET["pkid"]);
}	
if($int_pkid <= 0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E&#ptop");
    exit();
}

$str_title="";
if (isset($_GET["title"]))
{
    $str_title = trim($_GET["title"]);
}
#----------------------------------------------------------------------------------------------------
#select record from table
$str_query_select = "";
$str_query_select = "SELECT catpkid,title FROM " .$STR_DB_TABLE_NAME. " WHERE catpkid=". $int_pkid;
$rs_edit=GetRecordSet($str_query_select);

if($rs_edit->eof())
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E&#ptop");
    exit();
}
#------------------------------------------------------------------------------------------
#   Initialization of variables used for message display.   
    $str_type = "";
    $str_message = "";
#	Get message type.
    if(isset($_GET["type"]))
    {
        switch(trim($_GET["type"]))
        {
            case("S"): $str_type = "S"; break;
            case("E"): $str_type = "E"; break;
            case("W"): $str_type = "W";  break;
        }
    }
#	Get message text.
    if(isset($_GET["msg"]))
    {
        switch(trim($_GET["msg"]))
        {
            case("F"): $str_message =$STR_MSG_ACTION_INFO_MISSING; break;				
            case("DU"): $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE;
            case("SDU"): $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?>  : <?php print($STR_TITLE_PAGE_CAT);?> : <?php print($STR_TITLE_EDIT); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
    <div class="container content_bg">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
        <div class="row padding-10">
            <div class="col-md-3 col-sm-6 col-xs-12 button_space">
                <div class="btn-group" role="group" aria-label="...">
                    <a href="./item_cat_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE_CAT);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_CAT);?></a>
		</div>
            </div>
            <div class="col-md-9 col-sm-6 col-xs-12 " align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE_CAT);?></h3></div>
	</div><hr> 
	<?php if($str_type != "" && $str_message != "")	{ print(DisplayMessage(0,$str_message,$str_type)); }	?>
	<div class="row padding-10">
            <div class="col-md-12">
        	<div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                	<div class="panel-heading">
                            <h4 class="panel-title">
                                <b><i class="fa fa-edit"></i>&nbsp;<?php print($STR_TITLE_EDIT); ?></b>
                                <?php print($STR_LINK_HELP); ?>												
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse">
                            <div class="panel-body">
                                <form name="frm_edit" action="./item_cat_edit_p.php" method="post" onSubmit="return frm_edit_validate()">
                                    <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="form-group">
                                        <label>Title</label><span class="text-help-form"> *</span>
                                        <input type="text" name="txt_title" value="<?php print(MyHtmlEncode($rs_edit->fields("title"))); ?>" maxlength="255" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>">
                                    </div>
                                    <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                    <input type="hidden" name="hdn_pkid" value="<?php print($int_pkid);?>">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</div>
    <a name="help"></a>
    <?php include "../../includes/help_for_edit.php"; ?>
</div>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
<?php include "../../includes/include_files_admin.php"; ?>        
<script language="JavaScript" src="./item_cat_edit.js" type="text/javascript"></script>

</body></html>