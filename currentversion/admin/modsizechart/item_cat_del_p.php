<?php
/*
Module Name:- modstore
File Name  :- item_cat_del_p.php
Create Date:- 04-FEB-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid = 0;
if(isset($_GET["pkid"]) == true)
{
    $int_pkid = trim($_GET["pkid"]);
}	
if($int_pkid <= 0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------

# Delete Query
$str_query_delete="";
$str_query_delete="DELETE FROM ".$STR_DB_TABLE_NAME." WHERE catpkid=" .$int_pkid;
ExecuteQuery($str_query_delete);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to item_cat_list.php page	
CloseConnection();
Redirect("item_cat_list.php?type=S&msg=D&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------

?>