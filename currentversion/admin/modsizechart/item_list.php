<?php
/*
Module Name:- modstore
File Name  :- item_list.php
Create Date:- 04-FEB-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
//include "./item_cat_config.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT * ";
$str_query_select .= "FROM ".$STR_DB_TABLE_NAME." ";
$str_query_select .= "".$STR_DB_TABLE_NAME_ORDER_BY." ";
$rs_list = GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------------
# get Query String Data
$str_title = "";
$str_mode = "";
$str_cattitle = "";
$str_cattype = "";
$str_visible = "YES";

if (isset($_GET["title"]))
{
    $str_title = trim($_GET["title"]);
}
if (isset($_GET["mode"]))
{
    $str_mode = trim($_GET["mode"]);
}
if (isset($_GET["visible"]))
{
    $str_visible = trim($_GET["visible"]);
}
if (isset($_GET["value1"]))
{
    $int_value1 = trim($_GET["value1"]);
}
if (isset($_GET["cattype"]))
{
    $str_cattype = trim($_GET["cattype"]);
}
#------------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING;	break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("SS"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("SD"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("SU"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
        case("SV"): $str_message = " '" . MyHtmlEncode(MakeStringShort(RemoveQuote($str_title),30)) . "' changed to '". $str_mode . "' successfully.";	break;
        case("DU"): $str_message = $STR_MSG_ACTION_TITLE_ALREADY_EXIST_CAT; break;
        case("SDU"): $str_message = $STR_MSG_ACTION_TITLE_ALREADY_EXIST_SUBCAT;break;
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../css/admin.css" rel="stylesheet">
</head>
<body> <a name="ptop" id="ptop"></a>
    <div class="container">
	<?php include($STR_ADMIN_HEADER_PATH); ?>
	<div class="row padding-10">
            <div class="col-md-6 col-sm-6 col-xs-12 button_space">
                <div class="btn-group" role="group" aria-label="...">
                    <a href="./item_cms.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)">
                    <i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)
                    </a>
                    </div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE);?> </h3></div>
	</div><hr> 
	<?php if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
	<div class="row padding-10">
            <div class="col-md-12">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <h4 class="panel-title">
                            <span>
                                <a class="accordion-toggle collapsed " data-toggle="collapse" data-parent="#accordion" title="<?php print($STR_TITLE_ADD); ?>" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>
                            </span>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" >
                            <div class="panel-body">
                                <form name="frm_add" action="item_add_p.php" method="POST" onSubmit="return frm_add_validate();">
                                    <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Size / Length</label><span class="text-help-form"> *&nbsp;(<?php print $STR_MSG_HELP_SIZE_INCH; ?>)</span> 
                                                <input type="text" id="txt_value1"  name="txt_value1" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Bust</label><span class="text-help-form"> *&nbsp;(<?php print $STR_MSG_HELP_SIZE_INCH; ?>)</span> 
                                                <input type="text" id="txt_value2"  name="txt_value2" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Waist</label><span class="text-help-form"> *&nbsp;(<?php print $STR_MSG_HELP_SIZE_INCH; ?>)</span> 
                                                <input type="text" id="txt_value3"  name="txt_value3" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Hips</label><span class="text-help-form"> *&nbsp;(<?php print $STR_MSG_HELP_SIZE_INCH; ?>)</span> 
                                                <input type="text" id="txt_value4"  name="txt_value4" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <?php $str_visible="YES";
                                                if(isset($_GET["vis"]) && trim($_GET["vis"])!="" ) { $str_visible=trim($_GET["vis"]);}?>
                                                <label>Visible</label>&nbsp;<span class="text-help-form">* (<?php print($STR_MSG_VISIBLE)?>)</span><br/>
                                                <select name="cbo_visible" class="form-control input-sm" >
                                                    <option value="YES" <?php print(CheckSelected($str_visible,"YES")); ?>>YES</option>
                                                    <option value="NO" <?php print(CheckSelected($str_visible,"NO")); ?>>NO</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</div>	
	<div class="table-responsive">
	<form name="frm_list" action="item_order_p.php" method="POST" onSubmit="return frm_list_check_displayorder();">
            <table class="table table-striped table-bordered ">
        	<thead>
                <tr>
                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                    <th width=""><?php print $STR_TABLE_COLUMN_NAME_SIZE; ?></th>
                    <th width=""><?php print $STR_TABLE_COLUMN_NAME_BUST; ?></th>
                    <th width=""><?php print $STR_TABLE_COLUMN_NAME_WAIST; ?></th>
                    <th width=""><?php print $STR_TABLE_COLUMN_NAME_HIP; ?></th>
                    <!--<th width="7%">Total Items</th>-->
                    <th width="6%">Display  Order</th>
                    <th width="10%">Action</th>
		</tr>
            </thead>
            <tbody>
            <?php if($rs_list->EOF()==true)  {  ?>
		<tr><td colspan="7" align="center" class="alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
		<?php } else {?>
                <?php 
                $int_cnt = 1;
                while($rs_list->eof()==false) 
                { ?>
                <tr>
                    <td  align="center" >
                        <?php print($int_cnt);?><?php //print($cattype);?>
                        <input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->Fields("pkid"));?>">                        
                    </td>
                        <td valign="middle" > 

                        <?php 
                        $int_inches1 = $rs_list->Fields("value1");
                        $int_cm1 = ceil($int_inches1 * 2.54);
                        ?>
                        <h5><b><?php print $int_inches1. "</b> inches."; ?>&nbsp;|&nbsp;<b><?php print($int_cm1). "</b> cm.";?></h5>
                    </td>
                    <td valign="middle" >  
                        <?php 
                        $int_inches2 =$rs_list->Fields("value2");
                        $int_cm2 = ceil($int_inches2 * 2.54);
                        ?>
                        <h5><b><?php print $int_inches2. "</b> inches"; ?>&nbsp;|&nbsp;<b><?php print($int_cm2). "</b> cm.";?></h5>
                    </td>
                    <td valign="middle" >  
                        <?php 
                        $int_inches3 =$rs_list->Fields("value3");
                        $int_cm3 = ceil($int_inches3 * 2.54);
                        ?>
                        <h5><b><?php print $int_inches3. "</b> inches"; ?>&nbsp;|&nbsp;<b><?php print($int_cm3). "</b> cm.";?></h5>
                    </td>
                    <td valign="middle" >  
                       <?php 
                        $int_inches4 =$rs_list->Fields("value4");
                        $int_cm4 = ceil($int_inches4 * 2.54);
                        ?>
                        <h5><b><?php print $int_inches4. "</b> inches"; ?>&nbsp;|&nbsp;<b><?php print($int_cm4). "</b> cm.";?></h5>
                    </td>
             
                    <td  align="center" >
                        <input type="text" name="txt_displayorder<?php print($int_cnt);?>" maxlength="5" value="<?php print($rs_list->Fields("displayorder"));?>" class="form-control input-sm text-center">
                    </td>
                    <?php 
                        $str_image = "";
                        if(strtoupper($rs_list->Fields("visible")) == "YES")
                        {   
                            $str_image = $STR_LINK_ICON_PATH_VISIBLE;
                            $str_class = "btn btn-warning btn-xs";
                            $str_title = $STR_HOVER_VISIBLE;
                        }
                        else
                        {   
                            $str_image = $STR_LINK_ICON_PATH_INVISIBLE;
                            $str_class = "btn btn-default active btn-xs";
                            $str_title = $STR_HOVER_INVISIBLE;
                        } ?>
                    <td align="center">
                        <a class="<?php print($str_class); ?>" href="item_visible_p.php?pkid=<?php print($rs_list->Fields("pkid"));?>" title="<?php print($STR_HOVER_VISIBLE);?>"><?php print($str_image);?></a>
                        <a class="btn btn-success btn-xs" href="item_edit.php?pkid=<?php print($rs_list->Fields("pkid"));?>" title="<?php print($STR_HOVER_EDIT);?>"><?php print $STR_LINK_ICON_PATH_EDIT; ?></a>
                        <?php //if($int_total_items <= 0) { ?>
                            <a class="btn btn-danger btn-xs" href="./item_del_p.php?pkid=<?php print($rs_list->Fields("pkid"));?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE);?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>
                        <?php //} ?>
                    </td>
                </tr>
                <?php  $int_cnt++; 
                $rs_list->MoveNext(); 
                } ?>
                <tr> 
                    <td><input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td  align="center" class="text-align"><?php print DisplayFormButton("SAVE",0); ?></td>
                    <td></td>
                </tr>
            <?php } ?>
            </tbody>
	</table> 
    </form>
    </div>	
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<?php include($STR_ADMIN_FOOTER_PATH); ?>    
<?php include "../../includes/include_files_admin.php"; ?>    
<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
</body></html>
