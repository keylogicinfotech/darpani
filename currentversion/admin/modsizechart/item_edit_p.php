<?php
/*
Module Name:- modPstore
File Name  :- cat_edit_p.php
Create Date:- 20-MARCH-2006
Intially Create By :- 0023
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
include "./item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid = 0;
if (isset($_POST["hdn_pkid"]))
{	
    $int_pkid = trim($_POST["hdn_pkid"]);
}		
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}

$str_title = "";
$str_type = 0;

/*if (isset($_POST["txt_title"]))
{
    $str_title = trim($_POST["txt_title"]);
}
if (isset($_POST["cbo_type"]))
{
    $str_type = trim($_POST["cbo_type"]);
}*/
if (isset($_POST["txt_value1"]))
{
    $int_value1 = trim($_POST["txt_value1"]);
}
if (isset($_POST["txt_value2"]))
{
    $int_value2 = trim($_POST["txt_value2"]);
}
if (isset($_POST["txt_value3"]))
{
    $int_value3 = trim($_POST["txt_value3"]);
}
if (isset($_POST["txt_value4"]))
{
    $int_value4 = trim($_POST["txt_value4"]);
}

#----------------------------------------------------------------------------------------------------
#Validation Check

if($int_value1 == "" || $int_value2 == ""  || $int_value3 == ""  || $int_value4 == "" )
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&pkid=". $int_pkid."&#ptop");
    exit();
}
/*if($str_type <0 || is_numeric($str_type)==false)
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&pkid=". $int_pkid."&#ptop");
    exit();
}*/
#----------------------------------------------------------------------------------------------------
# Duplication Check
/*$str_query_select = "";
$str_query_select = "SELECT title FROM ".$STR_DB_TABLE_NAME." WHERE pkid!=".$int_pkid." AND title= '" . ReplaceQuote($str_title) . "'";
$rs_list_check_duplicate = GetRecordSet($str_query_select);

if(!$rs_list_check_duplicate->eof())
{
    CloseConnection();
    Redirect("item_edit.php?msg=DU&type=E&title=".$rs_list_check_duplicate->fields("title")."&catflag=".$str_cat_flag."&pkid=". $int_pkid."&#ptop");
    exit();
}*/
#----------------------------------------------------------------------------------------------------
# Update Query
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME." SET value1=" . ($int_value1) . ", value2=" .$int_value2. ", value3=" .$int_value3. ", value4=" .$int_value4. "" ;
$str_query_update .= " WHERE pkid=". $int_pkid;
//print $str_query_update;exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------------------------------------------	
# Writing data of table into XML file	
WriteXml();
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to page	
CloseConnection();
Redirect("item_list.php?type=S&msg=U&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------

?>