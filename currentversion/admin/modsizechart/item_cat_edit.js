/*
Module Name:- modPhotoset
File Name  :- cat_edit.js
Create Date:- 20-SEP-2019
Intially Create By :- 015
Update History:
*/

function frm_edit_validate()
{
    with(document.frm_edit)
    {
        if(trim(txt_title.value) == "")
        {
            alert("Please enter title.");
            txt_title.select();
            txt_title.focus();			
            return false;
        }
    }
    return true;
}