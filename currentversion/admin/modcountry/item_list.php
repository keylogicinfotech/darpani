<?php
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------
# Select Query.
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME." ".$STR_DB_TABLE_NAME_ORDER_BY."";
$rs_list = GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_msg = "";
$str_msg_title = "";
$str_title = "";
$str_mode = "";	

if(isset($_GET['msg_title']))
{ $str_msg_title=MyHtmlEncode(RemoveQuote(MakeStringShort(trim($_GET['msg_title']),35))); }
if(isset($_GET['title']))
{ $str_title=MyHtmlEncode(RemoveQuote(MakeStringShort(trim($_GET['title']),25))); }
if(isset($_GET['mode']))
{ $str_mode=MyHtmlEncode(RemoveQuote(trim($_GET['mode']))); }
if($str_mode=="YES")
{ $str_mode="VISIBLE"; }
else if($str_mode=="NO")
{ $str_mode="INVISIBLE"; }	

#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
# Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("A"): $str_msg = $STR_MSG_ACTION_ADD; break;
        case("U"): $str_msg = $STR_MSG_ACTION_EDIT; break;
        case("D"): $str_msg = $STR_MSG_ACTION_DELETE; break;
        case("F"): $str_msg = $STR_MSG_ACTION_INFO_MISSING; break;
        case("I"): $str_msg = $STR_MSG_ACTION_INVALID_IMAGE_EXT; break;
        case("O"): $str_msg = $STR_MSG_ACTION_DISPLAY_ORDER; break;	
        case("V"): $str_msg = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode; break;	
        case("DU"): $str_msg = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="fa fa-chevron-left "></i>&nbsp;Back</a><?php */?>
            </div>
        </div>
        <div class="col-md-9" align="right" >
            <h3><?php print($STR_TITLE_PAGE);?></h3>
        </div>
    </div><hr> 
    <?php if($str_type != "" && $str_msg != "") { print(DisplayMessage(0,$str_msg,$str_type)); } ?>
    <div class="row padding-10">
    	<div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <span>
                                <a class="accordion-toggle collapsed" data-toggle="collapse" title="<?php print($STR_TITLE_ADD); ?>" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>
                            </span>
                            <?php print($STR_LINK_HELP); ?>                               
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" >
                        <div class="panel-body">
                            <form name="frm_add" action="item_add_p.php" method="post" onSubmit="return frm_add_validateform();" enctype="multipart/form-data">                        <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label><span class="text-help-form"> *</span>
                                            <input type="text" class="form-control input-sm" name="txt_title" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" maxlength="100" value="<?php if(isset($_GET['title'])) { print(MyHtmlEncode(RemoveQuote($_GET['title']))); }?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Courier Price Per kg.</label><span class="text-help-form"> *</span>
                                            <input type="text" class="form-control input-sm" name="txt_courierprice" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE; ?>" maxlength="100" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Currency</label><span class="text-help-form"> *</span>
                                            <input type="text" class="form-control input-sm" name="txt_currency" size="90" placeholder="Enter currency" maxlength="100" value="<?php //if(isset($_GET['title'])) { print(MyHtmlEncode(RemoveQuote($_GET['title']))); }?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Currency Symbol</label><span class="text-help-form"> *</span>
                                            <input type="text" class="form-control input-sm" name="txt_symbol" size="90" placeholder="Enter currency symbol"  maxlength="10" value="<?php //if(isset($_GET['title'])) { print(MyHtmlEncode(RemoveQuote($_GET['title']))); }?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Conversion Rate</label><span class="text-help-form"> *</span>
                                            <input type="text" class="form-control input-sm" name="txt_conversionrate" size="90" placeholder="Enter Conversion Rate" maxlength="10" value="<?php //if(isset($_GET['title'])) { print(MyHtmlEncode(RemoveQuote($_GET['title']))); }?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Currency Short Form</label><span class="text-help-form"> </span>
                                            <input type="text" class="form-control input-sm" name="txt_shortform" size="90" placeholder="Enter currency short form" maxlength="10" value="<?php //if(isset($_GET['title'])) { print(MyHtmlEncode(RemoveQuote($_GET['title']))); }?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Upload Image<span class="text-help"></span></label>&nbsp;
                                    <span class="text-help-form"> (<?php print($STR_MSG_IMG_FILE_TYPE);?></span>&nbsp;<span class="text-help-form"> | <?php print($STR_MSG_IMG_UPLOAD);?>)</span>
                                    <input type="file" name="fileimage" >
                                </div>
                                <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form name="frm_list" method="post">
    <div class="table-responsive">
        <table class="table table-striped table-bordered ">
            <thead>   
                <tr>
                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                    <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                    <th width="8%"><?php print $STR_TABLE_COLUMN_NAME_COURIER_PRICE; ?></th>
                    <th width="12%"><?php print $STR_TABLE_COLUMN_NAME_CURRENCY; ?></th>
                    <th width="8%"><?php print $STR_TABLE_COLUMN_NAME_CURRENCY_SYMBOL; ?></th>
                    <th width="8%"><?php print $STR_TABLE_COLUMN_NAME_CURRENCY_SHORT_FORM; ?></th>
                    <th width="8%"><?php print $STR_TABLE_COLUMN_NAME_CONVERSION_RATE; ?></th>
                    <th width="5%"><?php print $STR_TABLE_COLUMN_NAME_MANAGE_STATE; ?></th>
                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_ORDER; ?></th>
                    <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>
                </tr>
            </thead>
            <tbody>
            	<?php if($rs_list->EOF()==true)  {  ?>
                <tr><td colspan="5" align="center" class="alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                    <?php } else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
                <tr>
                    <td align="center"><?php print($int_cnt)?></td>
                  <?php /* ?>      <td>
                            <?php if($rs_list->fields("imagefilename")!="") { ?>
                                <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list->fields("pkid")); ?>" rel="thumbnail"><img src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>"  class="img-responsive img-thumbnail" border="0" align="absmiddle" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image"></a>
                                <div class="modal fade f-pop-up-<?php print($rs_list->fields("pkid")); ?> " align="center"  role="dialog" aria-labelledby="" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                            <img src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>" class="img-responsive img-rounded" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image">
                                           
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                                </div>
                            <?php } ?>
                                <?php  print(MyHtmlEncode($rs_list->Fields("title"))); ?>
                        </td> <?php */ ?> 
                    <td>
                        <?php if($rs_list->fields("imagefilename") != "" ) { ?><img align="absmiddle" border="0" alt="<?php print(MyHtmlEncode($rs_list->Fields("title")));?>" title="<?php print(MyHtmlEncode($rs_list->Fields("title")));?>" src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>" class=""> <?php }
                        print(MyHtmlEncode($rs_list->Fields("title"))) ?>
                    </td>
                    <td align="right"><?php print($rs_list->fields("courierpriceperkg"))?></td>
                    <td align="center"><?php print($rs_list->fields("currency_title"))?></td>
                    <td align="center"><h4><?php print($rs_list->fields("currency_symbol"))?></h4></td>
                    <td align="center"><h4><?php print($rs_list->fields("currency_shortform"))?></h4></td>
                    <td align="right"><?php print($rs_list->fields("conversionrate"))?></td>
               <?php /* ?>     <td>
                            <?php if($rs_list->fields("imagefilename") != "" ) { ?><img align="absmiddle" border="0" alt="<?php print $STR_TITLE_PAGE; ?> image" title="<?php print $STR_TITLE_PAGE; ?> image" src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>"> <?php }
                            print(MyHtmlEncode($rs_list->Fields("title"))) ?>
                    </td>  <?php */ ?>
                    <td align="center">
                        <h4 class="nopadding"><a href="item_sub_list.php?pkid=<?php print ($rs_list->fields("pkid"));?>" title="<?php print $STR_HOVER_MANAGE; ?>" class="link"><i class="fa fa-bars"></i></a></h4>
                    </td>
                    <td>
                        <input  type="text" class="form-control input-sm text-center" maxlength="4" value="<?php print($rs_list->fields("displayorder"))?>" name="txt_displayorder_<?php print($int_cnt)?>"> 
                        <input type="hidden" name="hdn_pkid<?php print($int_cnt)?>" value="<?php print($rs_list->fields("pkid"))?>"> 
                    </td>
                    <?php $str_image = "";
                        $str_class = "";
                        $str_title = "";
                        if(strtoupper($rs_list->fields("visible")) == "YES") { $str_image=$STR_LINK_ICON_PATH_VISIBLE;
                            $str_class = "btn btn-warning btn-xs";
                            $str_title = $STR_HOVER_INVISIBLE;
                        }
                        else { 
                            $str_image = $STR_LINK_ICON_PATH_INVISIBLE;
                            $str_class = "btn btn-default active btn-xs";
                            $str_title = $STR_HOVER_VISIBLE;
                        } ?>
                    <td align="center">
                        <a class="<?php print($str_class);?>" href="item_visible_p.php?pkid=<?php print($rs_list->fields("pkid")); ?>" title="<?php print($str_title); ?>"><?php print($str_image); ?></a>
                        <a class="btn btn-success btn-xs" href="item_edit.php?pkid=<?php print($rs_list->fields("pkid"))?>" title="<?php print($STR_HOVER_EDIT);?>"><?php print $STR_LINK_ICON_PATH_EDIT; ?></a>
                        <a class="btn btn-danger btn-xs" href="./item_del_p.php?pkid=<?php print($rs_list->fields("pkid"));?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE);?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>
                    </td>
                </tr>
		<?php $int_cnt++; $rs_list->MoveNext(); } ?>
                <tr>
                    <td><input type="hidden" name="hdn_counter" value="<?php print($int_cnt)?>"></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                    <td align="center" valign="middle"><?php print DisplayFormButton("SAVE",0); ?></td><td class="align-middle" align="center"></td>
                </tr>
		<?php } ?>
            </tbody>
    	</table>
    </div>
    </form>
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<?php include "../../includes/include_files_admin.php"; ?>
    <?php include($STR_ADMIN_FOOTER_PATH); ?>
<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>

</body></html>