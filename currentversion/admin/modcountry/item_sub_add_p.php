<?php 
/*
File Name  :- item_sub_add_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
#------------------------------------------------------------------------------------------
# GEt, Validate
$int_master_pkid = 0;
$str_title = "";

if(isset($_GET["pkid"]))
{
    $int_master_pkid = trim($_GET["pkid"]);
}
if(isset($_POST["txt_title"]))
{
    $str_title = trim($_POST["txt_title"]);
}
if($int_master_pkid == "" || $int_master_pkid < 0 || is_numeric($int_master_pkid) == false)
{
    CloseConnection();
    Redirect("item_sub_list.php?msg=F&type=E&#ptop");
    exit();
}
if(trim($str_title) == trim(""))
{	
    Redirect("item_sub_list.php?msg=ee");			
    exit();
}

# Check for the Duplication of State in this Country...
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME_SUB. " WHERE title ='".trim($str_title)."' AND masterpkid=".$int_master_pkid;
$rs_list_check_duplicate = GetRecordset($str_query_select);	
if($rs_list_check_duplicate->eof()==false)
{ 
    CloseConnection();
    Redirect("item_sub_list.php?msg=ae&sname=".urlencode(RemoveQuote($str_title))."&pkid=".$int_master_pkid);
    exit();		
}   
# Insert Query.
$str_query_insert = "";
$str_query_insert = "INSERT INTO " .$STR_DB_TABLE_NAME_SUB. "(masterpkid,title) VALUES(".$int_master_pkid.",'" .trim(ReplaceQuote($str_title)). "')";
ExecuteQuery($str_query_insert);
CloseConnection();	
Redirect("item_sub_list.php?msg=as&pkid=".$int_master_pkid);
exit();
?>	