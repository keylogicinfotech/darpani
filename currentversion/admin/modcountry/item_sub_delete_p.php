<?php 
/*
File Name  :- item_sub_delete_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
#------------------------------------------------------------------------------------------
# Get, Validate & Set CountryID,StateID...
if(isset($_GET["spkid"]))
{   
  $int_title = trim($_GET["spkid"]);	
}	  
if(isset($_GET["pkid"]))
{
    $int_master_pkid=trim($_GET["pkid"]);
}
if($int_master_pkid=="" || $int_master_pkid<0 || is_numeric($int_master_pkid)==false || $int_title=="" || $int_title<0 || is_numeric($int_title)==false)
{
    CloseConnection();
    Redirect("item_sub_list.php?msg=F&type=E&#ptop");
    exit();
}
# Delete State Record from Database...
$sql ="DELETE FROM " .$STR_DB_TABLE_NAME_SUB. " WHERE pkid=" .$int_title." AND masterpkid=".$int_master_pkid;
ExecuteQuery($sql);
CloseConnection();	  
Redirect("item_sub_list.php?msg=ds&pkid=".$int_master_pkid);
exit();  	  	
?>	