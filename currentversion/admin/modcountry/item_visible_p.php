<?php
/*
File Name  :- item_visible_p_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_app_specific.php";
#------------------------------------------------------------------------------------------------------------------------------------------
# Initialize variables
$int_pkid = "";
$str_title = "";
$str_mode = "";

if(isset($_GET["pkid"]) && trim($_GET["pkid"])!="")
{
    $int_pkid=trim($_GET["pkid"]);
}	
#------------------------------------------------------------------------------------------------------------------------------------------
# Validate pkid
if($int_pkid=="" || $int_pkid<0 || is_numeric($int_pkid)==false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
#------------------------------------------------------------------------------------------------------------------------------------------
# Select Query.
$str_query_select = "";
$str_query_select = "SELECT title,visible FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
$rs_list = GetRecordSet($str_query_select);

if($rs_list->EOF())
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
$str_title = $rs_list->Fields("title");
$str_mode = $rs_list->Fields("visible");
# Change existing visibility mode
if($str_mode == "YES")
    $str_mode = "NO";
else
    $str_mode = "YES";
#------------------------------------------------------------------------------------------------------------------------------------------
# Update Query
$str_query_update = "UPDATE  " .$STR_DB_TABLE_NAME. " SET visible='".$str_mode."' WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#------------------------------------------------------------------------------------------------------------------------------------------
#write xml file
WriteXml();
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=V&type=S&msg_title=".urlencode(RemoveQuote($str_title))."&mode=".$str_mode."&#ptop");
exit();
?>