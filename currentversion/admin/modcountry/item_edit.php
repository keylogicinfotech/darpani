<?php
/*
File Name  :- item_edit.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------------------------------------------------------
# Initialization of GET / POST variables
$int_pkid = "";
if(isset($_GET["pkid"]) && trim($_GET["pkid"])!="")
{
    $int_pkid = trim($_GET["pkid"]);
}	
#------------------------------------------------------------------------------------------------------------------------------------------
# Validate pkid
if($int_pkid == "" || $int_pkid < 0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
#-----------------------------------------------------------------------------------------------------------------
# Select query to get Country Name data from tabel.
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
$rs_list=GetRecordSet($str_query_select);
#-----------------------------------------------------------------------------------------------------------------
if($rs_list->EOF())
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
#-------------------------------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_msg = "";
$str_title="";

if(isset($_GET['title']))
{ $str_title=MyHtmlEncode(RemoveQuote(MakeStringShort(trim($_GET['title']),25))); }

#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("E"): $str_msg = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
        case("F"): $str_msg = $STR_MSG_ACTION_INFO_MISSING; break;
        case("I"): $str_msg = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("DEL"): $str_msg = $STR_MSG_ACTION_DELETE_IMAGE; break;
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_EDIT); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="fa fa-chevron-left "></i>&nbsp;Back</a><?php */?>
                <a href="./item_list.php" class="btn btn-default" title="<?php print $STR_TITLE_GO_TO; ?> <?php print($STR_TITLE_PAGE); ?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
            </div>
        </div>
        <div class="col-md-9" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE); ?></h3></div>
    </div><hr> 
        <?php #	Diplay message.
		if($str_type != "" && $str_msg != "") { print(DisplayMessage(0,$str_msg,$str_type)); } ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <i class="fa fa-edit "></i>&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse">
                        <div class="panel-body">
                            <form name="frm_edit" action="item_edit_p.php" method="post" onSubmit="return frm_edit_validateform();" enctype="multipart/form-data">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label><span class="text-help-form"> *</span>
                                            <input type="text" class="form-control input-sm" name="txt_title" maxlength="100" placeholder="<?php print $STR_PLACEHOLDER_TITLE ?>" value="<?php print(MyHtmlEncode($rs_list->Fields("title")))?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Currency</label><span class="text-help-form"> *</span>
                                            <input type="text" class="form-control input-sm" name="txt_currency" maxlength="100" value="<?php print(MyHtmlEncode($rs_list->Fields("currency_title"))); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Currency Symbol</label><span class="text-help-form"> *</span>
                                            <input type="text" class="form-control input-sm" name="txt_symbol" maxlength="100" value="<?php print(MyHtmlEncode($rs_list->Fields("currency_symbol"))); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Conversion Rate</label><span class="text-help-form"> *</span>
                                            <input type="text" class="form-control input-sm" name="txt_conversionrate" size="90" placeholder="Enter Conversion Rate" maxlength="10" value="<?php print(MyHtmlEncode($rs_list->Fields("conversionrate"))); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Currency Short Form</label><span class="text-help-form"> </span>
                                            <input type="text" class="form-control input-sm" name="txt_shortform" size="90" placeholder="Enter currency short form" maxlength="10" value="<?php print(MyHtmlEncode($rs_list->Fields("currency_shortform"))); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Courier Price Per kg.</label><span class="text-help-form"> *</span>
                                            <input type="text" class="form-control input-sm" name="txt_courierprice" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE; ?>" maxlength="100" value="<?php print(MyHtmlEncode($rs_list->Fields("courierpriceperkg"))); ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Existing Image</label><br>
                                            <?php if($rs_list->fields("imagefilename")!="") { ?>
                                                <img alt="<?php print(MyHtmlEncode($rs_list->Fields("title")))?>" title="<?php print(MyHtmlEncode($rs_list->Fields("title")))?>" class="image-responsive" src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>">
                                            <?php } else { print(MakeBlankTab(50,75,"Photo not uploaded","PhotoNotAvailable")); } ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Upload Image</label>&nbsp;
                                            <span class="text-help-form"> (<?php print($STR_MSG_IMG_FILE_TYPE);?></span>&nbsp;<span class="text-help-form"> | <?php print($STR_MSG_IMG_UPLOAD);?>)</span>
                                            <input type="file" name="fileimage">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="hdn_pkid" value="<?php print($int_pkid)?>">
                                <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a name="help"></a>
    <?php include "../../includes/help_for_edit.php"; ?>    
</div>
    <?php include "../../includes/include_files_admin.php"; ?>
    <script language="JavaScript" src="./item_edit.js" type="text/javascript"></script>

</body></html>