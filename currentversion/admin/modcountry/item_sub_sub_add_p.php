<?php 
/*
File Name  :- item_sub_sub_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
#------------------------------------------------------------------------------------------
# Get, VAaidate & Set CountryID...
$int_mastersub_pkid = 0;
$str_title = "";

if(isset($_GET["pkid"]))
{
    $int_mastersub_pkid = trim($_GET["pkid"]);
}
if(isset($_GET["cpkid"]))
{
    $int_country_pkid = trim($_GET["cpkid"]);
}
if(isset($_POST["txt_city"]))
{
    $str_title = trim($_POST["txt_city"]);
}
if($int_mastersub_pkid == "" || $int_mastersub_pkid < 0 || is_numeric($int_mastersub_pkid)==false)
{
    CloseConnection();
    Redirect("item_sub_sub_list.php?msg=F&type=E&#ptop");
    exit();
}
if(trim($str_title) == trim(""))
{	
    Redirect("item_sub_sub_list.php?msg=ee");			
    exit();
}

# Check for the Duplication
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME_SUB_SUB. " WHERE name='".trim($str_title)."' AND statepkid=".$int_mastersub_pkid." AND masterpkid=".$int_country_pkid;
$rs_list_check_duplicate = GetRecordset($str_query_select);	
if($rs_list_check_duplicate->eof()==false)
{ 
    CloseConnection();
    Redirect("item_sub_sub_list.php?msg=ae&sname=".urlencode(RemoveQuote($str_title))."&pkid=".$int_mastersub_pkid);
    exit();		
}   
#Insert Query
$str_query_insert = "";
$str_query_insert = "INSERT INTO " .$STR_DB_TABLE_NAME_SUB_SUB. "(mastersubpkid,masterpkid,title) VALUES (".$int_mastersub_pkid.",'".$int_country_pkid."', '" .trim(ReplaceQuote($str_title)). "')";
ExecuteQuery($str_query_insert);
CloseConnection();	
Redirect("item_sub_sub_list.php?msg=as&pkid=".$int_mastersub_pkid);
exit();
?>	