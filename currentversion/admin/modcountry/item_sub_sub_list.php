<?php 
/*
File Name  :- item_sub_sub_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------
$int_mastersub_pkid="";
if(isset($_GET["pkid"]) && trim($_GET["pkid"])!="")
{
    $int_mastersub_pkid=trim($_GET["pkid"]);
}
if($int_mastersub_pkid=="" || $int_mastersub_pkid<0 || is_numeric($int_mastersub_pkid)==false)
{
    CloseConnection();
    Redirect("item_sub_list.php?msg=F&type=E&#ptop");
    exit();
}

# Check if Master data is Exists in Database Or Not...
$str_query_select = "SELECT *, tc.title FROM " .$STR_DB_TABLE_NAME_SUB. " ts LEFT JOIN " .$STR_DB_TABLE_NAME. " tc ON ts.masterpkid = tc.pkid WHERE ts.pkid=".$int_mastersub_pkid;
$rs_list_check_duplicate=GetRecordSet($str_query_select);
if($rs_list_check_duplicate->EOF())
{
    CloseConnection();	
    Redirect("item_sub_list.php?msg=ee&cpkid=".$int_mastersub_pkid);	
    exit();
}

# Get State Records Listing from Database...	
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME_SUB_SUB. " WHERE mastersubpkid=".$int_mastersub_pkid ." $STR_DB_TABLE_NAME_SUB_SUB_ORDER_BY ";
$rs_list = GetRecordSet($str_query_select);
    $strmsg="";
    $strcss="";
	
if(isset($_GET["msg"]) && (trim($_GET["msg"])!=""))
        {	
            switch(trim($_GET["msg"]))
            {	case("as"): $strmsg = $STR_MSG_ACTION_ADD; $strcss = "S"; break;				
                case("es"): $strmsg =  $STR_MSG_ACTION_UPDATE; $strcss = "S"; break;
                case("ds"): $strmsg =  $STR_MSG_ACTION_DELETE; $strcss = "S"; break;
                case("ee"): $strmsg =  $STR_MSG_ACTION_INFO_MISSING; $strcss = "E"; break;
                case("ae"): $strmsg = "City name already exists. Please enter another city name."; $strcss = "E"; break;										
        }		
    }
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_PAGE_SUB);?> : <?php print($STR_TITLE_PAGE_SUB_SUB);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
	<?php include("../../includes/adminheader.php"); ?>
	<div class="row">
		<div class="col-md-6 col-sm-12 col-xs-12 button_space">
			<div class="btn-group" role="group" aria-label="...">
				<?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;Back</a><?php */?>
				<a href="./item_list.php" class="btn btn-default" title="<?php print $STR_TITLE_GO_TO; ?> <?php print($STR_TITLE_PAGE); ?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
				<a href="./item_sub_list.php?pkid=<?php print($rs_list_check_duplicate->fields("masterpkid")); ?>" class="btn btn-default" title="<?php print $STR_TITLE_GO_TO; ?> <?php print($STR_TITLE_PAGE); ?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_SUB); ?></a>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12" align="right" >
			<h3><?php print($STR_TITLE_PAGE_SUB_SUB);?> [<?php print($rs_list_check_duplicate->fields("title")); ?> | <?php print($rs_list_check_duplicate->fields("title")); ?>]</h3>
		</div>
	</div><hr> 
	<?php if($strmsg!=trim("") && $strcss !=trim(""))  { print(DisplayMessage(0,$strmsg,$strcss)); }	?>
	<div class="row">
    	<div class="col-lg-12">
        	<div class="panel-group" id="accordion">
            	<div class="panel panel-default">
                	<div class="panel-heading">
                    	<h4 class="panel-title">
							<div class="row">
								<div class="col-md-12  padding-10">
									<div class="col-md-6 col-xs-8"> 
										<a class="accordion-toggle collapsed link" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><i class="glyphicon  glyphicon-plus "></i>&nbsp;&nbsp;<?php print($STR_TITLE_ADD); ?> </a>
									</div>
									<div class="col-md-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>												
								</div>
							</div>
						</h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    	<div class="panel-body">
                        	<div class="panel-body">
                                        <form action="item_sub_sub_add_p.php?pkid=<?php print($int_mastersub_pkid);?>&cpkid=<?php print($rs_list_check_duplicate->fields("masterpkid")); ?>" method="post"  name="frm_add" onSubmit="return frm_add_validateform();">
                                                <div class="form-group text-help" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                                <div class="form-group">
                                                        <label>Name</label><span class="HelpText"> *</span>
                                                        <input type="text" class="form-control input-sm" name="txt_title" value="<?php //if(isset($_GET['sname'])) print(htmlentities(RemoveQuote($_GET['sname']))); else print($rs_list_check_duplicates->fields("state_name"));?>" placeholder="<?php print $STR_TITLE_TITLE_PLACEHOLDER ?>" tabindex="1" maxlength="100" > 
                                                </div>
                                                <?php print DisplayFormButton("ADD"); ?><?php print DisplayFormButton("RESET"); ?>
                                        </form>
            				</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="table-responsive">
    	<table class="table table-striped table-bordered">
        	<thead>
                <tr>
                <th width="4%">Sr. #</th>
                <th width="94%">Details</th>
                <th width="2%">Action</th>
                </tr>
                </thead>
                <tbody>
            	<?php if($rs_list->EOF()==true)  {  ?><tr><td colspan="3" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                    <?php } else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
                        <tr>
                        <td align="center"><?php print($int_cnt)?></td>
                        <td><?php print($rs_list->fields("name"));?></td>
                        <td align="center"><a class="btn btn-danger btn-xs" href="item_sub_sub_delete_p.php?cpkid=<?php print($rs_list->fields("pkid"));?>&pkid=<?php print($int_mastersub_pkid);?>" title="<?php print($STR_TITLE_HOVER_DELETE);?>" onClick="return confirm_delete();"><i class="glyphicon glyphicon-remove"></i></a></td>
                    </tr>
                    <?php $int_cnt++; $rs_list->MoveNext(); } ?>
                            <tr><td></td><td></td><td style="vertical-align:middle" align="center"></td></tr>
                    <?php }	?>
        </tbody>
	</table>
    </div>
	<?php include "../../includes/help_for_list.php"; ?>
</div>
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/admin.css" rel="stylesheet">
<script language="JavaScript" src="../../js/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="./item_sub_sub_list.js" type="text/javascript"></script>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
</body></html>