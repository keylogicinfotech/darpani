<?php 
include "../includes/validatesession.php";
include "../includes/configuration.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_file_system.php";

# Get, Validate & Set CountryID,StateID...
if(isset($_GET["cpkid"]))
{   
  $int_city_pkid = trim($_GET["cpkid"]);	
}	  
if(isset($_GET["pkid"]))
{
        $int_state_pkid=trim($_GET["pkid"]);
}
if($int_state_pkid=="" || $int_state_pkid<0 || is_numeric($int_state_pkid)==false || $int_state_pkid=="" || $int_state_pkid<0 || is_numeric($int_state_pkid)==false)
{
        CloseConnection();
        Redirect("city_list.php?msg=F&type=E&#ptop");
        exit();
}

# Delete State Record from Database...
$sql ="DELETE FROM " .$STR_DB_TABLE_NAME_SUB_SUB. " WHERE pkid=" .$int_city_pkid." AND statepkid=".$int_state_pkid;
ExecuteQuery($sql);
CloseConnection();	  
Redirect("city_list.php?msg=ds&pkid=".$int_state_pkid);
exit();  	  	
?>	