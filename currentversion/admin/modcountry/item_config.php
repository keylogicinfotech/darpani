<?php

$STR_TITLE_PAGE = "Country List";
$STR_TITLE_PAGE_SUB = "State List";
$STR_TITLE_PAGE_SUB_SUB = "City List";

$UPLOAD_IMG_PATH = "../../mdm/sitecountry/";
$INT_IMG_WIDTH = 0;
$INT_IMG_HEIGHT = 0;

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "t_site_country"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY visible DESC, displayorder DESC, title ASC ";  

global $STR_DB_TABLE_NAME_SUB;
global $STR_DB_TABLE_NAME_SUB_ORDER_BY;
$STR_DB_TABLE_NAME_SUB = "t_state"; 
$STR_DB_TABLE_NAME_SUB_ORDER_BY = " ORDER BY title ASC "; 

global $STR_DB_TABLE_NAME_SUB_SUB;
global $STR_DB_TABLE_NAME_SUB_SUB_ORDER_BY;
$STR_DB_TABLE_NAME_SUB_SUB = "tr_city"; 
$STR_DB_TABLE_NAME_SUB_SUB_ORDER_BY = " ORDER BY title ASC "; 

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "";

global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/country.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/country_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";

$STR_TABLE_COLUMN_NAME_CURRENCY = "Currency";
$STR_TABLE_COLUMN_NAME_CURRENCY_SYMBOL = "Currency Symbol";
$STR_TABLE_COLUMN_NAME_CONVERSION_RATE = "Conversion Rate";
$STR_TABLE_COLUMN_NAME_CURRENCY_SHORT_FORM = "Currency Short Form";
?>
