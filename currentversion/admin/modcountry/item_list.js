
function frm_list_check_displayorder()
{
    with(document.frm_list)
    {
        cnt=hdn_counter.value;
        for(i=1;i<cnt;i++)
        {
            if(isEmpty(eval("txt_displayorder_" + i).value))
            {
                alert("Please enter display order.");
                eval("txt_displayorder_" + i).focus();
                return false;
            }
            if(isNaN(eval("txt_displayorder_" + i).value))
            {
                alert("Please enter numeric value for display order.");
                eval("txt_displayorder_" + i).select();
                eval("txt_displayorder_" + i).focus();
                return false;
            }
            if(eval("txt_displayorder_" + i).value<0)
            {
                alert("Please enter positive integer for display order.");
                eval("txt_displayorder_" + i).select();
                eval("txt_displayorder_" + i).focus();
                return false;
            }
            if(sValidateInt(eval("txt_displayorder_" + i).value)==false)
            {
                alert("Please do not enter fractional value for display order.");
                eval("txt_displayorder_" + i).select();
                eval("txt_displayorder_" + i).focus();
                return false;
            }
        }
        action="item_order_p.php";
        method="post";
        submit();
    }
}

function frm_add_validateform()
{
    with(document.frm_add)
    {
        if(trim(txt_title.value)=="")
        {
            alert("Please enter name.");
            txt_title.focus();
            txt_title.select();
            return false;
        }
        if(trim(txt_currency.value)=="")
        {
                alert("Please enter currency.");
                txt_currency.focus();
                txt_currency.select();
                return false;
        }
        if(trim(txt_symbol.value)=="")
        {
                alert("Please enter currency symbol.");
                txt_symbol.focus();
                txt_symbol.select();
                return false;
        }
        if(trim(txt_conversionrate.value)=="")
        {
                alert("Please enter conversion rate.");
                txt_conversionrate.focus();
                txt_conversionrate.select();
                return false;
        }
        if(trim(txt_courierprice.value)=="" /* || trim(txt_courierprice.value) <= 0 */)
        {
                alert("Please enter courier price.");
                txt_courierprice.focus();
                txt_courierprice.select();
                return false;
        }
        
        if(trim(fileimage.value) != "")
        {

            //if(checkExtGIF(trim(fileimage.value))==false)
            if(checkExt(trim(fileimage.value))==false)
            {
                fileimage.select();
                fileimage.focus();
                return false;
            }
        }
    }
}

function confirm_delete()
{
    if(confirm("Are you sure you want to delete this details?"))
    {
        if(confirm("Confirm Deletion: Click 'OK' to this details or click 'Cancel' to cancel deletion."))
        {
                return true;
        }
    }
    return false;
}
