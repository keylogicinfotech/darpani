function frm_add_validate()
{
    with(document.frm_add)
    {
        if(trim(txt_title.value) == "")
        {
            alert("Please Enter Name.");
            txt_title.select();
            txt_title.focus();
            return false;
        }
    }
}

function confirm_delete()
{
    if(confirm("Are you sure you want to delete this details?"))
    {
        if(confirm("Confirm Deletion: Click 'OK' to delete this details or click 'Cancel' to cancel deletion."))
        {
            return true;
        }
    }
    return false;
}