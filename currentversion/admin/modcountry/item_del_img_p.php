<?php 
/*	
	Module Name:- modcountry
	File Name  :- country_del_img_p.php
	Create Date:- 23-May-2006
	Intially Create By :- 0025
	Update History:
*/
#-----------------------------------------------------------------------------------------
#	Include files
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "master_config.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
	include "../includes/lib_file_system.php";
#-----------------------------------------------------------------------------------
#Get previous page values
	$int_pkid="";
	$str_query_select = "";
	
	if(isset($_GET["pkid"]))
	{
		$int_pkid = trim($_GET["pkid"]);
	}
	
	if( $int_pkid == "" || !is_numeric($int_pkid) || $int_pkid < 0 )
	{
		CloseConnection();	  
		Redirect("country_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid));
		exit();
	}
#Select query to get records from t_country table
   	$str_query_select = "";
	$rs_delete_photo = "";				
	$str_query_select = "SELECT countryname,photofilename FROM t_site_country WHERE countrypkid=".$int_pkid;
	
	$rs_delete_photo = GetRecordSet($str_query_select);
	if($rs_delete_photo->eof())
	{
		CloseConnection();	  
		Redirect("country_edit.php?msg=F&type=E");
		exit();
	}
	$str_title=$rs_delete_photo->Fields("countryname");
	#Get image name from database ...
	
	$str_thumb_file=$rs_delete_photo->Fields("photofilename");
	
	//Delete image from server 
	if(trim($str_thumb_file)!="")
	{
		DeleteFile($UPLOAD_SITE_COUNTRY_PATH.$str_thumb_file);
	}
	
	#Update the fields which store filename with null values
	$str_query_update = "";
	$str_query_update = "UPDATE t_site_country SET photofilename='' WHERE countrypkid=".$int_pkid;

	ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file

	CloseConnection();	  
	Redirect("country_edit.php?msg=DEL&type=S&pkid=".urlencode($int_pkid)."&tit=".urlencode(RemoveQuote($str_title)));
	exit();	 
?>