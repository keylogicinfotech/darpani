<?php
/*
File Name  :- item_del_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_app_specific.php";
include "../../includes/lib_file_system.php";
#------------------------------------------------------------------------------------------------------------------------------------------
# Initialize variables
$int_pkid = "";
$str_title = "";
#------------------------------------------------------------------------------------------------------------------------------------------
# Get pkid	
if(isset($_GET["pkid"]) && trim($_GET["pkid"])!="")
{
    $int_pkid = trim($_GET["pkid"]);
}	
#------------------------------------------------------------------------------------------------------------------------------------------
# Validate pkid
if($int_pkid == "" || $int_pkid < 0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
#------------------------------------------------------------------------------------------------------------------------------------------
# Select query to get country data from t_site_country tabel.
$str_select_query="SELECT title,imagefilename from ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
$rs_list=GetRecordSet($str_select_query);
if($rs_list->EOF())
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
$str_title=$rs_list->Fields("title");
$str_thumb_image=$rs_list->Fields("imagefilename");
#------------------------------------------------------------------------------------------------------------------------------------------
# Delete query to delete country
if(trim($str_thumb_image)!="")
{
    DeleteFile($UPLOAD_IMG_PATH.trim($str_thumb_image));
}
$str_delete_query="DELETE FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
ExecuteQuery($str_delete_query);
#------------------------------------------------------------------------------------------------------------------------------------------
#write xml file
WriteXml();
#-----------------------------------------------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=D&type=S&msg_title=".urlencode(RemoveQuote($str_title))."&#ptop");
exit();
?>