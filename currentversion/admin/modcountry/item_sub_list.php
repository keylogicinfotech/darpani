<?php 
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------
# GEt, Validate
$int_master_pkid = "";
if(isset($_GET["pkid"]) && trim($_GET["pkid"])!="")
{
    $int_master_pkid=trim($_GET["pkid"]);
}
if($int_master_pkid == "" || $int_master_pkid < 0 || is_numeric($int_master_pkid)==false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
# Check if Master data is Exists in Database Or Not...
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME." WHERE pkid=".$int_master_pkid;
$rs_list_master=GetRecordset($str_query_select);


if($rs_list_master->EOF())
{
    CloseConnection();	
    Redirect("item_list.php?msg=ee&cpkid=".$int_master_pkid);	
    exit();
}
#Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME_SUB. " WHERE masterpkid=".$int_master_pkid.$STR_DB_TABLE_NAME_SUB_ORDER_BY;
$rs_list = GetRecordSet($str_query_select);
	

$strmsg="";
$strcss="";
		
if(isset($_GET["msg"]) && (trim($_GET["msg"])!=""))
{	
    switch(trim($_GET["msg"]))
    {	case("as"): $strmsg = $STR_MSG_ACTION_ADD; $strcss = "S"; break;				
        case("es"): $strmsg = $STR_MSG_ACTION_UPDATE; $strcss = "S"; break;
        case("ds"): $strmsg = $STR_MSG_ACTION_DELETE; $strcss = "S"; break;
        case("ee"): $strmsg = $STR_MSG_ACTION_INFO_MISSING; $strcss = "E"; break;
        case("ae"): $strmsg = "State name already exists. Please enter another State name."; $strcss = "E"; break;        
    }		
}
?>	
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_PAGE_SUB);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="fa fa-chevron-left "></i>&nbsp;Back</a><?php */?>
                <a href="./item_list.php" class="btn btn-default" title="<?php print $STR_TITLE_PAGE; ?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
            </div>
        </div>
        <div class="col-md-9" align="right" >
            <h3><?php print($STR_TITLE_PAGE_SUB);?> [<?php print($rs_list_master->fields("title")); ?>]</h3>
        </div>
    </div><hr> 
        <?php if($strmsg!=trim("") && $strcss !=trim("")) { print(DisplayMessage(0,$strmsg,$strcss)); } ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <div class="row padding-10">
                                <div class="col-md-6 col-xs-8"> 
                                    <a class="accordion-toggle collapsed link" data-toggle="collapse" data-parent="#accordion" title="<?php print($STR_TITLE_ADD); ?>" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>
                                </div>
                                <div class="col-md-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>
                            </div>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
                        <div class="panel-body">
                            <form action="item_sub_add_p.php?pkid=<?php print($int_master_pkid);?>" method="post"  name="frm_add" onSubmit="return frm_add_validate();">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="form-group">
                                    <label>Name <span class="text-help-form"> *</span></label>	
                                    <input type="text" class="form-control input-sm" name="txt_title" value="<?php if(isset($_GET['sname'])) { print(htmlentities(RemoveQuote($_GET['sname']))); }?>" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" maxlength="100" > 
                                </div>
                                    <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                    <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                    <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if($rs_list->EOF()==true)  {  ?>
                <tr><td colspan="4" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                    <?php } else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
                <tr>
                    <td align="center"><?php print($int_cnt)?></td>
                    <td>
			<?php print($rs_list->fields("title"));?>
			<?php /* ?><table width="100%" class="table table-bordered">
			<tr width="100%">
			<?php $sel_cities="SELECT * FROM tr_city WHERE statepkid=".$rs_list->fields("statepkid")." ORDER BY name ASC";
                            $rs_cities=GetRecordSet($sel_cities);
                            $int_cnt_cty=1; 
                            while($rs_cities->eof() == false){ ?>
                                <td width="20%" class="DescriptionText10ptNormal"><?php print($rs_cities->fields("name")); ?> </td>
				<?php if($int_cnt_cty >= 5) 
                                    {	echo "</tr><tr>"; // close and open a div with class row
                                    $int_cnt_cty = 0;
				}
				$rs_cities->MoveNext();
				$int_cnt_cty++;
									}
								?>
							</tr>	
						</table><?php */ ?>
					
                    </td>
                    <?php /* ?><td align="center">
                    <a href="city_list.php?pkid=<?php print ($rs_list->fields("statepkid"));?>&cpkid=<?php print($int_master_pkid);?>" title="Click to Manage Cities"><i class="fa fa-list-ul"></i></a>
                    </td><?php */ ?>
                    <td align="center">
                        <a class="btn btn-danger btn-xs" title="<?php print($STR_HOVER_DELETE);?>" href="item_sub_delete_p.php?spkid=<?php print($rs_list->fields("pkid"));?>&pkid=<?php print($int_master_pkid);?>"  onClick="return confirm_delete();"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
		<?php $int_cnt++; $rs_list->MoveNext(); } ?>
		<?php }  ?>
            </tbody>
        </table>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<?php include "../../includes/include_files_admin.php"; ?>
    <?php include($STR_ADMIN_FOOTER_PATH); ?>
<script language="JavaScript" src="./item_sub_list.js" type="text/javascript"></script>

</body></html>