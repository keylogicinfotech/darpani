<?php
/*
File Name  :- item_add_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#----------------------------------------------------------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "item_config.php";
include "../../includes/lib_common.php";
include "./item_app_specific.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
#----------------------------------------------------------------------------------------------------------------------------------------------
# Initialize variables
	
$int_country_pkid = "";
$str_title = "";
$str_currency = "";
$str_symbol = "";
$int_conversionrate = 1.00;
$int_courierprice = 0.00;
$str_shortform = "";
$str_image = "";
#----------------------------------------------------------------------------------------------------------------------------------------------
# Collect form data
if(isset($_POST["txt_title"]) && trim($_POST["txt_title"])!="")
{
    $str_title = trim($_POST["txt_title"]);
}
if(isset($_POST["txt_courierprice"]) && trim($_POST["txt_courierprice"])!="")
{
    $int_courierprice = trim($_POST["txt_courierprice"]);
}
if(isset($_POST["txt_currency"]) && trim($_POST["txt_currency"])!="")
{
    $str_currency = trim($_POST["txt_currency"]);
}
if(isset($_POST["txt_symbol"]) && trim($_POST["txt_symbol"])!="")
{
    $str_symbol = trim($_POST["txt_symbol"]);
}
if(isset($_POST["txt_shortform"]) && trim($_POST["txt_shortform"])!="")
{
    $str_shortform = trim($_POST["txt_shortform"]);
}
if(isset($_POST["txt_conversionrate"]) && trim($_POST["txt_conversionrate"])!="")
{
    $int_conversionrate = trim($_POST["txt_conversionrate"]);
}
if(isset($_FILES['fileimage']))
{
    $str_image = trim($_FILES['fileimage']['name']);
}
#----------------------------------------------------------------------------------------------------------------------------------------------
#Validate data
if($str_title=="" || $str_symbol == "" || $str_currency == "" || $int_conversionrate == "" || $int_conversionrate < 1 || $int_courierprice <=0)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
if($str_image != "" )
{
    if(ValidateImageExtension($_FILES['fileimage']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
        CloseConnection();
        Redirect("item_list.php?msg=I&type=E&#ptop");
        exit();
    }
}	
#----------------------------------------------------------------------------------------------------------------------------------------------
# Check duplication for country	
$str_query_select="SELECT title FROM ".$STR_DB_TABLE_NAME." WHERE title='".ReplaceQuote($str_title)."'";
$rs_list=GetRecordSet($str_query_select);

if(!$rs_list->EOF())
{
    CloseConnection();
    Redirect("item_list.php?msg=E&type=E&title=".urlencode(RemoveQuote($str_title))."&#ptop");
    exit();
}
#---------------------------------------------------------------------------------------------------------	
#upload image
$str_file_name="";
$str_path="";

$str_max_count=0;
$str_max_count=GetMaxValue($STR_DB_TABLE_NAME,"pkid");
if($str_image!="")
{
    $str_file_name=$str_max_count.".".getextension($str_image);
    $str_path= trim($UPLOAD_IMG_PATH.$str_file_name);
    UploadFile($_FILES['fileimage']['tmp_name'],$str_path);
}	
#----------------------------------------------------------------------------------------------------------------------------------------------
# get max value of displayorder
$int_max_do=GetMaxValue($STR_DB_TABLE_NAME,"displayorder");	
# Insert query to insert skin color detail to table
$str_query_insert  = "INSERT INTO ".$STR_DB_TABLE_NAME." (pkid,title,courierpriceperkg,currency_title,currency_symbol, currency_shortform,conversionrate,imagefilename,visible,displayorder ) ";
$str_query_insert .= " VALUES (".$str_max_count.",'". ReplaceQuote($str_title)."', ".$int_courierprice.", '".ReplaceQuote($str_currency)."','".ReplaceQuote($str_symbol)."', '".ReplaceQuote($str_shortform)."',".$int_conversionrate.",'". ReplaceQuote($str_file_name)."','YES',".$int_max_do.")";	
//print $str_query_insert;exit;
ExecuteQuery($str_query_insert);
#----------------------------------------------------------------------------------------------------------------------------------------------
WriteXml();
#----------------------------------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=A&type=S&msg_title=".urlencode(RemoveQuote($str_title))."&#ptop");
exit();
?>