function frm_edit_validateform()
{
    with(document.frm_edit)
    {
        if(trim(txt_title.value)=="")
        {
            alert("Please enter title.");
            txt_title.focus();
            txt_title.select();
            return false;
        }
        if(trim(txt_currency.value)=="")
        {
                alert("Please enter currency.");
                txt_currency.focus();
                txt_currency.select();
                return false;
        }
        if(trim(txt_symbol.value)=="")
        {
                alert("Please enter currency symbol.");
                txt_symbol.focus();
                txt_symbol.select();
                return false;
        }
        if(trim(txt_conversionrate.value)=="" || trim(txt_conversionrate.value) < 1)
        {
                alert("Please enter proper conversion value.");
                txt_conversionrate.focus();
                txt_conversionrate.select();
                return false;
        }
        if(trim(txt_courierprice.value)=="" /* || trim(txt_courierprice.value) <= 0 */)
        {
                alert("Please enter courier price.");
                txt_courierprice.focus();
                txt_courierprice.select();
                return false;
        }
        if(trim(fileimage.value) != "")
        {
            if(checkExtGIF(trim(fileimage.value))==false)
            {
                fileimage.select();
                fileimage.focus();
                return false;
            }
        }
    }
}
function confirm_deletion()
{
    if(confirm("Are you sure you want to delete this detail permanently."))
    {
        if(confirm("Confirm Deletion:Click 'OK' to delete this detail or 'Cancel' to deletion."))
        {
            return true;
        }
    }
    return false;
}

