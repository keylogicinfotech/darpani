<?php
/*
File Name  :- item_edit_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#----------------------------------------------------------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_file_system.php";
include "./item_app_specific.php";
#----------------------------------------------------------------------------------------------------------------------------------------------
# Initialize variables
$int_pkid="";
$str_title="";
$str_currency = "";
$str_symbol = "";
$int_conversionrate = 1.00;
$int_courierprice = 0.00;
$str_shortform = "";
$str_image = "";
#----------------------------------------------------------------------------------------------------------------------------------------------
# Get pkid	
if(isset($_POST["hdn_pkid"]) && trim($_POST["hdn_pkid"])!="")
{
    $int_pkid=trim($_POST["hdn_pkid"]);
}	

# Validate pkid
if($int_pkid=="" || $int_pkid<0 || is_numeric($int_pkid)==false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
#------------------------------------------------------------------------------------------------------------------------------------------
if(isset($_POST["txt_title"]) && trim($_POST["txt_title"])!="")
{
    $str_title=trim($_POST["txt_title"]);
}
if(isset($_POST["txt_courierprice"]) && trim($_POST["txt_courierprice"])!="")
{
    $int_courierprice = trim($_POST["txt_courierprice"]);
}
if(isset($_POST["txt_currency"]) && trim($_POST["txt_currency"])!="")
{
    $str_currency = trim($_POST["txt_currency"]);
}
if(isset($_POST["txt_symbol"]) && trim($_POST["txt_symbol"])!="")
{
    $str_symbol=trim($_POST["txt_symbol"]);
}
if(isset($_POST["txt_conversionrate"]) && trim($_POST["txt_conversionrate"])!="")
{
    $int_conversionrate = trim($_POST["txt_conversionrate"]);
}
if(isset($_POST["txt_shortform"]) && trim($_POST["txt_shortform"])!="")
{
    $str_shortform = trim($_POST["txt_shortform"]);
}
if(isset($_FILES['fileimage']))
{
    $str_image=trim($_FILES['fileimage']['name']);
}
#----------------------------------------------------------------------------------------------------------------------------------------------
# Validate data	
if($str_title == "" || $int_conversionrate < 1 || $int_conversionrate == "")
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&pkid=".$int_pkid."&#ptop");
    exit();
}
if($str_image != "" )
{
    if(ValidateImageExtension($_FILES['fileimage']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
        CloseConnection();
        Redirect("item_edit.php?msg=F&type=E&pkid=".$int_pkid."&#ptop");
        exit();
    }
}
#----------------------------------------------------------------------------------------------------------------------------------------------
# Check duplication for country	
$str_query_select = "SELECT title FROM ".$STR_DB_TABLE_NAME." WHERE title='".ReplaceQuote($str_title)."' AND pkid<>".$int_pkid;
$rs_list_check_duplicate = GetRecordSet($str_query_select);

if(!$rs_list_check_duplicate->EOF())
{
    CloseConnection();
    Redirect("item_edit.php?msg=E&type=E&pkid=".$int_pkid."&title=".urlencode(RemoveQuote($str_title))."&#ptop");
    exit();
}
#---------------------------------------------------------------------------------------------------------	
#upload image
$str_thumb_file_name="";
$str_thumb_path="";

$str_query_select = "SELECT imagefilename FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
$rs_list=GetRecordset($str_query_select);
if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&pkid=".$int_pkid."&#ptop");
    exit();
}
$str_thumb_file_name=$rs_list->fields("imagefilename");
if($str_image!="")
{
    #delete old image
    if($str_thumb_file_name!="")
    {
            DeleteFile($UPLOAD_IMG_PATH.trim($str_thumb_file_name));
    }
    #upload new image
    $str_thumb_file_name=$int_pkid.".".getextension($str_image);
    $str_thumb_path = trim($UPLOAD_IMG_PATH.$str_thumb_file_name);
    UploadFile($_FILES['fileimage']['tmp_name'],$str_thumb_path);
}
#----------------------------------------------------------------------------------------------------------------------------------------------
$str_update_query  = "UPDATE ".$STR_DB_TABLE_NAME." SET title='".  ReplaceQuote($str_title) ."', courierpriceperkg = ".$int_courierprice.", currency_title = '".ReplaceQuote($str_currency)."', currency_symbol = '".ReplaceQuote($str_symbol)."', currency_shortform = '".ReplaceQuote($str_shortform)."', conversionrate=".$int_conversionrate.",imagefilename='".  ReplaceQuote($str_thumb_file_name) ."' WHERE pkid=".$int_pkid;
ExecuteQuery($str_update_query);
#----------------------------------------------------------------------------------------------------------------------------------------------
#write xml file
WriteXml();
#----------------------------------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=U&type=S&msg_title=".urlencode(RemoveQuote($str_title))."&#ptop");
exit();
?>