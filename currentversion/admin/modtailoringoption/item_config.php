<?php
$STR_TITLE_PAGE = "Tailoring Options List";
$UPLOAD_IMG_PATH = "../../mdm//";
$INT_IMG_WIDTH = 400;
$INT_IMG_HEIGHT = 200;

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "t_store_tailoringoption"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY displayorder DESC, title ASC "; 

global $STR_DB_TR_TABLE_NAME;
global $STR_DB_TR_TABLE_NAME_ORDER_BY;
$STR_DB_TR_TABLE_NAME = "tr_store_tailoringoption"; 
$STR_DB_TR_TABLE_NAME_ORDER_BY = " ORDER BY displayorder DESC, title ASC "; 


global $STR_DB_TABLE_NAME_SERVICES;
global $STR_DB_TABLE_NAME_SERVICES_ORDER_BY;
$STR_DB_TABLE_NAME_SERVICES = "t_store_tailoringservice"; 
$STR_DB_TABLE_NAME_SERVICES_ORDER_BY = " ORDER BY displayorder DESC, title ASC "; 

/*global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "cms_occasion";

global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/occasion.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/occasion_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";	*/


#For Form 
//$STR_PLACEHOLDER_SIZING_OPTION = "Enter Sizing Optin"; 
$STR_MSG_SIZING_OPTION = "Enter Sizing Options Here. Separate Multiple Sizing Options By Pipe (|)";

#For <th>
$STR_TABLE_COLUMN_NAME_TAILORING_SERVICE = "Tailoring Service";
?>
