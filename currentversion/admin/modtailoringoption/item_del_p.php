<?php 
/*
File Name  :- item_del_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
//include "./item_app_specific.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
#-----------------------------------------------------------------------------------------------------------------------------------------------------
$int_pkid="";
$str_title="";
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#To check whether all values are passed properly or not.	
if(isset($_GET["pkid"]))
{
    $int_pkid=trim($_GET["pkid"]);
}
if($int_pkid=="")
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
if (!is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
$cat_flag="";
if(isset($_GET["catflag"])==true)
{
	$cat_flag=trim($_GET["catflag"]);
}
if($cat_flag=="" || ($cat_flag!="c" && $cat_flag!="s") )
{
	CloseConnection();
	Redirect("item_list.php?msg=F&type=E&#ptop");
	exit();
}
if($cat_flag=="c")
{
#delete from table
	
	#check wheather child record exists
	$str_query_check="";
	$str_query_check="SELECT pkid FROM ".$STR_DB_TR_TABLE_NAME." WHERE optionpkid=".$int_pkid;
	$rs_list_check=GetRecordSet($str_query_check);
	
	if(!$rs_list_check->eof())
	{
		CloseConnection();
		Redirect("item_list.php?msg=F&type=E&#ptop");
		exit();
	}
	
	#select query to get details from table
	$str_query_select="";
	$str_query_select="SELECT title FROM ".$STR_DB_TABLE_NAME."  WHERE pkid=". $int_pkid;
	$rs_list_delete=GetRecordSet($str_query_select);
	
	if($rs_list_delete->eof())
	{
		CloseConnection();
		Redirect("item_list.php?msg=F&type=E&#ptop");
		exit();
	}
		
	#Delete from table
	$str_query_delete="";
	$str_query_delete="DELETE FROM ".$STR_DB_TABLE_NAME."  WHERE pkid=" .$int_pkid;
	ExecuteQuery($str_query_delete);
	#----------------------------------------------------------------------------------------------------
	#Close connection and redirect to page	
		CloseConnection();
		Redirect("item_list.php?type=S&msg=D&title=".urlencode(RemoveQuote($rs_list_delete->fields("title")))."&#ptop");
		exit();
	#------------------------------------------------------------------------------------------------------------
}
else if($cat_flag=="s");
{
#delete from.
	
	#select query to get details from table
	$str_query_select="";
	$str_query_select="SELECT title FROM ".$STR_DB_TR_TABLE_NAME." WHERE pkid=". $int_pkid;
	$rs_list_delete=GetRecordSet($str_query_select);
	
	if($rs_list_delete->eof())
	{
		CloseConnection();
		Redirect("item_list.php?msg=F&type=E&#ptop");
		exit();
	}
		
	#Delete from table
	$str_query_delete="";
	$str_query_delete="DELETE FROM ".$STR_DB_TR_TABLE_NAME." WHERE pkid=" .$int_pkid;
	ExecuteQuery($str_query_delete);
	#----------------------------------------------------------------------------------------------------
	#Close connection and redirect to item_list.php page	
		CloseConnection();
		Redirect("item_list.php?type=S&msg=D&title=".urlencode(RemoveQuote($rs_list_delete->fields("title")))."&#ptop");
		exit();
	#------------------------------------------------------------------------------------------------------------
}
?>