<?php 
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------
#select query to get all the details from table	
$str_query_select="";
$str_query_select = "";
$str_query_select = "SELECT a.pkid,a.title cattitle,a.visible catvisible,a.displayorder catorder,";
$str_query_select .= "b.pkid AS subpkid,b.title subtitle,b.visible subvisible,b.displayorder suborder";
$str_query_select .= " FROM ".$STR_DB_TABLE_NAME."  a";
$str_query_select .= " LEFT JOIN ".$STR_DB_TR_TABLE_NAME." b";
$str_query_select .= " ON b.optionpkid=a.pkid";
$str_query_select .= " ORDER BY a.displayorder desc,a.title,a.pkid,";
$str_query_select .= "b.displayorder desc,b.title,b.pkid";
//print $str_query_select;
$rs_list=GetRecordset($str_query_select);

#------------------------------------------------------------------------------------------
#getting query string variables	
$str_sub="";
$str_desc="";
$str_visible="";
$str_pos="";
if(isset($_GET['question'])) { $str_sub=trim($_GET['question']); }
if(isset($_GET['desc'])) { $str_desc=trim($_GET['desc']); }
if(isset($_GET["visible"])) { $str_visible=trim($_GET["visible"]); }
if (isset($_GET["position"])){ $str_pos = trim($_GET["position"]);  }
if (isset($_GET["po_mode"])){$str_pos = trim($_GET["po_mode"]);}


$str_cattype="";
if (isset($_GET["cattype"]))
{
	$str_cattype = trim($_GET["cattype"]);
}//
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type="";
$str_message="";
$str_title="";
$str_mode="";
if(isset($_GET['tit'])) { $str_title=trim($_GET['tit']); }
if(isset($_GET['mode'])) { $str_mode=trim($_GET['mode']); }
#Get message type.
if(isset($_GET['type']))
{
    switch(trim($_GET['type']))
    {
        case("S"): $str_type = "S"; break; case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Get message text.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
        
    }
}	
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-6 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="fa fa-chevron-left "></i>&nbsp;Back</a>
                <a href="./item_cms.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)">
                    <i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)
                </a><?php */?>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12" align="right" >
            <h3><?php print($STR_TITLE_PAGE);?> </h3>
        </div>
    </div>
    <hr> 
    <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <span>
                                <a class="accordion-toggle collapsed" data-toggle="collapse" title="<?php print $STR_HOVER_ADD; ?>" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>
                            </span>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
                        <div class="panel-body">
                            <form name="frm_add" action="item_add_p.php" method="post" onSubmit="return frm_add_validate();" enctype="multipart/form-data" role="form">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="form-group">
                                    <label>Title</label><span class="text-help-form"> * </span>
                                    <input id="txt_title" name="txt_title" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" type="text">
                                </div>
                                <div class="form-group">
                                    <label>Tailoring Option Type </label><span class="text-help-form"> * </span>
                                    <select name="cbo_type" class="form-control input-sm">
                                        <?php //  print "cat type" .$str_cattype; ?>
                                        <option value=""><-- Select Tailoring Option --></option>
                                        <option value="0" <?php print(CheckSelected("0",strtoupper($str_cattype)));?>>Create As Main Option</option>
                                        <option value="">-------------- OR --------------</option>
                                    <?php while($rs_list->eof()==false) 
                                         { 
                                                   if($str_temp!=$rs_list->fields("pkid"))
                                                         {
                                                           $str_temp=$rs_list->fields("pkid");
                                         ?>
                                                 <option value="<?php print($rs_list->fields("pkid")); ?>" <?php print(CheckSelected($rs_list->fields("pkid"),$str_cattype));?>><?php print(MakeStringShort(MyHtmlEncode($rs_list->fields("cattitle")),"90")); 
                                                 if(strtoupper($rs_list->fields("catvisible")=="NO")) { print(" (Invisible)"); }

                                                 ?></option>
                                    <?php } $rs_list->MoveNext(); } ?>
                                    </select>	   
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Visible</label>&nbsp;<span class="text-help-form">*(<?php print($STR_MSG_VISIBLE)?>)</span><br/>
                                            <select name="cbo_visible" class="form-control input-sm">
                                            <option value="YES">YES</option><option value="NO">NO</option></select>
                                        </div>
                                    </div>
                                    </div>
                                <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                <input type="hidden" name="hdn_pkid" value="<?php print($rs_list->fields("pkid")); ?>" >
                                <input type="hidden" name="txt_header" size="80" class="clsTextBox" maxlength="100" value="">                            
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
            
            <table class="table table-striped table-bordered">
               
                <form name="frm_list" action="item_order_p.php" method="post" onSubmit="return frm_list_check_displayorder();">
                     
                    <thead>
                        <tr>
                            
                            <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                            <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                            <th width="5%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_ORDER; ?></th>
                            <th width="9%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>

                        </tr>
                    </thead>
                    
                    <tbody>	
                        <?php 
                            if($rs_list->MoveLast())	
			{
				$rs_list->MoveFirst(); 
			}
	   		$str_temp=""; ?> 
                        <?php if($rs_list->EOF())
                        { ?>
							
                        <tr>
                            <td colspan="7" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td>
                        </tr>
                       <?php } else {?>
		<?php 
			$int_cnt=1;
			$int_cat_cnt=0;
			$int_sub_cnt=0;
			$pkid=0;
			$cnt="";
			$title="";
			$visible="";
			$order="";
			$optionpkid="";
			$flag=0;
			$delete_flag=0;
			$disp_class="clsTextBox";
			$cat_flag="";
			while($rs_list->eof()==false)
			{ ?>		
			 <?php if($pkid != $rs_list->fields("pkid") && $flag==0) 
			 {	#setting all variables from table.
				$int_sub_cnt = 0;
			    $flag=1;
				$delete_flag=0;	#delete flag to check wheather category has sub details or not.
				$cat_flag="c";	#flag to indicate category record.
                                
				$pkid = $rs_list->fields("pkid");
                                $str_highlightclass = "class='alert-warning'";
				$int_cat_cnt = $int_cat_cnt + 1;
				$cnt="<strong>".$int_cat_cnt."</strong>"; #to display serial no.
				$title= "<strong>".MyHtmlEncode($rs_list->fields("cattitle"))."</strong>"; 
				$visible=$rs_list->fields("catvisible");
				$order=$rs_list->fields("catorder");
				$optionpkid=$rs_list->fields("pkid");
				$disp_class="";	#to set class for textbox of display order.
				if(trim($rs_list->fields("subtitle"))=="")
					{ $delete_flag=1; }
			 }
			 else	
			 #setting all variables from table.
			 {	
			 	$flag=0;				
				$delete_flag=1;		 	
				$cat_flag="s";	#flag to indicate subcategory record.
                                $str_highlightclass = "";
				$int_sub_cnt = $int_sub_cnt + 1;
				$cnt=$int_sub_cnt; 
				$title= MyHtmlEncode($rs_list->fields("subtitle")); 
				$visible=$rs_list->fields("subvisible");
				$order=$rs_list->fields("suborder");
				$optionpkid=$rs_list->fields("subpkid");
				$disp_class="";
			 }
		 ?>
		<?php  if(trim($title)!="") { ?>
        <tr class="al<?php print($int_cnt%2); ?>"> 
		  <td align="center" <?php print($str_highlightclass);?> valign="middle"><?php print($cnt);?><input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($optionpkid);?>"><input type="hidden" name="hdn_cattype<?php print($int_cnt);?>" value="<?php print($cat_flag);?>"></td>
		  <td align="center" <?php print($str_highlightclass);?>> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                         <tr class="alert-warning<?php print($int_cnt%2); ?>"> 
			   <td align="left" valign="middle">
                <?php print($title);  ?> 
			   </td>
                         </tr>
              </table>    
                  </td> 
                  
           
                         <td align="center" <?php print($str_highlightclass);?>valign="center"><input type="text" name="txt_displayorder<?php print($int_cnt);?>" size="5" maxlength="5" value="<?php print($order);?>" class="form-control input-sm text-center"> </td>
                                <?php $str_image="";
                        if(strtoupper($visible)=="YES")
                        {   $str_image=$STR_LINK_ICON_PATH_VISIBLE;
                            $str_class="btn btn-warning btn-xs";
                            $str_title=$STR_HOVER_VISIBLE;
                        }
                        else
                        {   $str_image=$STR_LINK_ICON_PATH_INVISIBLE;
                            $str_class="btn btn-default active btn-xs";
                            $str_title=$STR_HOVER_INVISIBLE;
                        }
                        ?>
                        <td align="center" <?php print($str_highlightclass);?>class="text-center">
                            <a class="<?php print($str_class); ?>" href="./item_visible_p.php?pkid=<?php print($optionpkid);?>&catflag=<?php print($cat_flag);?>"  title="<?php print($str_title); ?>"><?php print($str_image);?></a>
                            <a class="btn btn-success btn-xs" href="./item_edit.php?pkid=<?php print($optionpkid);?>&catflag=<?php print($cat_flag);?>" title="<?php print($STR_HOVER_EDIT);?>"><?php print $STR_LINK_ICON_PATH_EDIT; ?></a>
                           
                            
                     <?php      $str_query_check="";
                                $str_query_check="SELECT pkid FROM ".$STR_DB_TABLE_NAME;
//                                print $str_query_check;
                                $rs_list_cat=GetRecordSet($str_query_check); 
                               
                                if($rs_list->Fields("subpkid") != NULL && $cat_flag=="c")
                                { ?>
                                  <a class="btn btn-default disabled btn-xs" href="./item_del_p.php?pkid=<?php print($optionpkid);?>&catflag=<?php print($cat_flag);?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE);?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>       
                           
                       <?php    }else if($rs_list->Fields("pkid") != NULL && $cat_flag=="s"){  ?>
                            <a class="btn btn-danger  btn-xs" href="./item_del_p.php?pkid=<?php print($optionpkid);?>&catflag=<?php print($cat_flag);?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE);?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>
                            
                           <?php  }else { ?>
                            <a class="btn btn-danger  btn-xs" href="./item_del_p.php?pkid=<?php print($optionpkid);?>&catflag=<?php print($cat_flag);?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE);?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>
                           <?php } ?>
                        </td>
                        </tr>
                   <?php 
		$int_cnt=$int_cnt+1;
		}
		if($flag!=1)
		{
			$rs_list->movenext();
		}
				
		}		
		?>
                    <input type="hidden" name="hdn_counter" value="<?php print($int_cnt)?>">
                    <tr class="<?php print($int_cnt%2); ?>">
                        <td></td>
                        <td></td>
                        
                            <?php /*?><td><button class="btn btn-success" name="btn" title="Click to change 'Display on Top' setting" onClick="return displayontop_click();">Change</button></td>
                            <td><button class="btn btn-success" name="shop_btn" title="Click to change 'Display on Home' setting" onClick="return displayonhome_click();">Change</button></td><?php */?>
                        <td align="center" valign="middle"><?php print DisplayFormButton("SAVE",8); ?></td>
                        <td align="center"></td>
                    </tr>
                <?php } ?>	
                    </tbody>
                </form>
            </table>
        </div>
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
<script language="JavaScript" src="./item_add.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>
