<?php 
/*
File Name  :- item_edit.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
//include "./item_app_specific.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
#------------------------------------------------------------------------------
#Select query to get records from table
$int_pkid="";
if(isset($_GET['pkid']))
{
    $int_pkid=trim($_GET['pkid']);
}
$cat_flag="";
if(isset($_GET["catflag"])==true)
{
    $cat_flag=trim($_GET["catflag"]);
}//print $cat_flag; exit;
if($cat_flag=="" || ($cat_flag!="c" && $cat_flag!="s"))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}

$str_title="";
if (isset($_GET["title"]))
{
    $str_title = trim($_GET["title"]);
}
#select record from table
$str_query_select="";
if($cat_flag=="c")
{	
    $str_query_select="SELECT * FROM ".$STR_DB_TABLE_NAME." WHERE pkid=". $int_pkid;
        //print $str_query_select; exit;
}
else if($cat_flag=="s")
{
    $str_query_select="SELECT * FROM ".$STR_DB_TR_TABLE_NAME." WHERE pkid=". $int_pkid;
        //print $str_query_select; exit;
}

$rs_edit=GetRecordSet($str_query_select);

if($rs_edit->eof())
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}

$str_query_select = "";	
$str_query_select = "SELECT pkid,title,visible FROM ".$STR_DB_TABLE_NAME." ORDER BY displayorder desc,title,pkid";
//print $str_query_select; exit;
$rs_list=GetRecordSet($str_query_select);

#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
# Diplay message.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("E"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("DEL"): $str_message = $STR_MSG_ACTION_DELETE_IMAGE; break;
        case("LINK"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;
        case("IINV"): $str_message = "All images can't be INVISIBLE, Please keep atleast one image visible."; break;
        case("DBC"): $str_message = "Please select button type and enter valid caption for selected button.";	break;
        case("DU"): $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_EDIT); ?> </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<!-- Page Content -->
<a name="ptop"></a>
<div class="container">
<?php include($STR_ADMIN_HEADER_PATH); ?> 
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?><a href="#help"  class="btn btn-info " title="Click to view help"><i class="glyphicon glyphicon-info-sign  "></i>&nbsp; Help</a> <?php */?>
                    <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Back to State List"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;Back</a><?php */?>
                <a href="./item_list.php" title="<?php print($STR_TITLE_GO_TO); ?> <?php print($STR_TITLE_PAGE); ?>" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
            </div>
        </div>
        <div class="col-md-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE); ?></h3></div>
    </div><hr>
	<?php	if($str_type != "" && $str_message != "") { print(DisplayMessage("99%",$str_message,$str_type)); } ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <i class="fa  fa-edit "></i>&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b> </a>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    <div id="collapse01" class="panel-collapse">
                        <div class="panel-body">
                            <form name="frm_edit" action="item_edit_p.php" method="post" onSubmit="return frm_edit_validate();" enctype="multipart/form-data" role="form">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                               <?php /* ?> <div class="form-group">	
                                    <input type="hidden" name="txt_header" size="80" class="clsTextBox" maxlength="100" value="">
                                </div> <?php */ ?>
                                <div class="form-group">
                                    <label>Title</label><span class="text-help-form"> * </span>
                                    <input id="txt_title" name="txt_title" size="90" class="form-control input-sm" maxlength="512" value="<?php print(RemoveQuote($rs_edit->fields("title"))); ?>" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" type="text">
                                </div>
                               
                                <div class="form-group">
                                    <?php 
                                        if($cat_flag=="s")
                                        {
                                             $str_temp=""; ?>
	  
                                       <label>Work and Skill Type: </label><span class="text-help-form"> * </span>
                                       <select name="cbo_type" class="form-control input-sm">
                                            <!--<select name="cbo_type">-->
                                                <?php while($rs_list->eof()==false) 
                                                { 			  
                                                ?>
                                                <option value="<?php print($rs_list->fields("pkid")); ?>" <?php print(CheckSelected($rs_list->fields("pkid"),$rs_edit->fields("pkid")));?>><?php print(MakeStringShort(MyHtmlEncode($rs_list->fields("title")),"90")); 
                                                if(strtoupper($rs_list->fields("visible")=="NO")) { print(" (Invisible)"); } ?></option>
                                                <?php $rs_list->movenext(); } ?>
                                            </select>	   
                                    <?php } ?>
                                </div>
<!--                                <div class="form-group">
                                    <label>Description</label><span class="text-help-form"></span>
                                    <textarea name="ta_desc" id="ta_desc" cols="100" rows="10" class="form-control input-sm" placeholder="<?php //print $STR_PLACEHOLDER_DESC; ?>" ><?php //print(RemoveQuote($rs_list->fields("description"))); ?></textarea>
                                    <span class="text-help-form"><?php //print($STR_MAX_255_MSG);?></span>	
                                </div>-->
                              <?php /* ?>  <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Existing Image</label> 
                                                <?php if($rs_list->fields("imagefilename")!="") { ?>
                                                   <a href="./item_del_img_p.php?pkid=<?php print($rs_list->fields("pkid"));?>" title="<?php print($STR_HOVER_DELETE_IMAGE)?>" onClick="return confirm_delete();" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></a><br> 

                                                    <a href="#" data-toggle="modal" data-target=".profile-<?php print($rs_list->fields("pkid")); ?>" rel="thumbnail" ><img class="img-responsive" title="<?php print $STR_HOVER_IMAGE; ?>" src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>" border="0" alt="<?php print $rs_list->fields("imagefilename"); ?>" align="top" class="align-middle"></a>
                                                    <div class="modal fade profile-<?php print($rs_list->fields("pkid")); ?>" align="center" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                                    <img src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>" class="img-responsive img-rounded" title="<?php print $STR_HOVER_IMAGE; ?>">
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div>
                                                    <?php }
                                            else {  print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); } ?> 


                                                    <?php /*?><?php if($rs_list->fields("imagefilename")!="") { ?>&nbsp;<a href="cm_home_content_image_del_p.php?pkid=<?php print($int_pkid);?>" title="Click to delete this image permanentely" class="Link" onClick="return confirm_delete();" >[Delete this image]</a><?php } ?>
                                            <div align="left" valign="top">
                                                <?php if($rs_list->fields("imagefilename")!="") { ?>
                                                    <?php $path=$UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"); 
                                                                                        //print $path; //print($image_tag);?>
                                                <img src="<?php print $path; ?>" alt="Image not uploaded" title="Existing Image" border="0" class="img-responsive">
                                                    <?php }  else { print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); } ?>
                                            </div>  
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Upload New Image</label>&nbsp;
                                            <span class="text-help-form"> (<?php print($STR_MSG_IMG_FILE_TYPE);?></span>&nbsp;
                                            <span class="text-help-form"> )</span>
                                            <input type="file" name="fileimage" size="90"  >
                                            <input name="hdn_existing_image" type="hidden" id="hdn_existing_image" value="<?php print($rs_list->fields("imagefilename")); ?>">
                                        </div>
                                    </div>
                                </div>  
                                <div class="row padding-10">
                                    
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>URL Title</label>
                                            <input class="form-control input-sm" name="txt_urltitle" id="txt_urltitle"  size="90"  maxlength="512" value="<?php print(RemoveQuote($rs_list->fields("urltitle"))); ?>"  placeholder="Enter website url title here" >
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>URL</label>
                                            <input class="form-control input-sm" name="txt_url" id="txt_url"  size="90"  maxlength="512" value="<?php print(RemoveQuote($rs_list->fields("url"))); ?>"  placeholder="<?php print $STR_MSG_URL_FORMAT; ?>" >
                                        </div>
                                    </div>
                                    
                                    </div>
                                
                                 <div class="row padding-10">
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Open URL In New Window </label>&nbsp;<span class="text-help-form">(<?php print($STR_MSG_OPEN_IN_NEW_WINDOW); ?>)</span>
                                            <select name="cbo_window" class="form-control input-sm">
                                                <option value="YES" <?php print(CheckSelected("YES",strtoupper($rs_list->fields("openurlinnewwindow")))); ?>>YES</option>
                                                <option value="NO" <?php print(CheckSelected("NO",strtoupper($rs_list->fields("openurlinnewwindow")))); ?>>NO</option></select>
                                        </div>
                                    </div>
                                     <?php */ ?> 
<!--                            <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Hover Text</label>
                                            <input class="form-control input-sm" name="txt_hover" id="txt_hover"  size="90"  maxlength="512" value=""  placeholder="Enter Hover Text here" tabindex="">
                                        </div>
                                    </div>  -->
<!--                                </div>-->
				<?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                <input type="hidden" name="hdn_pkid" value="<?php print($int_pkid);?>" >
                                <input type="hidden" name="hdn_catflag" value="<?php print($cat_flag); ?>">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php include "../../includes/help_for_edit.php"; ?>
</div>
    <!-- /.container -->											
<!-- Bootstrap Core CSS -->
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
    <script language="JavaScript" src="./item_edit.js" type="text/javascript"></script>

</body></html>