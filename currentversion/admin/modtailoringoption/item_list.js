function frm_add_validate()
{
	with(document.frm_add)
	{
		/*if(trim(txt_section_title.value) == "")
		{
                    alert("Please enter section title.");
                    txt_section_title.select();
                    txt_section_title.focus();
                    return false;
		}*/
                if(trim(txt_title.value) == "")
		{
                    alert("Please enter title.");
                    txt_title.select();
                    txt_title.focus();
                    return false;
		}
		
		/*if(trim(ta_desc.value) == "")
		{
			alert("Please enter about description.");
			ta_desc.select();
			ta_desc.focus();			
			return false;
		}*/
		
		if(trim(imagefile.value) != "")
		{
			if(!checkExt(trim(imagefile.value)))
			{
			imagefile.select();
			imagefile.focus();
			return false;
			}
		}
//		if(trim(fileimage.value) == "")
//                {
//                    alert("Please Select Image");
//                    imagefile.select();
//                    imagefile.focus();
//                    return false;
//                }
//		if(txt_url.value!="")
//		{
//			if(!isValidUrl(txt_url.value))	
//			{
//					alert("Please enter valid URL.");
//					txt_url.select();
//					txt_url.focus();
//					return false;					
//			}
//		}
		
		/*if(trim(txt_url.value)!="")
		{
			if(!isValidUrl(trim(txt_url.value)))
			{
				txt_url.select();
				txt_url.focus();
				return false;			
			}
		}*/

		if(cbo_visible.value == "")
		{
			alert("Please set value for visible.");
			cbo_visible.focus();			
			return false;
		}
		
	}
	return true;
}
function frm_list_check_displayorder()
{
	with(document.frm_list)
	{
		cnt=hdn_counter.value;
		for(i=1;i<cnt;i++)
		{
			if(trim(eval("txt_displayorder" + i).value) =="")
			{
				alert("Please enter display order.");
				eval("txt_displayorder" + i).select();
				eval("txt_displayorder" + i).focus();
				return false;
			}
			
			if(eval("txt_displayorder" + i).value <0)
			{
				alert("Please enter positive integer for display order.");
				eval("txt_displayorder" + i).select();
				eval("txt_displayorder" + i).focus();
				return false;
			}
			
			if(isNaN(eval("txt_displayorder" + i).value))
			{
				alert("Please enter numeric value for display order.");
				eval("txt_displayorder" + i).select();
				eval("txt_displayorder" + i).focus();
				return false;
			}
		}
	}
	return true;
}

function confirm_delete()
{
if(confirm("Are you sure you want to delete this details?"))
	{
		if(confirm("Confirm Deletion:Click 'Ok' to delete this details or 'Cancel' to deletion."))
		{
			return true;
		}
	}
	return false;
}

function show_details(Url)
{
		window.open(Url,'About','left=50,top=20,scrollbars=yes,resizable=yes,width=570,height=490');
		return false;
}


