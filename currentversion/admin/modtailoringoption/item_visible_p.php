<?php 
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
//include "./item_app_specific.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
//include "../../item_app_specific.php";
#------------------------------------------------------------------------------
#Get values of all passed GET / POST variables		
$int_pkid = 0;
$str_mode = "";

if(isset($_GET["pkid"]))
{
    $int_pkid = trim($_GET["pkid"]);
}//print $int_pkid; exit;
if(!is_numeric($int_pkid) || $int_pkid == "" || $int_pkid < 0 )
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#potp");
    exit();
}
$cat_flag="";
if(isset($_GET["catflag"])==true)
{
	$cat_flag=trim($_GET["catflag"]);
}
if($cat_flag=="" || ($cat_flag!="c" && $cat_flag!="s"))
{
	CloseConnection();
	Redirect("item_list.php?msg=F&type=E&#ptop");
	exit();
}

#---------------------------------------------------------------------------------------------------------
# Select Query
if($cat_flag=="c")
{
#update in table

	#select query to get details from table
	$str_query_select="";
	$str_query_select="SELECT title,visible FROM ".$STR_DB_TABLE_NAME." WHERE pkid=". $int_pkid;
	$rs_visible=GetRecordSet($str_query_select);
	
	if($rs_visible->eof())
	{
		CloseConnection();
		Redirect("item_list.php?msg=F&type=E&#ptop");
		exit();
	}

	$str_mode="";
	$str_title="";
	$str_mode=strtoupper(trim($rs_visible->fields("visible")));
	$str_title=trim($rs_visible->fields("title"));
	
	#----------------------------------------------------------------------------------------------------
	#change visible mode
	$str_mode_title="";
	if(strtoupper($str_mode)=="YES")
	{
			$str_mode="NO";
			$str_mode_title="Invisible";
	}
	else if(strtoupper($str_mode)=="NO")
	{
			$str_mode="YES";
			$str_mode_title="Visible";
	}
	#----------------------------------------------------------------------------------------------------
	#update status in table
	$str_query_update="";
	$str_query_update="UPDATE ".$STR_DB_TABLE_NAME." SET visible='" . ReplaceQuote($str_mode) . "' WHERE pkid=".$int_pkid;
	ExecuteQuery("$str_query_update");
        #-----------------------------------------------------------------------------------------------------------
        #write xml file
        #----------------------------------------------------------------------------------------------------
	#Close connection and redirect to page	
		CloseConnection();
		Redirect("item_list.php?type=S&msg=V&title=".urlencode(RemoveQuote($str_title))."&mode=". urlencode(RemoveQuote($str_mode_title)) ."&#ptop");
		exit();
	#------------------------------------------------------------------------------------------------------------
}
else if($cat_flag=="s")
{
#update in table

	#select query to get details from table
	$str_query_select="";
	$str_query_select="SELECT title,visible FROM ".$STR_DB_TR_TABLE_NAME." WHERE pkid=". $int_pkid;
	$rs_visible=GetRecordSet($str_query_select);
	
	if($rs_visible->eof())
	{
		CloseConnection();
		Redirect("item_list.php?msg=F&type=E&#ptop");
		exit();
	}

	$str_mode="";
	$str_title="";
	$str_mode=strtoupper(trim($rs_visible->fields("visible")));
	$str_title=trim($rs_visible->fields("title"));
	
	#----------------------------------------------------------------------------------------------------
	#change visible mode
	$str_mode_title="";
	if(strtoupper($str_mode)=="YES")
	{
			$str_mode="NO";
			$str_mode_title="Invisible";
	}
	else if(strtoupper($str_mode)=="NO")
	{
			$str_mode="YES";
			$str_mode_title="Visible";
	}
	#----------------------------------------------------------------------------------------------------
	#update status in table
	$str_query_update="";
	$str_query_update="UPDATE ".$STR_DB_TR_TABLE_NAME." SET visible='" . ReplaceQuote($str_mode) . "' WHERE pkid=".$int_pkid;
	ExecuteQuery("$str_query_update");
        #-----------------------------------------------------------------------------------------------------------
        #write xml file
        // WriteXML();	
        #----------------------------------------------------------------------------------------------------
	#Close connection and redirect to page	
	CloseConnection();
	Redirect("item_list.php?type=S&msg=V&title=".urlencode(RemoveQuote($str_title))."&mode=". urlencode(RemoveQuote($str_mode_title)) ."&#ptop");
	exit();
	#------------------------------------------------------------------------------------------------------------
}
?>