<?php
/*
File Name  :-item_cms.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_CMS."";
$rs_list = GetRecordSet($str_query_select); 
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";

#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Diplay message.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("E"): $str_message = $STR_MSG_ACTION_EDIT; break;
    }
}
#------------------------------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?>: <?php print($STR_TITLE_PAGE); ?>  (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<a name="ptop"></a>
<div class="container">
	<?php include "../../includes/adminheader.php"; ?>
	<div class="row padding-10">
            <div class="col-md-3 button_space">
		<div class="btn-group" role="group" aria-label="...">
                    <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
		</div>
            </div>
            <div class="col-md-9" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)</h3></div>
	</div>
	<hr>
	<?php if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); } ?>
	<div class="row padding-10">
            <div class="col-md-12">
		<div class="panel-group" id="accordion">
                    <div class="panel panel-default">
			<div class="panel-heading">
                            <h4 class="panel-title">
				<i class="fa fa-edit"></i>&nbsp;&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b>
                                <?php print($STR_LINK_HELP); ?>
                            </h4>
			</div>
                                    
                    <div id="collapse01" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <form name="frm_edit" action="item_cms_p.php" method="post" role="form">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="form-group">
                                        <label>Description</label>
                                            <textarea name="ta_desc" id="ta_desc" rows="20" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>" ><?php print(RemoveQuote($rs_list->fields("description"))); ?></textarea><span class="text-help"></span>
                                    </div>
                                <div class="form-group">
                                    <label>Visible</label><span class="text-help-form"> * (<?php print($STR_MSG_VISIBLE);?>) </span>
                                    <select name="cbo_visible" class="form-control input-sm" >
                                    <option value="YES" <?php print(CheckSelected("YES",strtoupper($rs_list->fields("visible"))));?>>YES</option>
                                    <option value="NO" <?php print(CheckSelected("NO",strtoupper($rs_list->fields("visible"))));?>>NO</option>
                                  </select>
								</div>
                                    <input type="hidden" name="hdn_pkid" value="<?php print($rs_list->fields("pkid")); ?>">
				    <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
			</div>
                    </div>
                    </div>
                    </div>
		</div>
	</div>
        <?php include "../../includes/help_for_edit.php"; ?>
</div>
<?php include "../../includes/include_files_admin.php"; ?>
</body></html>
