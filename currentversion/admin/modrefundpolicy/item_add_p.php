<?php
/*
File Name  :- item_add_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_button="";
$str_ques="";
$str_desc="";
$str_visible="";
$str_image="";
if(isset($_POST['hdnSubmit']))
{
    $int_button=trim($_POST['hdnSubmit']);
}	
/*if($int_button<=0 || $int_button=="")
{
    CloseConnection();
    Redirect("privacy_list.php?msg=F&type=E");
    exit();
}*/
if(isset($_POST['txt_title']))
{
    $str_ques=trim($_POST['txt_title']);
}
if(isset($_POST['ta_desc']))
{
    $str_desc=trim($_POST['ta_desc']);
}
if(isset($_FILES['fileimage']))
{
    $str_image=trim($_FILES['fileimage']['name']);
}
if(isset($_POST['cbo_visible']))
{
    $str_visible=trim($_POST['cbo_visible']);
}
#----------------------------------------------------------------------------------------------------------------
#Prepare query string to pass variables back 
$str_redirect="";
$str_redirect="&question=".RemoveQuote(urlencode($str_ques))."";
$str_redirect=$str_redirect."&desc=".RemoveQuote(urlencode($str_desc))."";
$str_redirect=$str_redirect."&visible=".RemoveQuote(urlencode($str_visible));
#-----------------------------------------------------------------------------------------------------------------
#Check all validation
if($str_ques == "" || $str_visible == "" || $str_desc == "")
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_redirect);
    exit();
}
if($str_image!="" )
{
    //here we cannot use directly name of image file as first argument.
    // instead we have to specify $_FILES['filename']['tmp_name'].
    if(ValidateImageExtension($_FILES['fileimage']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
        CloseConnection();
        Redirect("item_list.php?msg=I&type=E".$str_redirect);
        exit();
    }
/* if(CheckFileExtension($str_image,$STR_IMG_FILE_TYPE_VALIDATION)==0 )
    {
        CloseConnection();
        Redirect("item_add.php?msg=I&type=E".$str_redirect);
        exit();
    }
 */}	
#---------------------------------------------------------------------------------------------------------	
#Upload image
$str_thumb_file_name="";
$str_thumb_path="";
if($str_image!="")
{
    $str_thumb_file_name=GetUniqueFileName()."_t.".getextension($str_image);
    $str_thumb_path = trim($UPLOAD_IMG_PATH."/".$str_thumb_file_name);
    UploadFile($_FILES['fileimage']['tmp_name'],$str_thumb_path);
    //ResizeImage($str_thumb_path,$INT_PHOTO_THUMB_WIDTH);
}	
#---------------------------------------------------------------------------------------------------------------------
#Select query to find maximum display order
$str_max="";
$str_max=GetMaxValue($STR_DB_TABLE_NAME,"displayorder");

#Insert query
$str_query_insert="";
$str_query_insert="INSERT INTO ".$STR_DB_TABLE_NAME. " (title,description,imagefilename,visible,displayorder) VALUES(";
$str_query_insert=$str_query_insert."'".ReplaceQuote($str_ques)."',";
$str_query_insert=$str_query_insert."'".ReplaceQuote($str_desc)."',";
$str_query_insert=$str_query_insert."'".ReplaceQuote($str_thumb_file_name)."',";
$str_query_insert=$str_query_insert."'".ReplaceQuote($str_visible)."','".$str_max."')";
ExecuteQuery($str_query_insert);
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#----------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=S&type=S&tit=".urlencode(RemoveQuote($str_ques)));
exit();
?>
