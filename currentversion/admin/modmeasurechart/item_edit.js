function frm_edit_validate()
{
    with(document.frm_edit)
    {
        if(trim(fileimage.value) != "")
        {
            if(checkExt(trim(fileimage.value))==false)
            {
                fileimage.select();
                fileimage.focus();
                return false;
            }
        }  

     /*   if(trim(txt_url.value) != "")
        {
            if(!validateURL_v02(trim(txt_url.value)))	
            {
                alert("Please enter valid url.");
                txt_url.select();
                txt_url.focus();
                return false;					
            }
        } */
        /*if(trim(cbo_display_button.value) != "NON" && trim(txt_caption) == "")
        {
            alert("Please enter valid caption for selected button.");
            return false;
        }*/
    }
    return true;
}

function confirm_delete()
{
    if(confirm("Are you sure you want to delete this image?"))
    {
        if(confirm("Confirm Deletion: Click 'OK' to delete this image or click 'Cancel' to cancel deletion."))
        {
            return true;
        }
    }
    return false;
}

// JavaScript Document