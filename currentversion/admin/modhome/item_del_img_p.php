<?php 
/*
File Name  :- item_del_img_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#-----------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
#-----------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid = 0;
$str_query_select = "";

if(isset($_GET["pkid"]))
{
    $int_pkid = trim($_GET["pkid"]);
}
//print $int_pkid;exit;
if( $int_pkid == "" || !is_numeric($int_pkid) || $int_pkid < 0 )
{
    CloseConnection();	  
    Redirect("item_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid));
    exit();
}
#-----------------------------------------------------------------------------------
#Select Query
$str_query_select = "";
$str_query_select = "SELECT title,imagefilename FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
//	print $str_query_select;exit;
$rs_list = GetRecordSet($str_query_select);

if($rs_list->eof())
{
    CloseConnection();	  
    Redirect("item_edit.php?msg=F&type=E");
    exit();
}
$str_title = "";
$str_title = $rs_list->Fields("title");

$str_thumb_file = "";
$str_thumb_file = $rs_list->Fields("imagefilename");

//Delete image from server 
if(trim($str_thumb_file) != "")
{
    DeleteFile($UPLOAD_IMG_PATH.$str_thumb_file);
}
#-----------------------------------------------------------------------------------------------------------------------
# Update Query
$str_query_update = "";
$str_query_update = "UPDATE " .$STR_DB_TABLE_NAME. " SET imagefilename='' WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------------------
#Close connection and redirect to it's relevant location
CloseConnection();	  
Redirect("item_edit.php?msg=DEL&type=S&pkid=".urlencode($int_pkid)."&tit=".urlencode(RemoveQuote($str_title)));
exit();	 
?>