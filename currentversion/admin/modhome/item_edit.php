<?php 
/*
File Name  :- item_edit.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "./item_app_specific.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
#------------------------------------------------------------------------------
#Select query to get records from
$int_pkid="";
if(isset($_GET['pkid']))
{
    $int_pkid=trim($_GET['pkid']);
}

$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
//print $str_query_select;exit;
$rs_list = GetRecordSet($str_query_select); 

#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
# Diplay message.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("E"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("DEL"): $str_message = $STR_MSG_ACTION_DELETE_IMAGE; break;
        case("LINK"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;
        case("IINV"): $str_message = "All images can't be INVISIBLE, Please keep atleast one image visible."; break;
        case("IDE"): $str_message = "Please Select Image.";	break;
        case("DBC"): $str_message = "Please select button type and enter valid caption for selected button.";	break;
        case("DU"): $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?>: <?php print($STR_TITLE_EDIT); ?> </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<!-- Page Content -->
<a name="ptop"></a>
<div class="container">
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?><a href="#help"  class="btn btn-info " title="Click to view help"><i class="fa fa-info-circle"></i>&nbsp; Help</a> <?php */?>
                    <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Back to State List"><i class="fa fa-chevron-left"></i>&nbsp;Back</a><?php */?>
                <a href="./item_list.php" title="<?php print($STR_TITLE_GO_TO); ?> <?php print($STR_TITLE_PAGE); ?>" class="btn btn-default"><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
            </div>
        </div>
        <div class="col-md-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE); ?></h3></div>
    </div><hr>
	<?php	if($str_type != "" && $str_message != "") { print(DisplayMessage("99%",$str_message,$str_type)); } ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <div class="row padding-10">
                                <div class="col-md-6 col-sm-6 col-xs-8"><i class="fa fa-edit "></i>&nbsp;&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b></div>
                                <div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>	
                            </div>
                        </h4>
                    </div>
                    <div id="collapse01" class="panel-collapse">
                        <div class="panel-body">
                            <form name="frm_edit" action="item_edit_p.php" method="post" onSubmit="return frm_edit_validate();" enctype="multipart/form-data" role="form">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="form-group">
                                    <label>Title</label><span class="text-help-form"></span>
                                    <input id="txt_title" name="txt_title" class="form-control input-sm" maxlength="512" value="<?php print(RemoveQuote($rs_list->fields("title"))); ?>" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" type="text" >
                                </div>
                                <div class="form-group">
                                    <label>Description</label><span class="text-help-form"></span>
                                    <textarea name="ta_desc" id="ta_desc" rows="10" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>" ><?php print(RemoveQuote($rs_list->fields("description"))); ?></textarea>
                                    <span class="text-help-form"><?php //print($STR_MAX_255_MSG);?></span>	
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Existing Image</label> 
                                                <?php if($rs_list->fields("imagefilename")!="") { ?>
                                                    <?php /* ?><a href="./item_del_img_p.php?pkid=<?php print($rs_list->fields("pkid"));?>" title="<?php print($STR_HOVER_DELETE_IMAGE)?>" onClick="return confirm_delete();" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a><br> <?php */ ?>

                                                    <a href="#" data-toggle="modal" data-target=".profile-<?php print($rs_list->fields("pkid")); ?>" rel="thumbnail" ><img class="img-responsive" title="<?php print $STR_HOVER_IMAGE; ?>" src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>" border="0" alt="<?php print $rs_list->fields("imagefilename"); ?>" align="top" class="align-middle"></a>
                                                    <div class="modal fade profile-<?php print($rs_list->fields("pkid")); ?>" align="center"  role="dialog" aria-labelledby="" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                                    <img src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>" class="img-responsive img-rounded" title="<?php print $STR_HOVER_IMAGE; ?>">
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div>
                                                    <?php }
                                            else {  print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); } ?> 

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Upload New Image</label>&nbsp;
                                            <span class="text-help-form"> (<?php print($STR_MSG_IMG_FILE_TYPE);?>&nbsp;|
                                             <?php print($STR_MSG_IMG_UPDATE);?>)</span>
                                            <input type="file" name="fileimage" >
                                            <input name="hdn_existing_image" type="hidden" id="hdn_existing_image" value="<?php print($rs_list->fields("imagefilename")); ?>">
                                        </div>
                                    </div>
                                </div>  
                                 <div class="row padding-10">
                                    
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>URL Title</label>
                                            <input class="form-control input-sm" name="txt_urltitle" id="txt_urltitle"  maxlength="512" value="<?php print(RemoveQuote($rs_list->fields("urltitle"))); ?>"  placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" >
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>URL</label>
                                            <input class="form-control input-sm" name="txt_url" id="txt_url"  maxlength="512" value="<?php print(RemoveQuote($rs_list->fields("photourl"))); ?>"  placeholder="<?php print $STR_MSG_URL_FORMAT; ?>" >
                                        </div>
                                    </div>
                                    
                                    </div>
                                
                                 <div class="row padding-10">
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Open URL In New Window </label>&nbsp;<span class="text-help-form">(<?php print($STR_MSG_OPEN_IN_NEW_WINDOW); ?>)</span>
                                            <select name="cbo_window" class="form-control input-sm" >
                                                <option value="YES" <?php print(CheckSelected("YES",strtoupper($rs_list->fields("openurlinnewwindow")))); ?>>YES</option>
                                                <option value="NO" <?php print(CheckSelected("NO",strtoupper($rs_list->fields("openurlinnewwindow")))); ?>>NO</option></select>
                                        </div>
                                    </div>
                                     
                            <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Hover Text</label>
                                            <input class="form-control input-sm" name="txt_hover" id="txt_hover"  maxlength="512" value="<?php print(RemoveQuote($rs_list->fields("hovertext"))); ?>"  placeholder="<?php print $STR_PLACEHOLDER_HOVER_TEXT; ?>" >
                                        </div>
                                    </div>  
                                </div>  
                                
                                 
				<?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                <input type="hidden" name="hdn_pkid" value="<?php print($rs_list->fields("pkid")); ?>" >
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php include "../../includes/help_for_edit.php"; ?>
</div>
    <!-- /.container -->											
<!-- Bootstrap Core CSS -->
<script language="JavaScript" src="./item_edit.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
    
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_desc');
		//new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
  });
		// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
		 // convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_desc');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>
</body></html>