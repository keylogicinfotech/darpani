<?php
$STR_TITLE_PAGE = "Wish List";

$UPLOAD_IMG_PATH="../../mdm/wishlist/";
$INT_IMG_WIDTH = 550;
$INT_IMG_HEIGHT = 750;

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "t_wish"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY displayorder DESC "; 

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "cms_wishlist";

global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/wishlist.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/wishlist_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";	

?>


