function frm_add_validateform()
{
    with(document.frm_add)
    {
        if(trim(txt_title.value) == "")
        {
            alert("Please enter title.");
            txt_title.select();
            txt_title.focus();
            return false;
        }
        if(trim(ta_desc.value) == "")
        {
            alert("Please enter description.");
            ta_desc.select();
            ta_desc.focus();
            return false;
        } 
        if(cbo_visible.value == "")
        {
            alert("Please set value for visible.");
            cbo_visible.focus();			
            return false;
        }
         if(trim(fileimage.value) != "")
        {
            if(checkExt(trim(fileimage.value))==false)
            {
                fileimage.select();
                fileimage.focus();
                return false;
            }
        }
    }
	return true;
}


function frmAdd_Validate(no)
{
    if(no==1)
    {
        document.frm_add.hdnSubmit.value=1;
    }
    else 
    {
        document.frm_add.hdnSubmit.value=2;
    }
    return true;
}