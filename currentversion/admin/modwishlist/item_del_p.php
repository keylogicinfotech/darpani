<?php 
/*
File Name  :- item_del_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "./item_app_specific.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
#-----------------------------------------------------------------------------------------------------------------------------------------------------
$int_pkid="";
$str_title="";
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#To check whether all values are passed properly or not.	
if(isset($_GET["pkid"]))
{
    $int_pkid=trim($_GET["pkid"]);
}
if($int_pkid=="")
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
if (!is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}

$str_query_select="";
$str_query_select="SELECT imagefilename FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
$rs_list=GetRecordset($str_query_select);
$str_large_image=$rs_list->Fields("imagefilename");
#-------------------------------------------------------------------------------------------------------------------	
#delete the images
	
//print "<br/><br/>".$UPLOAD_HOME_IMAGE_PATH.trim($str_large_image);
//exit;
if(trim($str_large_image)!="")
{
    DeleteFile($UPLOAD_IMG_PATH.trim($str_large_image));
}
	
#---------------------------------------------------------------------------------------------------------------------
#Delete query
$str_query_update="";
$str_query_update="DELETE from " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------
#write xml file
WriteXML();			
#-----------------------------------------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=D&type=S&#ptop");
exit();
?>
