<?php
/*
File Name  :- item_del_p.php
Create Date:- 18-JAN-2019
Intially Create By :- 0022
Update History:
*/
#-----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
include "item_app_specific.php";
#-----------------------------------------------------------------------------------------------------	
#Get values of all passed GET / POST variables
$int_pkid=0;
if(isset($_GET['pkid']))
{
    $int_pkid=$_GET['pkid'];
}
if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F");
    exit();
}
#-----------------------------------------------------------------------------------------------------	
#Select query to get the details from table
$str_query_select="";
$str_query_select="SELECT title,imagefilename FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
$rs_list=GetRecordset($str_query_select);
if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F");
    exit();
}
$str_title=$rs_list->Fields("title");
$str_thumb_image=$rs_list->Fields("imagefilename");
#-----------------------------------------------------------------------------------------------------	
#Delete image file and then delete data from table
if(trim($str_thumb_image)!="")
{
    DeleteFile($UPLOAD_IMG_PATH.trim($str_thumb_image));
}
$str_query_update="";
$str_query_update="DELETE FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?type=S&msg=D&tit=".urlencode(RemoveQuote($str_title)));
exit();
#-----------------------------------------------------------------------------------------------------
?>	
	
