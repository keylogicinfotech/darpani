<?php
/*
	Module Name:- Content Management Module
	File Name  :-cm_terms_p.php
	Create Date:- 15-Apr-2006
	Intially Create By :- 0026
	Update History:
*/
#------------------------------------------------------------------------------
#	Include files
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "item_config.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
	include "../includes/lib_xml.php";
#----------------------------------------------------------------------------------------------------------------------------------------
# get all the passed values
	$int_pkid="";
	$str_desc="";
	
	if(isset($_POST['ta_desc']))
	{
		$str_desc=trim($_POST['ta_desc']);
	}
	if(isset($_POST['hdn_pkid']))
	{
		$int_pkid=trim($_POST['hdn_pkid']);
	}
#---------------------------------------------------------------------------------------------------------------------------------------	
#select query to get details from cm_guest_book table
	$str_select_query="";
	$str_select_query="SELECT * from cm_terms";
	$rs_select_details=GetRecordset($str_select_query);
	
	if($rs_select_details->eof())
	{
			CloseConnection();
			Redirect("cm_item.php?msg=F&type=E");
			exit();
	}
#----------------------------------------------------------------------------------------------------------------------------------------
#update query to set values in cm_guest_book table
$str_update_query="UPDATE cm_terms SET description='".ReplaceQuote($str_desc)."' WHERE termspkid=".$int_pkid;
ExecuteQuery($str_update_query);
#----------------------------------------------------------------------------------------------------------------------------------------	
#Writing data of table into XML file
	#creating recordset.
	$str_query_select = "";
	$str_query_select = "SELECT * FROM cm_terms";
	$rs_xml_list = GetRecordSet($str_query_select);

	#storing recordset data into array.
	if(!$rs_xml_list->eof())
	{
		$arr=array();
		$arr1=array();
		$i=0;
		while(!$rs_xml_list->eof())
		{
			$arr[$i]=$rs_xml_list->fields("termskey")."_description";
			$arr1[$i]=$rs_xml_list->fields("description");
			$i=$i+1;
			$rs_xml_list->MoveNext();
		}		
		#writting data to the file
		writeXmlFile($XML_TERMS_CMS_FILE_PATH,$XML_TERMS_CMS_ROOT_TAG,$arr,$arr1);
	}
#----------------------------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("cm_item.php?msg=E&type=S");
exit();
?>