<?php
/*
File Name  :- item_del_img_p.php
Create Date:- 18-JAN-2019
Intially Create By :- 0022
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid="";
$str_query_select = "";
if(isset($_GET["pkid"]))
{
    $int_pkid = trim($_GET["pkid"]);
}
if( $int_pkid == "" || !is_numeric($int_pkid) || $int_pkid < 0 )
{
    CloseConnection();	  
    Redirect("item_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid));
    exit();
}
#----------------------------------------------------------------------------------------------------
#Select query to get records from table
$str_query_select = "";
$rs_list = "";				
$str_query_select = "SELECT title,imagefilename FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
$rs_list = GetRecordSet($str_query_select);
if($rs_list->eof())
{
    CloseConnection();	  
    Redirect("item_edit.php?msg=F&type=E");
    exit();
}
$str_title=$rs_list->Fields("title");
$str_image_file_name=$rs_list->Fields("imagefilename");

#Delete image file
if(trim($str_image_file_name)!="")   
{
    DeleteFile($UPLOAD_IMG_PATH.$str_image_file_name);
}
#----------------------------------------------------------------------------------------------------
#Update the fields which store filename with null values
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME." SET imagefilename='' WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#----------------------------------------------------------------------------------------------------	 
CloseConnection();	  
Redirect("item_edit.php?msg=DEL&type=S&pkid=".urlencode($int_pkid)."&tit=".urlencode(RemoveQuote($str_title)));
exit();
#----------------------------------------------------------------------------------------------------
?>
