<?php
/*
	Module Name:- Content Management Module
	File Name  :-cm_terms.php
	Create Date:- 20-june-2016
	Intially Create By :- 0026
	Update History:
*/
#------------------------------------------------------------------------------
#	Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
//include "cm_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#	Select query to get records from cm_journal
	$str_query_select = "SELECT * FROM cm_terms";
	$rs_home = GetRecordSet($str_query_select); 
#	Initialization of variables used for message display.   
		$str_type = "";
		$str_message = "";
#	Get message type.
		if(isset($_GET["type"]))
		{
			switch(trim($_GET["type"]))
			{
				case("S"): $str_type = "S"; break;
				case("E"): $str_type = "E"; break;
				case("W"): $str_type = "W"; break;
			}
		}
#	Diplay message.
		if(isset($_GET["msg"]))
		{
			switch(trim($_GET["msg"]))
			{
				case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
				case("E"): $str_message = $STR_MSG_ACTION_UPDATE; break;
			}
		}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_FORM_UPDATE); ?> : <?php print($STR_TITLE_TERMS_CONDITIONS); ?>  (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<!-- Page Content -->
<a name="ptop"></a>
<div class="container">
<?php include "../../includes/adminheader.php"; ?>
	<div class="row">
		<div class="col-lg-3 button_space">
			<div class="btn-group" role="group" aria-label="...">
				<a href="./item_list.php" class="btn btn-default" title="<?php print($STR_HREF_TITLE_LINK);?> <?php print($STR_TITLE_TERMS_CONDITIONS); ?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_TERMS_CONDITIONS); ?></a>
			</div>
		</div>
		<div class="col-lg-9" align="right" ><h3><?php print($STR_TITLE_FORM_UPDATE); ?> : <?php print($STR_TITLE_TERMS_CONDITIONS); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)</h3></div>
	</div><hr>
	<?php	if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); } ?>
	<div class="row">
    	<div class="col-lg-12">
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<div class="row">
								<div class="col-md-12  padding-10">
									<div class="col-md-6 col-xs-8"><i class="glyphicon  glyphicon-edit "></i>&nbsp;&nbsp;<?php print($STR_TITLE_FORM_UPDATE); ?></div>
									<div class="col-md-6 col-xs-4" align="right"><a href="#help"  class="link" title="Click to view help"><i class="glyphicon glyphicon-info-sign "></i>&nbsp; HELP</a></div>	
								</div>
							</div>
						</h4>
					</div>
					<div id="collapse01" class="panel-collapse collapse in">
						<div class="panel-body">
							<form name="frm_terms_book" action="cm_item_p.php" method="post" role="form" onSubmit="return frm_cm_terms_validateform()">
								<div class="HelpText" align="right"><?php print($STR_MANDATORY); ?></div>
								<div class="form-group">
									<label>Description</label>
										<textarea name="ta_desc" id="ta_desc" cols="100" rows="20" class="form-control input-sm" placeholder="Enter description here" tabindex="1"><?php print(RemoveQuote($rs_home->fields("description"))); ?></textarea><span class="HelpText"><?php //print($STR_MAX_255_MSG);?></span>	
								</div>						
								<button type="submit" class="btn btn-success" name="btn_submit_add" id="btn_submit_add2" title="<?php print($STR_BUTTON_TITLE_FORM_EDIT);?>" value="Add " tabindex="2"><span class="glyphicon  glyphicon-edit " aria-hidden="true"></span>&nbsp;Edit</button>&nbsp;
								<input name="btn_reset" id="btn_reset" title="<?php print($STR_BUTTON_TITLE_FORM_RESET);?>" value="Reset" class="btn btn-danger" type="reset" tabindex="3">
								<input type="hidden" name="hdn_pkid" value="<?php print($rs_home->fields("termspkid")); ?>" >		
							</form>
            			</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include "../../includes/help_for_edit.php"; ?>
    <!-- /.container -->											
</div>
<!-- Bootstrap Core CSS -->
<link href="../../includes/bootstrap.min.css" rel="stylesheet">
<link href="../../includes/admin.css" rel="stylesheet">
<!-- jQuery -->
<script src="../../includes/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="../../includes/bootstrap.min.js"></script>
<script language="JavaScript" src="../../includes/functions.js" type="text/javascript"></script>
<script type="text/javascript" src="../../includes/richetxteditor.js"></script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 400}).panelInstance('ta_desc');
		//new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
  });
		// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
		 // convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_desc');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>
</body></html>