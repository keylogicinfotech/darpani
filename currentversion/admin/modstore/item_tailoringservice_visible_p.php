<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
//include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$str_redirect_page_flag = "";
if(isset($_POST["flg"]) && trim($_POST["flg"])!="" )
{ $str_redirect_page_flag=trim($_POST["flg"]); }

$int_cat_pkid = 0;
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ $int_cat_pkid = trim($_GET["catid"]); }

$str_name_key = "";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_name_key = trim($_GET["key"]); }

# POST data for paging
$int_page = 0;
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page = 1; }

/*$int_userpkid = 0;
if(isset($_GET['uid']) && $_GET['uid']!= "")
{
    $int_userpkid=$_GET['uid'];
}*/

$int_masterpkid = 0;
if(isset($_GET['masterpkid']))
{
    $int_masterpkid=$_GET['masterpkid'];
}

if($int_masterpkid <= 0 || !is_numeric($int_masterpkid) || $int_masterpkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F");
    exit();
}
//print_r($_GET); exit;

$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&pkid=".$int_masterpkid;


$int_pkid = 0;
if(isset($_GET['pkid']))
{
    $int_pkid=$_GET['pkid'];
}
if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
{
    CloseConnection();
    Redirect("item_tailoringservice_list.php?type=E&msg=F".$str_filter);
    exit();
}

#----------------------------------------------------------------------------------------------------
#Select query to get the details from table
$str_query_select="";
$str_query_select="SELECT visible FROM ".$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE." WHERE pkid=".$int_pkid;
$rs_list = GetRecordset($str_query_select);
if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_tailoringservice_list.php?type=E&msg=F".$str_filter);
    exit();
}
$str_visible=$rs_list->Fields("visible");
if(strtoupper($str_visible)=='YES')
{
    $str_visible = "NO";
    $str_visible_title = "Invisible";
}
else if(strtoupper($str_visible) == 'NO')
{
    $str_visible = "YES";
    $str_visible_title ="Visible";
}
#-----------------------------------------------------------------------------------------------------
#Update query to change the mode
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE." SET visible='" .ReplaceQuote($str_visible). "' WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------
#Write to xml file
//WriteXml();
#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_tailoringservice_list.php?type=S&msg=V&mode=".urlencode(RemoveQuote($str_visible)).$str_filter);
exit();
#-----------------------------------------------------------------------------------------------------
?>
