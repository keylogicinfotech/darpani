<?php 
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_GET);
$int_catpkid = 0;
if(isset($_GET["catid"]))
{
    $int_catpkid = $_GET["catid"];
}
if($int_catpkid == 0 ||  $int_catpkid == "" || !is_numeric($int_catpkid))
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E".$str_filter);
    exit();
}

# Select Query
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_CAT." WHERE catpkid=".$int_catpkid;
//print $str_query_select;
$rs_list_cat = GetRecordset($str_query_select);

$str_category = "";
$str_category = $rs_list_cat->Fields("title");

#-----------------------------------------------------------------------------------------
# Select Query
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_TAILORING_OPTION." WHERE visible='YES' ".$STR_DB_TABLE_NAME_ORDER_BY_TAILORING_OPTION."";
//print $str_query_select;
$rs_list = GetRecordset($str_query_select);



$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_CATWISE_TAILORING_OPTION." WHERE catpkid=".$int_catpkid."";
//print $str_query_select; exit;
$rs_list_selected_options = GetRecordSet($str_query_select);


$arr_selected_option = array();
while(!$rs_list_selected_options->EOF())
{
    array_push($arr_selected_option, $rs_list_selected_options->Fields("optionpkid")); 
    $rs_list_selected_options->MoveNext();
}
//print_r($arr_selected_option);

//print $rs_list_selected_options->Count();

#------------------------------------------------------------------------------------------
#getting query string variables	
$str_sub="";
$str_desc="";
$str_visible="";
$str_pos="";
if(isset($_GET['question'])) { $str_sub=trim($_GET['question']); }
if(isset($_GET['desc'])) { $str_desc=trim($_GET['desc']); }
if(isset($_GET["visible"])) { $str_visible=trim($_GET["visible"]); }
if (isset($_GET["position"])){ $str_pos = trim($_GET["position"]);  }
if (isset($_GET["po_mode"])){$str_pos = trim($_GET["po_mode"]);}
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type="";
$str_message="";
$str_title="";
$str_mode="";
if(isset($_GET['tit'])) { $str_title=trim($_GET['tit']); }
if(isset($_GET['mode'])) { $str_mode=trim($_GET['mode']); }
#Get message type.
if(isset($_GET['type']))
{
    switch(trim($_GET['type']))
    {
        case("S"): $str_type = "S"; break; case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Get message text.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
        
    }
} 	
?>
<!DOCTYPE html> 
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE_TAILORING_OPTIONS);?><?php if($str_category != "") { print " [".$str_category."]"; } ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
            <a href="./item_cat_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE_CATEGORY); ?> " ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_CATEGORY); ?> </a>
            </div>
        </div>  
        <div class="col-md-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE_TAILORING_OPTIONS); ?><?php if($str_category != "") { print " [".$str_category."]"; } ?></h3></div>
    </div><hr/>
    <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <span>
                            <b><a class="accordion-toggle collapsed" title="<?php print $STR_HOVER_ADD; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><?php //print($STR_TITLE_ADD); ?> </a></b>
                            </span>
                            <?php print($STR_LINK_HELP); ?><br/>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" <?php /* ?> style="height: 0px;"<?php */ ?>>
                        <form id="frm_add" name="frm_add" enctype="multipart/form-data">
                            <div class="panel-body">
                            </div>
                        </form>
                    </div>
                </div>                        
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <form name="frm_list" action="./item_cat_tailoringoption_list_p.php" method="POST" onSubmit="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                        <th width="6%">Select</th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list->EOF()==true) {  ?>
                    <tr>
                        <td align="center" class="text-center"><?php print($int_cnt); ?><input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>"></td>
                        <td align="center" class="text-center">
                            <?php 
                            $str_checked = "";
                            if(in_array($rs_list->fields("pkid"), $arr_selected_option)) {
                            $str_checked = " checked"; }?>
                            <input type="checkbox" name="cbx_option<?php print($int_cnt);?>" <?php print($str_checked); ?>>
                        </td>
                        <td align=""><h4 class="nopadding"><b><?php print($rs_list->Fields("title")); ?></b></h4></td>
                    </tr>
                    <?php $int_cnt++; $rs_list->MoveNext(); } ?>
                    <tr>
                        
                        <td></td>
                        <td align="center">
                            <input type="hidden" name="hdn_counter" id="hdn_counter" value="<?php print($int_cnt);?>">
                            <input type="hidden" name="hdn_catpkid" id="hdn_catpkid" value="<?php print($int_catpkid);?>">
                            <?php print DisplayFormButton('SAVE', 0); ?>
                        </td>
                        <td></td>
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>
