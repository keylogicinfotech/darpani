<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/http_to_https.php";	
include "../../includes/lib_xml.php";
include "./item_config.php";
//print_r($_SESSION);exit;
#----------------------------------------------------------------------
//print_r($_GET); exit;
$str_redirect_page_flag = "";
if(isset($_GET["flg"]) && trim($_GET["flg"])!="" )
{ $str_redirect_page_flag=trim($_GET["flg"]); }

$int_cat_pkid = 0;
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ $int_cat_pkid=trim($_GET["catid"]); }

$str_key = "";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_key=trim($_GET["key"]); }

# POST data for paging
$int_page = 0;
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page=1; }

$int_userpkid = 0;
if(isset($_GET['uid']) && $_GET['uid']!= "")
{
    $int_userpkid=$_GET['uid'];
}

$int_masterpkid = 0;
if(isset($_GET['masterpkid']))
{
    $int_masterpkid=$_GET['masterpkid'];
}

if($int_masterpkid <= 0 || !is_numeric($int_masterpkid) || $int_masterpkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F");
    exit();
}
//print_r($_GET); exit;

$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_key."&PagePosition=".$int_page."&userpkid=".$int_userpkid."&pkid=".$int_masterpkid;

//print $int_masterpkid; exit;
$int_pkid = 0;
if(isset($_GET["pkid"]) && trim($_GET["pkid"])!="" )
{
    $int_pkid=trim($_GET["pkid"]);
}

if($int_pkid=="" || $int_pkid==0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_tailoringservice_list.php?msg=F&type=E&pkid=".$int_masterpkid."&#ptop");
    exit();
}


$str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE." WHERE pkid=" . $int_pkid;
//print $str_query_select; exit;
$rs_edit = GetRecordset($str_query_select);
if ($rs_edit->count()==0)
{
    CloseConnection();
    Redirect("item_tailoringservice_list.php?msg=F&type=E&pkid=".$int_masterpkid."&#ptop");
    exit();
}	
#------------------------------------------------------------------------------------------------
# Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
	case("E"): $str_type = "E"; break;
	case("W"): $str_type = "W"; break;
    }
}
#	Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"):$str_message =$STR_MSG_ACTION_INFO_MISSING;break;				
        case("DT"):$str_message =  $STR_MSG_ACTION_TITLE_ALREADY_EXIST;break;
        case("IU"):$str_message =  $STR_MSG_ACTION_INVALID_URL_FORMAT;break;
        case("DU"):$str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;	
    }
} 
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_PAGE_TAILORING_SERVICE) ;?> : <?php print($STR_TITLE_EDIT); ?></title>    
</head>
<body>
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="row padding-10">
                    <div class="col-md-3 col-sm-6 col-xs-12 "><br/>
                        <div class="btn-group" role="group" aria-label="...">
                            <a href="./item_tailoringservice_list.php?pkid=<?php print $int_masterpkid.$str_filter; ?>" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE_TAILORING_SERVICE);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_TAILORING_SERVICE);?></a>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-6 col-xs-12 " align="right" >
                        <h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE_TAILORING_SERVICE);?></h3>
                    </div>
                </div><hr/>
                <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); } ?>
                <div class="row padding-10">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <div class="row padding-10">
                                            <div class="col-md-6 col-sm-6 col-xs-8"><b><i class="fa fa-edit"></i>&nbsp;<?php print($STR_TITLE_EDIT); ?> </b></div>
                                            <div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>                                            </div>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="false" <?php /* ?> style="height: 0px;"<?php */ ?>>
                                    <div class="panel-body">
                                        <form id="frm_edit" name="frm_edit" method="POST" action="item_tailoringservice_edit_p.php" onSubmit="return frm_edit_validate();">
                                            <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                            <?php 
                                            $str_query_select = "";
                                            $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_TAILORING_SERVICE." WHERE visible='YES' ORDER BY displayorder";
                                            $rs_list_type = GetRecordSet($str_query_select); ?>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Select Tailoring Option</label><span class="text-help-form"> *</span>
                                                        <span class="text-help-form"></span>
                                                        <select id="cbo_type" name="cbo_type" class="form-control input-sm">
                                                            <option value="0">-- SELECT TAILORING OPTION --</option>
                                                            <?php 
                                                            while(!$rs_list_type->EOF()) {
                                                            ?>
                                                            <option value="<?php print $rs_list_type->Fields("pkid"); ?>" <?php print CheckSelected($rs_list_type->Fields("pkid"), $rs_edit->Fields("masterpkid")) ?>><?php print $rs_list_type->Fields("title"); ?></option>
                                                            <?php $rs_list_type->MoveNext();
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Price</label><span class="text-help-form"> *</span>
                                                        <span class="text-help-form"></span>
                                                        <input type="text" name="txt_price" id="txt_price" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="<?php print $rs_edit->Fields("price"); ?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" id="hdn_pkid" name="hdn_pkid" value="<?php print $rs_edit->Fields("pkid"); ?>" />
                                            <input type="hidden" id="hdn_masterpkid" name="hdn_masterpkid" value="<?php print $rs_edit->Fields("itempkid"); ?>" />
                                            <input type="hidden" id="catid" name="catid" value="<?php print $int_cat_pkid; ?>" />
                                            <input type="hidden" id="key" name="key" value="<?php print $str_key; ?>" />
                                            <input type="hidden" id="PagePosition" name="PagePosition" value="<?php print $int_page; ?>" />
                                            <input type="hidden" id="uid" name="uid" value="<?php print $int_userpkid; ?>" />
                                            <?php print DisplayFormButton("EDIT", 0); ?><?php print DisplayFormButton("RESET", 0); ?>                                                           </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                        
                </div> 
                <?php include "../../includes/help_for_edit.php"; ?>
            </div>
        </div>
    </div>        
    <?php include($STR_ADMIN_FOOTER_PATH); ?>
    <?php include "../../includes/include_files_admin.php"; ?>
    <script language="JavaScript" src="./item_tailoringservice_edit.js"></script>    
</body>
</html>
