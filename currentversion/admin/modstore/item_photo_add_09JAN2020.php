<?php
/*
	Module Name:- modPhotoset
	File Name  :- item_photo_add.php
	Create Date:- 08-Sep-2006
	Intially Create By :- 0014
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "item_config.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";

#---------------------------------------------------------------------------------------------------------------------------------------------------------------
	#initialize variables	
	# filter variables
	$int_mid="";

#	get filter data
	if(isset($_GET["mid"]) && trim($_GET["mid"])!="" )
	{
		$int_mid=trim($_GET["mid"]);
	}

#---------------------------------------------------------------------------------------------------------------------------------------------------------------
#initializing variables
	$int_pkid="";
	$str_visible="";
	$str_ptitle="";
	$str_purl="";
	$str_preview="YES";
	$str_access="NO";
#getting query string datas	
	if(isset($_GET['pkid']))
	{
		$int_pkid=trim($_GET['pkid']);
	}
	if(!is_numeric($int_pkid) || $int_pkid<0 || $int_pkid=="")
	{
		CloseConnection();
		Redirect("item_list.php?msg=F&type=E&mid=".$int_mid);
		exit();	
	}
	if(isset($_GET["visible"]))
	{ 
		$str_visible=trim($_GET["visible"]);
	}
	if(isset($_GET["ptitle"]))
	{ 
		$str_ptitle=trim($_GET["ptitle"]);
	}
	if(isset($_GET["purl"]))
	{ 
		$str_purl=trim($_GET["purl"]);
	}

	if (isset($_GET["preview"]))
	{
		$str_preview = trim($_GET["preview"]);
	}
	if (isset($_GET["access"]))
	{
		$str_access = trim($_GET["access"]);
	}
#select query to get photosetitle from t_photoset table		
	$select_query="";
	$select_query="SELECT title FROM t_store where pkid=".$int_pkid;
	$rs_select=GetRecordset($select_query);
	if($rs_select->eof())
	{
		CloseConnection();
		Redirect("item_list.php?msg=F&type=E&mid=".$int_mid);
		exit();
	}
?>
<html>
<head>
<title>:: Add Photo ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="../includes/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="item_photo_add.js" type="text/javascript"></script>
</head>
<link href="../includes/admin.css" rel="stylesheet" type="text/css">
<body>
<table width="778" border="0" cellpadding="0" cellspacing="0" align="center">
 <tr> 
  <td><?php include "../includes/adminheader.php";?></td>
 </tr>
</table>
<table width="778" align="center" cellpadding="0" cellspacing="0">
 <tr> 
  <td background="../images/tablebg.gif" height="390" valign="top">
   <table width="740" border="0" align="center" cellpadding="0" cellspacing="0">
   <tr><td height="20"></td></tr>
    <tr> 
     <td align="center" height="25" class="PageHeader">
	 Add Photo </td>
    </tr>
	<tr> 
     <td height="2" align="right" class="TableHeader8ptBold"></td>
   </tr>
   </table>
   <table width="740" border="0" align="center" cellpadding="2" cellspacing="0">
    <tr> 
     <td width="95%" height="20" class="DescriptionText8ptNormal"><a href="../admin_home.php" title="Go to admin main menu" class="NavigationLink">Main Menu</a>&nbsp;&gt;&gt;&nbsp;
		<a href="item_list.php?mid=<?php print($int_mid); ?>" title="Go to photoset list" class="NavigationLink">Photoset / Video List</a>&nbsp;&gt;&gt;&nbsp;
		<a href="item_photo_list.php?pkid=<?php print($int_pkid);?>&mid=<?php print($int_mid); ?>" class="NavigationLink" title="Go to photo list">Photo List: <?php print(MyHtmlEncode(MakeStringShort($rs_select->fields("ttitle"),40)));?></a>&nbsp;&gt;&gt;&nbsp;Add Photo
	 </td>
     <td width="5%" align="right" class="DescriptionText8ptNormal">&lt;&lt;&nbsp;<a href="item_photo_list.php?pkid=<?php print($int_pkid);?>&mid=<?php print($int_mid); ?>" class="NavigationLink" title="Back to photo list">Back</a></td>
	</tr>
   </table>
    <?php
#------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
   	$str_type="";
	$str_message="";
	$str_title="";
	
	if(isset($_GET['tit']))
	{
		$str_title=trim($_GET['tit']);
	}

#	Get message type.
   		if(isset($_GET['type']))
		{
			switch(trim($_GET['type']))
			{
				case("S"):
					$str_type = "S";
					break;
				case("E"):
					$str_type = "E";
					break;
				case("W"):
					$str_type = "W";
					break;
			}
		}
#	Get message text.
		if(isset($_GET['msg']))
		{
			switch(trim($_GET['msg']))
			{
				case("F"):
					$str_message = "Some information(s) missing. Please try again.";
					break;
				case("I"):
					$str_message ="Invalid photograph file extension.";
					break;
				case ("S"):
					$str_message = "Photo added successfully.";
					break;
				case("W"):
					$str_message = "Invalid url format.";
					break;	
			}
		}
#	Diplay message.
		if($str_type != "" && $str_message != "")
		{
			print(DisplayMessage(740,$str_message,$str_type));
		}
	?>
   	<br>
	<a name="ptop"></a>
   <table width="740" border="0" cellspacing="0" cellpadding="0" align="center">
   <tr>
    <td width="100%"> 
     <table width="100%" height="20" border="0" cellpadding="0" cellspacing="0">
      <tr class="TableHeader10ptBold">
       <td>Enter Photo Details</td>
	   <td align="right"><a href="#help" class="NavigationLink" title="Click to view help">Help</a></td>
      </tr>
     </table> 
	</td>
   </tr>
   <tr>
    <td>
	 <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1" class="tbl">
     <form name="frm_photo_add" action="item_photo_add_p.php?mid=<?php print($int_mid); ?>" method="post" onSubmit="return frm_photo_add_validateform()" enctype="multipart/form-data">
      <tr> 
         <td width="200" height="10"></td>
		 <td width="537"></td>
      </tr>
       <tr> 
       <td align="right" valign="top" class="FieldCaptionText8ptNormal">Upload Sample Photo For Photoset / Video:<span class="HelpText">*</span></td>
	    <td align="left" valign="top"><input type="file" name="fileimage" maxlength="255" size="70">
	   	<br><span class="HelpText"><?php print($STR_IMG_FILE_TYPE);?></span>&nbsp;&nbsp;
		<span class="HelpText"><?php print($STR_PHOTO_IMG_MSG);?></span>
		<br><span class="HelpText"><?php print($STR_IMG_UPLOAD_MSG);?></span>
	   </td>
      </tr> 
	  <tr><td height="10"></td><td></td></tr>
  	  <tr>
  	    <td height="20" align="right" valign="top" class="FieldCaptionText8ptNormal">Photographer Title:</td>
  	    <td><input type="text" name="txt_ptitle" class="clsTextBox" maxlength="255" size="70" value="<?php print(RemoveQuote($str_ptitle)); ?>">
		<br><span class="HelpText"><?php print($STR_MAX_255_MSG);?></span></td>
	    </tr> 
	<tr>
  	    <td height="20" align="right" valign="top" class="FieldCaptionText8ptNormal">Photographer URL:</td>
  	    <td><input type="text" name="txt_purl" class="clsTextBox" maxlength="1024" size="70" value="<?php print(RemoveQuote($str_purl)); ?>">
		<br>
		<span class="HelpText"><?php print($STR_MAX_1024_MSG."<br>".$STR_URL_FORMAT_MSG);?></span></td>
	    </tr>
	<tr><td height="10"></td><td></td></tr>
	
	 <?php /*?><tr> 
       <td align="right" valign="top" class="FieldCaptionText8ptNormal">Preview to Non-Members:<span class="HelpText">*</span></td>
                  <td align="left" valign="top"><select name="cbo_preview">
					  <option value="YES" <?php print(CheckSelected("YES",strtoupper($str_preview)));?>>Yes</option>
   					  <option value="NO" <?php print(CheckSelected("NO",strtoupper($str_preview)));?>>No</option>
				  </select><br><span class="HelpText"><?php print($STR_PREVIEW_MEMBER)?></span></td>
      </tr>
	  <tr> 
       <td align="right" valign="top" class="FieldCaptionText8ptNormal">Access to Non-Members:<span class="HelpText">*</span></td>
                  <td align="left" valign="top"><select name="cbo_access">
					  <option value="YES" <?php print(CheckSelected("YES",strtoupper($str_access)));?>>Yes</option>
   					  <option value="NO" <?php print(CheckSelected("NO",strtoupper($str_access)));?>>No</option>
				  </select><br><span class="HelpText"><?php print($STR_ACCESS_MEMBER)?></span></td>
      </tr>
	  <tr><td height="10"></td><td></td></tr><?php */?>
	  
	  <input type="hidden" name="cbo_preview" value="YES">
	  <input type="hidden" name="cbo_access" value="YES">
	  	 
	
	  <tr> 
       <td align="right" valign="top" class="FieldCaptionText8ptNormal">Visible:<span class="HelpText">*</span></td>
       <td align="left" valign="top"><select name="cbo_visible">
					  <option value="YES" <?php print(CheckSelected("YES",$str_visible)); ?>>Yes</option>
   					  <option value="NO" <?php print(CheckSelected("NO",$str_visible)); ?>>No</option>
				  </select><br><span class="HelpText"><?php print($STR_VISIBLE)?></span>
	  </td>
      </tr>	  
      <tr> 
       <td height="26"><input type="hidden" name="hdn_pkid" value="<?php print($int_pkid); ?>"></td>
       <td align="left">
	   	<input type="submit" name="SubmitAdd" title="click to add photo and return on add page" value="Add and return to add again" class="ButtonStyle" onClick="frmAdd_Validate(1)">&nbsp;&nbsp;
		<input type="hidden" name="hdnSubmit" value="1"> 
		<input type="submit" name="SubmitList" title="click to add photo and return on list page" value="Add and go to list" class="ButtonStyle" onClick="frmAdd_Validate(2)">&nbsp;&nbsp;<input type="Reset" title="click to Reset form " value="Reset" class="ButtonStyle">
	    </td>
      </tr>
	  </form>
     </table>
	</td>
   </tr>
   <tr><td height="10"></td></tr>
   <tr valign="top"> 
    <td>
	 <table width="100%"  border="0" cellpadding="2" cellspacing="0">
	  <tr class="TableHeader8ptBold"> 

      <td height="20" colspan="2">
	 	<table width="100%" cellpadding="0" cellspacing="0" border="0">
      	<tr class="TableHeader10ptBold"> 
       			<td>Help Section</td>
       			<td align="right"><a href="#ptop"  class="NavigationLink" title="Go to top">Top</a></td>
      	</tr>
     	</table>
	  </td>
      </tr>
	  <a name="help"></a>
      <tr> 
        <td width="3%" valign="baseline" class="HelpText">&#8226;</td>
		<td width="97%" valign="baseline" class="HelpText"><?php print($STR_MANDATORY); ?></td>     
      </tr>
     </table>
	</td>
   </tr>
  </table>
  </td>
  </tr>
  </table>
  <?php
	include "../includes/adminfooter.php";
	CloseConnection();
	?>
  </body>
  </html>
  
  