<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
include "../../includes/lib_file_system.php";
//include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$str_redirect_page_flag = "";
if(isset($_GET["flg"]) && trim($_GET["flg"])!="" )
{ $str_redirect_page_flag=trim($_GET["flg"]); }

$int_cat_pkid = 0;
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ $int_cat_pkid=trim($_GET["catid"]); }

$str_name_key = "";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_name_key=trim($_GET["key"]); }

# POST data for paging
$int_page = 0;
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page=1; }
//print $int_page; exit;

$int_userpkid = 0;
if(isset($_GET['uid']) && $_GET['uid']!= "")
{
    $int_userpkid=$_GET['uid'];
}

$int_masterpkid = 0;
if(isset($_GET['masterpkid']))
{
    $int_masterpkid=$_GET['masterpkid'];
}

if($int_masterpkid <= 0 || !is_numeric($int_masterpkid) || $int_masterpkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F");
    exit();
}
//print_r($_GET); exit;

$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&userpkid=".$int_userpkid."&pkid=".$int_masterpkid;
//print $str_filter;exit;

$int_pkid = 0;
if(isset($_GET['pkid']))
{
    $int_pkid = $_GET['pkid'];
}
//print $int_pkid; exit;
if($int_pkid <= 0 || !is_numeric($int_pkid) || $int_pkid == "")
{
    CloseConnection();
    Redirect("item_price_list.php?type=E&msg=F".$str_filter);
    exit();
}

#----------------------------------------------------------------------------------------------------
# Delete Query 
$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_PRICE." WHERE pkid=".$int_pkid;
//print $str_query_delete;exit;
ExecuteQuery($str_query_delete);
#-----------------------------------------------------------------------------------------------------
# Write to xml file
//WriteXml();
#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_price_list.php?type=S&msg=D".$str_filter);
exit();
#----------------------------------------------------------------------------------------------------- 
?>
