<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";

include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_file_system.php";
//include "./item_app_specific.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------------
#get filter parameters
#------------------------------------------------------------------------------------------------
/*$page=1;
if (isset($_POST["PagePosition"]))
{
	$page = trim($_POST["PagePosition"]);
}
// if model pkid passed then set filter criteria for modelpkid
$int_model_pkid="-1";
if (isset($_POST["modelpkid"]))
{
	$int_model_pkid = trim($_POST["modelpkid"]);
}
$str_filter_status="&modelpkid=".$int_model_pkid."&PagePosition=".$page."&";*/

#------------------------------------------------------------------------------------------------
# To distinguish different redirect pages
//print_r($_POST); exit;
$str_redirect_page_flag = "";
//print_r($_POST);exit;
if(isset($_POST["flg"]) && trim($_POST["flg"])!="" )
{ $str_redirect_page_flag=trim($_POST["flg"]); }

$int_cat_pkid = 0;
if(isset($_POST["catid"]) && trim($_POST["catid"])!="" )
{ $int_cat_pkid=trim($_POST["catid"]); }

$str_mname = "";
if(isset($_POST["key"]) && trim($_POST["key"])!="" )
{ $str_mname=trim($_POST["key"]); }

# POST data for paging
$int_page = 0;
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"])!="" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"])>0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page=1; }

$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_mname."&PagePosition=".$int_page;
//print $str_filter;exit;
$int_masterpkid=0;
if(isset($_POST["masterpkid"]))
{ $int_masterpkid=trim($_POST["masterpkid"]); }

//print $int_masterpkid;exit;
$str_modelname="";
if(isset($_POST["mname"])) { $str_modelname = trim($_POST["mname"]); }

$int_mempkid=0;
if(isset($_POST["memid"]))
{ $int_mempkid=trim($_POST["memid"]); }

$str_membername="";
if(isset($_POST["memname"])) { $str_membername = trim($_POST["memname"]); }

$int_masterpkid=0;
if(isset($_POST["masterpkid"]))
{ $int_masterpkid=trim($_POST["masterpkid"]); }

	
///initializing variables
//$int_modelpkid=0;
$int_pkid="";
$str_name="";
$str_desg="";
$str_email="";
$str_loct="";
$str_desc="";
$str_image="";
$str_urltitle="";
$str_url="";
$str_window="";
$str_display="";
///getting post datas
/*if(isset($_POST['cbo_modelpkid']))
{
    $int_modelpkid=trim($_POST['cbo_modelpkid']);
}*/

if(isset($_POST['hdn_pkid']))
{
    $int_pkid=trim($_POST['hdn_pkid']);
}
if(isset($_POST['txt_title']))
{
    $str_name=trim($_POST['txt_title']);
}
if(isset($_POST['txt_desgn']))
{
    $str_desg=trim($_POST['txt_desgn']);
}
if(isset($_POST['txt_email']))
{
    $str_email=trim($_POST['txt_email']);
}
if(isset($_POST['txt_loc']))
{
    $str_loct=trim($_POST['txt_loc']);
}
if(isset($_POST['ta_desc']))
{
    $str_desc=trim($_POST['ta_desc']);
}
if(isset($_FILES['fileimage']))
{
    $str_image=trim($_FILES['fileimage']['name']);
}
if(isset($_POST['txt_urltitle']))
{
    $str_urltitle=trim($_POST['txt_urltitle']);
}
if(isset($_POST['txt_url']))
{
    $str_url=trim($_POST['txt_url']);
}
if(isset($_POST['cbo_window']))
{
    $str_window=trim($_POST['cbo_window']);
}
if(isset($_POST['cbo_display']))
{
    $str_display=trim($_POST['cbo_display']);
}
////-------------------------------------------------------------------------------

////-------------------------------------------------------------------------------
///check all validation
////-------------------------------------------------------------------------------
if($str_desc == "" || $str_desc == "<br>")
{
    CloseConnection();
    //Redirect("tm_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid).$str_filter_status);
    Redirect("item_comment_edit.php?type=E&msg=F&pkid=".urlencode($int_pkid)."&masterpkid=".$int_masterpkid."&mname=".$str_modelname.$str_filter);
    exit();
}
/*if($str_email!="")
{
	if(!validateEmail($str_email))
	{
		CloseConnection();
		//Redirect("tm_edit.php?msg=EMAIL&type=E&pkid=".urlencode($int_pkid).$str_filter_status);
		Redirect("item_comment_edit.php?type=E&msg=EMAIL&pkid=".urlencode($int_pkid)."&masterpkid=".$int_masterpkid."&mname=".$str_modelname.$str_filter);
		exit();
	}
}	
if($str_loct!="")
{
	if(strlen(RemoveQuote($str_loct))>255)
	{
		CloseConnection();
		//Redirect("tm_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid).$str_filter_status);
		Redirect("item_comment_edit.php?type=E&msg=F&pkid=".urlencode($int_pkid)."&masterpkid=".$int_masterpkid."&mname=".$str_modelname.$str_filter);
		exit();
	}
}
if($str_image!="" )
{
	if(CheckFileExtension($str_image,$STR_IMG_FILE_TYPE_VALIDATION)==0 )
	{
		CloseConnection();
		//Redirect("tm_edit.php?msg=I&type=E&pkid=".urlencode($int_pkid).$str_filter_status);
		Redirect("item_comment_edit.php?type=E&msg=I&pkid=".urlencode($int_pkid)."&masterpkid=".$int_masterpkid."&mname=".$str_modelname.$str_filter);
		exit();
	}
}		
if($str_url != "")
{
	if(!validateURL($str_url))
	{
		CloseConnection();
		//Redirect("tm_edit.php?msg=LINK&type=E&pkid=".urlencode($int_pkid).$str_filter_status);
		Redirect("item_comment_edit.php?type=E&msg=LINK&pkid=".urlencode($int_pkid)."&masterpkid=".$int_masterpkid."&mname=".$str_modelname.$str_filter);
		exit();
	}
}*/
////-------------------------------------------------------------------------------
#Where Condition 
/*$str_where = "";
if($int_masterpkid != 0) { 
    $str_where = "AND modelpkid = ".$int_masterpkid;
}*/
////-------------------------------------------------------------------------------
///upload image
////-------------------------------------------------------------------------------
//print_r($_FILES); exit;
$str_select="";
$str_select="SELECT * FROM tr_blog WHERE pkid=".$int_pkid;
//print $str_select;exit;
$rs_select=GetRecordset($str_select);
if($rs_select->eof())
{
    CloseConnection();
    //Redirect("tm_edit.php?msg=I&type=E&".$str_filter_status);
    Redirect("item_comment_edit.php?type=E&msg=I&masterpkid=".$int_masterpkid."&mname=".$str_modelname."&masterpkid=".$int_masterpkid."&pkid=".urlencode($int_pkid).$str_filter);
    exit();
}
$str_temp_filename=$rs_select->fields("imagefilename");
$str_temp_filename_large=$rs_select->fields("largeimagefilename");
///print $str_temp_filename_large; exit;
$str_filename="";
$str_filename_large="";
//print $str_image; exit;
if($str_image!="")
{
    ///delete old small file
    if($str_temp_filename!="")
    {
        DeleteFile($UPLOAD_BLOG_IMG_FILE_PATH.trim($str_temp_filename));
    }
    ///delete old large file
    if($str_temp_filename_large!="")
    {
        DeleteFile($UPLOAD_BLOG_IMG_FILE_PATH.trim($str_temp_filename_large));
    }

    $str_temp_filename=GetUniqueFileName()."_t.".getextension($str_image);
    $str_temp_filename_large=GetUniqueFileName()."_l.".getextension($str_image);
    //print $str_temp_filename; exit;

    //$str_filename=$UPLOAD_PHOTO_PATH.$str_temp_filename;
    //$str_filename_large=$UPLOAD_PHOTO_PATH.$str_temp_filename_large;

    $str_filename=$UPLOAD_BLOG_IMG_FILE_PATH.trim($str_temp_filename);
    $str_filename_large=$UPLOAD_BLOG_IMG_FILE_PATH.trim($str_temp_filename_large);

    UploadFile($_FILES['fileimage']['tmp_name'],$str_filename_large);	
    //ResizeImage($str_filename_large,$INT_COMMENT_PHOTO_LARGE_WIDTH);
    SaveAsThumbImage($str_filename_large,$str_filename,$INT_COMMENT_PHOTO_THUMB_WIDTH);	

}
////-------------------------------------------------------------------------------

////-------------------------------------------------------------------------------
// find out the model name on the basis of modelpkid
////-------------------------------------------------------------------------------
/*$str_model_list="select * from t_model where modelpkid=".$int_modelpkid;
$rs_model_list=GetRecordset($str_model_list);
$str_model_name="";
if(!$rs_model_list->eof())
{
	$str_model_name=$rs_model_list->fields("modelname");
}*/

////-------------------------------------------------------------------------------
////-------------------------------------------------------------------------------
///update query to update details in t_testimonial table.
////-------------------------------------------------------------------------------
$str_query_update="UPDATE tr_blog SET ";	
//$str_query_update.="modelpkid=".$int_masterpkid.",";
//$str_query_update.="modelname='".ReplaceQuote($str_modelname)."',";
$str_query_update.="title='".ReplaceQuote($str_name)."',";
//$str_query_update.="designation='".ReplaceQuote($str_desg)."',";
//$str_query_update.="emailid='". ReplaceQuote($str_email) ."',";
$str_query_update.="location='".ReplaceQuote($str_loct)."',";
$str_query_update.="description='".ReplaceQuote($str_desc)."', ";
$str_query_update.="imagefilename='".ReplaceQuote($str_temp_filename)."',";
$str_query_update.="largeimagefilename='".ReplaceQuote($str_temp_filename_large)."', ";
//$str_query_update.="urltitle='".ReplaceQuote($str_urltitle)."',";
//$str_query_update.="url='".ReplaceQuote($str_url)."',";
//$str_query_update.="openinnewwindow='".ReplaceQuote($str_window)."',";
$str_query_update.="displayasnew='".ReplaceQuote($str_display)."' ";
//$str_query_update.="ipaddress='".$_SERVER['REMOTE_ADDR']."' ";
$str_query_update.="WHERE pkid=". $int_pkid;
//print $str_query_update;exit;
ExecuteQuery($str_query_update);

////-------------------------------------------------------------------------------
Redirect("item_comment_list.php?type=S&msg=U&masterpkid=".$int_masterpkid."&memname=".$str_membername."&pkid=".$int_pkid.$str_filter);
exit();
////-------------------------------------------------------------------------------
?>
