<?php
/*
File Name  :- item_order_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
include "../../includes/lib_file_system.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_POST);exit;
$int_masterpkid = 0;
if(isset($_POST["hdn_masterpkid"]))
{
    $int_masterpkid = trim($_POST["hdn_masterpkid"]);
}	
if($int_masterpkid == "" || $int_masterpkid < 0 || !is_numeric($int_masterpkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
//print $int_masterpkid; exit;

$int_service_option = 0;
if(isset($_POST['cbo_type']))
{
    $int_service_option = trim($_POST['cbo_type']);
}


$int_price = 0;
if(isset($_POST['txt_price']))
{
    $int_price = trim($_POST['txt_price']);
}
//print $int_price; exit;

$str_visible = "";
if(isset($_POST['cbo_visible']))
{
    $str_visible = trim($_POST['cbo_visible']);
}

#----------------------------------------------------------------------------------------------------
$str_redirect = "";
$str_redirect.= "&visible=".RemoveQuote(urlencode($str_visible))."&pkid=".urlencode($int_masterpkid);
$str_redirect.= "&#ptop";
#----------------------------------------------------------------------------------------------------
# check all validation
if($int_service_option == 0 || $int_price == 0 || $int_price == "" || $str_visible == "")
{
    CloseConnection();
    Redirect("item_tailoringservice_list.php?msg=F&type=E".$str_redirect);
    exit();
}
#---------------------------------------------------------------------------------------------------- 
$str_query_select = "";
$str_query_select = "SELECT pkid FROM ".$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE." WHERE masterpkid=".$int_service_option." AND itempkid=".$int_masterpkid;
$rs_list_check_duplicate = GetRecordset($str_query_select);
if (!$rs_list_check_duplicate->EOF())
{
    CloseConnection();
    Redirect("item_tailoringservice_list.php?msg=DU&type=E".$str_redirect);
    exit();
}
#----------------------------------------------------------------------------------------------------
#select query to find maximum display order
$int_max = "";
$int_max = GetMaxValue($STR_DB_TR_TABLE_NAME_TAILORING_SERVICE,"displayorder");

# Insert Query
$str_query_insert = "";
$str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE."(masterpkid,itempkid,price,displayorder,visible) VALUES (";
$str_query_insert = $str_query_insert."".$int_service_option.",".$int_masterpkid.", ".$int_price.", ".$int_max.", '".ReplaceQuote($str_visible)."')";
//print $str_query_insert;exit;
ExecuteQuery($str_query_insert);	

#----------------------------------------------------------------------------------------------------
#write to xml file
//WriteXml();
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to listing page	
CloseConnection();
Redirect("item_tailoringservice_list.php?type=S&msg=S".$str_redirect."&#ptop");
exit();
#----------------------------------------------------------------------------------------------------
?>
