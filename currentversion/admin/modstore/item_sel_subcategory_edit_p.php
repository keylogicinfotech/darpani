<?php
/*
	Module Name:- modProductCat
	File Name  :- prod_visible_p.php
	Create Date:- 18-March-2006
	Intially Create By :- 0023
	Update History:
*/

#------------------------------------------------------------------------------------------------
	include "../../includes/validatesession.php";
	include "../../includes/configuration.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
	//include "item_app_specific.php";
#------------------------------------------------------------------------------------------------
#
#get Query String Data
if ($_POST) 
{
    $int_cat_pkid = 0;
    if(isset($_POST["cat_pkid"])==true)
    {
            $int_cat_pkid=trim($_POST["cat_pkid"]);
    }
    
    $int_sub_catpkid = 0;
    if(isset($_POST["subcat_pkid"])==true)
    {
            $int_sub_catpkid=trim($_POST["subcat_pkid"]);
    }

    //print $int_sub_catpkid; exit;
#----------------------------------------------------------------------------------------------------
#select query to get title from tr_product table

    $str_query_select="SELECT subcatpkid, subcattitle FROM t_store_subcat WHERE catpkid=". $int_cat_pkid." ORDER BY subcattitle ASC";
    $rs_visible=GetRecordSet($str_query_select);

    if($rs_visible->eof()==true)
    {
        CloseConnection();
        Redirect("item_list.php?msg=F&type=E&pkid=".$int_cpkid."&#ptop");
        exit();
    }

    $str_subcatval = array();
    echo "<label>Select Sub Category</label><span class='text-help-form'> *</span>";
    echo "<select class='form-control input-sm' name='cbo_subcat' id='cbo_subcat'>";
    echo "<option value='0'>-- SELECT SUB CATEGORY --</option>";
//    while (!$rs_cat->eof() == "true")
    while(!$rs_visible->eof() == true)
    {
        
        //$str_subcatval [] = array("id" => $rs_visible->fields("subcatpkid"), "name" => $rs_visible->fields("subcattitle"));
        echo "<option value='" . $rs_visible->fields("subcatpkid") . "' ".CheckSelected($int_sub_catpkid, $rs_visible->fields("subcatpkid"))."  >" . $rs_visible->fields("subcattitle") . "</option>";
        $rs_visible->MoveNext();
        
    }
    
    echo "</select><br/>";
    //print_r(array_slice($str_subcatval, 1)); exit;
    //echo json_encode($str_subcatval);
}

?>