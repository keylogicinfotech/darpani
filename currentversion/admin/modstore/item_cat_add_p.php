<?php
/*
Module Name:- modstore
File Name  :- item_cat_add_p.php
Create Date:- 04-FEB-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
//include "./item_cat_app_specific.php";	
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$str_title = "";
$str_type = "";
$str_visible = "YES";
if (isset($_POST["cbo_visible"]))
{
    $str_visible = trim($_POST["cbo_visible"]);
}	
if (isset($_POST["txt_title"]))
{
    $str_title = trim($_POST["txt_title"]);
}
if (isset($_POST["cbo_type"]))
{
    $str_type = trim($_POST["cbo_type"]);
}
//print $str_type;exit;
#----------------------------------------------------------------------------------------------------
#Redirect URL

$str_redirect = "";
$str_redirect .= "&visible=". urlencode(RemoveQuote($str_visible));
$str_redirect .= "&cattitle=". urlencode(RemoveQuote($str_title));
$str_redirect .= "&cattype=". urlencode(RemoveQuote($str_type));
#----------------------------------------------------------------------------------------------------
#To check required parameters are passed properly or not

if($str_title == "" || $str_type == "" || $str_visible == "")
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E". $str_redirect."&#ptop");
    exit();
}
if($str_type < 0 || is_numeric($str_type) == false)
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E". $str_redirect."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
//print $str_type;exit;
if($str_type==0)
{
    ## For Category
    
    #Duplication Check
    $str_query_select = "";
    $str_query_select = "SELECT title from ".$STR_DB_TABLE_NAME_CAT." WHERE title= '" . ReplaceQuote($str_title) . "'";
//        print "dup" .$str_query_select;exit; 
    $rs_list_check_duplicate=GetRecordSet($str_query_select);

    if(!$rs_list_check_duplicate->eof())
    {
        CloseConnection();
        Redirect("item_cat_list.php?msg=DU&type=E&". $str_redirect."&#ptop");
        exit();
    }
    #----------------------------------------------------------------------------------------------------
    #Insert Record In a t_workskill_category table

    $int_max=0;
    $int_max=GetMaxValue($STR_DB_TABLE_NAME_CAT,"displayorder");	
    $str_query_insert = "";
    $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_CAT." (title,visible,displayorder)";
    $str_query_insert .= " VALUES('" . ReplaceQuote($str_title) . "',";
    $str_query_insert .= "'" . ReplaceQuote($str_visible) . "',".$int_max.")";
    //print $str_query_insert;exit;
    ExecuteQuery($str_query_insert);

    #----------------------------------------------------------------------------------------------------
    #Close connection and redirect to it's relevant location
    CloseConnection();
    Redirect("item_cat_list.php?type=S&msg=S&title=".urlencode(RemoveQuote($str_title))."&#ptop");
    exit();
    #------------------------------------------------------------------------------------------------------------
}
else
{
    ## For Sub Category
    
    #Duplication Check
    $str_query_select = "";
    $str_query_select = "SELECT subcattitle FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE catpkid=".$str_type." AND subcattitle= '" . ReplaceQuote($str_title) . "'";
        //print $str_query_select;exit; 
    $rs_list_check_duplicate = GetRecordSet($str_query_select);

    if(!$rs_list_check_duplicate->eof())
    {
        CloseConnection();
        Redirect("item_cat_list.php?msg=SDU&type=E&". $str_redirect."&#ptop");
        exit();
    }
    #----------------------------------------------------------------------------------------------------
    #Insert Query

    $int_max=0;
    $int_max=GetSubcatMaxValue($STR_DB_TABLE_NAME_SUBCAT,"catpkid",$str_type,"displayorder");

    $str_query_insert = "";
    $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_SUBCAT."(catpkid,subcattitle,visible,displayorder)";
    $str_query_insert .= " VALUES (".$str_type.",'" . ReplaceQuote($str_title) . "',";
    $str_query_insert .= "'" . ReplaceQuote($str_visible) . "',".$int_max.")";
    //	print $str_query_insert;exit;
    ExecuteQuery($str_query_insert);

    #write xml file
    //WriteXml_Category();	

    #----------------------------------------------------------------------------------------------------
    #Close connection and redirect to it's relevant location

    CloseConnection();
    Redirect("item_cat_list.php?type=S&msg=SS&title=".urlencode(RemoveQuote($str_title))."&#ptop");
    exit();
    #------------------------------------------------------------------------------------------------------------
}
?>
