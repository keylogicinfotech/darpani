<?php
/*
File Name  :- item_cms_p.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_xml.php";
#----------------------------------------------------------------------------------------------------------------------------------------
$int_pkid="";
$str_bdesc="";
$str_ldesc="";
$str_rdesc="";
$str_usage_terms="";
$str_posting_terms="";
$str_visible="YES";
if(isset($_POST['ta_bdesc']))
{
    $str_bdesc=trim($_POST['ta_bdesc']);
}
if(isset($_POST['ta_ldesc']))
{
    $str_ldesc=trim($_POST['ta_ldesc']);
}
$str_desc2 = "";
$str_desc3 = "";
$str_desc4 = "";
$str_desc5 = "";
$str_desc1 = "";
$str_desc6 = "";
$str_desc7 = "";
$str_desc8 = "";
$str_desc9 = "";

$str_visible1 = "";
$str_visible2 = "";
$str_visible3 = "";
$str_visible4 = "";

if(isset($_POST['ta_desc1']))
{
    $str_desc1=trim($_POST['ta_desc1']);
}

if(isset($_POST['ta_desc2']))
{
    $str_desc2=trim($_POST['ta_desc2']);
}


if(isset($_POST['ta_desc3']))
{
    $str_desc3=trim($_POST['ta_desc3']);
}

if(isset($_POST['ta_desc4']))
{
    $str_desc4=trim($_POST['ta_desc4']);
}

if(isset($_POST['ta_desc5']))
{
    $str_desc5=trim($_POST['ta_desc5']);
}

if(isset($_POST['ta_desc6']))
{
    $str_desc6=trim($_POST['ta_desc6']);
}

if(isset($_POST['ta_desc7']))
{
    $str_desc7=trim($_POST['ta_desc7']);
}

if(isset($_POST['ta_desc8']))
{
    $str_desc8=trim($_POST['ta_desc8']);
}

if(isset($_POST['ta_desc9']))
{
    $str_desc9=trim($_POST['ta_desc9']);
}

if(isset($_POST['ta_rdesc']))
{
    $str_rdesc=trim($_POST['ta_rdesc']);
}
if(isset($_POST['hdn_pkid']))
{
    $int_pkid=trim($_POST['hdn_pkid']);
}
//print $int_pkid;exit;
if(isset($_POST["ta_usage_terms"]))
{
$str_usage_terms=trim($_POST["ta_usage_terms"]);
}
if(isset($_POST["ta_posting_terms"]))
{
    $str_posting_terms=trim($_POST["ta_posting_terms"]);
}
if(isset($_POST['cbo_visible1']))
{
    $str_visible1=trim($_POST['cbo_visible1']);
}
if(isset($_POST['cbo_visible2']))
{
    $str_visible2=trim($_POST['cbo_visible2']);
}
if(isset($_POST['cbo_visible3']))
{
    $str_visible3=trim($_POST['cbo_visible3']);
}
if(isset($_POST['cbo_visible4']))
{
    $str_visible4=trim($_POST['cbo_visible4']);
}
/*if(isset($_POST['cbo_visible']))
{
    $str_visible=trim($_POST['cbo_visible']);
}*/
$str_photoid_desc="";
$str_autosign_desc="";
if(isset($_POST["ta_photoid_desc"]))
{
    $str_photoid_desc=trim($_POST["ta_photoid_desc"]);
}
if(isset($_POST["ta_autosign_desc"]))
{
    $str_autosign_desc=trim($_POST["ta_autosign_desc"]);
}
        
#---------------------------------------------------------------------------------------------------------------------------------------	
#select query to get details
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_CMS. " ";
//print $str_query_select;exit;
$rs_list=GetRecordset($str_query_select);

if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_cms.php?msg=F&type=E");
    exit();
}
#----------------------------------------------------------------------------------------------------------------------------------------
#update query to set values in table
#----------------------------------------------------------------------------------------------------------------------------------------
#update query to set values
$str_query_update="";
$str_query_update="UPDATE " .$STR_DB_TABLE_NAME_CMS. " SET description1='".ReplaceQuote($str_desc1)."',";
$str_query_update.="description2='".ReplaceQuote($str_desc2)."',";
$str_query_update.="description3='".ReplaceQuote($str_desc3)."',";
$str_query_update.="description4='".ReplaceQuote($str_desc4)."',";
$str_query_update.="description5='".ReplaceQuote($str_desc5)."',";
$str_query_update.="description6='".ReplaceQuote($str_desc6)."',";
$str_query_update.="visible1='".ReplaceQuote($str_visible1)."',";
$str_query_update.="visible2='".ReplaceQuote($str_visible2)."',";
$str_query_update.="visible3='".ReplaceQuote($str_visible3)."',";
/*$str_query_update.="description7='".ReplaceQuote($str_desc7)."',";
$str_query_update.="description8='".ReplaceQuote($str_desc8)."',";
$str_query_update.="description9='".ReplaceQuote($str_desc9)."',";
$str_query_update.="buttondesc='".ReplaceQuote($str_bdesc)."',";
$str_query_update.="regdesc='".ReplaceQuote($str_rdesc)."',";
$str_query_update.="usageterms='".ReplaceQuote($str_usage_terms)."',";
$str_query_update.="postingterms='".ReplaceQuote($str_posting_terms)."', ";
$str_query_update.="photoidverificationdesc='".ReplaceQuote($str_photoid_desc)."', ";
$str_query_update.="autosignaturedesc='".ReplaceQuote($str_autosign_desc)."', ";  */
$str_query_update=$str_query_update."visible4='".ReplaceQuote($str_visible4)."' WHERE pkid=".$int_pkid;
//print($str_query_update); exit();
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------------------------------------------	
#Writing data of table into XML file
#creating recordset.
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME_CMS. "";
$rs_list = GetRecordSet($str_query_select);
#storing recordset data into array.
if(!$rs_list->eof())
{
    $arr=array();
    $arr1=array();
    $i=0;
    while(!$rs_list->eof())
    {
        $arr[$i]=$rs_list->fields("itemkey")."_buttondesc";
        $arr1[$i]=$rs_list->fields("buttondesc");
        $i=$i+1;

        $arr[$i]=$rs_list->fields("itemkey")."_login";
        $arr1[$i]=$rs_list->fields("description");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_description1";
        $arr1[$i]=$rs_list->fields("description1");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_description2";
        $arr1[$i]=$rs_list->fields("description2");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_description3";
        $arr1[$i]=$rs_list->fields("description3");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_description4";
        $arr1[$i]=$rs_list->fields("description4");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_description5";
        $arr1[$i]=$rs_list->fields("description5");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_description6";
        $arr1[$i]=$rs_list->fields("description6");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_description7";
        $arr1[$i]=$rs_list->fields("description7");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_description8";
        $arr1[$i]=$rs_list->fields("description8");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_description9";
        $arr1[$i]=$rs_list->fields("description9");
        $i=$i+1;


    /*    $arr[$i]=$rs_list->fields("itemkey")."_registration";
        $arr1[$i]=$rs_list->fields("regdesc");
        $i=$i+1;

        $arr[$i]=$rs_list->fields("itemkey")."_usageterms";
        $arr1[$i]=$rs_list->fields("usageterms");
        $i=$i+1;

        $arr[$i]=$rs_list->fields("itemkey")."_postingterms";
        $arr1[$i]=$rs_list->fields("postingterms");
        $i=$i+1;

        $arr[$i]=$rs_list->fields("itemkey")."_photoid";
        $arr1[$i]=$rs_list->fields("photoidverificationdesc");
        $i=$i+1;

        $arr[$i]=$rs_list->fields("itemkey")."_autosignature";
        $arr1[$i]=$rs_list->fields("autosignaturedesc");
        $i=$i+1;*/
        
        $arr[$i]=$rs_list->fields("itemkey")."_visible1";
        $arr1[$i]=$rs_list->fields("visible1");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_visible2";
        $arr1[$i]=$rs_list->fields("visible2");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_visible3";
        $arr1[$i]=$rs_list->fields("visible3");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_visible4";
        $arr1[$i]=$rs_list->fields("visible4");
        $i=$i+1;
        
        
        $rs_list->MoveNext();
    }		
    #writting data to the file
    writeXmlFile($XML_FILE_PATH_CMS,$XML_ROOT_TAG_CMS,$arr,$arr1);
    }
#----------------------------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_cms.php?msg=E&type=S&#ptop");
exit();
?>