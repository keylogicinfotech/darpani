/*
	Module Name:- modstore
	File Name  :- item_cat_list.js
	Create Date:- 04-FEB-2019
	Intially Create By :- 015
	Update History:
*/
function checkallnew()
{
    var counter,i;
    with(document.frm_list)
    {
        counter=hdn_counter.value;
        if (chk_allnew.checked==true)
        {
            for (i=1;i<counter;i++)
            {
                eval("chk_new" + i).checked=true;
            }
        }
        else
        {
            for (i=1;i<counter;i++)
            {
                eval("chk_new" + i).checked=false;
            }
        }
    }
}

function new_click()
{
    with(document.frm_list)
    {
        action="item_set_new_p.php";
        method="post";
        submit();
    }
}

function checkallhot()
{
    var counter,i;
    with(document.frm_list)
    {
        counter=hdn_counter.value;
        if (chk_allhot.checked==true)
        {
            for (i=1;i<counter;i++)
            {
                eval("chk_hot" + i).checked=true;
            }
        }
        else
        {
            for (i=1;i<counter;i++)
            {
                eval("chk_hot" + i).checked=false;
            }
        }
    }
}

function hot_click()
{
    with(document.frm_list)
    {
        action="item_set_hot_p.php";
        method="post";
        submit();
    }
}

function checkallfeatured()
{
    var counter,i;
    with(document.frm_list)
    {
        counter=hdn_counter.value;
        if (chk_allfeatured.checked==true)
        {
            for (i=1;i<counter;i++)
            {
                eval("chk_featured" + i).checked=true;
            }
        }
        else
        {
            for (i=1;i<counter;i++)
            {
                eval("chk_featured" + i).checked=false;
            }
        }
    }
}

function featured_click()
{
    with(document.frm_list)
    {
        action="item_set_featured_p.php";
        method="post";
        submit();
    }
}
function additional_featured_click()
{
    with(document.frm_list)
    {
        action="item_set_additional_info_p.php";
        method="post";
        submit();
    }
}
function noofview_click()
{
    with(document.frm_list)
    {
        action="item_noofview_p.php";
        method="post";
        submit();
    }
}
function frm_list_check_displayorder()
{
    with(document.frm_list)
    {
        cnt=hdn_counter.value;
        for(i=1;i<cnt;i++)
        {
            if(trim(eval("txt_displayorder" + i).value) =="")
            {
                alert("Please enter display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }

            if(eval("txt_displayorder" + i).value <0)
            {
                alert("Please enter positive integer for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }

            if(isNaN(eval("txt_displayorder" + i).value))
            {
                alert("Please enter numeric value for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }
        }
    }
    return true;
}

function checkalldelete()
{
    var counter,i;
    with(document.frm_list)
    {
        counter=hdn_counter.value;
        if (chk_alldelete.checked==true)
        {
            for (i=1;i<counter;i++)
            {
                eval("chk_alldelete" + i).checked=true;
            }
        }
        else
        {
            for (i=1;i<counter;i++)
            {
                eval("chk_alldelete" + i).checked=false;
            }
        }
    }
}

function alldelete_click()
{
    with(document.frm_list)
    {
        action="item_set_alldelete_p.php";
        method="post";
        submit();
    }
}

function confirm_delete()
{
    if(confirm("Are you sure you want to delete this details?"))
    {
        if(confirm("Confirm Deletion:Click 'Ok' to delete this details or 'Cancel' to deletion."))
        {
            return true;
        }
    }
    return false;
}

function confirm_duplicate()
{
    if(confirm("Are you sure you want to create duplicate of this item?"))
    {
        if(confirm("Confirm Deletion:Click 'Ok' to create duplicate of this item or 'Cancel' to cancel process."))
        {
            return true;
        }
    }
    return false;
}

/*
function frm_add_validate()
{
    with(document.frm_add)
    {
        if(cbo_cat.value == "0")
        {
                alert("Please Select Category.");
                //cbo_cat.select();
                cbo_cat.focus();			
                return false;
        }
        
        if(trim(txt_title.value) == "")
        {
                alert("Please enter title.");
                txt_title.select();
                txt_title.focus();			
                return false;
        }

        if(trim(txt_listprice.value) == "")
        {
                alert("Please enter list price.");
                txt_listprice.select();
                txt_listprice.focus();			
                return false;
        }

        if(cbo_visible.value == "")
        {
                alert("Please set value for visible.");			
                cbo_visible.focus();			
                return false;
        }
    }
    return true;
}
*/

function view_description(divno)
{		
	if(document.getElementById("div_desc"+ divno).style.display=="block")
	{
		document.getElementById("div_desc"+ divno).style.display="none";
		document.getElementById("a_desc"+ divno).title="Click to view description";
		document.getElementById("a_desc"+ divno).innerHTML="Click To View Description";
	}
	else
	{
		document.getElementById("div_desc"+ divno).style.display="block";
		document.getElementById("a_desc"+ divno).title="Click to hide description";
		document.getElementById("a_desc"+ divno).innerHTML="Click To Hide Description";
	}	
	return true;
}
