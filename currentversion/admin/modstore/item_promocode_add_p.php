<?php
/*
	Module Name:- modBiography
	File Name  :- bio_add_p.php
	Create Date:- 11-FEB-2006
	Intially Create By :- 0023
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "../../includes/validatesession.php";
	include "../../includes/configuration.php";
	include "item_config.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
	include "../../includes/lib_file_upload.php";
	include "../../includes/lib_image.php";	
	//include "bio_app_specific.php";	

#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$str_title = "";
$int_percent = 0;
$int_amount = 0;

if (isset($_POST["txt_name"]))
{
    $str_title = trim($_POST["txt_name"]);
}	
if (isset($_POST["txt_percent"]))
{
    $int_percent = trim($_POST["txt_percent"]);
    if($int_percent == "")
    {
        $int_percent = 0;
    }
}	
if (isset($_POST["txt_amount"]))
{
    $int_amount = trim($_POST["txt_amount"]);
    if($int_amount == "")
    {
        $int_amount = 0;
    }
}	
$int_min_purchase = 0.00;
if(isset($_POST["txt_min_purchase"]) && trim($_POST["txt_min_purchase"])!="")
{
    $int_min_purchase=trim($_POST["txt_min_purchase"]);
}
$str_desc = "";
if(isset($_POST["ta_desc"]))
{ $str_desc = trim($_POST["ta_desc"]); }
	
$str_visible = "YES";
if(isset($_POST['cbo_visible']))
{ $str_visible = trim($_POST['cbo_visible']); }
#----------------------------------------------------------------------------------------------------
#To check whether required parameters are passed properly or not
if($str_title == "")
{
    CloseConnection();
    Redirect("item_promocode_list.php?msg=F&type=E&#ptop");
    exit();
}

if($int_percent == "" && $int_amount == "")
{
    CloseConnection();
    Redirect("item_promocode_list.php?msg=E&type=E&#ptop");
    exit();
}

if($int_percent != "" )
{
    if(!is_numeric($int_percent) || $int_percent < 0){
        CloseConnection();
        Redirect("item_promocode_list.php?msg=N&type=E&#ptop");
        exit();
    }
}

if($int_amount != "")
{
    if(!is_numeric($int_amount)  || $int_amount<0){
        CloseConnection();
	Redirect("item_promocode_list.php?msg=N&type=E&#ptop");
	exit();
    }
}
#-------------------------------------------------------------------------------------------------------------------
#Duplication Check
$str_query_select = "";
$str_query_select = "SELECT * FROM  ".$STR_DB_TABLE_NAME_PROMOCODE." WHERE title='".ReplaceQuote($str_title)."'"; 
$rs_list_check_duplicate = GetRecordset($str_query_select);

if($rs_list_check_duplicate->EOF() == false)
{
    CloseConnection();
    Redirect("item_promocode_list.php?type=E&msg=DU".$str_redirect);
    exit();
}
#----------------------------------------------------------------------------------------------------
#Insert Query
$int_max = 0;
$int_max = GetMaxValue($STR_DB_TABLE_NAME_PROMOCODE,"displayorder");

$str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_PROMOCODE." (title,description,visible,displayorder,discountpercentage,discountamount,minimumpurchaserequired)";
$str_query_insert .= " VALUES ('" . ReplaceQuote($str_title) . "','" . ReplaceQuote($str_desc) . "',";
$str_query_insert .= " 'YES',".$int_max.",".$int_percent.",".$int_amount.", ".ReplaceQuote($int_min_purchase).")";
//print $str_query_insert;exit;
ExecuteQuery($str_query_insert);
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file

//WriteXml();
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_promocode_list.php?type=S&msg=S");
exit();
#------------------------------------------------------------------------------------------------------------
?>