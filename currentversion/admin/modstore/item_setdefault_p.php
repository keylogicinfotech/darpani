<?php
/*
	Module Name:- Modstore
	File Name  :- mdl_ps_setdefault_p.php
	Create Date:- 7-sep-2006
	Intially Create By :- 0014
	Update History:-
*/
#--------------------------------------------------------------------------------------------------------------------------------------------------------
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "../includes/lib_data_access.php";
	include "./item_config.php";
	include "../includes/lib_common.php";
	include "./item_app_specific.php";
	include "../includes/lib_xml.php";
#---------------------------------------------------------------------------------------------------------
	# filter variables
	$int_mid="";
	#---------------------------------------------------------------------------------------------------------------------------------------------------------------
	#	get filter data
	if(isset($_GET["mid"]) && trim($_GET["mid"])!="" )
	{
		$int_mid=trim($_GET["mid"]);
	}

#	Get passed value and check whether all values are passed properly or not.			
	$int_pkid="";
	$str_mode="";

	if(isset($_GET["pkid"]))
	{
		$int_pkid=trim($_GET["pkid"]);
	}
	if(!is_numeric($int_pkid) || $int_pkid=="" || $int_pkid<0 )
	{
		CloseConnection();
		Redirect("item_list.php?msg=F&type=E&mid=".$int_mid."&#potp");
		exit();
	}
#---------------------------------------------------------------------------------------------------------
# Getting skill title to display message on cont_sub_list.php
	$str_select_query="SELECT subcatpkid,title,setasdefault FROM t_store where pkid=".$int_pkid;
	$rs_check=GetRecordSet($str_select_query);
	if($rs_check->EOF()==true)
	{
		CloseConnection();
		Redirect("item_list.php?msg=F&type=E&mid=".$int_mid."&#potp");
		exit();
	}	
	$int_subcatpkid="";
	$int_subcatpkid=$rs_check->Fields("subcatpkid");
	$str_title=$rs_check->Fields("title");
	$str_mode=$rs_check->Fields("setasdefault");
#---------------------------------------------------------------------------------------------------------
#	Set the value for variable to be stored in database.
	if(strtoupper($str_mode)=="YES")
	{
		$str_set_text="NO";
	}
	else
	{
		$str_set_text="YES";
	}
#---------------------------------------------------------------------------------------------------------
#	Update query to update table.	
	$str_select_update="UPDATE t_store SET setasdefault='" .ReplaceQuote($str_set_text). "' where pkid=".$int_pkid;
	ExecuteQuery($str_select_update);

#	Update query to update table.	
	$str_select_update="UPDATE t_store SET setasdefault='NO' where pkid!=".$int_pkid." and subcatpkid=".$int_subcatpkid;
	ExecuteQuery($str_select_update);
	
#-----------------------------------------------------------------------------------------------------------
	#write to xml file
	WriteXML($int_subcatpkid);
#---------------------------------------------------------------------------------------------------------
#	Close connection and redirect to skill_list.php
	CloseConnection();
	Redirect("item_list.php?msg=SD&type=S&title=".urlencode(RemoveQuote($str_title))."&mode=".urlencode(RemoveQuote($str_set_text))."&mid=".$int_mid."&#ptop");
	exit();
?>