<?php
/*
Module Name:- Manage photoset
File Name  :- mdl_ps_add_p.php
Create Date:- 8-Sep-2006
Intially Create By :- 0014
*/
#---------------------------------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_image.php";
include "../../includes/lib_xml.php";
include "../../includes/lib_email.php";
//print_r($_POST); exit;
#----------------------------------------------------------------------------------------------------
#Initializing varialbles
$str_redirect_page_flag = "";
if(isset($_GET["flg"]) && trim($_POST["flg"])!="" )
{ $str_redirect_page_flag=trim($_POST["flg"]); }

$int_cat_pkid = 0;
if(isset($_POST["catid"]) && trim($_POST["catid"])!="" )
{ $int_cat_pkid=trim($_POST["catid"]); }

$str_key = "";
if(isset($_POST["key"]) && trim($_POST["key"])!="" )
{ $str_key=trim($_POST["key"]); }

# POST data for paging
$int_page = 0;
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"])!="" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"])>0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page=1; }
//print $int_page; exit;

$int_userpkid = 0;
if(isset($_GET['uid']) && $_GET['uid']!= "")
{
    $int_userpkid=$_GET['uid'];
}



$int_pkid = 0;
$int_masterpkid = 0;

//print_r($_POST); exit;
if(isset($_POST['hdn_masterpkid']))
{
    $int_masterpkid = trim($_POST['hdn_masterpkid']);
}
if($int_masterpkid == "" || $int_masterpkid<0 || !is_numeric($int_masterpkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}

$int_countrypkid = 0;
if(isset($_POST['cbo_country']))
{
    $int_countrypkid = trim($_POST['cbo_country']);
}


$int_listprice = 0;
if(isset($_POST['txt_listprice']))
{
    $int_listprice = trim($_POST['txt_listprice']);
}

$int_ourprice = 0;
if(isset($_POST['txt_ourprice']))
{
    $int_ourprice = trim($_POST['txt_ourprice']);
}

$int_memberprice = 0;
if(isset($_POST['txt_memberprice']))
{
    $int_memberprice = trim($_POST['txt_memberprice']);
}


$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_key."&PagePosition=".$int_page."&userpkid=".$int_userpkid."&pkid=".$int_masterpkid;
//print $str_filter; exit;
#Get values of all passed GET / POST variables
if(isset($_POST['hdn_pkid']))
{
    $int_pkid=trim($_POST['hdn_pkid']);
}
if($int_pkid=="" || $int_pkid<0 || !is_numeric($int_pkid) || $int_listprice <= 0 || $int_listprice == "")
{
    CloseConnection();
    Redirect("item_price_list.php?msg=F&type=E".$str_filter);
    exit();
}

#---------------------------------------------------------------------------------------------------- 
$str_query_select = "";
$str_query_select = "SELECT pkid FROM ".$STR_DB_TR_TABLE_NAME_PRICE." WHERE countrypkid=".$int_countrypkid." AND itempkid=".$int_masterpkid." AND pkid!= ".$int_pkid;
$rs_list_check_duplicate = GetRecordset($str_query_select);
if (!$rs_list_check_duplicate->EOF())
{
    CloseConnection();
    Redirect("item_price_list.php?msg=DU&type=E".$str_redirect);
    exit();
}
#------------------------------------------------------------------------------------------------------------
# Update Query
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TR_TABLE_NAME_PRICE." SET countrypkid=".$int_countrypkid.",";
//$str_query_update = $str_query_update."photographerurl='".ReplaceQuote($str_purl)."',";
//$str_query_update = $str_query_update."photographertitle='".ReplaceQuote($str_ptitle)."',";
$str_query_update.= "listprice=".$int_listprice.",";
$str_query_update.= "ourprice=".$int_ourprice.",";
$str_query_update.= "memberprice=".$int_memberprice." ";
//$str_query_update = $str_query_update."colorpkid=".$int_colorpkid.",";
//$str_query_update = $str_query_update."color='".ReplaceQuote($str_color)."',";
//$str_query_update = $str_query_update."previewtoall='".ReplaceQuote($str_preview)."',";
//$str_query_update = $str_query_update."accesstoall='".ReplaceQuote($str_access)."' ";
$str_query_update.= " WHERE pkid=".$int_pkid." AND itempkid=".$int_masterpkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#write to xml file
//WriteXml();
#----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_price_list.php?msg=U&type=S".$str_filter);
exit();
#----------------------------------------------------------------------------------------------------
?>
