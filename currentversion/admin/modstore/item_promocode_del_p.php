<?php
/*
	Module Name:- modstore
	File Name  :- item_promocode_del_p.php
	Create Date:- 21-JAN-2019
	Intially Create By :- 015
	Update History: 
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid=0;
if(isset($_GET['pkid']))
{ $int_pkid=$_GET['pkid']; }

if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
{ 
    CloseConnection();
    Redirect("item_promocode_list.php?type=E&msg=F");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Select Query
$str_query_select = "";
$str_query_select = "SELECT title FROM ".$STR_DB_TABLE_NAME_PROMOCODE." WHERE pkid=".$int_pkid;
$rs_list = GetRecordset($str_query_select);

$str_title = "";
$str_title = $rs_list->Fields("title");

if($rs_list->EOF() == true)
{
    CloseConnection();
    Redirect("item_promocode_list.php?type=E&msg=F");
    exit();
}
#-------------------------------------------------------------------------------------------------------------------	
#Delete Query
$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TABLE_NAME_PROMOCODE." WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_delete);
#-------------------------------------------------------------------------------------------------------------------	
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_promocode_list.php?type=S&msg=D&tit=".urlencode(RemoveQuote($str_title))."&#ptop");
exit(); ?>	