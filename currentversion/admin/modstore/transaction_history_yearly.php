<?php
/*
	Module Name:- modstore
	File Name  :- purchase_list.php
	Create Date:- 11-Sep-2006
	Intially Create By :- 0014
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "purchase_config.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
	include "../includes/lib_datetimeyear.php";
        include "./item_app_specific.php";

#------------------------------------------------------------------------------------------------
$int_year=date("Y");
if(isset($_GET['cbo_year']))
{ $int_year=trim($_GET['cbo_year']); }

$int_model_pkid=0;
if(isset($_GET['cbo_model']))
{ $int_model_pkid=trim($_GET['cbo_model']); }

//print $int_model_pkid."<br/>".$int_month."<br/>".$int_year."<br/>";

$filtercriteria="";
if($int_model_pkid > 0 && $int_year == 0 ) 
{
    $filtercriteria=" WHERE modelpkid=".$int_model_pkid. " AND inyear=".date("Y");
}
else if($int_model_pkid > 0 && $int_year != 0 ) 
{
    $filtercriteria=" WHERE modelpkid=".$int_model_pkid. " AND inyear=".$int_year;
}
else if($int_model_pkid == 0 && $int_year != 0 ) 
{
    $filtercriteria=" WHERE inyear=".$int_year;
}
else if($int_model_pkid == 0 && $int_year == 0 ) 
{
    $filtercriteria=" WHERE inyear=".date("Y");
}


// Get model name to display on report
if($int_model_pkid>0)
{
	$str_query_select = "SELECT modelname FROM t_model WHERE modelpkid=".$int_model_pkid;
	$rs_modelname=GetRecordset($str_query_select);
	$str_modelname = $rs_modelname->fields("modelname")." : ";
}
else
{ $str_modelname = ""; }

#------------------------------------------------------------------------------------------------
// Below query will group items by purchase date time. So it will do SUM SUM(extendedprice) as gross_price of all items in single order 
// and will display it. 
// GROUP_CONTACT(CONCAT will display details of each item in single order using this GROUP_CONCAT(CONCAT(producttitle, ' | ', price, '<br/>')) as item_details
// Here | is separator of different values of single item.
// Here <br/> is separator among different items in single order
// Below query is working but we are not using s15-AUG-2014
$str_query_select = "SELECT purchasedatetime, ANY_VALUE(purchasepkid) AS purchasepkid, ANY_VALUE(productpkid) AS productpkid,ANY_VALUE(memberpkid) AS memberpkid,ANY_VALUE(emailid) AS emailid,ANY_VALUE(subscriptionid) AS subscriptionid,ANY_VALUE(ccbillmemberloginid) AS ccbillmemberloginid,ANY_VALUE(ccbillmemberpassword) AS ccbillmemberpassword,ANY_VALUE(price) AS price,ANY_VALUE(firstname) AS firstname, ANY_VALUE(lastname) AS lastname, ANY_VALUE(address) AS address, ANY_VALUE(city) AS city, ANY_VALUE(state) AS state, ANY_VALUE(country) AS country, ANY_VALUE(zipcode) AS zipcode, ANY_VALUE(ipaddress) AS ipaddress,ANY_VALUE(phoneno) AS phoneno,ANY_VALUE(expiredate) AS expiredate,ANY_VALUE(allowdownload) AS allowdownload,ANY_VALUE(addedbyccbill) AS addedbyccbill,ANY_VALUE(pricevalue) AS pricevalue,ANY_VALUE(producttitle) AS producttitle,ANY_VALUE(subcattitle) AS subcattitle,ANY_VALUE(membername) AS membername,ANY_VALUE(noofdownload) AS noofdownload,ANY_VALUE(modelpkid) AS modelpkid, ANY_VALUE(modelname) AS modelname, ANY_VALUE(catpkid) AS catpkid, ANY_VALUE(subcatpkid) AS subcatpkid, ANY_VALUE(pricepkid) AS pricepkid,ANY_VALUE(shippinginfo) AS shippinginfo,ANY_VALUE(dshippingvalue) AS dshippingvalue,ANY_VALUE(ishippingvalue) AS ishippingvalue, ANY_VALUE(shippingaddress) AS shippingaddress,ANY_VALUE(color) AS color, ANY_VALUE(size) AS size,ANY_VALUE(type) AS type, ANY_VALUE(finaldiscountedamount) AS finaldiscountedamount, SUM(extendedprice)as gross_price, SUM(dshippingvalue)as gross_shippping";
//$str_query_select .=", GROUP_CONCAT(CONCAT(producttitle , '|' , price , '|' , quantity , '|' , extendedprice , '|' , color , '|' , size , '|' , typemodel , '|' , emailid , '|' , subscriptionid , '|' , shippingaddress , '|' , firstname , '|' , lastname , '|' , address , '|' , city , '|' , state , '|' , country , '|' , zipcode , '|' , specialnote , '|' , phoneno ) SEPARATOR '::') as item_details "; 
$str_query_select .= " FROM t_store_purchase ";
$str_query_select .=" ".$filtercriteria; 
$str_query_select .= " GROUP BY purchasedatetime ORDER BY purchasedatetime DESC";
//print $str_query_select;exit;
$rs_purchase_history_list=GetRecordSet($str_query_select);



//producttitle, productcattitle, pricepkid, pricevalue, price, extendedprice, shipping, shippingvalue, discountpercentage, discountamount, finaldiscountedamount, quantity, color, size, typemodel, freememberpkid, freemembername, emailid, subscriptionid, ccbillmemberloginid, ccbillmemberpassword, firstname, lastname, address, city, state, country, zipcode, shippingaddress, specialnote, ipaddress, purchaseday, photosetinmonth, photosetinyear, initialprice, initialperiod, recurringprice, recurringperiod, numrebills, billstatus, payoptpkid, pay_opt_title, phoneno, expiredate, allowdownload, addedbyccbill, noofdownload, orderstatus, orderstatusnote, res_code, res_code_msg, authcode, transid, transhash ORDER BY purchasedatetime DESC";

//$str_query_select .= " GROUP BY purchasedatetime, purchasepkid, modelpkid, photosetpkid, subcatpkid, pricepkid, price, dshippingvalue,ishippingvalue,shippinginfo, color, size, type, freememberpkid, emailid, subscriptionid, ccbillmemberloginid, ccbillmemberpassword, firstname, lastname, address, city, state, country, zipcode, ipaddress,  photosetinmonth, phoneno,photosetinyear ORDER BY purchasedatetime DESC";
//


#------------------------------------------------------------------------------------------------
$str_type = "";
$str_message = "";
$str_visible_value="";

if(isset($_GET['mode']))
{
    $str_mode=trim($_GET['mode']);
}
#	Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#	Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"):
                $str_message = "Some information(s) missing. Please try again.";
                break;
        case("A"):
                $str_message = "Product Purchase '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' details added successfully.";
                break;
        case("U"):
                $str_message = "Purchase Product expire dete updated successfully.";
                break;
        case("D"):
                $str_message = "Product Purchase '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' details deleted successfully.";
                break;
        case("V"):
                $str_message = "Product Purchase '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' login mode changed to '". $str_mode . "' successfully.";
                break;
    }
}
#------------------------------------------------------------------------------------------------
?><!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_YEARLY_SALES_REPORT);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> 
<a name="ptop" id="ptop"></a>
<div class="container">
	<?php include("../includes/adminheader.php"); ?>
	<div class="row">
            <div class="col-md-2 col-sm-6 col-xs-12 button_space">
                <div class="btn-group" role="group" aria-label="...">
                    <a href="./transaction_history_monthly.php" class="btn btn-default" title="<?php print($STR_MONTHLY_SALES_REPORT);?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_MONTHLY_SALES_REPORT);?></a>

                </div>
            </div>
            <div class="col-md-10 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_YEARLY_SALES_REPORT);?>&nbsp;[<?php print($int_year);?>] </h3></div>
	</div><hr> 
	<?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); } ?>
	
	
	<div class="panel panel-default">
		 <div class="panel-heading">
              	<h4 class="panel-title">
					<div class="row">
						<div class="col-md-12  padding-10">
							<div class="col-md-6 col-sm-6 col-xs-8"><strong><?php print($STR_FILTER_CRITERIA);?></strong></div>
							<div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>												
						</div>
					</div>
				</h4>
             </div>
	 	<div class="panel-body">
				<div class="col-md-12" align="center">
					<form name="frm_rpt_filter" method="get" action="./transaction_history_yearly.php" >
                                            <label>Select&nbsp;&nbsp;</label>
                                            <label>
                                                <?php 
								/*$str_query_seller="SELECT modelpkid, modelname, shortenurlkey ";
								$str_query_seller.=" FROM t_model ";
								$str_query_seller.=" WHERE approved='YES' AND visible='YES' ";
								$str_query_seller.=" ORDER BY modelname ";*/				
								
								$str_query_seller="SELECT DISTINCT(p.modelpkid), m.shortenurlkey";
								$str_query_seller.=" FROM t_store_purchase p ";
								$str_query_seller.=" LEFT JOIN t_model m ON m.modelpkid=p.modelpkid ";
								//$str_query_seller.=" WHERE photosetinmonth=".$int_month." AND photosetinyear=".$int_year;
								$str_query_seller.=" ORDER BY shortenurlkey ASC";
//                                                                print $str_query_seller;exit;
								$rs_seller=GetRecordSet($str_query_seller);
								?>
								
								<select name="cbo_model" id="cbo_model" class="form-control input-sm">
									<option value="0" >--- ALL RECIPIENTS ---</option>
									<?php 
									$int_prev_catpkid=0;
									while(!$rs_seller->eof())
									{ ?>
									<option value="<?php print($rs_seller->fields("modelpkid")); ?>" <?php print(CheckSelected($rs_seller->fields("modelpkid"),$int_model_pkid)); ?> ><?php print($rs_seller->fields("shortenurlkey")); ?></option>
									<?php
										$rs_seller->movenext();
									}
									?>
								</select>
						
                                            </label>
<!--                                            <label>&nbsp;&nbsp;for&nbsp;&nbsp;</label>
						<label><? //php print(DisplayMonth($int_month)); ?></label>-->
						<label>&nbsp;&nbsp;&&nbsp;&nbsp;</label>
						<label><?php print(DisplayYear($int_year)); ?></label>
						&nbsp;&nbsp;
						<label><button type="submit" name="btn_submit" class="btn btn-warning btn-sm">View Report</button></label>
					</form>
			</div>	 
		</div>
	</div>
	<div class="table-responsive">
	<form name="frm_list" action="./action_page.php" method="post" onSubmit="javascript: validate_submit_fomrm();"   >
            <table class="table table-striped table-bordered ">
            <thead>
            	<tr>
                    <th width="4%">Sr. #</th>
                    <th width="10%">Transaction Date</th>
                    <th width="">Details</th>
                    <th width="10%">Price<br><span class="text-help">(in US$)</span></th>	
                    <th width="10%">Admin <br/>Amount<br><span class="text-help">(in US$)</span></th>	
                    <th width="10%">(-) Processing Fees<br><span class="text-help">(in US$)</span></th>	
                    <?php /* ?><th width="10%">(-) .55 Deduction<br><span class="text-help">(in US$)</span></th><?php */ ?>
                    <th width="12%">Total <br/>Amount<br><span class="text-help">(in US$)</span></th>	
                </tr>
            </thead>
                <tbody>
                <?php if($rs_purchase_history_list->EOF()==true)  {  ?>
                            <tr><td colspan="7" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                        <?php }
                        else {

                        $int_total_amount_model_without_shipping_and_promo=0; 
                        $int_total_amount_model=0;
                        $int_total_amount_admin=0;
                        $int_total_card_processing_fees=0;
                        $int_total_amount=0;
                        $int_total_shipping_charge=0;
                        $int_total_promo_code=0;
                        $int_cnt=1;
                        while(!$rs_purchase_history_list->EOF()==true) { ?>
                        <tr>
                                <td align="center" class="text-align"><?php print($int_cnt);?></td>
                                <td align="center" >
                                    <i class="text-primary"><?php print(date("d-M-Y h:i:s A",strtotime($rs_purchase_history_list->fields("purchasedatetime"))));?></i>
                                </td>
                                <td align="left" style="vertical-align:text-top">
				<?php $sel_mdl_details = ""; 
                               	$sel_mdl_details = "SELECT shortenurlkey FROM t_model WHERE modelpkid = ".$rs_purchase_history_list->fields("modelpkid"); 
                                $rs_details = GetRecordSet($sel_mdl_details);
                                ?>  
                                Profile ID: <b><a href="<?php print $STR_SITE_NAME_WITH_PROTOCOL.$rs_details->Fields("shortenurlkey"); ?>"><?php print $rs_details->Fields("shortenurlkey");?></a></b><br/><br/>
                                <?php //print "<b>Transaction details of</b> <span class='Heading10ptBold'> ".$rs_purchase_history_list->fields("shortenurlkey")."</span><br/>"; ?>
									
                                <?php 
                               $str_query_select = "SELECT * FROM t_store_purchase ";
                                $str_query_select .= " WHERE purchasedatetime=  '".$rs_purchase_history_list->fields("purchasedatetime")."'";
                                $str_query_select .= " AND modelpkid=".$rs_purchase_history_list->fields("modelpkid");
                                $str_query_select .= " ORDER BY purchasedatetime DESC ";
                                $rs_item_list=GetRecordset($str_query_select);

                                $str_emailid = "";
                                $str_phoneno = "";
                                $str_firstname = "";
                                $str_lastname = "";
                                $str_address = "";
                                $str_zipcode = "";
                                $str_city = "";
                                $str_state = "";
                                $str_country = "";
                                $str_ipaddress = "";
                                $str_subscriptionid = "";
                                $str_shippinginfo = "";
                                $str_shippingaddress = "";

                                $int_prod_cnt=1;
                                $int_actual_amount = 0;
                                while(!$rs_item_list->EOF()) 
                                { 
                                    print "<br/><span class=''>".$int_prod_cnt.".</span>  "; 
                                    print "<b><span class='text-help'>Item Info:</span></b><br/>"; 
                                    if($rs_item_list->fields("producttitle")!="") { print("<span class='text-help'>Name:</span> <span class=''>".$rs_item_list->fields("producttitle"))."</span>";}
                                    if($rs_item_list->fields("price")!="") { print(" | <span class='text-help'>Price:</span> <span class=''>".GetPriceValue($rs_item_list->fields("price")))."</span>";}
                                    if($rs_item_list->fields("quantity")!="") { print(" | <span class='text-help'>Qty:</span> <span class=''>".$rs_item_list->fields("quantity"))."</span> |";}
                                    if($rs_item_list->fields("extendedprice")!="") { print("  <span class='text-help'>Total:</span> <span class=''>".GetPriceValue($rs_item_list->fields("extendedprice")))."</span>";}
//                                    if($rs_item_list->fields("cattitle")!="") { print("<br/> <span class='text-help'>Category:</span> <span class=''>".$rs_item_list->fields("cattitle"))."</span>";}
                                    if($rs_item_list->fields("subcattitle")!="") { print(" | <span class='text-help'>Category:</span> <span class=''>".$rs_item_list->fields("subcattitle"))."</span> |";}
                                    if($rs_item_list->fields("color")!="") { print(" <br/> <span class='text-help'>Color:</span> <span class=''>".$rs_item_list->fields("color"))."";}
                                    if($rs_item_list->fields("size")!="") { print(" | <span class='text-help'>Size:</span> <span class=''>".$rs_item_list->fields("size"))."";}
                                    if($rs_item_list->fields("type")!="") { print(" | <span class='text-help'>Type / Model:</span> <span class=''>".$rs_item_list->fields("type"))."";}
                                    print "<br/><span class=''></span>"; 

                                    $str_emailid = $rs_item_list->fields("emailid");
                                    $str_phoneno = $rs_item_list->fields("phoneno");
                                    $str_firstname = $rs_item_list->fields("firstname");
                                    $str_lastname = $rs_item_list->fields("lastname");
                                    $str_address = $rs_item_list->fields("address");
                                    $str_zipcode = $rs_item_list->fields("zipcode");
                                    $str_city = $rs_item_list->fields("city");
                                    $str_state = $rs_item_list->fields("state");
                                    $str_country = $rs_item_list->fields("country");
                                    $str_ipaddress = $rs_item_list->fields("ipaddress");
                                    $str_ishippingval = $rs_item_list->fields("ishippingvalue");
                                    $str_dshippingval = $rs_item_list->fields("dshippingvalue");
                                    $str_shippinginfo = $rs_item_list->fields("shippinginfo");
                                    $int_actual_amount = $int_actual_amount + $rs_item_list->fields("extendedprice");
                                    $int_promocodepkid = $rs_item_list->fields("promocodepkid");
                                    $str_subscriptionid = $rs_item_list->fields("subscriptionid");

                                    $rs_item_list->MoveNext(); 
                                    $int_prod_cnt=$int_prod_cnt+1;
                                } 

                                        print "<br/><b><span class='text-help'>Payee Info:</span></b><br/>";

                                        $str_shippinginfo = $rs_purchase_history_list->fields("shippinginfo");
                                        $str_shippingaddress = $rs_purchase_history_list->fields("shippingaddress");
                                    
					$sel_mem_details = ""; 
                                        $sel_mem_details = "SELECT shortenurlkey FROM t_freemember WHERE pkid = ".$rs_purchase_history_list->fields("memberpkid"); 
                                        $rs_mem_details = GetRecordSet($sel_mem_details);
                                      	?>  
                                    	Profile ID: <b><a href="<?php print $STR_SITE_NAME_WITH_PROTOCOL.$rs_mem_details->Fields("shortenurlkey"); ?>"><?php print $rs_mem_details->Fields("shortenurlkey");?></a></b><br/>
  					<?php 
                                        if($str_subscriptionid!="") { print(" <span class='text-help'>Subscription ID:</span> <span class=''>".$str_subscriptionid)."";}
                                        if($str_emailid!="") { print(" | <span class='text-help'>Email:</span> <span class=''>".$str_emailid)."";}
                                        if($str_phoneno!="") { print(" | <span class='text-help'>Phone:</span> <span class='HighLightText10ptNormal'>".$str_phoneno)."";}
                                        if($str_firstname!="") { print(" | <span class='text-help'>Name:</span> <span class='HighLightText10ptNormal'>".$str_firstname)."";}
                                        if($str_lastname!="") { print(" ".$str_lastname)."";}
                                        if($str_address!="") { print(" | <span class='text-help'>Address:</span> <span class='HighLightText10ptNormal'>".$str_address)."";}
                                        if($str_zipcode!="") { print(" | <span class='text-help'>Zipcode:</span> <span class='HighLightText10ptNormal'>".$str_zipcode)."";}
                                        if($str_city!="") { print(" | <span class='text-help'>City:</span> <span class='HighLightText10ptNormal'>".$str_city)."";}
                                        if($str_state!="") { print(" | <span class='text-help'>State:</span> <span class='HighLightText10ptNormal'>".$str_state)."";}
                                        if($str_country!="") { print(" | <span class='text-help'>Country:</span> <span class='HighLightText10ptNormal'>".$str_country)."";}
                                        if($str_ipaddress!="") { print(" | <span class='text-help'>IP:</span> <span class='HighLightText10ptNormal'>".$str_ipaddress)."";}
                                        if($str_dshippingval!="") { print(" | <span class='text-help'>Shipping Value:</span> <span class='HighLightText10ptNormal'>".$str_dshippingval)."";}
//                                        if($str_ishippingval!="") { print(" | <span class='text-help'>Ishipping Value:</span> <span class='HighLightText10ptNormal'>".$str_ishippingval)."";}
                                        if($str_shippinginfo!="") { print(" <br/> <span class='text-help'>Shipping Info:</span> <span class='HighLightText10ptNormal'>".$str_shippinginfo)."";}
                                        if($str_shippingaddress!="") { print(" <br/> <span class='text-help'>Shipping Info:</span> <span class='HighLightText10ptNormal'>".$str_shippingaddress)."";}
                                        print "<br/><span class='FieldCaptionText10ptNormal'></span>";									
                                ?>
                                </td>
                                
                            

				<?php 
				if($rs_purchase_history_list->EOF()==false)
				{
					$str_query_select = "SELECT splitamount, processingfees FROM t_model WHERE modelpkid=".$rs_purchase_history_list->fields("modelpkid");
					$rs_model=GetRecordset($str_query_select);
										
					if($rs_model->EOF()==false)
					{
                                            $int_splitamount = $rs_model->fields("splitamount");
                                            $int_processingfees = $rs_model->fields("processingfees");
						
					}
					else
					{
                                            $int_splitamount = 1;
                                            $int_processingfees = 1;
					}
				}
				else
				{
                                    $int_splitamount = 1;
                                    $int_processingfees = 1;
				} 
				?> 
				<?php 
				$int_gross_price_after_split_admin = ((100-$int_splitamount) * $rs_purchase_history_list->fields("gross_price"))/100;
				$int_gross_price_after_card_fees = ($int_processingfees * $rs_purchase_history_list->fields("gross_price"))/100;
				$int_gross_price_after_split_model = $rs_purchase_history_list->fields("gross_price") - $int_gross_price_after_split_admin - $int_gross_price_after_card_fees;
				
				?>
                                
				<td align="right" valign="middle"  class="">
					<?php
					//print(number_format($int_gross_price_after_all_deduction,2,".",",")."&nbsp;");
					print("<span class='Heading10ptBold'>".number_format($int_gross_price_after_split_model,2,".",",")."&nbsp;</span><br/><span class='HelpText'>(".($int_splitamount - $int_processingfees)."%)</span>&nbsp;");
					?>
				</td>
				<td align="right" valign="middle">
					<?php // print(number_format($int_gross_price_after_split_admin,2,".",",")."&nbsp;"); ?>
					<?php print("<span class='Heading10ptBold'>".number_format($int_gross_price_after_split_admin,2,".",",")."&nbsp;</span><br/><span class='HelpText'>(".(100 - $int_splitamount)."%)</span>&nbsp;"); ?>
				</td>
				<td align="right" valign="middle">
					<?php // print(number_format($int_gross_price_after_card_fees,2,".",",")."&nbsp;"); ?>
					<?php print("<span class='Heading10ptBold'>".number_format($int_gross_price_after_card_fees,2,".",",")."&nbsp;</span><br/><span class='HelpText'>(".($int_processingfees + 0)."%)</span>&nbsp;"); ?>
				</td>


                                <td align="right">
                                        <?php print("<span class=''><b>".number_format($int_actual_amount,2,".",",")."&nbsp;</b></span><br/>"); ?>
                                        <?php //print $int_actual_amount;?>
                                </td>
                        </tr>
                         <tr>
                             <td colspan="3" align="right"><b>+ Order Shipping Charge</b></td>
                             <td align="right"><b><?php print($rs_purchase_history_list->fields("gross_shippping")); ?></b></td>
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="right"><b>- Order Promo Code Discount</b>
                             <?php
                            if($int_promocodepkid > 0)
                            {
                                $sel_qry_promocode = "";
                                $sel_qry_promocode = "SELECT * FROM t_store_promocode WHERE pkid = ".$int_promocodepkid;
                                //print $sel_qry_promocode;
                                $rs_promocode_details = GetRecordSet($sel_qry_promocode);
                                //print $rs_promocode_details->Fields("title");
                            
                            ?>
                            [<?php
                                if($rs_promocode_details->Fields("title") != "") {
                                    
                                    print "Used Promo Code: ".$rs_promocode_details->Fields("title");
                                }
                                if($rs_promocode_details->Fields("discountamount") != "" && $rs_promocode_details->Fields("discountamount") > 0) {
                                    
                                    print " - US$ ".$rs_promocode_details->Fields("discountamount");
                                }
                                if($rs_promocode_details->Fields("discountpercentage") != "" && $rs_promocode_details->Fields("discountpercentage") > 0) {
                                    
                                    print " - ".$rs_promocode_details->Fields("discountpercentage")."%";
                                }
                                ?>]
                            <?php } ?>
                            </td>
                            <td align="right"><b><?php print($rs_purchase_history_list->fields("finaldiscountedamount")); ?></b></td>
                            <td colspan="3"></td>                            
                        </tr>
                        <tr>
                            <td colspan="3" align="right" class="text-info"><b>= Gross Payable For This Order</b></td>
                            <?php 
                            $int_gross_payable_price_model = $int_gross_price_after_split_model + $rs_purchase_history_list->fields("gross_shippping") - $rs_purchase_history_list->fields("finaldiscountedamount"); ?>
                            <td align="right" class="text-info"><b><?php print(number_format($int_gross_payable_price_model,2,".",",")."&nbsp;"); ?></b></td>
                            <td colspan="3"></td>
                        </tr>
                    <?php
			//$int_gross_payable_price_model = $int_gross_price_after_split_model + $rs_purchase_history_list->fields("gross_shippping");
                        $int_total_amount_model = $int_total_amount_model + $int_gross_payable_price_model;
                        $int_total_amount_model_without_shipping_and_promo = $int_total_amount_model_without_shipping_and_promo + $int_gross_price_after_split_model;
                        $int_total_amount_admin = $int_total_amount_admin + $int_gross_price_after_split_admin;
                        $int_total_card_processing_fees = $int_total_card_processing_fees + $int_gross_price_after_card_fees;
                        $int_total_amount = $int_total_amount + $rs_purchase_history_list->fields("gross_price");
                        //$int_total_amount = $int_total_amount + $int_actual_amount;
                        $int_total_shipping_charge = $int_total_shipping_charge + $rs_purchase_history_list->fields("gross_shippping");
                        $int_total_promo_code = $int_total_promo_code + $rs_purchase_history_list->fields("finaldiscountedamount");

                        $rs_purchase_history_list->movenext();
                        $int_cnt=$int_cnt+1;
                        }
                        ?>
                        <tr>
                            <td colspan="7"><br/></td>
                        </tr>
                        <tr>
                            
                           <?php // $int_total_price = 20; ?>
                            <td align="right" class="text-danger" colspan="3"><input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>"><strong>Total amount for the year [<?php print($int_year);?>] </strong></td>
                            <?php /*?><td align="right"><?php print("<span class=''><b>$".number_format($int_total_actual_amount,2,".",",")."&nbsp;</b></span><br/>"); ?></td>
                            <td align="right"><?php print("<span class=''><b>$".number_format($int_total_processing_fees,2,".",",")."&nbsp;</b></span><br/>"); ?></td>
                            <td align="right"><?php print("<span class=''><b>$".number_format($int_total_deduction_amt,2,".",",")."&nbsp;</b></span><br/>"); ?></td><?php */?>
                            <td align="right" valign="middle"><?php // print(number_format($int_total_amount_model,2,".",",")); ?><?php print("<span class='text-danger'><b>".number_format($int_total_amount_model_without_shipping_and_promo,2,".",",")."&nbsp;</b></span>"); ?></td>
                            <td align="right" valign="middle"><?php // print(number_format($int_gross_price_after_split_admin,2,".",",")."&nbsp;"); ?>		<?php print("<span class='text-danger'><b>".number_format($int_total_amount_admin,2,".",",")."&nbsp;</b></span>"); ?></td>
                            <td align="right" valign="middle"><?php // print(number_format($int_gross_price_after_card_fees,2,".",",")."&nbsp;"); ?><?php print("<span class='text-danger'><b>".number_format($int_total_card_processing_fees,2,".",",")."&nbsp;</b></span>"); ?></td>
                            <td align="right"><?php print("<span class='text-danger'><b>".number_format($int_total_amount,2,".",",")."&nbsp;</b></span><br/>"); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="right"><b>+ Total Shipping Charge For This Year</b></td>
                            <td align="right"><b><?php print("<span class=''>".number_format($int_total_shipping_charge,2,".",",")."&nbsp;</span>"); ?></b></td>
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="right"><b>- Total Promo Code Discount For This Year</b></td>
                            <td align="right"><b><?php print("<span class=''>".number_format($int_total_promo_code,2,".",",")."&nbsp;</span>"); ?></b></td>
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="right" class="text-info"><b>Gross Total For <?php print($int_year); ?> (+ Shipping Charge - Promo Code Discount)</b></td>
                            <td align="right"><?php print("<h4 class='text-info '><b>".number_format($int_total_amount_model,2,".",",")."&nbsp;</b></h4>"); ?></td>
                            <td colspan="3"></td>
                        </tr>
                <?php }  ?>					
                </tbody>
            </table>
	</form>	
    </div>
    <?php include "../includes/help_for_list.php"; ?>
</div>
<link href="../includes/bootstrap.min.css" rel="stylesheet">
<link href="../includes/bootstrap_theme.css" rel="stylesheet">
<link href="../includes/admin.css" rel="stylesheet">

<script language="JavaScript" src="../includes/functions.js" type="text/javascript"></script>
<script src="../includes/jquery.min.js"></script>
<script src="../includes/bootstrap.min.js"></script>
</body></html>
