<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_cat_config.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/http_to_https.php";	
include "../../includes/lib_xml.php";

//print_r($_SESSION);exit;
#----------------------------------------------------------------------
$int_cat_pkid = "";
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ 
    $int_cat_pkid=trim($_GET["catid"]);     
}

$str_name_key="";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ 
    $str_name_key=trim($_GET["key"]); 
}

# get data for paging
$int_page = "";
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ 
    $int_page = $_GET["PagePosition"];     
}
else
{
    $int_page = 1; 
}

$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page;
#------------------------------------------------------------------------------------------------
$int_masterpkid = "";
if(isset($_GET["masterpkid"]) && trim($_GET["masterpkid"])!="" )
{ 
    $int_masterpkid = trim($_GET["masterpkid"]);     
}

if($int_masterpkid<=0 || !is_numeric($int_masterpkid) || $int_masterpkid=="")
{
    CloseConnection();
    Redirect("item_order_status_list.php?type=E&msg=F");
    exit();
}

$str_side = "";
if(isset($_GET["side"]) && trim($_GET["side"])!="" )
{ 
    $str_side = trim($_GET["side"]);     
}



$str_query_select = "";
$str_query_select = "SELECT * FROM t_store WHERE pkid = ".$int_masterpkid;
$rs_list = GetRecordSet($str_query_select);

if($rs_list->Count() == 0)
{
    CloseConnection();
    Redirect("item_order_status_list.php?type=E&msg=F");
    exit();
}

$str_img_path = "";
if($str_side == "F")
{
    $str_img_path = $UPLOAD_IMG_PATH.$int_masterpkid."/".$rs_list->Fields("imagefilename2");
}
else if($str_side == "B")
{
    $str_img_path = $UPLOAD_IMG_PATH.$int_masterpkid."/".$rs_list->Fields("imagefilename3");
}

# width
$int_image_width = 0;
$int_image_width = imagesx(imagecreatefromstring(file_get_contents($str_img_path)));

# height
$int_image_height = 0;
$int_image_height = imagesy(imagecreatefromstring(file_get_contents($str_img_path)));

$int_image_printable_width = 0;
$int_image_printable_height = 0;

if($int_image_width == $INT_IMAGE_WIDTH_L && $int_image_height == $INT_IMAGE_HEIGHT_L)
{
    $int_image_printable_width = $INT_IMAGE_WIDTH_PRINTABLE_L;
    $int_image_printable_height = $INT_IMAGE_HEIGHT_PRINTABLE_L;
}
else if($int_image_width == $INT_IMAGE_WIDTH_P && $int_image_height == $INT_IMAGE_HEIGHT_P)
{
    $int_image_printable_width = $INT_IMAGE_WIDTH_PRINTABLE_P;
    $int_image_printable_height = $INT_IMAGE_HEIGHT_PRINTABLE_P;
}
//print $int_image_printable_width."<br/>";
//print $int_image_printable_height;exit;

//print $str_img_path; exit;
#------------------------------------------------------------------------------------------------
# Initialization of variables used for message display.  
$str_type = "";
$str_message = "";
# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
	case("E"): $str_type = "E"; break;
	case("W"): $str_type = "W"; break;
    }
}
#	Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
  
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("SU"): $str_message = $STR_MSG_ACTION_STATUS_CHANGED; break;
        case("IE"): $str_message = $STR_MSG_ACTION_INVALID_EMAIL_FORMAT; break;
        case("MS"): $str_message = $STR_MSG_ACTION_MAIL_SENT; break;
    }
} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE_ORDER_STATUS) ;?></title>
    <?php //print(Display_Page_Metatag($STR_TITLE_PAGE)); ?>
    <?php /* ?><link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../css/user.css" rel="stylesheet">       <?php */ ?>

    <style type="text/css" media="print">
    
    @media print 
    {
        header, .footer, footer { display: none; };
        // .noprint {display:none; !important}; // Not working this so used #SCREEN_VIEW_CONTAINER as given in below code
        -webkit-print-color-adjust: exact; //:For Chrome
        color-adjust: exact; //:For Firefox
        print-color-adjust: exact;
    }
    @page 
    {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html
    {
        background-color: #FFFFFF; 
        margin: 0px;  /* this affects the margin on the html before sending to printer */
    }

    body
    {
        border: solid 0px black ; /* border around entire page */
        margin: 10mm 15mm 10mm 15mm; /* margin you want for the content */
    }
    </style>
    
    
    
    
    
    
    

</head>
<body onload="window.print();">
    <img src="<?php print $str_img_path; ?>" width="<?php print $int_image_printable_width; ?>" height="<?php print $int_image_printable_height; ?>" />;
</body>
</html>
