<?php
/*
    Module Name:- modstore
    File Name  :- item_cat_tailoringoption_list_p.php
    Create Date:- 13-MAY-2019 
    Intially Create By :- 015
    Update History:
*/
#------------------------------------------------------------------------------------------------
    include "../../includes/validatesession.php";
    include "../../includes/configuration.php";
    include "../../includes/lib_data_access.php";
    include "../../includes/lib_common.php";
    include "item_config.php";
#------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables

//print_r($_POST);exit;
    
$str_filter = "&#ptop";

$int_cnt = "";
if (isset($_POST["hdn_counter"]))
{
    $int_cnt = trim($_POST["hdn_counter"]);
}	
if($int_cnt == "" || $int_cnt < 0 || !is_numeric($int_cnt))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}

$int_catpkid = "";
if (isset($_POST["hdn_catpkid"]))
{
    $int_catpkid = trim($_POST["hdn_catpkid"]);
}	

if($int_catpkid == "" || $int_catpkid <= 0 || !is_numeric($int_catpkid))
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E".$str_filter);
    exit();
}
#----------------------------------------------------------------------------------------------------
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_CATWISE_TAILORING_OPTION." WHERE catpkid=".$int_catpkid;
$rs_list = GetRecordSet($str_query_select);
if(!$rs_list->EOF())
{
    $str_query_delete = "";
    $str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_CATWISE_TAILORING_OPTION." WHERE catpkid=".$int_catpkid;
    ExecuteQuery($str_query_delete);
}


//print_r($_POST); exit;
for ($i = 1;$i < $int_cnt;$i++)
{ 
    if(isset($_POST["cbx_option" . $i]))
    {
        if (($_POST["cbx_option" . $i])=="on")
        {
            $int_optionpkid = $_POST["hdn_pkid" . $i];
            $str_query_insert = "";
            $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_CATWISE_TAILORING_OPTION." (optionpkid, catpkid) VALUES (".$int_optionpkid.",".$int_catpkid.")";
            print $str_query_insert."<br/>";
            ExecuteQuery($str_query_insert);
            
        }
    }
    
}
#------------------------------------------------------------------------------------------------------------
#Close connection and redirect to prod_list.php page	
CloseConnection();
Redirect("item_cat_tailoringoption_list.php?type=S&msg=U&catid=".$int_catpkid.$str_filter);
exit();
#------------------------------------------------------------------------------------------------------------
?>