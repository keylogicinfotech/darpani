<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
//include "./item_cat_config.php";
//include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/http_to_https.php";	
include "../../includes/lib_xml.php";
include "../../includes/lib_email.php";
#------------------------------------------------------------------------------------------------
#get filter parameters
#------------------------------------------------------------------------------------------------
/*$page=1;
if (isset($_GET["PagePosition"]))
{
	$page = trim($_GET["PagePosition"]);
}
// if model pkid passed then set filter criteria for modelpkid
$int_model_pkid="-1";
if (isset($_GET["modelpkid"]))
{
	$int_model_pkid = trim($_GET["modelpkid"]);
}
$str_filter_status="&modelpkid=".$int_model_pkid."&PagePosition=".$page."&";*/

#------------------------------------------------------------------------------------------------
# To distinguish different redirect pages
if(isset($_GET["flg"]) && trim($_GET["flg"])!="" )
{ $str_redirect_page_flag=trim($_GET["flg"]); }

if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ $int_cat_pkid=trim($_GET["catid"]); }
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_mname=trim($_GET["key"]); }

# get data for paging
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page=1; }

$str_filter="";
//$str_filter="&catid=".$int_cat_pkid."&key=".$str_mname."&PagePosition=".$int_page;

$int_masterpkid="";
if(isset($_GET["masterpkid"]))
{ $int_masterpkid=trim($_GET["masterpkid"]); }

$str_modelname="";
if(isset($_GET["mname"])) { $str_modelname=trim($_GET["mname"]); }

$int_pkid=0;
if(isset($_GET['pkid']))
{ $int_pkid=$_GET['pkid']; }
if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
{ 
    CloseConnection(); 
    //if($str_redirect_page_flag=='NACL')
    //{ 
    Redirect("item_comment_list.php?type=E&msg=F&masterpkid=".$int_masterpkid); //}
    //Redirect("blog_notapproved_list.php?type=E&msg=F"); }
    /*else
    { Redirect("tm_list.php?type=E&msg=F&masterpkid=".$int_masterpkid."&mname=".$str_modelname); }*/
    exit(); 
}

#------------------------------------------------------------------------------------------------
#select query to get the details of pkid from tr_modelsearch_comment table
$str_select_query="";
$str_select_query="SELECT * FROM tr_blog WHERE pkid=".$int_pkid;
//print $str_select_query;exit;
$rs_approved=GetRecordset($str_select_query);
$str_title=$rs_approved->Fields("modelname");
$int_modelpkid = 0;
$int_modelpkid = $rs_approved->Fields("userpkid");

if($rs_approved->eof())
{
    CloseConnection();
    //if($str_redirect_page_flag=='NACL')
    //{ 
    Redirect("item_comment_list.php?type=E&msg=F&masterpkid=".$int_masterpkid."&mname=".$str_modelname); //}
    //Redirect("blog_notapproved_list.php?type=E&msg=F"); }
    /*else
    { Redirect("tm_list.php?type=E&msg=F&masterpkid=".$int_masterpkid."&mname=".$str_modelname); }*/
    exit();
}
$str_approved=$rs_approved->Fields("approved");
if(strtoupper($str_approved)=='YES')
{
    $str_approved="NO";
    $str_approved_title="Not Approved";
}
else if(strtoupper($str_approved)=='NO')
{
    $str_approved="YES";
    $str_approved_title="Approved";
}
	
#-----------------------------------------------------------------------------------------------------------------------------
	#update query to change the mode
	$str_select_update="";
	$str_select_update="UPDATE tr_blog SET visible='YES', approved='".ReplaceQuote($str_approved)."', submitdatetime='".date("Y-m-d H:i:s")."' WHERE pkid=".$int_pkid;
        //print $str_select_update; exit;
	ExecuteQuery($str_select_update);	
#-----------------------------------------------------------------------------------------------------------------------------
#NOTIFY USER REGARDING APPROVAL
/*$fp=openXMLfile($UPLOAD_XML_CONTENT_PATH."siteconfiguration.xml");
$str_from=getTagValue($STR_FROM_DEFAULT,$fp);
closeXMLfile($fp); */

$str_query_select = "SELECT shortenurlkey, emailid FROM t_user WHERE pkid=".$int_masterpkid."";
$rs_list = GetRecordSet($str_query_select);
$str_to = $rs_list->Fields("emailid");

$str_subject="Your blog is approved on ".$STR_SITENAME_WITHOUT_PROTOCOL." ";
$mailbody="Your blog is approved. Below are details.<br/><br/>";
$mailbody.="<strong>Your Profile ID:</strong> ".$rs_list->fields("shortenurlkey")."<br/>";
$mailbody.="<strong>Blog Name:</strong> ".$rs_approved->Fields("title")."<br/>";
$mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL."";
//print $mailbody."<br/>".$str_from."<br/>".$str_to."<br/>"; exit;
sendmail($str_to,$str_subject,$mailbody,$str_from,1);          
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file
//Comment_Synchronize($int_modelpkid);
//Comment_WriteXml($int_modelpkid);
#----------------------------------------------------------------------------------------------------------------------------	
CloseConnection();
//if($str_redirect_page_flag=='NACL')
//{
 Redirect("item_comment_list.php?type=S&msg=V&masterpkid=".$int_masterpkid."&mode=".urlencode(RemoveQuote($str_approved_title))."&tit=".urlencode(RemoveQuote($str_title))."&".$str_filter ); //}
/*else
{ Redirect("tm_list.php?type=S&msg=V&masterpkid=".$int_masterpkid."&mname=".$str_modelname."&mode=".urlencode(RemoveQuote($str_approved_title))."&tit=".urlencode(RemoveQuote($str_title))."&".$str_filter ); }*/
exit();
?>
