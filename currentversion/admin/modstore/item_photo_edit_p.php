<?php
/*
Module Name:- Manage photoset
File Name  :- mdl_ps_add_p.php
Create Date:- 8-Sep-2006
Intially Create By :- 0014
*/
#---------------------------------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_image.php";
include "../../includes/lib_xml.php";
include "../../includes/lib_email.php";
//print_r($_POST); exit;
#----------------------------------------------------------------------------------------------------
#Initializing varialbles
$str_redirect_page_flag = "";
if(isset($_GET["flg"]) && trim($_POST["flg"])!="" )
{ $str_redirect_page_flag=trim($_POST["flg"]); }

$int_cat_pkid = 0;
if(isset($_POST["catid"]) && trim($_POST["catid"])!="" )
{ $int_cat_pkid=trim($_POST["catid"]); }

$str_key = "";
if(isset($_POST["key"]) && trim($_POST["key"])!="" )
{ $str_key=trim($_POST["key"]); }

# POST data for paging
$int_page = 0;
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"])!="" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"])>0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page=1; }
//print $int_page; exit;

$int_userpkid = 0;
if(isset($_GET['uid']) && $_GET['uid']!= "")
{
    $int_userpkid=$_GET['uid'];
}



$int_pkid = 0;
$int_masterpkid = 0;
$str_image = "";

//print_r($_POST); exit;
if(isset($_POST['hdn_masterpkid']))
{
    $int_masterpkid = trim($_POST['hdn_masterpkid']);
}
if($int_masterpkid == "" || $int_masterpkid<0 || !is_numeric($int_masterpkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}


$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_key."&PagePosition=".$int_page."&userpkid=".$int_userpkid."&pkid=".$int_masterpkid;
//print $str_filter; exit;
#Get values of all passed GET / POST variables
if(isset($_POST['hdn_pkid']))
{
    $int_pkid=trim($_POST['hdn_pkid']);
}
if($int_pkid=="" || $int_pkid<0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_photo_list.php?msg=F&type=E".$str_filter);
    exit();
}

$str_image_url = "";
if (isset($_POST["txt_imgurl"]) && $_POST["txt_imgurl"]!= "")
{
    $str_image_url = trim($_POST["txt_imgurl"]);
}

//print_r($_FILES); exit;

if(isset($_FILES['fileimage']))
{
    $str_image = trim($_FILES['fileimage']['name']);
}
#------------------------------------------------------------------------------------------------------------
#check all validation
if($str_image != "" )
{
    if(ValidateImageExtension($_FILES['fileimage']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
        CloseConnection();
        Redirect("item_photo_edit.php?msg=I&type=E&pkid=".$int_pkid.$str_filter);
        exit();
    }
}	

if($str_image_url != "")
{
    if(validateURL02(strtolower($str_image_url))==false )
    {
        //CloseConnection();
        //Redirect("item_photo_edit.php?msg=IU&type=E".$str_redirect);
        //exit();
    }
}
#----------------------------------------------------------------------------------------------------
#upload image
$str_main_file_name = "";
$str_large_file_name = "";
$str_thumb_file_name = "";
$str_main_path = "";
$str_large_path = "";
$str_thumb_path = "";

$str_query_select = "";
$str_query_select = "SELECT thumbphotofilename,largephotofilename,mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE pkid=".$int_pkid." AND masterpkid=".$int_masterpkid;
//print $str_query_select; exit;
$rs_list = GetRecordset($str_query_select);

if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_photo_edit.php?msg=F&type=E&pkid=".$int_pkid.$str_filter);
    exit();
}

$str_main_file_name = $rs_list->fields("mainphotofilename");
$str_large_file_name = $rs_list->fields("largephotofilename");
$str_thumb_file_name = $rs_list->fields("thumbphotofilename");

//print $str_image;exit;
if($str_image != "")
{
    #delete old image
    //print $UPLOAD_IMG_PATH.$int_masterpkid."/"; exit;
    if($str_main_file_name!="")
    {
        DeleteFile($UPLOAD_IMG_PATH.$int_masterpkid."/".trim($str_main_file_name));
    }
    if($str_large_file_name!="")
    {
        DeleteFile($UPLOAD_IMG_PATH.$int_masterpkid."/".trim($str_large_file_name));
    }
    if($str_thumb_file_name!="")
    {
        DeleteFile($UPLOAD_IMG_PATH.$int_masterpkid."/".trim($str_thumb_file_name));
    }
    #upload new image

    $str_main_file_name=GetUniqueFileName()."_photo_main.".getextension($str_image);
    $str_large_file_name=GetUniqueFileName()."_photo_l.".getextension($str_image);
    $str_thumb_file_name=GetUniqueFileName()."_photo_t.".getextension($str_image);
    
    $str_main_path = trim($UPLOAD_IMG_PATH.$int_masterpkid."/".$str_main_file_name);
    //print $str_main_path;exit;
    $str_large_path = trim($UPLOAD_IMG_PATH.$int_masterpkid."/".$str_large_file_name);
    $str_thumb_path = trim($UPLOAD_IMG_PATH.$int_masterpkid."/".$str_thumb_file_name);
    UploadFile($_FILES['fileimage']['tmp_name'],$str_main_path);
    
    ## START - Code to put watermark on image
        /*$str_image_to_print_watermark = imagecreatefromstring(file_get_contents($str_main_path));
        $str_image_watermark = imagecreatefromstring(file_get_contents("./../../images/logo_150x29.png"));

        imagecopy($str_image_to_print_watermark, $str_image_watermark, 0, 0, 0, 0, 150 , 29);
        imagejpeg($str_image_to_print_watermark, $str_main_path);
        imagedestroy($str_image_to_print_watermark);*/
    ## END - Code to put watermark on image
    
    CompressImage($str_main_path, $str_main_path, 60);
    //ResizeImage($str_main_path,$INT_PHOTO_MAIN_WIDTH);
    SaveAsThumbImage($str_main_path,$str_large_path,$INT_IMG_WIDTH_LARGE);
    SaveAsThumbImage($str_large_path,$str_thumb_path,$INT_IMG_WIDTH_THUMB);
}

#----------------------------------------------------------------------------------------------------
# Update Query
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_PHOTO." SET thumbphotofilename='".ReplaceQuote($str_thumb_file_name)."',";
//$str_query_update = $str_query_update."photographerurl='".ReplaceQuote($str_purl)."',";
//$str_query_update = $str_query_update."photographertitle='".ReplaceQuote($str_ptitle)."',";
$str_query_update = $str_query_update."largephotofilename='".ReplaceQuote($str_large_file_name)."',";
$str_query_update = $str_query_update."mainphotofilename='".ReplaceQuote($str_main_file_name)."',";
$str_query_update = $str_query_update."imageurl='".$str_image_url."'";
//$str_query_update = $str_query_update."colorpkid=".$int_colorpkid.",";
//$str_query_update = $str_query_update."color='".ReplaceQuote($str_color)."',";
//$str_query_update = $str_query_update."previewtoall='".ReplaceQuote($str_preview)."',";
//$str_query_update = $str_query_update."accesstoall='".ReplaceQuote($str_access)."' ";
$str_query_update = $str_query_update." WHERE pkid=".$int_pkid." AND masterpkid=".$int_masterpkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#write to xml file
//WriteXml();
#----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_photo_list.php?msg=U&type=S".$str_filter);
exit();
#----------------------------------------------------------------------------------------------------
?>
