<?php
/*
Module Name:- modccbillmember
File Name  :- item_add_p.php
Create Date:- 02OCT2018
Intially Create By :- 0022
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../includes/validatesession.php";
include "../includes/configuration.php";
include "item_config.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_email.php";
include "../includes/lib_xml.php";
#------------------------------------------------------------------------------------------
//print_r($_POST); exit;

$int_model_pkid=0;
if (isset($_POST["cbo_modelpkid"])) { $int_model_pkid = trim($_POST["cbo_modelpkid"]); }

$int_fmember_pkid=0;
if (isset($_POST["cbo_memberpkid"])) { $int_fmember_pkid = trim($_POST["cbo_memberpkid"]); }

$int_item_pkid=0;
if (isset($_POST["cbo_item"])) { $int_item_pkid = trim($_POST["cbo_item"]); }

$int_color_pkid=0;
if (isset($_POST["cbo_color"])) { $int_color_pkid = trim($_POST["cbo_color"]); }

$int_type_pkid=0;
if (isset($_POST["cbo_type"])) { $int_type_pkid = trim($_POST["cbo_type"]); }

$int_size_pkid=0;
if (isset($_POST["cbo_size"])) { $int_size_pkid = trim($_POST["cbo_size"]); }
//print $int_item_pkid;exit;

//$int_procedure_pkid=0;
//if (isset($_POST["cbo_procedurepkid"])) { $int_procedure_pkid = trim($_POST["cbo_procedurepkid"]); }
//
//$int_price_pkid=0;
//if (isset($_POST["cbo_pricepkid"])) { $int_price_pkid = trim($_POST["cbo_pricepkid"]); }

$int_subcat_pkid=0;
if (isset($_POST["subcatpkid"])) { $int_subcat_pkid = trim($_POST["subcatpkid"]); }
//print "subcat" .$int_subcat_pkid;exit;
//if($int_model_pkid==0 || $int_fmember_pkid==0 || $int_procedure_pkid==0 || $int_price_pkid==0)
//{
//    print "error";exit;
//    CloseConnection();
//    Redirect("item_add_transaction_manual.php?type=E&msg=F&cbo_modelpkid=".$int_model_pkid."&cbo_memberpkid=".$int_fmember_pkid."&cbo_procedurepkid=".$int_procedure_pkid."&cbo_pricepkid=".$int_price_pkid);
//    exit();
//}

$str_query_select="SELECT modelpkid, shortenurlkey ";
$str_query_select.=" FROM t_model ";
$str_query_select.=" WHERE approved='YES' AND visible='YES' AND modelpkid=".$int_model_pkid;
$rs_list_model=GetRecordSet($str_query_select);

$str_query_select="SELECT * ";
$str_query_select.=" FROM t_freemember ";
$str_query_select.=" WHERE approved='YES' AND visible='YES' AND pkid=".$int_fmember_pkid;
$str_query_select.=" ORDER BY shortenurlkey ASC";				
$rs_list_member=GetRecordSet($str_query_select);
//print $rs_list_member->fields("pkid");exit;

$str_query_select="SELECT *";
$str_query_select.=" FROM t_store ";
$str_query_select.=" WHERE approved='YES' AND visible='YES' AND subcatpkid=".$int_item_pkid;
$str_query_select.=" ORDER BY title ASC";				
$rs_list_item=GetRecordSet($str_query_select);

$str_query_select="SELECT *";
$str_query_select.=" FROM t_store_subcat ";
$str_query_select.=" WHERE subcatpkid=".$int_item_pkid;
//$str_query_select.=" ORDER BY title ASC";				
$rs_list_subcatitem=GetRecordSet($str_query_select);

$str_query_select="SELECT *";
$str_query_select.=" FROM tr_store_photo ";
$str_query_select.=" WHERE masterpkid=".$int_color_pkid;
//$str_query_select.=" ORDER BY title ASC";				
$rs_list_itemcolor=GetRecordSet($str_query_select);

$str_query_select="SELECT *";
$str_query_select.=" FROM t_store_subcat ";
$str_query_select.=" WHERE subcatpkid=".$int_type_pkid;
//$str_query_select.=" ORDER BY title ASC";				
$rs_list_itemtype=GetRecordSet($str_query_select);

$str_query_select="SELECT *";
$str_query_select.=" FROM t_store_subcat ";
$str_query_select.=" WHERE subcatpkid=".$int_size_pkid;
//$str_query_select.=" ORDER BY title ASC";				
$rs_list_itemsize=GetRecordSet($str_query_select);
//print "color" .$rs_list_item->fields("color");exit; 
//print_r($rs_list_member);exit;
 
//$str_query_select="SELECT pkid, title ";
//$str_query_select.=" FROM t_procedure ";
//$str_query_select.=" WHERE pkid=".$int_procedure_pkid;
//$str_query_select.=" ORDER BY title ASC";				
//$rs_list_procedure=GetRecordSet($str_query_select);

//$str_query_select="SELECT pkid, title, price ";
//$str_query_select.=" FROM t_ccbill_dynamic_price ";
//$str_query_select.=" WHERE visible='YES' AND pkid=".$int_price_pkid;
//$str_query_select.=" ORDER BY title ASC";				
//$rs_list_price=GetRecordSet($str_query_select);  


$str_trans_id="ADDEDBYSITEADMIN";
if(isset($_POST["subscription_Id"]) && trim($_POST["subscription_Id"])!="") 
{ 
    $str_trans_id=RemoveQuote(trim($_POST["subscription_Id"])); }
    $str_purchase_date=date("Y-m-d H:i:s");
    $str_purchase_day="";
    $str_purchase_month="";
    $str_purchase_year="";

    /// find the day, month and year from the transaction date
    $str_purchase_day=date("d",strtotime($str_purchase_date));
    $str_purchase_month=date("m",strtotime($str_purchase_date));
    $str_purchase_year=date("Y",strtotime($str_purchase_date));

    $str_download="YES";
    $str_addedby="YES";		

    $INT_PRODUCT_DOWNLOAD_LIMIT_DAY_STORE = 3;
    $expire_date=date("Y-m-d H:i:s",mktime(23, 59, 59, date("m")  , date("d")+$INT_PRODUCT_DOWNLOAD_LIMIT_DAY_STORE, date("Y")));


    
//    print "<br>subcattitle:" .$rs_list_item->fields("title");exit;
    $int_qty=1;
    $ins_qry_member="";
    $ins_qry_member="INSERT INTO t_store_purchase( ";
    $ins_qry_member.="modelpkid, modelname, productpkid,producttitle,catpkid, subcatpkid, ";
    $ins_qry_member.="pricepkid,memberpkid, membername, emailid, subscriptionid,quantity,   ";
    $ins_qry_member.="price, extendedprice, firstname, lastname, subcattitle, ";
    $ins_qry_member.="address, city, state, country, zipcode, phoneno, ipaddress,";
    $ins_qry_member.="purchasedatetime,expiredate,pricevalue,";
    $ins_qry_member.="inmonth,inyear,color,size,type) ";
    $ins_qry_member.="VALUES (";
    $ins_qry_member.=$rs_list_model->fields("modelpkid").",'".$rs_list_model->fields("shortenurlkey")."', ".$rs_list_item->fields("pkid").", '".$rs_list_item->fields("title")."', ".$rs_list_item->fields("catpkid").", ".$rs_list_item->fields("subcatpkid").", ".$rs_list_item->fields("pkid").", ".ReplaceQuote($rs_list_member->fields("pkid")).", '".$rs_list_member->fields("shortenurlkey")."', '".ReplaceQuote($rs_list_member->fields("emailid"))."', '".$str_trans_id."', ".$int_qty.", ".$rs_list_item->fields("listprice").", ".$rs_list_item->fields("listprice").", '".ReplaceQuote($rs_list_member->fields("firstname"))."', '".ReplaceQuote($rs_list_member->fields("lastname"))."', '".ReplaceQuote($rs_list_subcatitem->fields("subcattitle"))."', '".ReplaceQuote($rs_list_member->fields("address"))."', '".ReplaceQuote($rs_list_member->fields("city"))."', '".ReplaceQuote($rs_list_member->fields("state"))."', '".ReplaceQuote($rs_list_member->fields("country"))."', '".ReplaceQuote($rs_list_member->fields("zipcode"))."', '".ReplaceQuote($rs_list_member->fields("phoneno"))."', '".$rs_list_member->fields("ipaddress")."', '".$str_purchase_date."','".$expire_date."', '".$rs_list_item->fields("listprice")."',".$str_purchase_month.",".$str_purchase_year.",'".$rs_list_item->fields("color")."','".$rs_list_item->fields("size")."','".$rs_list_item->fields("type")."')";
    
//    print $ins_qry_member;exit; 
    ExecuteQuery($ins_qry_member);

    $fp=openXMLfile($UPLOAD_XML_CONTENT_PATH."siteconfiguration.xml");
    $str_from = getTagValue($STR_FROM_DEFAULT,$fp);
    $str_to = getTagValue($STR_FROM_DEFAULT,$fp);
    closeXMLfile($fp);

    $str_query_select = "SELECT shortenurlkey, emailid, getnotification FROM t_model WHERE modelpkid=".$rs_list_model->fields("modelpkid")."";
    //print $str_query_select;exit;
    $rs_list = GetRecordSet($str_query_select);
    $str_to_model = $rs_list->fields("emailid");

    if(!$rs_list->eof())
    {
        $str_subject="Donation received on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
        $mailbody="Donation received. Below are details.<br/><br/>";
        $mailbody.="<strong>Recipient Name:</strong> ".$rs_list->fields("shortenurlkey")."<br/>";
        $mailbody.="<strong>Amount:</strong> ".$rs_list_item->fields("listprice")."<br/>";
        $mailbody.="<strong>Item:</strong> ".$rs_list_item->fields("title")."<br/>";
        $mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL."";
//        print "<br/>".$str_from; print "<br/>".$str_to; print "<br/>".$mailbody."<br/><br/>"; exit;
        sendmail($str_to,$str_subject,$mailbody,$str_from,1);  
        sendmail($str_to_model,$str_subject,$mailbody,$str_from,1);  
    }
    #----------------------------------------------------------------------------------------------------
    $str_query_select = "";
    $str_query_select = "SELECT SUM(extendedprice) AS total_sum FROM t_store_purchase WHERE memberpkid=".$rs_list_member->fields("pkid");
    $rs_list_member_donation_total = GetRecordSet($str_query_select);

    //$str_query_select = "";
    //$str_query_select = "SELECT shortenurlkey, emailid, getnotification FROM t_freemember WHERE pkid=".$rs_list_member->fields("pkid")."";
    //$rs_list_member = GetRecordSet($str_query_select);
    //$str_to_member = $rs_list_member->Fields("emailid"); 

    $str_query_select = "";
    $str_query_select = "SELECT * FROM t_trophy WHERE visible='YES'";
    $rs_list_trophy = GetRecordSet($str_query_select);

    /*
    while(!$rs_list_trophy->EOF())
    {
        // Send notification only if user's total donated amount fits between any membership level range
        if($rs_list_member_donation_total->fields("total_sum") >= $rs_list_trophy->Fields("from_amount") && $rs_list_member_donation_total->fields("total_sum") <= $rs_list_trophy->Fields("to_amount"))
        {
            // If notification is ON by user then only send notification
            if($rs_list_member->Fields("getnotification")=='YES')
            {
                    $str_subject="Contributor reached a new membership level on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
                    $mailbody="Contributor reached a new membership level. Below are details.<br/><br/>";
                    $mailbody.="<strong>Contributor Name:</strong> ".$rs_list_member->fields("shortenurlkey")."<br/>";
                    $mailbody.="<strong>Membership Level:</strong> <br/>".trim($rs_list_trophy->Fields("title"))."<br/>";
                    $mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL.""; 
                    //print "<br/>".$str_from; print "<br/>".$str_to; print "<br/>".$str_to_member; print "<br/>".$mailbody; //exit;
                    sendmail($str_to,$str_subject,$mailbody,$str_from,1);
                    sendmail($str_to_member,$str_subject,$mailbody,$str_from,1);
            }
        }
        $rs_list_trophy->MoveNext();
    }
*/

while(!$rs_list_trophy->EOF())
                	{
// Send notification only if user's total donated amount fits between any membership level range
//print "<br/> " .$rs_list_trophy->fields("title"). "<br/>";
if($rs_list_member_donation_total->fields("total_sum") >= $rs_list_trophy->Fields("from_amount") && $rs_list_member_donation_total->fields("total_sum") <= $rs_list_trophy->Fields("to_amount"))
{
				$str_query_select = "";
	        	        $str_query_select = "SELECT * FROM t_freemember_trophy_notification WHERE memberpkid=".$rs_list_member->fields("pkid")." AND trophypkid=".$rs_list_trophy->Fields("pkid")."";
                              	//print "member trophy query:" .$str_query_select. "<br/><br/>"; //exit();
                                $rs_list_mem_trophy_notification = GetRecordSet($str_query_select);
                           
                                if($rs_list_mem_trophy_notification->EOF())
                                {
                                        $str_query_insert = "";
                                        $str_query_insert = "INSERT INTO t_freemember_trophy_notification(memberpkid, trophypkid, sent) VALUES (";
                                        $str_query_insert.= $rs_list_member->fields("pkid").",";
                                        $str_query_insert.= $rs_list_trophy->fields("pkid").",";
                                        $str_query_insert.="'YES')";
                                        ExecuteQuery($str_query_insert);
                                        //print $str_query_insert. "<br/>"; //exit();
                                        
                                        $str_subject="Contributor reached a new membership level on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
				        $mailbody="Contributor reached a new membership level. Below are details.<br/><br/>";
					$mailbody.="<strong>Contributor Name:</strong> ".$rs_list_member->fields("shortenurlkey")."<br/>";
				        $mailbody.="<strong>Membership Level:</strong> <br/>".trim($rs_list_trophy->Fields("title"))."<br/>";
				        $mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL.""; 
			                //print "<br/>1:".$str_from; print "<br/>".$str_to; print "<br/>".$str_to_member; print "<br/>".$str_subject; print "<br/>".$mailbody; //exit;
			                sendmail($str_to,$str_subject,$mailbody,$str_from,1);
                                        
                                        // If notification is ON by user then only send notification
					if($rs_list_member->Fields("getnotification")=='YES')
		                	{
                                            $str_subject="You have reached a new membership level on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
                                            $mailbody="Congratulations!  You have reached a new membership level.<br/><br/>";
                                            $mailbody.="<strong>Contributor Name:</strong> ".$rs_list_member->fields("shortenurlkey")."<br/>";
                                            $mailbody.="<strong>Membership Level:</strong> <br/>".trim($rs_list_trophy->Fields("title"))."<br/>";
                                            $mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL.""; 
			                    //print "<br/>2:".$str_from; print "<br/>".$str_to; print "<br/>".$str_to_member; print "<br/>".$str_subject;  print "<br/>".$mailbody; //exit;
                                            sendmail($str_to_member,$str_subject,$mailbody,$str_from,1);
					}
                                }
                                else
                                {
                                    if($rs_list_mem_trophy_notification->fields("sent")=="NO")
                                    {

					$str_subject="Contributor reached a new membership level on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
	                                $mailbody="Contributor reached a new membership level. Below are details.<br/><br/>";
		                        $mailbody.="<strong>Contributor Name:</strong> ".$rs_list_member->fields("shortenurlkey")."<br/>";
	        	                $mailbody.="<strong>Membership Level:</strong> <br/>".trim($rs_list_trophy->Fields("title"))."<br/>";
	        	                $mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL.""; 
					//print "<br/>3:".$str_from; print "<br/>".$str_to; print "<br/>".$str_to_member; print "<br/>".$str_subject;  print "<br/>".$mailbody; // exit;
                                    	sendmail($str_to,$str_subject,$mailbody,$str_from,1);
                                    	
					// If notification is ON by user then only send notification
                                    	if($rs_list_member->Fields("getnotification")=='YES')
                                    	{
                                            $str_subject="You have reached a new membership level on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
                                            $mailbody="Congratulations!  You have reached a new membership level.<br/><br/>";
                                            $mailbody.="<strong>Contributor Name:</strong> ".$rs_list_member->fields("shortenurlkey")."<br/>";
                                            $mailbody.="<strong>Membership Level:</strong> <br/>".trim($rs_list_trophy->Fields("title"))."<br/>";
                                            $mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL.""; 
					    //print "<br/>4:".$str_from; print "<br/>".$str_to; print "<br/>".$str_to_member; print "<br/>".$str_subject;  print "<br/>".$mailbody; //exit;
                                            sendmail($str_to_member,$str_subject,$mailbody,$str_from,1);
                                    	}
                                    }
                                }
                               
                            }
                            
                            $rs_list_trophy->MoveNext();
	                }
//exit;
#----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("./item_add_transaction_manual.php?type=S&msg=S");
exit();
?>
