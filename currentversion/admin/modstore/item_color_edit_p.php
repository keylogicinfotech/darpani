<?php
/*
Module Name:- modstore
File Name  :- item_color_edit_p.php
Create Date:- 21-JAN-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#----------------------------------------------------------------------------------------------------
# Get values of all passed GET / POST variables    
$int_pkid = 0;
if (isset($_POST["hdn_pkid"]))
{
    $int_pkid = trim($_POST["hdn_pkid"]);
}

if($int_pkid == 0 || $int_pkid == "" || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_color_list.php?msg=F&type=E". $str_redirect."&#ptop");
    exit();
}

$str_title = "";
$str_cmyk = "";
$str_rgb = "";
	
if (isset($_POST["txt_title"]))
{
    $str_title = trim($_POST["txt_title"]);
}
if (isset($_POST["txt_cmykcode"]))
{
    $str_cmyk = trim($_POST["txt_cmykcode"]);
}
if (isset($_POST["txt_rgbcode"]))
{
    $str_rgb = trim($_POST["txt_rgbcode"]);
}	
#----------------------------------------------------------------------------------------------------
#Prepare query string to pass variables back

$str_redirect="";
#----------------------------------------------------------------------------------------------------
#To check required parameters are passed properly or not

if($str_title == "" || $str_cmyk == "" /* || $str_rgb == ""*/)
{
    CloseConnection();
    Redirect("item_color_edit.php?msg=F&type=E". $str_redirect."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Duplicate Checking for Title field.
$str_query_select = "";
$str_query_select = "SELECT title FROM ".$STR_DB_TABLE_NAME_COLOR." WHERE title= '" . ReplaceQuote($str_title) . "' AND pkid!= ".$int_pkid;
$rs_list_check_duplicate = GetRecordSet($str_query_select);

if(!$rs_list_check_duplicate->eof())
{
    CloseConnection();
    Redirect("item_color_edit.php?msg=DU&type=E&". $str_redirect."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Update Query
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_COLOR." SET title = '".ReplaceQuote($str_title)."', cmyk_code = '".ReplaceQuote($str_cmyk)."', rgb_code = '".$str_rgb."' WHERE pkid = ".$int_pkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_color_list.php?type=S&msg=U&title=".urlencode(RemoveQuote($str_title))."&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------

?>

