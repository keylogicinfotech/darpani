<?php
/*
File Name  :- item_add_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
//ini_set('memory_limit', '512M');
include_once('../../PHPExcel-1.8.1/Classes/PHPExcel/IOFactory.php');


include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_file_system.php";
//include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_FILES); exit;
if(isset($_FILES['file_img']['tmp_name']) && $_FILES['file_img']['tmp_name'] != "")
{
    
    $uploadFilePath = $UPLOAD_IMG_PATH.basename($_FILES['file_img']['name']);

    if(file_exists($uploadFilePath))
    {
        DeleteFile($uploadFilePath);
    }
    
    move_uploaded_file($_FILES['file_img']['tmp_name'], $uploadFilePath);



    $inputFileName = $uploadFilePath;
    
    /*check point*/

    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
    
    
    /*$worksheetData = $objReader->listWorksheetInfo($inputFileName);
    $totalRows     = $worksheetData[0]['totalRows'];
    $totalColumns  = $worksheetData[0]['totalColumns'];*/

    $data = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

    //print the result
    /*print "COUNT : ".Count($data)."<br/>";
    print "ROW : ".$totalRows."<br/>";
    print "COLUMN : ".$totalColumns."<br/>";*/
    
    //exit;
    for($i = 2;  $i <= Count($data); $i++):
        /*$str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME." (product, pincode, city, state, region, prepaid, cod, reversepickup, pickup) VALUES('".$data[$i]['B']."', '".$data[$i]['C']."', '".$data[$i]['D']."', '".$data[$i]['E']."', '".$data[$i]['F']."', '".$data[$i]['G']."', '".$data[$i]['H']."', '".$data[$i]['I']."', '".$data[$i]['J']."' )";
        ExecuteQuery($str_query_insert);*/
        //print $str_query_insert."<br/>";
        
        if($data[$i]['A'] > 0) // This will not insert row with blank or 0 value catpkid
        {
            #Insert Query
            $str_query_insert = "";
            $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME."(catpkid,subcatpkid,itemcode,title, description,createdatetime, displayasnew,displayashot ,displayasfeatured, displayorder, visible, listprice, ourprice, memberprice, inmonth, inyear, availability, availability_info, approved, dshippingvalue,";
            $str_query_insert.= "dshippinginfo, ishippingvalue, ishippinginfo, weight, seotitle, seokeyword, seodescription) VALUES (";
            $str_query_insert.= "".$data[$i]['A'].", ";
            $str_query_insert.= "".$data[$i]['B'].", ";
            $str_query_insert.= "'".$data[$i]['C']."', ";
            $str_query_insert.= "'".$data[$i]['D']."', ";
            $str_query_insert.= "'".ReplaceQuote($data[$i]['E'])."', ";
            $str_query_insert.= "NOW(), ";
            $str_query_insert.= "'".$data[$i]['F']."', ";
            $str_query_insert.= "'".$data[$i]['G']."', ";
            $str_query_insert.= "'".$data[$i]['H']."', ";
            //$str_query_insert.= "'".ReplaceQuote($str_additional_info)."', ";
            $str_query_insert.= "0, ";
            $str_query_insert.= "'YES', ";
            $str_query_insert.= "".$data[$i]['I'].", ";
            $str_query_insert.= "".$data[$i]['J'].", ";
            $str_query_insert.= "".$data[$i]['K'].", ";
            $str_query_insert.= "09, ";
            $str_query_insert.= "2020, ";
            //$str_query_insert.= "'".ReplaceQuote($str_size)."', ";
            //$str_query_insert.= "'".ReplaceQuote($str_color)."', ";
            //$str_query_insert.= "'".ReplaceQuote($str_type)."', ";
            //$str_query_insert.= "'".ReplaceQuote($str_length)."', ";
            //$str_query_insert.= "'".ReplaceQuote($str_work)."', ";
            //$str_query_insert.= "'".ReplaceQuote($str_occasion)."', ";
            //$str_query_insert.= "'".ReplaceQuote($str_shipping_type)."', ";
            //$str_query_insert.= "'".ReplaceQuote($str_fabric)."', ";
            //$str_query_insert.= "".$int_dressmaterialprice.", ";
            //$str_query_insert.= "".$int_semistitchedprice.", ";
            //$str_query_insert.= "".$int_customizedprice.", ";
            $str_query_insert.= "'".$data[$i]['O']."', ";
            $str_query_insert.= "'".$data[$i]['P']."', ";
            $str_query_insert.= "'YES', ";
            $str_query_insert.= "0, ";
            $str_query_insert.= "'".$data[$i]['Q']."', ";
            $str_query_insert.= "0, ";
            $str_query_insert.= "'".$data[$i]['R']."', ";
            $str_query_insert.= "".$data[$i]['S'].", ";
            $str_query_insert.= "'".ReplaceQuote($data[$i]['T'])."', ";
            $str_query_insert.= "'".ReplaceQuote($data[$i]['U'])."', ";
            $str_query_insert.= "'".ReplaceQuote($data[$i]['V'])."') ";
            print $str_query_insert."<br/>"; //exit;
            ExecuteQuery($str_query_insert);
            
            $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME." ORDER BY pkid DESC LIMIT 0,1";
            $rs_list = GetRecordSet($str_query_select);
            print $rs_list->Fields("pkid")."<br/>";
        }
        
    endfor;
}
else
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}

exit;
#----------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=S&type=S");
exit();
?>
