<?php
/*
Module Name:- modstore
File Name  :- item_promocode_edit_p.php
Create Date:- 21-JAN-2018
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid = 0;
if(isset($_POST['hdn_pkid'])) 
{ $int_pkid = trim($_POST['hdn_pkid']); }

$str_title = "";
if(isset($_POST['txt_title'])) 
{ $str_title = trim($_POST['txt_title']); }


$int_discount_percentage = "";
if(isset($_POST['txt_percent']))
{  
    $int_discount_percentage = trim($_POST['txt_percent']);
    if($int_discount_percentage == "")
    {
        $int_discount_percentage = 0;
    }
}
$int_discount_amount = "";
if(isset($_POST['txt_amount']))
{ 
    $int_discount_amount = trim($_POST['txt_amount']);
    if($int_discount_amount == "")
    {
        $int_discount_amount = 0;
    }
}
$int_min_purchase=0.00;
if(isset($_POST["txt_min_purchase"]) && trim($_POST["txt_min_purchase"]) != "")
{
    $int_min_purchase = trim($_POST["txt_min_purchase"]);
}

$str_desc="";
if(isset($_POST["ta_desc"]))
{ $str_desc=trim($_POST["ta_desc"]); }
#------------------------------------------------------------------------------------------------------------
#To check whether required parameters are passed properly or not
 if($str_title == "")
{
	CloseConnection();
	Redirect("item_promocode_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid));
	exit();
}

#if both are blank then give error message
if($int_discount_percentage == "" && $int_discount_amount == "")
{
	CloseConnection();
	Redirect("item_promocode_edit.php?msg=DPA&type=E&pkid=".urlencode($int_pkid));
}

/*#if both are blank then give error message
if($int_discount_percentage < 0 || $int_discount_amount < 0)
{
	CloseConnection();
	Redirect("mod_shop_promocode.php?msg=PNV&type=E&pkid=".urlencode($int_pkid));
	exit();
}	*/

if($int_discount_percentage != "" )
{
    if(!is_numeric($int_discount_percentage) || $int_discount_percentage < 0)
    {
        CloseConnection();
        Redirect("item_promocode_edit.php?msg=PNV&type=E&pkid=".urlencode($int_pkid));
        exit();
    }
}

if($int_discount_amount !="")
{
    if(!is_numeric($int_discount_amount)  || $int_discount_amount<0)
    {
        CloseConnection();
	Redirect("item_promocode_edit.php?msg=PNV&type=E&pkid=".urlencode($int_pkid));
	exit();
    }
}
#-------------------------------------------------------------------------------------------------------------------
##Duplication Check
$str_select_query="";
$str_select_query="SELECT * FROM ".$STR_DB_TABLE_NAME_PROMOCODE." WHERE title='".ReplaceQuote($str_title)."' AND pkid!=".$int_pkid;
$rs_list_check_duplicate=GetRecordset($str_select_query);

if($rs_list_check_duplicate->EOF()==false)
{
    CloseConnection();
    Redirect("item_promocode_edit.php?msg=DU&type=E&pkid=".urlencode($int_pkid));
    exit();
}
#---------------------------------------------------------------------------------------------------------------------
#Update Query
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_PROMOCODE." SET "; 
$str_query_update = $str_query_update."title='".ReplaceQuote($str_title)."',";
$str_query_update = $str_query_update."description='".ReplaceQuote($str_desc)."',";
$str_query_update = $str_query_update."discountpercentage=".ReplaceQuote($int_discount_percentage).",";
$str_query_update = $str_query_update."discountamount=".ReplaceQuote($int_discount_amount).",";
$str_query_update = $str_query_update." minimumpurchaserequired=".ReplaceQuote($int_min_purchase)."";
$str_query_update = $str_query_update." WHERE pkid=".$int_pkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------------------------
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_promocode_list.php?msg=U&type=S");
exit();	
?>