<?php
/*
Module Name:- Manage photoset
File Name  :- mdl_ps_add_p.php
Create Date:- 8-Sep-2006
Intially Create By :- 0014
*/
#---------------------------------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
include "../../includes/lib_file_upload.php";
//include "./store_item_app_specific.php";
include "../../includes/lib_xml.php";
include "../../includes/lib_email.php";
//print_r($_POST); exit;
        //print_r($_FILES);exit; 
#----------------------------------------------------------------------------------------------------
#Initializing varialbles
$int_cat_pkid = "";
if(isset($_POST["catid"]) && trim($_POST["catid"]) != "" )
{   
    $int_cat_pkid = trim($_POST["catid"]);    
}

$str_name_key = "";
if(isset($_POST["key"]) && trim($_POST["key"]) != "" )
{ $str_name_key = trim($_POST["key"]); }

# get data for paging
$int_page = "";
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"]) != "" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"]) > 0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page = 1; }

$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";

$int_pkid = 0;
if(isset($_POST["hdn_pkid"]))
{ 
    $int_pkid = $_POST["hdn_pkid"]; 
}
if($int_pkid == "" || $int_pkid <= 0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}


$int_catpkid = 0;
$int_subcatpkid = 0;

$str_cattitle = "";
$str_subcattitle = "";
//$int_mdlpkid=0;
//$int_pricepkid = 0;
$int_listprice = 0.00;
$int_ourprice = 0.00;
$int_memberprice = 0.00;
//$int_promocodepkid = 0;
$str_color = array();
$str_size = array();
$str_length = array();
$str_occasion = array();
$str_shipping_type = array();
$str_fabric = array();
$str_work = array();
$str_type = array();
//$str_month="";
//$str_year="";
$str_itemcode = "";
$str_title = "";
//$str_sub_title = "";
//$str_nophoto="";

$str_desc = "";
//$str_name="";
//$str_purchase="YES";
$str_new = "YES";
$str_hot = "NO";
$str_featured = "NO";
//$str_ptitle = "";
//$str_purl = "";
$str_availability = "";
$str_availability_status = "";

$int_domestic_shipping_value = 0;
$int_international_shipping_value = 0;
$str_domestic_shipping_info = "";
$str_international_shipping_info = "";
$int_weight = 0;

$str_seo_title = "";
$str_seo_keywords = "";
$str_seo_desc = "";

//$int_dressmaterialprice = 0.00;
//$int_semistitchedprice = 0.00;
//$int_customizedprice = 0.00;

/* $str_video_file="";
$str_ext="";
 */
#-------------------------------------------------------------------------------------------------------------------------
# get form data
if(isset($_POST["cbo_cat"]) && trim($_POST["cbo_cat"])!="")
{
    $int_catpkid = trim($_POST["cbo_cat"]);
}
if(isset($_POST["cbo_subcat"]) && trim($_POST["cbo_subcat"])!="")
{
    $int_subcatpkid = trim($_POST["cbo_subcat"]);
}
if(isset($_POST["txt_code"]) && trim($_POST["txt_code"])!="")
{
    $str_itemcode = trim($_POST["txt_code"]);
}
if(isset($_POST["txt_title"]) && trim($_POST["txt_title"])!="")
{
    $str_title = trim($_POST["txt_title"]);
}
if(isset($_POST["ta_desc"]))
{
    $str_desc = trim($_POST["ta_desc"]);
}
/*if(isset($_POST["cbo_mdl"]) && trim($_POST["cbo_mdl"])!="")
{
    $int_mdlpkid=trim($_POST["cbo_mdl"]);
}
if(isset($_POST["cbo_price"]) && trim($_POST["cbo_price"])!="")
{
    $int_pricepkid=trim($_POST["cbo_price"]);
}*/
if(isset($_POST["txt_listprice"]) && trim($_POST["txt_listprice"])!="")
{
    $int_listprice = trim($_POST["txt_listprice"]);
}
if(isset($_POST["txt_ourprice"]) && trim($_POST["txt_ourprice"])!="")
{
    $int_ourprice = trim($_POST["txt_ourprice"]);
}
if(isset($_POST["txt_memberprice"]) && trim($_POST["txt_memberprice"])!="")
{
    $int_memberprice = trim($_POST["txt_memberprice"]);
}
/* if(isset($_POST["cbo_promocode"]) && trim($_POST["cbo_promocode"])!="")
{
    $int_promocodepkid=trim($_POST["cbo_promocode"]);
}
*/  
//print_r($_POST["cbx_color"]); exit;
if(isset($_POST["cbx_color"]) && $_POST["cbx_color"]!="")
{
    $str_color = $_POST["cbx_color"];
}
//if(Count($str_color) > 0)
//{
    $str_query_delete = "";
    $str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_COLOR." WHERE itempkid=".$int_pkid;
    //print $str_query_delete;
    ExecuteQuery($str_query_delete);
    
    foreach($str_color AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_COLOR."(masterpkid, itempkid) VALUES(".$i.", ".$int_pkid.")";
        //print $str_query_insert."<br/>";
        ExecuteQuery($str_query_insert);
    endforeach;
//}
//print(Count($str_color)); exit;


if(isset($_POST["cbx_size"]) && $_POST["cbx_size"]!="")
{
    $str_size = $_POST["cbx_size"];
}
//print Count($str_size); exit;
//if(Count($str_size) > 0)
//{
    $str_query_delete = "";
    $str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_SIZE." WHERE itempkid=".$int_pkid;
    //print $str_query_delete;exit;
    ExecuteQuery($str_query_delete);
    
    foreach($str_size AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_SIZE."(masterpkid, itempkid) VALUES(".$i.", ".$int_pkid.")";
        //print $str_query_insert."<br/>";
        ExecuteQuery($str_query_insert);
    endforeach;
//}


if(isset($_POST["cbx_type"]) && $_POST["cbx_type"]!="")
{
    $str_type = $_POST["cbx_type"];
}

//if(Count($str_type) > 0)
//{
    $str_query_delete = "";
    $str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_TYPE." WHERE itempkid=".$int_pkid;
    //print $str_query_delete;
    ExecuteQuery($str_query_delete);
    
    foreach($str_type AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_TYPE."(masterpkid, itempkid) VALUES(".$i.", ".$int_pkid.")";
        //print $str_query_insert."<br/>";
        ExecuteQuery($str_query_insert);
    endforeach;
//}

if(isset($_POST["cbx_length"]) && $_POST["cbx_length"]!="")
{
    $str_length = $_POST["cbx_length"];
}

//if(Count($str_length) > 0)
//{
    $str_query_delete = "";
    $str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_LENGTH." WHERE itempkid=".$int_pkid;
    //print $str_query_delete;
    ExecuteQuery($str_query_delete);
    
    foreach($str_length AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_LENGTH."(masterpkid, itempkid) VALUES(".$i.", ".$int_pkid.")";
        //print $str_query_insert."<br/>";
        ExecuteQuery($str_query_insert);
    endforeach;
//}

//print $_POST["cbx_work"]; exit;
if(isset($_POST["cbx_work"]) && $_POST["cbx_work"]!="")
{
    $str_work = $_POST["cbx_work"];    
}
//if(Count($str_work) > 0)
//{
    $str_query_delete = "";
    $str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_WORK." WHERE itempkid=".$int_pkid;
    //print $str_query_delete;
    ExecuteQuery($str_query_delete);
    
    foreach($str_work AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_WORK."(masterpkid, itempkid) VALUES(".$i.", ".$int_pkid.")";
        //print $str_query_insert."<br/>";
        ExecuteQuery($str_query_insert);
    endforeach;
//}


if(isset($_POST["cbx_occasion"]) && $_POST["cbx_occasion"]!="")
{
    $str_occasion = $_POST["cbx_occasion"];
}
//if(Count($str_occasion) > 0)
//{
    $str_query_delete = "";
    $str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_OCCASION." WHERE itempkid=".$int_pkid;
    //print $str_query_delete;
    ExecuteQuery($str_query_delete);
    
    foreach($str_occasion AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_OCCASION."(masterpkid, itempkid) VALUES(".$i.", ".$int_pkid.")";
        //print $str_query_insert."<br/>";
        ExecuteQuery($str_query_insert);
    endforeach;
//}

 /*   if(isset($_POST["cbx_shipping"]) && $_POST["cbx_shipping"]!="")
    {
        $str_shipping_type = $_POST["cbx_shipping"];
    }
    $str_query_delete = "";
    $str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_SHIPPING." WHERE itempkid=".$int_pkid;
    //print $str_query_delete;
    ExecuteQuery($str_query_delete);
    
    foreach($str_shipping_type AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_SHIPPING."(masterpkid, itempkid) VALUES(".$i.", ".$int_pkid.")";
        //print $str_query_insert."<br/>";
        ExecuteQuery($str_query_insert);
    endforeach;
*/
if(isset($_POST["cbx_fabric"]) && $_POST["cbx_fabric"]!="")
{
    $str_fabric = $_POST["cbx_fabric"];
}
//if(Count($str_fabric) > 0)
//{
    $str_query_delete = "";
    $str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_FABRIC." WHERE itempkid=".$int_pkid;
    //print $str_query_delete;
    ExecuteQuery($str_query_delete);
    
    foreach($str_fabric AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_FABRIC."(masterpkid, itempkid) VALUES(".$i.", ".$int_pkid.")";
        //print $str_query_insert."<br/>";
        ExecuteQuery($str_query_insert);
    endforeach;
//}

/*if(isset($_POST["txt_dressmaterialprice"]) && trim($_POST["txt_dressmaterialprice"])!="")
{
    $int_dressmaterialprice = trim($_POST["txt_dressmaterialprice"]);
}
if(isset($_POST["txt_semistitchedprice"]) && trim($_POST["txt_semistitchedprice"])!="")
{
    $int_semistitchedprice = trim($_POST["txt_semistitchedprice"]);
}
if(isset($_POST["txt_customizedprice"]) && trim($_POST["txt_customizedprice"])!="")
{
    $int_customizedprice = trim($_POST["txt_customizedprice"]);
}
*/
if(isset($_POST["txt_availability"])  && $_POST["txt_availability"] != "")
{
    $str_availability = trim($_POST["txt_availability"]);
}

if(isset($_POST["cbo_avail_status"])  && $_POST["cbo_avail_status"] != "")
{
    $str_availability_status = trim($_POST["cbo_avail_status"]);
}

/*if(isset($_POST["cbo_month"]))
{
    $str_month = date("m");
}
if(isset($_POST["cbo_year"]))
{
    $str_year = date("Y");
}*/
//$str_month = date("m");
//$str_year = date("Y");
	//print $str_year ; exit;


/*if(isset($_POST["txt_sub_title"]) && trim($_POST["txt_sub_title"])!="")
{
    $str_sub_title = trim($_POST["txt_sub_title"]);
}*/

/*if(isset($_POST["txt_nophoto"]) && trim($_POST["txt_nophoto"])!="")
{
    $str_nophoto=trim($_POST["txt_nophoto"]);
}
*/

if(isset($_POST["txt_dshipping_value"])  && $_POST["txt_dshipping_value"] != "")
{
    $int_domestic_shipping_value = trim($_POST["txt_dshipping_value"]);
}
if(isset($_POST["txt_dshipping_info"])  && $_POST["txt_dshipping_info"] != "")
{
    $str_domestic_shipping_info = trim(RemoveQuote($_POST["txt_dshipping_info"]));
}
//print $str_domestic_shipping_info;exit;
if(isset($_POST["txt_ishipping_value"])  && $_POST["txt_ishipping_value"] != "")
{
    $int_international_shipping_value = trim($_POST["txt_ishipping_value"]);
}
if(isset($_POST["txt_ishipping_info"])  && $_POST["txt_ishipping_info"] != "")
{
    $str_international_shipping_info = trim($_POST["txt_ishipping_info"]);
}
if(isset($_POST["txt_weight"])  && $_POST["txt_weight"] != "")
{
    $int_weight = trim($_POST["txt_weight"]);
}
if(isset($_POST["txt_seo_title"]) && trim($_POST["txt_seo_title"])!="")
{
    $str_seo_title = trim($_POST["txt_seo_title"]);
}
if(isset($_POST["txt_seo_keywords"]) && trim($_POST["txt_seo_keywords"])!="")
{
    $str_seo_keywords = trim($_POST["txt_seo_keywords"]);
}
if(isset($_POST["ta_seo_desc"]))
{
    $str_seo_desc = trim($_POST["ta_seo_desc"]);
}
//print $str_desc; exit;

if(isset($_POST["cbo_new"]))
{
    $str_new = trim($_POST["cbo_new"]);
}
if(isset($_POST["cbo_hot"]))
{
    $str_hot = trim($_POST["cbo_hot"]);
}
if(isset($_POST["cbo_featured"]))
{
    $str_featured = trim($_POST["cbo_featured"]);
}
/*if(isset($_POST["cbo_purchase"]))
{
    $str_purchase=trim($_POST["cbo_purchase"]);
}
if(isset($_POST['txt_ptitle']))
{
    $str_ptitle=trim($_POST['txt_ptitle']);
}
if(isset($_POST['txt_purl']))
{
    $str_purl=trim($_POST['txt_purl']);
}
if(isset($_FILES['video_clip']))
{
    $str_video_file=trim($_FILES['video_clip']['name']);
}
if(isset($_POST['cbo_extension']))
{
    $str_ext=trim($_POST['cbo_extension']); 
}
*/
//print_r($_POST); exit;
#----------------------------------------------------------------------------------------------------
#Check all validation
//print_r($_POST); exit;
if($int_subcatpkid == 0 || $str_title == ""  || $int_listprice == 0 || $int_listprice == "")
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid));
    exit();
}

if($int_ourprice == "" || $int_ourprice == 0)
{
    $int_ourprice = $int_listprice;
}

//print_r($str_color);exit;
#----------------------------------------------------------------------------------------------------
# Update Query
$str_query_update  = "";
$str_query_update  = "UPDATE ".$STR_DB_TABLE_NAME." SET catpkid = ".$int_catpkid.", subcatpkid = ".$int_subcatpkid.", ";
$str_query_update .= "itemcode='".ReplaceQuote($str_itemcode)."', ";
$str_query_update .= "title='".ReplaceQuote($str_title)."', ";
$str_query_update .= "description='".ReplaceQuote($str_desc)."', ";
$str_query_update .= "displayasnew='".ReplaceQuote($str_new)."', ";
$str_query_update .= "displayashot='".ReplaceQuote($str_hot)."', ";
$str_query_update .= "displayasfeatured='".ReplaceQuote($str_featured)."', ";
$str_query_update .= "listprice=".$int_listprice.", ";
$str_query_update .= "ourprice=".$int_ourprice.", ";
$str_query_update .= "memberprice=".$int_memberprice.", ";
//$str_query_update .= "size='".ReplaceQuote($str_size)."', ";
//$str_query_update .= "color='".ReplaceQuote($str_color)."', ";
//$str_query_update .= "type='".ReplaceQuote($str_type)."', ";

//$str_query_update .= "length='".ReplaceQuote($str_length)."', ";
//$str_query_update .= "work='".ReplaceQuote($str_work)."', ";
//$str_query_update .= "occasion='".ReplaceQuote($str_occasion)."', ";
//$str_query_update .= "shippingtype='".ReplaceQuote($str_shipping_type)."', ";
//$str_query_update .= "fabric='".ReplaceQuote($str_fabric)."', ";

//$str_query_update .= "freesizeprice=".$int_dressmaterialprice.", ";
//$str_query_update .= "semistitchedprice=".$int_semistitchedprice.", ";
//$str_query_update .= "customizedprice=".$int_customizedprice.", ";

$str_query_update .= "availability='".ReplaceQuote($str_availability_status)."', ";
$str_query_update .= "availability_info='".ReplaceQuote($str_availability)."', ";
$str_query_update .= "dshippingvalue=".$int_domestic_shipping_value.", ";
$str_query_update .= "dshippinginfo='". ReplaceQuote($str_domestic_shipping_info)."', ";
$str_query_update .= "ishippingvalue=".$int_international_shipping_value.", ";
$str_query_update .= "ishippinginfo='".ReplaceQuote($str_international_shipping_info)."', ";
$str_query_update .= "weight=".ReplaceQuote($int_weight).", ";

$str_query_update .= "seotitle='".ReplaceQuote($str_seo_title)."', ";
$str_query_update .= "seokeyword='".ReplaceQuote($str_seo_keywords)."', ";
$str_query_update .= "seodescription='".ReplaceQuote($str_seo_desc)."', ";

$str_query_update .= "lastupdatedatetime='".ReplaceQuote(date("Y-m-d H:i:s"))."' ";

$str_query_update .= "WHERE pkid=". $int_pkid;
//print $str_query_update; exit;

ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#write to xml file
//WriteXml();
#----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=U&type=S".$str_filter);
exit();
#----------------------------------------------------------------------------------------------------
?>