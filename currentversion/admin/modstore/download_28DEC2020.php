<?php
/*
File Name  :- item_add_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
//ini_set('memory_limit', '512M');
include_once('../../PHPExcel-1.8.1/Classes/PHPExcel/IOFactory.php');


include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";

include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
// include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
// include "../../includes/lib_file_system.php";
//include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_FILES); exit;
$objPHPExcel    =   new PHPExcel();
$str_query_select = "";
$str_query_select .= "SELECT sc.itempkid, ss.itempkid, st.catpkid, st.subcatpkid, st.itemcode, st.title, st.description, st.displayasnew, st.displayashot, st.displayasfeatured, st.listprice, st.ourprice, st.memberprice, st.availability, st.availability_info, st.dshippinginfo, st.ishippinginfo, st.weight, st.seotitle, st.seokeyword, st.seodescription,
                  Group_concat(distinct sc.masterpkid order by sc.pkid ASC separator  '|') as  color, 
				  Group_concat(distinct ss.masterpkid order by ss.pkid ASC separator '|') as  size,
                 Group_concat(distinct stt.masterpkid order by stt.pkid ASC separator '|')   as type,
                     Group_concat(distinct sl.masterpkid order by sl.pkid ASC separator '|')   as length,
  Group_concat(distinct so.masterpkid order by so.pkid ASC separator '|')   as ocassion,
  Group_concat(distinct sw.masterpkid order by sw.pkid ASC separator '|')   as work,
  Group_concat(distinct sf.masterpkid order by sf.pkid ASC separator '|')   as fabric FROM  ".$STR_DB_TABLE_NAME. " st ";
  //$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON a.catpkid=d.catpkid AND d.visible='YES' ";
$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_SIZE." ss ON st.pkid = ss.itempkid AND st.visible='YES'";
$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_COLOR." sc ON st.pkid = sc.itempkid AND st.visible='YES'";
$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_TYPE." stt ON st.pkid = stt.itempkid AND st.visible='YES'";
$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_LENGTH." sl ON st.pkid = sl.itempkid AND st.visible='YES'";
$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_OCCASION." so ON st.pkid = so.itempkid AND st.visible='YES'";
$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_WORK." sw ON st.pkid = sw.itempkid AND st.visible='YES'";
$str_query_select .= "LEFT JOIN ".$STR_DB_TR_TABLE_NAME_FABRIC." sf ON st.pkid = sf.itempkid AND st.visible='YES'";

$str_query_select .= "GROUP BY  sc.itempkid DESC LIMIT 20";

//$str_query_select .= $str_order_by;
//ExecuteQuery($str_query_select);
$rs_list = GetRecordSet($str_query_select);

///$ab=ExecuteQuery($str_query_select);
 //print_r($rs_list);

  
$objPHPExcel->setActiveSheetIndex(0);
 
$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'catpkid');
$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'subcatpkid');
$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'itemcode');
$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'title');
$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'description');
$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'displayasnew');
$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'displayashot');
$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'displayasfeatured'); 
$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'ListPrice'); 
$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'OurPrice'); 
$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'availability'); 
$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'availability_info'); 
$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'dshippinginfo'); 
$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'IshippingInfo'); 
$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Weight'); 
$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'SEOTitle'); 
$objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'SEOKeywords'); 
$objPHPExcel->getActiveSheet()->SetCellValue('R1', 'SEODescription'); 
$objPHPExcel->getActiveSheet()->SetCellValue('S1', 'color'); 
$objPHPExcel->getActiveSheet()->SetCellValue('T1', 'size'); 
$objPHPExcel->getActiveSheet()->SetCellValue('U1', 'type'); 
$objPHPExcel->getActiveSheet()->SetCellValue('V1', 'length'); 
$objPHPExcel->getActiveSheet()->SetCellValue('W1', 'ocassion'); 
$objPHPExcel->getActiveSheet()->SetCellValue('X1', 'work'); 
$objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'fabric'); 
$objPHPExcel->getActiveSheet()->getStyle("A1:Y1")->getFont()->setBold(true);
 
$rowCount   =   2;


 //foreach($rs_list as $idd){
   // $int_cnt = 1; 
   while($rs_list->eof() != true)
{
    
   
                                 

   
// echo $int_itempkid;
    //$int_itempkid = $rs_list->Fields['catpkid'];
	// $int_itempkid = $rs_list->Fields("catpkid");
    //    echo $rs->pkid;
   // print_r($int_itempkid);
   //$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, mb_strtoupper($objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, mb_strtoupper($row['catpkid'],'UTF-8')),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, mb_strtoupper($rs_list->Fields("catpkid"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, mb_strtoupper($rs_list->Fields("subcatpkid"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, mb_strtoupper($rs_list->Fields("itemcode"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, mb_strtoupper($rs_list->Fields("title"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, mb_strtoupper($rs_list->Fields("description"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, mb_strtoupper($rs_list->Fields("displayasnew"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, mb_strtoupper($rs_list->Fields("displayashot"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, mb_strtoupper($rs_list->Fields("displayasfeatured"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, mb_strtoupper($rs_list->Fields("listprice"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, mb_strtoupper($rs_list->Fields("ourprice"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, mb_strtoupper($rs_list->Fields("availability"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, mb_strtoupper($rs_list->Fields("availability_info"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, mb_strtoupper($rs_list->Fields("dshippinginfo"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, mb_strtoupper($rs_list->Fields("ishippinginfo"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, mb_strtoupper($rs_list->Fields("weight"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, mb_strtoupper($rs_list->Fields("seotitle"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, mb_strtoupper($rs_list->Fields("seokeyword"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount, mb_strtoupper($rs_list->Fields("seodescription"),'UTF-8'));
    // $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, mb_strtoupper($row['imageurl'],'UTF-8'));
  
    $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, mb_strtoupper($rs_list->Fields("color"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount, mb_strtoupper($rs_list->Fields("size"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount, mb_strtoupper($rs_list->Fields("type"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('V'.$rowCount, mb_strtoupper($rs_list->Fields("length"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('W'.$rowCount, mb_strtoupper($rs_list->Fields("ocassion"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('X'.$rowCount, mb_strtoupper($rs_list->Fields("work"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('Y'.$rowCount, mb_strtoupper($rs_list->Fields("fabric"),'UTF-8'));
               
   

 $rowCount++;
  $rs_list->MoveNext();

 }
 
 
$objWriter  =   new PHPExcel_Writer_Excel2007($objPHPExcel);
 
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");;
header("Content-Disposition: attachment;filename=test.xlsx");
header("Content-Transfer-Encoding: binary");
header('Content-Type: application/openxmlformats-officedocument.spreadsheetml.sheet');

// header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="rtt.xlsx"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
$objWriter->save('php://output');


 
   
                        
 
exit;
#----------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=S&type=S");
exit();
?>
