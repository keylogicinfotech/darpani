<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------------
$STR_TITLE_MODEL_APPROVED_BLOGS_COMMENTS = "brag comments";
#get filter parameters
#------------------------------------------------------------------------------------------------
/*$page=1;
if (isset($_GET["PagePosition"]))
{
    $page = trim($_GET["PagePosition"]);
}
// if model pkid passed then set filter criteria for modelpkid
$int_model_pkid="-1";
if (isset($_GET["modelpkid"]))
{
    $int_model_pkid = trim($_GET["modelpkid"]);
}
$str_filter_status="&modelpkid=".$int_model_pkid."&PagePosition=".$page."&";*/

# get data 
# To distinguish different redirect pages
if(isset($_GET["flg"]) && trim($_GET["flg"])!="" )
{ $str_redirect_page_flag=trim($_GET["flg"]); }

$int_cat_pkid=0;
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ $int_cat_pkid=trim($_GET["catid"]); }
$str_mname="";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_mname=trim($_GET["key"]); }

# get data for paging
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page=1; }

$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_mname."&PagePosition=".$int_page;

$int_mdlpkid = 0;
if(isset($_GET["mdlid"]))
{ $int_mdlpkid = trim($_GET["mdlid"]); }

$int_blogpkid="";
if(isset($_GET["blogpkid"]))
{ $int_blogpkid = trim($_GET["blogpkid"]); }
//print $int_blogpkid;exit;

$str_modelname="";
if(isset($_GET["mname"])) { $str_modelname=trim($_GET["mname"]); }

$int_mempkid=0;
if(isset($_POST["memid"]))
{ $int_mempkid=trim($_POST["memid"]); }

$str_membername="";
if(isset($_POST["memname"])) { $str_membername = trim($_POST["memname"]); }

$int_pkid=0;
if(isset($_GET['pkid']))
{ $int_pkid=$_GET['pkid']; }
if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
{ CloseConnection(); Redirect("item_comment_list.php?type=E&msg=F&mdlid=".$int_mdlpkid."&mname=".$str_modelname.$str_filter); exit(); }
#------------------------------------------------------------------------------------------------
#select query to get all details from tr_modelsearch_comment table
$str_select_query="";
$str_select_query="SELECT * FROM tr_blog where pkid=".$int_pkid;
$rs_edit=GetRecordset($str_select_query);
if($rs_edit->eof())
{
    CloseConnection();
    Redirect("item_comment_list.php?type=E&msg=F&mdlid=".$int_mdlpkid."&mname=".$str_modelname.$str_filter);
    exit();
}
#------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#	Get message type.
if(isset($_GET["type"]))
{
	switch(trim($_GET["type"]))
	{
		case("S"): $str_type = "S"; break;
		case("E"): $str_type = "E"; break;
		case("W"): $str_type = "W"; break;
	}
}
#	Get message text.
if(isset($_GET["msg"]))
{
	switch(trim($_GET["msg"]))
	{
		case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;				
		case("IE"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
		case("DI"): $str_message= $STR_MSG_ACTION_DELETE_IMAGE; break;
		case("IDE"): $str_message = "Please add title or add description.";	break;
		case("URL"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_MODEL_APPROVED_BLOGS_COMMENTS);?><?php if($str_modelname != "") { print " [".$str_modelname."]"; } else if($str_membername != "") { print " [".$str_membername."]"; } ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
	<?php include("../../includes/adminheader.php"); ?>
	<div class="row">
		<div class="col-lg-3 button_space">
			<div class="btn-group" role="group" aria-label="...">
				<?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;Back</a><?php */?>
				<a href="./item_comment_list.php?pkid=<?php print($rs_edit->fields("blogpkid"));?>&masterpkid=<?php print $int_masterpkid; ?>&mname=<?php print $str_modelname.$str_filter; ?>" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_MODEL_APPROVED_BLOGS_COMMENTS); ?><?php if($str_modelname != "") { print " [".$str_modelname."]"; } else if($str_membername != "") { print " [".$str_membername."]"; } ?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_MODEL_APPROVED_BLOGS_COMMENTS);?><?php if($str_modelname != "") { print " [".$str_modelname."]"; } else if($str_membername != "") { print " [".$str_membername."]"; } ?></a>
			</div>
		</div>
		<div class="col-lg-9" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_MODEL_APPROVED_BLOGS_COMMENTS);?><?php if($str_modelname != "") { print " [".$str_modelname."]"; } else if($str_membername != "") { print " [".$str_membername."]"; } ?></h3></div>
	</div><hr> 
	<?php if($str_type != "" && $str_message != "")	{ print(DisplayMessage(0,$str_message,$str_type)); }	?>
	<div class="row">
    	<div class="col-lg-12">
        	<div class="panel-group" id="accordion">
            	<div class="panel panel-default">
                	<div class="panel-heading">
                    	<h4 class="panel-title">
                            <div class="row">
                                <div class="col-md-12  padding-10">
                                    <div class="col-md-6 col-xs-8"> 
                                        <i class="glyphicon  glyphicon-edit "></i>&nbsp;&nbsp;<?php print($STR_TITLE_EDIT); ?> </a>
                                    </div>
                                    <div class="col-md-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>                                                                                      </div>
                            </div>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse">
                        <div class="panel-body">
                            <form name="frm_edit" action="item_comment_edit_p.php" method="post" onSubmit="return frm_edit_validateform();" enctype="multipart/form-data">
                                    <div class="HelpText" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <?php /* ?><div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Title</label><span class="HelpText"> *</span>
                                                <input id="txt_title" name="txt_title" value="<?php print(RemoveQuote($rs_edit->fields("title"))); ?>" class="form-control input-sm" maxlength="512" placeholder="Enter title here" type="text" tabindex="1">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Location</label><span class="HelpText"> </span>
                                                <input id="txt_loc" name="txt_loc" size="90" value="<?php print(RemoveQuote($rs_edit->fields("location"))); ?>" class="form-control input-sm" maxlength="512" placeholder="Enter location here" type="text" tabindex="2">
                                            </div>
                                        </div>
                                    </div>
                                    <?php */ ?>
                                    <div class="form-group">
                                        <label>Description</label><span class="HelpText"> *</span>
                                        <textarea name="ta_desc"  id="ta_desc" rows="20" class="form-control input-sm" placeholder="Enter description here" tabindex="1"><?php print(RemoveQuote($rs_edit->fields("description"))); ?></textarea>
                                    </div>
                                    <?php /* ?><div class="row">
                                        <div class="col-md-6">
                                            <label>Existing Image</label>
                                            <?php if($rs_edit->Fields("imagefilename") != "") { ?>
                                                <a href="blog_comment_del_img_p.php?pkid=<?php print($int_pkid);?>&blogpkid=<?php print $rs_edit->Fields("blogpkid"); ?>&mdlid=<?php print($int_mdlpkid);?>&mname=<?php print($str_modelname)?>&memid=<?php print($int_mempkid)?>&memname=<?php print($str_membername)?>" title="Click to delete image" onClick="return Confirm_Deletion();" class="link" tabindex="4">[Delete this image]</a><br>
                                                <img width="100%" class="img-responsive" title="Click to view actual size image" src="<?php print($UPLOAD_BLOG_IMG_FILE_PATH.$rs_edit->fields("imagefilename"));?>" border="0" alt="Comment Image" align="top">
                                            <?php } else { print(MakeBlankTab(0,0,"No Image Available","alert alert-danger")); }  ?>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Upload New Image</label><span class="HelpText"> </span>
                                                <input type="file" name="fileimage" id="fileimage" tabindex="5" />
                                            </div>
                                        </div>                                                                    
                                    </div><br/>
                                    <?php /* ?><div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Display As New</label><span class="HelpText"> *</span>
                                                <select name="cbo_display" class="form-control input-sm">
                                                    <option value="YES" <?php print(CheckSelected("YES",strtoupper($rs_edit->fields("displayasnew"))));?>>YES</option>
                                                    <option value="NO" <?php print(CheckSelected("NO",strtoupper($rs_edit->fields("displayasnew"))));?>>NO</option>
                                                </select>
                                            </div>
                                        </div>                                                                    
                                    </div><?php */ ?>
                                    <input type="hidden" name="hdn_pkid" value="<?php print($int_pkid);?>">
                                    <input type="hidden" name="PagePosition" value="<?php print($int_page);?>" >
                                    <input type="hidden" name="catid" value="<?php print($int_cat_pkid);?>" >
                                    <input type="hidden" name="key" value="<?php print($str_mname);?>" >
                                    <input type="hidden" name="mdlid" value="<?php print($int_mdlpkid);?>" >
                                    <input type="hidden" name="mname" value="<?php print($str_modelname);?>" >
                                    <input type="hidden" name="memid" value="<?php print($int_mempkid);?>" >
                                    <input type="hidden" name="memname" value="<?php print($str_membername);?>" >
                                    <input type="hidden" name="masterpkid" value="<?php print($rs_edit->fields("userpkid"));?>" >

                                    <input type="hidden" name="txt_desgn" value="<?php print(MyHtmlEncode($rs_edit->fields("designation")));?>">
                                    <?php /* ?><input type="hidden" name="txt_loc" value="<?php print(MyHtmlEncode($rs_edit->fields("location")));?>" ><?php */ ?>
                                    <input type="hidden" name="txt_urltitle" value="<?php //print(MyHtmlEncode($rs_edit->fields("urltitle"))); ?>">
                                    <input type="hidden" name="txt_url" value="<?php //print(MyHtmlEncode($rs_edit->fields("url"))); ?>">
                                    <input type="hidden" name="cbo_window" value="YES" >
                                    <input type="hidden" name="cbo_display" value="NO" >
                                    <input type="hidden" name="flg" value="<?php //print($str_redirect_page_flag); ?>">
                                    <?php print DisplayFormButton("EDIT",2); ?><?php print DisplayFormButton("RESET",3); ?>
                            </form>
                        </div>
                </div>
                </div>
                </div>
                </div>
	</div>
	<?php include "../../includes/help_for_edit.php"; ?>
</div>
<?php include "../../includes/include_files_admin.php"; ?>
    <script language="JavaScript" src="./item_comment_edit.js" type="text/javascript"></script>
<?php /* ?><link href="../includes/bootstrap.min.css" rel="stylesheet">
<link href="../includes/admin.css" rel="stylesheet">
<script language="JavaScript" src="../includes/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="./item_comment_edit.js" type="text/javascript"></script>
<script src="../includes/jquery.min.js"></script>
<script src="../includes/bootstrap.min.js"></script>  <?php */ ?>
<script type="text/javascript" src="../includes/richetxteditor.js"></script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_desc');
		//new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
  });
		// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
		 // convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_desc');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>
<script>
/*$( "#btn_submit_edit" ).click(function() {
if($('div .nicEdit-main').text()=="" && $("input#txt_title").val()=="" ){ alert('Please enter title or description');return false;}
});
$( "#btn_reset" ).click(function() {
    location.reload();
});*/
</script>
</body></html>
