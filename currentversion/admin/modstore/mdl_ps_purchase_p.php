<?php
/*
	Module Name:- Mod Photoset
	File Name  :- mdl_ps_purchase_p.php
	Create Date:- 7-sep-2006
	Intially Create By :- 0014
	Update History:-
*/
#--------------------------------------------------------------------------------------------------------------------------------------------------------
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "../includes/lib_data_access.php";
	include "./mdl_ps_config.php";
	include "../includes/lib_common.php";
	include "./mdl_ps_app_specific.php";
	include "../includes/lib_xml.php";
#---------------------------------------------------------------------------------------------------------
	# filter variables
	$int_mid="";
	#---------------------------------------------------------------------------------------------------------------------------------------------------------------
	#	get filter data
	if(isset($_GET["mid"]) && trim($_GET["mid"])!="" )
	{
		$int_mid=trim($_GET["mid"]);
	}

#	Get passed value and check whether all values are passed properly or not.			
	$int_photosetpkid="";
	$str_mode="";

	if(isset($_GET["pkid"]))
	{
		$int_photosetpkid=trim($_GET["pkid"]);
	}
	if(!is_numeric($int_photosetpkid) || $int_photosetpkid=="" || $int_photosetpkid<0 )
	{
		CloseConnection();
		Redirect("mdl_ps_list.php?msg=F&type=E&mid=".$int_mid."&#potp");
		exit();
	}
#---------------------------------------------------------------------------------------------------------
# Getting skill title to display message on cont_sub_list.php
	$str_select_query="SELECT subcatpkid,photosettitle,allowpurchase FROM tr_mdl_photoset where photosetpkid=".$int_photosetpkid;
	$rs_check=GetRecordSet($str_select_query);
	if($rs_check->EOF()==true)
	{
		CloseConnection();
		Redirect("mdl_ps_list.php?msg=F&type=E&mid=".$int_mid."&#potp");
		exit();
	}	
	
	$str_title=$rs_check->Fields("photosettitle");
	$str_mode=$rs_check->Fields("allowpurchase");
	$int_subcatpkid=$rs_check->Fields("subcatpkid");
#---------------------------------------------------------------------------------------------------------
#	Set the value for variable to be stored in database.
	if(strtoupper($str_mode)=="YES")
	{
		$str_set_text="NO";
	}
	else
	{
		$str_set_text="YES";
	}
#---------------------------------------------------------------------------------------------------------
#	Update query to update t_eyecolor.	
	$str_select_update="UPDATE tr_mdl_photoset SET allowpurchase='" .ReplaceQuote($str_set_text). "' where photosetpkid=".$int_photosetpkid;
	ExecuteQuery($str_select_update);
	
#-----------------------------------------------------------------------------------------------------------
	#write to xml file
	Write_Photoset_xml($int_subcatpkid);
#---------------------------------------------------------------------------------------------------------
#	Close connection and redirect to skill_list.php
	CloseConnection();
	Redirect("mdl_ps_list.php?msg=AP&type=S&title=".urlencode(RemoveQuote($str_title))."&mode=".urlencode(RemoveQuote($str_set_text))."&mid=".$int_mid."&#ptop");
	exit();
?>