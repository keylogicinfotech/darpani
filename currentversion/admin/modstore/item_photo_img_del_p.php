<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
include "../../includes/lib_file_system.php";
//include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$str_redirect_page_flag = "";
if(isset($_GET["flg"]) && trim($_GET["flg"])!="" )
{ $str_redirect_page_flag=trim($_GET["flg"]); }

$int_cat_pkid = 0;
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ $int_cat_pkid=trim($_GET["catid"]); }

$str_name_key = "";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_name_key=trim($_GET["key"]); }

# POST data for paging
$int_page = 0;
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page=1; }
//print $int_page; exit;

$int_userpkid = 0;
if(isset($_GET['uid']) && $_GET['uid']!= "")
{
    $int_userpkid=$_GET['uid'];
}

$int_masterpkid = 0;
if(isset($_GET['masterpkid']))
{
    $int_masterpkid=$_GET['masterpkid'];
}

if($int_masterpkid <= 0 || !is_numeric($int_masterpkid) || $int_masterpkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F");
    exit();
}
//print_r($_GET); exit;

$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&userpkid=".$int_userpkid."&masterpkid=".$int_masterpkid;
//print $str_filter;exit;

$int_pkid = 0;
if(isset($_GET['pkid']))
{
    $int_pkid = $_GET['pkid'];
}
//print $int_pkid; exit;
if($int_pkid <= 0 || !is_numeric($int_pkid) || $int_pkid == "")
{
    CloseConnection();
    Redirect("item_photo_edit.php?type=E&msg=F".$str_filter);
    exit();
}
$str_filter.="&pkid=".$int_pkid;
#----------------------------------------------------------------------------------------------------
#Select query to get the details from table
$str_query_select = "";
$str_query_select = "SELECT thumbphotofilename,largephotofilename, mainphotofilename FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE pkid=".$int_pkid;
$rs_list = GetRecordset($str_query_select);
//print $rs_list->Count(); exit;
if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_photo_edit.php?type=E&msg=F".$str_filter);
    exit();
}

$str_thumb_file_name = "";
$str_large_file_name = "";
$str_main_file_name = "";

$str_thumb_file_name = $rs_list->Fields("thumbphotofilename");
$str_large_file_name = $rs_list->Fields("largephotofilename");
$str_main_file_name = $rs_list->Fields("mainphotofilename");
#-----------------------------------------------------------------------------------------------------
$str_dir = "";
$str_dir = $UPLOAD_IMG_PATH.$int_pkid."/";
//print $str_dir; exit;
if(trim($str_thumb_file_name) != "")
{
    DeleteFile($UPLOAD_IMG_PATH.trim($str_thumb_file_name));
}

if(trim($str_large_file_name) != "")
{
    DeleteFile($UPLOAD_IMG_PATH.trim($str_large_file_name));
}
if(trim($str_main_file_name) != "")
{
    DeleteFile($UPLOAD_IMG_PATH.trim($str_main_file_name));
}

#-----------------------------------------------------------------------------------------------------
# Delete Query 
$str_query_delete = "";
$str_query_delete = "UPDATE ".$STR_DB_TABLE_NAME_PHOTO." SET mainphotofilename='', largephotofilename='', thumbphotofilename='' WHERE pkid=".$int_pkid;
//print $str_query_delete;exit;
ExecuteQuery($str_query_delete);
#-----------------------------------------------------------------------------------------------------
# Write to xml file
//WriteXml();
#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_photo_edit.php?type=S&msg=D".$str_filter);
exit();
#----------------------------------------------------------------------------------------------------- 
?>
