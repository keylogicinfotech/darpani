<?php
/*
    Module Name:- modstore
    File Name  :- item_set_new_p.php
    Create Date:- 18-MAR-2019
    Intially Create By :- 015
    Update History:
*/
#------------------------------------------------------------------------------------------------
    include "../../includes/validatesession.php";
    include "../../includes/configuration.php";
    include "../../includes/lib_data_access.php";
    include "../../includes/lib_common.php";
    include "item_config.php";
#------------------------------------------------------------------------------------------------
#get post data
#Get values of all passed GET / POST variables
#get filter data
$int_cat_pkid = "";
if(isset($_POST["catid"]) && trim($_POST["catid"]) != "" )
{   
    $int_cat_pkid = trim($_POST["catid"]);    
}

$str_name_key = "";
if(isset($_POST["key"]) && trim($_POST["key"]) != "" )
{ $str_name_key = trim($_POST["key"]); }

# get data for paging
$int_page = "";
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"]) != "" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"]) > 0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page = 1; }

$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";
//print $str_filter;exit;

$int_cnt = "";
if (isset($_POST["hdn_counter"]))
{
    $int_cnt = trim($_POST["hdn_counter"]);
}	
if($int_cnt == "" || $int_cnt < 0 || !is_numeric($int_cnt))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}
#----------------------------------------------------------------------------------------------------


$arr_strpkid="";
$arry_strnopkid = "";
//print_r($_POST); exit;
for ($i = 1;$i < $int_cnt;$i++)
{ 
    if (isset($_POST["chk_alldelete" . $i]))
    {
        if (($_POST["chk_alldelete" . $i])=="on")
        {
            $arr_strpkid = $arr_strpkid . $_POST["hdn_pkid" . $i] . ",";
        }
    }
    else
    {
        $arry_strnopkid = $arry_strnopkid . $_POST["hdn_pkid" . $i] . ",";
    }
}

$arr_strpkid = substr($arr_strpkid,0,strrpos($arr_strpkid,","));
//print $arr_strpkid;
$arry_strnopkid = substr($arry_strnopkid,0,strrpos($arry_strnopkid,","));
//print $arry_strnopkid; exit;
#------------------------------------------------------------------------------------------------
#delete table
/*if(file_exists($UPLOAD_IMG_PATH.$int_pkid))
{
    RemoveDirectory($UPLOAD_IMG_PATH.$int_pkid);
}*/

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TABLE_NAME." WHERE pkid IN (" . $arr_strpkid . ")";
//print $str_query_delete; exit;
ExecuteQuery($str_query_delete);


$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_COLOR." WHERE itempkid IN (" . $arr_strpkid . ")";
//print $str_query_delete; exit;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_SIZE." WHERE itempkid IN (" . $arr_strpkid . ")";
//print $str_query_delete; exit;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_TYPE." WHERE itempkid IN (" . $arr_strpkid . ")";
//print $str_query_delete; exit;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_FABRIC." WHERE itempkid IN (" . $arr_strpkid . ")";
//print $str_query_delete; exit;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_OCCASION." WHERE itempkid IN (" . $arr_strpkid . ")";
//print $str_query_delete; exit;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_LENGTH." WHERE itempkid IN (" . $arr_strpkid . ")";
//print $str_query_delete; exit;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_WORK." WHERE itempkid IN (" . $arr_strpkid . ")";
//print $str_query_delete; exit;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid IN (" . $arr_strpkid . ")";
//print $str_query_delete; exit;
ExecuteQuery($str_query_delete);
#-----------------------------------------------------------------------------------------------------------
#update t_product_category table.
//Synchronize($int_cpkid);
#------------------------------------------------------------------------------------------------------------
#Close connection and redirect to prod_list.php page	
CloseConnection();
Redirect("item_list.php?type=S&msg=DA".$str_filter);
exit();
#------------------------------------------------------------------------------------------------------------
?>
