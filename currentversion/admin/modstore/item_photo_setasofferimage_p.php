<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
//include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$str_redirect_page_flag = "";
if(isset($_GET["flg"]) && trim($_GET["flg"])!="" )
{ $str_redirect_page_flag=trim($_GET["flg"]); }

$int_cat_pkid = 0;
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ $int_cat_pkid=trim($_GET["catid"]); }

$str_key = "";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_key=trim($_GET["key"]); }

# POST data for paging
$int_page = 0;
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page = 1; }

$str_flag = "";
if(isset($_GET['flg']) && $_GET['flg'] != "")
{
    $str_flag = $_GET['flg'];
}
//print $str_flag;exit;
//print_r($_GET); exit;
/*$int_userpkid = 0;
if(isset($_GET['uid']) && $_GET['uid']!= "")
{
    $int_userpkid=$_GET['uid'];
}*/

$int_masterpkid = 0;
if(isset($_GET['masterpkid']))
{
    $int_masterpkid=$_GET['masterpkid'];
}

if($int_masterpkid <= 0 || !is_numeric($int_masterpkid) || $int_masterpkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F");
    exit();
}
//print_r($_GET); exit;

$int_pkid = 0;
if(isset($_GET['pkid']))
{
    $int_pkid=$_GET['pkid'];
}
if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F".$str_filter);
    exit();
}


$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_key."&PagePosition=".$int_page."&pkid=".$int_masterpkid."&#scroll".$int_pkid;
//print $str_filter; exit;

#----------------------------------------------------------------------------------------------------
#Select query to get the details from table
$str_query_select="";
$str_query_select="SELECT setasofferimage, setasfront FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE pkid=".$int_pkid;
$rs_list = GetRecordset($str_query_select);

if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_photo_list.php?type=E&msg=F".$str_filter);
    exit();
}
$str_setasfront = "";
$str_setasfront = $rs_list->Fields("setasfront");

$str_setasofferimage = "";
$str_setasofferimage = $rs_list->Fields("setasofferimage");

//print $str_setasofferimage;exit;

if(strtoupper($str_setasfront)=='YES')
{
    CloseConnection();
    Redirect("item_photo_list.php?type=E&msg=IASF".$str_filter);
    exit();
}

if($str_flag == "L")
{
    #---------------------------------------------------------------------------------------------------------
    #Update Query
    $str_query_update = "";    
    $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_PHOTO." SET setasofferimage='LEFT' WHERE pkid=".$int_pkid;
    ExecuteQuery($str_query_update);

    #Update query.	
    $str_query_update = "";
    $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_PHOTO." SET setasofferimage='' WHERE pkid!=".$int_pkid." AND setasofferimage='LEFT' AND masterpkid=".$int_masterpkid;
    //        print $str_select_update;exit;
    ExecuteQuery($str_query_update);
    
}
else if($str_flag == "R")
{
     #---------------------------------------------------------------------------------------------------------
    #Update Query
    $str_query_update = "";    
    $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_PHOTO." SET setasofferimage='RIGHT' WHERE pkid=".$int_pkid;
    //print $str_query_update;exit;
    ExecuteQuery($str_query_update);

    #Update query.	
    $str_query_update = "";
    $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_PHOTO." SET setasofferimage='' WHERE pkid!=".$int_pkid." AND setasofferimage='RIGHT' AND masterpkid=".$int_masterpkid;
    //        print $str_select_update;exit;
    ExecuteQuery($str_query_update);
    
}


#-----------------------------------------------------------------------------------------------------
#Write to xml file
//WriteXml();
#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_photo_list.php?type=S&msg=SF".$str_filter);
exit();
#-----------------------------------------------------------------------------------------------------
?>
