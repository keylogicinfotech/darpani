<?php
/*
	Module Name:- modvideo
	File Name  :- video_view.php
	Create Date:- 22-MAR-2006
	Intially Create By :- 0022
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "./mdl_ps_config.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";	
#----------------------------------------------------------------------------------------------------------------------------------------------------------------	
#	To get query string data.
	$int_pkid="";
	if(isset($_GET["pkid"]))
	{
		$int_pkid =trim($_GET["pkid"]);
	}
	
	//print_r($_GET);exit;
# 	if id is empty then to close window
	if($int_pkid=="" || !is_numeric($int_pkid) || $int_pkid < 0)
	{
?>
	<script language="JavaScript">
		window.close()
	</script>
<?php 
	}
#select query to get large image path from t_video table.
	$str_query_select="SELECT photosetfilename FROM  t_photoset WHERE photosetpkid='".$int_pkid."'";
	//exit;
	$rs_video=GetRecordSet($str_query_select);
	if($rs_video->eof())
	{
	?>
	<script language="JavaScript">
		window.close()
	</script>
	<?php
	}
	$str_path=$UPLOAD_FILE_PATH.$rs_video->fields("photosetfilename");
?>
<html>
<head>
<title>:: Video Clip ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" style="text/css" href="../includes/admin.css">
</head>
<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td valign="top" align="left"><?php include "../includes/popupheader.php" ?></td>
  </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
 <tr>
 	<td height="390" valign="baseline" background="../images/popup_center_bg.jpg"> 
	   <table width="100%" border="0" align="center" cellpadding="1" cellspacing="2">
 		 <tr><td height="20"></td></tr>
	  	  <tr>
		  <td align="center" valign="top" class="FieldValueText8ptBold">
			<?php print(MyHtmlEncode($rs_video->Fields("photosetfilename")));?>		  
		</td>
		  </tr>
	      <tr>  
		  <td align="center" valign="top">
			<object width="375" height="325"
				classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95">
				<param type="video/x-mpeg2" />
				<param name="FileName" value="<?php print($str_path); ?>" />
				<param name="AutoStart" value="true" />
				<embed type="application/x-mplayer2" src="<?php print($str_path); ?>" width="375" height="325" autoplay="true" pluginspage="http://www.microsoft.com/Windows/Downloads/Contents/MediaPlayer/" name="MediaPlayer">
				</embed>
			</object>	  
		  </td>			    
		  </tr>
      </table>   		
      <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
		<tr><td height="10"></td></tr>
		<tr>
	     <td align="center"><a href="#" onClick="javascript: window.close();" class="NavigationLink">Close 
            Window</a></td>
		</tr>
	</table>
    </td>
  </tr>
</table>
<table border="0" width="100%" cellpadding="0" cellspacing="0" align="center">
 <tr> 
   <td valign="top">
   <?php include "../includes/popupfooter.php";
   		CloseConnection();
   ?></td>
 </tr>
</table>
</body>
</html>
