<?php
/*
	Module Name:- modstore
	File Name  :- mdl_ps_photo_large_image.php
	Create Date:- 8-Sep-2006
	Intially Create By :- 0014
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "mdl_ps_config.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";	
#----------------------------------------------------------------------------------------------------------------------------------------------------------------	
	
#	To get query string data.
	$int_tpkid="";
	$int_pkid="";
	
	if(isset($_GET["tpkid"]))
	{
		$int_tpkid = $_GET["tpkid"];
	}

# 	if id is empty then to close window
	if($int_tpkid<=0 || !is_numeric($int_tpkid) || $int_tpkid=="")
	{
?>
	<script language="JavaScript">
		window.close()
	</script>
<?php 
	}
	
	if(isset($_GET["pkid"]))
	{
		$int_pkid = $_GET["pkid"];
	}
	# 	if id is empty then to close window
	if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
	{
?>
	<script language="JavaScript">
		window.close()
	</script>
<?php 
	}

#select query to get large image path from tr_promogallery_photo table.
	$str_query_select="select mainphotofilename from tr_store_photo where pkid=".$int_tpkid." and pkid=".$int_pkid;
	$rs_photo=GetRecordSet($str_query_select);
	
	if($rs_photo->eof())
	{
	?>
	<script language="JavaScript">
		window.close()
	</script>
	<?php
	}
?>
<html>
<head>
<title>:: Actual Size Photograph ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" style="text/css" href="../includes/admin.css">
</head>
<body>
<?php include "../includes/popupheader.php" ?>
<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" height="100%">
 <tr>
 	<td height="390" valign="baseline" background="../images/popup_center_bg.jpg"> 
	 <table width="100%" border="0" align="center" cellpadding="1" cellspacing="2">
		<tr><td height="20"></td></tr>
	  	  <tr>
		  <td align="center" valign="top"><span class="FieldValueText8ptBold">Actual Size Photograph</span></td>
		  </tr>
	      <tr>  
		  <td align="center" valign="top">
		  <img alt="Photograph" src="<?php print($UPLOAD_IMG_PATH.$int_pkid."/".$rs_photo->fields("mainphotofilename"));?>">
		   </td>			    
		  </tr>
      </table>   		
	  <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
		<tr><td height="10"></td></tr>
		<tr>
	     <td align="center"><a href="#" onClick="javascript: window.close();" class="NavigationLink">Close 
            Window</a></td>
		</tr>
	</table>
    </td>
  </tr>
</table>
<?php include "../includes/popupfooter.php" ?>
</body>
</html>