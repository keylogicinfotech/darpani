<?php
/*
	Module Name:- Manage Photoset
	File Name  :- mdl_ps_add.php
	Create Date:- 7-Sep-2006
	Intially Create By :- 0014
	Update History:
*/
#----------------------------------------------------------------------------------------------------------------------------
#Include Files
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "./item_config.php";
        include "./item_app_specific.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
	include "../includes/lib_datetimeyear.php";
#-------------------------------------------------------------------------------------------------------------------------------
	#initialize variables	
	# filter variables
	$int_mid="";
	
	$str_ext="";

#-------------------------------------------------------------------------------------------------------------------------------
#	get filter data
	if(isset($_GET["mid"]) && trim($_GET["mid"])!="" )
	{
		$int_mid=trim($_GET["mid"]);
	}
	
#---------------------------------------------------------------------------------------------------------------------------------------------------------------
?>
<html>
<head>
<title>:: Add Photoset / Video::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="../includes/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="item_add.js" type="text/javascript"></script>
</head>
<link href="../includes/admin.css" rel="stylesheet" type="text/css">
<body>
<table width="778" border="0" cellpadding="0" cellspacing="0" align="center">
 <tr> 
  <td><?php include "../includes/adminheader.php";?></td>
 </tr>
</table>
<a name="ptop"></a>
<table width="778" align="center" cellpadding="0" cellspacing="0">
 <tr> 
  <td background="../images/tablebg.gif" height="390" valign="top">
   <table width="740" border="0" align="center" cellpadding="0" cellspacing="0">
   <tr><td height="20"></td></tr>
    <tr> 
     <td align="center" height="25" class="PageHeader">Add Photoset / Video</td>
    </tr>
	<tr> 
     <td height="2" align="right" class="TableHeader8ptBold"></td>
   </tr>
   </table>
   <table width="740" border="0" align="center" cellpadding="2" cellspacing="0">
    <tr> 
     <td width="95%" height="20" class="DescriptionText8ptNormal">
	 <a href="../admin_home.php" title="Go to admin main menu" 
	 class="NavigationLink">Main Menu</a> &gt;&gt; <a href="item_list.php?mid=<?php print($int_mid);?>" title="Go to Photoset list" class="NavigationLink">Photoset / Video List</a> &gt;&gt; Add Photoset / Video</td>
     <td width="5%" align="right" class="DescriptionText8ptNormal">&lt;&lt;&nbsp;<a 
	 href="item_list.php?mid=<?php print($int_mid);?>" class="NavigationLink" title="Back to photoset list">Back</a></td>
	</tr>
   </table>
   <a name="ptop"></a><?php
#------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
   	$str_type = "";
	$str_message = "";
	$str_subcat_title="";
	$str_title="";
	$str_mode="";
	
	if(isset($_GET['title']))
	{
		$str_title=MyHtmlEncode(RemoveQuote(MakeStringShort(trim($_GET['title']),50)));
	}

#	Get message type.
   		if(isset($_GET["type"]))
		{
			switch(trim($_GET["type"]))
			{
				case("S"):
					$str_type = "S";
					break;
				case("E"):
					$str_type = "E";
					break;
				case("W"):
					$str_type = "W";
					break;
			}
		}
#	Get message text.
		if(isset($_GET["msg"]))
		{
			switch(trim($_GET["msg"]))
			{
                                case("S"): 
                                        $str_message = $STR_MSG_ACTION_ADD; 
                                        break;
				case("AS"):
					$str_message = "Photoset '$str_title' details added successfully.";
					break;
				case("F"):
					$str_message = "Some information(s) missing. Please try again.";
					break;
				case("DT"):
					$str_message = "Photoset title '$str_title' already exists for selected sub category. please try with another photoset title.";
					break;
				case("IU"):
					$str_message = "Invalid URL format.";
					break;
				case("IE"):
					$str_message = "Invalid photoset / video file extension.";
					break;	
			}
		}
#	Diplay message.
		if($str_type != "" && $str_message != "")
		{
			print(DisplayMessage(740,$str_message,$str_type));
		}
	?>
   <br>
      <table width="740" border="0" cellspacing="0" cellpadding="0" align="center">
   <tr>
    <td width="100%"> 
     <table width="100%" height="23" border="0" cellspacing="0" cellpadding="0">
      <tr class="TableHeader10ptBold">
       <td width="95%">Enter Photoset / Video Details</td>
       <td align="right"><a href="#help" class="NavigationLink" title="Click to view help">Help</a></td>
      </tr>
     </table> 
	</td>
   </tr>
   <tr>
    <td>
	 <table width="100%" border="0" align="center" cellpadding="3" cellspacing="0" class="tbl">
     <form name="frm_add" action="item_add_p.php" method="post" onSubmit="return frm_add_validateform();" enctype="multipart/form-data">
	  <tr><td height="10" colspan="2"></td></tr>
	  <tr>
		<?php
			$int_subcat_pkid="";
			if(isset($_GET['subcatpkid']))
			{
				$int_subcat_pkid=trim($_GET['subcatpkid']);
			}
			
			# Select Query to display fill combobox
			$str_query_select="";
			$str_query_select = "SELECT a.subcatpkid,a.subcattitle,b.title,b.catpkid,count(c.subcatpkid) totalphotoset ";
			$str_query_select .= "FROM t_photoset_subcat a ";
			$str_query_select .= "LEFT JOIN t_photoset_cat b ON a.catpkid = b.catpkid ";
			$str_query_select .= "LEFT JOIN t_photoset c ON a.subcatpkid=c.subcatpkid ";
			$str_query_select .= " GROUP BY a.subcatpkid ";
			$str_query_select .= "ORDER BY b.displayorder desc,b.title,a.subcattitle ";
			$rs_cat_subcat=GetRecordSet($str_query_select);
		?>
  	    <td height="20" align="right" valign="top" class="FieldCaptionText8ptNormal">Select Sub Category:<span class="HelpText">*</span> </td>
  	    <td>
			<select name="cbo_subcat">
				<option class="FieldCaptionText8ptBold" <?php print(CheckSelected("0",$int_subcat_pkid));?> value="0">-- Select Sub Category --</option>
					<?php
						while(!$rs_cat_subcat->EOF())
						{
						$int_old_archivepkid=$rs_cat_subcat->fields("catpkid");
						$int_new_archivepkid=$rs_cat_subcat->fields("catpkid");
					?>
						<optgroup class="FieldValueText8ptBold" label="<?php print(MyHtmlEncode($rs_cat_subcat->fields("title")));?>">
						<?php
							while($int_old_archivepkid==$int_new_archivepkid && !$rs_cat_subcat->eof())
							{
							?>
							<OPTION class="FieldCaptionText8ptNormal" <?php print(CheckSelected($rs_cat_subcat->fields("subcatpkid"),$int_subcat_pkid));?> VALUE='<?php print($rs_cat_subcat->fields("subcatpkid"));?>'><?php print(MyHtmlEncode($rs_cat_subcat->fields("subcattitle")));?> [<?php print(MyHtmlEncode($rs_cat_subcat->fields("totalphotoset")));?>]</OPTION>
							<?php
							$rs_cat_subcat->MoveNext();
							$int_new_archivepkid = $rs_cat_subcat->fields("catpkid");
							}?></optgroup>
				  <?php
						}
					?>
			</select>
		</td>
	  </tr>
	  <tr>
	    <td height="10" colspan="2"></td>
	    </tr> 
	  <tr>
        <td align="right" valign="middle" class="FieldCaptionText8ptNormal">Consider  in:<span class="HelpText">*</span> </td>
        <td valign="middle">
          <?php 
			$str_m=""; 
			$str_y="";

			if(isset($_GET["rmonth"]) && trim($_GET["rmonth"])!="")
			{
				$str_m=trim($_GET["rmonth"]);
			}

			if(isset($_GET["ryear"]) && trim($_GET["ryear"])!="")
			{
				$str_y=trim($_GET["ryear"]);
			}

			if($str_m=="" && $str_y=="")
			{
				$str_y=date("Y");
				$str_m=date("n");
			}
			print(DisplayMonth($str_m,"Month")." &nbsp;");
			print(DisplayYear($str_y,0));
		?>
        </td>
	  </tr>  	   	  
	  <tr><td height="10" colspan="2"></td></tr>	 
  	  <tr>
  	    <td height="20" align="right" valign="top" class="FieldCaptionText8ptNormal">Photoset / Video Title:<span class="HelpText">*</span> </td>
  	    <td width="560"><input type="text" name="txt_title" class="clsTextBox" maxlength="255" size="70" value="<?php if(isset($_GET["t_title"]) && trim($_GET["t_title"])!=""){ print(RemoveQuote(trim($_GET["t_title"]))); } ?>">
		<br><span class="HelpText"><?php print($STR_MAX_255_MSG);?></span></td>
	  </tr>
	<tr><td height="5"></td><td></td></tr>
	  <tr>
		<?php
			$int_list_price="";
			if(isset($_GET['listprice']))
			{
				$int_list_price=trim($_GET['listprice']);
			}
                        $int_our_price="";
			if(isset($_GET['ourprice']))
			{
				$int_our_price=trim($_GET['ourprice']);
			}
			
			# Select Query to display fill combobox
			$str_query_select="";
			/*$str_query_select = "SELECT userpricetitle,ccbillpricetitle,pkid from t_photoset_ccbill_price ORDER BY displayorder, ccbillpricetitle";*/
                        $str_query_select = "SELECT title,description,pkid from t_photoset_ccbill_price ORDER BY displayorder, description";
                       // print $str_query_select; exit;
			$rs_price=GetRecordSet($str_query_select);
		?>
              <td><input type="text" name="txt_lprice" id="txt_lprice" class="form-control input-sm" placeholder="Enter photoset / list price" value="<?php if(isset($_GET["t_lprice"]) && trim($_GET["t_lprice"])!=""){ print(RemoveQuote(trim($_GET["t_lprice"]))); } ?>" tabindex="3"></td>
              <td><input type="text" name="txt_oprice" id="txt_oprice" class="form-control input-sm" placeholder="Enter photoset / our price" value="<?php if(isset($_GET["t_oprice"]) && trim($_GET["t_oprice"])!=""){ print(RemoveQuote(trim($_GET["t_oprice"]))); } ?>" tabindex="3"></td>
  <?php /* ?>	    <td height="20" align="right" valign="top" class="FieldCaptionText8ptNormal">Select  Price: </td>
  	    <td class="HelpText">
			<select name="cbo_price">
				<option <?php print(CheckSelected("0",$int_price_pkid));?> value="0">-- Select Photoset Price --</option>
					<?php
						while(!$rs_price->EOF())
						{?>
							<OPTION <?php print(CheckSelected($rs_price->fields("pricepkid"),$int_price_pkid));?> VALUE='<?php print($rs_price->fields("pricepkid"));?>'><?php print(MyHtmlEncode($rs_price->fields("userpricetitle")));?></OPTION>
						<?php
						$rs_price->MoveNext();
						}
					?>
			</select>
			<br>
			Note:
If	you will not set price then Visitors can't purchase photoset <br>
and &quot;Coming Soon..&quot; will be Dsiplayed instead of price.</td> <?php */ ?>
	  </tr>  	  
	  <tr><td height="10" colspan="2"></td></tr>
          <tr>
<!--              <td height="20" align="left" valign="top" class="FieldCaptionText8ptNormal">Color:</td>-->
          </tr>
	  <tr>
        <!--<td height="20" align="right" valign="top" class="FieldCaptionText8ptNormal">Special Note:</td>-->
        <!--<td class="HelpText"><input type="text" name="txt_nophoto" class="clsTextBox" maxlength="255" size="70" value="<?php // if(isset($_GET["nophoto"]) && trim($_GET["nophoto"])!=""){ print(RemoveQuote(trim($_GET["nophoto"]))); } ?>">-->
            <!--<br><?php // print($STR_MAX_255_MSG);?><br>e.g. 50 Photos, R Rating Video, etc.</td>-->
	    </tr>
  	  <tr><td height="5"></td><td></td></tr>
  	  <tr>
  	    <td height="20" align="right" valign="top" class="FieldCaptionText8ptNormal"> Description:</td>
  	    <td class="HelpText">
		<textarea name="ta_desc" id="ta_desc" class="clsTextArea" rows="8" cols="80"><?php if(isset($_GET["desc"]) && trim($_GET["desc"])!=""){ print(RemoveQuote(trim($_GET["desc"]))); } ?></textarea>
		<br><?php print($STR_HTML_MSG);?>
		</td>
	  </tr>  	   	    	 
  	  <tr><td height="10"></td><td></td></tr>
	  
		<tr><td height="20" align="left" valign="top"></td><td align="left" class="HelpText"><br><?php print($STR_VIDEO_METHOD1_MSG);?></td></tr>
		<tr>
			<td align="right" valign="top" class="FieldCaptionText8ptNormal">Upload Photoset / Video File:</td>
			<td align="left" valign="top" class="HelpText">
			<input type="file" name="video_clip" maxlength="255" size="80">
			<br><?php print($STR_VIDEO_FILE_TYPE);?><br>
			</td>
		</tr>
		<tr><td height="30" colspan="2" align="center" valign="middle" class="HighLightText8ptBold">OR</td></tr>
		<tr>
			<td height="20" align="left" valign="top"></td>
			<td align="left" class="HelpText">
			<?php
			#select query to get details from siteconfiguration table.
			$str_query_select = "SELECT * FROM siteconfiguration  WHERE settingkey='p_VIDEO_FTP_USERNAME' OR settingkey='p_VIDEO_FTP_PASSWORD'";
			$rs_siteconfig_list = GetRecordSet($str_query_select);
			
			$str_ftp_details=array(); 
			$int_cnt=0;
			while(!$rs_siteconfig_list->EOF())
			{
				$str_ftp_details[$int_cnt]=$rs_siteconfig_list->fields("settingvalue");
				//print($str_ftp_details[$int_cnt])."<BR>";
				$rs_siteconfig_list->movenext();
				$int_cnt=$int_cnt+1;		
			}
 
			print($STR_VIDEO_METHOD2_MSG." <B>".$str_ftp_details[0]."</B> / <B>".$str_ftp_details[1]."</B>");
			print($STR_VIDEO_METHOD2_MSG2);
			print($STR_VIDEO_METHOD2_MSG3);
			?>
			</td>
		</tr>
		<tr> 
       		<td align="right" valign="top" class="FieldCaptionText8ptNormal">Select File Extension:</td>
			<td align="left" valign="top" class="HelpText">
				<select name="cbo_extension">
					<option value="mp4" <?php print(CheckSelected("mp4",$str_ext)); ?>>MP4</option>
					<option value="flv" <?php print(CheckSelected("flv",$str_ext)); ?>>FLV</option>
					<option value="wmv" <?php print(CheckSelected("wmv",$str_ext)); ?>>WMV</option>
					<option value="mpg" <?php print(CheckSelected("mpg",$str_ext)); ?>>MPG</option>
					<option value="mpeg" <?php print(CheckSelected("mpeg",$str_ext)); ?>>MPEG</option>
					<option value="avi" <?php print(CheckSelected("avi",$str_ext)); ?>>AVI</option>
					<option value="mov" <?php print(CheckSelected("mov",$str_ext)); ?>>MOV</option>
					<option value="rm" <?php print(CheckSelected("rm",$str_ext)); ?>>RM</option>
					<option value="ram" <?php print(CheckSelected("rm",$str_ext)); ?>>RAM</option>
					<option value="zip" <?php print(CheckSelected("zip",$str_ext)); ?>>ZIP</option>
					<option value="rar" <?php print(CheckSelected("rar",$str_ext)); ?>>RAR</option>
				</select>
	  		</td>
                </tr>
	  	<tr><td height="10"></td><td ></td></tr>
	  
	  	<?php 
		$str_ptitle="";
		if(isset($_GET["ptitle"]))
		{ 
			$str_ptitle=trim($_GET["ptitle"]);
		}
		$str_purl="";
		if(isset($_GET["purl"]))
		{ 
			$str_purl=trim($_GET["purl"]);
		}?>
  	  <tr>
        <td height="20" align="right" valign="top" class="FieldCaptionText8ptNormal">Photographer Title:</td>
        <td><input type="text" name="txt_ptitle" class="clsTextBox" maxlength="100" size="70" value="<?php print(RemoveQuote($str_ptitle)); ?>">
            <br>
            <span class="HelpText"><?php print($STR_MAX_100_MSG);?></span></td>
	    </tr>
  	  <tr>
        <td height="20" align="right" valign="top" class="FieldCaptionText8ptNormal">Photographer URL:</td>
        <td><input type="text" name="txt_purl" class="clsTextBox" maxlength="1024" size="70" value="<?php print(RemoveQuote($str_purl)); ?>">
            <br>
            <span class="HelpText"><?php print($STR_MAX_1024_MSG."<br>".$STR_URL_FORMAT_MSG);?></span></td>
	    </tr>
  	  <tr>
  	    <td height="15"></td>
  	    <td></td>
	    </tr>	  
		<?php 	  
		$str_hot="NO";
		$str_disnew="YES";
		$str_purchase="YES";
		if (isset($_GET["disnew"]))
		{
			$str_disnew = trim($_GET["disnew"]);
		}
		if (isset($_GET["dishot"]))
		{
			$str_hot = trim($_GET["dishot"]);
		}
		if (isset($_GET["purchase"]))
		{
			$str_purchase = trim($_GET["purchase"]);
		}?>
	  
	  <input type="hidden" name="cbo_purchase" id="cbo_purchase" value="NO">
	  <!--
	  <?php /*
	  <tr> 
       <td align="right" valign="top" class="FieldCaptionText8ptNormal">Allow Purchase:<span class="HelpText">*</span></td>
       <td align="left" valign="top">
	   		<select name="cbo_purchase">
			  <option value="YES" <?php print(CheckSelected("YES",strtoupper($str_purchase)));?>>Yes</option>
   			  <option value="NO" <?php print(CheckSelected("NO",strtoupper($str_purchase)));?>>No</option>
		  	</select><br><span class="HelpText"><?php print($STR_DISPLAY_AS_PURCHASE)?></span></td>
      </tr>
	  
	  */ ?> -->
	
	  <tr> 
       <td align="right" valign="top" class="FieldCaptionText8ptNormal">Display As New:<span class="HelpText"></span></td>
       <td align="left" valign="top">
	   		<select name="cbo_disnew">
			  <option value="YES" <?php print(CheckSelected("YES",strtoupper($str_disnew)));?>>YES</option>
   			  <option value="NO" <?php print(CheckSelected("NO",strtoupper($str_disnew)));?>>NO</option>
		  	</select><br><span class="HelpText"><?php print($STR_DISPLAY_AS_NEW)?></span></td>
      </tr>
	  <tr> 
       <td align="right" valign="top" class="FieldCaptionText8ptNormal">Display As Hot:<span class="HelpText"></span></td>
       <td align="left" valign="top">
		  <select name="cbo_dishot">
			  <option value="YES" <?php print(CheckSelected("YES",strtoupper($str_hot)));?>>YES</option>
			  <option value="NO" <?php print(CheckSelected("NO",strtoupper($str_hot)));?>>NO</option>
		  </select><br><span class="HelpText"><?php print($STR_DISPLAY_AS_HOT);?></span></td>
      </tr>
	  <tr><td height="15"></td><td></td></tr>
      <tr> 
       <td height="26">
         <input type="hidden" name="mid" value="<?php print($int_mid);?>">
	   </td>
       <td align="left">
			<table width="100%"  border="0" cellpadding="0">
              <tr>
                <td width="15%"><input name="btn_list_page" type="submit" id="btn_list_page" title="Click to add Photoset and go to Photoset list page" value=" Add Photoset and go to List " class="ButtonStyle"></td>
                <td width="3%">&nbsp;</td>
                <td width="15%"><input name="btn_add_page" type="submit" id="btn_add_page" title="Click to add Photoset and back to photoset add page" value=" Add Photoset and Back to Add " class="ButtonStyle"></td>
                <td width="5%">&nbsp;</td>
                <td width="56%"><input name="btn_reset" class="ButtonStyle" type="reset" id="btn_reset3" title="Click to reset the form " value="Reset"></td>
                <td width="6%">&nbsp;</td>
              </tr>
            </table></td>			
      </tr>
      <tr> 
       <td height="10"></td>
       <td></td>
      </tr>
     </form>
     </table>
	</td>
   </tr>
  </table>
  <br><a name="help"></a>
  <table width="740" border="0" cellpadding="2" cellspacing="0" align="center">
   <tr class="TableHeader8ptBold"> 
    <td height="20" class="TableHeader">
	 <table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr class="TableHeader10ptBold"> 
       <td width="93%">Help Section</td>
       <td width="7%" align="right"><a href="#ptop" class="NavigationLink" title="Go to top">Top</a></td>
      </tr>
     </table>
	</td>
   </tr>
   <tr valign="top"> 
    <td>
	 <table width="100%" border="0" cellpadding="2" cellspacing="0">
              <tr> 
                <td width="20" valign="baseline" class="HelpText">&#8226;</td>
                <td valign="baseline" class="HelpText"><?php print($STR_MANDATORY); ?></td>
              </tr>
            </table>
	</td>
   </tr>
   <tr><td height="10"></td></tr>
  </table>
  </td>
</tr>
</table>
	<?php
	include "../includes/adminfooter.php";
	CloseConnection();
	?>
</body>
</html>