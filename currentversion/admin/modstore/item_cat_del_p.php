<?php
/*
Module Name:- modstore
File Name  :- item_cat_del_p.php
Create Date:- 04-FEB-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid = 0;
if(isset($_GET["pkid"]) == true)
{
    $int_pkid = trim($_GET["pkid"]);
}	
if($int_pkid <= 0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E&#ptop");
    exit();
}

$str_cat_flag="";
if(isset($_GET["catflag"])==true)
{
    $str_cat_flag=trim($_GET["catflag"]);
}
if($str_cat_flag=="" || ($str_cat_flag!="c" && $str_cat_flag!="s") )
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E&#ptop");
    exit();
}

#----------------------------------------------------------------------------------------------------
if($str_cat_flag=="c")
{
    #delete from t_ppd_cat table
    #check wheather child record exists
    $str_query_select="";
    $str_query_select="SELECT subcatpkid FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE catpkid=".$int_pkid;
    //print $str_query_select;exit;
    $rs_list_check=GetRecordSet($str_query_select);

    if(!$rs_list_check->eof())
    {
        CloseConnection();
        Redirect("item_cat_list.php?msg=F&type=E&#ptop");
        exit();
    }

    #select query to get details from t_ppd_cat table
    $str_query_select = "";
    $str_query_select = "SELECT title FROM ".$STR_DB_TABLE_NAME_CAT." WHERE catpkid=". $int_pkid;
    $rs_list = GetRecordSet($str_query_select);

    if($rs_list->eof())
    {
        CloseConnection();
        Redirect("item_cat_list.php?msg=F&type=E&#ptop");
        exit();
    }

    # Delete Query
    $str_query_delete="";
    $str_query_delete="DELETE FROM ".$STR_DB_TABLE_NAME_CAT." WHERE catpkid=" .$int_pkid;
    //print $str_query_delete; exit;
    ExecuteQuery($str_query_delete);
    #----------------------------------------------------------------------------------------------------
    #Close connection and redirect to item_cat_list.php page	
    CloseConnection();
    Redirect("item_cat_list.php?type=S&msg=D&title=".urlencode(RemoveQuote($rs_list->fields("title")))."&#ptop");
    exit();
    #------------------------------------------------------------------------------------------------------------
}
else if($str_cat_flag=="s");
{
    # For Subcat

    # Select Query
    $str_query_select = "";
    $str_query_select = "SELECT subcattitle FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid=". $int_pkid;
    $rs_list_subcat = GetRecordSet($str_query_select);

    if($rs_list_subcat->eof())
    {
        CloseConnection();
        Redirect("item_cat_list.php?msg=F&type=E&#ptop");
        exit();
    }

    # Delete Query
    $str_query_delete = "";
    $str_query_delete = "DELETE FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid=" .$int_pkid;
    ExecuteQuery($str_query_delete);
    #----------------------------------------------------------------------------------------------------
    #Close connection and redirect to item_cat_list.php page	
    CloseConnection();
    Redirect("item_cat_list.php?type=S&msg=SD&title=".urlencode(RemoveQuote($rs_list_subcat->fields("subcattitle")))."&#ptop");
    exit();
    #------------------------------------------------------------------------------------------------------------
}
?>