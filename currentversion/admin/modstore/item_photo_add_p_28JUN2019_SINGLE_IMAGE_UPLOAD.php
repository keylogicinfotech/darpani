<?php
/*
File Name  :- item_order_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
include "../../includes/lib_file_system.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables

$int_masterpkid = 0;
if(isset($_POST["hdn_masterpkid"]))
{
    $int_masterpkid = trim($_POST["hdn_masterpkid"]);
}	
if($int_masterpkid == "" || $int_masterpkid < 0 || !is_numeric($int_masterpkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}

$str_visible = "";
if(isset($_POST['cbo_visible']))
{
    $str_visible = trim($_POST['cbo_visible']);
}

$str_image = "";
if(isset($_FILES['fileimage']))
{
    $str_image = trim($_FILES['fileimage']['name']);
}

$str_preview = "";
if (isset($_POST["cbo_preview"]) && $_POST["cbo_preview"]!= "")
{
    $str_preview = trim($_POST["cbo_preview"]);
}

$str_image_url = "";
if (isset($_POST["txt_imgurl"]) && $_POST["txt_imgurl"]!= "")
{
    $str_image_url = trim($_POST["txt_imgurl"]);
}

$str_access = "";
if (isset($_POST["cbo_access"]))
{
    $str_access = trim($_POST["cbo_access"]);
}
$str_ptitle = "";
$str_purl = "";
$int_colorpkid = 0;
$str_color = "";
#----------------------------------------------------------------------------------------------------
$str_redirect = "";
$str_redirect.= "&visible=".RemoveQuote(urlencode($str_visible))."&pkid=".urlencode($int_masterpkid);
$str_redirect.= "&preview=".$str_preview."&access=".$str_access."&#ptop";
#----------------------------------------------------------------------------------------------------
# check all validation
if(($str_image == "" && $str_image_url== "") || $str_visible == "")
{
    CloseConnection();
    Redirect("item_photo_list.php?msg=F&type=E".$str_redirect);
    exit();
}
if($str_image != "")
{
    if(ValidateImageExtension($_FILES['fileimage']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
        CloseConnection();
        Redirect("item_photo_list.php?msg=I&type=E".$str_redirect);
        exit();
    }
}

if($str_image_url != "")
{
    if(validateURL02(strtolower($str_image_url))==false )
    {
        CloseConnection();
        Redirect("item_photo_list.php?msg=IU&type=E".$str_redirect);
        exit();
    }
}
#---------------------------------------------------------------------------------------------------- 
$str_dir = $UPLOAD_IMG_PATH.$int_masterpkid;
//print $str_dir; exit;
CreateDirectory($str_dir);

if(!file_exists($str_dir))
{
    //print "Error"; exit;
    CloseConnection();
    Redirect("item_photo_list.php?msg=F&type=E".$str_redirect);
    exit();
}
#upload image
$str_main_file_name="";
$str_large_file_name="";
$str_thumb_file_name="";
$str_main_path="";
$str_large_path="";
$str_thumb_path="";

if($str_image!="")
{	
    $str_main_file_name=GetUniqueFileName()."_main.".getextension($str_image);
    $str_large_file_name=GetUniqueFileName()."_l.".getextension($str_image);
    $str_thumb_file_name=GetUniqueFileName()."_t.".getextension($str_image);

    $str_main_path = trim($str_dir."/".$str_main_file_name);
    $str_large_path = trim($str_dir."/".$str_large_file_name);
    $str_thumb_path = trim($str_dir."/".$str_thumb_file_name);

    UploadFile($_FILES['fileimage']['tmp_name'],$str_main_path);	
    
    CompressImage($str_main_path, $str_main_path, 60);
    /*## START - Code to put watermark on image
        $str_image_to_print_watermark = imagecreatefromstring(file_get_contents($str_main_path));
        $str_image_watermark = imagecreatefromstring(file_get_contents("./../../images/logo_150x29.png"));

        imagecopy($str_image_to_print_watermark, $str_image_watermark, 0, 0, 0, 0, 150 , 29);
        imagejpeg($str_image_to_print_watermark, $str_main_path);
        imagedestroy($str_image_to_print_watermark);
    ## END - Code to put watermark on image

    //ResizeImage($str_main_path,$INT_PHOTOSET_PHOTO_MAIN_WIDTH);
     * 
     */
    SaveAsThumbImage($str_main_path,$str_large_path,$INT_IMG_WIDTH_LARGE);
    SaveAsThumbImage($str_large_path,$str_thumb_path,$INT_IMG_WIDTH_THUMB);
}	
#----------------------------------------------------------------------------------------------------
$str_photo_front = "NO";
$str_query_select = "SELECT pkid FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE setasfront='YES' AND masterpkid=".$int_masterpkid;
$rs_list = GetRecordSet($str_query_select);
if($rs_list->EOF() == true)
{
    $str_photo_front="YES";
}
#----------------------------------------------------------------------------------------------------
#select query to find maximum display order
$int_max = "";
$int_max = GetSubcatMaxValue($STR_DB_TABLE_NAME_PHOTO,"masterpkid",$int_masterpkid,"displayorder");

# Insert Query
$str_query_insert = "";
$str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_PHOTO."(masterpkid,thumbphotofilename,largephotofilename,mainphotofilename, imageurl,photographertitle,photographerurl,colorpkid, color,visible,displayorder,previewtoall,accesstoall,setasfront) VALUES (";
$str_query_insert = $str_query_insert."'".$int_masterpkid."','".ReplaceQuote($str_thumb_file_name)."','".ReplaceQuote($str_large_file_name)."',";		
$str_query_insert = $str_query_insert."'".ReplaceQuote($str_main_file_name)."', '".ReplaceQuote($str_image_url)."', '".ReplaceQuote($str_ptitle)."','".ReplaceQuote($str_purl)."',".$int_colorpkid.", ";
$str_query_insert = $str_query_insert."'".ReplaceQuote($str_color)."','".ReplaceQuote($str_visible)."',".$int_max.",'".ReplaceQuote($str_preview)."','". ReplaceQuote($str_access)."','".ReplaceQuote($str_photo_front)."')";
//print $str_query_insert;exit;
ExecuteQuery($str_query_insert);	

# Update Query
$str_update = "UPDATE ".$STR_DB_TABLE_NAME." SET lastupdatedate='".date("Y-m-d H:i:s")."' WHERE pkid=".$int_masterpkid;
ExecuteQuery($str_update);
#----------------------------------------------------------------------------------------------------
#write to xml file
//WriteXml();
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to listing page	
CloseConnection();
Redirect("item_photo_list.php?type=S&msg=S".$str_redirect."&#ptop");
exit();
#----------------------------------------------------------------------------------------------------
?>
