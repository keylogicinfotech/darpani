function frm_edit_validateform()
{
    with(document.frm_edit)
    {
        /*if(trim(txt_title.value) == "" && trim(ta_desc.value) == "")
        {
            alert("Please enter title or description.");
            ta_desc.select();
            ta_desc.focus();
            txt_title.select();
            txt_title.focus();
            return false;
        }
        if(trim(txt_title.value) == "")
        {
            alert("Please enter title.");
            txt_title.select();
            txt_title.focus();
            return false;
        }*/
        if(trim(ta_desc.value) == "" || trim(ta_desc.value) == "<br>")
        {
            alert("Please enter description.");
            ta_desc.select();
            ta_desc.focus();			
            return false;
        }

        /*if(trim(file_large_image.value) != "")
        {
            if(!checkExt(trim(file_large_image.value)))
            {
                file_large_image.select();
                file_large_image.focus();
                return false;
            }
        }
        */
        if(txt_url.value!="")
        {
            if(!isValidUrl(txt_url.value))	
            {
                alert("Please enter valid URL.");
                txt_url.select();
                txt_url.focus();
                return false;					
            }
        }

        /*if(trim(txt_url.value)!="")
        {
            if(!isValidUrl(trim(txt_url.value)))
            {
                txt_url.select();
                txt_url.focus();
                return false;			
            }
        }*/

    }
    return true;
}


function Confirm_Deletion()
{
    if(confirm("Are you sure you want to delete image?"))
    {
        if(confirm("Confirm Deletion: Click 'OK' to delete image or click 'Cancel' to cancel deletion."))
        {
            return true;
        }
    }
    return false;
}