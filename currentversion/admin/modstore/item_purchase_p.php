<?php
/*
    Module Name:- modstore
    File Name  :- item_purchase_p.php
    Create Date:- 18-MAR-2019
    Intially Create By :- 015
    Update History:-
*/
#-------------------------------------------------------------------------=-------------------------------
    include "../../includes/validatesession.php";
    include "../../includes/configuration.php";
    include "../../includes/lib_data_access.php";
    include "./item_config.php";
    include "../../includes/lib_common.php";
    include "./../item_app_specific.php";
    include "../../includes/lib_xml.php";
#---------------------------------------------------------------------------------------------------------
    #Get values of all passed GET / POST variables
    $int_cat_pkid = "";
    if(isset($_GET["catid"]) && trim($_GET["catid"]) != "" )
    {   
        $int_cat_pkid = trim($_GET["catid"]);    
    }

    $str_name_key = "";
    if(isset($_GET["key"]) && trim($_GET["key"]) != "" )
    { $str_name_key = trim($_GET["key"]); }

    # get data for paging
    $int_page="";
    if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
    { $int_page = $_GET["PagePosition"]; }
    else
    { $int_page = 1; }

    $str_filter = "";
    $str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";
    
    $int_pkid = 0;
    $str_mode = "";
    
    if(isset($_GET["pkid"]))
    {
        $int_pkid = trim($_GET["pkid"]);
    }
    if(!is_numeric($int_pkid) || $int_pkid == "" || $int_pkid < 0 )
    {
        CloseConnection();
	Redirect("item_list.php?msg=F&type=E".$str_filter);
	exit();
    }
#---------------------------------------------------------------------------------------------------------
# Getting skill title to display message on cont_sub_list.php
    $str_select_query = "";
    $str_select_query = "SELECT allowpurchase FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
    $rs_list = GetRecordSet($str_select_query);
    if($rs_list->EOF()==true)
    {
        CloseConnection();
        Redirect("item_list.php?msg=F&type=E".$str_filter);
        exit();
    }	

    $str_mode = "";
    $str_mode = $rs_list->Fields("allowpurchase");    
#---------------------------------------------------------------------------------------------------------
#   Set the value
    $str_set_text = "";
    if(strtoupper($str_mode)=="YES")
    {
        $str_set_text = "NO";
    }
    else
    {
        $str_set_text = "YES";
    }
#---------------------------------------------------------------------------------------------------------
# Update Query.
    $str_select_update = "";
    $str_select_update = "UPDATE ".$STR_DB_TABLE_NAME." SET allowpurchase='" .ReplaceQuote($str_set_text). "' WHERE pkid=".$int_pkid;
    ExecuteQuery($str_select_update);
#-----------------------------------------------------------------------------------------------------------
    # write to xml file
    //WriteXML($int_subcatpkid);
#---------------------------------------------------------------------------------------------------------
#	Close connection and redirect to skill_list.php
    CloseConnection();
    Redirect("item_list.php?msg=AP&type=S".$str_filter);
    exit();
?>