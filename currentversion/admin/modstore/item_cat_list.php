<?php
/*
Module Name:- modstore
File Name  :- item_cat_list.php
Create Date:- 04-FEB-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT a.catpkid,a.title cattitle,a.visible catvisible,a.displayorder catorder,";
$str_query_select .= "b.subcatpkid,b.subcattitle subtitle,b.visible subvisible,b.displayorder suborder";
$str_query_select .= " FROM " . $STR_DB_TABLE_NAME_CAT . " a";
$str_query_select .= " LEFT JOIN " . $STR_DB_TABLE_NAME_SUBCAT . " b";
$str_query_select .= " ON b.catpkid=a.catpkid";
$str_query_select .= " ORDER BY a.displayorder DESC,a.title,a.catpkid,";
$str_query_select .= "b.displayorder DESC,b.subcattitle,b.subcatpkid";

$rs_list = GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------------
# get Query String Data
$str_title = "";
$str_mode = "";
$str_cattitle = "";
$str_cattype = "";
$str_visible = "YES";

if (isset($_GET["title"])) {
    $str_title = trim($_GET["title"]);
}
if (isset($_GET["mode"])) {
    $str_mode = trim($_GET["mode"]);
}
if (isset($_GET["visible"])) {
    $str_visible = trim($_GET["visible"]);
}
if (isset($_GET["cattitle"])) {
    $str_cattitle = trim($_GET["cattitle"]);
}
if (isset($_GET["cattype"])) {
    $str_cattype = trim($_GET["cattype"]);
}
#------------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if (isset($_GET["type"])) {
    switch (trim($_GET["type"])) {
        case ("S"):
            $str_type = "S";
            break;
        case ("E"):
            $str_type = "E";
            break;
        case ("W"):
            $str_type = "W";
            break;
    }
}
#Get message text.
if (isset($_GET["msg"])) {
    switch (trim($_GET["msg"])) {
        case ("F"):
            $str_message = $STR_MSG_ACTION_INFO_MISSING;
            break;
        case ("S"):
            $str_message = $STR_MSG_ACTION_ADD;
            break;
        case ("SS"):
            $str_message = $STR_MSG_ACTION_ADD;
            break;
        case ("D"):
            $str_message = $STR_MSG_ACTION_DELETE;
            break;
        case ("SD"):
            $str_message = $STR_MSG_ACTION_DELETE;
            break;
        case ("U"):
            $str_message = $STR_MSG_ACTION_EDIT;
            break;
        case ("SU"):
            $str_message = $STR_MSG_ACTION_EDIT;
            break;
        case ("O"):
            $str_message = $STR_MSG_ACTION_DISPLAY_ORDER;
            break;
        case ("V"):
            $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode . ".";
            break;
        case ("SV"):
            $str_message = " '" . MyHtmlEncode(MakeStringShort(RemoveQuote($str_title), 30)) . "' changed to '" . $str_mode . "' successfully.";
            break;
        case ("DU"):
            $str_message = $STR_MSG_ACTION_TITLE_ALREADY_EXIST_CAT;
            break;
        case ("SDU"):
            $str_message = $STR_MSG_ACTION_TITLE_ALREADY_EXIST_SUBCAT;
            break;
    }
}
?>
<!DOCTYPE html>
<html>

<head>
    <title><?php print($STR_SITE_TITLE); ?> : <?php print($STR_TITLE_PAGE_CAT); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../../css/admin.css" rel="stylesheet">
</head>

<body> <a name="ptop" id="ptop"></a>
    <div class="container">
        <?php include($STR_ADMIN_HEADER_PATH); ?>
        <div class="row padding-10">
            <div class="col-md-6 col-sm-6 col-xs-12 button_space">
                <div class="btn-group" role="group" aria-label="...">
                    <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO); ?> <?php print($STR_TITLE_PAGE); ?>"><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12" align="right">
                <h3><?php print($STR_TITLE_PAGE_CAT); ?> </h3>
            </div>
        </div>
        <hr>
        <?php if ($str_type != "" && $str_message != "") {
            print(DisplayMessage(0, $str_message, $str_type));
        } ?>
        <div class="row padding-10">
            <div class="col-md-12">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <span>
                                    <a class="accordion-toggle collapsed " data-toggle="collapse" data-parent="#accordion" title="<?php print($STR_TITLE_ADD); ?>" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>
                                </span>
                                <?php print($STR_LINK_HELP); ?>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                <form name="frm_add" action="item_cat_add_p.php" method="POST" onSubmit="return frm_add_validate();">
                                    <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Title</label><span class="text-help-form"> *</span>
                                                <input type="text" id="txt_title" name="txt_title" value="<?php print(MyHtmlEncode($str_cattitle)); ?>" maxlength="255" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>">
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?php $str_visible = "YES";
                                                if (isset($_GET["vis"]) && trim($_GET["vis"]) != "") {
                                                    $str_visible = trim($_GET["vis"]);
                                                } ?>
                                                <label>Visible</label>&nbsp;<span class="text-help-form">* (<?php print($STR_MSG_VISIBLE) ?>)</span><br />
                                                <select name="cbo_visible" class="form-control input-sm">
                                                    <option value="YES" <?php print(CheckSelected($str_visible, "YES")); ?>>YES</option>
                                                    <option value="NO" <?php print(CheckSelected($str_visible, "NO")); ?>>NO</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <?php $str_cbo_type = "Main Category"; ?>
                                            <?php if ($rs_list->count() > 0) {
                                                $rs_list->movefirst();
                                            }
                                            $str_temp = ""; ?>
                                            <div class="form-group">
                                                <label>Category Type</label><span class="text-help-form"> *</span>
                                                <select name="cbo_type" class="form-control input-sm">
                                                    <?php /*?><option value=""><-- Select Category --></option><?php */ ?>
                                                    <option value="0" <?php print(CheckSelected("0", strtoupper($str_cattype))); ?>>Create New Category</option>
                                                    <option value="">------------ OR select any below category and create sub category under it ------------</option>
                                                    <?php
                                                    while ($rs_list->eof() == false) {
                                                        if ($str_temp != $rs_list->fields("catpkid")) {
                                                            $str_temp = $rs_list->fields("catpkid"); ?>

                                                            <option value="<?php print($rs_list->fields("catpkid")); ?>" <?php print(CheckSelected($rs_list->fields("catpkid"), $str_cattype)); ?>><?php print(MakeStringShort(MyHtmlEncode($rs_list->fields("cattitle")), "90"));
                                                                                                                                                                                                    if (strtoupper($rs_list->fields("catvisible") == "NO")) {
                                                                                                                                                                                                        print(" (Invisible)");
                                                                                                                                                                                                    } ?></option><?php }
                                                                                                                                                $rs_list->movenext();
                                                                                                                                            } ?>
                                                </select>
                                            </div>
                                            <?php if ($rs_list->count() > 0) {
                                                $rs_list->movefirst();
                                            }
                                            $str_temp = ""; ?>
                                        </div>
                                    </div>
                                    <?php print DisplayFormButton("ADD", 0); ?><?php print DisplayFormButton("RESET", 0); ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <form name="frm_list" action="item_cat_order_p.php" method="POST" onSubmit="return frm_list_check_displayorder();">
                <table class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                            <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                            <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                            <th width="7%">Manage Tailoring Options</th>
                            <th width="7%"><?php print $STR_TABLE_COLUMN_NAME_TOTAL_ITEMS; ?></th>
                            <th width="6%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_ORDER; ?></th>
                            <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($rs_list->EOF() == true) {  ?>
                            <tr>
                                <td colspan="8" align="center" class="alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td>
                            </tr>
                        <?php } else { ?>
                            <?php
                            $int_mod_cnt = 0;
                            $int_sub_cnt = 0;
                            $pkid = 0;
                            $cnt = "";
                            $str_title = "";
                            $int_order = "";
                            $catpkid = "";
                            $int_flag = 0;
                            $int_delete_flag = "";
                            $str_visible = "";
                            $int_cnt = 1;
                            $str_cat_flag = "";
                            $str_query_select = "";
                            $str_class_collapse = "";
                            $int_total_subcat = 0; ?>
                            <?php
                            while ($rs_list->eof() == false) {
                                if ($pkid != $rs_list->fields("catpkid") && $int_flag == 0) {
                                    $int_sub_cnt = 0;
                                    $int_flag = 1;
                                    $int_delete_flag = 0;
                                    $pkid = $rs_list->fields("catpkid");
                                    $int_mod_cnt = $int_mod_cnt + 1;
                                    $cnt = "<strong>" . $int_mod_cnt . "</strong>";
                                    //$cnt=""; 
                                    $str_cat_flag = "c";
                                    $str_class_collapse = "collapse";
                                    $str_title = "<b><span>" . MyHtmlEncode($rs_list->fields("cattitle")) . "</span></b>";
                                    $int_order = $rs_list->fields("catorder");
                                    $int_modpkid = $rs_list->fields("catpkid");
                                    $int_modparentpkid = $rs_list->fields("catpkid");
                                    $str_disp_class = "form-control input-sm text-align";
                                    $str_highlightclass = "class='alert-warning'";
                                    $str_visible = $rs_list->fields("catvisible");
                                    if (trim($rs_list->fields("subtitle")) == "") {
                                        $int_delete_flag = 1;
                                    }
                                    $str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME . " WHERE catpkid=" . $int_modpkid;

                                    $str_query_select_subcat = "";
                                    $str_query_select_subcat = "SELECT * FROM " . $STR_DB_TABLE_NAME_SUBCAT . " WHERE catpkid=" . $int_modpkid;
                                    $rs_list_subcat = GetRecordSet($str_query_select_subcat);
                                    $int_total_subcat = $rs_list_subcat->Count();
                                    //print $rs_list_subcat->Count();
                                } else {
                                    $int_flag = 0;
                                    $int_delete_flag = "1";
                                    $int_sub_cnt = $int_sub_cnt + 1;
                                    $cnt = $int_sub_cnt;
                                    $str_cat_flag = "s";
                                    $str_visible = $rs_list->fields("subvisible");
                                    $str_title = ReplaceRecordWithCharacter($rs_list->fields("subtitle"), "" . MyHtmlEncode($rs_list->fields("subtitle")) . "");
                                    $int_order = $rs_list->fields("suborder");
                                    $int_modpkid = $rs_list->fields("subcatpkid");
                                    $int_modparentpkid = $rs_list->fields("catpkid");
                                    $str_disp_class = "form-control input-sm text-align";
                                    $str_highlightclass = "";
                                    $str_class_collapse = "class='panel-collapse collapse' id='" . $rs_list->fields("catpkid") . "' ";
                                    $str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME . " WHERE subcatpkid=" . $int_modpkid;
                                } ?>
                                <?php if (trim($str_title) != "") { ?>
                                    <tr <?php //print $str_class_collapse
                                        ?>>
                                        <td align="center" <?php print($str_highlightclass); ?>>
                                            <?php print($cnt); ?><?php //print($cattype);
                                                                ?>
                                            <input type="hidden" name="hdn_pkid<?php print($int_cnt); ?>" value="<?php print($int_modpkid); ?>">
                                            <input type="hidden" name="hdn_cattype<?php print($int_cnt); ?>" value="<?php print($str_cat_flag); ?>">
                                        </td>
                                        <td valign="middle" <?php print($str_highlightclass); ?>>
                                            <?php print($str_title); ?>
                                        </td>
                                        <td align="center" <?php print($str_highlightclass); ?>>
                                            <?php if ($int_flag == 1) { ?>
                                                <h4><a href="item_cat_tailoringoption_list.php?catid=<?php print $int_modpkid; ?>"><i class="fa fa-bars"></i></a></h4>
                                            <?php } ?>
                                        </td>
                                        <td align="center" <?php print($str_highlightclass); ?>>
                                            <label>
                                                <h4 class="nopadding"><a href="item_list.php?catid=<?php print $int_modpkid; ?>"><i class="fa fa-shopping-cart text-muted align-bottom" aria-hidden="true"></i></a></h4>
                                            </label>
                                            <span class="text-help">(<?php $rs_list_total = GetRecordSet($str_query_select);
                                                                        $int_total_items = $rs_list_total->count();
                                                                        print $int_total_items; ?>)</span>
                                        </td>
                                        <td align="center" <?php print($str_highlightclass); ?>>
                                            <input type="text" name="txt_displayorder<?php print($int_cnt); ?>" maxlength="5" value="<?php print($int_order); ?>" class="form-control input-sm text-center" />
                                        </td>
                                        <?php
                                        $str_image = "";
                                        if (strtoupper($str_visible) == "YES") {
                                            $str_image = $STR_LINK_ICON_PATH_VISIBLE;
                                            $str_class = "btn btn-warning btn-xs";
                                            $str_title = $STR_HOVER_VISIBLE;
                                        } else {
                                            $str_image = $STR_LINK_ICON_PATH_INVISIBLE;
                                            $str_class = "btn btn-default active btn-xs";
                                            $str_title = $STR_HOVER_INVISIBLE;
                                        } ?>
                                        <td align="center" <?php print($str_highlightclass); ?>>
                                            <a class="<?php print($str_class); ?>" href="item_cat_visible_p.php?pkid=<?php print($int_modpkid); ?>&catflag=<?php print($str_cat_flag); ?>" title="<?php print($STR_HOVER_VISIBLE); ?>"><?php print($str_image); ?></a>
                                            <a class="btn btn-success btn-xs" href="item_cat_edit.php?pkid=<?php print($int_modpkid); ?>&catflag=<?php print($str_cat_flag); ?>" title="<?php print($STR_HOVER_EDIT); ?>"><?php print $STR_LINK_ICON_PATH_EDIT; ?></a>
                                            <?php if ($str_cat_flag == "s") { ?>
                                                <?php if ($int_total_items <= 0) { ?>
                                                    <a class="btn btn-danger btn-xs" href="./item_cat_del_p.php?pkid=<?php print($int_modpkid); ?>&catflag=<?php print($str_cat_flag); ?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE); ?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <?php if ($int_total_subcat <= 0) { ?>
                                                    <a class="btn btn-danger btn-xs" href="./item_cat_del_p.php?pkid=<?php print($int_modpkid); ?>&catflag=<?php print($str_cat_flag); ?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE); ?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>
                                                <?php } ?>
                                            <?php } ?>
                                        </td>
                                    </tr>
                            <?php $int_cnt++;
                                }
                                if ($int_flag != 1) {
                                    $rs_list->MoveNext();
                                }
                            } ?>
                            <tr>
                                <td><input type="hidden" name="hdn_counter" value="<?php print($int_cnt); ?>"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td align="center" class="text-align"><?php print DisplayFormButton("SAVE", 0); ?></td>
                                <td></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </form>
        </div>
        <?php include "../../includes/help_for_list.php"; ?>
    </div>

    <?php include "../../includes/include_files_admin.php"; ?>
    <?php include($STR_ADMIN_FOOTER_PATH); ?>
    <script language="JavaScript" src="item_cat_list.js" type="text/javascript"></script>
</body>

</html>