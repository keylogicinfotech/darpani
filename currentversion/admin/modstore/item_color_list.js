// ADD Form Validation
function frm_add_validate()
{
    with(document.frm_add)
    {
        if(isEmpty(trim(txt_title.value)))
	{
            alert("Please enter title.");
            txt_title.select();
            txt_title.focus();
            return false;
	}
        if(isEmpty(trim(txt_cmykcode.value)))
	{
            alert("Please enter hex color code.");
            txt_cmykcode.select();
            txt_cmykcode.focus();
            return false;
	}
        /* if(isEmpty(trim(txt_rgbcode.value)))
        {
                alert("Please enter rgb code.");
                txt_rgbcode.select();
                txt_rgbcode.focus();
                return false;
        }*/
    }
    return true;
}

// Confirmation for DELETE
function confirm_delete()
{
    if(confirm("Are you sure you want to delete this details?"))
    {
        if(confirm("Confirm Deletion:Click 'Ok' to delete this details or 'Cancel' to deletion."))
        {
            return true;
        }
    }
    return false;
}
