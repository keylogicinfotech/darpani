<?php 
#Used in module -> modPhotogallery, modJournal
#----------------------------------------------------------------------------------------------------------------------------------------------------------
/*''Function Name :- DisplayMemberAccess
''	Prototype :-  DisplayMemberAccess($strPreview,$strAccess)
''	Input Parameter :- 2 Parameters Required.
					   $strPreview -> value for preview field.
					   $strAccess -> value for access field.
''	Return Value :- string message
''	Purpose :- This function creates message according to passed values.
*/

function DisplayMemberAccess($strPreview,$strAccess)
{
	$msg="";
	if(strtoupper(trim($strPreview))=="YES" && strtoupper(trim($strAccess))=="YES")
	{
		$msg="Preveiw: ALL, Access: ALL";
	}
	else if(strtoupper(trim($strPreview))=="NO" && strtoupper(trim($strAccess))=="NO")
	{
		$msg="Preveiw: MEMBER, Access: MEMBER";
	}
	else if(strtoupper(trim($strPreview))=="YES" && strtoupper(trim($strAccess))=="NO")
	{
		$msg="Preveiw: ALL, Access: MEMBER";
	}
	else if(strtoupper(trim($strPreview))=="NO" && strtoupper(trim($strAccess))=="YES")
	{
		$msg="Preveiw: MEMBER, Access: MEMBER";	
	}
	return $msg;
}
#----------------------------------------------------------------------------------------------------------------------------------------------------------

#Writing data of table into XML file 
function WriteXML($int_subcatpkid=0)
{
	global $XML_FILE_PATH;
	
	if($int_subcatpkid=="" || $int_subcatpkid==NULL)
	{
		$int_subcatpkid=0;
	}
	
        
       
 	#creating recordset.	
	$str_query_select = "";
	$str_query_select = "SELECT a.*,b.pkid,b.previewtoall,b.accesstoall,b.setasfront,b.thumbphotofilename,b.largephotofilename,b.photographerurl phurl,b.photographertitle phtitle,c.pkid,c.title,c.subscriptiontypeid,c.description,d.subcattitle ";
        
	$str_query_select .= " FROM t_store a LEFT JOIN tr_ppd_photo b ON a.pkid=b.pkid AND b.visible='YES' ";
	$str_query_select .= " LEFT JOIN t_store_ccbill_price c ON c.pkid=a.pricepkid AND c.visible='YES' ";
	$str_query_select .= " LEFT JOIN t_store_subcat d ON d.subcatpkid=a.subcatpkid AND d.visible='YES' ";
	$str_query_select .= " WHERE a.visible='YES' AND a.subcatpkid=".$int_subcatpkid;
	$str_query_select .= " ORDER BY a.setasdefault desc, a.displayorder,a.title,b.setasfront desc,b.displayorder";
	$rs_xml_list = GetRecordSet($str_query_select);
        
       // print $str_query_select;
	
	if($rs_xml_list->eof()==true)
	{
		if (file_exists($XML_FILE_PATH."item_".$int_subcatpkid.".xml"))
		{
			unlink($XML_FILE_PATH."item_".$int_subcatpkid.".xml");
		}
		return;
	}

	#storing recordset data into array.
	if(!$rs_xml_list->eof())
	{
		$arr=array();
		$arr1=array();
		$i=0;
		$int_id="";
		$str_filename="item_".$int_subcatpkid.".xml";
		$str_item_xml_tag="MODEL_STORE";
		while(!$rs_xml_list->eof())
		{			
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="PKID";
			$arr4[$i]=$rs_xml_list->fields("pkid");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="SUBCATEGORYNAME";
			$arr4[$i]=$rs_xml_list->fields("subcattitle");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="TITLE";
			$arr4[$i]=$rs_xml_list->fields("title");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="DESC";
			$arr4[$i]=$rs_xml_list->fields("description");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="PURCHASE";
			$arr4[$i]=$rs_xml_list->fields("allowpurchase");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="FILENAME";
			$arr4[$i]=$rs_xml_list->fields("filename");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="FILEEXTENSION";
			$arr4[$i]=$rs_xml_list->fields("fileextension");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="SETASDEFAULT";
			$arr4[$i]=$rs_xml_list->fields("setasdefault");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="DISPLAYNEW";
			$arr4[$i]=$rs_xml_list->fields("displayasnew");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="DISPLAYHOT";
			$arr4[$i]=$rs_xml_list->fields("displayashot");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="ALLOWFREE";
			$arr4[$i]=$rs_xml_list->fields("allowfree");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="NOOFPHOTO";
			$arr4[$i]=$rs_xml_list->fields("noofphoto");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="UPDATEDATE";
			$arr4[$i]=$rs_xml_list->fields("lastupdatedate");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="PRICEPKID";
			$arr4[$i]=$rs_xml_list->fields("pricepkid");  
			$i=$i+1;
			
//			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
//			$arr2[$i]="";
//			$arr3[$i]="USERPRICE";
//			$arr4[$i]=$rs_xml_list->fields("userpricetitle");  
//			$i=$i+1;
                        
                        $arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="USERPRICE";
			$arr4[$i]=$rs_xml_list->fields("title");  
			$i=$i+1;
			
//			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
//			$arr2[$i]="";
//			$arr3[$i]="CCBILLPRICE";
//			$arr4[$i]=$rs_xml_list->fields("ccbillpricetitle");  
//			$i=$i+1;
                        
                        $arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="CCBILLPRICE";
			$arr4[$i]=$rs_xml_list->fields("description");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="SUBID";
			$arr4[$i]=$rs_xml_list->fields("subscriptiontypeid");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="PHGRTITLE";
			$arr4[$i]=$rs_xml_list->fields("photographertitle");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_xml_list->fields("pkid");
			$arr2[$i]="";
			$arr3[$i]="PHGRURL";
			$arr4[$i]=$rs_xml_list->fields("photographerurl");  
			$i=$i+1;
			
			$tagvalue="";
			$int_new_id = $rs_xml_list->fields("pkid");
			$int_old_id = $rs_xml_list->fields("pkid");
			while($int_new_id == $int_old_id && $rs_xml_list->eof()==false)
			{
				if($rs_xml_list->fields("pkid")!="" && $rs_xml_list->fields("pkid")!=NULL)
				{
					#######GET MAKE A STRING FOR A PHOTOSET SAMPLE PHOTO DETAILS ########
										
					$tagvalue .= $rs_xml_list->fields("pkid");
					$tagvalue .= " || ".$rs_xml_list->fields("previewtoall");
					$tagvalue .= " || ".$rs_xml_list->fields("accesstoall");
					$tagvalue .= " || ".$rs_xml_list->fields("setasfront");
					$tagvalue .= " || ".$rs_xml_list->fields("thumbphotofilename");
					$tagvalue .= " || ".$rs_xml_list->fields("largephotofilename");					
					$tagvalue .= " || ".$rs_xml_list->fields("phurl");
					$tagvalue .= " || ".$rs_xml_list->fields("phtitle");
					$tagvalue .= " ||| ";	/// Separeate form each photo recored with 3 poipe separator
				}				
				$rs_xml_list->MoveNext();
				$int_new_id = $rs_xml_list->fields("pkid");
			}
			
			$arr1[$i]="P_".$int_old_id;
			$arr2[$i]="";
			$arr3[$i]="PHOTOS";
			$arr4[$i]=$tagvalue;  
			$i=$i+1;			
		}

		#writting data to the file		
//		writeXmlFile($XML_FILE_PATH.$str_filename,$str_photoset_xml_tag,$arr1,$arr2,$arr3,$arr4);
                writeXmlFile($XML_FILE_PATH.$str_filename,$str_item_xml_tag,$arr1,$arr2,$arr3,$arr4);
	}
	return;
}
## To draw vote chart 
function GetComparisonChartView($width,$color,$totalcount)
{
    $str="<table width='$width' border='0' cellspacing='0' cellpadding='0'>";
    $str=$str."<tr>";
    $str=$str."<td valign='baseline' width='".$width."' height='5' style='color: #FFF' bgcolor='". $color ."' class='progress-bar-success progress-bar-striped' align='center'><b>".$totalcount."</b></td>";
    $str=$str."</tr>";
    $str=$str."</table>";
    return $str;        
}
#----------------------------------------------------------------------------------------------------------------------
?>