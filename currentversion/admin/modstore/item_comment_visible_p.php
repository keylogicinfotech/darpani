<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#get Query String Data
$int_cat_pkid=0;
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ $int_cat_pkid=trim($_GET["catid"]); }

$str_mname="";  
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_mname=trim($_GET["key"]); }

# get data for paging
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page=1; }

// $str_mname = "";

$str_filter=""; 
$str_filter="&catid=".$int_cat_pkid."&key=".$str_mname."&PagePosition=".$int_page;        
//PRINT $str_filter; EXIT;
$int_masterpkid=0;
if(isset($_GET["masterpkid"]))
{ $int_masterpkid=trim($_GET["masterpkid"]); }

$str_modelname="";
if(isset($_GET["mname"])) { $str_modelname=trim($_GET["mname"]); }

$int_mempkid=0;
if(isset($_POST["memid"]))
{ $int_mempkid=trim($_POST["memid"]); }

$str_membername="";
if(isset($_POST["memname"])) { $str_membername = trim($_POST["memname"]); }

$int_pkid=""; 
if(isset($_GET["pkid"])==true)
{   
    $int_pkid=trim($_GET["pkid"]);
}	
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_comment_list.php?msg=F&type=E&pkid=".$int_masterpkid."&masterpkid=".$int_masterpkid."&mname=".$str_modelname.$str_filter."&#ptop");
    exit();
}
$int_masterpkid="";
	if(isset($_GET["masterpkid"]))
	{ $int_masterpkid=trim($_GET["masterpkid"]); }
#----------------------------------------------------------------------------------------------------
#select query to get about details from t_biography table

$str_query_select="SELECT visible, masterpkid FROM tr_blog WHERE pkid=". $int_pkid;
//print $str_query_select;exit;
$rs_visible=GetRecordSet($str_query_select);

if($rs_visible->eof()==true) 
{
	CloseConnection();
	Redirect("item_comment_list.php?msg=F&type=E&pkid=".$int_masterpkid."&masterpkid=".$int_masterpkid."&mname=".$str_modelname.$str_filter."&#ptop");
	exit();
}
$str_mode="";
$str_title="";
$str_mode=$rs_visible->fields("visible");
//$str_title=trim($rs_visible->fields("description"));
#----------------------------------------------------------------------------------------------------
#change visible mode
$str_mode_title="";
if(strtoupper($str_mode)=="YES")
{
    $str_mode="NO";
    $str_mode_title="Invisible";
}
else if(strtoupper($str_mode)=="NO")
{
    $str_mode="YES";
    $str_mode_title="Visible";
}
#----------------------------------------------------------------------------------------------------
#update status in t_biography table
$str_query_update="UPDATE tr_blog SET visible='" . ReplaceQuote($str_mode) . "' WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file
//WriteXml();
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to service_list.php page	
CloseConnection();
//Redirect("blog_comment_list.php?pkid=".$rs_visible->Fields("masterpkid")."&type=S&msg=V"."&mode=". urlencode(RemoveQuote($str_mode_title)) ."&masterpkid=".$int_masterpkid."&mname=".$str_modelname.$str_filter."&#ptop");
Redirect("item_comment_list.php?type=S&msg=V&pkid=".$int_masterpkid."&mode=". urlencode(RemoveQuote($str_mode_title))."&pkid=".$rs_visible->Fields("masterpkid")."&masterpkid=".$int_masterpkid."&mname=".$str_modelname."&memid=".$int_mempkid."&memname=".$str_membername.$str_filter."&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------
?>