<?php
/*
Module Name:- modstore
File Name  :- color_del_p.php
Create Date:- 21-JAN-2019
Intially Create By :- 0023
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid="";
if(isset($_GET["pkid"]))
{
    $int_pkid = trim($_GET["pkid"]);
}	
if($int_pkid <= 0 || $int_pkid == "" || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_color_list.php?msg=F&type=E&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Delete Query
$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TABLE_NAME_COLOR." WHERE pkid=" .$int_pkid;
ExecuteQuery($str_query_delete);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_color_list.php?type=S&msg=D&#ptop");
exit();
#----------------------------------------------------------------------------------------------------
?>