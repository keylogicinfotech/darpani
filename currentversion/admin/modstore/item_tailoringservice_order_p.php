<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
//include "item_app_specific.php";
//print_r($_POST);exit;
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables

$str_redirect_page_flag = "";
if(isset($_POST["flg"]) && trim($_POST["flg"])!="" )
{ $str_redirect_page_flag=trim($_POST["flg"]); }

$int_cat_pkid = 0;
if(isset($_POST["catid"]) && trim($_POST["catid"])!="" )
{ $int_cat_pkid=trim($_POST["catid"]); }

$str_name_key = "";
if(isset($_POST["key"]) && trim($_POST["key"])!="" )
{ $str_name_key=trim($_POST["key"]); }

# POST data for paging
$int_page = 0;
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"])!="" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"])>0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page=1; }

/*$int_userpkid = 0;
if(isset($_POST['hdn_userpkid']))
{
    $int_userpkid = $_POST['hdn_userpkid'];
}*/

$int_masterpkid = 0;
if(isset($_POST['hdn_masterpkid']))
{
    $int_masterpkid = $_POST['hdn_masterpkid'];
}
//print $int_masterpkid; exit;
if($int_masterpkid <= 0 || !is_numeric($int_masterpkid) || $int_masterpkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F");
    exit();
}

$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&pkid=".$int_masterpkid;
//print $int_masterpkid;
//exit;

$int_cnt = "";
if (isset($_POST["hdn_counter"]))
{
    $int_cnt=trim($_POST["hdn_counter"]);
}	
if($int_cnt=="" || $int_cnt<0 || !is_numeric($int_cnt))
{
    CloseConnection();
    Redirect("item_tailoringservice_list.php?msg=F".$str_filter."&type=E&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Update display order field in table
for($i=1;$i<$int_cnt;$i++)
{
        if(trim($_POST["txt_displayorder".$i]) != "" && trim($_POST["txt_displayorder".$i]) >=0 && is_numeric(trim($_POST["txt_displayorder".$i]))==true  && trim($_POST["hdn_pkid".$i])!="" && trim($_POST["hdn_pkid".$i])>0 && is_numeric(trim($_POST["hdn_pkid".$i]))==true)
        {
            $str_query_update="UPDATE ".$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE." SET displayorder='".trim($_POST["txt_displayorder".$i])."' WHERE pkid=".trim($_POST["hdn_pkid".$i])." AND itempkid=".$int_masterpkid;
            //print $str_query_update."<br/>";
            ExecuteQuery($str_query_update);
        }
}
//exit;


#-----------------------------------------------------------------------------------------------------
# Write to xml file
//WriteXml();
#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_tailoringservice_list.php?type=S&msg=O".$str_filter);
exit();
#----------------------------------------------------------------------------------------------------- ?>
