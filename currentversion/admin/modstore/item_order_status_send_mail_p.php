<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/

//
#----------------------------------------------------------------------------------------------------
//session_start();
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_xml.php";
include "../../includes/lib_email.php";
include "item_config.php";
//include "item_app_specific.php";
//print "HI"; exit;
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_POST);exit;
$int_pkid = 0;
if (isset($_POST['hdn_pkid'])) {
    $int_pkid = $_POST['hdn_pkid'];
}
//print $int_pkid; exit;
if ($int_pkid <= 0 || !is_numeric($int_pkid) || $int_pkid == "") {
    CloseConnection();
    Redirect("item_order_status_list.php?type=E&msg=F");
    exit();
}

$int_masterpkid = 0;
if (isset($_POST['hdn_masterpkid'])) {
    $int_masterpkid = $_POST['hdn_masterpkid'];
}
//print $int_masterpkid; exit;
if ($int_pkid <= 0 || !is_numeric($int_pkid) || $int_pkid == "") {
    CloseConnection();
    Redirect("item_order_status_list.php?type=E&msg=F");
    exit();
}

$int_quantity = 0;
if (isset($_POST['hdn_quantity'])) {
    $int_quantity = $_POST['hdn_quantity'];
}


$str_email = "";
if (isset($_POST['txt_email'])) {
    $str_email = $_POST['txt_email'];
}

if ($str_email == "") {
    CloseConnection();
    Redirect("item_order_status_list.php?type=E&msg=F&#ptop");
    exit();
}


if (validateEmail($str_email) == false) {
    CloseConnection();
    Redirect("./item_order_status_list.php?msg=IE&type=E&#ptop");
    exit();
}

$str_from = "";

//print $STR_XML_FILE_PATH_MODULE; exit;
$fp_keyword = openXMLfile("../" . $STR_XML_FILE_PATH_MODULE . "siteconfiguration.xml");

$str_from = getTagValue("P_SITE_EMAIL_ADDRESS", $fp_keyword);
//print $str_from; exit;







$str_query_select = "";
$str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME . " WHERE pkid = " . $int_masterpkid;
$rs_list = GetRecordSet($str_query_select);

$str_file_path = "";
$str_file_path = $UPLOAD_IMG_PATH . $int_masterpkid . "/" . $rs_list->Fields("imagefilename2");

//print $str_file_path;exit;

$str_to = "";
$str_to = $str_email;

$str_subject = "";
$str_subject = "Send mail for printout";

$str_mailbody = "";
$str_mailbody = "Brag Details to print are as below with attached files";
$str_mailbody .= "<br/><br/>Quantity: " . $int_quantity;
$str_mailbody .= "<br/><br/><br/><br/>Thank you...";



$mail = new PHPMailer();

$mail->IsSMTP();                                      // set mailer to use SMTP
$mail->SMTPAuth = true;     // turn on SMTP authentication
$mail->SMTPSecure = "ssl";                 // sets the prefix to the server					
$mail->Mailer = "smtp";
$mail->SetLanguage("en", "../includes/");

$mail->Host = "dedrelay.secureserver.net";
//$mail->Host = "localhost";  // specify main and backup server
$mail->Port = 25;          // TCP port to connect to
$mail->Username = "info@AdoptABill.com";  // SMTP username
$mail->Password = "Simple123!"; // SMTP password

$mail->From = $str_from;
$mail->FromName = "";
$mail->AddAddress($str_to);
//$mail->AddAddress("abc@abc.com");                  // name is optional
//$mail->AddReplyTo($strfrom, "www.AdoptABill.com");

$mail->WordWrap = 50;                                 // set word wrap to 50 characters
$mail->AddAttachment($str_file_path);         // add attachments
//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
$mail->IsHTML(true);                                  // set email format to HTML

//			$mail->Subject = "Here is the subject";
$mail->Subject = $str_subject;
//			$mail->Body    = "This is the HTML message body <b>in bold!</b>";
$mail->Body    = $str_mailbody;

//			$mail->AltBody = "This is the body in plain text for non-HTML mail clients";

if (!$mail->Send()) {
    // print("Message could not be sent. <p>");
    // print("Mailer Error: " . $mail->ErrorInfo);
    //exit;
}

#----------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------
#Write to xml file
//WriteXml();
#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_order_status_list.php?type=S&msg=MS&tit=" . urlencode(RemoveQuote($str_title)));
exit();
#-----------------------------------------------------------------------------------------------------
