function frm_edit_validate()
{
    with(document.frm_edit)
    {
        if(trim(txt_title.value) == "")
        {
            alert("Please Enter Title.");
            txt_title.select();
            txt_title.focus();
            return false;
        }

        if((trim(txt_percent.value) == "" && trim(txt_amount.value) == "") || (trim(txt_percent.value) != "" && trim(txt_amount.value) != ""))
        {
            alert("Please Enter Either Discount Percentage Or Discount Amount.");
            txt_percent.select();
            txt_percent.focus();
            return false;
        }
        if(trim(txt_amount.value) == "")
        {
            //if(sValidatePercentage(trim(txt_discount_percentage.value)) == false)
            if(trim(txt_percent.value)>100)
            {
                alert("Please Enter Valid Discount Percentage.");
                txt_percent.select();
                txt_percent.focus();
                return false;
            }
        }
        if(trim(txt_percent.value) == "")
        {
            if(sValidatePrice(trim(txt_amount.value)) == false)
            {
                alert("Please Enter Valid Discount Amount.");
                txt_amount.select();
                txt_amount.focus();
                return false;
            }
        }

    }
    return true;
}