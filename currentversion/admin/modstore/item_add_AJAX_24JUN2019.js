// JavaScript Document
$(document).ready(function() {
    $('#frm_add').on('submit', function(event) {
        event.preventDefault();

        var formData = new FormData($('#frm_add')[0]);
        formData.append("cbo_cat",$("select#cbo_cat").val());
        formData.append("cbo_subcat",$("select#cbo_subcat").val());
        //formData.append("cbo_mdl",$("select#cbo_mdl").val());
        //formData.append("cbo_month",$("select#cbo_month").val());
        //formData.append("cbo_year",$("select#cbo_year").val());
        formData.append("txt_code",$("input#txt_code").val());
        formData.append("txt_title",$("input#txt_title").val());
        
        formData.append("txt_listprice",$("input#txt_listprice").val());
        formData.append("txt_ourprice",$("input#txt_ourprice").val());
        formData.append("txt_memberprice",$("input#txt_memberprice").val());
        formData.append("ta_desc",$("textarea#ta_desc").val());
        
        var arr_color = new Array();
        $("[id='cbx_color']:checked").each(function (i) {
            arr_color[i] = $(this).val();
        });
        formData.append('cbx_color', arr_color);
        
        
        var arr_size = new Array();
        $("[id='cbx_size']:checked").each(function (i) {
            arr_size[i] = $(this).val();
        });
        formData.append('cbx_size', arr_size);
        
        var arr_length = new Array();
        $("[id='cbx_length']:checked").each(function (i) {
            arr_length[i] = $(this).val();
        });
        formData.append('cbx_length', arr_length);
        
        var arr_type = new Array();
        $("[id='cbx_type']:checked").each(function (i) {
            arr_type[i] = $(this).val();
        });
        formData.append('cbx_type', arr_type);
        
        var arr_work = new Array();
        $("[id='cbx_work']:checked").each(function (i) {
            arr_work[i] = $(this).val();
        });
        formData.append('cbx_work', arr_work);
        
        var arr_fabric = new Array();
        $("[id='cbx_fabric']:checked").each(function (i) {
            arr_fabric[i] = $(this).val();
        });
        formData.append('cbx_fabric', arr_fabric);
        
        var arr_occasion = new Array();
        $("[id='cbx_occasion']:checked").each(function (i) {
            arr_occasion[i] = $(this).val();
        });
        formData.append('cbx_occasion', arr_occasion);
        
       /* var arr_shippingoptions = new Array();
        $("[id='cbx_shipping']:checked").each(function (i) {
            arr_shippingoptions[i] = $(this).val();
        });
        formData.append('cbx_shipping', arr_shippingoptions);*/
        
        //formData.append("txt_dressmaterialprice",$("input#txt_dressmaterialprice").val());
        //formData.append("txt_semistitchedprice",$("input#txt_semistitchedprice").val());
        //formData.append("txt_customizedprice",$("input#txt_customizedprice").val());
        
        
        formData.append("txt_availability",$("input#txt_availability").val());
        formData.append("cbo_avail_status",$("select#cbo_avail_status").val());
        //formData.append("txt_type",$("input#txt_type").val());
        
        formData.append("txt_dshipping_value",$("input#txt_dshipping_value").val());
        formData.append("txt_dshipping_info",$("input#txt_dshipping_info").val());
        formData.append("txt_ishipping_value",$("input#txt_ishipping_value").val());
        formData.append("txt_ishipping_info",$("input#txt_ishipping_info").val());
        formData.append("txt_weight",$("input#txt_weight").val());
        
        formData.append("cbo_new",$("select#cbo_new").val());
        formData.append("cbo_featured",$("select#cbo_featured").val());
        formData.append("cbo_hot",$("select#cbo_hot").val());
        formData.append("cbo_visible",$("select#cbo_visible").val());
        
        formData.append("txt_seo_title",$("input#txt_seo_title").val());
        formData.append("txt_seo_keywords",$("input#txt_seo_keywords").val());
        formData.append("ta_seo_desc",$("textarea#ta_seo_desc").val());
        
        formData.append("catid",$("input#catid").val());
        formData.append("key",$("input#key").val());
        formData.append("PagePosition",$("input#PagePosition").val());
        
        //formData.append("txt_nophoto",$("input#txt_nophoto").val());
        //formData.append('hdn_pkid', $("input#hdn_pkid").val());
        
        //formData.append('video_clip', $("input#video_clip")[0].files[0]);
        //formData.append("cbo_extension",$("select#cbo_extension").val());
        //formData.append("txt_ptitle",$("input#txt_ptitle").val());
        //formData.append("txt_purl",$("input#txt_purl").val());
        //var formData = new FormData($('#frm_add')[1]);
        //alert($("input#video_clip")[0].files[0]));

        if($("select#cbo_cat").val() == 0) 
        {
            alert("Please Select Category.");
            $("select#cbo_cat").focus();
            exit();
        }
        if($("select#cbo_subcat").val() == 0) 
        {
            alert("Please Select Sub Category.");
            $("select#cbo_subcat").focus();
            exit();
        }
        /*if($("select#cbo_mdl").val() == 0) 
        {
                alert("Please Select Recipient.");
                exit();
        }*/
        if($("input#txt_code").val() == "") 
        {
            alert("Please enter item code");
            $("input#txt_code").focus();
            exit();
        }

        if($("input#txt_title").val() == "") 
        {
            alert("Please enter title");
            $("input#txt_title").focus();
            exit();
        }
        if($("input#txt_listprice").val() == "") 
        {
            alert("Please Enter Price");
            $("input#txt_listprice").focus();
            exit();
        }
        
                
        var num_value=$('#txt_listprice').val();
        if(!$.isNumeric(num_value))
        {
            alert("Please Enter Numeric Value For List Price.");
            $("input#txt_listprice").focus();
            exit();
            
        }

        var num_ourprice=$('#txt_ourprice').val();
        if((!$.isNumeric(num_ourprice)) && (num_ourprice != ""))
        {
            alert("Please Enter Numeric Value For Our Price.");
            $("input#txt_ourprice").focus();
            exit();
        }
        /*var num_memberprice=$('#txt_memberprice').val();
        if((!$.isNumeric(num_memberprice)) && (num_memberprice != ""))
        {
            alert("Please Enter Numeric Value For Member Price.");
            $("input#txt_memberprice").focus();
            exit();
        }*/
        
        /*if($("input#video_clip").val() == "") 
        {
            alert("Please Select File");
            exit();
        }*/
        /*var num_shippingprice=$('#txt_domestic').val();
        if((!$.isNumeric(num_shippingprice)) && (num_shippingprice != ""))
        {
            alert("Please Enter Numeric Value For Shipping Price.");
            return false;
        }*/

//        alert($("input#txt_ourprice").val());
//        if($("input#txt_ourprice").val() != "") 
//        {
//            alert("hi....");
//            var num_ourprice=$('#txt_ourprice').val();
//            if(!$.isNumeric(num_ourprice))
//            {
//                {
//                    alert("Please Enter Numeric Value For Our Price.");
//                    return false;
//                }
//            return true;
//            }
//        }

//        if(isNumeric('#txt_domestic'.value)==true)
//        {
//                {
//                    alert("Please Enter Numeric Value For Shipping Value.");
//                    return false;
//                }
//        return true;
//        }
        
//        if($("input#txt_ourprice").val() != "") 
//        {
//            var num_our_value=$('#txt_ourprice').val();
//            if(!$.isNumeric(num_our_value))
//            {
//                {
//                    alert("Please Enter Numeric Value For Our Price.");
//                    return false;
//                }
//
//            return true;
//            }
//            
//        }


//        if(!$("input#txt_ourprice").val() == "")
//        {
//            var num_our_value=$('#txt_ourprice').val();
//            if(!$.isNumeric(num_our_value))
//            {
//                {
//                    alert("Please Enter Numeric Value For Our Price.");
//                    return false;
//                }
//            return true;
//            }
//            
//        }

        
        
//        var num_ship_value=$('#txt_domestic').val();
//        if(!$("input#txt_domestic").val() == "")
//        {
//            
//            if(!$.isNumeric(num_ship_value))
//            {
//                {
//                    alert("Please Enter Numeric Value For Shipping Price.");
//                    return false;
//                }
//            return true;
//            }
//        }
//        if(!$("input#txt_domestic").val() == "")
//        {
//            if(!$.isNumeric(num_value))
//            {
//                {
//                    alert("Please Enter Numeric Value For Shipping Value.");
//                    return false;
//                }
//                return true;
//            }
//             return false;
//        }

//        
//        if(!$("input#txt_ourprice").val() == "") 
//        {
//            var num_oprice=$('#txt_ourprice').val();
//            if(!$.isNumeric(num_oprice))
//
//            {
//
//                {
//                    alert("Please Enter Numeric Value For Our Price.");
//                    return false;
//                }
//
//            return true;
//            }
//        }
       
//        if(!$("input#txt_domestic").val() == "")
//        {
//            var num_ship_value=$('#txt_domestic').val();
//            if(!$.isNumeric(num_ship_value))
//
//            {
//
//                {
//                    alert("Please Enter Numeric Value For Shipping Value.");
//                    return false;
//                }
//
//            return true;
//            }
//        }

       // else
      //  {
            $.ajax({
                    /*xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener('progress', function(e) {
                        if(e.lengthComputable)
                        {
                            //console.log('Bytes Loaded: ' + e.loaded);
                            //console.log('Total Size: ' + e.total);
                            //console.log('Percentage uploaded: ' + (e.loaded/e.total))
                            var percent = Math.round((e.loaded/e.total) * 100);
                            $('#progressBar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
                        }												 
                    });
                    return xhr;
                   },*/

                    type: 'POST',
                    url: 'item_add_p.php',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        //alert(data);
                        var $responseText=JSON.parse(data);

                            if($responseText.status == 'ERR' && $responseText.IFE == 'IF')
                            {
                                location.href = './item_list.php?' + $responseText.redirect_string +'&msg=IE&type=E&#ptop';
                            }
                            else if($responseText.status == 'ERR' && $responseText.IFE == 'IU')
                            {
                                location.href = './item_list.php?' + $responseText.redirect_string + '&msg=IU&type=E&#ptop';
                            }
                            else if($responseText.status == 'ERR' && $responseText.IFE == 'DU')
                            {
                                //alert('./item_list.php?' + $responseText.redirect_string + '&msg=DU&type=E&#ptop');
                                location.href = './item_list.php?' + $responseText.redirect_string + '&msg=DU&type=E&#ptop';
                            } 
                            else if($responseText.status == 'ERR' && $responseText.IFE == 'SM')
                            {
                                location.href = './item_list.php?'+ $responseText.redirect_string +'&msg=F&type=E&#ptop';
                            }
                            else if($responseText.status == 'SUC' && $responseText.IFE == 'SA')
                            {
                                location.href = './item_list.php?msg=S&type=S&#ptop';
                                    //location.reload();
                                    //window.setTimeout(function(){location.reload()},3000)
                            }
                            else if($responseText.status == 'ERR' && $responseText.IFE == 'BL')
                            {
                                location.href = './item_list.php?' + $responseText.redirect_string +'&msg=BL&type=E&#ptop';
                            }
                            /*else if($responseText.status == 'SUC')
                            {
                                    // Fail message
                                    $('#success').html("<div class='alert alert-danger'>");
                                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                            .append("</button>");
                                    $('#success > .alert-danger').append("<strong> " + $responseText.message + " ");
                                    $('#success > .alert-danger').append('</div>');
                            }*/

                            //alert(data);
                            //alert("File Uploaded Successfully");
                            //location.reload();
                  }
//                  alert(data);
           });
   // } 

        });
	});
				
						   // JavaScript Document
