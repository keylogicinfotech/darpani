<?php
/*
    File Name  :- int_report_view_monthly.php
    Create Date:- MARCH-2019
    Intially Create By :- 0013
*/
#---------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_app_specific.php";
include "../../includes/lib_datetimeyear.php";

#---------------------------------------------------------------------------------------------

//$str_adminname = $_SESSION["adminname"];
#---------------------------------------------------------------------------------------------
# Initialization of variables used for message display.   
$str_type = "";
$str_msg = "";

# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
# Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    { case("F"): $str_msg = "Some information(s) missing. Please try again."; break; }
}

# Display message.
if($str_type != "" && $str_msg != "")
{ print(DisplayMessage(0,$str_msg,$str_type)); }
#---------------------------------------------------------------------------------------------
$str_m="";
if(isset($_GET["cbo_month"]) && trim($_GET["cbo_month"])!="")
        { $str_m=trim($_GET["cbo_month"]); }
if($str_m=="" || $str_m<=0 || is_numeric($str_m)==false)
        { $str_m=date("m");	}
$str_y="";
if(isset($_GET["cbo_year"]) && trim($_GET["cbo_year"])!="")
        { $str_y=trim($_GET["cbo_year"]); }
if($str_y=="" || $str_y<=0 || is_numeric($str_y)==false)
        { $str_y=date("Y"); }
				
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_UNIQUE_VIEW_REPORT_YEARLY);?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<a name="ptop"></a>
<div class="container">
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-6 col-sm-6 col-md-12 button_space">
            <?php /*?><div class="btn-group" role="group" aria-label="...">
                <a href="#help"  class="btn btn-info " title="Click to view help"><i class="glyphicon glyphicon-info-sign  "></i>&nbsp; Help</a> 
                <a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Back to State List"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;Back</a>
                </div><?php */?>
            <a href="item_report_view_monthly.php" class="btn btn-default" title="<?php print($STR_TITLE_UNIQUE_VIEW_REPORT_MONTHLY); ?>"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;<?php print($STR_TITLE_UNIQUE_VIEW_REPORT_MONTHLY); ?></a>
        </div>
        <div class="col-md-6 col-sm-6 col-md-12" align="right" ><h3><?php print($STR_TITLE_UNIQUE_VIEW_REPORT_YEARLY); ?> </h3></div>
    </div>
    <hr>
    <?php if($str_type != "" && $str_msg != "") { print(DisplayMessage(0,$str_msg,$str_type)); } ?>
    <div class="row padding-10">
    <div class="col-md-12">
			
			
    <div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <div class="row padding-10">
                    <div class="col-md-6 col-sm-6 col-xs-8"><i class="fa fa-filter"></i>&nbsp;<strong><?php print($STR_FILTER_CRITERIA); ?></strong></div>
                    <div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>	
            </div>
        </h4>
    </div>
    <div class="panel-body">
        <div class="col-md-12" align="center">
            <form name="frm_filter" method="get" action="item_report_view_yearly.php">
                <label></label>
                <label><?php // print(DisplayMonth($str_m,"-- Select Month --"));?></label>
                <label><?php print(DisplayYear($str_y,"-- Select Year --",0));?></label>
                <label>
                <?php /*?><input type="submit" name="btn_submit" value="Click to View" class="btn btn-success btn-sm" title="Click to view report" ><?php */?>
                <?php print DisplayFormButton('VIEWREPORT', 0)?>
                </label>&nbsp;
                <?php /*?><label>
                    <a href="item_report_view_yearly.php?cbo_month=<?php print(date("m")); ?>&cbo_year=<?php print(date("Y")); ?>" class="btn btn-danger btn-sm" title="Click to view current month reports">&nbsp;&nbsp;&nbsp;Reset&nbsp;&nbsp;&nbsp;</a>
                </label><?php */?>
            </form>
        </div>
        <?php /*?></div><?php */?>
    </div>
    </div>
        <div class="table-responsive">
            <?php 
            $str_month_title="";
            $str_year_title="";
            $str_filter_criteria="";
            $str_month_title=MonthName($str_m);
            $str_year_title=$str_y;
            $str_filter_criteria="  WHERE ( b.viewyear= ".$str_y." ) ";
            #------------------------------------------------------------------------------------------------
            $str_query_select="SELECT a.pkid,a.title,COUNT(b.pkid) totalview FROM " .$STR_DB_TABLE_NAME. " a  ";
            $str_query_select.=" LEFT JOIN " .$STR_DB_TABLE_NAME_STORE_VIEW. " b ON ( b.pkid=a.pkid )  ";
            $str_query_select.=" ".$str_filter_criteria;
            $str_query_select.=" GROUP BY pkid ";
            $str_query_select.=" ORDER BY totalview DESC, title";
            //print $str_query_select; exit;
            $rs_list=GetRecordSet($str_query_select);
            #------------------------------------------------------------------------------------------------
            #### Get COUNT and then MAX value of view from table
            $str_query_select = "SELECT MAX(totalviewforyear) AS maxviewforyear  FROM ( SELECT COUNT(pkid) AS totalviewforyear FROM " .$STR_DB_TABLE_NAME_STORE_VIEW. " WHERE viewyear= ".$str_y." GROUP BY pkid ) AS DVIEW";
//            print $str_query_select; 
            $rs_list_maximum=GetRecordSet($str_query_select);

            $int_max_view="";
            if($rs_list_maximum->fields("maxviewforyear")==NULL || $rs_list_maximum->fields("maxviewforyear")=="") 
            { $int_max_view=0; }
            else 
            { $int_max_view=$rs_list_maximum->fields("maxviewforyear"); }
            #------------------------------------------------------------------------------------------------
            ?>
            <table class="table table-striped table-bordered ">
                <thead>
        <?php /*?>	<form name="frm_siteconfig" action="sc_edit_p.php" method="post" onSubmit="return frm_siteconfig_validate_siteconfig();" role="form"><?php */?>					
                    <tr>
                        <th width="4%">Sr.#</th>
                        <th width="36%">Details</th>
                        <th >Unique View for <?php print $str_year_title ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($rs_list->eof()==true) { ?>
                    <tr>
                        <td colspan="3" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td>
                    </tr>
                    <?php } else { $int_cnt=1; $int_total_monthly_view=0; while(!$rs_list->eof()) { ?>
                    <tr class="<?php print($int_cnt%2); ?>">
                        <td class="text-center"><?php print($int_cnt); ?></td>
                        <td><h4 class="nopadding"><b><?php print($rs_list->fields("title")); ?></b></h4></td>
                        <td>
                            <table align="left" cellpadding="3" cellspacing="0" border="0">
                                <td valign="top" align="left">
                                <?php if($rs_list->fields("totalview")!=0) 
                                    { $width=($rs_list->fields("totalview")/$int_max_view)*$INT_BAR_CHART_WIDTH;
//                                       print "width:" .($width); 
                                        $totalview = $rs_list->fields("totalview");
                                        print(GetComparisonChartView($width,$VIEW_BAR_CHART_COLOR,$totalview));                                                      
//                                        print $totalview;					
                                    } else { print("Domain not viewed yet."); } ?>
                                </td>
                                <?php /*?><td valign="top" align="left" class="HighLightText10ptBold text-danger"><strong>&nbsp;<?php print($rs_list->fields("totalview")."&nbsp;"); ?></strong></td><?php */?>
                            </table>
                        </td>
                    </tr>
                    <?php $int_total_monthly_view=$int_total_monthly_view+$rs_list->fields("totalview");
                        $int_cnt=$int_cnt+1; $rs_list->movenext(); } ?>
                    <tr class="<?php print($int_cnt%2); ?>">
                        <?php /*?><td height="20" align="center" valign="middle"></td><td></td><td></td><?php */?>
                    </tr>
                <?php }  ?>
                </tbody>	
            </table>
            </div>
        </div>
    </div>
    <?php include "../../includes/help_for_edit.php"; ?>		
    <!-- /.container -->											
</div>
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>   
</body></html>
