<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
//include "./item_cat_config.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_xml.php";
include "../../includes/lib_email.php";

//print_r($_SESSION);exit;
#----------------------------------------------------------------------
$int_cat_pkid = "";
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ 
    $int_cat_pkid=trim($_GET["catid"]);     
}

$str_name_key="";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ 
    $str_name_key=trim($_GET["key"]); 
}

# get data for paging
$int_page = "";
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ 
    $int_page = $_GET["PagePosition"];     
}
else
{
    $int_page = 1; 
}

$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page;

#------------------------------------------------------------------------------------------------
# get Query String Data
$str_status = "";
if(isset($_GET["cbo_status"])) { $str_status = trim($_GET["cbo_status"]); }
#------------------------------------------------------------------------------------------------
$str_query_where = "";
if($str_status != "") { 
    $str_query_where = " WHERE shippingstatus = '".$str_status."'";
}
# Select Query
$str_query_select = "";
$str_query_select = "SELECT *";
$str_query_select .= " FROM t_store_purchase";
$str_query_select .= " ".$str_query_where;
$str_query_select .= " ORDER BY purchasedatetime DESC";
$rs_list = GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------------
# Initialization of variables used for message display.  
$str_type = "";
$str_message = "";
# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
	case("E"): $str_type = "E"; break;
	case("W"): $str_type = "W"; break;
    }
}
#	Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
  
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("SU"): $str_message = $STR_MSG_ACTION_STATUS_CHANGED; break;
        case("IE"): $str_message = $STR_MSG_ACTION_INVALID_EMAIL_FORMAT; break;
        case("MS"): $str_message = $STR_MSG_ACTION_MAIL_SENT; break;
    }
} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE_ORDER_STATUS) ;?></title>
    <?php //print(Display_Page_Metatag($STR_TITLE_PAGE)); ?>
    <?php /* ?><link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../css/user.css" rel="stylesheet">       <?php */ ?>
</head>
<body>
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="row padding-10">
                    <div class="col-md-6 col-sm-6 col-xs-12 button_space">
                        <div class="btn-group" role="group" aria-label="...">
                        <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Back to State List"><i class="fa fa-chevron-left"></i>&nbsp;Back</a>
                        <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a><?php */?>
                        <a href="./item_report_transaction_monthly.php" class="btn btn-default" title="<?php print($STR_TITLE_REPORT_MONTHLY);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_REPORT_MONTHLY);?></a>
                        <a href="./item_report_transaction_yearly.php" class="btn btn-default" title="<?php print($STR_TITLE_REPORT_YEARLY);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_REPORT_YEARLY);?></a>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE_ORDER_STATUS); ?></h3></div>
                </div><hr>
    
                <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); } ?>
                <div class="row padding-10">
                    <div class="col-md-12" align="left">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                            <span>
                                <strong><i class="fa fa-filter"></i>&nbsp;&nbsp;Filter Criteria</strong>
                            </span>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12" align="center">
                                    <form name="frm_filter" method="GET" action="item_order_status_list.php">
                                        <label>Select  </label>
                                        <label>
                                            <select id="cbo_status" name="cbo_status" class="form-control input-sm">
                                                <option value=""> -- ALL -- </option>
                                                <option value="<?php print $STR_CBO_OPTION1; ?>" <?php print(CheckSelected($STR_CBO_OPTION1,strtoupper($str_status)));?>><?php print $STR_CBO_OPTION1; ?></option>;
                                                <option value="<?php print $STR_CBO_OPTION2; ?>" <?php print(CheckSelected($STR_CBO_OPTION2,strtoupper($str_status)));?>><?php print $STR_CBO_OPTION2; ?></option>
                                                <option value="<?php print $STR_CBO_OPTION3; ?>" <?php print(CheckSelected($STR_CBO_OPTION3,strtoupper($str_status)));?>><?php print $STR_CBO_OPTION3; ?></option>
                                                <option value="<?php print $STR_CBO_OPTION4; ?>" <?php print(CheckSelected($STR_CBO_OPTION4,strtoupper($str_status)));?>><?php print $STR_CBO_OPTION4; ?></option>
                                                <option value="<?php print $STR_CBO_OPTION5; ?>" <?php print(CheckSelected($STR_CBO_OPTION5,strtoupper($str_status)));?>><?php print $STR_CBO_OPTION5; ?></option>
                                            </select>
                                        </label>
                                        <label>&nbsp;<?php print DisplayFormButton('VIEW', 0)?></label>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php /* ?><form name="frm_list" action="item_order_p.php" method="POST" onSubmit="return frm_list_check_displayorder();"><?php */ ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered ">
                        <thead>
                            <tr>
                                <th width="4%">Sr. #</th>
                                <th width="10%">Date</th>
                                <th width="">Details</th>
                                <th width="20%">Order Status</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if($rs_list->Count() <= 0)
                            { ?>
                                <tr>
                                    <td colspan="9" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE);?></td>
                                </tr>
                            <?php 
                            } else {
                                $int_cnt = 1;    
                                while(!$rs_list->EOF()) 
                                { ?>
                                <tr>
                                    <td align="center"><?php print $int_cnt; ?><input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>"></td>
                                    <td align="center" class="text-help"><?php print(date("d-M-Y h:i:s", strtotime($rs_list->Fields("purchasedatetime")))); ?></td>
                                    <td align="left">
                                        <h4><b><?php print $rs_list->Fields("producttitle"); ?></b></h4>
					<?php if($rs_list->Fields("itemcode") != "") { ?><span class="text-help">Itemcode: </span>&nbsp;<?php print $rs_list->Fields("itemcode"); ?><?php } ?>
                                        <?php if($rs_list->Fields("cattitle") != "") { ?><br/><span class="text-help">Category: </span>&nbsp;<?php print $rs_list->Fields("cattitle"); ?><?php } ?>
                                        <?php if($rs_list->Fields("subcattitle") != "") { ?><br/><span class="text-help">Sub Category: </span>&nbsp;<?php print $rs_list->Fields("subcattitle"); ?><?php } ?>
                                        <?php if($rs_list->Fields("quantity") != "") { ?><br/><span class="text-help">Quantity: </span>&nbsp;<?php print $rs_list->Fields("quantity"); ?><?php } ?>
                                        <?php if($rs_list->Fields("price") != "") { ?><br/><span class="text-help">Item Price: </span>&nbsp;<?php print $rs_list->Fields("price"); ?><?php } ?>
                                        <?php if($rs_list->Fields("tailoringprice") != "") { ?><br/><span class="text-help">Tailoring Price: </span>&nbsp;<?php print $rs_list->Fields("tailoringprice"); ?><?php } ?>
                                        <?php 
                                        $int_total_amount = 0;
                                        if($rs_list->Fields("extendedprice") != "") { ?><br/><span class="text-help">Order Total: </span>&nbsp;
                                        <?php 
                                        $int_total_amount = $rs_list->Fields("extendedprice") + ($rs_list->Fields("tailoringprice") * $rs_list->Fields("quantity"));
                                        print $int_total_amount; ?>
                                        <?php } ?>
                                        <?php /*
                                        if($rs_list->Fields("shippingstatus") == $STR_CBO_OPTION4 || $rs_list->Fields("shippingstatus") == $STR_CBO_OPTION5) { ?><br/><span class="text-help">Refund Amount: </span>&nbsp; 
                                        <?php 
                                        $int_refund_amount = ($int_total_amount * $INT_REFUND_PERCENTAGE) / 100;
                                        print $int_refund_amount; ?>
                                        <?php } */ ?>
                                    </td>
                                    <?php $str_class_color_td = ""; ?>
                                    <?php if($rs_list->Fields("shippingstatus") == $STR_CBO_OPTION1) { $str_class_color_td = "alert-warning"; ?>
                                    <?php } else if($rs_list->Fields("shippingstatus") == $STR_CBO_OPTION2) { $str_class_color_td = "alert-info"; ?>
                                    <?php } else if($rs_list->Fields("shippingstatus") == $STR_CBO_OPTION3) { $str_class_color_td = "alert-success"; ?>
                                    <?php } else if($rs_list->Fields("shippingstatus") == $STR_CBO_OPTION4) { $str_class_color_td = "alert-danger";?>
                                    <?php } else if($rs_list->Fields("shippingstatus") == $STR_CBO_OPTION5) { $str_class_color_td = "alert-danger"; ?>
                                    <?php } ?>
                                    <td align="center" class="<?php print $str_class_color_td; ?>">
                                        <form id="frm_status<?php print $int_cnt; ?>" method="POST" name="frm_status<?php print $int_cnt; ?>" action="item_order_status_list_p.php"> 
                                            <div class="form-group control-group">
                                                <div class="controls">
                                                    <select id="cbo_status" name="cbo_status" class="form-control input-sm">
                                                        <option value="<?php print $STR_CBO_OPTION1; ?>" <?php print(CheckSelected($STR_CBO_OPTION1,strtoupper($rs_list->Fields("shippingstatus"))));?>><?php print $STR_CBO_OPTION1; ?></option>;
                                                        <option value="<?php print $STR_CBO_OPTION2; ?>" <?php print(CheckSelected($STR_CBO_OPTION2,strtoupper($rs_list->Fields("shippingstatus"))));?>><?php print $STR_CBO_OPTION2; ?></option>
                                                        <option value="<?php print $STR_CBO_OPTION3; ?>" <?php print(CheckSelected($STR_CBO_OPTION3,strtoupper($rs_list->Fields("shippingstatus"))));?>><?php print $STR_CBO_OPTION3; ?></option>
                                                        <option value="<?php print $STR_CBO_OPTION4; ?>" <?php print(CheckSelected($STR_CBO_OPTION4,strtoupper($rs_list->Fields("shippingstatus"))));?>><?php print $STR_CBO_OPTION4; ?></option>
                                                        <option value="<?php print $STR_CBO_OPTION5; ?>" <?php print(CheckSelected($STR_CBO_OPTION5,strtoupper($rs_list->Fields("shippingstatus"))));?>><?php print $STR_CBO_OPTION5; ?></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group control-group">
                                                <div class="controls">
							<?php /* ?><label>Courier Information</label><span class="text-help-form"> </span><?php */ ?>
                                                        <textarea name="ta_desc"  rows="4" class="form-control input-sm" placeholder="Enter Courier Information" ><?php print $rs_list->Fields("shippinginfo");?></textarea>
                                                </div>
                                            </div>

                                            <input type="hidden" id="hdn_pkid" name="hdn_pkid" value="<?php print $rs_list->Fields("purchasepkid"); ?>" />
                                            <input type="hidden" id="hdn_productpkid" name="hdn_productpkid" value="<?php print $rs_list->Fields("productpkid"); ?>" />
                                            <input type="hidden" id="hdn_userpkid" name="hdn_userpkid" value="<?php print $rs_list->Fields("userpkid"); ?>" />
                                            <input type="hidden" id="hdn_total_amount" name="hdn_total_amount" value="<?php print $int_total_amount; ?>" />
                                            <input type="hidden" id="hdn_total_amount" name="hdn_total_amount_giftcard" value="<?php print $rs_list->Fields("giftcard"); ?>" />
                                            <input type="hidden" id="hdn_paymentoptionpkid" name="hdn_paymentoptionpkid" value="<?php print $rs_list->Fields("paymentoptionpkid"); ?>" />
                                            <?php if($rs_list->Fields("shippingstatus") <> $STR_CBO_OPTION5) { ?>
                                                
                                            <button class="btn btn-sm btn-primary" type="submit"><i class='fa fa-edit'></i>&nbsp;<b>Change Status</b></button>
                                            <?php } ?>
                                        </form>
                                        <a class="btn btn-success btn-sm" href="./item_report_transaction_order_print.php?purdt=<?php print($rs_list->fields("purchasedatetime")); ?>&upkid=<?php print($rs_list->fields("userpkid")); ?>&cpkid=<?php print($rs_list->fields("countrypkid")); ?>" target="_blank"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print This Order </a> 
                                    </td>
                                </tr>
                                <?php 
                                $int_cnt++;
                                $rs_list->MoveNext();
                                }?>
                                
                    <?php  } ?>
                        </tbody>
                    </table>
                </div>
                <?php /* ?></form><?php */ ?>
                <?php include "../../includes/help_for_list.php"; ?>
            </div>            
        </div>        
    </div>        
    <?php include "../../includes/include_files_admin.php"; ?>    
    <?php include($STR_ADMIN_FOOTER_PATH); ?>   
</body>
</html>
