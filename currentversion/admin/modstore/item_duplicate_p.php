<?php
/*
Module Name:- Manage photoset
File Name  :- mdl_ps_add_p.php
Create Date:- 8-Sep-2006
Intially Create By :- 0014
*/
#---------------------------------------------------------------------------------------------------------------------------
#Include Files

include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
include "../../includes/lib_file_upload.php";
//include "./store_item_app_specific.php";
include "../../includes/lib_xml.php";
include "../../includes/lib_email.php";
#-------------------------------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_cat_pkid = "";
if(isset($_GET["catid"]) && trim($_GET["catid"]) != "" )
{   
    $int_cat_pkid = trim($_GET["catid"]);    
}

$str_name_key = "";
if(isset($_GET["key"]) && trim($_GET["key"]) != "" )
{ $str_name_key = trim($_GET["key"]); }

# get data for paging
$int_page="";
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page = 1; }

$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";

$int_pkid = 0;
if(isset($_GET["pkid"]) && trim($_GET["pkid"])!="" )
{
    $int_pkid=trim($_GET["pkid"]);
}

if($int_pkid=="" || $int_pkid==0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}

$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
//print $str_query_select; //exit;
$rs_list = GetRecordset($str_query_select);
if ($rs_list->count()==0)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}	
#-------------------------------------------------------------------------------------------------------------------------
//print_r($_POST); exit;
$int_catpkid = 0;
$int_subcatpkid = 0;

$str_cattitle = "";
$str_subcattitle = "";
//$int_mdlpkid=0;
//$int_pricepkid = 0;
$int_listprice = 0.00;
$int_ourprice = 0.00;
$int_memberprice = 0.00;
//$int_promocodepkid = 0;
$str_color = array();
$str_size = array();
$str_length = array();
$str_occasion = array();
$str_type=array();
$str_shipping_type = array();
$str_fabric = array();
$str_work = array();
$int_weight = 0;
//$str_month="";
//$str_year="";
$str_itemcode = "";
$str_title = "";
//$str_sub_title = "";
//$str_nophoto="";
$str_desc = "";
//$str_name="";
//$str_purchase="YES";
$str_new = "YES";
$str_hot = "NO";
$str_featured = "NO";
$str_visible = "YES";
//$str_ptitle = "";
//$str_purl = "";
$str_availability = "";
$str_availability_status = "";
$int_domestic_shipping_value = 0;
$int_international_shipping_value = 0;
$str_domestic_shipping_info = "";
$str_international_shipping_info = "";

$str_seo_title = "";
$str_seo_keywords = "";
$str_seo_desc = "";

//$int_dressmaterialprice = 0.00;
//$int_semistitchedprice = 0.00;
//$int_customizedprice = 0.00;


/* $str_video_file="";
$str_ext="";
 */
#-------------------------------------------------------------------------------------------------------------------------
$int_catpkid = $rs_list->Fields("catpkid");
$int_subcatpkid = $rs_list->Fields("subcatpkid");
$str_itemcode = $rs_list->Fields("itemcode");
$str_title = $rs_list->Fields("title")." ".date("Ymdhis");
$str_desc = $rs_list->Fields("description");
$int_listprice = $rs_list->Fields("listprice");
$int_ourprice = $rs_list->Fields("ourprice");
$int_memberprice = $rs_list->Fields("memberprice");

$str_query_select = "";
$str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_COLOR." WHERE itempkid=".$rs_list->Fields("pkid");
$rs_list_selected_color = GetRecordSet($str_query_select);
if($rs_list_selected_color->Count() > 0)
{
	while(!$rs_list_selected_color->EOF())
	{
		array_push($str_color, $rs_list_selected_color->Fields("masterpkid"));
		$rs_list_selected_color->MoveNext();
	}
}

$str_query_select = "";
$str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_SIZE." WHERE itempkid=".$rs_list->Fields("pkid");
$rs_list_selected_size = GetRecordSet($str_query_select);
if($rs_list_selected_size->Count() > 0)
{
	while(!$rs_list_selected_size->EOF())
        {
        	array_push($str_size, $rs_list_selected_size->Fields("masterpkid"));
                $rs_list_selected_size->MoveNext();
	}
}

$str_query_select = "";
$str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_TYPE." WHERE itempkid=".$rs_list->Fields("pkid");
$rs_list_selected_type = GetRecordSet($str_query_select);
if($rs_list_selected_type->Count() > 0)
{
	while(!$rs_list_selected_type->EOF())
	{
		array_push($str_type, $rs_list_selected_type->Fields("masterpkid"));
		$rs_list_selected_type->MoveNext();
	}
}

$str_query_select = "";
$str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_LENGTH." WHERE itempkid=".$rs_list->Fields("pkid");
$rs_list_selected_length = GetRecordSet($str_query_select);
if($rs_list_selected_length->Count() > 0)
{
	while(!$rs_list_selected_length->EOF())
        {
        	array_push($str_length, $rs_list_selected_length->Fields("masterpkid"));
                $rs_list_selected_length->MoveNext();
	}
}

$str_query_select = "";
$str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_WORK." WHERE itempkid=".$rs_list->Fields("pkid");
$rs_list_selected_work = GetRecordSet($str_query_select);
if($rs_list_selected_work->Count() > 0)
{
	while(!$rs_list_selected_work->EOF())
        {
        	array_push($str_work, $rs_list_selected_work->Fields("masterpkid"));
                $rs_list_selected_work->MoveNext();
	}
}

$str_query_select = "";
$str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_FABRIC." WHERE itempkid=".$rs_list->Fields("pkid");
$rs_list_selected_fabric = GetRecordSet($str_query_select);
if($rs_list_selected_fabric->Count() > 0)
{
	while(!$rs_list_selected_fabric->EOF())
        {
        	array_push($str_fabric, $rs_list_selected_fabric->Fields("masterpkid"));
                $rs_list_selected_fabric->MoveNext();
	}
}

$str_query_select = "";
$str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_OCCASION." WHERE itempkid=".$rs_list->Fields("pkid");
$rs_list_selected_occasion = GetRecordSet($str_query_select);
if($rs_list_selected_occasion->Count() > 0)
{
	while(!$rs_list_selected_occasion->EOF())
        {
        	array_push($str_occasion, $rs_list_selected_occasion->Fields("masterpkid"));
                $rs_list_selected_occasion->MoveNext();
	}
}

$str_availability = $rs_list->Fields("availability_info");
$str_availability_status = $rs_list->Fields("availability");
$int_domestic_shipping_value = $rs_list->Fields("dshippingvalue");
$str_domestic_shipping_info = $rs_list->Fields("dshippinginfo");
$int_international_shipping_value = $rs_list->Fields("ishippingvalue");
$str_international_shipping_info = $rs_list->Fields("ishippinginfo");
$int_weight = $rs_list->Fields("weight");
$str_seo_title = $rs_list->Fields("seotitle");
$str_seo_keywords = $rs_list->Fields("seokeyword");
$str_seo_desc = $rs_list->Fields("seodescription");
$str_new = $rs_list->Fields("displayasnew");
$str_hot = $rs_list->Fields("displayashot");
$str_featured = $rs_list->Fields("displayasfeatured");
$str_visible = $rs_list->Fields("visible");

#----------------------------------------------------------------------------------------------------
#Create String For Redirect
$str_redirect = "";
$str_redirect .= "catpkid=". urlencode(RemoveQuote($int_catpkid));
$str_redirect .= "&subcatpkid=". urlencode(RemoveQuote($int_subcatpkid));
$str_redirect .= "&desc=". urlencode(RemoveQuote($str_desc));
$str_redirect .= "&itemcode=". urlencode(RemoveQuote($str_itemcode));
$str_redirect .= "&title=". urlencode(RemoveQuote($str_title));

//$str_redirect .= "&color=". urlencode(RemoveQuote($str_color));
//$str_redirect .= "&size=". urlencode(RemoveQuote($str_size));
///$str_redirect .= "&ptype=". urlencode(RemoveQuote($str_type));

$str_redirect .= "&listprice=". urlencode($int_listprice);
$str_redirect .= "&ourprice=". urlencode($int_ourprice);
//$str_redirect .= "&memberprice=". urlencode($int_memberprice);
$str_redirect .= "&dshippingval=". urlencode($int_domestic_shipping_value);
$str_redirect .= "&dshippinginfo=". urlencode(RemoveQuote($str_domestic_shipping_info));
$str_redirect .= "&ishippingval=". urlencode($int_international_shipping_value);
$str_redirect .= "&ishippinginfo=". urlencode(RemoveQuote($str_international_shipping_info));
$str_redirect .= "&weight=". urlencode(RemoveQuote($int_weight));
$str_redirect .= "&new=". urlencode(RemoveQuote($str_new));
$str_redirect .= "&hot=". urlencode(RemoveQuote($str_hot));
$str_redirect .= "&featured=". urlencode(RemoveQuote($str_featured));
$str_redirect .= "&visible=". urlencode(RemoveQuote($str_visible));
$str_redirect .= "&seotitle=". urlencode(RemoveQuote($str_seo_title));
$str_redirect .= "&seokeyword=". urlencode(RemoveQuote($str_seo_keywords));
$str_redirect .= "&seodescription=". urlencode(RemoveQuote($str_seo_desc));
//print $str_redirect."<br/>";exit;
#--------------------------------------------------------------------------------------------------------------------------------
# Get Category ID
$str_query_select = "";
$str_query_select = "SELECT catpkid,subcattitle FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid=".$int_subcatpkid;
$rs_list_subcat = GetRecordset($str_query_select);
if ($rs_list_subcat->EOF())
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&".$str_redirect);
    exit();

    /*$response['status']='ERR';
    $response['IFE']= "SM";
    $response['redirect_string'] = $str_redirect;
    echo json_encode($response); 
    return;*/
}

$int_catpkid = $rs_list_subcat->fields("catpkid");

#--------------------------------------------------------------------------------------------------------------------------------
# query string datda to forward
//$str_querystring  = "&mid=".urlencode(RemoveQuote($int_mid));
$str_querystring  = "";
$str_querystring = $str_querystring . "&subcatpkid=".$int_subcatpkid."&t_title=".urlencode(RemoveQuote($str_title)). "&desc=".urlencode(RemoveQuote($str_desc));
//$str_querystring = $str_querystring ."&disnew=".urlencode(RemoveQuote($str_new))."&dishot=".urlencode(RemoveQuote($str_hot))."&purchase=".urlencode(RemoveQuote($str_purchase));
//$str_querystring = $str_querystring ."&rmonth=".urlencode(RemoveQuote($str_month))."&ryear=".urlencode(RemoveQuote($str_year))."&pricepkid=".urlencode(RemoveQuote($int_pricepkid))."&nophoto".urlencode(RemoveQuote($str_nophoto));
#---------------------------------------------------------------------------------------------------------------------------------
# check for null data
if($int_subcatpkid == 0 || $str_itemcode == "" || $str_title == ""  || $int_listprice == 0 || $int_listprice == "")
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&".$str_redirect);
    exit();
    
    /*$response['status']='ERR';
    $response['IFE']= "SM";
    $response['redirect_string'] = $str_redirect;
    echo json_encode($response); 
    return;*/
}
if($int_ourprice == "" || $int_ourprice == 0)
{
    $int_ourprice = $int_listprice;
}
//print_r($_POST);exit;
//print $int_subcatpkid; exit;
	
#Check File Extension whether it is valid extension or not
/*if($str_video_file!="")
{
    if(CheckFileExtension($str_video_file,$STR_VIDEO_FILE_TYPE_VALIDATION)==0)
    {
        $response['status']='ERR';
        $response['IFE']= "IF";
        echo json_encode($response); 
        return;
    }
}*/

#-----------------------------------------------------------------------------------------------------------------
#Check Approve Date
/*if($str_purl!="")
{
    if(validateURL02($str_purl)==false)
    {
        $response['status']='ERR';
        $response['IFE']= "IU";
        echo json_encode($response); 
        return;
    }
}*/	
#-----------------------------------------------------------------------------------------------------------------	
# Duplication Check
$str_query_select = "SELECT count(subcatpkid) noofrecords FROM ".$STR_DB_TABLE_NAME." WHERE title='" . ReplaceQuote($str_title) . "' AND subcatpkid=".$int_subcatpkid;
$rs_list_check_duplicate = GetRecordset($str_query_select);
if ($rs_list_check_duplicate->fields("noofrecords") > 0)
{
    //CloseConnection();
    //Redirect("item_list.php?msg=DU&type=E&".$str_redirect);
    //exit();
}

$int_max_displayorder = 0;
$int_max_displayorder = GetSubcatMaxValue($STR_DB_TABLE_NAME,"subcatpkid",$int_subcatpkid,"displayorder");
	
$str_create_datetime = "";
$str_create_datetime = date("Y-m-d H:i:s");
#-----------------------------------------------------------------------------------------------------------------
/*
#	Set to allow purchase YES If admin set price for a phoroset otherwise it set NO
if($int_pricepkid!=0 && $int_pricepkid!="")
{
	$str_purchase="YES";	
}*/
$str_query_select = "SELECT displayasadditionalinfo FROM ".$STR_DB_TABLE_NAME;
$rs_list_info = GetRecordset($str_query_select);
//$str_additional_info = $rs_list_info->Fields("displayasadditionalinfo")
#-----------------------------------------------------------------------------------------------------------------
#Insert Query
$str_query_insert = "";
$str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME."(catpkid,subcatpkid,itemcode,title, description,createdatetime, displayasnew,displayashot ,displayasfeatured, displayorder, visible, listprice, ourprice, memberprice, inmonth, inyear, availability, availability_info, approved, dshippingvalue,";
$str_query_insert.= "dshippinginfo, ishippingvalue, ishippinginfo, weight, seotitle, seokeyword, seodescription) VALUES (";
$str_query_insert.= "".$int_catpkid.", ";
$str_query_insert.= "".$int_subcatpkid.", ";
$str_query_insert.= "'".ReplaceQuote($str_itemcode)."', ";
$str_query_insert.= "'".ReplaceQuote($str_title)."', ";
$str_query_insert.= "'".ReplaceQuote($str_desc)."', ";
$str_query_insert.= "'".$str_create_datetime."', ";
$str_query_insert.= "'".ReplaceQuote($str_new)."', ";
$str_query_insert.= "'".ReplaceQuote($str_hot)."', ";
$str_query_insert.= "'".ReplaceQuote($str_featured)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_additional_info)."', ";
$str_query_insert.= "".$int_max_displayorder.", ";
$str_query_insert.= "'".ReplaceQuote($str_visible)."', ";
$str_query_insert.= "".$int_listprice.", ";
$str_query_insert.= "".$int_ourprice.", ";
$str_query_insert.= "".$int_memberprice.", ";
$str_query_insert.= "".date("m").", ";
$str_query_insert.= "".date("Y").", ";
//$str_query_insert.= "'".ReplaceQuote($str_size)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_color)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_type)."', ";

//$str_query_insert.= "'".ReplaceQuote($str_length)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_work)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_occasion)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_shipping_type)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_fabric)."', ";
//$str_query_insert.= "".$int_dressmaterialprice.", ";
//$str_query_insert.= "".$int_semistitchedprice.", ";
//$str_query_insert.= "".$int_customizedprice.", ";
$str_query_insert.= "'".ReplaceQuote($str_availability_status)."', ";
$str_query_insert.= "'".ReplaceQuote($str_availability)."', ";
$str_query_insert.= "'YES', ";
$str_query_insert.= "".$int_domestic_shipping_value.", ";
$str_query_insert.= "'".ReplaceQuote($str_domestic_shipping_info)."', ";
$str_query_insert.= "".$int_international_shipping_value.", ";
$str_query_insert.= "'".ReplaceQuote($str_international_shipping_info)."', ";
$str_query_insert.= "".$int_weight.", ";
$str_query_insert.= "'".ReplaceQuote($str_seo_title)."', ";
$str_query_insert.= "'".ReplaceQuote($str_seo_keywords)."', ";
$str_query_insert.= "'".ReplaceQuote($str_seo_desc)."') ";
//print $str_query_insert; exit;
ExecuteQuery($str_query_insert);
#--------------------------------------------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "SELECT pkid FROM ".$STR_DB_TABLE_NAME." WHERE title='". ReplaceQuote($str_title) ."' AND catpkid=".ReplaceQuote($int_catpkid)." AND subcatpkid=".ReplaceQuote($int_subcatpkid)."";
//print $str_query_select;exit;
$rs_list_pkid = GetRecordSet($str_query_select);	
if($rs_list_pkid->EOF()==true)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&".$str_redirect);
    exit();
    
    /*$response['status']='ERR';
    $response['IFE']= "SM";
    echo json_encode($response); 
    return;*/
}
$int_itempkid = $rs_list_pkid->fields("pkid");

#-----------------------------------------------------------------------------------------------------------------
## Color
if(Count($str_color) > 0)
{
    foreach($str_color AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_COLOR."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}

## Size
if(Count($str_size) > 0)
{
    foreach($str_size AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_SIZE."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}

## Type
if(Count($str_type) > 0)
{
    foreach($str_type AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_TYPE."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}

## LENGTH
if(Count($str_length) > 0)
{
    foreach($str_length AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_LENGTH."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}

## OCCASION
if(Count($str_occasion) > 0)
{
    foreach($str_occasion AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_OCCASION."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}

## WORK
if(Count($str_work) > 0)
{
    foreach($str_work AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_WORK."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}

## FABRIC
if(Count($str_fabric) > 0)
{
    foreach($str_fabric AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_FABRIC."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}

## SHIPPING
/*if(Count($str_shipping_type) > 0)
{
    foreach($str_shipping_type AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_SHIPPING."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}*/


#-----------------------------------------------------------------------------------------------------------------
# Upload image
//CreateDirectory($UPLOAD_IMG_PATH.$int_itempkid);

# UPLOAD FILE ON THE SERVER
/*$str_file_name="dfile_".$int_itempkid."_".get_image_text(5).".".$str_file_ext;

$str_video_file_path = "";
$str_video_file_path = $UPLOAD_FILE_PATH."/".$str_file_name;
//print $str_photoset_video_file_path;
//exit;
if($str_video_file!="")
{
    UploadFile($_FILES['video_clip']['tmp_name'],$str_video_file_path);
}

###	Rename Photoset Filename use photosetpkid
//$str_photoset_name="dfile_".$int_itempkid."_".get_image_text(5).".".$str_file_ext;
$str_update_query="UPDATE t_store SET filename='".ReplaceQuote($str_name)."' WHERE pkid=".$int_itempkid;
ExecuteQuery($str_update_query);
*/
#-----------------------------------------------------------------------------------------------------------------------
# write to xml file
//WriteXML($int_subcatpkid);
#-----------------------------------------------------------------------------------------------------------------
#Email Notification to admin from model
/*$str_fp = openXMLfile($STR_XML_FILE_PATH_CMS."siteconfiguration.xml");
$str_from = $str_user_email;
$str_to=getTagValue($STR_FROM_DEFAULT,$str_fp);
closeXMLfile($str_fp);

$str_query_select = "SELECT shortenurlkey FROM t_user WHERE pkid=".$_SESSION["userpkid"]."";
$rs_list = GetRecordSet($str_query_select);
if(!$rs_list->eof())
{
    $str_subject="New Brag is submitted by ".$rs_list->fields("shortenurlkey")." on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
    $mailbody="New Brag is submitted. Below are details.<br/><br/>";
    $mailbody.="<strong>Uploaded By:</strong> ".$rs_list->fields("shortenurlkey")."<br/>";
    $mailbody.="<strong>Category Name:</strong> ".$str_subcattitle." ";
    $mailbody.="<strong>Brag Name:</strong> ".$str_title." ";

    $mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL."";
//	print "<br/>".$str_from; print "<br/>".$str_to; print "<br/>".$mailbody;print "<br>".$str_subject; exit;
    sendmail($str_to,$str_subject,$mailbody,$str_from,1);
}*/
#-----------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=IDUP&type=S&".$str_redirect);
exit();
    
/*$response['status'] = 'SUC';
$response['IFE'] =  "SA";
$response['mid'] = $int_subcatpkid;
echo json_encode($response); 
return;*/	
#-----------------------------------------------------------------------------------------------------------------
?>
