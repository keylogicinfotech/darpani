/*
	Module Name:- Model Search Comment
	File Name  :- tm_list.js
	Create Date:- 26-APr-2007
	Intially Create By :- 
	Update History:
*/

function checksuball()
{
    var counter,i;
    with(document.frm_list)
    {
        counter=hdn_counter.value;
        if (chk_suball.checked==true)
        {
            for (i=1;i<counter;i++)
            {
                eval("chk_sub" + i).checked=true;
            }
        }
        else
        {
            for (i=1;i<counter;i++)
            {
                eval("chk_sub" + i).checked=false;
            }
        }
    }
}

function checksub(chk)
{
	if (chk.checked==false)
	{
		document.frm_list.chk_suball.checked=false;
	}
	else
	{
		var i,count,flag;
		flag=0;
		count=document.frm_list.hdn_counter.value;
		for(i=1;i<count;i++)
		{
			if (eval("document.frm_list.chk_sub" + i).checked==true)
			{
				flag++;
			}
		}
		if (flag==count-1)
		{
			document.frm_list.chk_suball.checked=true;
		}
	}
}


function frm_check_delete_multiple()
{
	var counter,i,cnt;
	with(document.frm_list)
	{
		counter=hdn_counter.value;
		cnt=0;

		for (i=1;i<counter;i++)
		{
			if (eval("chk_sub" + i).checked==true)
			{
				cnt++;
			}
		}

		if (cnt==0)
		{
			alert("Please select atleast one comment to delete");
			return false;
		}
		else	
		{
			if(confirm("Are you sure you want to delete selected Comment(s)?"))
			{
				if(confirm("Confirm: Click 'Ok' to Delete Comment(s) and 'Cancel' to cancel."))
				{
					return true;
				}
			}
			return false;
		}
		return false;
	}
}




function frm_test_list_check_displayorder()
{
	with(document.frm_test_list)
	{
		cnt=hdn_counter.value;
		for(i=1;i<cnt;i++)
		{
			if(isEmpty(eval("txt_displayorder" + i).value))
			{
				alert("Please enter display order.");
				eval("txt_displayorder" + i).focus();
				return false;
			}
			if(isNaN(eval("txt_displayorder" + i).value))
			{
				alert("Please enter numeric value for display order.");
				eval("txt_displayorder" + i).select();
				eval("txt_displayorder" + i).focus();
				return false;
			}
			if(eval("txt_displayorder" + i).value<0)
			{
				alert("Please enter positive integer for display order.");
				eval("txt_displayorder" + i).select();
				eval("txt_displayorder" + i).focus();
				return false;
			}
		}
	}
	return true;
}

function frm_list_confirmdelete()
{
	if(confirm("Are you sure you want to delete this Comment."))
	{
			if(confirm("Confirm Deletion:Click 'OK' to delete this Comment OR 'Cancel' to deletion."))
			{
					return true;
			}
	}
	return false;
}


function frm_test_add_validateform()
{
	with(document.frm_test_add)
	{
		if(cbo_modelpkid.value==0)
		{
				alert("Please Select model to Post A Comment.");
				cbo_modelpkid.focus();
				return false;
		}
		if(isEmpty(txt_pname.value))
		{
				alert("Please enter Posted By Name.");
				txt_pname.select();
				txt_pname.focus();
				return false;
		}
		if(isEmpty(txt_desgn.value))
		{
				alert("Please enter Alias.");
				txt_desgn.select();
				txt_desgn.focus();
				return false;
		}
		
	    if(trim(txt_email.value)!="")
	    {
			if(!sValidateMailAddress(txt_email.value))
			{
				alert("Please enter valid email address. Format:email@domain.com");
				txt_email.select();
				txt_email.focus();
				return false;
			}
	   }
	   if(trim(txt_loc.value)!="")
	   {
	  	 if(txt_loc.value.length>255)
	   	 {
			alert("Location should contain less than 256 characters.");
			txt_loc.select();
			txt_loc.focus();
			return false;		   	
	   	 }
	   }
	   if(trim(fileimage.value) != "")
		{
			if(checkExt(trim(fileimage.value))==false)
			{
				fileimage.select();
				fileimage.focus();
				return false;
			}
		}
		if(trim(txt_url.value)!="")
		{
			if(!isValidUrl(trim(txt_url.value)))
			{
				txt_url.select();
				txt_url.focus();
				return false;
			}
		}
	}
	return true;
}


function show_details(url)
{
	window.open(url,'comments','left=50,top=20,scrollbars=yes,width=570,height=520,resizable=yes');
	return false;
}

function frmAdd_Validate(no)
{
	if(no==1)
	{
		document.frm_test_add.hdnSubmit.value=1;
	}
	else 
	{
		document.frm_test_add.hdnSubmit.value=2;
	}
	return true;
}
