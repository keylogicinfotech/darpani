<?php
/*
Module Name:- modstore
File Name  :- item_color_edit.php
Create Date:- 21-JAN-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_datetimeyear.php";
#----------------------------------------------------------------------------------------------------------------------------
#get Query String Data
$int_pkid = "";
if(isset($_GET["pkid"]) == true)
{
    $int_pkid = trim($_GET["pkid"]);
}	
if($int_pkid <= 0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_color_list.php?msg=F&type=E&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_COLOR." WHERE pkid=". $int_pkid;
$rs_list = GetRecordSet($str_query_select);
if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_color_list.php?msg=F&type=E&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_UPDATE; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_md; break;
        case("DU"): $str_message = $STR_MSG_ACTION_COLOR_TITLE_ALREADY_EXIST; break;
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE_COLOR);?> : <?php print($STR_TITLE_EDIT); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container content_bg">
    <?php include("../../includes/adminheader.php"); ?>
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <a href="item_color_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE_COLOR);?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_COLOR);?></a>
            </div>
        </div>
        <div class="col-md-9 col-sm-6 col-xs-12 " align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE_COLOR);?></h3></div>
    </div><hr/> 
    <?php if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); }	?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
            	<div class="panel panel-default">
                    <div class="panel-heading">
                    	<h4 class="panel-title">
                            <div class="row padding-10">
                                <div class="col-md-6 col-sm-6 col-xs-8"><i class="glyphicon  glyphicon-edit "></i>&nbsp;&nbsp;<b><?php print($STR_TITLE_EDIT); ?> </a></b></div>
                                <div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>                                                                  </div>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse">
			<div class="panel-body">
                            <form name="frm_edit" action="item_color_edit_p.php" method="post" onSubmit="return frm_edit_validate();" enctype="multipart/form-data">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>

                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <span class="text-help-form"> *</span><input name="txt_title" type="text" tabindex="1" class="form-control input-sm" id="" maxlength="255" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" value="<?php print ($rs_list->Fields("title")); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Hex Color Code</label>
                                            <span class="text-help-form"> *</span>
                                            <div id="cp2" class="input-group colorpicker-component"> 
                                                <input name="txt_cmykcode" type="text" tabindex="2" class="form-control input-sm" id="txt_cmykcode" maxlength="255" placeholder="<?php print $STR_PLACEHOLDER_COLOR_CODE; ?>" value="<?php print ($rs_list->Fields("cmyk_code")); ?>">
                                                <span class="input-group-addon"><i></i></span>
                                            </div>                                            
                                        </div>
                                    </div>
                                    <?php /* ?><div class="col-md-4">
                                        <div class="form-group">
                                            <label>RGB Code</label>
                                           <span class="text-help"> *</span><input name="txt_rgbcode" type="text" tabindex="1" class="form-control input-sm" id="" maxlength="255" placeholder="Enter RGB code" value="<?php print ($rs_list->Fields("rgb_code")); ?>">
                                        </div>
                                    </div><?php */ ?>
                                </div>
                                <input type="hidden" name="hdn_pkid" value="<?php print($int_pkid);?>">
                                <?php print DisplayFormButton("EDIT",3); ?><?php print DisplayFormButton("RESET",4); ?>
                            </form>
			</div>
                    </div>
		</div>
            </div>
	</div>
    </div>
    <a name="help"></a>
    <?php include "../../includes/help_for_edit.php"; ?>
</div>

<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/bootstrap-colorpicker.css" rel="stylesheet">
<link href="../../css/admin.css" rel="stylesheet">
<script language="JavaScript" src="../../js/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="item_color_edit.js" type="text/javascript"></script>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/bootstrap-colorpicker.js"></script>
<script type="text/javascript">
$('#cp2').colorpicker();
</script>
</body>
</html>
