<?php
/*
Module Name:- modstore
File Name  :- purchase_list.php
Create Date:- 02-MAR-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_datetimeyear.php";
include "./item_app_specific.php";
#------------------------------------------------------------------------------------------------
$int_month=date("m");
if(isset($_GET['cbo_month']))
{ $int_month=trim($_GET['cbo_month']); }

$int_year=date("Y");
if(isset($_GET['cbo_year']))
{ $int_year=trim($_GET['cbo_year']); }

$int_user_pkid=0;
if(isset($_GET['cbo_model']))
{ $int_user_pkid=trim($_GET['cbo_model']); }

if($int_month==0 || $int_month=="")
{ $int_month=date("m"); }

if($int_year==0 || $int_year=="")
{ $int_year=date("Y"); }

$int_countrypkid = 82; //Default pkid of 
if(isset($_GET["countrypkid"]))
{
    $int_countrypkid = trim($_GET["countrypkid"]);
}

//print uesr_pkid."<br/>".$int_month."<br/>".$int_year."<br/>";

$str_filtercriteria="";
if($int_user_pkid > 0 && $int_month == 0 && $int_year == 0 ) 
{
    $str_filtercriteria=" WHERE userpkid=".$int_user_pkid. " AND inmonth=".date("m")." AND inyear=".date("Y");
}
else if($int_user_pkid > 0 && $int_month != 0 && $int_year == 0 ) 
{
    $str_filtercriteria=" WHERE userpkid=".$int_user_pkid. " AND inmonth=".$int_month." AND inyear=".date("Y");
}
else if($int_user_pkid > 0 && $int_month == 0 && $int_year != 0 ) 
{
    $str_filtercriteria=" WHERE userpkid=".$int_user_pkid. " AND inmonth=".date("m")." AND inyear=".$int_year;
}
else if($int_user_pkid > 0 && $int_month != 0 && $int_year != 0 ) 
{
    $str_filtercriteria=" WHERE userpkid=".$int_user_pkid. " AND inmonth=".$int_month." AND inyear=".$int_year;
}
else if($int_user_pkid == 0 && $int_month != 0 && $int_year != 0 ) 
{
    $str_filtercriteria=" WHERE inmonth=".$int_month." AND inyear=".$int_year;
}
else if($int_user_pkid == 0 && $int_month != 0 && $int_year == 0 ) 
{
    $str_filtercriteria=" WHERE inmonth=".$int_month." AND inyear=".date("Y");
}
else if($int_user_pkid == 0 && $int_month == 0 && $int_year != 0 ) 
{
    $str_filtercriteria=" WHERE inmonth=".date("m")." AND inyear=".$int_year;
}
else if($int_user_pkid == 0 && $int_month == 0 && $int_year == 0 ) 
{
    $str_filtercriteria=" WHERE inmonth=".date("m")." AND inyear=".date("Y");
}


// Get model name to display on report
$str_username = "";
/*if($int_user_pkid > 0)
{
    $str_query_select = "SELECT shortenurlkey FROM t_user WHERE pkid=".$int_user_pkid;
    $rs_username=GetRecordset($str_query_select);
    $str_username = $rs_username->fields("shortenurlkey")." : ";
}
else
{ $str_username = ""; }*/

#------------------------------------------------------------------------------------------------
// Below query will group items by purchase date time. So it will do SUM SUM(extendedprice) as gross_price of all items in single order 
// and will display it. 
// GROUP_CONTACT(CONCAT will display details of each item in single order using this GROUP_CONCAT(CONCAT(producttitle, ' | ', price, '<br/>')) as item_details
// Here | is separator of different values of single item.
// Here <br/> is separator among different items in single order
// Below query is working but we are not using s15-AUG-2014
$str_query_select = "SELECT purchasedatetime, MAX(purchasepkid) AS purchasepkid, MAX(productpkid) AS productpkid,MAX(memberpkid) AS memberpkid,MAX(emailid) AS emailid,MAX(subscriptionid) AS subscriptionid,MAX(ccbillmemberloginid) AS ccbillmemberloginid,MAX(ccbillmemberpassword) AS ccbillmemberpassword,MAX(price) AS price,MAX(firstname) AS firstname, MAX(lastname) AS lastname, MAX(address) AS address, MAX(city) AS city, MAX(state) AS state, MAX(country) AS country, MAX(zipcode) AS zipcode, MAX(ipaddress) AS ipaddress,MAX(phoneno) AS phoneno,MAX(expiredate) AS expiredate,MAX(allowdownload) AS allowdownload,MAX(addedbyccbill) AS addedbyccbill,MAX(pricevalue) AS pricevalue,MAX(producttitle) AS producttitle,MAX(subcattitle) AS subcattitle,MAX(membername) AS membername,MAX(noofdownload) AS noofdownload,MAX(userpkid) AS userpkid, MAX(username) AS username, MAX(catpkid) AS catpkid, MAX(subcatpkid) AS subcatpkid, MAX(pricepkid) AS pricepkid,MAX(shippinginfo) AS shippinginfo,MAX(dshippingvalue) AS dshippingvalue,MAX(ishippingvalue) AS ishippingvalue, MAX(shippingaddress) AS shippingaddress,MAX(color) AS color, MAX(size) AS size,MAX(type) AS type, MAX(finaldiscountedamount) AS finaldiscountedamount, SUM(extendedprice)as gross_price, SUM(dshippingvalue) AS gross_shippping, MAX(shippingstatus) AS shippingstatus, MAX(paymentoptionpkid) AS paymentoptionpkid, MAX(paymentoptiontitle) AS paymentoptiontitle, MAX(giftcard) AS giftcard";
//$str_query_select .=", GROUP_CONCAT(CONCAT(producttitle , '|' , price , '|' , quantity , '|' , extendedprice , '|' , color , '|' , size , '|' , typemodel , '|' , emailid , '|' , subscriptionid , '|' , shippingaddress , '|' , firstname , '|' , lastname , '|' , address , '|' , city , '|' , state , '|' , country , '|' , zipcode , '|' , specialnote , '|' , phoneno ) SEPARATOR '::') as item_details "; 
$str_query_select .= " FROM t_store_purchase ";
$str_query_select .=" ".$str_filtercriteria. " AND countrypkid=".$int_countrypkid.""; 
$str_query_select .= " GROUP BY purchasedatetime ORDER BY purchasedatetime DESC";

//print $str_query_select;exit;
$rs_list=GetRecordSet($str_query_select);
//print $rs_list->Fields("userpkid");exit;



//producttitle, productcattitle, pricepkid, pricevalue, price, extendedprice, shipping, shippingvalue, discountpercentage, discountamount, finaldiscountedamount, quantity, color, size, typemodel, freememberpkid, freemembername, emailid, subscriptionid, ccbillmemberloginid, ccbillmemberpassword, firstname, lastname, address, city, state, country, zipcode, shippingaddress, specialnote, ipaddress, purchaseday, photosetinmonth, photosetinyear, initialprice, initialperiod, recurringprice, recurringperiod, numrebills, billstatus, payoptpkid, pay_opt_title, phoneno, expiredate, allowdownload, addedbyccbill, noofdownload, orderstatus, orderstatusnote, res_code, res_code_msg, authcode, transid, transhash ORDER BY purchasedatetime DESC";

//$str_query_select .= " GROUP BY purchasedatetime, purchasepkid, userpkid, photosetpkid, subcatpkid, pricepkid, price, dshippingvalue,ishippingvalue,shippinginfo, color, size, type, freememberpkid, emailid, subscriptionid, ccbillmemberloginid, ccbillmemberpassword, firstname, lastname, address, city, state, country, zipcode, ipaddress,  photosetinmonth, phoneno,photosetinyear ORDER BY purchasedatetime DESC";
//


#------------------------------------------------------------------------------------------------
	$str_type = "";
	$str_message = "";
	$str_visible_value="";

	if(isset($_GET['mode']))
	{
		$str_mode=trim($_GET['mode']);
	}
#	Get message type.
   		if(isset($_GET["type"]))
		{
			switch(trim($_GET["type"]))
			{
				case("S"): $str_type = "S"; break;
				case("E"): $str_type = "E"; break;
				case("W"): $str_type = "W"; break;
			}
		}
#	Get message text.
		if(isset($_GET["msg"]))
		{
			switch(trim($_GET["msg"]))
			{
				case("F"):
					$str_message = "Some information(s) missing. Please try again.";
					break;
				case("A"):
					$str_message = "Product Purchase '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' details added successfully.";
					break;
				case("U"):
					$str_message = "Purchase Product expire dete updated successfully.";
					break;
				case("D"):
					$str_message = "Product Purchase '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' details deleted successfully.";
					break;
				case("V"):
					$str_message = "Product Purchase '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' login mode changed to '". $str_mode . "' successfully.";
					break;
			}
		}
#------------------------------------------------------------------------------------------------
?><!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_REPORT_MONTHLY);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> 
<a name="ptop" id="ptop"></a>
<div class="container">
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-6 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <a href="./item_order_status_list.php" class="btn btn-default" title="<?php print($STR_TITLE_PAGE_ORDER_STATUS);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_ORDER_STATUS);?></a>
                <a href="./item_report_transaction_yearly.php" class="btn btn-default" title="<?php print($STR_TITLE_REPORT_YEARLY);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_REPORT_YEARLY);?></a>
            </div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_REPORT_MONTHLY);?>&nbsp;[<?php print(MonthName($int_month));?>&nbsp;-&nbsp;<?php print($int_year);?>] </h3></div>
    </div><hr> 
    <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); } ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="row padding-10">
                            <div class="col-md-6 col-sm-6 col-xs-8"><strong><?php print($STR_TITLE_FILTER_CRITERIA);?></strong></div>
                            <div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>		
                        </div>
                    </h4>
                </div>
                <div class="panel-body text-center" >
                    <form name="frm_filter" method="GET" action="./item_report_transaction_monthly.php" >
                        <label>Select&nbsp;&nbsp;</label>
                                
                                <?php
                                $str_query_select = "";
                                $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_COUNTRY." WHERE visible='YES'";
                                $rs_list_country = GetRecordSet($str_query_select);
                                ?>
                                <label>
                                    <select name="countrypkid" id="countrypkid" class="form-control input-sm">
                                        
                                        <?php
                                        while(!$rs_list_country->EOF())
                                        { ?>
                                        <option value="<?php print $rs_list_country->Fields("pkid");  ?>" <?php print CheckSelected($rs_list_country->Fields("pkid"), $int_countrypkid) ?>><?php print $rs_list_country->Fields("title");  ?></option>
                                            <?php 
                                            $rs_list_country->MoveNext();
                                        }?>
                                    </select>
                                </label>
                                <label>&nbsp;&nbsp;&&nbsp;&nbsp;</label>
                           <?php /* ?><label>
                                <?php 
                                $str_query_seller="SELECT DISTINCT(p.userpkid), m.shortenurlkey";
                                $str_query_seller.=" FROM t_store_purchase p ";
                                $str_query_seller.=" LEFT JOIN t_user m ON m.pkid=p.userpkid ";
                                //$str_query_seller.=" WHERE photosetinmonth=".$int_month." AND photosetinyear=".$int_year;
                                $str_query_seller.=" ORDER BY shortenurlkey ASC";
                                //print $str_query_seller;exit;
                                $rs_seller=GetRecordSet($str_query_seller); ?>
                                <select name="cbo_model" id="cbo_model" class="form-control input-sm">
                                    <option value="0" ><?php print $STR_TITLE_DROPDOWN; ?></option>
                                    <?php 
                                    $int_prev_catpkid=0;
                                    while(!$rs_seller->eof())
                                    { ?>
                                        <option value="<?php print($rs_seller->fields("userpkid")); ?>" <?php print(CheckSelected($rs_seller->fields("userpkid"),$int_user_pkid)); ?> ><?php print($rs_seller->fields("shortenurlkey")); ?></option>
                                    <?php
                                    $rs_seller->movenext();
                                    }
                                    ?>
                                </select>
                            </label><?php */ ?>
                            <label>Month&nbsp;&nbsp;</label>
                            <label><?php print(DisplayMonth($int_month)); ?></label>
                            <label>&nbsp;&nbsp;& year&nbsp;&nbsp;</label>
                            <label><?php print(DisplayYear($int_year)); ?></label>
                            &nbsp;&nbsp;
                            <label><?php print DisplayFormButton('VIEWREPORT', 0);?></label>
                    </form>                    
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="table-responsive">
                <form name="frm_list" action="./action_page.php" method="post" onSubmit="javascript: validate_submit_fomrm();"   >
                    <table class="table table-striped table-bordered ">
                        <thead>
                            <tr>
                                <th width="4%">Sr. #</th>
                                <th width="10%">Transaction Date</th>
                                <th width="">Details</th>
                                <?php /* ?><th width="10%">Price<br><span class="text-help">(in US$)</span></th>	
                                <th width="10%">Admin <br/>Amount<br><span class="text-help">(in US$)</span></th>	
                                <th width="10%">(-) Processing Fees<br><span class="text-help">(in US$)</span></th>	
                                <th width="10%">(-) .55 Deduction<br><span class="text-help">(in US$)</span></th><?php */ ?>
                                <th width="12%">Total <br/>Amount<br>
                                    <?php $str_query_select = "";
                                    $str_query_select = "SELECT currency_symbol FROM ".$STR_DB_TABLE_NAME_COUNTRY." WHERE pkid=".$int_countrypkid; 
                                    $rs_list_currency_symbol = GetRecordSet($str_query_select);
                                    
                                    ?>
                                    <span class="text-help">(in <?php print $rs_list_currency_symbol->Fields("currency_symbol"); ?>)</span>
                                </th>	
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($rs_list->EOF()==true)  {  ?>
                                        <tr><td colspan="7" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                                    <?php }
                                    else {

                                    $int_total_amount=0;
                                    $int_total_shipping_charge=0;
                                    $int_total_promo_code=0;
                                    $int_total_wholesaler_amount = 0;
                                    $int_total_giftcard_amount = 0;
                                    $int_total_refund_amount = 0;
                                    $int_total_gross_amount = 0;
                                    $int_cnt=1;
                                    while(!$rs_list->EOF()==true) { ?> 
                                    <tr>
                                        <td align="center" class="text-align"><?php print($int_cnt);?></td>
                                        <td align="center" >
                                            <i class="text-help"><?php print(date("d-M-Y h:i:s A",strtotime($rs_list->fields("purchasedatetime"))));?></i>
                                        </td>
                                        <td align="left" style="vertical-align:text-top">
                                        <?php /*$sel_mdl_details = ""; 
                                        $sel_mdl_details = "SELECT shortenurlkey FROM t_user WHERE pkid = ".$rs_list->fields("userpkid"); 
                                        $rs_details = GetRecordSet($sel_mdl_details);
                                        ?>  
                                        User ID: <b><a href="<?php print $STR_SITENAME_WITH_PROTOCOL."/".$rs_details->Fields("shortenurlkey"); ?>"><?php print $rs_details->Fields("shortenurlkey");?></a></b>
                                        <?php //print "<b>Transaction details of</b> <span class='Heading10ptBold'> ".$rs_list->fields("shortenurlkey")."</span><br/>"; */?>
                                        <?php 
                                        $str_query_select = "SELECT * FROM t_store_purchase ";
                                        $str_query_select .= " WHERE purchasedatetime=  '".$rs_list->fields("purchasedatetime")."'";
                                        $str_query_select .= " AND userpkid=".$rs_list->fields("userpkid");
                                        $str_query_select .= " ORDER BY purchasedatetime DESC ";
                                        //print $str_query_select; exit;
                                        $rs_item_list=GetRecordset($str_query_select);

                                        $str_emailid = "";
                                        $str_phoneno = "";
                                        $str_firstname = "";
                                        $str_lastname = "";
                                        $str_address = "";
                                        $str_zipcode = "";
                                        $str_city = "";
                                        $str_state = "";
                                        $str_country = "";
                                        $str_ipaddress = "";
                                        $str_subscriptionid = "";
                                        $int_shippingval = 0;
                                        $int_weight = 0;
                                        $int_shipping_total_value = 0;
                                        $int_prod_cnt=1;
                                        $int_actual_amount = 0;
                                        $int_wholesaler_discount_percentages = 0;
                                        $int_giftcard_amount = 0;
                                        $int_gross_amount = 0;
                                        while(!$rs_item_list->EOF()) 
                                        { 
                                            print "<span class=''>".$int_prod_cnt.".</span>  "; 
                                            print "<b><span class='text-help'>Item Info:</span></b><br/>"; 
                                            if($rs_item_list->fields("producttitle")!="") { print("<span class='text-help'>Name:</span> <span class=''>".$rs_item_list->fields("producttitle"))."</span>";}
                                            if($rs_item_list->fields("cattitle")!="") { print(" | <span class='text-help'>Category:</span> <span class=''>".$rs_item_list->fields("cattitle"))."</span>";}
                                            if($rs_item_list->fields("subcattitle")!="") { print(" | <span class='text-help'>Sub Category:</span> <span class=''>".$rs_item_list->fields("subcattitle"))."</span>";}
                                            if($rs_item_list->fields("color")!="") { print("<br/><span class='text-help'>Color:</span> <span class=''>".$rs_item_list->fields("color"))."";}
                                            if($rs_item_list->fields("size")!="") { print(" | <span class='text-help'>Size:</span> <span class=''>".$rs_item_list->fields("size"))."";}
                                            if($rs_item_list->fields("type")!="") { print(" | <span class='text-help'>Type / Model:</span> <span class=''>".$rs_item_list->fields("type"))."";}
                                            
                                            
                                            
                                            
                                            
                                            
                                            if($rs_item_list->fields("price")!="") { print(" | <span class='text-help'>Price:</span> <span class=''>".GetPriceValue($rs_item_list->fields("price")))."</span>";}
                                            if($rs_item_list->fields("tailoringprice")!="") { print(" | <span class='text-help'>Tailoring Price:</span> <span class=''>".GetPriceValue($rs_item_list->fields("tailoringprice")))."</span>";}
                                            if($rs_item_list->fields("quantity")!="") { print(" | <span class='text-help'>Qty:</span> <span class=''>".$rs_item_list->fields("quantity"))."</span> ";}
                                            $int_total = 0;
                                            $int_total = ($rs_item_list->fields("price") + $rs_item_list->fields("tailoringprice")) *  $rs_item_list->fields("quantity");
                                            if($int_total > 0) { print(" | <span class='text-help'>Total:</span> <span class='  '>".GetPriceValue($int_total))."</span>";}
                                            //if($rs_item_list->fields("extendedprice")!="") { print(" | <span class='text-help'>Total:</span> <span class='  '>".GetPriceValue($rs_item_list->fields("extendedprice")))."</span>";}
                                            if($rs_item_list->fields("tailoringoptiontitle")!="") { print("<br/><span class='text-help'>Tailoring Option:</span> <span class=''>".$rs_item_list->fields("tailoringoptiontitle"))."</span>";}
                                            if($rs_item_list->fields("tailoringmeasurements")!="") { print(" | <span class='text-help'>Measurements :</span> <span class=''>".$rs_item_list->fields("tailoringmeasurements"))."</span> ";}
                                            
                                            
                                            
                                            
                                            print "<hr/>"; 
                                            $str_emailid = $rs_item_list->fields("emailid");
                                            $str_phoneno = $rs_item_list->fields("phoneno");
                                            $str_firstname = $rs_item_list->fields("firstname");
                                            $str_lastname = $rs_item_list->fields("lastname");
                                            $str_address = $rs_item_list->fields("address");
                                            $str_zipcode = $rs_item_list->fields("zipcode");
                                            $str_city = $rs_item_list->fields("city");
                                            $str_state = $rs_item_list->fields("state");
                                            $str_country = $rs_item_list->fields("country");
                                            $str_ipaddress = $rs_item_list->fields("ipaddress");
                                            
                                            $int_shippingval = $rs_item_list->fields("shippingvalue");
                                            $int_weight = $rs_item_list->fields("weight");
                                            $int_shipping_total_value = $int_shippingval * $int_weight * $rs_item_list->fields("quantity");
                                            
                                            $int_actual_amount = $int_actual_amount + $int_total;
                                            $int_promocodepkid = $rs_item_list->fields("promocodepkid");
                                            $str_subscriptionid = $rs_item_list->fields("subscriptionid");
                                            $int_wholesaler_discount_percentages = $rs_item_list->fields("wholesalerdiscount");
                                            $int_giftcard_amount = $rs_item_list->fields("giftcard");
                                            //print $str_shippingaddress; exit;

                                            $rs_item_list->MoveNext(); 
                                            $int_prod_cnt=$int_prod_cnt+1;
                                        } 
                                            $str_shippinginfo = $rs_list->fields("shippinginfo");
                                            $str_shippingaddress = $rs_list->fields("shippingaddress");
                                            //print $str_shippingaddress;//exit;


                                            /*$sel_mem_details = ""; 
                                            $sel_mem_details = "SELECT shortenurlkey FROM t_freemember WHERE pkid = ".$rs_list->fields("memberpkid"); 
                                            $rs_mem_details = GetRecordSet($sel_mem_details); */
                                            print "<b><span class='text-help'>Payee Info:</span></b><br/>";
                                            /*if($rs_mem_details->Fields("shortenurlkey") != ""){
                                            ?>  
                                            Profile ID: <b><a href="<?php print $STR_SITE_NAME_WITH_PROTOCOL.$rs_mem_details->Fields("shortenurlkey"); ?>"><?php print $rs_mem_details->Fields("shortenurlkey");?></a></b><br/>
                                            <?php }*/
                                            if($str_subscriptionid!="") { print(" <span class='text-help'>Transaction ID:</span> <span class=''>".$str_subscriptionid)." | ";}
                                            if($str_emailid!="") { print(" <span class='text-help'>Email:</span> <span class=''>".$str_emailid)."";}
                                            if($str_phoneno!="") { print(" | <span class='text-help'>Phone:</span> <span class='HighLightText10ptNormal'>".$str_phoneno)."";}
                                            if($str_firstname!="") { print(" | <span class='text-help'>Name:</span> <span class='HighLightText10ptNormal'>".$str_firstname)."";}
                                            if($str_lastname!="") { print(" ".$str_lastname)."";}
                                            if($str_address!="") { print(" | <span class='text-help'>Address:</span> <span class='HighLightText10ptNormal'>".$str_address)."";}
                                            if($str_zipcode!="") { print(" | <span class='text-help'>Zipcode:</span> <span class='HighLightText10ptNormal'>".$str_zipcode)."";}
                                            if($str_city!="") { print(" | <span class='text-help'>City:</span> <span class='HighLightText10ptNormal'>".$str_city)."";}
                                            if($str_state!="") { print(" | <span class='text-help'>State:</span> <span class='HighLightText10ptNormal'>".$str_state)."";}
                                            if($str_country!="") { print(" | <span class='text-help'>Country:</span> <span class='HighLightText10ptNormal'>".$str_country)."";}
                                            if($str_ipaddress!="") { print(" | <span class='text-help'>IP:</span> <span class='HighLightText10ptNormal'>".$str_ipaddress)."";}
                                            /*if($str_dshippingval!="") { print(" | <span class='text-help'>Shipping Value:</span> <span class='HighLightText10ptNormal'>".$str_dshippingval)."";}
                                            if($str_ishippingval!="") { print(" | <span class='text-help'>Ishipping Value:</span> <span class='HighLightText10ptNormal'>".$str_ishippingval)."";} */
                                            if($str_shippinginfo!="") { print(" <br/> <span class='text-help'>Shipping Info:</span> <span class='HighLightText10ptNormal'>".$str_shippinginfo)."";}
                                            if($str_shippingaddress!="") { print(" <br/> <span class='text-help'>Shipping Address:</span> <span class='HighLightText10ptNormal'>".$str_shippingaddress)."";}
                                            print "<br/>";									
                                        ?>
                                        </td>

                                        <td align="right">
                                                <?php print("<span class=''><b>".number_format($int_actual_amount,2,".",",")."&nbsp;</b></span><br/>"); ?>
                                                <?php //print $int_actual_amount;?>
                                        </td>
                                    </tr>
                                    <?php if($int_shipping_total_value > 0) { ?>
                                    <tr>
                                        <td colspan="3" align="right"><b>+ Shipping Charge</b></td>
                                        <td align="right"><b><?php print(number_format($int_shipping_total_value, 2)); ?></b></td>
                                        <?php /* ?><td colspan="3"></td><?php */ ?>
                                    </tr>
                                    <?php } ?>
                                    <?php if($int_promocodepkid > 0) {
                                        $sel_qry_promocode = "";
                                        $sel_qry_promocode = "SELECT * FROM t_store_promocode WHERE pkid = ".$int_promocodepkid;
                                        //print $sel_qry_promocode;
                                        $rs_promocode_details = GetRecordSet($sel_qry_promocode);
                                        //print $rs_promocode_details->Fields("title");
                                    ?>
                                    <tr>
                                        <td colspan="3" align="right"><b>- Promo Code Discount</b>
                                        [<?php
                                            if($rs_promocode_details->Fields("title") != "") {

                                                print "Used Promo Code: ".$rs_promocode_details->Fields("title");
                                            }
                                            if($rs_promocode_details->Fields("discountamount") != "" && $rs_promocode_details->Fields("discountamount") > 0) {

                                                print " - ".$rs_promocode_details->Fields("discountamount");
                                            }
                                            if($rs_promocode_details->Fields("discountpercentage") != "" && $rs_promocode_details->Fields("discountpercentage") > 0) {

                                                print " - ".$rs_promocode_details->Fields("discountpercentage")."%";
                                            }
                                            ?>]
                                        
                                        </td>
                                        <td align="right"><b><?php print($rs_list->fields("finaldiscountedamount"));  ?></b></td>
                                    </tr>
                                    <?php } ?>
                                    <?php 
                                    $int_wholesaler_discount_amount = 0;
                                    $int_wholesaler_discount_amount = ($int_actual_amount * $int_wholesaler_discount_percentages) / 100; ?>
                                    <?php if($int_wholesaler_discount_percentages > 0) { ?>
                                    <tr>
                                        <td colspan="3" align="right"><b>- Wholesaler Discount</b></td>
                                        <td align="right"><b><?php print(number_format($int_wholesaler_discount_amount, 2)); ?></b></td>
                                        <?php /* ?><td colspan="3"></td><?php */ ?>
                                    </tr>
                                    <?php } ?>
                                    <?php if($int_giftcard_amount > 0) { ?>
                                    <tr>
                                        <td colspan="3" align="right"><b>- Gift Card Amount</b></td>
                                        <td align="right"><b><?php print(number_format($int_giftcard_amount, 2)); ?></b></td>
                                        <?php /* ?><td colspan="3"></td><?php */ ?>
                                    </tr>
                                    <?php } ?>
                                    
                                    <?php 
                                    $int_refunded_amount = 0;
                                    $str_shipping_status = "";
                                    
                                    if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION4 && $rs_list->fields("paymentoptionpkid") == 2) {
                                        if($rs_list->fields("giftcard") > 0) {
                                            //$int_refunded_amount = $rs_list->fields("giftcard");
					    $int_refunded_amount = 0;


                                            $str_shipping_status = $STR_CBO_OPTION4;
                                        } 
                                        else { 
                                            $int_refunded_amount = 0;
                                            $str_shipping_status = $STR_CBO_OPTION4;
                                        }
                                    } 
                                    else if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION4 && $rs_list->fields("paymentoptionpkid") <> 2) {
                                        if($rs_list->fields("giftcard") > 0) {
                                            $int_refunded_amount = (($int_actual_amount - $rs_list->fields("giftcard")) * $INT_REFUND_PERCENTAGE) / 100;
                                            $str_shipping_status = $STR_CBO_OPTION4;
                                            
                                        }
                                        else {
                                            $int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
                                            $str_shipping_status = $STR_CBO_OPTION4;
                                        } 
                                    }
				    else if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION5 && $rs_list->fields("paymentoptionpkid") == 2) {
                                        if($rs_list->fields("giftcard") > 0) {
                                            //$int_refunded_amount = $rs_list->fields("giftcard");
                                            $int_refunded_amount = (($int_actual_amount - $rs_list->fields("giftcard")) * $INT_REFUND_PERCENTAGE) / 100;
                                            $str_shipping_status = $STR_CBO_OPTION5;
                                        } 
                                        else { 
                                            $int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
                                            $str_shipping_status = $STR_CBO_OPTION5;
                                        }
                                    }  
                                    else if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION5 && $rs_list->fields("paymentoptionpkid") <> 2) {
                                         if($rs_list->fields("giftcard") > 0) {
                                            $int_refunded_amount = (($int_actual_amount - $rs_list->fields("giftcard")) * $INT_REFUND_PERCENTAGE) / 100;
                                            $str_shipping_status = $STR_CBO_OPTION5;
                                            
                                        }
                                        else {
                                            $int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
                                            $str_shipping_status = $STR_CBO_OPTION5;
                                        } 
                                        //$int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
                                        //$str_shipping_status = $STR_CBO_OPTION5;
                                        
                                    }
				    else {
                                         if($rs_list->fields("giftcard") > 0) {
                                            //$int_refunded_amount = (($int_actual_amount - $rs_list->fields("giftcard")) * $INT_REFUND_PERCENTAGE) / 100;
                                            //$int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
					    $int_refunded_amount = 0;
                                            //$str_shipping_status = $STR_CBO_OPTION5;
                                            
                                        }
                                        else {
                                            //$int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
					    $int_refunded_amount = 0;
                                            //$str_shipping_status = $STR_CBO_OPTION5;
                                        } 
                                        //$int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
                                        //$str_shipping_status = $STR_CBO_OPTION5;
                                        
                                    }
				    ?>
                                    <?php 
                                    if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION4 || $rs_list->fields("shippingstatus") == $STR_CBO_OPTION5) { ?>
                                    <tr>
                                        <td colspan="3" align="right"><label class="label label-danger"><?php print $str_shipping_status; ?></label> <b>- Refunded Amount</b></td>
                                        <td align="right"><b><?php print(number_format($int_refunded_amount, 2)); ?></b></td>
                                    </tr>
                                    <?php } ?>
                                    
                                    <?php
                                    // If order is cancelled and payment mode is COD
                                    if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION4  && $rs_list->fields("paymentoptionpkid") == 1) {
                                        $int_gross_amount = 0;
                                    }    
                                    // If order is cancelled and payment mode is not COD
                                    else if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION4 && $rs_list->fields("paymentoptionpkid") <> 1) {
                                        $int_gross_amount = $int_actual_amount + $int_shipping_total_value - $rs_list->fields("finaldiscountedamount") - $int_wholesaler_discount_amount - $int_giftcard_amount - $int_refunded_amount;
                                    } 
				    else if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION5  && $rs_list->fields("paymentoptionpkid") == 1) {
					$int_gross_amount = $int_actual_amount + $int_shipping_total_value - $rs_list->fields("finaldiscountedamount") - $int_wholesaler_discount_amount - $int_giftcard_amount - $int_refunded_amount;
                                    }    
                                    // If order is cancelled and payment mode is not COD
                                    else if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION5 && $rs_list->fields("paymentoptionpkid") <> 1) {
                                        $int_gross_amount = $int_actual_amount + $int_shipping_total_value - $rs_list->fields("finaldiscountedamount") - $int_wholesaler_discount_amount - $int_giftcard_amount - $int_refunded_amount;
                                    } 
				    else {
                                        $int_gross_amount = $int_actual_amount + $int_shipping_total_value - $rs_list->fields("finaldiscountedamount") - $int_wholesaler_discount_amount - $int_giftcard_amount - $int_refunded_amount;
                                    } 
                                    ?>
                                    <tr>
                                        <td colspan="3" align="right" class="text-primary">
                                            <?php print $rs_list->fields("paymentoptiontitle")." | "; ?>
                                            <b>Gross Total For This Order</b></td>
                                            <?php
                                        
                                        //$int_gross_amount = $int_actual_amount + $int_shipping_total_value - $rs_list->fields("finaldiscountedamount") - $int_wholesaler_discount_amount - $int_giftcard_amount - $int_refunded_amount; ?>
                                        <td align="right" class="text-primary"><b><?php print(number_format($int_gross_amount, 2)); ?></b></td>
                                        
                                    </tr>
                                <?php
                                    $int_total_amount = $int_total_amount + $int_actual_amount;
                                    //$int_total_amount = $int_total_amount + $int_actual_amount;
                                    $int_total_shipping_charge = $int_total_shipping_charge + $int_shipping_total_value;
                                    $int_total_promo_code = $int_total_promo_code + $rs_list->fields("finaldiscountedamount");
                                    $int_total_wholesaler_amount = $int_total_wholesaler_amount + $int_wholesaler_discount_amount;
                                    $int_total_giftcard_amount = $int_total_giftcard_amount + $int_giftcard_amount;
                                    $int_total_refund_amount = $int_total_refund_amount + $int_refunded_amount;
                                    $int_total_gross_amount = $int_total_gross_amount + $int_gross_amount;
                                    $rs_list->movenext();
                                    $int_cnt=$int_cnt+1;
                                    }
                                    ?>
                                    <tr><td colspan="4"><br/></td></tr>
                                    <tr>

                                       <?php // $int_total_price = 20; ?>
                                        <td colspan="3" class="text-danger" align="right"><input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>"><strong>Total For <?php print(MonthName($int_month));?>&nbsp;-&nbsp;<?php print($int_year);?> </strong></td>
                                        <td align="right"><?php print("<span class='text-danger'><b>".number_format($int_total_amount,2,".",",")."&nbsp;</b></span><br/>"); ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right"><b>+ Total Shipping Charge For This Month</b></td>
                                        <td align="right"><b><?php print("<span class=''>".number_format($int_total_shipping_charge,2,".",",")."&nbsp;</span>"); ?></b></td>
                                        <?php /* ?><td colspan="3"></td><?php */ ?>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right"><b>- Total Promo Code Discount For This Month</b></td>
                                        <td align="right"><b><?php print("<span class=''>".number_format($int_total_promo_code,2,".",",")."&nbsp;</span>"); ?></b></td>
                                        <?php /* ?><td colspan="3"></td><?php */ ?>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right"><b>- Total Wholesaler Discount For This Month</b></td>
                                        <td align="right"><b><?php print("<span class=''>".number_format($int_total_wholesaler_amount,2,".",",")."&nbsp;</span>"); ?></b></td>
                                        <?php /* ?><td colspan="3"></td><?php */ ?>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right"><b>- Total Gift Card Discount For This Month</b></td>
                                        <td align="right"><b><?php print("<span class=''>".number_format($int_total_giftcard_amount,2,".",",")."&nbsp;</span>"); ?></b></td>
                                        <?php /* ?><td colspan="3"></td><?php */ ?>
                                    </tr>
				    <tr>
                                        <td colspan="3" align="right"><b>- Total Refund Amount For This Month</b></td>
                                        <td align="right"><b><?php print("<span class=''>".number_format($int_total_refund_amount,2,".",",")."&nbsp;</span>"); ?></b></td>
                                        <?php /* ?><td colspan="3"></td><?php */ ?>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right" class="text-primary"><b>Gross Total For <?php print(MonthName($int_month)); ?> - <?php print($int_year); ?> (+ Shipping Charge - Promo Code Discount - Wholesaler Discount - Gift Card Discount)</b></td>
                                        <td align="right"><?php print("<h4 class='text-primary '><b>".number_format($int_total_gross_amount,2,".",",")."&nbsp;</b></h4>"); ?></td>
                                        <?php /* ?><td colspan="3"></td><?php */ ?>
                                    </tr>
                            <?php }  ?>					
                        </tbody>
                    </table>
                </form>	
            </div>
        </div>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>   
</body></html>
