<?php
/*  
Module Name:- modPhotoset
File Name  :- item_list.php
Create Date:- 19-JAN-2019
Intially Create By :- 015
Update History: 
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
include "../../includes/lib_file_upload.php";	
include "../../includes/lib_datetimeyear.php";	
#------------------------------------------------------------------------------------------------
# Select Query to display list
$str_select_query = "";
$str_select_query = "SELECT * FROM ".$STR_DB_TABLE_NAME_COLOR.$STR_DB_TABLE_NAME_ORDER_BY_COLOR."";
$rs_list = GetRecordSet($str_select_query);
#------------------------------------------------------------------------------------------------
# Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
	case("E"): $str_type = "E"; break;
	case("W"): $str_type = "W"; break;
    }
}
# Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
        case("DU"): $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
    }
} ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE_COLOR);?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../css/admin.css" rel="stylesheet">
</head>
<body onLoad="myFunction()">
<!-- Page Content -->
<a name="ptop"></a>
<div class="container content_bg">
<?php include "../../includes/adminheader.php"; ?>
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
            <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Back to State List"><i class="glyphicon glyphicon-chevron-left"></i>&nbsp;Back</a><?php */?>
            <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?> " ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?> </a>
            </div>
        </div>
        <div class="col-md-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE_COLOR); ?></h3></div>
    </div><hr>
    <?php   if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); } ?>
    <div class="row padding-10">
    	<div class="col-md-12">
            <div class="panel-group" id="accordion">
            	<div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <div class="row padding-10">
                                <div class="col-md-6 col-sm-6 col-xs-8"><b><a class="accordion-toggle collapsed link" data-toggle="collapse" title="<?php print($STR_TITLE_ADD); ?>" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?php print($STR_TITLE_ADD); ?> </a></b></div>
                                <div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>                                                     </div>
			</h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="false" <?php /* ?> style="height: 0px;"<?php */ ?>>
                    	<div class="panel-body">
                            <form name="frm_add" action="item_color_add_p.php" method="post" onSubmit="return frm_add_validate();">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <span class="text-help-form"> *</span><input name="txt_title" type="text" tabindex="1" class="form-control input-sm" id="" maxlength="255" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Hex Color Code</label>
                                            <span class="text-help-form"> *</span>
                                            <div id="cp2" class="input-group colorpicker-component"> 
                                                <input name="txt_cmykcode" value="#000000" type="text" tabindex="2" class="form-control input-sm" id="" maxlength="255" placeholder="<?php print $STR_PLACEHOLDER_COLOR_CODE; ?>">
                                                <span class="input-group-addon"><i></i></span>
                                            </div>                                            
                                        </div>                                        
                                    </div>
                                    <?php /* ?><div class="col-md-4">
                                        <div class="form-group">
                                            <label>RGB Code</label>
                                           <span class="text-help"> *</span><input name="txt_rgbcode" type="text" tabindex="1" class="form-control input-sm" id="" maxlength="255" placeholder="Enter RGB code">
                                        </div>
                                    </div><?php */ ?>
                                </div>
                                <?php print DisplayFormButton("ADD", 3); ?><?php print DisplayFormButton("RESET", 4); ?>                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="table-responsive">
                <form name="frm_list" method="post">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="4%">Sr. #</th>
                                <th width="">Details</th>
                                <th width="8%">Color</th>
                                <th width="8%">Hex Color <br/>Code</th>
                                <?php /* ?><th width="8%">RGB Code</th><?php */ ?>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>	
                        <?php if($rs_list->EOF()==true)  {  ?>
                        <tr><td colspan="7" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                    <?php  }
                    else   {
                            $int_cnt=1;
                            while(!$rs_list->EOF()==true) { ?>    
                                <tr>
                                    <td align="center" ><?php print($int_cnt); ?></td>
                                    <td><h4 class=""><b><?php print(MyHtmlEncode($rs_list->Fields("title"))) ?></b></h4></td>
                                    <td align="center">
                                        <span class="color-circle-lg" style="background-color:<?php print $rs_list->Fields("cmyk_code"); ?>;" ></span>
                                    </td>
                                    <td align="center"><?php print $rs_list->Fields("cmyk_code"); ?></td>
                                    <?php /* ?><td align="center"><?php print $rs_list->Fields("rgb_code"); ?></td><?php */ ?>
                                    <td align="center">
                                        <a class="btn btn-success btn-xs" href="item_color_edit.php?pkid=<?php print($rs_list->fields("pkid"))?>" title="<?php print($STR_HOVER_EDIT)?>"><?php print($STR_LINK_ICON_PATH_EDIT); ?></a>
                                        <a class="btn btn-danger btn-xs" href="item_color_del_p.php?pkid=<?php print($rs_list->fields("pkid"))?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE); ?>"><?php print($STR_LINK_ICON_PATH_DELETE); ?></a>
                                    </td>
                                </tr>
                            <?php 
                            $int_cnt++;
                            $rs_list->MoveNext();
                            }
                            ?>
                    <?php } ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
    </div>
</div>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<link href="../../css/bootstrap-colorpicker.css" rel="stylesheet">
<script src="../../js/bootstrap-colorpicker.js"></script>
<script language="JavaScript" src="../../js/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="item_color_list.js" type="text/javascript"></script>
<script type="text/javascript">
$('#cp2').colorpicker();
</script>
</body></html>
