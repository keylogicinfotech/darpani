<?php
/*
Module Name:- modPstore
File Name  :- cat_edit_p.php
Create Date:- 20-MARCH-2006
Intially Create By :- 0023
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid = 0;
if (isset($_POST["hdn_pkid"]))
{	
    $int_pkid = trim($_POST["hdn_pkid"]);
}		
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E&#ptop");
    exit();
}

$str_cat_flag="";
if (isset($_POST["hdn_catflag"]))
{
    $str_cat_flag = trim($_POST["hdn_catflag"]);
}
if($str_cat_flag=="" || ($str_cat_flag!="c" && $str_cat_flag!="s"))
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E&#ptop");
    exit();
}

$str_title = "";
$str_type = 0;

if (isset($_POST["txt_title"]))
{
    $str_title = trim($_POST["txt_title"]);
}
if (isset($_POST["cbo_type"]))
{
    $str_type = trim($_POST["cbo_type"]);
}	

#----------------------------------------------------------------------------------------------------
#Validation Check

if($str_title == "")
{
    CloseConnection();
    Redirect("item_cat_edit.php?msg=F&type=E&catflag=".$str_cat_flag."&pkid=". $int_pkid."&#ptop");
    exit();
}
if($str_type <0 || is_numeric($str_type)==false)
{
    CloseConnection();
    Redirect("item_cat_edit.php?msg=F&type=E&catflag=".$str_cat_flag."&pkid=". $int_pkid."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
if($str_cat_flag=="c")
{
    
    # For Category
    # Duplication Check
    $str_query_select = "";
    $str_query_select = "SELECT title FROM ".$STR_DB_TABLE_NAME_CAT." WHERE catpkid!=".$int_pkid." AND title= '" . ReplaceQuote($str_title) . "'";
    $rs_list_check_duplicate = GetRecordSet($str_query_select);

    if(!$rs_list_check_duplicate->eof())
    {
        CloseConnection();
        Redirect("item_cat_edit.php?msg=DU&type=E&title=".$rs_list_check_duplicate->fields("title")."&catflag=".$str_cat_flag."&pkid=". $int_pkid."&#ptop");
        exit();
    }
    #----------------------------------------------------------------------------------------------------
    #update Record In a t_ppd_cat table
    $str_query_update = "";
    $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_CAT." SET title='" .  ReplaceQuote($str_title) . "'";
    $str_query_update .= " WHERE catpkid=". $int_pkid;
//  print $str_query_update; exit;
    ExecuteQuery($str_query_update);
    #----------------------------------------------------------------------------------------------------
    #Close connection and redirect to cat_list.php page	
    CloseConnection();
    Redirect("item_cat_list.php?type=S&msg=U&#ptop");
    exit();
    #------------------------------------------------------------------------------------------------------------
}
else if($str_cat_flag=="s")
{
#perform operation on t_ppd_subcat
    #Duplicate Checking for Title field.
    $str_query_select="";
    $str_query_select="SELECT subcattitle FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid!=".$int_pkid." AND catpkid=".$str_type." AND subcattitle= '" . ReplaceQuote($str_title) . "'";
    $rs_list_check_duplicate=GetRecordSet($str_query_select);

    if(!$rs_list_check_duplicate->eof())
    {
        CloseConnection();
        Redirect("item_cat_edit.php?msg=SDU&type=E&title=".$rs_list_check_duplicate->fields("title")."&catflag=".$str_cat_flag."&pkid=". $int_pkid."&#ptop");
        exit();
    }
    #----------------------------------------------------------------------------------------------------
    #Update Query
    $str_query_update = "";
    $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_SUBCAT." SET subcattitle='".ReplaceQuote($str_title) . "',catpkid=".$str_type;
    $str_query_update .= " WHERE subcatpkid=". $int_pkid;

    ExecuteQuery($str_query_update);
    #----------------------------------------------------------------------------------------------------
    #Close connection and redirect to it's relevant location
    CloseConnection();
    Redirect("item_cat_list.php?type=S&msg=SU&title=".urlencode(RemoveQuote($str_title))."&#ptop");
    exit();
    #------------------------------------------------------------------------------------------------------------
}
?>