// JavaScript Document
$(document).ready(function() {
    $('#frm_add').on('submit', function(event) {
        event.preventDefault();

        var formData = new FormData($('#frm_add')[0]);
        formData.append("cbo_subcat",$("select#cbo_subcat").val());
        formData.append("cbo_mdl",$("select#cbo_mdl").val());
        formData.append("cbo_month",$("select#cbo_month").val());
        formData.append("cbo_year",$("select#cbo_year").val());
        formData.append("txt_title",$("input#txt_title").val());
        formData.append("txt_listprice",$("input#txt_listprice").val());
        formData.append("txt_ourprice",$("input#txt_ourprice").val());
        formData.append("txt_color",$("input#txt_color").val());
       // formData.append("cbo_color",$("select#cbo_color").val());
        formData.append("txt_size",$("input#txt_size").val());
        formData.append("txt_type",$("input#txt_type").val());
        formData.append("txt_domestic",$("input#txt_domestic").val());
        formData.append("txt_international",$("input#txt_international").val());
        formData.append("txt_shipping_info",$("input#txt_shipping_info").val());
        
        
        formData.append("txt_nophoto",$("input#txt_nophoto").val());
        //formData.append('hdn_pkid', $("input#hdn_pkid").val());
        formData.append("ta_desc",$("textarea#ta_desc").val());
        //formData.append('video_clip', $("input#video_clip")[0].files[0]);
        formData.append("cbo_extension",$("select#cbo_extension").val());
        formData.append("txt_ptitle",$("input#txt_ptitle").val());
        formData.append("txt_purl",$("input#txt_purl").val());
        //var formData = new FormData($('#frm_add')[1]);
        //alert($("input#video_clip")[0].files[0]));

        if($("select#cbo_subcat").val() == 0) 
        {
                alert("Please Select category.");
                exit();
        }
        if($("select#cbo_mdl").val() == 0) 
        {
                alert("Please Select Recipient.");
                exit();
        }

        if($("input#txt_title").val() == "") 
        {
            alert("Please enter title");
            exit();
        }
        if($("input#txt_listprice").val() == "") 
        {
            alert("Please Enter List Price");
            exit();
        }
        var num_value=$('#txt_listprice').val();
        if(!$.isNumeric(num_value))
        {
                alert("Please Enter Numeric Value For List Price.");
                return false;
            
        }
        
                
        

        var num_ourprice=$('#txt_ourprice').val();
        if((!$.isNumeric(num_ourprice)) && (num_ourprice != ""))
        {
            alert("Please Enter Numeric Value For Our Price.");
            return false;
        }
        
        var num_shippingprice=$('#txt_domestic').val();
        if((!$.isNumeric(num_shippingprice)) && (num_shippingprice != ""))
        {
            alert("Please Enter Numeric Value For Shipping Price.");
            return false;
        }
        if($("input#video_clip").val() == "") 
        {
            alert("Please Select File");
            exit();
        }
        

//        alert($("input#txt_ourprice").val());
//        if($("input#txt_ourprice").val() != "") 
//        {
//            alert("hi....");
//            var num_ourprice=$('#txt_ourprice').val();
//            if(!$.isNumeric(num_ourprice))
//            {
//                {
//                    alert("Please Enter Numeric Value For Our Price.");
//                    return false;
//                }
//            return true;
//            }
//        }

//        if(isNumeric('#txt_domestic'.value)==true)
//        {
//                {
//                    alert("Please Enter Numeric Value For Shipping Value.");
//                    return false;
//                }
//        return true;
//        }
        
//        if($("input#txt_ourprice").val() != "") 
//        {
//            var num_our_value=$('#txt_ourprice').val();
//            if(!$.isNumeric(num_our_value))
//            {
//                {
//                    alert("Please Enter Numeric Value For Our Price.");
//                    return false;
//                }
//
//            return true;
//            }
//            
//        }


//        if(!$("input#txt_ourprice").val() == "")
//        {
//            var num_our_value=$('#txt_ourprice').val();
//            if(!$.isNumeric(num_our_value))
//            {
//                {
//                    alert("Please Enter Numeric Value For Our Price.");
//                    return false;
//                }
//            return true;
//            }
//            
//        }

        
        
//        var num_ship_value=$('#txt_domestic').val();
//        if(!$("input#txt_domestic").val() == "")
//        {
//            
//            if(!$.isNumeric(num_ship_value))
//            {
//                {
//                    alert("Please Enter Numeric Value For Shipping Price.");
//                    return false;
//                }
//            return true;
//            }
//        }
//        if(!$("input#txt_domestic").val() == "")
//        {
//            if(!$.isNumeric(num_value))
//            {
//                {
//                    alert("Please Enter Numeric Value For Shipping Value.");
//                    return false;
//                }
//                return true;
//            }
//             return false;
//        }

//        
//        if(!$("input#txt_ourprice").val() == "") 
//        {
//            var num_oprice=$('#txt_ourprice').val();
//            if(!$.isNumeric(num_oprice))
//
//            {
//
//                {
//                    alert("Please Enter Numeric Value For Our Price.");
//                    return false;
//                }
//
//            return true;
//            }
//        }
       
//        if(!$("input#txt_domestic").val() == "")
//        {
//            var num_ship_value=$('#txt_domestic').val();
//            if(!$.isNumeric(num_ship_value))
//
//            {
//
//                {
//                    alert("Please Enter Numeric Value For Shipping Value.");
//                    return false;
//                }
//
//            return true;
//            }
//        }

        else
        {
            $.ajax({
            xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener('progress', function(e) {
                        if(e.lengthComputable)
                        {
                            //console.log('Bytes Loaded: ' + e.loaded);
                            //console.log('Total Size: ' + e.total);
                            //console.log('Percentage uploaded: ' + (e.loaded/e.total))
                            var percent = Math.round((e.loaded/e.total) * 100);
                            $('#progressBar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
                        }												 
                    });
                    return xhr;
                   },

                    type: 'POST',
                    url: 'item_add_p.php',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        alert(data);
                        var $responseText=JSON.parse(data);

                            if($responseText.status == 'ERR' && $responseText.IFE == 'IF')
                            {
                                location.href = './item_list.php?msg=IE&type=E&#ptop';
                            }
                            else if($responseText.status == 'ERR' && $responseText.IFE == 'IU')
                            {
                                location.href = './item_list.php?msg=IU&type=E&#ptop';
                            }
                            else if($responseText.status == 'ERR' && $responseText.IFE == 'DU')
                            {
                                location.href = './item_list.php?msg=DU&type=E&#ptop';
                            } 
                            else if($responseText.status == 'ERR' && $responseText.IFE == 'SM')
                            {
                                location.href = './item_list.php?msg=F&type=E&#ptop';
                            }
                            else if($responseText.status == 'SUC' && $responseText.IFE == 'SA')
                            {
                                location.href = './item_list.php?msg=S&type=S&#ptop';
                                    //location.reload();
                                    //window.setTimeout(function(){location.reload()},3000)
                            }
                            else if($responseText.status == 'ERR' && $responseText.IFE == 'BL')
                            {
                                location.href = './item_list.php?msg=BL&type=E&#ptop';
                            }
                            /*else if($responseText.status == 'SUC')
                            {
                                    // Fail message
                                    $('#success').html("<div class='alert alert-danger'>");
                                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                            .append("</button>");
                                    $('#success > .alert-danger').append("<strong> " + $responseText.message + " ");
                                    $('#success > .alert-danger').append('</div>');
                            }*/

                            //alert(data);
                            //alert("File Uploaded Successfully");
                            //location.reload();
                  }
//                  alert(data);
           });
    } 

        });
	});
				
						   // JavaScript Document