<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------------------------------------
//session_start();
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_xml.php";
include "../../includes/lib_email.php";
include "item_config.php";
//include "item_app_specific.php";
//print "HI"; exit;
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_POST);exit;

$str_purchasedatetime = "";
if (isset($_POST['hdn_purchasedatetime'])) {
    $str_purchasedatetime = $_POST['hdn_purchasedatetime'];
}

$int_pkid = 0;
if (isset($_POST['hdn_pkid'])) {
    $int_pkid = $_POST['hdn_pkid'];
}

$int_productpkid = 0;
if (isset($_POST['hdn_productpkid'])) {
    $int_productpkid = $_POST['hdn_productpkid'];
}

$int_userpkid = 0;
if (isset($_POST['hdn_userpkid'])) {
    $int_userpkid = $_POST['hdn_userpkid'];
}

$int_total_amount = 0;
if (isset($_POST['hdn_total_amount'])) {
    $int_total_amount = $_POST['hdn_total_amount'];
}

$int_total_amount_giftcard = 0;
if (isset($_POST['hdn_total_amount_giftcard'])) {
    $int_total_amount_giftcard = $_POST['hdn_total_amount_giftcard'];
}

$int_paymentoptionpkid = 0;
if (isset($_POST['hdn_paymentoptionpkid'])) {
    $int_paymentoptionpkid = $_POST['hdn_paymentoptionpkid'];
}


if ($int_pkid <= 0 || !is_numeric($int_pkid) || $int_pkid == "" || $int_productpkid <= 0 || !is_numeric($int_productpkid) || $int_productpkid == "" || $int_userpkid <= 0 || !is_numeric($int_userpkid) || $int_userpkid == "") {
    CloseConnection();
    Redirect("item_order_status_list.php?type=E&msg=F");
    exit();
}

$str_status = "";
if (isset($_POST['cbo_status'])) {
    $str_status = $_POST['cbo_status'];
}

$str_currency_symbol = "";
if (isset($_POST['hdn_currency_symbol'])) {
    $str_currency_symbol = $_POST['hdn_currency_symbol'];
}

$str_filter_shipping_status = "";
if (isset($_POST['hdn_filter_shipping_status'])) {
    $str_filter_shipping_status = $_POST['hdn_filter_shipping_status'];
}

$int_filter_country = "";
if (isset($_POST['hdn_filter_country'])) {
    $int_filter_country = $_POST['hdn_filter_country'];
}

$int_filter_year = "";
if (isset($_POST['hdn_filter_year'])) {
    $int_filter_year = $_POST['hdn_filter_year'];
}

$int_filter_month = "";
if (isset($_POST['hdn_filter_month'])) {
    $int_filter_month = $_POST['hdn_filter_month'];
}
//print "&countrypkid=".$int_filter_country."&cbo_year=".$int_filter_year."&cbo_status=".$str_filter_shipping_status; exit;
#-----------------------------------------------------------------------------------------------------
$int_giftcard = 0;
$rs_list_user = "";
// If order status is CANCELLED & giftcard amount in order is greater than 0, 
//print $int_total_amount_giftcard."<br/>".$int_total_amount."<br/>"; 

$str_query_select = "";
$str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME_USER . " WHERE pkid = " . $int_userpkid;
$rs_list_user = GetRecordSet($str_query_select);

if ($str_status == $STR_CBO_OPTION4 && $int_paymentoptionpkid == 2) {
    if ($int_total_amount_giftcard > 0) {
        $int_giftcard = $rs_list_user->Fields("giftcard") + $int_total_amount_giftcard;
    } else {
        $int_giftcard = 0;
    }
} else if ($str_status == $STR_CBO_OPTION4 && $int_paymentoptionpkid <> 2) {
    if ($int_total_amount_giftcard > 0) {
        //$int_giftcard = $rs_list_user->Fields("giftcard") + $int_total_amount_giftcard + ((($int_total_amount - $int_total_amount_giftcard) * $INT_REFUND_PERCENTAGE) / 100);
        $int_giftcard = $rs_list_user->Fields("giftcard") + $int_total_amount_giftcard;
    } else {
        //$int_giftcard = $rs_list_user->Fields("giftcard") + $int_total_amount_giftcard + (($int_total_amount * $INT_REFUND_PERCENTAGE) / 100);
        $int_giftcard = $rs_list_user->Fields("giftcard") + $int_total_amount_giftcard;
    }
} else if ($str_status == $STR_CBO_OPTION5 && $int_paymentoptionpkid == 2) // it will be same for all type of payment options
{
    if ($int_total_amount_giftcard > 0) {
        $int_giftcard = $rs_list_user->Fields("giftcard") + ((($int_total_amount - $int_total_amount_giftcard) * $INT_REFUND_PERCENTAGE) / 100);
    } else {
        $int_giftcard = $rs_list_user->Fields("giftcard") + $int_total_amount_giftcard + (($int_total_amount * $INT_REFUND_PERCENTAGE) / 100);
    }
} else if ($str_status == $STR_CBO_OPTION5 && $int_paymentoptionpkid <> 2) // it will be same for all type of payment options
{
    if ($int_total_amount_giftcard > 0) {
        $int_giftcard = $rs_list_user->Fields("giftcard") + ((($int_total_amount - $int_total_amount_giftcard) * $INT_REFUND_PERCENTAGE) / 100);
    } else {
        $int_giftcard = $rs_list_user->Fields("giftcard") + $int_total_amount_giftcard + (($int_total_amount * $INT_REFUND_PERCENTAGE) / 100);
    }
}


/*if($str_status == $STR_CBO_OPTION4 && $int_total_amount_giftcard > 0) 
{
    $int_giftcard = $rs_list_user->Fields("giftcard") + $int_total_amount_giftcard;
}
if($str_status == $STR_CBO_OPTION5) 
{
    $int_giftcard = $rs_list_user->Fields("giftcard") + (($int_total_amount * $INT_REFUND_PERCENTAGE) / 100);
}*/


$str_query_update = "";
$str_query_update = "UPDATE " . $STR_DB_TABLE_NAME_USER . " SET giftcard=" . $int_giftcard . " WHERE pkid=" . $int_userpkid;
ExecuteQuery($str_query_update);

//print $int_giftcard."<br/>";
//exit;

#----------------------------------------------------------------------------------------------------
$str_mailbody_product_list = "";
$str_query_select = "";
$str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME_PURCHASE . " WHERE purchasedatetime='" . $str_purchasedatetime . "' AND userpkid=" . $int_userpkid;
$rs_list = GetRecordSet($str_query_select);
if (!$rs_list->EOF()) {
    while (!$rs_list->EOF() == true) {
        #Update query to change the mode
        $str_query_update = "";
        $str_query_update = "UPDATE " . $STR_DB_TABLE_NAME_PURCHASE . " SET shippingstatus='" . ReplaceQuote($str_status) . "' WHERE purchasedatetime='" . $str_purchasedatetime . "' AND userpkid=" . $int_userpkid . " AND purchasepkid=" . $rs_list->Fields("purchasepkid");
        //print $str_query_update."<br/>"; 
        ExecuteQuery($str_query_update);

        $str_mailbody_product_list .= "<br/>" . $rs_list->Fields("producttitle") . " (Quantity " . $rs_list->Fields("quantity") . ")";

        $rs_list->movenext();
    }
}
//exit;
#----------------------------------------------------------------------------------------------------
#Write to xml file
//WriteXml();
#-----------------------------------------------------------------------------------------------------
# Send Mail
$str_query_select = "";
$str_query_select = "SELECT * FROM " . $STR_DB_TABLE_NAME_PURCHASE . " WHERE purchasepkid = " . $int_pkid;
$rs_list = GetRecordSet($str_query_select);

//------------------ get order id and phone_no from t_store_purchase table for sms ------------
$purchase_id = $rs_list->Fields('purchasepkid');
$order_id = $rs_list->Fields('subscriptionid');
$str_customer_ten_digit_number = (substr($rs_list->Fields('phoneno'), -10));
// $track_order = "<a href='" . $STR_SITENAME_WITHOUT_PROTOCOL . "/user/user_order_status_list.php' target='_blank'>Track Your Order</a>";

if ($rs_list->Count() > 0) {

    $str_query_select = "";
    $str_query_select = "SELECT * FROM t_user WHERE pkid=" . $int_userpkid;
    $rs_list_user = GetRecordSet($str_query_select);

    $str_subject = "";
    $str_mailbody = "";
    $fp = openXMLfile("../" . $STR_XML_FILE_PATH_MODULE . "siteconfiguration.xml");
    $str_from = getTagValue($STR_FROM_DEFAULT, $fp);
    //$str_to = $rs_list_user->fields("emailid"); // This will take emailid from user table
    $str_to = $rs_list->fields("emailid"); // This will take emailid from purchase table

    closeXMLfile($fp);

    //$str_mail_subject_shipping_status_user = "";
    //$str_mail_mailbody_shipping_status_user = "";

    if ($str_status == "SHIPPED") {
        $str_subject = "Your order is shipped from " . $STR_SITENAME_WITHOUT_PROTOCOL . "";
        $str_mailbody = "Your order is shipped from " . $STR_SITENAME_WITHOUT_PROTOCOL . " <br> Following are details: ";
        $str_mailbody .= "<br/><br/> <strong>Shipping Date Time:</strong> " . date('Y-m-d H:i:s') . " ";
        $str_mailbody .= "<br/><br/><strong>Ordered Items : </strong> " . $str_mailbody_product_list . "";
        $str_mailbody .= "<br/><br/> <strong>Total Order Amount:</strong> " . $str_currency_symbol . " " . $int_total_amount . " ";
        // ------------------------ create shipped msg format for mobile sms --------------------------
        $msg = urlencode("SHIPPED: Your order id " . $order_id . " With DARPANI is Traking Number " . $purchase_id . " Click Here To Track Status:" . $STR_SITE_URL);
        //  die(urldecode($msg));
    } elseif ($str_status == "CANCELLED") {
        $str_subject = "Your order is cancelled on " . $STR_SITENAME_WITHOUT_PROTOCOL . "";
        $str_mailbody = "Your order is cancelled on " . $STR_SITENAME_WITHOUT_PROTOCOL . " <br> Following are details: ";
        $str_mailbody .= "<br/><br/> <strong>Cancel Date Time:</strong> " . date('Y-m-d H:i:s') . " ";
        $str_mailbody .= "<strong>Ordered Items : </strong> " . $str_mailbody_product_list . "<br/>";
        $str_mailbody .= "<br/><br/> <strong>Total Order Amount:</strong> " . $str_currency_symbol . " " . $int_total_amount . " ";
    }
    // ============================== send sms to user mobile =============================================================
    sms($str_customer_ten_digit_number, $msg); // call function which is write in lib_common.php
    //print $str_subject."<br/>".$str_mailbody; exit;
    sendmail($str_to, $str_subject, $str_mailbody, $str_from, 1);
}

#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_report_transaction_yearly.php?type=S&msg=U&countrypkid=" . $int_filter_country . "&cbo_year=" . $int_filter_year . "&cbo_month=" . $int_filter_month . "&cbo_status=" . $str_filter_shipping_status);
exit();
#-----------------------------------------------------------------------------------------------------
