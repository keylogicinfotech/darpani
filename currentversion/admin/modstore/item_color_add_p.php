<?php
/*
Module Name:- modstore
File Name  :- color_add_p.php
Create Date:- 19-JAN-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "./item_config.php";
include "../../includes/lib_common.php";	
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$str_title = "";
$str_cmyk = "";
$str_rgb = "";
	
if (isset($_POST["txt_title"]))
{
    $str_title = trim($_POST["txt_title"]);
}
if (isset($_POST["txt_cmykcode"]))
{
    $str_cmyk = trim($_POST["txt_cmykcode"]);
}
if (isset($_POST["txt_rgbcode"]))
{
    $str_rgb = trim($_POST["txt_rgbcode"]);
}	
#----------------------------------------------------------------------------------------------------
#Prepare query string to pass variables back
$str_redirect = "";
#----------------------------------------------------------------------------------------------------
#To check required parameters are passed properly or not
if($str_title == "" || $str_cmyk == "" /* || $str_rgb == ""*/)
{
    CloseConnection();
    Redirect("item_color_list.php?msg=F&type=E". $str_redirect."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Duplication Check
$str_query_select = "";
$str_query_select = "SELECT title FROM ".$STR_DB_TABLE_NAME_COLOR." WHERE title= '" . ReplaceQuote($str_title) . "'";
$rs_list_check_duplicate = GetRecordSet($str_query_select);

if(!$rs_list_check_duplicate->eof())
{
    CloseConnection();
    Redirect("item_color_list.php?msg=DU&type=E&". $str_redirect."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Insert Query
$str_query_insert = "";
$str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_COLOR." (title, cmyk_code, rgb_code)";
$str_query_insert.= " VALUES ('" . ReplaceQuote($str_title) . "','" . ReplaceQuote($str_cmyk) . "', '".$str_rgb."' )";
ExecuteQuery($str_query_insert);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_color_list.php?type=S&msg=S&title=".urlencode(RemoveQuote($str_title))."&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------
?>

