/*
	Module Name:- modstore
	File Name  :- item_cat_list.js
	Create Date:- 04-FEB-2019
	Intially Create By :- 015
	Update History:
*/

function frm_edit_validate()
{
    with(document.frm_edit)
    {
        if(cbo_cat.value == "0")
        {
                alert("Please Select Category.");
                //cbo_cat.select();
                cbo_cat.focus();			
                return false;
        }
        if(cbo_subcat.value == "0")
        {
                //alert("Please Select Sub Category.");
                //cbo_cat.select();
                //cbo_subcat.focus();			
                //return false;
        }
        if(trim(txt_code.value) == "")
        {
            alert("Please enter item code.");
            txt_code.select();
            txt_code.focus();
            return false;
        }
        if(trim(txt_title.value) == "")
        {
            alert("Please enter title.");
            txt_title.select();
            txt_title.focus();
            return false;
        }
        if(trim(txt_listprice.value) == "")
        {
                alert("Please enter list price.");
                txt_listprice.select();
                txt_listprice.focus();			
                return false;
        }
        if(trim(txt_weight.value) == "" || trim(txt_weight.value) <= 0)
        {
                alert("Please enter weight.");
                txt_weight.select();
                txt_weight.focus();			
                return false;
        }
        
    }
    return true;
}
