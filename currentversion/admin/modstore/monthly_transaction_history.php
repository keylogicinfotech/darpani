<?php
/*
    File Name  :- monthly_transaction_history.php
    Create Date:- MARCH-2019
    Intially Create By :- 0013
    Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../includes/validatesession.php";
include "../includes/configuration.php";
include "purchase_config.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
include "../includes/lib_datetimeyear.php";

#------------------------------------------------------------------------------------------------
$int_month=date("m");
if(isset($_GET['cbo_month']))
{ $int_month=trim($_GET['cbo_month']); }

$int_year=date("Y");
if(isset($_GET['cbo_year']))
{ $int_year=trim($_GET['cbo_year']); }

$int_model_pkid=0;
if(isset($_GET['cbo_model']))
{ $int_model_pkid=trim($_GET['cbo_model']); }

if($int_month==0 || $int_month=="")
{ $int_month=date("m"); }

if($int_year==0 || $int_year=="")
{ $int_year=date("Y"); }

//print $int_model_pkid."<br/>".$int_month."<br/>".$int_year."<br/>";

$filtercriteria="";
if($int_model_pkid > 0 && $int_month == 0 && $int_year == 0 ) 
{
	$filtercriteria=" WHERE modelpkid=".$int_model_pkid. " AND photosetinmonth=".date("m")." AND photosetinyear=".date("Y");
}
else if($int_model_pkid > 0 && $int_month != 0 && $int_year == 0 ) 
{
	$filtercriteria=" WHERE modelpkid=".$int_model_pkid. " AND photosetinmonth=".$int_month." AND photosetinyear=".date("Y");
}
else if($int_model_pkid > 0 && $int_month == 0 && $int_year != 0 ) 
{
	$filtercriteria=" WHERE modelpkid=".$int_model_pkid. " AND photosetinmonth=".date("m")." AND photosetinyear=".$int_year;
}
else if($int_model_pkid > 0 && $int_month != 0 && $int_year != 0 ) 
{
	$filtercriteria=" WHERE modelpkid=".$int_model_pkid. " AND photosetinmonth=".$int_month." AND photosetinyear=".$int_year;
}
else if($int_model_pkid == 0 && $int_month != 0 && $int_year != 0 ) 
{
	$filtercriteria=" WHERE photosetinmonth=".$int_month." AND photosetinyear=".$int_year;
}
else if($int_model_pkid == 0 && $int_month != 0 && $int_year == 0 ) 
{
	$filtercriteria=" WHERE photosetinmonth=".$int_month." AND photosetinyear=".date("Y");
}
else if($int_model_pkid == 0 && $int_month == 0 && $int_year != 0 ) 
{
	$filtercriteria=" WHERE photosetinmonth=".date("m")." AND photosetinyear=".$int_year;
}
else if($int_model_pkid == 0 && $int_month == 0 && $int_year == 0 ) 
{
	$filtercriteria=" WHERE photosetinyear=".date("m")." AND photosetinyear=".date("Y");
}


// Get model name to display on report
if($int_model_pkid>0)
{
	$str_query_select = "SELECT modelname FROM t_model WHERE modelpkid=".$int_model_pkid;
	$rs_modelname=GetRecordset($str_query_select);
	$str_modelname = $rs_modelname->fields("modelname")." : ";
}
else
{ $str_modelname = ""; }

#------------------------------------------------------------------------------------------------
// Below query will group items by purchase date time. So it will do SUM SUM(extendedprice) as gross_price of all items in single order 
// and will display it. 
// GROUP_CONTACT(CONCAT will display details of each item in single order using this GROUP_CONCAT(CONCAT(producttitle, ' | ', price, '<br/>')) as item_details
// Here | is separator of different values of single item.
// Here <br/> is separator among different items in single order
// Below query is working but we are not using s15-AUG-2014
$str_query_select = "SELECT *, SUM(pricepkid)as gross_price, SUM(dshippingvalue)as gross_shippping";
//$str_query_select .=", GROUP_CONCAT(CONCAT(producttitle , '|' , price , '|' , quantity , '|' , extendedprice , '|' , color , '|' , size , '|' , typemodel , '|' , emailid , '|' , subscriptionid , '|' , shippingaddress , '|' , firstname , '|' , lastname , '|' , address , '|' , city , '|' , state , '|' , country , '|' , zipcode , '|' , specialnote , '|' , phoneno ) SEPARATOR '::') as item_details "; 
$str_query_select .= " FROM t_photoset";
$str_query_select .=" ".$filtercriteria; 
$str_query_select .= " GROUP BY lastupdatedate, photosetpkid, modelpkid, photosetfilename, catpkid, subcatpkid,photosettitle,pricepkid,noofphoto,shippinginfo,dshippingvalue,ishippingvalue,color, size,type ORDER BY lastupdatedate DESC";

//print $str_query_select."<br/>";exit;
$rs_purchase_history_list = GetRecordset($str_query_select);


#------------------------------------------------------------------------------------------------
	$str_type = "";
	$str_message = "";
	$str_visible_value="";

	if(isset($_GET['mode']))
	{
		$str_mode=trim($_GET['mode']);
	}
#	Get message type.
   		if(isset($_GET["type"]))
		{
			switch(trim($_GET["type"]))
			{
				case("S"): $str_type = "S"; break;
				case("E"): $str_type = "E"; break;
				case("W"): $str_type = "W"; break;
			}
		}
#	Get message text.
		if(isset($_GET["msg"]))
		{
			switch(trim($_GET["msg"]))
			{
				case("F"):
					$str_message = "Some information(s) missing. Please try again.";
					break;
				case("A"):
					$str_message = "Product Purchase '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' details added successfully.";
					break;
				case("U"):
					$str_message = "Purchase Product expire dete updated successfully.";
					break;
				case("D"):
					$str_message = "Product Purchase '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' details deleted successfully.";
					break;
				case("V"):
					$str_message = "Product Purchase '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' login mode changed to '". $str_mode . "' successfully.";
					break;
			}
		}
#------------------------------------------------------------------------------------------------
?><!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_MONTHLY_SALES_REPORT);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> 
<a name="ptop" id="ptop"></a>
<div class="container">
	<?php include("../includes/adminheader.php"); ?>
	<div class="row">
		<div class="col-md-2 col-sm-6 col-xs-12 button_space">
			<div class="btn-group" role="group" aria-label="...">
				
				<a href="./yearly_transaction_history.php" class="btn btn-default" title="<?php print($STR_YEARLY_SALES_REPORT);?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_YEARLY_SALES_REPORT);?></a>
				
			</div>
		</div>
		<div class="col-md-10 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_MONTHLY_SALES_REPORT);?>&nbsp;[<?php print(MonthName($int_month));?>&nbsp;-&nbsp;<?php print($int_year);?>] </h3></div>
	</div><hr> 
	<?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); } ?>
	
	
	<div class="panel panel-default">
		 <div class="panel-heading">
              	<h4 class="panel-title">
					<div class="row">
						<div class="col-md-12  padding-10">
							<div class="col-md-6 col-sm-6 col-xs-8"><strong><?php print($STR_FILTER_CRITERIA);?></strong></div>
							<div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>												
						</div>
					</div>
				</h4>
             </div>
	 	<div class="panel-body">
				<div class="col-md-12" align="center">
					<form name="frm_rpt_filter" method="get" action="./monthly_transaction_history.php" >
                                            <label>Select&nbsp;&nbsp;</label>
                                            <label>
                                                <?php 
								/*$str_query_seller="SELECT modelpkid, modelname, shortenurlkey ";
								$str_query_seller.=" FROM t_model ";
								$str_query_seller.=" WHERE approved='YES' AND visible='YES' ";
								$str_query_seller.=" ORDER BY modelname ";*/				
								
								$str_query_seller="SELECT DISTINCT(p.modelpkid), m.shortenurlkey";
								$str_query_seller.=" FROM t_product_purchase p ";
								$str_query_seller.=" LEFT JOIN t_model m ON m.modelpkid=p.modelpkid ";
								//$str_query_seller.=" WHERE purchasemonth=".$int_month." AND purchaseyear=".$int_year;
								$str_query_seller.=" ORDER BY shortenurlkey ASC";
								$rs_seller=GetRecordSet($str_query_seller);
								?>
								
								<select name="cbo_model" id="cbo_model" class="form-control input-sm">
									<option value="0" >--- ALL RECIPIENTS ---</option>
									<?php 
									$int_prev_catpkid=0;
									while(!$rs_seller->eof())
									{ ?>
									<option value="<?php print($rs_seller->fields("modelpkid")); ?>" <?php print(CheckSelected($rs_seller->fields("modelpkid"),$int_model_pkid)); ?> ><?php print($rs_seller->fields("shortenurlkey")); ?></option>
									<?php
										$rs_seller->movenext();
									}
									?>
								</select>
						
                                            </label>
                                            <label>&nbsp;&nbsp;for&nbsp;&nbsp;</label>
						<label><?php print(DisplayMonth($int_month)); ?></label>
						<label>&nbsp;&nbsp;&&nbsp;&nbsp;</label>
						<label><?php print(DisplayYear($int_year)); ?></label>
						&nbsp;&nbsp;
						<label><button type="submit" name="btn_submit" class="btn btn-warning btn-sm">View Report</button></label>
					</form>
			</div>	 
		</div>
	</div>
	<div class="table-responsive">
	<form name="frm_list" action="./action_page.php" method="post" onSubmit="javascript: validate_submit_fomrm();"   >
            <table class="table table-striped table-bordered ">
            <thead>
            	<tr>
                    <th width="4%">Sr. #</th>
                    <th width="10%">Transaction Date</th>
                    <th width="">Details</th>
                    <th width="10%">Member <br/>Amount<br><span class="text-help">(in US$)</span></th>	
                    <th width="10%">Admin <br/>Amount<br><span class="text-help">(in US$)</span></th>	
                    <th width="10%">(-) Processing Fees<br><span class="text-help">(in US$)</span></th>	
                    <?php /* ?><th width="10%">(-) .55 Deduction<br><span class="text-help">(in US$)</span></th><?php */ ?>
                    <th width="12%">Total <br/>Amount<br><span class="text-help">(in US$)</span></th>	
                </tr>
            </thead>
                <tbody>
                <?php if($rs_purchase_history_list->EOF()==true)  {  ?>
                            <tr><td colspan="7" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                        <?php }
                        else {

                        $int_total_amount_model_without_shipping_and_promo=0; 
                        $int_total_amount_model=0;
                        $int_total_amount_admin=0;
                        $int_total_card_processing_fees=0;
                        $int_total_amount=0;
                        $int_total_shipping_charge=0;
                        $int_total_promo_code=0;
                        $int_cnt=1;
                        while(!$rs_purchase_history_list->EOF()==true) { ?>
                        <tr>
                                <td align="center" class="text-align"><?php print($int_cnt);?></td>
                                <td align="center" >
                                    <i class="text-primary"><?php print(date("d-M-Y h:i:s A",strtotime($rs_purchase_history_list->fields("purchasedatetime"))));?></i>
                                </td>
                                <td align="left" style="vertical-align:text-top">
				<?php $sel_mdl_details = ""; 
                               	$sel_mdl_details = "SELECT shortenurlkey FROM t_model WHERE modelpkid = ".$rs_purchase_history_list->fields("modelpkid"); 
                                $rs_details = GetRecordSet($sel_mdl_details);
                                ?>  
                                Profile ID: <b><a href="<?php print $STR_SITE_NAME_WITH_PROTOCOL.$rs_details->Fields("shortenurlkey"); ?>"><?php print $rs_details->Fields("shortenurlkey");?></a></b><br/><br/>
                                <?php //print "<b>Transaction details of</b> <span class='Heading10ptBold'> ".$rs_purchase_history_list->fields("shortenurlkey")."</span><br/>"; ?>
									
                                <?php 
                                $str_query_select = "SELECT * FROM t_product_purchase ";
                                $str_query_select .= " WHERE purchasedatetime=  '".$rs_purchase_history_list->fields("purchasedatetime")."'";
                                $str_query_select .= " AND modelpkid=".$rs_purchase_history_list->fields("modelpkid");
                                $str_query_select .= " ORDER BY purchasedatetime DESC ";
                                $rs_item_list=GetRecordset($str_query_select);

                                $str_emailid = "";
                                $str_phoneno = "";
                                $str_firstname = "";
                                $str_lastname = "";
                                $str_address = "";
                                $str_zipcode = "";
                                $str_city = "";
                                $str_state = "";
                                $str_country = "";
                                $str_ipaddress = "";
                                $str_subscriptionid = "";
                                $str_shippingaddress = "";

                                $int_prod_cnt=1;
                                while(!$rs_item_list->EOF()) 
                                { 
                                        //print "<br/><span class='FieldCaptionText10ptBold'>".$int_prod_cnt.".</span>"; 
                                        print "<b><span class='text-help'>Item Info:</span></b><br/>"; 
                                        if($rs_item_list->fields("producttitle")!="") { print("<span class='text-help'>Name:</span> <span class='FieldCaptionText10ptNormal'>".$rs_item_list->fields("producttitle"))."</span>";}
                                        if($rs_item_list->fields("price")!="") { print(" | <span class='text-help'>Price:</span> <span class='FieldCaptionText10ptNormal'>".GetPriceValue($rs_item_list->fields("price")))."</span>";}
                                        //if($rs_item_list->fields("quantity")!="") { print(" | <span class='text-help'>Qty:</span> <span class='FieldCaptionText10ptNormal'>".$rs_item_list->fields("quantity"))."</span>";}
                                        //if($rs_item_list->fields("extendedprice")!="") { print(" | <span class='text-help'>Total:</span> <span class='FieldCaptionText10ptNormal'>".GetPriceValue($rs_item_list->fields("extendedprice")))."</span>";}
                                        if($rs_item_list->fields("color")!="") { print(" <br/> <span class='text-help'>Color:</span> <span class='HighLightText10ptNormal'>".$rs_item_list->fields("color"))."";}
                                        if($rs_item_list->fields("size")!="") { print(" | <span class='text-help'>Size:</span> <span class='HighLightText10ptNormal'>".$rs_item_list->fields("size"))."";}
                                        if($rs_item_list->fields("typemodel")!="") { print(" | <span class='text-help'>Type / Model:</span> <span class='HighLightText10ptNormal'>".$rs_item_list->fields("typemodel"))."";}
                                        print "<br/><span class='FieldCaptionText10ptBold'></span>"; 

                                        $str_emailid = $rs_item_list->fields("emailid");
                                        $str_phoneno = $rs_item_list->fields("phoneno");
                                        $str_firstname = $rs_item_list->fields("firstname");
                                        $str_lastname = $rs_item_list->fields("lastname");
                                        $str_address = $rs_item_list->fields("address");
                                        $str_zipcode = $rs_item_list->fields("zipcode");
                                        $str_city = $rs_item_list->fields("city");
                                        $str_state = $rs_item_list->fields("state");
                                        $str_country = $rs_item_list->fields("country");
                                        $str_ipaddress = $rs_item_list->fields("ipaddress");
                                        $str_subscriptionid = $rs_item_list->fields("subscriptionid");
                                        $str_shippingaddress = $rs_item_list->fields("shippingaddress");
                                        $int_actual_amount = $rs_item_list->fields("price");

                                        $rs_item_list->MoveNext(); 
                                        $int_prod_cnt=$int_prod_cnt+1;
                                } 

                                        print "<br/><b><span class='text-help'>Payee Info:</span></b><br/>";

					$sel_mem_details = ""; 
                                        $sel_mem_details = "SELECT shortenurlkey FROM t_freemember WHERE pkid = ".$rs_purchase_history_list->fields("memberpkid"); 
                                        $rs_mem_details = GetRecordSet($sel_mem_details);
                                      	?>  
                                    	Profile ID: <b><a href="<?php print $STR_SITE_NAME_WITH_PROTOCOL.$rs_mem_details->Fields("shortenurlkey"); ?>"><?php print $rs_mem_details->Fields("shortenurlkey");?></a></b><br/>
  					<?php 
                                        if($str_emailid!="") { print(" <span class='text-help'>Email:</span> <span class=''>".$str_emailid)."";}
                                        if($str_phoneno!="") { print(" | <span class='text-help'>Phone:</span> <span class='HighLightText10ptNormal'>".$str_phoneno)."";}
                                        if($str_firstname!="") { print(" | <span class='text-help'>Name:</span> <span class='HighLightText10ptNormal'>".$str_firstname)."";}
                                        if($str_lastname!="") { print(" ".$str_lastname)."";}
                                        if($str_address!="") { print(" | <span class='text-help'>Address:</span> <span class='HighLightText10ptNormal'>".$str_address)."";}
                                        if($str_zipcode!="") { print(" | <span class='text-help'>Zipcode:</span> <span class='HighLightText10ptNormal'>".$str_zipcode)."";}
                                        if($str_city!="") { print(" | <span class='text-help'>City:</span> <span class='HighLightText10ptNormal'>".$str_city)."";}
                                        if($str_state!="") { print(" | <span class='text-help'>State:</span> <span class='HighLightText10ptNormal'>".$str_state)."";}
                                        if($str_country!="") { print(" | <span class='text-help'>Country:</span> <span class='HighLightText10ptNormal'>".$str_country)."";}
                                        if($str_ipaddress!="") { print(" | <span class='text-help'>IP:</span> <span class='HighLightText10ptNormal'>".$str_ipaddress)."";}
                                        if($str_subscriptionid!="") { print(" | <span class='text-help'>Subscription ID:</span> <span class='HighLightText10ptNormal'>".$str_subscriptionid)."";}
                                        if($str_shippingaddress!="") { print(" <br/> <span class='text-help'>Shipping Address:</span> <span class='HighLightText10ptNormal'>".$str_shippingaddress)."";}
                                        print "<br/><span class='FieldCaptionText10ptNormal'></span>";									
                                ?>
                                </td>
                                <?php /*?><td align="center">
                                      <?php $sel_mdl_details = ""; 
                                        $sel_mdl_details = "SELECT shortenurlkey FROM t_model WHERE modelpkid = ".$rs_purchase_history_list->fields("modelpkid"); 
                                        $rs_details = GetRecordSet($sel_mdl_details);
                                        
                                      ?>  
                                    <a href="<?php print $STR_SITE_NAME_WITH_PROTOCOL.$rs_details->Fields("shortenurlkey"); ?>"><?php print $rs_details->Fields("shortenurlkey");?></a>
                                </td><?php */?>
                                <?php /*?><td align="center">
                                      <?php $sel_mem_details = ""; 
                                        $sel_mem_details = "SELECT shortenurlkey FROM t_freemember WHERE pkid = ".$rs_purchase_history_list->fields("memberpkid"); 
                                        $rs_mem_details = GetRecordSet($sel_mem_details);
                                        
                                      ?>  
                                    <a href="<?php print $STR_SITE_NAME_WITH_PROTOCOL.$rs_mem_details->Fields("shortenurlkey"); ?>"><?php print $rs_mem_details->Fields("shortenurlkey");?></a>
                                </td><?php */?>

				<?php 
				if($rs_purchase_history_list->EOF()==false)
				{
					$str_query_select = "SELECT splitamount, processingfees FROM t_model WHERE modelpkid=".$rs_purchase_history_list->fields("modelpkid");
					$rs_model=GetRecordset($str_query_select);
										
					if($rs_model->EOF()==false)
					{
						$int_splitamount = $rs_model->fields("splitamount");
						$int_processingfees = $rs_model->fields("processingfees");
						/*while(!$rs_model->eof())
						{
							print ("<br/>SPLIT: ".$int_splitamount."<br/>PROCESSING FEES: ".$int_processingfees."<br/><br/>");
							$rs_model->movenext();
						}*/
					}
					else
					{
						$int_splitamount = 1;
						$int_processingfees = 1;
					}
				}
				else
				{
					$int_splitamount = 1;
					$int_processingfees = 1;
				}
				?>
				<?php
				$int_gross_price_after_split_admin = ((100-$int_splitamount) * $rs_purchase_history_list->fields("gross_price"))/100;
				$int_gross_price_after_card_fees = ($int_processingfees * $rs_purchase_history_list->fields("gross_price"))/100;
				$int_gross_price_after_split_model = $rs_purchase_history_list->fields("gross_price") - $int_gross_price_after_split_admin - $int_gross_price_after_card_fees;
				// $int_gross_price_after_all_deduction = $rs_purchase_history_list->fields("gross_price") -  $int_gross_price_after_split_admin - $int_gross_price_after_card_fees;
				?>

				<td align="right" valign="middle"  class="DescriptionText10ptNormal">
					<?php 
					//print(number_format($int_gross_price_after_all_deduction,2,".",",")."&nbsp;");
					print("<span class='Heading10ptBold'>".number_format($int_gross_price_after_split_model,2,".",",")."&nbsp;</span><br/><span class='HelpText'>(".($int_splitamount - $int_processingfees)."%)</span>&nbsp;");
					?>
				</td>
				<td align="right" valign="middle">
					<?php // print(number_format($int_gross_price_after_split_admin,2,".",",")."&nbsp;"); ?>
					<?php print("<span class='Heading10ptBold'>".number_format($int_gross_price_after_split_admin,2,".",",")."&nbsp;</span><br/><span class='HelpText'>(".(100 - $int_splitamount)."%)</span>&nbsp;"); ?>
				</td>
				<td align="right" valign="middle">
					<?php // print(number_format($int_gross_price_after_card_fees,2,".",",")."&nbsp;"); ?>
					<?php print("<span class='Heading10ptBold'>".number_format($int_gross_price_after_card_fees,2,".",",")."&nbsp;</span><br/><span class='HelpText'>(".($int_processingfees + 0)."%)</span>&nbsp;"); ?>
				</td>


                                <td align="right">
                                        <?php print("<span class=''><b>".number_format($int_actual_amount,2,".",",")."&nbsp;</b></span><br/>"); ?>
                                        <?php //print $int_actual_amount;?>
                                </td>
                        </tr>
                        <tr></tr>
                        <tr></tr>
                        <tr></tr>
                    <?php
			$int_gross_payable_price_model = $int_gross_price_after_split_model + $rs_purchase_history_list->fields("gross_shippping") - $rs_purchase_history_list->fields("finaldiscountedamount");
                        $int_total_amount_model = $int_total_amount_model + $int_gross_payable_price_model;
                        $int_total_amount_model_without_shipping_and_promo = $int_total_amount_model_without_shipping_and_promo + $int_gross_price_after_split_model;
                        $int_total_amount_admin = $int_total_amount_admin + $int_gross_price_after_split_admin;
                        $int_total_card_processing_fees = $int_total_card_processing_fees + $int_gross_price_after_card_fees;
                        $int_total_amount = $int_total_amount + $rs_purchase_history_list->fields("gross_price");
                        //$int_total_amount = $int_total_amount + $int_actual_amount;
                        $int_total_shipping_charge = $int_total_shipping_charge + $rs_purchase_history_list->fields("gross_shippping");
                        $int_total_promo_code = $int_total_promo_code + $rs_purchase_history_list->fields("finaldiscountedamount");

                        $rs_purchase_history_list->movenext();
                        $int_cnt=$int_cnt+1;
                        }
                        ?>
                        <tr>
                        	<td></td>
                        	<td></td>
                            	<td align="right"><input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>"><strong>Total amount for the month [<?php print(MonthName($int_month));?>&nbsp;-&nbsp;<?php print($int_year);?>] </strong></td>
                            	<?php /*?><td align="right"><?php print("<span class=''><b>$".number_format($int_total_actual_amount,2,".",",")."&nbsp;</b></span><br/>"); ?></td>
                            	<td align="right"><?php print("<span class=''><b>$".number_format($int_total_processing_fees,2,".",",")."&nbsp;</b></span><br/>"); ?></td>
                            	<td align="right"><?php print("<span class=''><b>$".number_format($int_total_deduction_amt,2,".",",")."&nbsp;</b></span><br/>"); ?></td><?php */?>
				<td align="right" valign="middle"><?php // print(number_format($int_total_amount_model,2,".",",")); ?><?php print("<span class='Heading10ptBold'>".number_format($int_total_amount_model_without_shipping_and_promo,2,".",",")."&nbsp;</span>"); ?></td>
				<td align="right" valign="middle"><?php // print(number_format($int_gross_price_after_split_admin,2,".",",")."&nbsp;"); ?>		<?php print("<span class='Heading10ptBold'>".number_format($int_total_amount_admin,2,".",",")."&nbsp;</span>"); ?></td>
				<td align="right" valign="middle"><?php // print(number_format($int_gross_price_after_card_fees,2,".",",")."&nbsp;"); ?><?php print("<span class='Heading10ptBold'>".number_format($int_total_card_processing_fees,2,".",",")."&nbsp;</span>"); ?></td>
                                <td align="right"><?php print("<span class=''><b>".number_format($int_total_amount,2,".",",")."&nbsp;</b></span><br/>"); ?></td>
                        </tr>
                <?php }  ?>					
                </tbody>
            </table>
	</form>	
    </div>
    <?php include "../includes/help_for_list.php"; ?>
</div>
<link href="../includes/bootstrap.min.css" rel="stylesheet">
<link href="../includes/bootstrap_theme.css" rel="stylesheet">
<link href="../includes/admin.css" rel="stylesheet">

<script language="JavaScript" src="../includes/functions.js" type="text/javascript"></script>
<script src="../includes/jquery.min.js"></script>
<script src="../includes/bootstrap.min.js"></script>
</body></html>
