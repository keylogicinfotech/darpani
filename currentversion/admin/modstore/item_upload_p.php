<?php
/*
File Name  :- item_add_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
//ini_set('memory_limit', '512M');
include_once('../../PHPExcel-1.8.1/Classes/PHPExcel/IOFactory.php');
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_file_system.php";
//include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_FILES); exit;
if(isset($_FILES['file_img']['tmp_name']) && $_FILES['file_img']['tmp_name'] != "")
{
    
    $uploadFilePath = $UPLOAD_IMG_PATH.basename($_FILES['file_img']['name']);

    if(file_exists($uploadFilePath))
    {
        DeleteFile($uploadFilePath);
    }
    
    move_uploaded_file($_FILES['file_img']['tmp_name'], $uploadFilePath);



    $inputFileName = $uploadFilePath;
    
    /*check point*/

    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
    
    
    /*$worksheetData = $objReader->listWorksheetInfo($inputFileName);
    $totalRows     = $worksheetData[0]['totalRows'];
    $totalColumns  = $worksheetData[0]['totalColumns'];*/

    $data = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

    //print the result
    /*print "COUNT : ".Count($data)."<br/>";
    print "ROW : ".$totalRows."<br/>";
    print "COLUMN : ".$totalColumns."<br/>";*/
    
    //exit;
    for($i = 2;  $i <= Count($data); $i++):
        /*$str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME." (product, pincode, city, state, region, prepaid, cod, reversepickup, pickup) VALUES('".$data[$i]['B']."', '".$data[$i]['C']."', '".$data[$i]['D']."', '".$data[$i]['E']."', '".$data[$i]['F']."', '".$data[$i]['G']."', '".$data[$i]['H']."', '".$data[$i]['I']."', '".$data[$i]['J']."' )";
        ExecuteQuery($str_query_insert);*/
        //print $str_query_insert."<br/>";
        
        if($data[$i]['A'] > 0) // This will not insert row with blank or 0 value catpkid
        {
            #Insert Query
            $str_query_insert = "";
            $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME."( ";
            $str_query_insert.= "catpkid, subcatpkid, itemcode, title, description, createdatetime, displayasnew, displayashot ,displayasfeatured, displayorder, visible, listprice, ourprice, memberprice, inmonth, inyear, availability, availability_info, approved, dshippingvalue, dshippinginfo, ishippingvalue, ishippinginfo, weight, seotitle, seokeyword, seodescription ";
            $str_query_insert.= ") VALUES (";
            $str_query_insert.= "".trim($data[$i]['A']).", ";
            $str_query_insert.= "".trim($data[$i]['B']).", ";
            $str_query_insert.= "'".trim($data[$i]['C'])."', ";
            $str_query_insert.= "'".trim($data[$i]['D'])."', ";
            $str_query_insert.= "'".ReplaceQuote(trim($data[$i]['E']))."', ";
            $str_query_insert.= "NOW(), ";
            $str_query_insert.= "'".trim($data[$i]['F'])."', ";
            $str_query_insert.= "'".trim($data[$i]['G'])."', ";
            $str_query_insert.= "'".trim($data[$i]['H'])."', ";
            //$str_query_insert.= "'".ReplaceQuote($str_additional_info)."', ";
            $str_query_insert.= "0, ";
            $str_query_insert.= "'YES', ";
            $str_query_insert.= "".trim($data[$i]['I']).", ";
            $str_query_insert.= "".trim($data[$i]['J']).", ";
            $str_query_insert.= "".trim($data[$i]['K']).", ";
            $str_query_insert.= "MONTH(NOW()), ";
            $str_query_insert.= "YEAR(NOW()), ";
            $str_query_insert.= "'".trim($data[$i]['L'])."', ";
            $str_query_insert.= "'".trim($data[$i]['M'])."', ";
            $str_query_insert.= "'YES', ";
            $str_query_insert.= "0, ";
            $str_query_insert.= "'".trim($data[$i]['N'])."', ";
            $str_query_insert.= "0, ";
            $str_query_insert.= "'".trim($data[$i]['O'])."', ";
            $str_query_insert.= "".trim($data[$i]['P']).", ";
            $str_query_insert.= "'".ReplaceQuote(trim($data[$i]['Q']))."', ";
            $str_query_insert.= "'".ReplaceQuote(trim($data[$i]['R']))."', ";
            $str_query_insert.= "'".ReplaceQuote(trim($data[$i]['S']))."') ";
            //print $str_query_insert."<br/>"; //exit;
            ExecuteQuery($str_query_insert);
            
            $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME." ORDER BY pkid DESC LIMIT 0,1";
            $rs_list = GetRecordSet($str_query_select);
            $int_itempkid = $rs_list->Fields("pkid");
            
            //print "<br/><br/>";
            
            if(trim($data[$i]['T']) != "")
            {
                $str_query_insert = "";
                $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_PHOTO."(masterpkid, imageurl, setasfront, visible) VALUES (";
                $str_query_insert = $str_query_insert."".$int_itempkid.", '".ReplaceQuote(trim($data[$i]['T']))."', 'YES', 'YES')";
                //print "T : ".$str_query_insert."<br/>"; //exit;
                ExecuteQuery($str_query_insert);
            }
            if(trim($data[$i]['U']) != "")
            {
                $str_query_insert = "";
                $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_PHOTO."(masterpkid, imageurl, setasfront, visible) VALUES (";
                $str_query_insert = $str_query_insert."".$int_itempkid.", '".ReplaceQuote(trim($data[$i]['U']))."', 'NO', 'YES')";
                //print "U : ".$str_query_insert."<br/>"; //exit;
                ExecuteQuery($str_query_insert);
            }
            if(trim($data[$i]['V']) != "")
            {
                $str_query_insert = "";
                $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_PHOTO."(masterpkid, imageurl, setasfront, visible) VALUES (";
                $str_query_insert = $str_query_insert."".$int_itempkid.", '".ReplaceQuote(trim($data[$i]['V']))."', 'NO', 'YES')";
                //print "V : ".$str_query_insert."<br/>"; //exit;
                ExecuteQuery($str_query_insert);
            }
            if(trim($data[$i]['W']) != "")
            {
                $str_query_insert = "";
                $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_PHOTO."(masterpkid, imageurl, setasfront, visible) VALUES (";
                $str_query_insert = $str_query_insert."".$int_itempkid.", '".ReplaceQuote(trim($data[$i]['W']))."', 'NO', 'YES')";
                //print "W : ".$str_query_insert."<br/>"; //exit;
                ExecuteQuery($str_query_insert);
            }
            //print "<br/><br/>";
            
            $str_color = array();
            $str_color = explode("|",trim($data[$i]['X']));
            if(Count($str_color) > 0)
            {
                foreach($str_color AS $str_color):
                    $str_query_insert = "";
                    $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_COLOR."(masterpkid, itempkid) VALUES (".$str_color.", ".$int_itempkid.")";
                    ExecuteQuery($str_query_insert);
                    //print "COLOR : ".$str_query_insert."<br/>";
                endforeach;
            }
            //print "<br/><br/>";
            
            
            $str_size = array();
            $str_size = explode("|",trim($data[$i]['Y']));
            if(Count($str_size) > 0)
            {
                foreach($str_size AS $str_size):
                    $str_query_insert = "";
                    $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_SIZE."(masterpkid, itempkid) VALUES (".$str_size.", ".$int_itempkid.")";
                    ExecuteQuery($str_query_insert);
                    //print "SIZE : ".$str_query_insert."<br/>";
                endforeach;
            }
            //print "<br/><br/>";
            
            $str_type = array();
            $str_type = explode("|",trim($data[$i]['Z']));
            if(Count($str_type) > 0)
            {
                foreach($str_type AS $str_type):
                    $str_query_insert = "";
                    $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_TYPE."(masterpkid, itempkid) VALUES (".$str_type.", ".$int_itempkid.")";
                    ExecuteQuery($str_query_insert);
                    //print "TYPE : ".$str_query_insert."<br/>";
                endforeach;
            }
            //print "<br/><br/>";
            
            $str_length = array();
            $str_length = explode("|",trim($data[$i]['AA']));
            if(Count($str_length) > 0)
            {
                foreach($str_length AS $str_length):
                    $str_query_insert = "";
                    $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_LENGTH."(masterpkid, itempkid) VALUES (".$str_length.", ".$int_itempkid.")";
                    ExecuteQuery($str_query_insert);
                    //print "LENGTH : ".$str_query_insert."<br/>";
                endforeach;
            }
            //print "<br/><br/>";
            
            $str_occasion = array();
            $str_occasion = explode("|",trim($data[$i]['AB']));
            if(Count($str_occasion) > 0)
            {
                foreach($str_occasion AS $str_occasion):
                    $str_query_insert = "";
                    $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_OCCASION."(masterpkid, itempkid) VALUES (".$str_occasion.", ".$int_itempkid.")";
                    ExecuteQuery($str_query_insert);
                    //print "OCCASION : ".$str_query_insert."<br/>";
                endforeach;
            }
            //print "<br/><br/>";
            
            $str_work = array();
            $str_work = explode("|",trim($data[$i]['AC']));
            if(Count($str_work) > 0)
            {
                foreach($str_work AS $str_work):
                    $str_query_insert = "";
                    $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_WORK."(masterpkid, itempkid) VALUES (".$str_work.", ".$int_itempkid.")";
                    ExecuteQuery($str_query_insert);
                    //print "WORK : ".$str_query_insert."<br/>";
                endforeach;
            }
            //print "<br/><br/>";
            
            $str_fabric = array();
            $str_fabric = explode("|",trim($data[$i]['AD']));
            if(Count($str_fabric) > 0)
            {
                foreach($str_fabric AS $str_fabric):
                    $str_query_insert = "";
                    $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_FABRIC."(masterpkid, itempkid) VALUES (".$str_fabric.", ".$int_itempkid.")";
                    ExecuteQuery($str_query_insert);
                    //print "FABRIC : ".$str_query_insert."<br/>";
                endforeach;
            }
            //print "<br/><br/>";
        }
        
    endfor;
}
else
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}


CloseConnection();
Redirect("item_list.php?msg=S&type=S");
exit();
?>
