<?php 
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------
	
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
    


<!------------------------------------------store color,size and length list-------------------------->
<?php
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_COLOR. " WHERE visible='YES' ORDER BY pkid";
//print $str_query_select;
$rs_list_store_color=GetRecordset($str_query_select);
?>

<?php
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_SIZE. " WHERE visible='YES' ORDER BY pkid";
//print $str_query_select;
$rs_list_store_size=GetRecordset($str_query_select);


$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_LENGTH. " WHERE visible='YES' ORDER BY pkid";
//print $str_query_select;
$rs_list_length=GetRecordset($str_query_select);
?>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    
    <div class="row padding-10">
    <div class="table-responsive col-md-4">
         <h3><?php print($STR_TITLE_PAGE_COLOR);?> </h3>
    <hr> 
        <form name="frm_list_store_purchase" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list_store_color->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list_store_color->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print $rs_list_store_color->fields("pkid"); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list_store_color->fields("title"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list_store_color->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    <div class="table-responsive col-md-4">
        <h3><?php print($STR_TITLE_PAGE_SIZE);?> </h3>
    <hr> 
        <form name="frm_list_store_size" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list_store_size->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list_store_size->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print $rs_list_store_size->fields("pkid"); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list_store_size->fields("title"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list_store_size->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    <div class="table-responsive col-md-4">
            <h3><?php print($STR_TITLE_PAGE_LENGTH);?> </h3>
        <hr> 
        <form name="frm_list_length" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list_length->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list_length->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print $rs_list_length->fields("pkid"); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list_length->fields("title"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list_length->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    </div>
</div>

<!------------------------------------------store work, occasion list-------------------------->
<?php
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_WORK. " WHERE visible='YES' ORDER BY pkid";
//print $str_query_select;
$rs_list_store_work=GetRecordset($str_query_select);

?>
<?php $str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_OCCASION. " WHERE visible='YES' ORDER BY pkid";
//print $str_query_select;
$rs_list_occation=GetRecordset($str_query_select); ?>

<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
            
    <div class="row padding-10">
    <div class="table-responsive col-md-6">
        <h3><?php print($STR_TITLE_PAGE_WORK);?> </h3>
    <hr> 
        <form name="frm_list_store_work" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list_store_work->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list_store_work->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print $rs_list_store_work->fields("pkid"); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list_store_work->fields("title"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list_store_work->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
     <div class="table-responsive col-md-6">
          <h3><?php print($STR_TITLE_PAGE_OCCATION);?> </h3><hr/>
        <form name="frm_list_typeh" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list_occation->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list_occation->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print $rs_list_occation->fields("pkid"); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list_occation->fields("title"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list_occation->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    </div>
    </div>

<!------------------------------------------Fabric, Type List-------------------------->
<?php $str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_FABRIC. " WHERE visible='YES' ORDER BY pkid";
//print $str_query_select;
$rs_list_fabric=GetRecordset($str_query_select); ?>

<?php $str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_TYPE. " WHERE visible='YES' ORDER BY pkid";
//print $str_query_select;
$rs_list_type=GetRecordset($str_query_select); ?>

    <div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
            
    <div class="row padding-10">
    <div class="table-responsive col-md-6">
        <h3><?php print($STR_TITLE_PAGE_FABRIC);?> </h3>
    <hr> 
        <form name="frm_list_length" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list_fabric->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list_fabric->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print $rs_list_fabric->fields("pkid"); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list_fabric->fields("title"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list_fabric->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
        <div class="table-responsive col-md-6">
        <h3><?php print($STR_TITLE_PAGE_TYPE);?> </h3>
     <hr> 
        <form name="frm_list_type" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list_type->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list_type->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print $rs_list_type->fields("pkid"); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list_type->fields("title"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list_type->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    </div>
    </div>
  
    
<!------------------------------------------tailoring service, tailoring option list-------------------------->
<?php
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_TAILORING_SERVICE. " WHERE visible='YES' ORDER BY pkid";
//print $str_query_select;
$rs_list_tailoring_service=GetRecordset($str_query_select);
?>

<?php 

$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_TAILORING_OPTION. " WHERE visible='YES' ORDER BY pkid";
//print $str_query_select;
$rs_list_tailoring_option=GetRecordset($str_query_select);
?>
    <div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
            
    <div class="row padding-10">
    <div class="table-responsive col-md-6">
        <h3><?php print($STR_TITLE_PAGE_TAILORING_SERVICE);?> </h3>
        <hr> 
        <form name="frm_list_tailoring services" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list_tailoring_service->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list_tailoring_service->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print $rs_list_tailoring_service->fields("pkid"); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list_tailoring_service->fields("title"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list_tailoring_service->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    <div class="table-responsive col-md-6">
        <h3><?php print($STR_TITLE_PAGE_TAILORING_OPTIONS);?> </h3><hr/>
        <form name="frm_list_tailoring options" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list_tailoring_option->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list_tailoring_option->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print $rs_list_tailoring_option->fields("pkid"); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list_tailoring_option->fields("title"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list_tailoring_option->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    </div>
    </div>
    
<!------------------------------------------Shipping Options, Promocode List-------------------------->
<?php $str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_SHIPPING. " WHERE visible='YES' ORDER BY pkid";
//print $str_query_select;
$rs_list_shipping_option=GetRecordset($str_query_select); ?>

<?php $str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_PROMOCODE. " WHERE visible='YES' ORDER BY pkid";
//print $str_query_select;
$rs_list_promocode=GetRecordset($str_query_select); ?>
    <div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
           
    <div class="row padding-10">
    <div class="table-responsive col-md-6">
         <h3><?php print($STR_TITLE_PAGE_SHIPPING_OPTION);?> </h3>
    <hr> 
        <form name="frm_list_shipping_option" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list_shipping_option->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list_shipping_option->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print $rs_list_shipping_option->fields("pkid"); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list_shipping_option->fields("title"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list_shipping_option->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    <div class="table-responsive col-md-6">
         <h3><?php print($STR_TITLE_PAGE_PROMOCODE);?> </h3>
    <hr> 
        <form name="frm_list_shipping_option" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list_promocode->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list_promocode->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print $rs_list_promocode->fields("pkid"); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list_promocode->fields("title"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list_promocode->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    </div>
    </div> 
    
    

<!---------------------------------------category, subcategory list--------------------------------------->

<?php
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_CAT. " WHERE visible='YES' ORDER BY catpkid";
//print $str_query_select;
$rs_list=GetRecordset($str_query_select);
?>

<?php 
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_SUBCAT. " WHERE visible='YES' ORDER BY subcatpkid";
//print $str_query_select;
$rs_list_subcat=GetRecordset($str_query_select); ?>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    
    <div class="row">
    
    <div class="table-responsive col-md-6">
        <h3><?php print($STR_TITLE_PAGE_CAT);?> </h3>
    <hr> 
        <form name="frm_list" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print $rs_list->fields("catpkid"); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list->fields("title"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    <div class="table-responsive col-md-6">
        <h3><?php print($STR_TITLE_PAGE_SUBCAT);?> </h3>
        <hr> 
        <form name="frm_list_subcat" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_CAT_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list_subcat->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list_subcat->EOF()==true) {  ?>
                     <tr>
                         <td align="center" class="text-center"><?php print $rs_list_subcat->fields("subcatpkid"); ?></td>
                         <td align="center" class="text-center"><?php print $rs_list_subcat->fields("catpkid"); ?></td>
                        
                        <td align="leflt" >
                            <h4><b><?php print $rs_list_subcat->fields("subcattitle"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list_subcat->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    </div>
</div>
 
<!---------------------------------------item list--------------------------------------->
<?php $str_query_select="";
    $str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE visible='YES' ORDER BY pkid,catpkid,subcatpkid ";
//    print $str_query_select;
    $rs_list_store=GetRecordset($str_query_select);
?>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-12 col-sm-12 col-xs-12" align="right" >
            <h3><?php print("Item List");?> </h3>
        </div>
    </div>
    <hr> 
 
    <div class="table-responsive">
        <form name="frm_list" action="" method="">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_PKID; ?></th>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_CAT_PKID; ?></th>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SUBCAT_PKID; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list_store->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list_store->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print $rs_list_store->fields("pkid"); ?></td>
                        <td align="center" class="text-center"><?php print $rs_list_store->fields("catpkid"); ?></td>
                        <td align="center" class="text-center"><?php print $rs_list_store->fields("subcatpkid"); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list_store->fields("title"); ?></b></h4>
                        </td>
                        
                    </tr>
                    <?php $int_cnt++; $rs_list_store->MoveNext(); } ?>
                    <tr>
                        
                        
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>
