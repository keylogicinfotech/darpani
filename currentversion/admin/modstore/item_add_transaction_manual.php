<?php
#----------------------------------------------------------------------------------------------------
#
#Include Files
include "../includes/validatesession.php";
include "../includes/configuration.php";
include "./item_config.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
$int_model_pkid=0;
if (isset($_GET["cbo_modelpkid"])) { $int_model_pkid = trim($_GET["cbo_modelpkid"]); }

$int_fmember_pkid=0;
if (isset($_GET["cbo_memberpkid"])) { $int_fmember_pkid = trim($_GET["cbo_memberpkid"]); }

$int_procedure_pkid=0;
if (isset($_GET["cbo_procedurepkid"])) { $int_procedure_pkid = trim($_GET["cbo_procedurepkid"]); }

$int_price_pkid=0;
if (isset($_GET["cbo_pricepkid"])) { $int_price_pkid = trim($_GET["cbo_pricepkid"]); }

$int_item_pkid=0;
if (isset($_GET["cbo_item"])) { $int_item_pkid = trim($_GET["cbo_item"]); }
//print $int_item_pkid;exit;

$int_subcat_pkid="";


if(isset($_GET['subcatpkid']))
{
    $int_subcat_pkid=trim($_GET['subcatpkid']);
}
//print $int_subcat_pkid;exit;
$int_itempkid=0;
if(isset($_GET['cbo_item'])) { $int_itempkid=trim($_GET['cbo_item']); }

$int_colorpkid=0;
if (isset($_GET["cbo_color"])) { $int_colorpkid = trim($_POST["cbo_color"]); }

$int_typepkid=0;
if (isset($_GET["cbo_type"])) { $int_typepkid = trim($_POST["cbo_type"]); }

$int_sizepkid=0;
if (isset($_GET["cbo_size"])) { $int_sizepkid = trim($_POST["cbo_size"]); }
#----------------------------------------------------------------------------------------------------	
#	Initialization of variables used for message display.   
		$str_type="";
		$str_message="";
		$str_title="";
		$str_mode="";
		if(isset($_GET['tit']))
		{
			$str_title=trim($_GET['tit']);
		}
		if(isset($_GET['mode']))
		{
			$str_mode=trim($_GET['mode']);
		} 
	#	Get message type.
		if(isset($_GET['type']))
		{
			switch(trim($_GET['type']))
			{
				case("S"): $str_type = "S"; break;
				case("E"): $str_type = "E"; break;
				case("W"): $str_type = "W"; break;
			}
		}
	#	Get message text.
		if(isset($_GET['msg']))
		{
			switch(trim($_GET['msg']))
			{
				case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
				case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
			}
		}
                
                
#--------fetch item value model wise---------
//        $str_query_select="";
//        $str_query_select = "SELECT a.subcatpkid,a.subcattitle,b.title,b.catpkid,count(c.subcatpkid) totalitems ";
//        $str_query_select .= "FROM t_ppd_subcat a ";
//        $str_query_select .= "LEFT JOIN t_ppd_cat b ON a.catpkid= b.catpkid ";
//        $str_query_select .= "LEFT JOIN t_ppd c ON a.subcatpkid=c.subcatpkid  ";
//        $str_query_select .= " GROUP BY a.subcatpkid ";
//        $str_query_select .= "ORDER BY b.displayorder desc,b.title,a.subcattitle ";	
         
        $str_query_select = "SELECT * FROM t_store  WHERE approved='YES' AND visible='YES' ORDER BY title";
        $rs_list_item=GetRecordSet($str_query_select);	
        $str_itemvalue="";
        while(!$rs_list_item->EOF()==true)
        {
                $str_itemvalue=$str_itemvalue.$rs_list_item->fields("modelpkid").",".$rs_list_item->fields("subcatpkid").",'".addslashes($rs_list_item->fields("title"))."',";
                $rs_list_item->MoveNext();
        }
        $str_itemvalue = substr($str_itemvalue,0,strlen($str_itemvalue)-1);
        
//        ------------for color---------
//        $str_query_select = "SELECT * FROM tr_store_photo  WHERE visible='YES' ORDER BY color";
//        $rs_list_color=GetRecordSet($str_query_select);	
//        $str_colorvalue="";
//        while(!$rs_list_color->EOF()==true)
//        {
//                $str_colorvalue=$str_colorvalue.$rs_list_color->fields("pkid").",".$rs_list_color->fields("masterpkid").",'".addslashes($rs_list_color->fields("color"))."',";
//                $rs_list_color->MoveNext();
//        }
//        $str_colorvalue = substr($str_colorvalue,0,strlen($str_colorvalue)-1);
        
        $str_query_select = "SELECT * FROM t_store  WHERE approved='YES' AND visible='YES'";
        $rs_list_item=GetRecordSet($str_query_select);	
        $str_typevalue="";
        while(!$rs_list_item->EOF()==true)
        {
                $str_typevalue=$str_typevalue.$rs_list_item->fields("modelpkid").",".$rs_list_item->fields("subcatpkid").",'".addslashes($rs_list_item->fields("type"))."',";
                $rs_list_item->MoveNext();
        }
        $str_typevalue = substr($str_typevalue,0,strlen($str_typevalue)-1);
        
//        $str_query_select = "SELECT * FROM t_store  WHERE approved='YES' AND visible='YES' ORDER BY title";
//        $rs_list_color=GetRecordSet($str_query_select);	
//        $str_colorvalue="";
//        while(!$rs_list_color->EOF()==true)
//        {
//                $str_colorvalue=$str_colorvalue.$rs_list_item->fields("modelpkid").",".$rs_list_item->fields("subcatpkid").",'".addslashes($rs_list_item->fields("type"))."',";
//                $rs_list_color->MoveNext();
//        }
//        $str_colorvalue = substr($str_colorvalue,0,strlen($str_colorvalue)-1);
        
//        print $str_typevalue;exit;
                    
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_STORE_LIST);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="../includes/bootstrap.min.css" rel="stylesheet">
<link href="../includes/bootstrap-theme.min.css" rel="stylesheet">
<link href="../includes/font-awesome.min.css" rel="stylesheet">

<link href="../includes/admin.css" rel="stylesheet">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include("../includes/adminheader.php"); ?>
    <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="..."></div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12" align="right" ><h3>Store</h3></div>
    </div><hr> 
    <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <div class="row">
                                <div class="col-md-12  padding-10">
                                    <div class="col-md-6 col-xs-8"><i class="glyphicon  glyphicon-plus " ></i>&nbsp;&nbsp;<?php print($STR_TITLE_FORM_ADD); ?></div>
                                    <div class="col-md-6 col-xs-6" align="right"><?php print($STR_LINK_HELP); ?></div>
                                </div>
                            </div>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" <?php /* ?>style="height: 0px;" <?php */ ?>>
                        <div class="panel-body">
                            <form name="frm_add" action="item_add_transaction_manual_p.php" method="POST" enctype="multipart/form-data">
                                <div class="text-help" align="right"><?php print($STR_MANDATORY); ?></div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Model</label><span class="text-help"> *</span>
                                            <?php 
                                            $str_query_select="SELECT modelpkid, shortenurlkey ";
                                            $str_query_select.=" FROM t_model ";
                                            $str_query_select.=" WHERE approved='YES' AND visible='YES' AND shortenurlkey!='' ";
                                            $str_query_select.=" ORDER BY shortenurlkey ASC";				
                                            $rs_list_model=GetRecordSet($str_query_select);				
                                            ?>
                                            <select name="cbo_modelpkid" class="form-control input-sm" onChange="selectcat('sr');" tabindex="1">
                                                    <option value="0" >-- SELECT MODEL --</option>
                                                    <?php 
                                                    while(!$rs_list_model->eof())
                                                    {
                                                    ?>
                                                    <option value="<?php print($rs_list_model->fields("modelpkid")); ?>" <?php print(CheckSelected($rs_list_model->fields("modelpkid"),$int_model_pkid)); ?> ><?php print($rs_list_model->fields("shortenurlkey")); ?></option>
                                                    <?php
                                                    $rs_list_model->movenext();
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Member</label><span class="text-help"> *</span>
                                            <?php 
                                            $str_query_select="SELECT pkid, shortenurlkey ";
                                            $str_query_select.=" FROM t_freemember ";
                                            $str_query_select.=" WHERE approved='YES' AND visible='YES' AND shortenurlkey!=''";
                                            $str_query_select.=" ORDER BY shortenurlkey ASC";				
                                            $rs_list_member=GetRecordSet($str_query_select);				
                                            ?>
                                            <select name="cbo_memberpkid" class="form-control input-sm" tabindex="2">
                                                    <option value="0" >-- SELECT MEMBER --</option>
                                                    <?php 
                                                    while(!$rs_list_member->eof())
                                                    {
                                                    ?>
                                                    <option value="<?php print($rs_list_member->fields("pkid")); ?>" <?php print(CheckSelected($rs_list_member->fields("pkid"),$int_fmember_pkid)); ?> ><?php print($rs_list_member->fields("shortenurlkey")); ?></option>
                                                    <?php
                                                    $rs_list_member->movenext();
                                                    }
                                                    ?>
                                            </select>
                                        </div>
                                    </div>
                        <?php /* ?>            <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Category</label>
                   <?php                    $str_query_select="";
                                            $str_query_select = "SELECT a.subcatpkid,a.subcattitle,b.title,b.catpkid,count(c.subcatpkid) totalitems ";
                                            $str_query_select .= "FROM t_ppd_subcat a ";
                                            $str_query_select .= "LEFT JOIN t_ppd_cat b ON a.catpkid= b.catpkid ";
                                            $str_query_select .= "LEFT JOIN t_ppd c ON a.subcatpkid=c.subcatpkid  ";
                                            $str_query_select .= " GROUP BY a.subcatpkid ";
                                            $str_query_select .= "ORDER BY b.displayorder desc,b.title,a.subcattitle ";
                                            $rs_cat_subcat=GetRecordSet($str_query_select); ?>	
                            
                          <select name="subcatpkid" id="subcatpkid" class="form-control input-sm" onChange="selectcat('sr');">
                                <option class=""  <?php print(CheckSelected("0",$int_subcat_pkid));?> value="0">-- All CATEGORY --</option>
				<?php
                                while(!$rs_cat_subcat->EOF())
                                {
                                    $int_old_archivepkid=$rs_cat_subcat->fields("catpkid");
                                    $int_new_archivepkid=$rs_cat_subcat->fields("catpkid"); ?>
                                    
                                <?php    while($int_old_archivepkid==$int_new_archivepkid && !$rs_cat_subcat->eof()) { ?>
                                        <OPTION class="FieldCaptionText8ptNormal" <?php print(CheckSelected($rs_cat_subcat->fields("subcatpkid"),$int_subcat_pkid));?> VALUE='<?php print($rs_cat_subcat->fields("subcatpkid"));?>'><?php print(MyHtmlEncode($rs_cat_subcat->fields("subcattitle")));?> [<?php print(MyHtmlEncode($rs_cat_subcat->fields("totalitems")));?>]</option>
                                     
                                        
                                    <?php
                                    $rs_cat_subcat->MoveNext();
                                    $int_new_archivepkid = $rs_cat_subcat->fields("catpkid");
                                }?>
                          
                            <?php   }  ?>
                            </select>  
                                        </div>
                                    </div>   <?php */ ?>
                                    
                                    <div class="col-md-2">
<!--                                        <div class="form-group">
                                            <label>State</label><span class="text-help">*</span>
                                            <select class="form-control input-sm" id="cbo_state" name="cbo_state" tabindex="6">
                                                <option value="0">-- Select State --</option>
                                            </select>
                                        </div>-->
                                        <div class="form-group">
                                            <label>Item</label><span class="text-help"> *</span>
                                            <select class="form-control input-sm" id="cbo_item" name="cbo_item" onChange="selecttype('item');" tabindex="3" >
                                                <option value="0" >-- SELECT ITEM --</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
<!--                                        <div class="form-group">
                                            <label>State</label><span class="text-help">*</span>
                                            <select class="form-control input-sm" id="cbo_state" name="cbo_state" tabindex="6">
                                                <option value="0">-- Select State --</option>
                                            </select>
                                        </div>-->
                                        <div class="form-group">
                                            <label>Color</label><span class="text-help"> *</span>
                                            <select class="form-control input-sm" id="cbo_color" name="cbo_color" tabindex="4" >
                                                <option value="0" >-- SELECT COLOR --</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Type</label><span class="text-help"> *</span>
                                            <select class="form-control input-sm" id="cbo_type" name="cbo_type" tabindex="5" >
                                                <option value="0" >-- SELECT TYPE --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Size</label><span class="text-help"> *</span>
                                            <select class="form-control input-sm" id="cbo_size" name="cbo_size" tabindex="6" >
                                                <option value="0" >-- SELECT SIZE --</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <?php /*
                                            $str_query_select="SELECT pkid, title, price ";
                                            $str_query_select.=" FROM t_ccbill_dynamic_price ";
                                            $str_query_select.=" WHERE visible='YES' ";
                                            $str_query_select.=" ORDER BY title ASC";				
                                            $rs_list_price=GetRecordSet($str_query_select);
//                                             
                                         */   ?>
                                    <!--<input type="hidden" name="cbo_pricepkid" value="<?php // print $rs_list_price->Fields("pkid"); ?>">-->
                                    
                                    <?php  /*
                                            $str_query_select="SELECT pkid, title ";
                                            $str_query_select.=" FROM t_procedure ";
                                            $str_query_select.=" WHERE visible='YES' ";
                                            $str_query_select.=" ORDER BY title ASC";				
                                            $rs_list_procedure=GetRecordSet($str_query_select);	
//                                            print $rs_list_procedure->Fields("pkid");exit;
                                        */    ?>
                                        <!--<input type="hidden" name="cbo_procedurepkid" value="<?php // print $rs_list_procedure->Fields("pkid"); ?>">-->
                                            <!--<select name="cbo_procedurepkid" class="form-control input-sm">-->
                                    <?php /* ?>        <select name="cbo_pricepkid" class="form-control input-sm">
                                                    <option value="0" >---- SELECT ITEM ----</option>
                                                    <?php 
                                                    while(!$rs_list_price->eof())
                                                    {
                                                    ?>
                                                    <option value="<?php print($rs_list_price->fields("pkid")); ?>" <?php print(CheckSelected($rs_list_price->fields("pkid"),$int_price_pkid)); ?> ><?php print($rs_list_price->fields("title")); ?></option>
                                                    <?php
                                                    $rs_list_price->movenext();
                                                    }
                                                    ?>
                                            </select>  <?php */ ?>
                                        </div>
                                    <!--</div>-->
                                <!--</div>-->
                                
                                <button type="submit" tabindex="4" name="btn_Add"  id="btn_Add" class="btn btn-success" title="<?php print($STR_BUTTON_TITLE_FORM_ADD);?>" onClick="return frmAdd_Validate(2);" ><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span> Add</button>&nbsp;
                                <button name="btn_reset" id="btn_reset" title="<?php print($STR_BUTTON_TITLE_FORM_RESET);?>" class="btn btn-danger" type="reset" tabindex="5">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "../includes/help_for_list.php"; ?>
</div>
<script language="JavaScript" src="../includes/functions.js" type="text/javascript"></script>
<script src="../includes/jquery.min.js"></script>
<script src="../includes/bootstrap.min.js"></script>
<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
<script type="text/javascript" src="../includes/richetxteditor.js"></script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 100}).panelInstance('ta_desc');
		//new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
  });
		// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
		// convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_desc');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
regionarray=new Array(<?php print($str_itemvalue) ?>)
function selectregion(arg)
{
	if(arg=='sr')
	{
            filter('[value=' + id + ']');
  $('#select2').html(options);
            
		var_pkid=document.frm_filter.cbo_modelpkid.options[document.frm_filter.cbo_dstate.selectedIndex].value;
		document.frm_filter.cbo_item.options.length=0;
	
		$int_itempkid=<?php print($int_itempkid) ?>;
		for(cnt=0;cnt<regionarray.length;cnt=cnt+3)
		{
			if(regionarray[cnt]==var_pkid)
			{
				item_pkid=regionarray[cnt+1];
				item_name=regionarray[cnt+2];
				document.frm_filter.cbo_item.options.length=document.frm_filter.cbo_item.options.length+1;
				document.frm_filter.cbo_item.options[document.frm_filter.cbo_item.options.length-1].value=item_pkid;
				document.frm_filter.cbo_item.options[document.frm_filter.cbo_item.options.length-1].text=item_name;
				if(item_pkid==$int_itempkid)
				{
					document.frm_filter.cbo_item.options[document.frm_filter.cbo_item.options.length-1].selected=true;
				}
			}
		}
	
		if(document.frm_filter.cbo_item.options.length==0)
		{
			document.frm_filter.cbo_item.options.length=document.frm_filter.cbo_item.options.length+1;
			document.frm_filter.cbo_item.options[document.frm_filter.cbo_item.options.length-1].value=0;
			document.frm_filter.cbo_item.options[document.frm_filter.cbo_item.options.length-1].text="No items available";
		}
	}
}

function selectcat(arg)
{
	if(arg=='sr')
	{
		var_pkid=document.frm_add.cbo_modelpkid.options[document.frm_add.cbo_modelpkid.selectedIndex].value;
		document.frm_add.cbo_item.options.length=0;
	
		$int_itempkid=<?php print($int_itempkid) ?>;
		for(cnt=0;cnt<regionarray.length;cnt=cnt+3)
		{
			if(regionarray[cnt]==var_pkid)
			{
				item_pkid=regionarray[cnt+1];
				item_name=regionarray[cnt+2];
				document.frm_add.cbo_item.options.length=document.frm_add.cbo_item.options.length+1;
				document.frm_add.cbo_item.options[document.frm_add.cbo_item.options.length-1].value=item_pkid;
				document.frm_add.cbo_item.options[document.frm_add.cbo_item.options.length-1].text=item_name;
				if(item_pkid==$int_itempkid)
				{
					document.frm_add.cbo_item.options[document.frm_add.cbo_item.options.length-1].selected=true;
				}
			}
		}
	
		if(document.frm_add.cbo_item.options.length==0)
		{
			document.frm_add.cbo_item.options.length=document.frm_add.cbo_item.options.length++;
			document.frm_add.cbo_item.options[document.frm_add.cbo_item.options.length-1].value=0;
			document.frm_add.cbo_item.options[document.frm_add.cbo_item.options.length-1].text="No items available";
		}
	}
}
#--------------------------select type----------------
regioncatarray=new Array(<?php print($str_typevalue) ?>)
function selecttyperegion(arg)
{
	if(arg=='sr')
	{
            filter('[value=' + id + ']');
  $('#select2').html(options);
            
		var_pkid=document.frm_filter.cbo_item.options[document.frm_filter.cbo_dstate.selectedIndex].value;
		document.frm_filter.cbo_type.options.length=0;
	
		$int_typepkid=<?php print($int_typepkid) ?>;
		for(cnt=0;cnt<regioncatarray.length;cnt=cnt+3)
		{
			if(regioncatarray[cnt]==var_pkid)
			{
				item_pkid=regioncatarray[cnt+1];
				item_name=regioncatarray[cnt+2];
				document.frm_filter.cbo_type.options.length=document.frm_filter.cbo_type.options.length+1;
				document.frm_filter.cbo_type.options[document.frm_filter.cbo_type.options.length-1].value=item_pkid;
				document.frm_filter.cbo_type.options[document.frm_filter.cbo_type.options.length-1].text=item_name;
				if(item_pkid==$int_typepkid)
				{
					document.frm_filter.cbo_type.options[document.frm_filter.cbo_type.options.length-1].selected=true;
				}
			}
		}
	
		if(document.frm_filter.cbo_type.options.length==0)
		{
			document.frm_filter.cbo_type.options.length=document.frm_filter.cbo_type.options.length+1;
			document.frm_filter.cbo_type.options[document.frm_filter.cbo_type.options.length-1].value=0;
			document.frm_filter.cbo_type.options[document.frm_filter.cbo_type.options.length-1].text="No items available";
		}
	}
}

function selecttype(arg)
{
	if(arg=='item')
	{
		var_pkid=document.frm_add.cbo_item.options[document.frm_add.cbo_item.selectedIndex].value;
		document.frm_add.cbo_type.options.length=0;
	
		$int_typepkid=<?php print($int_typepkid) ?>;
		for(cnt=0;cnt<regioncatarray.length;cnt=cnt+3)
		{
			if(regioncatarray[cnt]==var_pkid)
			{
				item_pkid=regioncatarray[cnt+1];
				item_name=regioncatarray[cnt+2];
				document.frm_add.cbo_type.options.length=document.frm_add.cbo_type.options.length+1;
				document.frm_add.cbo_type.options[document.frm_add.cbo_type.options.length-1].value=item_pkid;
				document.frm_add.cbo_type.options[document.frm_add.cbo_type.options.length-1].text=item_name;
				if(item_pkid==$int_typepkid)
				{
					document.frm_add.cbo_type.options[document.frm_add.cbo_type.options.length-1].selected=true;
				}
			}
		}
	
		if(document.frm_add.cbo_type.options.length==0)
		{
			document.frm_add.cbo_type.options.length=document.frm_add.cbo_type.options.length++;
			document.frm_add.cbo_type.options[document.frm_add.cbo_type.options.length-1].value=0;
			document.frm_add.cbo_type.options[document.frm_add.cbo_type.options.length-1].text="No items available";
		}
	}
}

<!-- END of function getting states for particular country-->

<!-- START of function to preload states - Add OnLoad evenin BODY tag -->
function PreLoadStates()
{
	selectcat('sr');
}


<?php /* ?>
regionitemarray=new Array(<?php print($str_colorvalue) ?>)
function selectitemregion(arg)
{
	if(arg=='item')
	{
            filter('[value=' + id + ']');
  $('#select2').html(options);
            
		var_pkid=document.frm_filter.cbo_item.options[document.frm_filter.cbo_dstate.selectedIndex].value;
		document.frm_filter.cbo_color.options.length=0;
	
		$int_colorpkid=<?php print($int_colorpkid) ?>;
		for(cnt=0;cnt<regionitemarray.length;cnt=cnt+3)
		{
			if(regionitemarray[cnt]==var_pkid)
			{
				item_pkid=regionitemarray[cnt+1];
				item_name=regionitemarray[cnt+2];
				document.frm_filter.cbo_color.options.length=document.frm_filter.cbo_color.options.length+1;
				document.frm_filter.cbo_color.options[document.frm_filter.cbo_color.options.length-1].value=item_pkid;
				document.frm_filter.cbo_color.options[document.frm_filter.cbo_color.options.length-1].text=item_name;
				if(item_pkid==$int_colorpkid)
				{
					document.frm_filter.cbo_color.options[document.frm_filter.cbo_color.options.length-1].selected=true;
				}
			}
		}
	
		if(document.frm_filter.cbo_color.options.length==0)
		{
			document.frm_filter.cbo_color.options.length=document.frm_filter.cbo_color.options.length+1;
			document.frm_filter.cbo_color.options[document.frm_filter.cbo_color.options.length-1].value=0;
			document.frm_filter.cbo_color.options[document.frm_filter.cbo_color.options.length-1].text="No items available";
		}
	}
}


function selectitem(arg)
{
	if(arg=='item')
	{
                var_pkid=document.frm_add.cbo_item.options[document.frm_add.cbo_item.selectedIndex].value;
		document.frm_add.cbo_color.options.length=0;
	
		$int_colorpkid=<?php print($int_colorpkid) ?>;
		for(cnt=0;cnt<regionitemarray.length;cnt=cnt+3)
		{
			if(regionitemarray[cnt]==var_pkid)
			{
				item_pkid=regionitemarray[cnt+1];
				item_name=regionitemarray[cnt+2];
				document.frm_add.cbo_color.options.length=document.frm_add.cbo_color.options.length+1;
				document.frm_add.cbo_color.options[document.frm_add.cbo_color.options.length-1].value=item_pkid;
				document.frm_add.cbo_color.options[document.frm_add.cbo_color.options.length-1].text=item_name;
				if(item_pkid==$int_colorpkid)
				{
					document.frm_add.cbo_color.options[document.frm_add.cbo_color.options.length-1].selected=true;
				}
			}
		}
	
		if(document.frm_add.cbo_color.options.length==0)
		{
			document.frm_add.cbo_color.options.length=document.frm_add.cbo_color.options.length++;
			document.frm_add.cbo_color.options[document.frm_add.cbo_color.options.length-1].value=0;
			document.frm_add.cbo_color.options[document.frm_add.cbo_color.options.length-1].text="No items available";
		}
	}
}
<?php */ ?>
<!-- END of function getting states for particular country-->

<!-- START of function to preload states - Add OnLoad evenin BODY tag -->
function PreLoadStates()
{
	selectitem('item');
}
</script>
</body>
</html>
