//ADD Form validation
function frm_add_validate()
{
    with(document.frm_add)
    {
        if(trim(txt_name.value) == "")
        {
            alert("Please enter title.");
            txt_name.select();
            txt_name.focus();
            return false;
        }
        
        if((trim(txt_percent.value) == "" && trim(txt_amount.value) == "") || (trim(txt_percent.value) != "" && trim(txt_amount.value) != ""))
        {
            alert("Please Enter Either Discount Percentage Or Discount Amount.");
            txt_percent.select();
            txt_percent.focus();
            return false;
        }

        if(trim(txt_amount.value) == "")
        {
            if(trim(txt_percent.value)>100)
            {
                alert("Please Enter Valid Discount Percentage.");
                txt_percent.select();
                txt_percent.focus();
                return false;
            }
        }
        if(trim(txt_percent.value) == "")
        {
            if(sValidatePrice(trim(txt_amount.value)) == false)
            {
                alert("Please Enter Valid Discount Amount.");
                txt_amount.select();
                txt_amount.focus();
                return false;
            }
        }

    }
    return true;
}
function frm_list_check_displayorder()
{
    with(document.frm_list)
    {
        cnt=hdn_counter.value;
        for(i=1;i<cnt;i++)
        {
            if(trim(eval("txt_displayorder" + i).value) =="")
            {
                alert("Please enter display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }

            if(eval("txt_displayorder" + i).value <0)
            {
                alert("Please enter positive integer for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }

            if(isNaN(eval("txt_displayorder" + i).value))
            {
                alert("Please enter numeric value for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }
        }
    }
    return true;
}



function confirm_delete()
{
    if(confirm("Are you sure you want to delete this detail."))
    {
        if(confirm("Confirm Deletion:Click 'OK' to delete this detail or 'Cancel' to deletion."))
	{
            return true;
	}
    }
    return false;
}

