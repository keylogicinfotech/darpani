<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();

include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php"; 
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_xml.php";

//print_r($_SESSION);exit;
#----------------------------------------------------------------------
#get filter data
$int_cat_pkid = "";
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{   
    $int_cat_pkid = trim($_GET["catid"]);    
}

$str_name_key = "";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_name_key = trim($_GET["key"]); }

# get data for paging
$int_page = "";
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"]) != "" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"]) > 0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page = 1; }


$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page;

//$int_userpkid = $_SESSION["userpkid"]; 
#------------------------------------------------------------------------------------------------
# get Query String Data
$int_field = 0;
if(isset($_GET["cbo_fltr_name"]))
{
    $int_field = trim($_GET["cbo_fltr_name"]);
}

$int_catpkid = 0;
$int_subcatpkid = 0;
$str_itemcode = "";
$str_title = "";
$str_desc = "";
$str_color = "";
$str_size = "";
$str_ptype = "";
$str_availability = "";
$str_availability_status = "";
$int_listprice = 0.00;
$int_ourprice = 0.00;
$int_memberprice = 0.00;
$int_dshipping_value = 0.00;
$str_dshipping_info = "";
$int_ishipping_value = 0.00;
$str_ishipping_info = "";
$str_new = "";
$str_hot = "";
$str_featured = "";
$str_visible = "";
$str_seo_title = "";
$str_seo_keyword = "";
$str_seo_desc = "";
$int_dressmaterialprice = 0;
$int_semistitchedprice = 0;
$int_customizedprice = 0;
$int_weight = 0;

if(isset($_GET["catpkid"])) { $int_catpkid = trim($_GET["catpkid"]); }
if(isset($_GET["subcatpkid"])) { $int_subcatpkid = trim($_GET["subcatpkid"]); }
if(isset($_GET["itemcode"])) { $str_itemcode = trim($_GET["itemcode"]); }
if(isset($_GET["availability"])) { $str_availability = trim($_GET["availability"]); }
if(isset($_GET["avail_status"])) { $str_availability_status = trim($_GET["avail_status"]); }
if(isset($_GET["title"])) { $str_title = trim($_GET["title"]); }
if(isset($_GET["desc"])) { $str_desc = trim($_GET["desc"]); }
if(isset($_GET["color"])) { $str_color = trim($_GET["color"]); }
if(isset($_GET["ptype"])) { $str_ptype = trim($_GET["ptype"]); }
if(isset($_GET["size"])) { $str_size = trim($_GET["size"]); }
if(isset($_GET["listprice"])) { $int_listprice = trim($_GET["listprice"]); }
if(isset($_GET["ourprice"])) { $int_ourprice = trim($_GET["ourprice"]); }
if(isset($_GET["memberprice"])) { $int_memberprice = trim($_GET["memberprice"]); }
        
if(isset($_GET["dshippingval"])) { $int_dshipping_value = trim($_GET["dshippingval"]); }
if(isset($_GET["dshippinginfo"])) { $str_dshipping_info = trim($_GET["dshippinginfo"]); }
if(isset($_GET["ishippingval"])) { $int_ishipping_value = trim($_GET["ishippingval"]); }
if(isset($_GET["ishippinginfo"])) { $str_ishipping_info = trim($_GET["ishippinginfo"]); }

if(isset($_GET["seotitle"])) { $str_seo_title = trim($_GET["seotitle"]); }
if(isset($_GET["seokeyword"])) { $str_seo_keyword = trim($_GET["seokeyword"]); }
if(isset($_GET["seodescription"])) { $str_seo_desc = trim($_GET["seodescription"]); }
if(isset($_GET["weight"])) { $int_weight = trim($_GET["weight"]); }

$str_super = "";
if(isset($_GET["super"])) { $str_super=trim($_GET["super"]); }

$int_subcatpkid = 0;
$str_mode_type = "";
if(isset($_GET["mode"]))
{
    $str_mode_type = trim($_GET["mode"]);
}
if(isset($_GET["cbo_flt_sc"]))
{
    $int_subcatpkid = trim($_GET["cbo_flt_sc"]);
}
#------------------------------------------------------------------------------------------------
/*$int_userpkid = 0;
if(isset($_GET["userpkid"]))
{ 
    $int_userpkid = trim($_GET["userpkid"]);     
}*/
//print $int_masterpkid;exit;

$str_serach1="";
//$str_search_query="";
if(isset($_GET["search1"]))
{ 
	$str_serach1=trim($_GET["search1"]); 
	$search_terms = explode(" ",$str_serach1); //store all words in array
	$count_search_terms = count($search_terms); // count total records in array
}


$str_query_where = "";
if($str_serach1 == "") 
{
	if($int_cat_pkid > 0 && $str_name_key != "")
	{
	    $str_query_where = " WHERE a.subcatpkid=".$int_cat_pkid." AND a.title LIKE '".$str_name_key."%'";
	}
	else if($int_cat_pkid > 0 && $str_name_key == "")
	{
	    $str_query_where = " WHERE a.subcatpkid=".$int_cat_pkid;
	}
	else if($int_cat_pkid == 0 && $str_name_key != "")
	{
	    $str_query_where = " WHERE a.title LIKE '".$str_name_key."%'";
	}
}
else
{
	// To search all entered word(s) as single title	
	$str_query_where = " WHERE a.title LIKE '%".$str_serach1."%'";

	/*
	// To search all entered word(s) separately
	$str_query_where = " WHERE ";
	for($i=0; $i<$count_search_terms; $i++)
	{
		// Search from title and description both
		//$str_query_where .= " b.moduletitle LIKE '%".$search_terms[$i]."%' OR b.instructiontext LIKE '%".$search_terms[$i]."%' ";

		// Search from title only
		$str_query_where .= " a.title LIKE '%".$search_terms[$i]."%' ";

		// Check if previous was last record then do not add 'OR' at last of query sentence
		if($i<$count_search_terms-1)
		{
			$str_query_where .= " OR ";
		}
	}
	*/
}    

/*if($int_userpkid != 0) { 
    $str_query_where = " WHERE a.userpkid = ".$int_userpkid;
}*/

/*if($int_field == "" || !is_numeric($int_field) || $int_field<=0)
{
    $int_field = 0;
    
}
else
    {
        $str_fieldname="";
        if($int_field==1)
        {
                $str_fieldname="a.title";
        }
        else
        {
                $str_fieldname="";
        }
        
        
//        elseif($str_fieldname != "" )
    }*/
	


#------------------------------------------------------------------------------------------------
## For Pagination
$str_query_select = "";
$str_query_select = "SELECT count(*) AS totalrecords ";
$str_query_select .= "FROM ".$STR_DB_TABLE_NAME." a LEFT  JOIN  ".$STR_DB_TABLE_NAME_SUBCAT." b ON a.subcatpkid=b.subcatpkid ";
$str_query_select .= "LEFT  JOIN ".$STR_DB_TABLE_NAME_CAT." c ON b.catpkid=c.catpkid " .$str_query_where." " ;
//$str_query_select .= "LEFT  JOIN tr_store_photo d ON a.pkid = d.masterpkid ";
//$str_query_select .= " .$str_query_where ";
//$str_query_select .= "ORDER BY a.createdatetime DESC,a.displayorder DESC,a.title ASC";
//print $str_query_select; exit;
$rs_list_count = GetRecordSet($str_query_select);

$int_total_records = 0;
$int_total_records = $rs_list_count->Fields("totalrecords");


#setting paging parameter
$int_record_per_page = $INT_RECORD_PER_PAGE;				
$int_total_page=ceil($int_total_records / $int_record_per_page); 
if ($int_page > $int_total_page)
{
    $int_page=$int_total_page;
}
if($int_page < 1)
{
    $int_page=1;
}
$int_limit_start=($int_page -1)* $int_record_per_page;	

#------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT a.*, a.title as itemtitle,b.subcattitle, c.title as cattitle ";
$str_query_select .= "FROM ".$STR_DB_TABLE_NAME." a LEFT  JOIN  ".$STR_DB_TABLE_NAME_SUBCAT." b ON a.subcatpkid=b.subcatpkid ";
$str_query_select .= "LEFT  JOIN ".$STR_DB_TABLE_NAME_CAT." c ON b.catpkid=c.catpkid " .$str_query_where." " ;
//$str_query_select .= "LEFT  JOIN tr_store_photo d ON a.pkid = d.masterpkid ";
//$str_query_select .= " .$str_query_where ";
$str_query_select .= "GROUP BY a.pkid ";
$str_query_select .= "ORDER BY a.createdatetime DESC,a.displayorder DESC,a.title ASC";
$str_query_select .= " LIMIT ".$int_limit_start.",".$int_record_per_page;
//print $str_query_select."<br/><br/><br/><br/><br/><br/><br/>";//exit;
$rs_list = GetRecordSet($str_query_select);

if($rs_list->Count()>0)
{
    $int_srno = ($int_page -1) * $int_record_per_page +1;
}
#------------------------------------------------------------------------------------------------
//$sel_mdl_qry = "SELECT DISTINCT mdl.* FROM t_photoset as tm LEFT JOIN t_model as mdl ON mdl.modelpkid=tm.modelpkid";
//$sel_mdl_qry = "SELECT * FROM t_model WHERE visible='YES' AND approved='YES'";
//$rs_mdl_dropdown_list = GetRecordSet($sel_mdl_qry);
# Initialization of variables used for message display.  


$str_type = "";
$str_message = "";
# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
	case("E"): $str_type = "E"; break;
	case("W"): $str_type = "W"; break;
    }
}
#	Get message text.
if($str_mode_type == "YES") { $str_md = "Visible"; }  else { $str_md = "Invisible"; }
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("DA"): $str_message = $STR_MSG_ACTION_DELETE_ALL; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("IDUP"): $str_message = $STR_MSG_ACTION_DUPLICATE; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_md; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("AP"): $str_message = $STR_MSG_ACTION_PURCHASE; break;
        case("SD"): $str_message = $STR_MSG_ACTION_SET_AS_DEFAULT;break;
        case("DU"): $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
        //case("L"): $str_message = $STR_MSG_ACTION_LOGIN; break;
        case("IE"): $str_message = $STR_MSG_ACTION_INVALID_FILE_EXT; break; 
        case("IU"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break; 
        case("ST"): $str_message = $STR_MSG_ACTION_FILE_SET_AS_TRAILER; break;
        case("BL"): $str_message = $STR_MSG_ACTION_SEL_FILE_TO_UPLOAD; break;
        case("NE"): $str_message = "ERROR!!! You cannot delete this item Because it is already bought by member.";	break;
        case("DN"); $str_message = $STR_MSG_ACTION_DISPLAY_AS_NEW; break;
        case("DF"); $str_message = $STR_MSG_ACTION_DISPLAY_AS_FEATURED; break;
        case("DAI"); $str_message = $STR_MSG_ACTION_DISPLAY_ADDITIONAL_INFO; break;
        case("DH"); $str_message = $STR_MSG_ACTION_DISPLAY_AS_HOT; break;
        case("APR"); $str_message = $STR_MSG_ACTION_APPROVE; break;
        case("NV"): $str_message = $STR_MSG_ACTION_NO_OF_VIEW; break;
    }
} 
#----------------------------------------------------------------------
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_CAT.$STR_DB_TABLE_NAME_ORDER_BY_CAT."";
//print $str_query_select; exit; 
$rs_list_cat = GetRecordSet($str_query_select); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE) ;?></title>   
</head>
<body>
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="container center-bg">
        <a name="ptop" id="ptop"></a>
        <div class="row padding-10">
            <div class="col-md-3 col-sm-6 col-xs-12 button_space">
                <div class="btn-group" role="group" aria-label="...">
                    <a href="../modcategory/item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE_CATEGORY); ?> " ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_CATEGORY); ?> </a>
                <a href="./item_promocode_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE_PROMOCODE); ?> " ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_PROMOCODE); ?> </a>
                </div>
            </div>
            <div class="col-md-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE); ?></h3></div>
        </div><hr/>
        <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); } ?>

	<div class="row padding-10">
            <div class="col-md-12">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <span>
                                    <a class="accordion-toggle collapsed " data-toggle="collapse" title="<?php print $STR_HOVER_ADD; ?>" data-parent="#accordion" href="#collapseU" aria-expanded="false"><b><?php print($STR_TITLE_UPLOAD); ?></b></a>
                                </span>
                                <?php print($STR_LINK_HELP); ?>
                            </h4>
                        </div>
                        <div class="col-md-6">
                        <div id="collapseU" class="panel-collapse collapse in" aria-expanded="false">
                            <div class="panel-body">
                                <form name="frm_add" action="item_upload_p.php" method="post" required enctype="multipart/form-data">
                                    <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="form-group">
                                        <label>Upload File</label><span class="text-help-form"> *</span>
                                        <input type="file" name="file_img"  id="file_img"  maxlength="255" value="">
                                    </div>

                                    <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                </form>
                            </div>
                            </div>
                            <div class="col-md-6">
                            <form method="post" action="download.php" style="margin-left: 379px; margin-top: -76px;">
                            <input type="submit" value="Download" class="btn btn-warning btn-sm">
                           </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row padding-10">
            <div class="col-md-12">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <span>
                                <b><a class="accordion-toggle collapsed" title="<?php print $STR_HOVER_ADD; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><?php print($STR_TITLE_ADD); ?> </a></b>
                                </span>
                                <?php print($STR_LINK_HELP); ?>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" <?php /* ?> style="height: 0px;"<?php */ ?>>
                            <form id="frm_add" name="frm_add" method="POST" action="item_add_p.php" onSubmit="return frm_add_validate();" >
                                <div class="panel-body">
                                    <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Category</label>
                                                <span class="text-help-form"> *</span>
                                                <select id="cbo_cat" name="cbo_cat" class="form-control input-sm" onchange="get_subcat();">
                                                    <option value="0"> -- SELECT CATEGORY -- </option>
                                                    <?php 
                                                    while(!$rs_list_cat->eof())
                                                    { ?>
                                                    <option <?php print CheckSelected($int_catpkid, $rs_list_cat->Fields("catpkid")) ?> value="<?php print $rs_list_cat->Fields("catpkid");?>"><?php print $rs_list_cat->Fields("title");?></option>
                                                    <?php $rs_list_cat->MoveNext();
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="before_get_subcat">
                                                <label>Select Sub Category</label><span class='text-help-form'> *</span>
                                                <select class='form-control input-sm' name='cbo_subcat' id='cbo_subcat'>
                                                    <option value="0">-- SELECT SUB CATEGORY --</option>
                                                </select>
                                            </div>                                            
                                            <div id="get_subcat"></div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Title</label>
                                                <span class="text-help-form"> *</span><input name="txt_title" id="txt_title"  type="text" class="form-control input-sm" maxlength="255" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" value="<?php print(RemoveQuote($str_title)); ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Item Code</label>
                                                <span class="text-help-form"> *</span><input name="txt_code" id="txt_code"  type="text" class="form-control input-sm" maxlength="255" placeholder="<?php print $STR_PLACEHOLDER_TEXT; ?>" value="<?php print(RemoveQuote($str_itemcode)); ?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Weight (in kg)</label><span class="text-help-form"> *</span>
                                                <input type="text" name="txt_weight" id="txt_weight" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" size="60" value="<?php print(trim($int_weight));  ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>List Price</label><span class="text-help-form"> *</span>
                                                <input type="text" name="txt_listprice" id="txt_listprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="<?php print(trim($int_listprice));  ?>" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Our Price</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_ourprice" id="txt_ourprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="<?php print(trim($int_ourprice));  ?>" >        
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Wholesaler Price</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_memberprice" id="txt_memberprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="<?php print(trim($int_memberprice));  ?>" >        
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Description</label><span class="text-help-form"> </span>
                                                <textarea name="ta_desc" id="ta_desc" rows="10" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>" ><?php print(RemoveQuote($str_desc)); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Color</label><span class="text-help-form"> </span><br/>
                                                <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                <div class="row padding-10">
                                                <?php 
                                                $str_query_select = "";
                                                $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_COLOR." WHERE visible='YES' ORDER BY title, displayorder";
                                                $rs_list_color = GetRecordSet($str_query_select);
                                                while(!$rs_list_color->EOF()) { ?>
                                                    <div class="col-md-2">
                                                        <input type="checkbox" id="cbx_color" name="cbx_color[]" value="<?php print $rs_list_color->Fields("pkid"); ?>" />&nbsp;<?php print $rs_list_color->Fields("title"); ?>
                                                    </div>
                                                <?php $rs_list_color->MoveNext();
                                                }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div><hr/>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Size</label><span class="text-help-form"> </span><br/>
                                                <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                <div class="row padding-10">
                                                <?php 
                                                $str_query_select = "";
                                                $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_SIZE." WHERE visible='YES' ORDER BY displayorder";
                                                $rs_list_size = GetRecordSet($str_query_select);
                                                while(!$rs_list_size->EOF()) {
                                                ?>
                                                    <div class="col-md-2">
                                                        <input type="checkbox" id="cbx_size" name="cbx_size[]" value="<?php print $rs_list_size->Fields("pkid"); ?>" />&nbsp;<?php print $rs_list_size->Fields("title"); ?>
                                                    </div>
                                                <?php $rs_list_size->MoveNext();
                                                }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div><hr/>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Length</label><span class="text-help-form"> </span><br/>
                                                <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                <div class="row padding-10">
                                                <?php 
                                                $str_query_select = "";
                                                $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_LENGTH." WHERE visible='YES' ORDER BY displayorder";
                                                $rs_list_length = GetRecordSet($str_query_select);
                                                while(!$rs_list_length->EOF()) {
                                                ?>
                                                    <div class="col-md-2">
                                                        <input type="checkbox" id="cbx_length" name="cbx_length[]" value="<?php print $rs_list_length->Fields("pkid"); ?>" />&nbsp;<?php print $rs_list_length->Fields("title"); ?>
                                                    </div>
                                                <?php $rs_list_length->MoveNext();
                                                }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div><hr/>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Type / Model</label><span class="text-help-form"> </span><br/>
                                                <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                <div class="row padding-10">
                                                <?php 
                                                $str_query_select = "";
                                                $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_TYPE." WHERE visible='YES' ORDER BY displayorder";
                                                $rs_list_type = GetRecordSet($str_query_select);
                                                while(!$rs_list_type->EOF()) {
                                                ?>
                                                    <div class="col-md-2">
                                                        <input type="checkbox" id="cbx_type" name="cbx_type[]" value="<?php print $rs_list_type->Fields("pkid"); ?>" />&nbsp;<?php print $rs_list_type->Fields("title"); ?>
                                                    </div>
                                                <?php $rs_list_type->MoveNext();
                                                }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div><hr/>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Work</label><span class="text-help-form"> </span><br/>
                                                <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                <div class="row padding-10">
                                                <?php 
                                                $str_query_select = "";
                                                $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_WORK." WHERE visible='YES' ORDER BY displayorder";
                                                $rs_list_work = GetRecordSet($str_query_select);
                                                while(!$rs_list_work->EOF()) {
                                                ?>
                                                    <div class="col-md-2">
                                                        <input type="checkbox" id="cbx_work" name="cbx_work[]" value="<?php print $rs_list_work->Fields("pkid"); ?>" />&nbsp;<?php print $rs_list_work->Fields("title"); ?>
                                                    </div>
                                                <?php $rs_list_work->MoveNext();
                                                }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div><hr/>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Fabric</label><span class="text-help-form"> </span><br/>
                                                <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                <div class="row padding-10">
                                                <?php 
                                                $str_query_select = "";
                                                $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_FABRIC." WHERE visible='YES' ORDER BY displayorder";
                                                $rs_list_fabric = GetRecordSet($str_query_select);
                                                while(!$rs_list_fabric ->EOF()) {
                                                ?>
                                                    <div class="col-md-2">
                                                        <input type="checkbox" id="cbx_fabric" name="cbx_fabric[]" value="<?php print $rs_list_fabric->Fields("pkid"); ?>" />&nbsp;<?php print $rs_list_fabric->Fields("title"); ?>
                                                    </div>
                                                <?php $rs_list_fabric->MoveNext();
                                                }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div><hr/>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Occasion</label><span class="text-help-form"> </span><br/>
                                                <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                <div class="row padding-10">
                                                <?php 
                                                $str_query_select = "";
                                                $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_OCCASION." WHERE visible='YES' ORDER BY displayorder";
                                                $rs_list_occasion = GetRecordSet($str_query_select);
                                                while(!$rs_list_occasion->EOF()) {
                                                ?>
                                                    <div class="col-md-2">
                                                        <input type="checkbox" id="cbx_occasion" name="cbx_occasion[]" value="<?php print $rs_list_occasion->Fields("pkid"); ?>" />&nbsp;<?php print $rs_list_occasion->Fields("title"); ?>
                                                    </div>
                                                <?php $rs_list_occasion->MoveNext();
                                                }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div><hr/>
                                    <?php /* ?>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Shipping Options</label><span class="text-help-form"> </span><br/>
                                                <div class="row padding-10">
                                                <?php 
                                                $str_query_select = "";
                                                $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_SHIPPING." WHERE visible='YES' ORDER BY displayorder";
                                                $rs_list_shipping = GetRecordSet($str_query_select);
                                                while(!$rs_list_shipping->EOF()) {
                                                ?>
                                                    <div class="col-md-2">
                                                        <input type="checkbox" id="cbx_shipping" name="cbx_shipping" value="<?php print $rs_list_shipping->Fields("pkid"); ?>"/>&nbsp;<?php print $rs_list_shipping->Fields("title"); ?>
                                                    </div>
                                                <?php $rs_list_shipping->MoveNext();
                                                }?>
                                                </div>
                                            </div>
                                        </div>
                                    </div><hr/>
                                    <div class="row padding-10">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Dress Material Price</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_dressmaterialprice" id="txt_dressmaterialprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="<?php print(trim($int_dressmaterialprice));  ?>" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Semi Stitched Price</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_semistitchedprice" id="txt_semistitchedprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="<?php print(trim($int_semistitchedprice));  ?>" >        
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Customized Price</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_customizedprice" id="txt_customizedprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="<?php print(trim($int_customizedprice));  ?>" >        
                                            </div>
                                        </div>
                                    </div>
                                    <hr/><?php */ ?>
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Availability</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_availability" id="txt_availability" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_TEXT;?>" maxlength="255" value="<?php print(($str_availability));  ?>" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Availability Status</label><span class="text-help-form"> *</span>
                                                <select class="form-control input-sm" id="cbo_avail_status" name="cbo_avail_status">
                                                    <option value="YES" <?php print(CheckSelected("NO",$str_availability_status)); ?>>In Stock</option>
                                                    <option value="NO" <?php print(CheckSelected("NO",$str_availability_status)); ?>>Out Of Stock</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div><hr/>
                                    <div class="row padding-10">
                                        <?php /* ?><div class="col-md-6">
                                            <div class="form-group">
                                                <label>Domestic Shipping Value</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_dshipping_value" id="txt_dshipping_value" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" size="60" value="<?php print(trim($int_dshipping_value));  ?>">
                                            </div>
                                        </div><?php */ ?>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Domestic Shipping Info</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_dshipping_info" id="txt_dshipping_info" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_TEXT;?>"  maxlength="255" size="60" value="<?php print(ReplaceQuote(trim($str_dshipping_info)));  ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <?php /* ?><div class="col-md-6">
                                            <div class="form-group">
                                                <label>International Shipping Value</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_ishipping_value" id="txt_ishipping_value" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" size="60" value="<?php print(trim($int_ishipping_value));  ?>" >
                                            </div>
                                        </div><?php */ ?>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>International Shipping Info</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_ishipping_info" id="txt_ishipping_info" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_TEXT;?>"  maxlength="255" size="60" value="<?php print(ReplaceQuote(trim($str_ishipping_info)));  ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Display As New</label><span class="text-help-form"> *</span>
                                                <span class="text-help-form"> (<?php print($STR_MSG_NEW);?>)</span>
                                                <select id="cbo_new" name="cbo_new" class="form-control input-sm">
                                                    <option value="NO" <?php print(CheckSelected("NO",$str_new)); ?>>NO</option>
                                                    <option value="YES" <?php print(CheckSelected("YES",$str_new));?>>YES</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Display As Featured</label><span class="text-help-form"> *</span>
                                                <span class="text-help-form"> (<?php print($STR_MSG_FEATURED);?>)</span>
                                                <select id="cbo_featured" name="cbo_featured" class="form-control input-sm">
                                                    <option value="NO" <?php print(CheckSelected("NO",$str_featured)); ?>>NO</option>
                                                    <option value="YES" <?php print(CheckSelected("YES",$str_featured));?>>YES</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Display As Hot</label><span class="text-help-form"> *</span>
                                                <span class="text-help-form"> (<?php print($STR_MSG_HOT);?>)</span>
                                                <select id="cbo_hot" name="cbo_hot" class="form-control input-sm">
                                                    <option value="NO" <?php print(CheckSelected("NO",$str_hot)); ?>>NO</option>
                                                    <option value="YES" <?php print(CheckSelected("YES",$str_hot));?>>YES</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Visible</label><span class="text-help-form"> *</span>
                                                <span class="text-help-form"> (<?php print($STR_MSG_VISIBLE);?>)</span>
                                                <select id="cbo_visible" name="cbo_visible" class="form-control input-sm">
                                                    <option value="YES" <?php print(CheckSelected("YES",$str_visible));?>>YES</option>
                                                    <option value="NO" <?php print(CheckSelected("NO",$str_visible)); ?>>NO</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>SEO Title</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_seo_title" id="txt_seo_title" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_SEO_TITLE; ?>"  size="60" value="<?php print $str_seo_title; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>SEO Keywords</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_seo_keywords" id="txt_seo_keywords" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_SEO_KEYWORDS; ?>" size="60" value="<?php print $str_seo_keyword; ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>SEO Description</label><span class="text-help-form"> </span>
                                                <textarea name="ta_seo_desc" id="ta_seo_desc" rows="5" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>" ><?php print(RemoveQuote($str_seo_desc)); ?></textarea>
                                            </div>
                                        </div>
                                    </div>   
                                    <input type="hidden" name="catid" id="catid" value="<?php print($int_cat_pkid);?>">
                                    <input type="hidden" name="key" id="key" value="<?php print($str_name_key);?>">
                                    <input type="hidden" name="PagePosition" id="PagePosition" value="<?php print($int_page);?>">
                                    <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                </div>
                            </form>
                        </div>
                    </div>                        
                </div>
            </div>
        </div>
        <div class="row padding-10">
            <div class="col-md-12" align="left">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="nopadding">
                            <strong><?php print $STR_TITLE_FILTER_CRITERIA; ?></strong>
                        </h4>
                    </div>
                    <div class="panel-body">

			<?php /* ?><div align="center">
				<script language="JavaScript" type="text/javascript">
					function validate_search_by_keyword_1()
					{
						with (document.frm_search1)
						{
							if (search1.value=="")
							{
								alert("Please type some text to search");
								search1.focus();
								return false;			
							}
						}
						return true;
					}
				</script>
				<form name="frm_search1" action="item_list.php" method="GET" onSubmit="javascript: return validate_search_by_keyword_1();" >
					<label>Search</label>
                                        <label><input type="text" name="search1" class="form-control input-sm" maxlength="255" placeholder="<?php print $STR_PLACEHOLDER_TEXT; ?>" onFocus="this.placeholder=''" onBlur="this.placeholder='search here'"></label>
					<label>&nbsp;<?php print DisplayFormButton('VIEW', 0)?></label>
				</form>
			</div><?php */ ?>

                        <div align="center">
                            <?php
                            #For Filter Criteria
                            $str_query_select = "";
                            $str_query_select = "SELECT a.subcatpkid,a.subcattitle,b.title AS cattitle,b.catpkid,count(c.subcatpkid) totalitems ";
                            $str_query_select .= "FROM ".$STR_DB_TABLE_NAME_SUBCAT." a ";
                            $str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." b ON a.catpkid = b.catpkid ";
                            $str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME." c ON a.subcatpkid=c.subcatpkid AND c.visible='YES' AND c.approved='YES' ";
                            $str_query_select .= "GROUP BY a.subcatpkid ";
                            $str_query_select .= "ORDER BY b.displayorder desc,b.title,a.subcattitle ";
                            
                            $rs_list_cat = GetRecordSet($str_query_select); //print $rs_list_master_filter_criteria->Count();  ?>
                            <form name="frm_filter" method="GET" action="item_list.php" class="nopadding">

				
				<label>Search Item Here</label>
				<label><input type="text" name="search1" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_TEXT; ?>" onFocus="this.placeholder=''" onBlur="this.placeholder='search here'"></label>
				<label>&nbsp;<?php print DisplayFormButton('SEARCH', 0)?></label>
				<?php if($str_serach1 != "") { ?>&nbsp;<label><a href="item_list.php" title="">Cancel Search</a></label><?php } ?> 
				<?php if($str_serach1 != "") { ?><br/><label><h4 class="text text-danger">Below is search result for '<b><?php print $str_serach1; ?></b>'</h4></label><?php } ?>
				<br/><b>OR</b><br/>

                                <label>Select </label>
                                <label>
                                    <select name="catid" id="catid" class="form-control input-sm">
                                        <option value="0"><?php print($STR_TITLE_DROPDOWN_CAT); ?></option>
                                        <?php
                                        while(!$rs_list_cat->EOF())
                                        {
                                            $int_old_catpkid = $rs_list_cat->fields("catpkid");
                                            $int_new_catpkid = $rs_list_cat->fields("catpkid"); ?>
                                            <optgroup class="FieldValueText8ptBold" label="<?php print(MyHtmlEncode($rs_list_cat->fields("cattitle")));?>">
                                            <?php
                                            while($int_old_catpkid==$int_new_catpkid && !$rs_list_cat->eof())
                                            { ?>
                                                <OPTION class="FieldCaptionText8ptNormal" <?php print(CheckSelected($rs_list_cat->fields("subcatpkid"),$int_cat_pkid));?> VALUE='<?php print($rs_list_cat->fields("subcatpkid"));?>'><?php print(MyHtmlEncode($rs_list_cat->fields("subcattitle")));?> (<?php print(MyHtmlEncode($rs_list_cat->fields("totalitems")));?>)</OPTION>
                                            <?php
                                            $rs_list_cat->MoveNext();
                                            $int_new_catpkid = $rs_list_cat->fields("catpkid");
                                            }?></optgroup>
                                        <?php
                                        }?>
                                    </select>
                                </label>
                                <label>
                                <label>&nbsp;where&nbsp;</label>  
                                <label>
                                <select name="cbo_fltr_name" class="form-control input-sm">
                                    <option value="1" <?php print(CheckSelected("1",$int_field));?>>Item Name</option>
                                </select>
                                </label>
                                <label>&nbsp;start with&nbsp;</label>
                                    <label>
                                        <select name="key" class="form-control input-sm" >
                                            <option value=""><?php print($STR_TITLE_DROPDOWN_ALPHABET); ?></option>
                                            <?php 
                                            for($i=65;$i<91;$i++)
                                            {
                                                if($str_name_key != chr($i))
                                                { $select_key = ""; }
                                                else
                                                { $select_key="selected"; }?>
                                                <option value = "<?php print(chr($i));?>" <?php print($select_key);?>><?php print(chr($i));?></option>
                                    <?php   } ?>		
                                        </select>			
                                    </label>
                                <label>&nbsp;<?php print DisplayFormButton('VIEW', 0)?></label>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row padding-10">
            <div class="col-md-12">
                <div class="table-responsive">
                    <form name="frm_list" action="item_order_p.php" method="POST" onSubmit="return frm_list_check_displayorder();">
                        <table class="table table-striped table-bordered ">
                            <thead>
                                <tr>
                                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                                    <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_DATE; ?></th>
                                    <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_CATEGORY_SUBCATEGORY; ?></th>
                                    <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_MANAGE_OPTIONS; ?></th>
                                    <?php /* ?><th width="4%"><?php print $STR_TABLE_COLUMN_NAME_ALLOW_PURCHASE; ?></th><?php */ ?>
                                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_AS_NEW; ?><?php if(!$rs_list->eof()) {?><br/><input type="checkbox" onClick="return checkallnew();" id="chk_allnew" name="chk_allnew" ><?php } ?></th>
                                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_AS_HOT; ?><?php if(!$rs_list->eof()) {?><br/><input type="checkbox" onClick="return checkallhot();" id="chk_allhot" name="chk_allhot" ><?php } ?></th>
                                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_AS_FEATURED; ?><?php if(!$rs_list->eof()) {?><br/><input type="checkbox" onClick="return checkallfeatured();" id="chk_allfeatured" name="chk_allfeatured" ><?php } ?></th>
                                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_ADDITIONAL; ?></th>
                                    <?php /* ?><th width="4%"><?php print $STR_TABLE_COLUMN_NAME_NO_OF_VIEW; ?></th><?php */ ?>
                                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_ORDER; ?></th>
                                    <th width="5%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?><?php if(!$rs_list->eof()) {?><br/><input type="checkbox" onClick="return checkalldelete();" id="chk_alldelete" name="chk_alldelete" ><?php } ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if($rs_list->Count() <= 0)
                                { ?>
                                    <tr>
                                        <td colspan="12" align="center" class="alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE);?></td>
                                    </tr>
                                <?php 
                                } else {
                                    $int_cnt = 1;    
                                    while(!$rs_list->EOF()) 
                                    { ?>
                                    <tr>
                                        <td align="center"><?php print $int_srno; ?><input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>"></td>
                                        <td align="center" class="text-help">
					    <a name="scroll<?php print($rs_list->fields("pkid")); ?>" id="scroll<?php print($rs_list->fields("pkid")); ?>"></a>
                                            <p><?php print $STR_TITLE_DATE_CREATED; ?><br/><i class=""><?php print(DDMMMYYYYFormat($rs_list->fields("createdatetime"))); ?></i></p>
                                            <?php if($rs_list->fields("lastupdatedatetime") != "" && $rs_list->fields("lastupdatedatetime") != "1970-01-01 00:00:00") { ?>
                                            <hr/>
                                            <p class="nopadding"><?php print $STR_TITLE_DATE_UPDATED; ?><br/><i class=""><?php print(DDMMMYYYYHHIISSFormat($rs_list->fields("lastupdatedatetime"))); ?></i></p>
                                            <?php } ?>
                                        </td>
                                        <td align="center">
                                            <?php print "<b>".$rs_list->Fields("cattitle")."</b>"; ?>
                                            <?php print  "<br/>".$rs_list->Fields("subcattitle"); ?>
                                        </td>
                                        <td align="left" class="aligh-top">
                                            
                                            <?php 
                                            $str_query_select = "";
                                            $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE setasfront='YES' AND masterpkid=".$rs_list->Fields("pkid");
                                            $rs_list_photo = GetRecordSet($str_query_select);
                                            if($rs_list_photo->Count() > 0)
                                            {?>
                                               <?php if($rs_list_photo->fields("thumbphotofilename")!="") { ?>
                                                <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list->fields("pkid")); ?>" rel="thumbnail"><img src="<?php print($UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list_photo->fields("thumbphotofilename"));?>"  class="img-responsive" border="0" align="absmiddle" title="<?php print $rs_list->fields("title"); ?>" alt="<?php print $rs_list->fields("title"); ?>" /></a>
                                                <div class="modal fade f-pop-up-<?php print($rs_list->fields("pkid")); ?> " align="center" role="dialog" aria-labelledby="" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br/><br/>
                                                                <img src="<?php print($UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list_photo->fields("largephotofilename"));?>"  class="img-responsive" border="0" align="absmiddle" title="<?php print $rs_list->fields("title"); ?>" alt="<?php print $rs_list->fields("title"); ?>" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><p></p>
                                        <?php   } else if($rs_list_photo->fields("imageurl") != "") { ?>
                                                <img src="<?php print($rs_list_photo->fields("imageurl"));?>"  class="img-responsive" border="0" align="absmiddle" title="<?php print $rs_list->fields("title"); ?>" alt="<?php print $rs_list->fields("title"); ?>" />
                                        <?php   }  ?>        
                                                
                                    <?php   } ?>
                                            <?php if($rs_list->fields("title") != "") { ?>
                                                <h4 class=""><b><?php print(MyhtmlEncode($rs_list->fields("title"))) ?></b>&nbsp;&nbsp;<span class="small"><?php if($rs_list->fields("availability") == "YES") { print $STR_ICON_PATH_AVAILABLE; } else { print $STR_ICON_PATH_NOTAVAILABLE; } ?></span></h4>
                                            <?php } ?>   
                                            <?php if($rs_list->fields("itemcode") != "") { ?>
                                                <p><span class="text-help">Item Code:</span>&nbsp;<?php print "".$rs_list->fields("itemcode"); ?></p>
                                            <?php } ?>
                                            <?php if($rs_list->fields("listprice") > 0) { ?>
                                            <p><span class="text-help">List Price:</span>&nbsp;<span><b class="text-success"><?php print "".$rs_list->fields("listprice"); ?></b></span></p>
                                            <?php } ?>
                                            <?php if($rs_list->fields("ourprice") > 0) { ?>
                                            <p><span class="text-help">Our Price:</span>&nbsp;<span><b class="text-success"><?php print "".$rs_list->fields("ourprice"); ?></b></span></p>
                                            <?php } ?>
                                            <?php if($rs_list->fields("memberprice") > 0) { ?>
                                                <p><span class="text-help">Wholesaler Price:</span>&nbsp;<span><b class="text-success"><?php print "".$rs_list->fields("memberprice"); ?></b></span></p>
                                            <?php } ?>
                                            <?php /*if($rs_list->fields("availability") != "") { ?>
                                                <br/><span class="text-help">Available:</span>&nbsp;<span><?php print "".$rs_list->fields("availability"); ?></span>
                                            <?php } */?>
                                            <?php 
                                            $str_query_select = "";
                                            $str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_COLOR." a LEFT JOIN ".$STR_DB_TABLE_NAME_COLOR." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid =  ".$rs_list->Fields("pkid");
                                            //print $str_query_select;
                                            $rs_list_color = GetRecordSet($str_query_select);
                                            $str_color_list = "";
                                            while(!$rs_list_color->EOF())
                                            {
                                                $str_color_list .= $rs_list_color->Fields("title").", ";
                                                $rs_list_color->MoveNext();
                                            }
                                            //print $str_color_list;
                                            if($str_color_list != "") 
                                            { 
                                                /*$str_color_list = "";
                                                $arr_color = explode("|", $rs_list->fields("color"));
                                                //print_r($arr_color);    
                                                foreach($arr_color AS $i):
                                                    //print $i;
                                                    $str_query_select_color = "";
                                                    $str_query_select_color = "SELECT title FROM ".$STR_DB_TABLE_NAME_COLOR." WHERE pkid = ".$i;
                                                    //print $str_query_select."<br/>";
                                                    $rs_list_color_val = GetRecordSet($str_query_select_color);
                                                    $str_color_list.= $rs_list_color_val->Fields("title").", ";
                                                endforeach;*/
                                                //print $str_color_list;

                                            ?>
                                                <p><span class="text-help">Color:</span>&nbsp;<span class=""><?php print rtrim($str_color_list, ", "); ?></span></p>
                                            <?php } ?>    
                                            <?php 
                                                $str_query_select = "";
                                                $str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_SIZE." a LEFT JOIN ".$STR_DB_TABLE_NAME_SIZE." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid =  ".$rs_list->Fields("pkid");
                                                //print $str_query_select;
                                                $rs_list_size = GetRecordSet($str_query_select);
                                                $str_size_list = "";
                                                while(!$rs_list_size->EOF())
                                                {
                                                    $str_size_list .= $rs_list_size->Fields("title").", ";
                                                    $rs_list_size->MoveNext();
                                                }
                                            
                                            
                                            
                                                if($str_size_list != "") 
                                                { 
                                                    /*$str_size_list = "";
                                                    $arr_size = explode("|", $rs_list->fields("size"));
                                                    //print_r($arr_color);    
                                                    foreach($arr_size AS $i):
                                                        //print $i;
                                                        $str_query_select_size = "";
                                                        $str_query_select_size = "SELECT title FROM ".$STR_DB_TABLE_NAME_SIZE." WHERE pkid = ".$i;
                                                        //print $str_query_select."<br/>";
                                                        $rs_list_size_val = GetRecordSet($str_query_select_size);
                                                        $str_size_list.= $rs_list_size_val->Fields("title").", ";
                                                    endforeach;*/
                                                ?>
                                                <p><span class="text-help">Size:</span>&nbsp;<span class=""><?php print rtrim($str_size_list, ", "); ?></span></p>
                                            <?php } ?>    
                                            
                                                
                                                
                                            <?php /*if($rs_list->fields("description") != "" && $rs_list->fields("description") != "<br/>" || $rs_list->fields("seotitle") != "" || $rs_list->fields("seokeyword") != "" || $rs_list->fields("seodescription") != "") { */?>         
                                            <div class="" id="accordion">
                                                <p align="right" class="nopadding text-help"><i><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#moredetails<?php print $int_cnt; ?>"><?php print $STR_MSG_CLICK_TO_VIEW_MORE_DETAILS; ?></a></i></p>
                                                <div id="moredetails<?php print $int_cnt;?>" class="panel-collapse collapse" align="justify">
                                                    
                                                <?php /*if($rs_list->fields("freesizeprice") > 0) { ?>
                                                    <p><span class="text-help">Dress Material Price:</span>&nbsp;<span><b class="text-success"><?php print "".$rs_list->fields("freesizeprice"); ?></b></span></p>
                                                <?php } ?>
                                                <?php if($rs_list->fields("semistitchedprice") > 0) { ?>
                                                    <p><span class="text-help">Semi Stitched Price:</span>&nbsp;<span><b class="text-success"><?php print "".$rs_list->fields("semistitchedprice"); ?></b></span></p>
                                                <?php } ?>
                                                <?php if($rs_list->fields("customizedprice") > 0) { ?>
                                                    <p><span class="text-help">Customized Price:</span>&nbsp;<span><b class="text-success"><?php print "".$rs_list->fields("customizedprice"); ?></b></span></p>
                                                <?php } */?>
                                                <?php if($rs_list->fields("availability_info") != "") { ?>
                                                    <p><span class="text-help">Available Info.:</span>&nbsp;<span><?php print "".$rs_list->fields("availability_info"); ?></p>
                                                <?php } ?>    
                                                <?php
                                                    $str_query_select = "";
                                                    $str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_LENGTH." a LEFT JOIN ".$STR_DB_TABLE_NAME_LENGTH." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid =  ".$rs_list->Fields("pkid");
                                                    //print $str_query_select;
                                                    $rs_list_length = GetRecordSet($str_query_select);
                                                    $str_length_list = "";
                                                    while(!$rs_list_length->EOF())
                                                    {
                                                        $str_length_list .= $rs_list_length->Fields("title").", ";
                                                        $rs_list_length->MoveNext();
                                                    }
                                                    
                                                    if($str_length_list != "") 
                                                    { 
                                                        /*$str_length_list = "";
                                                        $arr_length = explode("|", $rs_list->fields("length"));
                                                        //print_r($arr_color);    
                                                        foreach($arr_length AS $i):
                                                            //print $i;
                                                            $str_query_select_length = "";
                                                            $str_query_select_length = "SELECT title FROM ".$STR_DB_TABLE_NAME_LENGTH." WHERE pkid = ".$i;
                                                            //print $str_query_select."<br/>";
                                                            $rs_list_length_val = GetRecordSet($str_query_select_length);
                                                            $str_length_list.= $rs_list_length_val->Fields("title").", ";
                                                        endforeach;*/
                                                    ?>
                                                    <p><span class="text-help">Length:</span>&nbsp;<span class=""><?php print rtrim($str_length_list, ", "); ?></span></p>
                                                <?php } ?>
                                                <?php 
                                                    $str_query_select = "";
                                                    $str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_TYPE." a LEFT JOIN ".$STR_DB_TABLE_NAME_TYPE." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid =  ".$rs_list->Fields("pkid");
                                                    //print $str_query_select;
                                                    $rs_list_type = GetRecordSet($str_query_select);
                                                    $str_type_list = "";
                                                    while(!$rs_list_type->EOF())
                                                    {
                                                        $str_type_list .= $rs_list_type->Fields("title").", ";
                                                        $rs_list_type->MoveNext();
                                                    }
                                                    
                                                    if($str_type_list != "") 
                                                    {
                                                        /*$str_type_list = "";
                                                        $arr_type = explode("|", $rs_list->fields("type"));
                                                        //print_r($arr_color);    
                                                        foreach($arr_type AS $i):
                                                            //print $i;
                                                            $str_query_select_type = "";
                                                            $str_query_select_type = "SELECT title FROM ".$STR_DB_TABLE_NAME_TYPE." WHERE pkid = ".$i;
                                                            //print $str_query_select."<br/>";
                                                            $rs_list_type_val = GetRecordSet($str_query_select_type);
                                                            $str_type_list.= $rs_list_type_val->Fields("title").", ";
                                                        endforeach;*/
                                                    ?>
                                                    <p><span class="text-help">Type:</span>&nbsp;<span class=""><?php print rtrim($str_type_list, ", "); ?></span></p>
                                                <?php }?>
                                                <?php
                                                    $str_query_select = "";
                                                    $str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_FABRIC." a LEFT JOIN ".$STR_DB_TABLE_NAME_FABRIC." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid =  ".$rs_list->Fields("pkid");
                                                    //print $str_query_select;
                                                    $rs_list_fabric = GetRecordSet($str_query_select);
                                                    $str_fabric_list = "";
                                                    while(!$rs_list_fabric->EOF())
                                                    {
                                                        $str_fabric_list .= $rs_list_fabric->Fields("title").", ";
                                                        $rs_list_fabric->MoveNext();
                                                    }
                                                
                                                    if($str_fabric_list!="") 
                                                    {
                                                        /*$str_fabric_list = "";
                                                        $arr_fabric = explode("|", $rs_list->fields("fabric"));
                                                        //print_r($arr_color);    
                                                        foreach($arr_fabric AS $i):
                                                            //print $i;
                                                            $str_query_select_fabric = "";
                                                            $str_query_select_fabric = "SELECT title FROM ".$STR_DB_TABLE_NAME_FABRIC." WHERE pkid = ".$i;
                                                            //print $str_query_select."<br/>";
                                                            $rs_list_fabric_val = GetRecordSet($str_query_select_fabric);
                                                            $str_fabric_list.= $rs_list_fabric_val->Fields("title").", ";
                                                        endforeach;*/
                                                    ?>
                                                    <p><span class="text-help">Fabric:</span>&nbsp;<span class=""><?php print rtrim($str_fabric_list, ", "); ?></span></p>
                                                <?php }?>
                                                <?php 
                                                    $str_query_select = "";
                                                    $str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_OCCASION." a LEFT JOIN ".$STR_DB_TABLE_NAME_OCCASION." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid =  ".$rs_list->Fields("pkid");
                                                    //print $str_query_select;
                                                    $rs_list_occasion = GetRecordSet($str_query_select);
                                                    $str_occasion_list = "";
                                                    while(!$rs_list_occasion->EOF())
                                                    {
                                                        $str_occasion_list .= $rs_list_occasion->Fields("title").", ";
                                                        $rs_list_occasion->MoveNext();
                                                    }
                                                
                                                    if($str_occasion_list != "") 
                                                    {
                                                        /*$str_occasion_list = "";
                                                        $arr_occasion = explode("|", $rs_list->fields("occasion"));
                                                        //print_r($arr_color);    
                                                        foreach($arr_occasion AS $i):
                                                            //print $i;
                                                            $str_query_select_occasion = "";
                                                            $str_query_select_occasion = "SELECT title FROM ".$STR_DB_TABLE_NAME_OCCASION." WHERE pkid = ".$i;
                                                            //print $str_query_select."<br/>";
                                                            $rs_list_occasion_val = GetRecordSet($str_query_select_occasion);
                                                            $str_occasion_list.= $rs_list_occasion_val->Fields("title").", ";
                                                        endforeach;*/
                                                    ?>
                                                    <p><span class="text-help">Occasion:</span>&nbsp;<span class=""><?php print rtrim($str_occasion_list, ", "); ?></span></p>
                                                <?php }?>
                                                <?php
                                                    $str_query_select = "";
                                                    $str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_WORK." a LEFT JOIN ".$STR_DB_TABLE_NAME_WORK." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid =  ".$rs_list->Fields("pkid");
                                                    //print $str_query_select;
                                                    $rs_list_work = GetRecordSet($str_query_select);
                                                    $str_work_list = "";
                                                    while(!$rs_list_work->EOF())
                                                    {
                                                        $str_work_list .= $rs_list_work->Fields("title").", ";
                                                        $rs_list_work->MoveNext();
                                                    }
                                                
                                                    if($str_work_list != "") 
                                                    {
                                                        /*$str_work_list = "";
                                                        $arr_work = explode("|", $rs_list->fields("work"));
                                                        //print_r($arr_color);    
                                                        foreach($arr_work AS $i):
                                                            //print $i;
                                                            $str_query_select_work = "";
                                                            $str_query_select_work = "SELECT title FROM ".$STR_DB_TABLE_NAME_WORK." WHERE pkid = ".$i;
                                                            //print $str_query_select."<br/>";
                                                            $rs_list_work_val = GetRecordSet($str_query_select_work);
                                                            $str_work_list.= $rs_list_work_val->Fields("title").", ";
                                                        endforeach;*/
                                                    ?>
                                                    <p><span class="text-help">Work:</span>&nbsp;<span class=""><?php print rtrim($str_work_list, ", "); ?></span></p>
                                                <?php }?>
                                                <?php
                                                    $str_query_select = "";
                                                    $str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_SHIPPING." a LEFT JOIN ".$STR_DB_TABLE_NAME_SHIPPING." b ON a.masterpkid=b.pkid AND b.visible='YES' WHERE a.itempkid =  ".$rs_list->Fields("pkid");
                                                    //print $str_query_select;
                                                    $rs_list_shipping = GetRecordSet($str_query_select);
                                                    $str_shipping_list = "";
                                                    while(!$rs_list_shipping->EOF())
                                                    {
                                                        $str_shipping_list .= $rs_list_shipping->Fields("title").", ";
                                                        $rs_list_shipping->MoveNext();
                                                    }
                                                    
                                                    if($str_shipping_list != "") 
                                                    {
                                                        /*$str_shipping_list = "";
                                                        $arr_shipping = explode("|", $rs_list->fields("shippingtype"));
                                                        //print_r($arr_color);    
                                                        foreach($arr_shipping AS $i):
                                                            //print $i;
                                                            $str_query_select_shipping = "";
                                                            $str_query_select_shipping = "SELECT title FROM ".$STR_DB_TABLE_NAME_SHIPPING." WHERE pkid = ".$i;
                                                            //print $str_query_select."<br/>";
                                                            $rs_list_shipping_val = GetRecordSet($str_query_select_shipping);
                                                            $str_shipping_list.= $rs_list_shipping_val->Fields("title").", ";
                                                        endforeach;*/
                                                    ?>
                                                    <p><span class="text-help">Shipping Options:</span>&nbsp;<span class=""><?php print rtrim($str_shipping_list, ", "); ?></span></p>
                                                <?php }?>
                                                    <?php if($rs_list->fields("dshippingvalue") > 0) { ?>
                                                    <p><span  class="text-help">Domestic Shipping Value:</span>&nbsp;<b class="text-success"><?php print "US$ ".$rs_list->fields("dshippingvalue"); ?></b></p>
                                                    <?php } ?>
                                                    <?php if($rs_list->fields("dshippinginfo") != "") { ?>
                                                        <p><span class="text-help">Domestic Shipping Info:</span>&nbsp;<?php print $rs_list->fields("dshippinginfo"); ?></p>
                                                    <?php } ?>
                                                    <?php if($rs_list->fields("ishippingvalue") > 0) { ?>
                                                        <p><span  class="text-help">International Shipping Value:</span>&nbsp;<b class="text-success"><?php print "US$ ".$rs_list->fields("ishippingvalue"); ?></b></p>
                                                    <?php } ?>
                                                    <?php if($rs_list->fields("ishippinginfo") != "") { ?>
                                                        <p><span class="text-help">International Shipping Info:</span>&nbsp;<?php print $rs_list->fields("ishippinginfo"); ?></p>
                                                    <?php } ?>  
                                                    <?php if($rs_list->fields("weight") != "") { ?>
                                                        <p><span class="text-help">Weight:</span>&nbsp;<?php print $rs_list->fields("weight"); ?></p>
                                                    <?php } ?>      
                                                        
                                                    <?php if($rs_list->fields("description") != "" && $rs_list->fields("description") != "<br/>") { ?>    
                                                    <hr/><p align="justify"><span class="text-help">Description:</span>&nbsp;<?php print(RemoveQuote($rs_list->fields("description"))); ?></p>
                                                    <?php } ?>
                                                    <hr/>
                                                    <?php if($rs_list->fields("seotitle") != "") { ?>    
                                                    <p align="justify"><span class="text-help">SEO Title:</span>&nbsp;<?php print(RemoveQuote($rs_list->fields("seotitle"))); ?></p>
                                                    <?php } ?>
                                                    <?php if($rs_list->fields("seokeyword") != "") { ?>    
                                                    <p align="justify"><span class="text-help">SEO Keywords:</span>&nbsp;<?php print(RemoveQuote($rs_list->fields("seokeyword"))); ?></p>
                                                    <?php } ?>
                                                    <?php if($rs_list->fields("seodescription") != "") { ?>    
                                                    <p align="justify"><span class="text-help">SEO Description:</span>&nbsp;<?php print(RemoveQuote($rs_list->fields("seodescription"))); ?></p>
                                                    <?php } ?>
                                                </div>
                                            </div>     
                                            <?php //} ?>
                                        </td>
                                        <td align="center">
                                            <h3 class="nopadding">
                                                <a href="item_photo_list.php?pkid=<?php print $rs_list->Fields("pkid").$str_filter; ?>" title=""><i class="fa fa-image" title="Manage Images"></i></a>
                                            </h3>
                                            <p class="text-help">
                                            <?php 
                                            $str_query_select = "";
                                            $str_query_select = "SELECT count(*) AS noofphotos FROM " .$STR_DB_TABLE_NAME_PHOTO. " WHERE masterpkid=" .$rs_list->Fields("pkid");
                                            $rs_list_photos = GetRecordSet($str_query_select); 
                                            print "(".$rs_list_photos->Fields("noofphotos").")"; ?>
                                            </p>
                                            <p>
                                                <?php 
                                                $str_query_select = "";
                                                $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='LEFT' ORDER BY setasofferimage ASC";
                                                $rs_list_offerimages_left = GetRecordSet($str_query_select);
                                                //print $rs_list_offerimages->Count();
                                                if($rs_list_offerimages_left->Count() > 0) {
                                                ?>

                                                <img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list_offerimages_left->Fields("thumbphotofilename"); ?>" alt="" title="" class="img-responsive"/>
                                                <?php }  ?>
                                                <?php 
                                                $str_query_select = "";
                                                $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE masterpkid=".$rs_list->Fields("pkid")." AND setasofferimage='RIGHT' ORDER BY setasofferimage ASC";
                                                $rs_list_offerimages_right = GetRecordSet($str_query_select);
                                                //print $rs_list_offerimages->Count();
                                                if($rs_list_offerimages_right->Count() > 0) {
                                                ?>
                                                <img src="<?php print $UPLOAD_IMG_PATH.$rs_list->Fields("pkid")."/".$rs_list_offerimages_right->Fields("thumbphotofilename"); ?>" alt="" title="" class="img-responsive"/>
                                                <?php }   ?>
                                            </p>
                                            
					    <?php /*
                                            $str_query_select = "";
                                            $str_query_select = "SELECT count(itempkid) AS totaltailoringservices FROM ".$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE." WHERE itempkid = ".$rs_list->fields("pkid");
                                            $rs_list_tailoringservices = GetRecordSet($str_query_select);                
                                            ?>
                                            <h3 class="nopadding">
                                                <a href="item_tailoringservice_list.php?pkid=<?php print $rs_list->Fields("pkid").$str_filter; ?>" title=""><i class="fa fa-scissors" title="Manage Tailoring Options"></i></a>
                                            </h3>
					    <span class="text-help">(<?php print($rs_list_tailoringservices->fields("totaltailoringservices")); ?>)</span>
                                            <hr/><?php */?>
                                            <?php /*
                                            $str_query_select = "";
                                            $str_query_select = "SELECT count(itempkid) AS totalfavorite FROM ".$STR_DB_TR_TABLE_NAME_FAVORITES." WHERE itempkid = ".$rs_list->fields("pkid");
                                            $rs_list_favorite = GetRecordSet($str_query_select);                
                                            ?>
                                            <h3 class="nopadding"><a href="item_list_favorite.php?pkid=<?php print($rs_list->fields("pkid"));?>&PagePosition=<?php print($int_page);?>&key=<?php print($str_name_key);?>" class="text-primary"><i class="fa fa-star text-primary" title="Click to view favorite list"></i></a></h3><span class="text-help">(<?php print($rs_list_favorite->fields("totalfavorite")); ?>)</span>
                                            <?php /* ?><h3 class="nopadding">
                                                <a href="item_price_list.php?pkid=<?php print $rs_list->Fields("pkid").$str_filter; ?>" title=""><i class="fa fa-money" title="Manage Price"></i></a>
                                            </h3>                                        <?php */ ?>    
                                        </td>
                                        <?php 
                                        /*$str_allow_purchase_class = "alert-danger";
                                        if(strtoupper($rs_list->fields("allowpurchase"))=="YES")
                                        {
                                            $str_allow_purchase_class = "alert-success";
                                        } ?>
                                        <td align="center" class="<?php print $str_allow_purchase_class; ?>">
                                            <a href="item_purchase_p.php?pkid=<?php print($rs_list->fields("pkid").$str_filter);?>" title="Click to change purchage mode" class="alert-link" ><?php print($rs_list->fields("allowpurchase")) ?></a>
                                        </td>
                                        <?php */ ?>
                                         
                                        <td class="text-center">
                                            <?php
                                            if(trim(strtoupper($rs_list->fields("displayasnew"))) == "YES") 
                                            { 
                                                /*## START - It will change displayasnew field to 'NO' after mentioned day in configuration
                                                
                                                    $str_current_date = "";
                                                    $str_created_date = "";
                                                    $int_time_diff_in_days = 0.00;
                                                    $str_time_diff = "";

                                                    $str_current_date = strtotime(date("Y-m-d H:i:s"));
                                                    $str_created_date = strtotime($rs_list->Fields("createdatetime"));


                                                    $str_time_diff = $str_current_date - $str_created_date;

                                                    $int_time_diff_in_days = $str_time_diff / (60 * 60 *24);
                                                    if($int_time_diff_in_days > $INT_DAYS_DISPLAY_AS_NEW)
                                                    {
                                                        $str_query_update = "";
                                                        $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME." SET displayasnew='NO' WHERE pkid=".$rs_list->Fields("pkid");

                                                        ExecuteQuery($str_query_update);

                                                    }
                                                ## END - It will change displayasnew field to 'NO' after mentioned day in configuration*/
                                            ?>
                                                <?php // print $STR_ICON_PATH_NEW; ?>
                                            <?php 
                                            } ?>
						<?php print $STR_ICON_PATH_NEW; ?>
						<br/>
                                            <?php 
                                            $str_checked_new = "";
                                            if(strtoupper(trim($rs_list->fields("displayasnew"))) == "YES") {
                                                $str_checked_new = " checked"; }?>  
                                            <input type="checkbox" name="chk_new<?php print($int_cnt);?>" <?php print($str_checked_new); ?>>
                                        </td>
                                        <td class="text-center">
                                            <?php if(trim(strtoupper($rs_list->fields("displayashot"))) == "YES") { ?>
                                                <?php //print $STR_ICON_PATH_HOT; ?>
                                            <?php 
                                            } ?><?php print $STR_ICON_PATH_HOT; ?><br/>
                                            <?php 
                                            $str_checked_hot = "";
                                            if(strtoupper(trim($rs_list->fields("displayashot"))) == "YES") {
                                                $str_checked_hot = " checked"; }?>  
                                            <input type="checkbox" name="chk_hot<?php print($int_cnt);?>" <?php print($str_checked_hot); ?>>
                                        </td>
                                        <td class="text-center">
                                            <?php if(trim(strtoupper($rs_list->fields("displayasfeatured"))) == "YES") { ?>
                                                <?php //print $STR_ICON_PATH_FEATURED; ?>
                                            <?php 
                                            } ?><?php print $STR_ICON_PATH_FEATURED; ?><br/>
                                            <?php 
                                            $str_checked_featured = "";
                                            if(strtoupper(trim($rs_list->fields("displayasfeatured"))) == "YES") {
                                                $str_checked_featured = " checked"; }?>  
                                            <input type="checkbox" name="chk_featured<?php print($int_cnt);?>" <?php print($str_checked_featured); ?>>
                                        </td>
                                        
                                        <td class="text-center">
                                            
                                            <?php 
                                            $str_checked_additional_featured = "";
                                            if(strtoupper(trim($rs_list->fields("displayasadditionalinfo"))) == "YES") {
                                                $str_checked_additional_featured = " checked"; }?>  
                                            <input type="checkbox" name="chk_additional_featured<?php print($int_cnt);?>" <?php print($str_checked_additional_featured); ?>>
                                        </td>
                                        
                                        <?php /* ?><td align="center">
                                            <input type="text" name="txt_noofview<?php print($int_cnt);?>" size="5" maxlength="5" value="<?php print($rs_list->fields("noofview"));?>" class="form-control input-sm text-center" />                                            
                                        </td><?php */ ?>
                                        <td align="center">
                                            <input type="text" name="txt_displayorder<?php print($int_cnt);?>" size="5" maxlength="5" value="<?php print($rs_list->fields("displayorder"));?>" class="form-control input-sm text-center" >                                            
                                        </td>
                                        
                                        
                                        <?php 
                                        ## For approve
                                        $str_image_approved = "";
                                        if(strtoupper($rs_list->fields("approved")) == "YES")
                                        {   
                                            $str_image_approved = $STR_LINK_ICON_PATH_APPROVED;
                                            $str_class_approved = "btn btn-info btn-xs";
                                            $str_title_approved = $STR_HOVER_APPROVED;
                                        }
                                        else
                                        {   
                                            $str_image_approved = $STR_LINK_ICON_PATH_NOT_APPROVED;
                                            $str_class_approved = "btn btn-default active btn-xs";
                                            $str_title_approved = $STR_HOVER_NOTAPPROVED;
                                        }
                                        
                                        ## For visible
                                        $str_image = "";
                                        if(strtoupper($rs_list->fields("visible")) == "YES")
                                        {   
                                            $str_image = $STR_LINK_ICON_PATH_VISIBLE;
                                            $str_class = "btn btn-warning btn-xs";
                                            $str_title = $STR_HOVER_VISIBLE;
                                        }
                                        else
                                        {   
                                            $str_image = $STR_LINK_ICON_PATH_INVISIBLE;
                                            $str_class = "btn btn-default active btn-xs";
                                            $str_title = $STR_HOVER_INVISIBLE;
                                        }
                                        
                                        ?>
                                        <td align="center">
                                            <?php /* ?><h4 class="nopadding"><?php print $STR_ICON_PATH_NO_OF_VIEW; ?></h4><span class="text-help">(<?php print($rs_list->fields("noofview")) ?>)</span><hr/>
                                            <h4 class="nopadding"><?php print $STR_ICON_PATH_NO_OF_DOWNLOAD; ?></h4><span class="text-help">(<?php print($rs_list->fields("noofdownload")) ?>)</span><hr/><?php */ ?>
                                            <h4 class="nopadding"><?php print $STR_ICON_PATH_NO_OF_SOLD; ?></h4><span class="text-help">(<?php print($rs_list->fields("noofsold")) ?>)</span><hr/>
                                            <?php /* ?><p><a class="<?php print($str_class_approved); ?>" href="item_approve_p.php?pkid=<?php print($rs_list->fields("pkid"))?><?php print $str_filter; ?>" title="<?php print($str_title_approved);?>"><?php print($str_image_approved);?></a></p><?php */ ?>
                                            <p><a class="<?php print($str_class); ?>" href="item_visible_p.php?pkid=<?php print($rs_list->fields("pkid"))?><?php print $str_filter; ?>" title="<?php print($str_title);?>"><?php print($str_image);?></a></p>
                                            <p><a class="btn btn-success btn-xs" href="item_edit.php?pkid=<?php print($rs_list->fields("pkid"))?><?php print $str_filter; ?>" title="<?php print($STR_HOVER_EDIT)?>"><?php print $STR_LINK_ICON_PATH_EDIT; ?></a></p>
					    <p><a class="btn btn-info btn-xs" href="item_duplicate_p.php?pkid=<?php print($rs_list->fields("pkid"))?><?php print $str_filter; ?>" onClick="return confirm_duplicate();" title="<?php print($STR_HOVER_DUPLICATE)?>"><?php print $STR_LINK_ICON_PATH_DUPLICATE; ?></a></p>
                                            <p><a class="btn btn-danger btn-xs" href="item_del_p.php?pkid=<?php print($rs_list->fields("pkid"))?><?php print $str_filter; ?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE)?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a></p>
						<input type="checkbox" name="chk_alldelete<?php print($int_cnt);?>">
                                        </td>
                                    </tr>
                                    <?php 
                                    $int_cnt++;$int_srno++;
                                    $rs_list->MoveNext();
                                    } ?> 
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        
                                        <td class="text-center">
                                            <button type="button" class="btn btn-success btn-sm" name="new_btn" title="<?php print($STR_HOVER_DISPLAY_AS_NEW); ?>" onClick="return new_click();"><b>Change<br/>New</b></button>
                                        </td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-success btn-sm" name="hot_btn" title="<?php print($STR_HOVER_DISPLAY_AS_HOT); ?>" onClick="return hot_click();"><b>Change<br/>Hot</b></button>
                                        </td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-success btn-sm" name="featured_btn" title="<?php print($STR_HOVER_DISPLAY_AS_FEATURED); ?>" onClick="return featured_click();"><b>Change<br/>Featured</b></button>
                                        </td>
                                        
                                        <td class="text-center">
                                            <button type="button" class="btn btn-success btn-sm" name="additional_featured_btn" title="Click to change 'Display As Additional Featured' mode" onClick="return additional_featured_click();"><b>Change</b></button>
                                        </td>
                                        <?php /* ?><td>
                                            <button type="submit" class="btn btn-success btn-sm" name="view_btn" title="<?php print $STR_HOVER_NUMBER_OF_VIEW_EDIT; ?>" onClick="return noofview_click();"><b>Save</b></button>
                                        </td><?php */ ?>
                                        <td>
                                            <input type="hidden" name="catid" id="catid" value="<?php print($int_cat_pkid);?>">
                                            <input type="hidden" name="key" id="key" value="<?php print($str_name_key);?>">
                                            <input type="hidden" name="PagePosition" id="PagePosition" value="<?php print($int_page);?>">
                                            <input type="hidden" name="hdn_counter" id="hdn_counter" value="<?php print($int_cnt);?>">
                                            <?php print DisplayFormButton('SAVE', 0)?>
                                        </td>
                                        <td><button type="button" class="btn btn-success btn-sm" name="alldelete_btn" title="<?php print($STR_HOVER_DISPLAY_AS_FEATURED); ?>" onClick="return alldelete_click();"><b>Delete<br/>Selected</b></button></td>
                                    </tr>
                                    <tr>
                                        <td colspan="11" align="center" >
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination nopadding">
                                                    <?php 
                                                    $int_margine = 2;
                                                    print(PagingWithMargine($int_total_records,$int_margine,$int_page,"item_list.php",$int_record_per_page,"","key=".$str_name_key."&catid=".$int_cat_pkid."&#ptop"));?>
                                               </ul>
                                            </nav>
                                        </td>
                                    </tr>
                            <?php  } ?>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
        <?php include "../../includes/help_for_list.php"; ?>
    </div>        
    <?php include "../../includes/include_files_admin.php"; ?>

    <script type="text/javascript">
    function get_subcat() { // Call to ajax function
        var catpkid = $('#cbo_cat').val();
        //alert(catpkid);
        var dataString = "cat_pkid="+catpkid;
        $.ajax({
            type: "POST",
            url: "item_sel_subcategory_p.php", // Name of the php files
            data: dataString,
            success: function(html)
            {
                $("#before_get_subcat").hide();
                $("#get_subcat").html(html);
            }
        });
    }
</script>


    <?php include($STR_ADMIN_FOOTER_PATH); ?>

    <script type="text/javascript" src="../../js/richetxteditor.js"></script>
    <script type="text/javascript">
            bkLib.onDomLoaded(function() {
                new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_desc');
            });
                    // convert all text areas to rich text editor on that page
            bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
                    // convert text area with id area2 to rich text editor with full panel.
            bkLib.onDomLoaded(function() {
            new nicEditor({fullPanel : true}).panelInstance('ta_desc');     
            });
    </script>

    <script language="JavaScript" src="./item_add.js"></script>    
    <script language="JavaScript" src="./item_list.js"></script>    
        

    
</body>
</html>
