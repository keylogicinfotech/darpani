<?php
/*
Module Name:- modstore
File Name  :- item_cat_visible_p.php
Create Date:- 04-FEB-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid = 0;
if(isset($_GET["pkid"]) == true)
{
    $int_pkid = trim($_GET["pkid"]);
}	
if($int_pkid <=0 || is_numeric($int_pkid) == false || $int_pkid=="")
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E&#ptop");
    exit();
}

$str_cat_flag="";
if(isset($_GET["catflag"])==true)
{
    $str_cat_flag = trim($_GET["catflag"]);
}
if($str_cat_flag=="" || ($str_cat_flag!="c" && $str_cat_flag!="s"))
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E&#ptop");
    exit();
}

#----------------------------------------------------------------------------------------------------
if($str_cat_flag == "c")
{
    ## For Category
    # Select Query
    $str_query_select="";
    $str_query_select="SELECT title,visible FROM ".$STR_DB_TABLE_NAME_CAT." WHERE catpkid=". $int_pkid;
    $rs_list=GetRecordSet($str_query_select);

    if($rs_list->eof())
    {
            CloseConnection();
            Redirect("item_cat_list.php?msg=F&type=E&#ptop");
            exit();
    }

    $str_mode = "";
    $str_title = "";
    $str_mode = strtoupper(trim($rs_list->fields("visible")));
    $str_title = trim($rs_list->fields("title"));

    #----------------------------------------------------------------------------------------------------
    #change visible mode
    $str_mode_title = "";
    if(strtoupper($str_mode) == "YES")
    {
        $str_mode = "NO";
        $str_mode_title = "Invisible";
    }
    else if(strtoupper($str_mode)=="NO")
    {
        $str_mode="YES";
        $str_mode_title="Visible";
    }
    #----------------------------------------------------------------------------------------------------
    # Update Query
    $str_query_update="";
    $str_query_update="UPDATE ".$STR_DB_TABLE_NAME_CAT." SET visible='" . ReplaceQuote($str_mode) . "' WHERE catpkid=".$int_pkid;
    //print $str_query_update; exit;
    ExecuteQuery($str_query_update);
    #----------------------------------------------------------------------------------------------------
    #Close connection and redirect to it's relevant location
    CloseConnection();
    Redirect("item_cat_list.php?type=S&msg=V&title=".urlencode(RemoveQuote($str_title))."&mode=". urlencode(RemoveQuote($str_mode_title)) ."&#ptop");
    exit();
    #------------------------------------------------------------------------------------------------------------
}
else if($str_cat_flag == "s")
{
    ## For Sub Category
    # Select Query
    $str_query_select="";
    $str_query_select="select subcattitle,visible from ".$STR_DB_TABLE_NAME_SUBCAT." where subcatpkid=". $int_pkid;
    $rs_list=GetRecordSet($str_query_select);

    if($rs_list->eof())
    {
        CloseConnection();
        Redirect("item_cat_list.php?msg=F&type=E&#ptop");
        exit();
    }

    $str_mode="";
    $str_title="";
    $str_mode=strtoupper(trim($rs_list->fields("visible")));
    $str_title=trim($rs_list->fields("subcattitle"));

    #----------------------------------------------------------------------------------------------------
    #change visible mode
    $str_mode_title="";
    if(strtoupper($str_mode)=="YES")
    {
        $str_mode="NO";
        $str_mode_title="Invisible";
    }
    else if(strtoupper($str_mode)=="NO")
    {
        $str_mode="YES";
        $str_mode_title="Visible";
    }
    #----------------------------------------------------------------------------------------------------
    # Update Query
    $str_query_update="";
    $str_query_update="UPDATE ".$STR_DB_TABLE_NAME_SUBCAT." SET visible='" . ReplaceQuote($str_mode) . "' WHERE subcatpkid=".$int_pkid;
    ExecuteQuery($str_query_update);
    #----------------------------------------------------------------------------------------------------
    #Close connection and redirect to it's relevant location
    CloseConnection();
    Redirect("item_cat_list.php?type=S&msg=V&title=".urlencode(RemoveQuote($str_title))."&mode=". urlencode(RemoveQuote($str_mode_title)) ."&#ptop");
    exit();
    #------------------------------------------------------------------------------------------------------------
}
?>