// JavaScript Document
$(document).ready(function() {
	$('#frm_add').on('submit', function(event) {
			event.preventDefault();
			
			var formData = new FormData($('#frm_add')[0]);
			formData.append("cbo_subcat",$("select#cbo_subcat").val());
			formData.append("cbo_month",$("select#cbo_month").val());
			formData.append("cbo_year",$("select#cbo_year").val());
			formData.append("txt_title",$("input#txt_title").val());
			formData.append("cbo_price",$("select#cbo_price").val());
			formData.append("txt_nophoto",$("input#txt_nophoto").val());
			//formData.append('hdn_pkid', $("input#hdn_pkid").val());
			formData.append("ta_desc",$("textarea#ta_desc").val());
			//formData.append('video_clip', $("input#video_clip")[0].files[0]);
			formData.append("cbo_extension",$("select#cbo_extension").val());
			formData.append("txt_ptitle",$("input#txt_ptitle").val());
			formData.append("txt_purl",$("input#txt_purl").val());
			  //var formData = new FormData($('#frm_add')[1]);
                        formData.append("txt_color",$("input#txt_color").val());
			formData.append("txt_size",$("input#txt_size").val());
			formData.append("txt_type",$("input#txt_type").val());
			formData.append("txt_domestic",$("input#txt_domestic").val());
			formData.append("txt_international",$("input#txt_international").val());
			formData.append("txt_shippinginfo",$("input#txt_shippinginfo").val());
			//alert($("input#video_clip")[0].files[0]));
			
			if($("select#cbo_subcat").val() == 0) {
			{
				alert("Please select sub category.");
				exit();
			}
                        if($("select#cbo_price").val() == 0) {
			{
				alert("Please select price.");
				exit();
			}
                        if($("select#cbo_mdl").val() == 0) {
			{
				alert("Please select price.");
				exit();
			}
			
			
//                        if($("input#txt_price").val() == 0) {
//				alert("Please enter price");
//				exit();
//			}
                        if($("input#txt_title").val() == "") {
				alert("Please enter video clip title");
				exit();
			}
			else
			{
				$.ajax({
					   xhr: function() {
								var xhr = new window.XMLHttpRequest();
								xhr.upload.addEventListener('progress', function(e) {
										if(e.lengthComputable){
											//console.log('Bytes Loaded: ' + e.loaded);
											//console.log('Total Size: ' + e.total);
											//console.log('Percentage uploaded: ' + (e.loaded/e.total))
											
											var percent = Math.round((e.loaded/e.total) * 100);
											$('#progressBar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
										}												 
								});
								return xhr;
						   },
						 
						  type: 'POST',
						  url: 'mdl_ps_add_p.php',
						  data: formData,
						  processData: false,
						  contentType: false,
						  success: function(data) {
							  //alert(data);
							  var $responseText=JSON.parse(data);
								
								if($responseText.status == 'ERR' && $responseText.IFE == 'IF')
								{
									location.href = './mdl_ps_list.php?msg=IE&type=E&#ptop';
								}
								else if($responseText.status == 'ERR' && $responseText.IFE == 'IU')
								{
									location.href = './mdl_ps_list.php?msg=IU&type=E&#ptop';
								}
								else if($responseText.status == 'ERR' && $responseText.IFE == 'DU')
								{
									location.href = './mdl_ps_list.php?msg=DU&type=E&#ptop';
								} 
								else if($responseText.status == 'ERR' && $responseText.IFE == 'SM')
								{
									location.href = './mdl_ps_list.php?msg=F&type=E&#ptop';
								}
								else if($responseText.status == 'SUC' && $responseText.IFE == 'SA')
								{
									location.href = './mdl_ps_list.php?msg=S&type=S&#ptop';
									//location.reload();
									//window.setTimeout(function(){location.reload()},3000)
								}
								else if($responseText.status == 'ERR' && $responseText.IFE == 'BL')
								{
									location.href = './mdl_ps_list.php?msg=BL&type=E&#ptop';
								}
								/*else if($responseText.status == 'SUC')
								{
									// Fail message
									$('#success').html("<div class='alert alert-danger'>");
									$('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
										.append("</button>");
									$('#success > .alert-danger').append("<strong> " + $responseText.message + " ");
									$('#success > .alert-danger').append('</div>');
								}*/
							  
								//alert(data);
								//alert("File Uploaded Successfully");
								//location.reload();
						  }
						  //alert(data);
					   });
				 } 
			
			});
	});
				
						   // JavaScript Document