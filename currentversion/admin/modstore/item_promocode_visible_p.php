<?php
/*
Module Name:- Modstore
File Name  :- promo_code_visible_p.php
Create Date:- 23-AUG-2014
Intially Create By :- 0022Honey
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid = 0;
if(isset($_GET['pkid'])) { $int_pkid = $_GET['pkid']; }

if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
{
    CloseConnection();
    Redirect("item_promocode_list.php?type=E&msg=F");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Select Query
$str_select_query="";
$str_select_query="SELECT title,visible FROM ".$STR_DB_TABLE_NAME_PROMOCODE." WHERE pkid=".$int_pkid;
$rs_list=GetRecordset($str_select_query);
if($rs_list->EOF()==true)
{
    CloseConnection();
    Redirect("item_promocode_list.php?type=E&msg=F");
    exit();
}

$str_visible = "";
$str_visible = $rs_list->Fields("visible");
if(strtoupper($str_visible) == 'YES')
{
    $str_visible = "NO";
    $str_visible_title = "Invisible";
}
else if(strtoupper($str_visible) == 'NO')
{
    $str_visible = "YES";
    $str_visible_title = "Visible";
}
#-----------------------------------------------------------------------------------------------------------------------------
#Update Query
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_PROMOCODE." SET visible='" .ReplaceQuote($str_visible). "' WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------------------------------	
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_promocode_list.php?type=S&msg=V&mode=".urlencode(RemoveQuote($str_visible_title)));
exit(); 
?>