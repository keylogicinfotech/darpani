<?php 
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php"; 
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_xml.php";
#----------------------------------------------------------------------------------------------------
$str_img_path_user = "../../mdm/freemember/";
$STR_PAGE_TITLE_FAVORITE = "Favorite list";
$int_pkid=0;
//print_r($_GET);
if(isset($_GET["pkid"]))  { $int_pkid = $_GET["pkid"]; }
# 	if id is empty then to close window
/*if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}*/
#----------------------------------------------------------------------
#get filter data
$int_cat_pkid = "";
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{   
    $int_cat_pkid = trim($_GET["catid"]);    
}

$str_name_key = "";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_name_key = trim($_GET["key"]); }

# get data for paging
$int_page = "";
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"]) != "" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"]) > 0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page = 1; }



$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page;

//$int_userpkid = $_SESSION["userpkid"]; 
#------------------------------------------------------------------------------------------------
#
# Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid. "";
//print $str_query_select;exit; 
//$str_query_select="SELECT * FROM cm_home_content";
$rs_list_item = GetRecordSet($str_query_select); 

if($rs_list_item->eof()==true)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}
$str_itemname = "";
$str_itemname = $rs_list_item->Fields("title");

#select query to get large image path from t_photoofday table.
$str_query_select="SELECT DISTINCT fm.userpkid,fm.*,m.* ";
$str_query_select.=" FROM  tr_user_favorite_item fm ";
$str_query_select.=" LEFT JOIN t_freemember m ON fm.userpkid=m.pkid ";
//$str_query_select.=" LEFT JOIN tr_store_photo p ON p.masterpkid = m.pkid AND p.setasfront='YES'";
$str_query_select.=" WHERE fm.itempkid=".$int_pkid;
$str_query_select.=" ORDER BY fm.date DESC";
//print $str_query_select;
$rs_list=GetRecordSet($str_query_select);

$str_query_select = "";
$str_query_select = "SELECT count(*) totalrec FROM " .$STR_DB_TABLE_NAME. "";
$rs_list_count = GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------
# Initialization of variables used for message display.   
$str_visible_value = "";
if(isset($_GET["mode"]))
{
    if($_GET["mode"] == "YES") { $str_visible_value = "Visible"; }
    elseif($_GET["mode"] == "NO") { $str_visible_value = "Invisible"; }
}

$str_open="";
if(isset($_GET["window"])) 	{ $str_open = trim($_GET["window"]); }

$str_button_caption = "";
if(isset($_GET["captionbut"]))  { $str_button_caption = trim($_GET["captionbut"]); 	}	
	
    $str_display_button = "NON";
    if(isset($_GET["displaybut"]))  { $str_display_button= trim($_GET["displaybut"]); }
#	Get message type.
    $str_type = "";
    $str_message = "";	
    if(isset($_GET["type"]))
    {
        switch(trim($_GET["type"]))
        {
            case("S"): $str_type = "S"; break;
            case("E"): $str_type = "E"; break;
            case("W"): $str_type = "W"; break;
        }
    }
#Display message.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_visible_value; break;	
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;	
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("E"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("LINK"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;
        case("IINV"): $str_message = "All data can't be INVISIBLE, Please keep atleast one image visible."; break;
        //case("IDE"): $str_message = "Please select image or add description.";	break;
        case("IDE"): $str_message = "Please select image.";	break;
        case("DBC"): $str_message = "Please select button type and enter valid caption for selected button.";	break;
        case("SOH"): $str_message = $STR_MSG_ACTION_DISPLAY_ON_HOME;	break;
        case("SOT"): $str_message = $STR_MSG_ACTION_DISPLAY_ON_TOP;	break;
        case("NV"): $str_message = "Error... You can not display more than 3 records on top of home page.";	break;
        case("DU"): $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_PAGE_TITLE_FAVORITE);?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<a name="ptop"></a>
<div class="container">
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?> " ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?> </a>
            </div>
        </div>
        <div class="col-md-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_PAGE_TITLE_FAVORITE); ?></h3></div>
    </div><hr>
    <?php if($str_type != "" && $str_message != "") { print(DisplayMessage("99%",$str_message,$str_type)); } ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        
                         <h4 class="panel-title">
                                <i class=" "></i>&nbsp;<b><?php // print($STR_TITLE_EDIT); ?></b>
                                <?php print($STR_LINK_HELP); ?>
                            </h4>
                   <?php /* ?>    <h4 class="panel-title">
                           
                                <span>   <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" title="<?php print $STR_HOVER_ADD; ?>" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>  </span>
                               
                                <?php print($STR_LINK_HELP); ?>
                        </h4> <?php */ ?>
                 
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
                        <div class="panel-body">
                            <form name="frm_add" action="item_add_p.php" method="post" onSubmit="return frm_add_validate();" enctype="multipart/form-data" role="form">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="form-group">
                                    <label>Title</label><span class="text-help-form"> </span>
                                    <input id="txt_title" name="txt_title" size="90" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" type="text" >
                                </div>
                                <div class="form-group">
                                    <label>Description</label><span class="text-help-form">*</span>
                                    <textarea name="ta_desc" id="ta_desc" cols="100" rows="20" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>" ></textarea>
                                    <span class="text-help-form"><?php //print($STR_MAX_255_MSG);?></span>
                                </div> 
                                <div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Upload Image</label>&nbsp;
                                            <span class="text-help-form"> (<?php print($STR_MSG_IMG_FILE_TYPE);?> &nbsp;| <?php print $STR_MSG_IMG_UPLOAD; ?>) </span>
                                                <input type="file" name="fileimage" size="90"  >
                                           
                                            <input name="hdn_existing_image" type="hidden" id="hdn_existing_image" value="<?php print($rs_list->fields("imagefilename")); ?>">
                                        </div>
                                    </div>
                                    
                                </div>
                   
                               <input type="hidden" name="txt_urltitle" value="" >
                               <input type="hidden" name="txt_url" value="" >
                                <input type="hidden" name="cbo_window" value="" >
                               <input type="hidden" name="txt_hover" value="" >
                               
                         
                                <div class="row padding-10">
                                    
                                    <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Visible</label>&nbsp;<span class="text-help-form">(<?php print($STR_MSG_VISIBLE)?>)</span><br/>
                                                <select name="cbo_visible" class="form-control input-sm" >
                                                <option value="YES">YES</option><option value="NO">NO</option></select>
                                            </div>
                                            
                                    </div>
                                    </div>
                                <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                <input type="hidden" name="hdn_pkid" value="<?php print($rs_list->fields("pkid")); ?>" >
                                <input type="hidden" name="txt_header" size="80" class="clsTextBox" maxlength="100" value="">                            
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <form name="frm_list" action="item_order_p.php" method="post" onSubmit="return frm_list_check_displayorder();">
                    <thead>
                        <tr>
                            
                            <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                            <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_DATE; ?></th>
                            <th width="20%"><?php print $STR_TABLE_COLUMN_NAME_IMAGE; ?></th>
                            <th><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                                
                        </tr>
                    </thead>
                    <tbody>		
                        <?php if($rs_list->EOF()==true)
                        { ?>
							
                        <tr>
                            <td colspan="7" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td>
                        </tr>
                    <?php } 
                    else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
                        <tr>
                            <td align="center"><?php print($int_cnt)?></td>
                            <td align="center" class="text-help"><p class="nopadding"><?php print(DDMMMYYYYHHIISSFormat($rs_list->fields("date"))); ?></p></td>
                            <td align="left">
                                <?php if($rs_list->fields("imagefilename")!="") { ?>
                                    <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list->fields("pkid")); ?>" rel="thumbnail"><img src="<?php print($str_img_path_user.$rs_list->fields("imagefilename"));?>"  class="img-responsive" border="0" align="absmiddle" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image"></a>
                                <div class="modal fade f-pop-up-<?php print($rs_list->fields("pkid")); ?> " align="center" role="dialog" aria-labelledby="" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                <img src="<?php print($str_img_path_user.$rs_list->fields("userpkid").$rs_list->fields("imagefilename"));?>" class="img-responsive" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image">
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>
                                <p class="align-top"><?php } else {  print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); } ?></p> 
                           
                            </td>
                            <td class="align-top">
                         <?php if($rs_list->fields("name") != "")  {?>
                                <h4>
                                   <b><?php print(RemoveQuote($rs_list->fields("name"))); ?></b>
                                </h4><?php } ?>
                            <?php if($rs_list->fields("description") != "")  {?>
                                <p align="justify" ><?php print(RemoveQuote($rs_list->fields("description")));?></p><?php } ?>
                                                  
                            </td>
                           
                        </tr>
                    <?php $int_cnt++; $rs_list->MoveNext(); } ?>
                    <input type="hidden" name="hdn_counter" value="<?php print($int_cnt)?>">
                    <tr class="<?php print($int_cnt%2); ?>">
                    </tr>
                <?php } ?>	
                    </tbody>
                </form>
            </table>
        </div>
    </div>
    
    
    
    
    
    

    <?php include "../../includes/help_for_list.php"; ?>
    <!-- /.container -->											
</div>
<!-- Bootstrap Core CSS -->
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
</body></html>