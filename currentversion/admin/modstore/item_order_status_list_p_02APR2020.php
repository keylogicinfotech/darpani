<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------------------------------------
//session_start();
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_xml.php";
include "../../includes/lib_email.php";
include "item_config.php";
//include "item_app_specific.php";
//print "HI"; exit;
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_POST);exit;
$int_pkid = 0;
if(isset($_POST['hdn_pkid']))
{
    $int_pkid = $_POST['hdn_pkid'];
}
//print $int_pkid; exit;
if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
{
    CloseConnection();
    Redirect("item_order_status_list.php?type=E&msg=F");
    exit();
}

$str_status = "";
if(isset($_POST['cbo_status']))
{
    $str_status = $_POST['cbo_status'];
}


#----------------------------------------------------------------------------------------------------
#Update query to change the mode
$str_query_update="";
$str_query_update="UPDATE ".$STR_DB_TABLE_NAME_PURCHASE." SET shippingstatus='" .ReplaceQuote($str_status). "' WHERE purchasepkid=".$int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------
#Write to xml file
//WriteXml();
#-----------------------------------------------------------------------------------------------------
# Send Mail
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_PURCHASE." WHERE purchasepkid = ".$int_pkid;
$rs_list = GetRecordSet($str_query_select);
if($rs_list->Count() > 0)
{
    if($rs_list->Fields("shippingstatus") == "SHIPPED") 
    {
        $str_query_select = "";
        $str_query_select = "SELECT * FROM t_user WHERE pkid=".$rs_list->Fields("userpkid");
        $rs_list_user = GetRecordSet($str_query_select);

        $str_subject = "";
        $str_mailbody = "";
        $str_from = "";
        $str_to = "";
        //print $STR_FROM_DEFAULT;
        //print $rs_list_user->Fields("emailid");
        //print $STR_XML_FILE_PATH_MODULE;
        $fp = openXMLfile("../".$STR_XML_FILE_PATH_MODULE."siteconfiguration.xml");
        $str_from = getTagValue($STR_FROM_DEFAULT,$fp);
        //print "FROM: ".$str_from;
        $str_to = $rs_list_user->fields("emailid");
        //print "<br/>TO: ".$str_to;exit;
        closeXMLfile($fp);

        $str_subject = $STR_MAIL_SUBJECT_SHIPPING_STATUS_USER;
        $str_mailbody = $STR_MAIL_MAILBODY_SHIPPING_STATUS_USER;
        sendmail($str_to,$str_subject,$str_mailbody,$str_from,1);                                                                              } 
}

#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_order_status_list.php?type=S&msg=SU&tit=".urlencode(RemoveQuote($str_title)));
exit();
#-----------------------------------------------------------------------------------------------------
?>
