<?php
/*
    Module Name:- modestore
    File Name  :- item_approve_p.php
    Create Date:- 18-JAN-2019
    Intially Create By :- 015
    Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
    include "../../includes/validatesession.php";
    include "../../includes/configuration.php";
    include "item_config.php";
    include "../../includes/lib_data_access.php";
    include "../../includes/lib_common.php";
    include "../../includes/lib_email.php";
    include "../../includes/lib_xml.php";
    //include "../includes/lib_file_system.php";
    //include "./item_app_specific.php";
#------------------------------------------------------------------------------------------------
#get filter parameters
#------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_cat_pkid = "";
if(isset($_GET["catid"]) && trim($_GET["catid"]) != "" )
{   
    $int_cat_pkid = trim($_GET["catid"]);    
}

$str_name_key = "";
if(isset($_GET["key"]) && trim($_GET["key"]) != "" )
{ $str_name_key = trim($_GET["key"]); }

# get data for paging
$int_page="";
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page = 1; }

$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";

$int_pkid = 0;
if(isset($_GET['pkid']))
{
    $int_pkid = $_GET['pkid'];
}
if($int_pkid <= 0 || !is_numeric($int_pkid) || $int_pkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F".$str_filter);
    exit();
}
##------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT visible, approved FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
$rs_list = GetRecordset($str_query_select);

if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F".$str_filter); //}
    exit();
}
$str_approved = "";
$str_approved = $rs_list->Fields("approved");

$str_visible = "";
$str_visible = $rs_list->Fields("visible");

$str_approved_title = "";
if(strtoupper($str_approved) == 'YES')
{
    $str_approved = "NO";
    $str_approved_title = "Not Approved";
    
    
    
}
else if(strtoupper($str_approved) == 'NO')
{
    $str_approved = "YES";
    $str_approved_title = "Approved";
    
    if($str_visible == "NO")
    {
        $str_visible = "YES";
    }
}
#-----------------------------------------------------------------------------------------------------------------------------
# Update Query
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME." SET visible='".ReplaceQuote($str_visible)."', approved='".ReplaceQuote($str_approved)."', approveddatetime = '".date("Y-m-d h:i:s")."' WHERE pkid=".$int_pkid;
//print $str_query_update;exit;
ExecuteQuery($str_query_update);	
#-----------------------------------------------------------------------------------------------------------------------------
#NOTIFY USER REGARDING APPROVAL
/*$fp = openXMLfile($STR_XML_FILE_PATH_MODULE."siteconfiguration.xml");
$str_from = getTagValue($STR_FROM_DEFAULT,$fp);
closeXMLfile($fp);

$str_query_select = "SELECT shortenurlkey, emailid FROM t_model WHERE modelpkid=".$int_mdlpkid."";
$rs_list = GetRecordSet($str_query_select);
$str_to = $rs_list->Fields("emailid");

$str_subject="Your item is approved on ".$STR_SITENAME_WITHOUT_PROTOCOL." ";
$mailbody="Your item is approved. Below are details.<br/><br/>";
$mailbody.="<strong>Your Profile ID:</strong> ".$rs_list->fields("shortenurlkey")."<br/>";
$mailbody.="<strong>Item Name:</strong> ".$rs_list->Fields("title")."<br/>";
$mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL."";
//print $mailbody."<br/>".$str_from."<br/>".$str_to."<br/>"; exit;
sendmail($str_to,$str_subject,$mailbody,$str_from,1);          */
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file
//Comment_Synchronize($int_modelpkid);
//Comment_WriteXml($int_modelpkid);
#----------------------------------------------------------------------------------------------------------------------------	
CloseConnection();
//if($str_redirect_page_flag=='NACL')
//{
Redirect("item_list.php?type=S&msg=APR".$str_filter); //}
/*else
{ Redirect("tm_list.php?type=S&msg=V&mdlid=".$int_mdlpkid."&mname=".$str_modelname."&mode=".urlencode(RemoveQuote($str_approved_title))."&tit=".urlencode(RemoveQuote($str_title))."&".$str_filter ); }*/
exit();
?>
