<?php
/*
Module Name:- Manage photoset
File Name  :- mdl_ps_add_p.php
Create Date:- 8-Sep-2006
Intially Create By :- 0014
*/
#---------------------------------------------------------------------------------------------------------------------------
#Include Files

include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
include "../../includes/lib_file_upload.php";
//include "./store_item_app_specific.php";
include "../../includes/lib_xml.php";
include "../../includes/lib_email.php";
#-------------------------------------------------------------------------------------------------------------------------
//print_r($_POST); exit;
$int_catpkid = 0;
$int_subcatpkid = 0;

$str_cattitle = "";
$str_subcattitle = "";
//$int_mdlpkid=0;
//$int_pricepkid = 0;
$int_listprice = 0.00;
$int_ourprice = 0.00;
$int_memberprice = 0.00;
//$int_promocodepkid = 0;
$str_color = array();
$str_size = array();
$str_length = array();
$str_occasion = array();
$str_type=array();
$str_shipping_type = array();
$str_fabric = array();
$str_work = array();
$int_weight = 0;
//$str_month="";
//$str_year="";
$str_itemcode = "";
$str_title = "";
//$str_sub_title = "";
//$str_nophoto="";
$str_desc = "";
//$str_name="";
//$str_purchase="YES";
$str_new = "YES";
$str_hot = "NO";
$str_featured = "NO";
$str_visible = "YES";
//$str_ptitle = "";
//$str_purl = "";
$str_availability = "";
$str_availability_status = "";
$int_domestic_shipping_value = 0;
$int_international_shipping_value = 0;
$str_domestic_shipping_info = "";
$str_international_shipping_info = "";

$str_seo_title = "";
$str_seo_keywords = "";
$str_seo_desc = "";

//$int_dressmaterialprice = 0.00;
//$int_semistitchedprice = 0.00;
//$int_customizedprice = 0.00;


/* $str_video_file="";
$str_ext="";
 */
#-------------------------------------------------------------------------------------------------------------------------
# get form data
if(isset($_POST["cbo_cat"]) && trim($_POST["cbo_cat"])!="")
{
    $int_catpkid = trim($_POST["cbo_cat"]);
}
if(isset($_POST["cbo_subcat"]) && trim($_POST["cbo_subcat"])!="")
{
    $int_subcatpkid = trim($_POST["cbo_subcat"]);
}
if(isset($_POST["txt_code"]) && trim($_POST["txt_code"])!="")
{
    $str_itemcode = trim($_POST["txt_code"]);
}
if(isset($_POST["txt_title"]) && trim($_POST["txt_title"])!="")
{
    $str_title = trim($_POST["txt_title"]);
}
if(isset($_POST["ta_desc"]))
{
    $str_desc = trim($_POST["ta_desc"]);
}

/*if(isset($_POST["cbo_mdl"]) && trim($_POST["cbo_mdl"])!="")
{
    $int_mdlpkid=trim($_POST["cbo_mdl"]);
}
if(isset($_POST["cbo_price"]) && trim($_POST["cbo_price"])!="")
{
    $int_pricepkid=trim($_POST["cbo_price"]);
}*/
if(isset($_POST["txt_listprice"]) && trim($_POST["txt_listprice"])!="")
{
    $int_listprice = trim($_POST["txt_listprice"]);
}
if(isset($_POST["txt_ourprice"]) && trim($_POST["txt_ourprice"])!="")
{
    $int_ourprice = trim($_POST["txt_ourprice"]);
}
if(isset($_POST["txt_memberprice"]) && trim($_POST["txt_memberprice"])!="")
{
    $int_memberprice = trim($_POST["txt_memberprice"]);
}
/* if(isset($_POST["cbo_promocode"]) && trim($_POST["cbo_promocode"])!="")
{
    $int_promocodepkid=trim($_POST["cbo_promocode"]);
}
*/        

if(isset($_POST["cbx_color"]) && $_POST["cbx_color"]!="")
{
    //$str_color = explode(",",trim($_POST["cbx_color"]));
    $str_color = $_POST["cbx_color"];
}
if(isset($_POST["cbx_size"]) && $_POST["cbx_size"]!="")
{
    //$str_size = explode(",",trim($_POST["cbx_size"]));
    $str_size = $_POST["cbx_size"];
}
if(isset($_POST["cbx_type"]) && $_POST["cbx_type"]!="")
{
    //$str_type = explode(",",trim($_POST["cbx_type"]));
    $str_type = $_POST["cbx_type"];
}
if(isset($_POST["cbx_length"]) && $_POST["cbx_length"]!="")
{
    //$str_length = explode(",",trim($_POST["cbx_length"]));
    $str_length = $_POST["cbx_length"];
}
if(isset($_POST["cbx_work"]) && $_POST["cbx_work"]!="")
{
    //$str_work = explode(",",trim($_POST["cbx_work"]));
    $str_work = $_POST["cbx_work"];
}
if(isset($_POST["cbx_occasion"]) && $_POST["cbx_occasion"]!="")
{
    //$str_occasion = explode(",",trim($_POST["cbx_occasion"]));
    $str_occasion = $_POST["cbx_occasion"];
}
if(isset($_POST["cbx_fabric"]) && $_POST["cbx_fabric"]!="")
{
    //$str_fabric = explode(",",trim($_POST["cbx_fabric"]));
    $str_fabric = $_POST["cbx_fabric"];
}

if(isset($_POST["cbx_shipping"]) && trim($_POST["cbx_shipping"])!="")
{
    $str_shipping_type = explode(",",trim($_POST["cbx_shipping"]));
}

//print($str_color); exit;
/*if(isset($_POST["cbo_month"]))
{
    $str_month = date("m");
}
if(isset($_POST["cbo_year"]))
{
    $str_year = date("Y");
}*/
//$str_month = date("m");
//$str_year = date("Y");
	//print $str_year ; exit;


/*if(isset($_POST["txt_sub_title"]) && trim($_POST["txt_sub_title"])!="")
{
    $str_sub_title = trim($_POST["txt_sub_title"]);
}*/

/*if(isset($_POST["txt_nophoto"]) && trim($_POST["txt_nophoto"])!="")
{
    $str_nophoto=trim($_POST["txt_nophoto"]);
}
*/
/*if(isset($_POST["txt_dressmaterialprice"]) && trim($_POST["txt_dressmaterialprice"])!="")
{
    $int_dressmaterialprice = trim($_POST["txt_dressmaterialprice"]);
}
if(isset($_POST["txt_semistitchedprice"]) && trim($_POST["txt_semistitchedprice"])!="")
{
    $int_semistitchedprice = trim($_POST["txt_semistitchedprice"]);
}
if(isset($_POST["txt_customizedprice"]) && trim($_POST["txt_customizedprice"])!="")
{
    $int_customizedprice = trim($_POST["txt_customizedprice"]);
}*/

if(isset($_POST["txt_availability"])  && $_POST["txt_availability"] != "")
{
    $str_availability = trim($_POST["txt_availability"]);
}

if(isset($_POST["cbo_avail_status"])  && $_POST["cbo_avail_status"] != "")
{
    $str_availability_status = trim($_POST["cbo_avail_status"]);
}
if(isset($_POST["txt_dshipping_value"])  && $_POST["txt_dshipping_value"] != "")
{
    $int_domestic_shipping_value = trim($_POST["txt_dshipping_value"]);
}
if(isset($_POST["txt_dshipping_info"])  && $_POST["txt_dshipping_info"] != "")
{
    $str_domestic_shipping_info = trim($_POST["txt_dshipping_info"]);
}
if(isset($_POST["txt_ishipping_value"])  && $_POST["txt_ishipping_value"] != "")
{
    $int_international_shipping_value = trim($_POST["txt_ishipping_value"]);
}
if(isset($_POST["txt_ishipping_info"])  && $_POST["txt_ishipping_info"] != "")
{
    $str_international_shipping_info = trim($_POST["txt_ishipping_info"]);
}
if(isset($_POST["txt_weight"])  && $_POST["txt_weight"] != "")
{
    $int_weight = trim($_POST["txt_weight"]);
}

if(isset($_POST["txt_seo_title"]) && trim($_POST["txt_seo_title"])!="")
{
    $str_seo_title = trim($_POST["txt_seo_title"]);
}
if(isset($_POST["txt_seo_keywords"]) && trim($_POST["txt_seo_keywords"])!="")
{
    $str_seo_keywords = trim($_POST["txt_seo_keywords"]);
}
if(isset($_POST["ta_seo_desc"]))
{
    $str_seo_desc = trim($_POST["ta_seo_desc"]);
}

if(isset($_POST["cbo_new"]))
{
    $str_new = trim($_POST["cbo_new"]);
}
if(isset($_POST["cbo_hot"]))
{
    $str_hot = trim($_POST["cbo_hot"]);
}
if(isset($_POST["cbo_featured"]))
{
    $str_featured = trim($_POST["cbo_featured"]);
}
if(isset($_POST["cbo_visible"]))
{
    $str_visible = trim($_POST["cbo_visible"]);
}


/*if(isset($_POST["cbo_purchase"]))
{
    $str_purchase=trim($_POST["cbo_purchase"]);
}
if(isset($_POST["cbo_disnew"]))
{
    $str_new=trim($_POST["cbo_disnew"]);
}
if(isset($_POST["cbo_dishot"]))
{
    $str_hot=trim($_POST["cbo_dishot"]);
}
if(isset($_POST['txt_ptitle']))
{
    $str_ptitle=trim($_POST['txt_ptitle']);
}
if(isset($_POST['txt_purl']))
{
    $str_purl=trim($_POST['txt_purl']);
}
if(isset($_FILES['video_clip']))
{
    $str_video_file=trim($_FILES['video_clip']['name']);
}
if(isset($_POST['cbo_extension']))
{
    $str_ext=trim($_POST['cbo_extension']); 
}
*/
#----------------------------------------------------------------------------------------------------
#Create String For Redirect
$str_redirect = "";
$str_redirect .= "catpkid=". urlencode(RemoveQuote($int_catpkid));
$str_redirect .= "&subcatpkid=". urlencode(RemoveQuote($int_subcatpkid));
$str_redirect .= "&desc=". urlencode(RemoveQuote($str_desc));
$str_redirect .= "&itemcode=". urlencode(RemoveQuote($str_itemcode));
$str_redirect .= "&title=". urlencode(RemoveQuote($str_title));

//$str_redirect .= "&color=". urlencode(RemoveQuote($str_color));
//$str_redirect .= "&size=". urlencode(RemoveQuote($str_size));
///$str_redirect .= "&ptype=". urlencode(RemoveQuote($str_type));

$str_redirect .= "&listprice=". urlencode($int_listprice);
$str_redirect .= "&ourprice=". urlencode($int_ourprice);
//$str_redirect .= "&memberprice=". urlencode($int_memberprice);
$str_redirect .= "&dshippingval=". urlencode($int_domestic_shipping_value);
$str_redirect .= "&dshippinginfo=". urlencode(RemoveQuote($str_domestic_shipping_info));
$str_redirect .= "&ishippingval=". urlencode($int_international_shipping_value);
$str_redirect .= "&ishippinginfo=". urlencode(RemoveQuote($str_international_shipping_info));
$str_redirect .= "&weight=". urlencode(RemoveQuote($int_weight));
$str_redirect .= "&new=". urlencode(RemoveQuote($str_new));
$str_redirect .= "&hot=". urlencode(RemoveQuote($str_hot));
$str_redirect .= "&featured=". urlencode(RemoveQuote($str_featured));
$str_redirect .= "&visible=". urlencode(RemoveQuote($str_visible));
$str_redirect .= "&seotitle=". urlencode(RemoveQuote($str_seo_title));
$str_redirect .= "&seokeyword=". urlencode(RemoveQuote($str_seo_keywords));
$str_redirect .= "&seodescription=". urlencode(RemoveQuote($str_seo_desc));
//print $str_redirect."<br/>";exit;
#--------------------------------------------------------------------------------------------------------------------------------
# Get Category ID
$str_query_select = "";
$str_query_select = "SELECT catpkid,subcattitle FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid=".$int_subcatpkid;
$rs_list_subcat = GetRecordset($str_query_select);
if ($rs_list_subcat->EOF())
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&".$str_redirect);
    exit();

    /*$response['status']='ERR';
    $response['IFE']= "SM";
    $response['redirect_string'] = $str_redirect;
    echo json_encode($response); 
    return;*/
}

$int_catpkid = $rs_list_subcat->fields("catpkid");

#--------------------------------------------------------------------------------------------------------------------------------
# query string datda to forward
//$str_querystring  = "&mid=".urlencode(RemoveQuote($int_mid));
$str_querystring  = "";
$str_querystring = $str_querystring . "&subcatpkid=".$int_subcatpkid."&t_title=".urlencode(RemoveQuote($str_title)). "&desc=".urlencode(RemoveQuote($str_desc));
//$str_querystring = $str_querystring ."&disnew=".urlencode(RemoveQuote($str_new))."&dishot=".urlencode(RemoveQuote($str_hot))."&purchase=".urlencode(RemoveQuote($str_purchase));
//$str_querystring = $str_querystring ."&rmonth=".urlencode(RemoveQuote($str_month))."&ryear=".urlencode(RemoveQuote($str_year))."&pricepkid=".urlencode(RemoveQuote($int_pricepkid))."&nophoto".urlencode(RemoveQuote($str_nophoto));
#---------------------------------------------------------------------------------------------------------------------------------
# check for null data
if($int_subcatpkid == 0 || $str_itemcode == "" || $str_title == ""  || $int_listprice == 0 || $int_listprice == "")
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&".$str_redirect);
    exit();
    
    /*$response['status']='ERR';
    $response['IFE']= "SM";
    $response['redirect_string'] = $str_redirect;
    echo json_encode($response); 
    return;*/
}
if($int_ourprice == "" || $int_ourprice == 0)
{
    $int_ourprice = $int_listprice;
}
//print_r($_POST);exit;
//print $int_subcatpkid; exit;
	
#Check File Extension whether it is valid extension or not
/*if($str_video_file!="")
{
    if(CheckFileExtension($str_video_file,$STR_VIDEO_FILE_TYPE_VALIDATION)==0)
    {
        $response['status']='ERR';
        $response['IFE']= "IF";
        echo json_encode($response); 
        return;
    }
}*/

#-----------------------------------------------------------------------------------------------------------------
#Check Approve Date
/*if($str_purl!="")
{
    if(validateURL02($str_purl)==false)
    {
        $response['status']='ERR';
        $response['IFE']= "IU";
        echo json_encode($response); 
        return;
    }
}*/	
#-----------------------------------------------------------------------------------------------------------------	
# Duplication Check
$str_query_select = "SELECT count(subcatpkid) noofrecords FROM ".$STR_DB_TABLE_NAME." WHERE title='" . ReplaceQuote($str_title) . "' AND subcatpkid=".$int_subcatpkid;
$rs_list_check_duplicate = GetRecordset($str_query_select);
if ($rs_list_check_duplicate->fields("noofrecords") > 0)
{
    CloseConnection();
    Redirect("item_list.php?msg=DU&type=E&".$str_redirect);
    exit();
    
    /*$response['status'] = 'ERR';
    $response['IFE']=   "DU";
    $response['redirect_string'] = $str_redirect;
    echo json_encode($response); 
    return;*/
}

$int_max_displayorder = 0;
$int_max_displayorder = GetSubcatMaxValue($STR_DB_TABLE_NAME,"subcatpkid",$int_subcatpkid,"displayorder");
	
$str_create_datetime = "";
$str_create_datetime = date("Y-m-d H:i:s");
#-----------------------------------------------------------------------------------------------------------------
/*
#	Set to allow purchase YES If admin set price for a phoroset otherwise it set NO
if($int_pricepkid!=0 && $int_pricepkid!="")
{
	$str_purchase="YES";	
}*/
$str_query_select = "SELECT displayasadditionalinfo FROM ".$STR_DB_TABLE_NAME;
$rs_list_info = GetRecordset($str_query_select);
//$str_additional_info = $rs_list_info->Fields("displayasadditionalinfo")
#-----------------------------------------------------------------------------------------------------------------
#Insert Query
$str_query_insert = "";
$str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME."(catpkid,subcatpkid,itemcode,title, description,createdatetime, displayasnew,displayashot ,displayasfeatured, displayorder, visible, listprice, ourprice, memberprice, inmonth, inyear, availability, availability_info, approved, dshippingvalue,";
$str_query_insert.= "dshippinginfo, ishippingvalue, ishippinginfo, weight, seotitle, seokeyword, seodescription) VALUES (";
$str_query_insert.= "".$int_catpkid.", ";
$str_query_insert.= "".$int_subcatpkid.", ";
$str_query_insert.= "'".ReplaceQuote($str_itemcode)."', ";
$str_query_insert.= "'".ReplaceQuote($str_title)."', ";
$str_query_insert.= "'".ReplaceQuote($str_desc)."', ";
$str_query_insert.= "'".$str_create_datetime."', ";
$str_query_insert.= "'".ReplaceQuote($str_new)."', ";
$str_query_insert.= "'".ReplaceQuote($str_hot)."', ";
$str_query_insert.= "'".ReplaceQuote($str_featured)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_additional_info)."', ";
$str_query_insert.= "".$int_max_displayorder.", ";
$str_query_insert.= "'".ReplaceQuote($str_visible)."', ";
$str_query_insert.= "".$int_listprice.", ";
$str_query_insert.= "".$int_ourprice.", ";
$str_query_insert.= "".$int_memberprice.", ";
$str_query_insert.= "".date("m").", ";
$str_query_insert.= "".date("Y").", ";
//$str_query_insert.= "'".ReplaceQuote($str_size)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_color)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_type)."', ";

//$str_query_insert.= "'".ReplaceQuote($str_length)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_work)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_occasion)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_shipping_type)."', ";
//$str_query_insert.= "'".ReplaceQuote($str_fabric)."', ";
//$str_query_insert.= "".$int_dressmaterialprice.", ";
//$str_query_insert.= "".$int_semistitchedprice.", ";
//$str_query_insert.= "".$int_customizedprice.", ";
$str_query_insert.= "'".ReplaceQuote($str_availability_status)."', ";
$str_query_insert.= "'".ReplaceQuote($str_availability)."', ";
$str_query_insert.= "'YES', ";
$str_query_insert.= "".$int_domestic_shipping_value.", ";
$str_query_insert.= "'".ReplaceQuote($str_domestic_shipping_info)."', ";
$str_query_insert.= "".$int_international_shipping_value.", ";
$str_query_insert.= "'".ReplaceQuote($str_international_shipping_info)."', ";
$str_query_insert.= "".$int_weight.", ";
$str_query_insert.= "'".ReplaceQuote($str_seo_title)."', ";
$str_query_insert.= "'".ReplaceQuote($str_seo_keywords)."', ";
$str_query_insert.= "'".ReplaceQuote($str_seo_desc)."') ";
//print $str_query_insert;exit;
ExecuteQuery($str_query_insert);
#--------------------------------------------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "SELECT pkid FROM ".$STR_DB_TABLE_NAME." WHERE title='". ReplaceQuote($str_title) ."' AND catpkid=".ReplaceQuote($int_catpkid)." AND subcatpkid=".ReplaceQuote($int_subcatpkid)."";
//print $str_query_select;exit;
$rs_list_pkid = GetRecordSet($str_query_select);	
if($rs_list_pkid->EOF()==true)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&".$str_redirect);
    exit();
    
    /*$response['status']='ERR';
    $response['IFE']= "SM";
    echo json_encode($response); 
    return;*/
}
$int_itempkid = $rs_list_pkid->fields("pkid");

#-----------------------------------------------------------------------------------------------------------------
## Color
if(Count($str_color) > 0)
{
    foreach($str_color AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_COLOR."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}
//exit;
## Size
if(Count($str_size) > 0)
{
    foreach($str_size AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_SIZE."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}

## Type
if(Count($str_type) > 0)
{
    foreach($str_type AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_TYPE."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}

## LENGTH
if(Count($str_length) > 0)
{
    foreach($str_length AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_LENGTH."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}

## OCCASION
if(Count($str_occasion) > 0)
{
    foreach($str_occasion AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_OCCASION."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}

## WORK
if(Count($str_work) > 0)
{
    foreach($str_work AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_WORK."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}

## FABRIC
if(Count($str_fabric) > 0)
{
    foreach($str_fabric AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_FABRIC."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}

## SHIPPING
/*if(Count($str_shipping_type) > 0)
{
    foreach($str_shipping_type AS $i):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TR_TABLE_NAME_SHIPPING."(masterpkid, itempkid) VALUES (".$i.", ".$int_itempkid.")";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endforeach;
}*/


#-----------------------------------------------------------------------------------------------------------------
# Upload image
//CreateDirectory($UPLOAD_IMG_PATH.$int_itempkid);

# UPLOAD FILE ON THE SERVER
/*$str_file_name="dfile_".$int_itempkid."_".get_image_text(5).".".$str_file_ext;

$str_video_file_path = "";
$str_video_file_path = $UPLOAD_FILE_PATH."/".$str_file_name;
//print $str_photoset_video_file_path;
//exit;
if($str_video_file!="")
{
    UploadFile($_FILES['video_clip']['tmp_name'],$str_video_file_path);
}

###	Rename Photoset Filename use photosetpkid
//$str_photoset_name="dfile_".$int_itempkid."_".get_image_text(5).".".$str_file_ext;
$str_update_query="UPDATE t_store SET filename='".ReplaceQuote($str_name)."' WHERE pkid=".$int_itempkid;
ExecuteQuery($str_update_query);
*/
#-----------------------------------------------------------------------------------------------------------------------
# write to xml file
//WriteXML($int_subcatpkid);
#-----------------------------------------------------------------------------------------------------------------
#Email Notification to admin from model
/*$str_fp = openXMLfile($STR_XML_FILE_PATH_CMS."siteconfiguration.xml");
$str_from = $str_user_email;
$str_to=getTagValue($STR_FROM_DEFAULT,$str_fp);
closeXMLfile($str_fp);

$str_query_select = "SELECT shortenurlkey FROM t_user WHERE pkid=".$_SESSION["userpkid"]."";
$rs_list = GetRecordSet($str_query_select);
if(!$rs_list->eof())
{
    $str_subject="New Brag is submitted by ".$rs_list->fields("shortenurlkey")." on ".$STR_SITENAME_WITHOUT_PROTOCOL."";
    $mailbody="New Brag is submitted. Below are details.<br/><br/>";
    $mailbody.="<strong>Uploaded By:</strong> ".$rs_list->fields("shortenurlkey")."<br/>";
    $mailbody.="<strong>Category Name:</strong> ".$str_subcattitle." ";
    $mailbody.="<strong>Brag Name:</strong> ".$str_title." ";

    $mailbody.="<br/>Thank You,<br/>".$STR_SITENAME_WITHOUT_PROTOCOL."";
//	print "<br/>".$str_from; print "<br/>".$str_to; print "<br/>".$mailbody;print "<br>".$str_subject; exit;
    sendmail($str_to,$str_subject,$mailbody,$str_from,1);
}*/
#-----------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=S&type=S&".$str_redirect);
exit();
    
/*$response['status'] = 'SUC';
$response['IFE'] =  "SA";
$response['mid'] = $int_subcatpkid;
echo json_encode($response); 
return;*/	
#-----------------------------------------------------------------------------------------------------------------
?>
