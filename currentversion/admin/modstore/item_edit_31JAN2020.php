<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/http_to_https.php";	
include "../../includes/lib_xml.php";
include "./item_config.php";
//print_r($_SESSION);exit;
#------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_cat_pkid = "";
if(isset($_GET["catid"]) && trim($_GET["catid"]) != "" )
{   
    $int_cat_pkid = trim($_GET["catid"]);    
}

$str_name_key = "";
if(isset($_GET["key"]) && trim($_GET["key"]) != "" )
{ $str_name_key = trim($_GET["key"]); }

# get data for paging
$int_page="";
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page = 1; }

$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";

$int_pkid = 0;
if(isset($_GET["pkid"]) && trim($_GET["pkid"])!="" )
{
    $int_pkid=trim($_GET["pkid"]);
}

if($int_pkid=="" || $int_pkid==0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}

$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
//print $str_query_select; exit;
$rs_list = GetRecordset($str_query_select);
if ($rs_list->count()==0)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}	
#------------------------------------------------------------------------------------------------
# Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
	case("E"): $str_type = "E"; break;
	case("W"): $str_type = "W"; break;
    }
}
#	Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"):$str_message = $STR_MSG_ACTION_INFO_MISSING;break;				
        case("DT"):$str_message =  $STR_MSG_ACTION_TITLE_ALREADY_EXIST;break;
        case("IU"):$str_message =  $STR_MSG_ACTION_INVALID_URL_FORMAT;break;
    }
} 
#----------------------------------------------------------------------
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_CAT." ORDER BY title ASC";
//print $str_query_select; exit; 
$rs_list_cat = GetRecordSet($str_query_select); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE) ;?></title>    
</head>
<body>
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="container center-bg">
        <a name="ptop" id="ptop"></a>
        <div class="row padding-10">
           <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="row padding-10">
                    <div class="col-md-3 col-sm-6 col-xs-12 "><br/>
                        <div class="btn-group" role="group" aria-label="...">
                            <a href="./item_list.php?<?php print $str_filter; ?>" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE);?></a>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-6 col-xs-12 " align="right" >
                        <h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE);?></h3>
                    </div>
                </div><hr/>
                <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); } ?>
                <div class="row padding-10">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <b><i class="fa fa-edit"></i>&nbsp;<?php print($STR_TITLE_EDIT); ?> </b>
                                        <?php print($STR_LINK_HELP); ?>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="false" <?php /* ?> style="height: 0px;"<?php */ ?>>
                                    <form id="frm_edit" name="frm_edit" method="POST" action="item_edit_p.php" onSubmit="return frm_edit_validate();" >
                                        <div class="panel-body">
                                            <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                            <div class="row padding-10">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Category</label>
                                                        <span class="text-help-form"> *</span>
                                                        <select id="cbo_cat" name="cbo_cat" class="form-control input-sm" onchange="get_subcat();">
                                                            <option value="0"> -- SELECT CATEGORY -- </option>
                                                            <?php 
                                                            while(!$rs_list_cat->eof())
                                                            { ?>
                                                            <option value="<?php print $rs_list_cat->Fields("catpkid");?>" <?php print CheckSelected($rs_list->Fields("catpkid"), $rs_list_cat->Fields("catpkid")); ?>><?php print $rs_list_cat->Fields("title");?></option>
                                                            <?php $rs_list_cat->MoveNext();
                                                            } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div id="before_get_subcat">
                                                        <label>Select Sub Category</label><span class='text-help-form'> *</span>
                                                        <select class='form-control input-sm' name='cbo_subcat' id='cbo_subcat'></select>
                                                    </div>
                                                    <div id="get_subcat"></div>                                          
                                                </div> 
                                            </div>
                                            <div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Title</label>
                                                        <span class="text-help-form"> *</span><input name="txt_title" id="txt_title"  type="text" class="form-control input-sm" maxlength="255" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" value="<?php print $rs_list->Fields("title");?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row padding-10">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Item Code</label>
                                                        <span class="text-help-form"> *</span><input name="txt_code" id="txt_code"  type="text" class="form-control input-sm" maxlength="255" placeholder="<?php print $STR_PLACEHOLDER_TEXT; ?>" value="<?php print $rs_list->Fields("itemcode");?>" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Weight</label><span class="text-help-form"> *</span>
                                                        <input type="text" name="txt_weight" id="txt_weight" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" size="60" value="<?php print $rs_list->Fields("weight");?>" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row padding-10">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>List Price</label><span class="text-help-form"> *</span>
                                                        <input type="text" name="txt_listprice" id="txt_listprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="<?php print $rs_list->Fields("listprice");?>" >
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Our Price</label><span class="text-help-form"> </span>
                                                        <input type="text" name="txt_ourprice" id="txt_ourprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="<?php print $rs_list->Fields("ourprice");?>" >        
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Wholesaler Price</label><span class="text-help-form"> </span>
                                                        <input type="text" name="txt_memberprice" id="txt_memberprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="<?php print $rs_list->Fields("memberprice");?>" >        
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Description</label><span class="text-help-form"> </span>
                                                        <textarea name="ta_desc" id="ta_desc" rows="10" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>" ><?php print $rs_list->Fields("description");?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Color</label><span class="text-help-form"> </span><br/>
                                                        <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                        <div class="row padding-10">
                                                        <?php 
                                                        $arr_color = array();
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_COLOR." WHERE itempkid=".$rs_list->Fields("pkid");
                                                        
                                                        $rs_list_selected_color = GetRecordSet($str_query_select);
                                                        if($rs_list_selected_color->Count() > 0)
                                                        {
                                                            while(!$rs_list_selected_color->EOF())
                                                            {
                                                                array_push($arr_color, $rs_list_selected_color->Fields("masterpkid"));
                                                                $rs_list_selected_color->MoveNext();
                                                            }
                                                        }
                                                        //print_r($arr_color);
                                                        
                                                        //$arr_color = explode("|",$rs_list->Fields("color"));
                                                        $str_color_checked = "checked";
                                                        
                                                        //print_r($arr_color);
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT pkid, title FROM ".$STR_DB_TABLE_NAME_COLOR." WHERE visible='YES' ORDER BY title,  displayorder";
                                                        $rs_list_color = GetRecordSet($str_query_select);
                                                        while(!$rs_list_color->EOF()) { ?>
                                                            <div class="col-md-2">
                                                                <input type="checkbox" id="cbx_color" name="cbx_color[]" value="<?php print $rs_list_color->Fields("pkid"); ?>" <?php if(in_array($rs_list_color->Fields("pkid"),$arr_color)) { print $str_color_checked; } ?> />&nbsp;<?php print $rs_list_color->Fields("title"); ?>
                                                            </div>
                                                        <?php $rs_list_color->MoveNext();
                                                        }?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><hr/>
                                            <div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Size</label><span class="text-help-form"> </span><br/>
                                                        <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                        <div class="row padding-10">
                                                        <?php 
                                                        $arr_size = array();
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_SIZE." WHERE itempkid=".$rs_list->Fields("pkid");
                                                        
                                                        $rs_list_selected_size = GetRecordSet($str_query_select);
                                                        if($rs_list_selected_size->Count() > 0)
                                                        {
                                                            while(!$rs_list_selected_size->EOF())
                                                            {
                                                                array_push($arr_size, $rs_list_selected_size->Fields("masterpkid"));
                                                                $rs_list_selected_size->MoveNext();
                                                            }
                                                        }
                                                        
                                                        
                                                        //$arr_size = explode("|",$rs_list->Fields("size"));
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT pkid, title FROM ".$STR_DB_TABLE_NAME_SIZE." WHERE visible='YES' ORDER BY displayorder";
                                                        $rs_list_size = GetRecordSet($str_query_select);
                                                        while(!$rs_list_size->EOF()) {
                                                        ?>
                                                            <div class="col-md-2">
                                                                <input type="checkbox" id="cbx_size" name="cbx_size[]" value="<?php print $rs_list_size->Fields("pkid"); ?>" <?php if(in_array($rs_list_size->Fields("pkid"),$arr_size)) { print "checked"; } ?> />&nbsp;<?php print $rs_list_size->Fields("title"); ?>
                                                            </div>
                                                        <?php $rs_list_size->MoveNext();
                                                        }?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><hr/>
                                            <div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Length</label><span class="text-help-form"> </span><br/>
                                                        <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                        <div class="row padding-10">
                                                        <?php 
                                                        $arr_length = array();
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_LENGTH." WHERE itempkid=".$rs_list->Fields("pkid");
                                                        
                                                        $rs_list_selected_length = GetRecordSet($str_query_select);
                                                        if($rs_list_selected_length->Count() > 0)
                                                        {
                                                            while(!$rs_list_selected_length->EOF())
                                                            {
                                                                array_push($arr_length, $rs_list_selected_length->Fields("masterpkid"));
                                                                $rs_list_selected_length->MoveNext();
                                                            }
                                                        }
                                                        
                                                        //$arr_length = explode("|",$rs_list->Fields("length"));
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT pkid, title FROM ".$STR_DB_TABLE_NAME_LENGTH." WHERE visible='YES' ORDER BY displayorder";
                                                        $rs_list_length = GetRecordSet($str_query_select);
                                                        while(!$rs_list_length->EOF()) {
                                                        ?>
                                                            <div class="col-md-2">
                                                                <input type="checkbox" id="cbx_length" name="cbx_length[]" value="<?php print $rs_list_length->Fields("pkid"); ?>" <?php if(in_array($rs_list_length->Fields("pkid"),$arr_length)) { print "checked"; } ?> />&nbsp;<?php print $rs_list_length->Fields("title"); ?>
                                                            </div>
                                                        <?php $rs_list_length->MoveNext();
                                                        }?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><hr/>
                                            <div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Type / Model</label><span class="text-help-form"> </span><br/>
                                                        <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                        <div class="row padding-10">
                                                        <?php 
                                                        $arr_type = array();
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_TYPE." WHERE itempkid=".$rs_list->Fields("pkid");
                                                        
                                                        $rs_list_selected_type = GetRecordSet($str_query_select);
                                                        if($rs_list_selected_type->Count() > 0)
                                                        {
                                                            while(!$rs_list_selected_type->EOF())
                                                            {
                                                                array_push($arr_type, $rs_list_selected_type->Fields("masterpkid"));
                                                                $rs_list_selected_type->MoveNext();
                                                            }
                                                        }
                                                        
                                                        //$arr_type = explode("|",$rs_list->Fields("type"));
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT pkid, title FROM ".$STR_DB_TABLE_NAME_TYPE." WHERE visible='YES' ORDER BY displayorder";
                                                        $rs_list_type = GetRecordSet($str_query_select);
                                                        while(!$rs_list_type->EOF()) {
                                                        ?>
                                                            <div class="col-md-2">
                                                                <input type="checkbox" id="cbx_type" name="cbx_type[]" value="<?php print $rs_list_type->Fields("pkid"); ?>" <?php if(in_array($rs_list_type->Fields("pkid"),$arr_type)) { print "checked"; } ?> />&nbsp;<?php print $rs_list_type->Fields("title"); ?>
                                                            </div>
                                                        <?php $rs_list_type->MoveNext();
                                                        }?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><hr/>
                                            <div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Work</label><span class="text-help-form"> </span><br/>
                                                        <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                        <div class="row padding-10">
                                                        <?php 
                                                        $arr_work = array();
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_WORK." WHERE itempkid=".$rs_list->Fields("pkid");
                                                        
                                                        $rs_list_selected_work = GetRecordSet($str_query_select);
                                                        if($rs_list_selected_work->Count() > 0)
                                                        {
                                                            while(!$rs_list_selected_work->EOF())
                                                            {
                                                                array_push($arr_work, $rs_list_selected_work->Fields("masterpkid"));
                                                                $rs_list_selected_work->MoveNext();
                                                            }
                                                        }
                                                        
                                                        //$arr_work = explode("|",$rs_list->Fields("work"));
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT pkid, title FROM ".$STR_DB_TABLE_NAME_WORK." WHERE visible='YES' ORDER BY displayorder";
                                                        $rs_list_work = GetRecordSet($str_query_select);
                                                        while(!$rs_list_work->EOF()) {
                                                        ?>
                                                            <div class="col-md-2">
                                                                <input type="checkbox" id="cbx_work" name="cbx_work[]" value="<?php print $rs_list_work->Fields("pkid"); ?>" <?php if(in_array($rs_list_work->Fields("pkid"),$arr_work)) { print "checked"; } ?> />&nbsp;<?php print $rs_list_work->Fields("title"); ?>
                                                            </div>
                                                        <?php $rs_list_work->MoveNext();
                                                        }?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><hr/>
                                            <div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Fabric</label><span class="text-help-form"> </span><br/>
                                                        <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                        <div class="row padding-10">
                                                        <?php 
                                                        $arr_fabric = array();
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_FABRIC." WHERE itempkid=".$rs_list->Fields("pkid");
                                                        
                                                        $rs_list_selected_fabric = GetRecordSet($str_query_select);
                                                        if($rs_list_selected_fabric->Count() > 0)
                                                        {
                                                            while(!$rs_list_selected_fabric->EOF())
                                                            {
                                                                array_push($arr_fabric, $rs_list_selected_fabric->Fields("masterpkid"));
                                                                $rs_list_selected_fabric->MoveNext();
                                                            }
                                                        }
                                                        //$arr_fabric = explode("|",$rs_list->Fields("fabric"));    
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT pkid, title FROM ".$STR_DB_TABLE_NAME_FABRIC." WHERE visible='YES' ORDER BY displayorder";
                                                        $rs_list_fabric = GetRecordSet($str_query_select);
                                                        while(!$rs_list_fabric ->EOF()) {
                                                        ?>
                                                            <div class="col-md-2">
                                                                <input type="checkbox" id="cbx_fabric" name="cbx_fabric[]" value="<?php print $rs_list_fabric->Fields("pkid"); ?>" <?php if(in_array($rs_list_fabric->Fields("pkid"),$arr_fabric)) { print "checked"; } ?> />&nbsp;<?php print $rs_list_fabric->Fields("title"); ?>
                                                            </div>
                                                        <?php $rs_list_fabric->MoveNext();
                                                        }?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><hr/>
                                            <div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Occasion</label><span class="text-help-form"> </span><br/>
                                                        <?php /* ?><input type="text" name="txt_color" id="txt_color" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_COLOR; ?>" maxlength="255" value="<?php print($str_color);?>" /><?php */ ?>
                                                        <div class="row padding-10">
                                                        <?php 
                                                        $arr_occasion = array();
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_OCCASION." WHERE itempkid=".$rs_list->Fields("pkid");
                                                        
                                                        $rs_list_selected_occasion = GetRecordSet($str_query_select);
                                                        if($rs_list_selected_occasion->Count() > 0)
                                                        {
                                                            while(!$rs_list_selected_occasion->EOF())
                                                            {
                                                                array_push($arr_occasion, $rs_list_selected_occasion->Fields("masterpkid"));
                                                                $rs_list_selected_occasion->MoveNext();
                                                            }
                                                        }
                                                        
                                                        //$arr_occasion = explode("|",$rs_list->Fields("occasion")); 
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT pkid, title FROM ".$STR_DB_TABLE_NAME_OCCASION." WHERE visible='YES' ORDER BY displayorder";
                                                        $rs_list_occasion = GetRecordSet($str_query_select);
                                                        while(!$rs_list_occasion->EOF()) {
                                                        ?>
                                                            <div class="col-md-2">
                                                                <input type="checkbox" id="cbx_occasion" name="cbx_occasion[]" value="<?php print $rs_list_occasion->Fields("pkid"); ?>" <?php if(in_array($rs_list_occasion->Fields("pkid"),$arr_occasion)) { print "checked"; } ?> />&nbsp;<?php print $rs_list_occasion->Fields("title"); ?>
                                                            </div>
                                                        <?php $rs_list_occasion->MoveNext();
                                                        }?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><hr/>
                                            <?php /* ?><div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Shipping Options</label><span class="text-help-form"> </span><br/>
                                                        <div class="row padding-10">
                                                        <?php 
                                                        $arr_shipping = array();
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT masterpkid FROM ".$STR_DB_TR_TABLE_NAME_SHIPPING." WHERE itempkid=".$rs_list->Fields("pkid");
                                                        
                                                        $rs_list_selected_shipping = GetRecordSet($str_query_select);
                                                        if($rs_list_selected_shipping->Count() > 0)
                                                        {
                                                            while(!$rs_list_selected_shipping->EOF())
                                                            {
                                                                array_push($arr_shipping, $rs_list_selected_shipping->Fields("masterpkid"));
                                                                $rs_list_selected_shipping->MoveNext();
                                                            }
                                                        }
                                                        
                                                        //$arr_shipping = explode("|",$rs_list->Fields("shippingtype"));
                                                        
                                                        $str_query_select = "";
                                                        $str_query_select = "SELECT pkid, title FROM ".$STR_DB_TABLE_NAME_SHIPPING." WHERE visible='YES' ORDER BY displayorder";
                                                        $rs_list_shipping = GetRecordSet($str_query_select);
                                                        while(!$rs_list_shipping->EOF()) {
                                                        ?>
                                                            <div class="col-md-2">
                                                                <input type="checkbox" id="cbx_shipping" name="cbx_shipping[]" value="<?php print $rs_list_shipping->Fields("pkid"); ?>" <?php if(in_array($rs_list_shipping->Fields("pkid"),$arr_shipping)) { print "checked"; } ?>  />&nbsp;<?php print $rs_list_shipping->Fields("title"); ?>
                                                            </div>
                                                        <?php $rs_list_shipping->MoveNext();
                                                        }?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr/><?php */ ?>
                                            <?php /* ?><div class="row padding-10">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Dress Material Price</label><span class="text-help-form"> </span>
                                                        <input type="text" name="txt_dressmaterialprice" id="txt_dressmaterialprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="<?php print(trim($rs_list->Fields("freesizeprice")));  ?>" >
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Semi Stitched Price</label><span class="text-help-form"> </span>
                                                        <input type="text" name="txt_semistitchedprice" id="txt_semistitchedprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="<?php print(trim($rs_list->Fields("semistitchedprice")));  ?>" >        
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Customized Price</label><span class="text-help-form"> </span>
                                                        <input type="text" name="txt_customizedprice" id="txt_customizedprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="<?php print(trim($rs_list->Fields("customizedprice")));  ?>" >                                                        </div>
                                                </div>
                                            </div>
                                            <hr/><?php */ ?>
                                            <div class="row padding-10">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Availability</label><span class="text-help-form"> </span>
                                                        <input type="text" name="txt_availability" id="txt_availability" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_TEXT;?>" maxlength="255" value="<?php print(($rs_list->Fields("availability_info")));  ?>" >
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Availability Status</label><span class="text-help-form"> *</span>
                                                        <select class="form-control input-sm" id="cbo_avail_status" name="cbo_avail_status">
                                                            <option value="YES" <?php print(CheckSelected("YES",$rs_list->Fields("availability"))); ?>>In Stock</option>
                                                            <option value="NO" <?php print(CheckSelected("NO",$rs_list->Fields("availability"))); ?>>Out Of Stock</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div><hr/>
                                            <div class="row padding-10">
                                                <?php /* ?><div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Domestic Shipping Value</label><span class="text-help-form"> </span>
                                                        <input type="text" name="txt_dshipping_value" id="txt_dshipping_value" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" size="60" value="<?php print RemoveQuote($rs_list->Fields("dshippingvalue"));?>">
                                                    </div>
                                                </div><?php */ ?>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Domestic Shipping Info</label><span class="text-help-form"> </span>
                                                        <input type="text" name="txt_dshipping_info" id="txt_dshipping_info" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_TEXT;?>"  maxlength="255" size="60" value="<?php print MyHtmlEncode($rs_list->Fields("dshippinginfo"));?>" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row padding-10">
                                                <?php /* ?><div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>International Shipping Value</label><span class="text-help-form"> </span>
                                                        <input type="text" name="txt_ishipping_value" id="txt_ishipping_value" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" size="60" value="<?php print $rs_list->Fields("ishippingvalue");?>" >
                                                    </div>
                                                </div><?php */ ?>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>International Shipping Info</label><span class="text-help-form"> </span>
                                                        <input type="text" name="txt_ishipping_info" id="txt_ishipping_info" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_TEXT;?>"  maxlength="255" size="60" value="<?php print $rs_list->Fields("ishippinginfo");?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row padding-10">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Display As New</label><span class="text-help-form"> *</span>
                                                        <span class="text-help-form"> (<?php print($STR_MSG_NEW);?>)</span>
                                                        <select id="cbo_new" name="cbo_new" class="form-control input-sm">
                                                            <option value="NO" <?php print(CheckSelected("NO",$rs_list->Fields("displayasnew"))); ?>>NO</option>
                                                            <option value="YES" <?php print(CheckSelected("YES",$rs_list->Fields("displayasnew")));?>>YES</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Display As Featured</label><span class="text-help-form"> *</span>
                                                        <span class="text-help-form"> (<?php print($STR_MSG_FEATURED);?>)</span>
                                                        <select id="cbo_featured" name="cbo_featured" class="form-control input-sm">
                                                            <option value="NO" <?php print(CheckSelected("NO",$rs_list->Fields("displayasfeatured"))); ?>>NO</option>
                                                            <option value="YES" <?php print(CheckSelected("YES",$rs_list->Fields("displayasfeatured")));?>>YES</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Display As Hot</label><span class="text-help-form"> *</span>
                                                        <span class="text-help-form"> (<?php print($STR_MSG_HOT);?>)</span>
                                                        <select id="cbo_hot" name="cbo_hot" class="form-control input-sm">
                                                            <option value="NO" <?php print(CheckSelected("NO",$rs_list->Fields("displayashot"))); ?>>NO</option>
                                                            <option value="YES" <?php print(CheckSelected("YES",$rs_list->Fields("displayashot")));?>>YES</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>SEO Title</label><span class="text-help-form"> </span>
                                                        <input type="text" name="txt_seo_title" id="txt_seo_title" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_SEO_TITLE; ?>" value="<?php print RemoveQuote($rs_list->Fields("seotitle"));?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>SEO Keywords</label><span class="text-help-form"> </span>
                                                        <input type="text" name="txt_seo_keywords" id="txt_seo_keywords" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_SEO_KEYWORDS; ?>"  value="<?php print RemoveQuote($rs_list->Fields("seokeyword"));?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row padding-10">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>SEO Description</label><span class="text-help-form"> </span>
                                                        <textarea name="ta_seo_desc" id="ta_seo_desc" rows="5" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>" ><?php print(RemoveQuote($rs_list->Fields("seodescription"))); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>   
                                            <input type="hidden" name="hdn_pkid" value="<?php print($int_pkid);?>">
                                            <input type="hidden" name="catid" id="catid" value="<?php print($int_cat_pkid);?>">
                                            <input type="hidden" name="key" id="key" value="<?php print($str_name_key);?>">
                                            <input type="hidden" name="PagePosition" id="PagePosition" value="<?php print($int_page);?>">
                                            <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                        </div>                                                                                          
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>                        
                </div> 
                <?php include "../../includes/help_for_edit.php"; ?>
            </div>
        </div>
    </div>   
    
    <?php /* ?>  <script language="JavaScript" src="../js/jquery.min.js"></script>
    <script language="JavaScript" src="../js/bootstrap.min.js"></script> <?php */ ?>
   
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/font-awesome.min.css" rel="stylesheet">
<link href="../../css/admin.css" rel="stylesheet">


<script src="../../js/jquery.min.js"></script>
 <script type="text/javascript">
        $(document).ready( function () {
           <?php if($rs_list->fields("catpkid") != 0) { ?>return get_subcat2(<?php print $rs_list->fields("catpkid"); ?>);<?php } ?>
        });
        function get_subcat2(categorypkid) { // Call to ajax function

            var catpkid = categorypkid;
            //alert(catpkid);
            var dataString = "cat_pkid="+catpkid+"&subcat_pkid=<?php print $rs_list->Fields("subcatpkid"); ?>";
            $.ajax({
                type: "POST",
                url: "item_sel_subcategory_edit_p.php", // Name of the php files
                data: dataString,
                success: function(html)
                {
                    $("#before_get_subcat").hide();
                    $("#get_subcat").html(html);
                }
            });
        }
        function get_subcat() { // Call to ajax function
            var catpkid = $('#cbo_cat').val();
            //alert(catpkid);
            var dataString = "cat_pkid="+catpkid;
            $.ajax({
                type: "POST",
                url: "item_sel_subcategory_p.php", // Name of the php files
                data: dataString,
                success: function(html)
                {
                    $("#before_get_subcat").hide();
                    $("#get_subcat").html(html);
                }
            });
        }
    </script>
    <script src="../../js/bootstrap.min.js"></script>
    <script language="JavaScript" src="../../js/functions.js" type="text/javascript"></script>
       
    <?php include($STR_ADMIN_FOOTER_PATH); ?>
    <script language="JavaScript" src="./item_edit.js"></script>  
    
    <script src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/richetxteditor.js"></script>
    <script type="text/javascript">
      bkLib.onDomLoaded(function() {
            new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_desc');
                    //new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
      });
                    // convert all text areas to rich text editor on that page
            bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
                    // convert text area with id area2 to rich text editor with full panel.
            bkLib.onDomLoaded(function() {
                 new nicEditor({fullPanel : true}).panelInstance('ta_desc');
                            // new nicEditor({fullPanel : true}).panelInstance('ta_remark');
            });
    </script>
</body>
</html>
