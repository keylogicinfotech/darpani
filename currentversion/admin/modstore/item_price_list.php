<?php
/*
    Module Name:- Modstore
    File Name  :- item_photo_list.php
    Create Date:- 08-Sep-2006
    Intially Create By :- 0014
    Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
    include "../../includes/validatesession.php";
    include "../../includes/configuration.php";
    include "item_config.php";
    include "item_app_specific.php";
    include "../../includes/lib_data_access.php";
    include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
#initialize variables	
# filter variables
$int_cat_pkid = "";
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ $int_cat_pkid = trim($_GET["catid"]); }

$str_name_key = "";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_name_key = trim($_GET["key"]); }

# get data for paging
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page = 1; }

$str_filter = ""; 
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";

#----------------------------------------------------------------------------------------------------
# get filter data
/*$int_userpkid = "";
if(isset($_GET["uid"]) && trim($_GET["uid"])!="" )
{
    $int_userpkid = trim($_GET["uid"]);
}*/
        //print $int_mid; exit;    
#-----------------------------------------------------------------------------------------------------
#getting query string datas
$int_pkid="";
if(isset($_GET['pkid']))
{
    $int_pkid=trim($_GET['pkid']);
}
if($int_pkid=="" || !is_numeric($int_pkid) || $int_pkid<0)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}

#select query to all details from tr_ppd_photo and t_ppd table	
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TR_TABLE_NAME_PRICE." WHERE itempkid=".$int_pkid." ORDER BY displayorder DESC";
//print $str_query_select; exit;
$rs_list = GetRecordset($str_query_select);
	
$str_query_select = "";
$str_query_select = "SELECT title FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;	
$rs_list_details = GetRecordset($str_query_select);
if($rs_list_details->EOF()==true)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}
#--------------------------------------------------------------------------------------------------------------
#initializing variables
    $str_visible = "";
    if(isset($_GET["visible"]))
    { 
        $str_visible=trim($_GET["visible"]);
    }
    
#------------------------------------------------------------------------------------------

#   Initialization of variables used for message display.   
    $str_type = "";
    $str_message = "";
    $str_mode = "";
    
    if($str_mode=="YES") { $str_md="Visible"; }  else { $str_md="Invisible"; }
    
    if(isset($_GET['mode']))
    {
        $str_mode=trim($_GET['mode']);
    }
# Get message type.
    if(isset($_GET['type']))
    {
        switch(trim($_GET['type']))
	{
            case("S"): $str_type = "S"; break;
            case("E"): $str_type = "E"; break;
            case("W"): $str_type = "W"; break;
	}
    }
# Get message text.
    if($str_mode=="YES") { $str_md="VISIBLE"; }  else { $str_md="INVISIBLE"; }
    if(isset($_GET['msg']))
    {
        switch(trim($_GET['msg']))
	{
            case("F"):$str_message =  $STR_MSG_ACTION_INFO_MISSING; break;
            case("S"):$str_message = $STR_MSG_ACTION_ADD; break;
            case("D"):$str_message = $STR_MSG_ACTION_DELETE; break;
            case("U"):$str_message = $STR_MSG_ACTION_EDIT; break;
            case("O"):$str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
            case("SF"):$str_message = $STR_MSG_ACTION_SET_AS_FRONT; break;
            case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_md; break;
            case("I"): 	$str_message =$STR_MSG_ACTION_INVALID_FILE_EXT; break; 
            case("W"):$str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;	
            case("DU"):$str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;	
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_PAGE_PRICE_LIST);?> [<?php print $rs_list_details->Fields("title"); ?>]</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> 
<a name="ptop" id="ptop"></a>
<div class="container content_bg">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <a href="./item_list.php?<?php print $str_filter; ?>" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
            </div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE_PRICE_LIST);?> [<?php print $rs_list_details->Fields("title"); ?>]</h3></div>
    </div><hr> 
    <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <b><a class="accordion-toggle collapsed" data-toggle="collapse" title="<?php print($STR_HOVER_ADD); ?>" data-parent="#accordion"  href="#collapseOne" aria-expanded="false"><?php print($STR_TITLE_ADD); ?></a></b>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="false">
                        <div class="panel-body">
                            <form id="frm_add" name="frm_add" action="item_price_add_p.php" method="POST" onSubmit="return frm_add_validate()" >
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <?php 
                                $str_query_select = "";
                                $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_COUNTRY." WHERE visible='YES' ORDER BY title ASC, displayorder DESC";
                                $rs_list_type = GetRecordSet($str_query_select);
                                ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Select Country</label><span class="text-help-form"> *</span>
                                            <span class="text-help-form"></span>
                                            <select id="cbo_country" name="cbo_country" class="form-control input-sm">
                                                <option value="0">-- SELECT COUNTRY --</option>
                                                <?php 
                                                while(!$rs_list_type->EOF()) {
                                                ?>
                                                <option value="<?php print $rs_list_type->Fields("pkid"); ?>"><?php print $rs_list_type->Fields("title"); ?></option>
                                                <?php $rs_list_type->MoveNext();
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>List Price</label><span class="text-help-form"> *</span>
                                            <span class="text-help-form"></span>
                                            <input type="text" name="txt_listprice" id="txt_listprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Our Price</label><span class="text-help-form"> </span>
                                            <span class="text-help-form"></span>
                                            <input type="text" name="txt_ourprice" id="txt_ourprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Wholesaler Price</label><span class="text-help-form"> </span>
                                            <span class="text-help-form"></span>
                                            <input type="text" name="txt_memberprice" id="txt_memberprice" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE;?>" maxlength="255" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Visible</label><span class="text-help-form"> *</span>
                                            <span class="text-help-form">(<?php print($STR_MSG_VISIBLE)?>)</span>
                                            <select name="cbo_visible" class="form-control input-sm">
                                                <option value="YES">YES</option>
                                                <option value="NO">NO</option>
                                            </select>
                                        </div>
                                    </div>                                            
                                </div>
                                <input type="hidden" name="hdn_masterpkid" value="<?php print($int_pkid); ?>">
                                <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <form name="frm_list" action="./item_price_order_p.php" method="POST" onSubmit="return frm_list_check_displayorder();">
                    <table class="table table-striped table-bordered ">
                    <thead align="center">
                        <tr>
                            
                            <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                            <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                            <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_LIST_PRICE; ?></th>
                            <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_OUR_PRICE; ?></th>
                            <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_MEMBER_PRICE; ?></th>
                            <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_ORDER; ?></th>
                            <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>                        
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($rs_list->EOF()==true)  {  ?>
                           <tr><td colspan="8" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                        <?php } else { 
                            $int_cnt = 1; 
                            while(!$rs_list->EOF()==true) { ?>
                            <tr>
                                <td style="vertical-align: middle;" align="center"><?php print($int_cnt);?></td>
                                <td align="left">
                                <?php $str_query_select = "";
                                $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_COUNTRY." WHERE pkid = ".$rs_list->Fields("countrypkid");
                                $rs_country_name = GetRecordSet($str_query_select); 
                                ?>
                                <span class="text-primary"></span>
                                        <h4><b><?php print $rs_country_name->Fields("title");?></b></h4>
                                </td>
                                <td align="right">
                                   <?php print $rs_country_name->Fields("currency_symbol"); ?>&nbsp;<?php print $rs_list->fields("listprice"); ?>
                                </td>
                                <td align="right">
                                   <?php print $rs_country_name->Fields("currency_symbol"); ?>&nbsp;<?php print $rs_list->fields("ourprice"); ?>
                                </td>
                                <td align="right">
                                   <?php print $rs_country_name->Fields("currency_symbol"); ?>&nbsp;<?php print $rs_list->fields("memberprice"); ?>
                                </td>
                                <td style="vertical-align: middle;" align="center">
                                   <input type="text" name="txt_displayorder<?php print($int_cnt);?>" size="5" maxlength="5" value="<?php print($rs_list->fields("displayorder"));?>" class="form-control input-sm text-align" style="text-align:center;">
                                   <input type="hidden"  name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>">
                                </td>
                                <td align="center" style="vertical-align: middle;" >
                                <?php
                                    $str_image="";
                                    if(strtoupper($rs_list->fields("visible"))=="YES")
                                    {   $str_image = $STR_LINK_ICON_PATH_VISIBLE;
                                        $str_class = "btn btn-warning btn-xs";
                                        $str_title = $STR_HOVER_VISIBLE;
                                    }
                                    else
                                    {   $str_image = $STR_LINK_ICON_PATH_INVISIBLE;
                                        $str_class = "btn btn-default active btn-xs";
                                        $str_title = $STR_HOVER_INVISIBLE;
                                    } ?>
                                    <a class="<?php print($str_class); ?>" href="item_price_visible_p.php?pkid=<?php print($rs_list->fields("pkid"))?>&masterpkid=<?php print($rs_list->fields("itempkid"))?><?php print($str_filter);?>" title="<?php print($str_title);?>"><?php print($str_image);?></a>
                                    <a class="btn btn-success btn-xs" href="item_price_edit.php?pkid=<?php print($rs_list->fields("pkid"))?>&masterpkid=<?php print($rs_list->fields("itempkid"))?><?php print($str_filter);?>" title="<?php print($STR_HOVER_EDIT)?>"><?php print $STR_LINK_ICON_PATH_EDIT; ?></a>
                                    <a class="btn btn-danger btn-xs" href="item_price_del_p.php?pkid=<?php print($rs_list->fields("pkid"))?>&masterpkid=<?php print($rs_list->fields("itempkid"))?><?php print($str_filter);?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE)?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>
                                </td>
                            </tr>
                            <?php $rs_list->movenext(); 
                            $int_cnt = $int_cnt +1;  
                            } ?>
                            <tr>
                                <td>
                                    <input type="hidden" name="catid" id="catid" value="<?php print($int_cat_pkid);?>">
                                    <input type="hidden" name="key" id="key" value="<?php print($str_name_key);?>">
                                    <input type="hidden" name="PagePosition" id="PagePosition" value="<?php print($int_page);?>">
                                    <input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>">
                                </td>
                                <td><input type="hidden" id="hdn_masterpkid" name="hdn_masterpkid" value="<?php print($int_pkid); ?>">
                                    <?php /* ?><input type="hdn_userpkid" id="" name="hdn_userpkid" value="<?php print($int_userpkid); ?>"><?php */ ?></td>
                                <td></td><td></td><td></td>
                                <td><?php print DisplayFormButton("SAVE",0); ?></td>
                                <td></td>
                                <?php /*?><td class="text-align"><input type="button" class="btn btn-success" name="shop_btn" value="Change" title="Click to change 'Display on Home' setting" onClick="return displayonhome_click();"></td><?php */?>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
<?php include "../../includes/include_files_admin.php"; ?>
<script language="JavaScript" src="./item_price_list.js" type="text/javascript"></script>
</body>
</html>