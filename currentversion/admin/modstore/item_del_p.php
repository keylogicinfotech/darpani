<?php
/*
File Name  :- item_del_p.php
Create Date:- 18-JAN-2019
Intially Create By :- 0014
Update History:
*/
#-----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
#-----------------------------------------------------------------------------------------------------	
#Get values of all passed GET / POST variables
$int_cat_pkid = "";
if(isset($_GET["catid"]) && trim($_GET["catid"]) != "" )
{   
    $int_cat_pkid = trim($_GET["catid"]);    
}

$str_name_key = "";
if(isset($_GET["key"]) && trim($_GET["key"]) != "" )
{ $str_name_key = trim($_GET["key"]); }

# get data for paging
$int_page="";
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page = 1; }

$str_filter = "";
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page."&#ptop";


$int_pkid = 0;
if(isset($_GET['pkid']))
{
    $int_pkid = $_GET['pkid'];
}
if($int_pkid <= 0 || !is_numeric($int_pkid) || $int_pkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F".$str_filter);
    exit();
}
#-----------------------------------------------------------------------------------------------------	
#Select query to get the details from table
/*$str_query_select = "";
$str_query_select = "SELECT title,imagefilename FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
$rs_list = GetRecordset($str_query_select);
if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F".$str_filter);
    exit();
}
$str_title = "";
$str_title = $rs_list->Fields("title");

$str_imagefilename = "";
$str_imagefilename = $rs_list->Fields("imagefilename");*/
#-----------------------------------------------------------------------------------------------------	
#Delete image file and then delete data from table
/*if(trim($str_imagefilename)!="")
{
    DeleteFile($UPLOAD_IMG_PATH.trim($str_imagefilename));
}*/

if(file_exists($UPLOAD_IMG_PATH.$int_pkid))
{
    RemoveDirectory($UPLOAD_IMG_PATH.$int_pkid);
}
$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_COLOR." WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_SIZE." WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_TYPE." WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_FABRIC." WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_OCCASION." WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_LENGTH." WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TR_TABLE_NAME_WORK." WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_delete);

$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_delete);
#-----------------------------------------------------------------------------------------------------
#write to xml file
//WriteXml();
#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?type=S&msg=D".$str_filter);
exit();
#-----------------------------------------------------------------------------------------------------
?>	
	
