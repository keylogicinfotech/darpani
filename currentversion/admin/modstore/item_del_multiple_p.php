<?php
///----------------------------------------------------------------------------------------------------
///	Module Name:- Model Search Comment
///	File Name  :- tm_del_multiple_p.php
///	Create Date:- 08-MAY-2007
///	Intially Create By :- 
///	Update History:
///----------------------------------------------------------------------------------------------------
///Include Files
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "item_config.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
	include "../includes/lib_file_system.php";
	include "./item_app_specific.php";
//print_r($_POST);exit;
#------------------------------------------------------------------------------------------------
#get filter parameters
#------------------------------------------------------------------------------------------------
/*$page=1;
if (isset($_POST["PagePosition"]))
{
	$page = trim($_POST["PagePosition"]);
}
// if model pkid passed then set filter criteria for modelpkid
$int_model_pkid="-1";
if (isset($_POST["modelpkid"]))
{
	$int_model_pkid = trim($_POST["modelpkid"]);
}
$str_filter_status="&modelpkid=".$int_model_pkid."&PagePosition=".$page."&";*/
////--------------------------------------------------------------------------------------
	# To distinguish different redirect pages
        $str_redirect_page_flag="";
	if(isset($_POST["flg"]) && trim($_POST["flg"])!="" )
	{ $str_redirect_page_flag=trim($_POST["flg"]); }
	$int_cat_pkid=0;
	if(isset($_POST["catid"]) && trim($_POST["catid"])!="" )
	{ $int_cat_pkid=trim($_POST["catid"]); }
        $str_mname = "";
	if(isset($_POST["key"]) && trim($_POST["key"])!="" )
	{ $str_mname=trim($_POST["key"]); }

	# POST data for paging
	if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"])!="" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"])>0)
	{ $int_page = $_POST["PagePosition"]; }
	else
	{ $int_page=1; }

	$str_filter="";
	$str_filter="&catid=".$int_cat_pkid."&key=".$str_mname."&PagePosition=".$int_page;
	
        $int_mdlpkid="";
	if(isset($_POST["mdlid"]))
	{ $int_mdlpkid=trim($_POST["mdlid"]); }
	
	$str_modelname="";
	if(isset($_POST["mname"])) { $str_modelname=trim($_POST["mname"]); }
	
        $int_mempkid="";
	if(isset($_POST["memid"]))
	{ $int_mempkid=trim($_POST["memid"]); }
	
	$str_membername="";
	if(isset($_POST["memname"])) { $str_membername = trim($_POST["memname"]); }
        
	// Comment PKID
	/*$int_pkid=0;
	if(isset($_POST['pkid']))
	{ $int_pkid=$_POST['pkid']; }
	if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
	{ CloseConnection(); Redirect("tm_list.php?type=E&msg=F&mdlid=".$int_mdlpkid."&mname=".$str_modelname); exit(); }*/

////--------------------------------------------------------------------------------------
$int_cnt=0;
if(isset($_POST["hdn_counter"]))
{ $int_cnt=$_POST["hdn_counter"]; }
$int_cnt=$int_cnt-1;
if($int_cnt<=0)
{ 
    CloseConnection();
    Redirect("blog_list.php?type=E&msg=F&mdlid=".$int_mdlpkid."&mname=".$str_modelname."&memid=".$int_mempkid."&memname=".$str_membername.$str_filter);
    exit();
}
//print($int_cnt);
//print("<br>");
for($i=0;$i<=$int_cnt;$i=$i+1)
{  
    if(isset($_POST["chk_sub".$i]))
    {
        $int_pkid=0;
        $int_pkid=$_POST["hdn_pkid".$i];
        //print($int_pkid);
        //print("<br>");

        ///---------------------------------------------------------------------
        ///select query to get the details of pkid from tr_modelsearch_comment table
        $str_select_query="";
        $str_select_query="SELECT * FROM tr_blog where pkid=".$int_pkid;
        $rs_visible=GetRecordset($str_select_query);
        $str_image=$rs_visible->Fields("imagefilename");
        $str_image_large=$rs_visible->Fields("largeimagefilename");
       
        
       // print $int_modelpkid;
        ///---------------------------------------------------------------------
        ///delete small and large image 
        //print $UPLOAD_IMG_FILE_PATH.$int_mdlpkid."/comment/".trim($str_image); exit;
        if(trim($str_image)!="")
        {
                DeleteFile($UPLOAD_IMG_FILE_PATH.trim($str_image));
        }
        if(trim($str_image_large)!="")
        {
                DeleteFile($UPLOAD_IMG_FILE_PATH.trim($str_image_large));		
        }			
        $str_delete="";
        $str_delete="DELETE FROM tr_blog WHERE pkid=".$int_pkid;
        //print $str_delete;
        ExecuteQuery($str_delete);
        ///---------------------------------------------------------------------		
    }
    ///---------------------------------------------------------------------
    ///write to xml file
    
}

if($int_mdlpkid != 0)
{
    //Comment_Synchronize($int_modelpkid);
    //Comment_WriteXml($int_modelpkid);
}


///---------------------------------------------------------------------
	CloseConnection();
	Redirect("blog_list.php?type=S&msg=DM&mdlid=".$int_mdlpkid."&mname=".$str_modelname."&mode=".urlencode(RemoveQuote($str_visible_title))."&tit=".urlencode(RemoveQuote($str_title))."&".$str_filter ); //}
		
	exit();
?>	