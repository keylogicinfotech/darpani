/*
	Module Name:- modgirlfriend
	File Name  :- gf_photo_edit.js
	Create Date:- 16-FEB-2006
	Intially Create By :- 0022
	Update History:
*/

function frm_edit_validate()
{
	with(document.frm_edit)
	{
                if(cbo_type.value == 0)
		{
			alert("Please select tailoring option.");
			cbo_type.focus();			
			return false;
		}
                /* if(cbo_color.value == "")
		{
			alert("Please select color.");
			cbo_color.focus();			
			return false;
		}*/
                if(txt_price.value == "")
		{
			alert("Please enter price.");
			txt_price.focus();			
			return false;
		}
	}
	return true;
}
//        }
//function frm_prg_photo_edit_validateform()
//{
//	with(document.frm_prg_photo_edit)
//	{
//  		if(trim(fileimage.value) != "")
//		{
//			if(checkExt(trim(fileimage.value))==false)
//			{
//				fileimage.select();
//				fileimage.focus();
//				return false;
//			}
//		}
//		
//		if(trim(txt_purl.value)!="")
//		{
//			if(isValidUrl(trim(txt_purl.value))==false)
//			{
//				txt_purl.focus();	
//				txt_purl.select();	
//				return false;
//			}
//		}
//               
//	}
//	return true;
//}

function show_details(url)
{
	window.open(url,'PromoGallery','left=50,top=20,scrollbars=yes,width=570,height=520,resizable=yes');
	return false;
}
