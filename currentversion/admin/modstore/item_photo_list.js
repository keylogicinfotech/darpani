function frm_add_validate()
{
    with(document.frm_add)
    {
        if(isEmpty(fileimage.value) && txt_imgurl.value == "")
        {
                alert("Please select image OR image url .");
                fileimage.select();
                fileimage.focus();
                return false;
        }
        if(trim(fileimage.value) != "")
        {
            if(checkExt(trim(fileimage.value))==false)
            {
                fileimage.select();
                fileimage.focus();
                return false;
            }
        }
        if(trim(txt_imgurl.value)!="")
        {
            if(isValidUrl(trim(txt_imgurl.value))==false)
            {
                txt_imgurl.focus();	
                txt_imgurl.select();	
                return false;
            }
        }
        if(cbo_visible.value == "")
        {
            alert("Please set value for visible.");
            cbo_visible.focus();			
            return false;
        }
        /* if(cbo_color.value == "")
        {
                alert("Please select color.");
                cbo_color.focus();			
                return false;
        }
        if(txt_ptitle.value == "")
        {
                alert("Please enter photograph title.");
                txt_ptitle.focus();			
                return false;
        }*/
    }
    return true;
}

function frm_list_check_displayorder()
{
    with(document.frm_list)
    {
        cnt=hdn_counter.value;
        for(i=1;i<cnt;i++)
        {
            if(trim(eval("txt_displayorder" + i).value) =="")
            {
                alert("Please enter display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }

            if(eval("txt_displayorder" + i).value <0)
            {
                alert("Please enter positive integer for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }

            if(isNaN(eval("txt_displayorder" + i).value))
            {
                alert("Please enter numeric value for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }
        }
    }
    return true;
}

function confirm_delete()
{
    if(confirm("Are you sure you want to delete this details?"))
    {
        if(confirm("Confirm Deletion:Click 'Ok' to delete this details or 'Cancel' to deletion."))
        {
            return true;
        }
    }
    return false;
}
