
function frm_edit_validate()
{
    with (document.frm_edit)
    {
        if(isEmpty(trim(txt_title.value)))
	{
            alert("Please enter title.");
            txt_title.select();
            txt_title.focus();
            return false;
	}
        if(isEmpty(trim(txt_cmykcode.value)))
	{
            alert("Please enter hex color code.");
            txt_cmykcode.select();
            txt_cmykcode.focus();
            return false;
	}
        /*if(isEmpty(trim(txt_rgbcode.value)))
	{
            alert("Please enter rgb code.");
            txt_rgbcode.select();
            txt_rgbcode.focus();
            return false;
	}*/
    }
    return true;
}