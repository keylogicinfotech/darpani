<?php
/*
	Module Name:- Modstore
	File Name  :- item_download_p.php
	Create Date:- 31-DEC-2018
	Intially Create By :- 0014
	Update History:
*/

#----------------------------------------------------------------------------------------------------
#Include Files
	include "../includes/configuration.php";
	include "item_config.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
	include "../includes/lib_file_upload.php";
#----------------------------------------------------------------------------------------------------
# Select Query to display list
$int_mid="";
if(isset($_GET["mid"]))
{
	$int_mid=trim($_GET["mid"]);
}

$int_pkid="";

if(isset($_GET["pkid"]))
{
	$int_pkid=trim($_GET["pkid"]);
}
if($int_pkid=="" || $int_pkid<=0 || !is_numeric($int_pkid))
{
	CloseConnection();
	Redirect("item_list.php?msg=F&type=E&mid=".$int_mid);
	exit;
}


	$str_query="";
	$str_query="select filename from t_store where pkid=".$int_pkid;
	$rs_download_file=GetRecordset($str_query);
	if($rs_download_file->EOF()==true)
	{		
		CloseConnection();
		Redirect("item_list.php?msg=F&type=E&mid=".$int_mid);
		exit();
	}
	
	/*$str_ext="";
	$str_filename=trim($rs_download_file->fields("photosetfilename"));	
	$str_filepath=trim($UPLOAD_FILE_PATH.$str_filename);
//	$str_ext=getextension($str_filename);
	header("Content-Type:application/octet-stream");
	header("Content-Disposition:attachment;filename=".$str_filename);
	$fp=fopen($str_filepath,'rb');
	header("Content-Length:".filesize($str_filepath));
	fpassthru($fp);
	exit();*/
	
	$str_filename=trim($rs_download_file->fields("filename"));	
	$str_filepath=trim($UPLOAD_FILE_PATH.$str_filename);
	//$str_download_filename = "Video_".$int_videopkid."_".date("d_M_Y").".".GetExtension($str_filename);
	
	// -----------------
	// Don't timeout when downloading large files
	@ignore_user_abort();
	@set_time_limit(0);
    ob_end_clean();

    if (!is_file($str_filepath) or connection_status()!=0) return(FALSE);
    
	header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header("Expires: ".gmdate("D, d M Y H:i:s", mktime(date("H")+2, date("i"), date("s"), date("m"), date("d"), date("Y")))." GMT");
    header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
    header("Content-Type: application/octet-stream");
    header("Content-Length: ".(string)(filesize($str_filepath)));
	header("Content-Disposition: attachment; filename=".$str_filename);
    header("Content-Transfer-Encoding: binary\n");
	header("Accept-Ranges: bytes");
    
	if ($fp = fopen($str_filepath, 'rb')) 
	{
        while(!feof($fp)) 
		{
            print(fread($fp, 1024*8));
			//echo(@fgets($fp, 8192));
        }
        fclose($fp);
		exit();
    }
	
	CloseConnection();	
	exit();

?>
