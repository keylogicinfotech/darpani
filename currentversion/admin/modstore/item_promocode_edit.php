<?php
/*
Module Name:- modstore
File Name  :- item_promocode_edit.php
Create Date:- 21-JAN-2019
Intially Create By :- 015
Update History:-
*/
#--------------------------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#--------------------------------------------------------------------------------------------------
#getting query string variables	
$int_pkid="";

if(isset($_GET["pkid"]))
{
    $int_pkid=trim($_GET["pkid"]);
}
//print $int_pkid;exit;
if($int_pkid == "" || !is_numeric($int_pkid) || $int_pkid == 0)
{
    CloseConnection();
    Redirect("item_promocode_list.php?msg=F&type=E");
    exit();
}
#--------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_PROMOCODE." WHERE pkid=" . $int_pkid;
$rs_list = GetRecordset($str_query_select);
if ($rs_list->count()==0)
{
    CloseConnection();
    Redirect("item_promocode_list.php?msg=F&type=E&#ptop");
    exit();
}
#-------------------------------------------------------------------------------------------------
# Initialization of variables used for message display.   
$str_type = "";
$str_message = "";

if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break; 
        case("E"): $str_type = "E"; break; 
        case("W"): $str_type = "W"; break;
    }
}

$str_tit="";

if(isset($_GET['tit']))
{ $str_tit=trim($_GET['tit']); }

if(isset($_GET["msg"]))
{
    switch (trim($_GET["msg"]))
    {
        case ("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("DU"): $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
        #Local
        case("DPA"): $str_message = $STR_MSG_ACTION_DISCOUNT_POSITIVE; break;
        case("PNV"): $str_message = $STR_MSG_ACTION_DISCOUNT_BLANK; break;
        
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?>  : <?php print($STR_TITLE_PAGE_PROMOCODE);?> : <?php print($STR_TITLE_EDIT); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
    	<div class="col-md-3 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="fa fa-chevron-left "></i>&nbsp;Back</a><?php */?>
                <a href="./item_promocode_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE_PROMOCODE); ?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_PROMOCODE);?></a>
            </div>
        </div>
        <div class="col-md-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE_PROMOCODE);?></h3></div>
    </div><hr> 
<?php if($str_type != "" && $str_message != "")	{ print(DisplayMessage(0,$str_message,$str_type)); }	?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <b><i class="fa  fa-edit "></i>&nbsp;<?php print($STR_TITLE_EDIT); ?></b></a>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse">
                        <div class="panel-body">
                            <form name="frm_edit" action="item_promocode_edit_p.php" method="post" onSubmit="return frm_edit_validate();">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="form-group">
                                        <label>Title</label><span class="text-help-form"> *</span>
                                        <input name="txt_title" type="text" class="form-control input-sm" id="txt_title" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" value="<?php print(RemoveQuote($rs_list->fields("title"))) ?>">
                                    </div>
                                <div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description</label><span class="text-help-form"> </span>
                                            <textarea name="ta_desc" id="ta_desc" rows="20" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>" ><?php print(RemoveQuote($rs_list->fields("description"))); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                    <div class="row padding-10">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Discount Percentage</label><span class="text-help-form"> *</span><?php /*?><span class="text-help"> (<?php print($STR_PASSWORD)?>)</span><?php */?>
                                                <input type="text" name="txt_percent" id="txt_percent" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PERCENTAGE; ?>" value="<?php if($rs_list->fields("discountpercentage") > 0) { print(RemoveQuote($rs_list->fields("discountpercentage"))); } ?>">
                                            </div>
                                        </div>
                                    <div class="col-md-2" align="center"><h4 class="or"><b>OR</b></h4></div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Discount Amount</label><span class="text-help-form"> *</span><?php /*?><span class="text-help"> (<?php print($STR_PASSWORD)?>)</span><?php */?>
                                            
                                            <input type="text" name="txt_amount" id="txt_amount" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE; ?>" value="<?php if($rs_list->fields("discountamount") > 0) { print(RemoveQuote($rs_list->fields("discountamount"))); } ?>">
                                        </div>
                                    </div>                                                                        
                                </div>
                                <div class="form-group">
                                    <label>Minimum Purchase Required To Use Promo Code</label><span class="text-help-form"> </span><?php /*?><span class="text-help"> (<?php print($STR_PASSWORD)?>)</span><?php */?>
                                    <input type="text" name="txt_min_purchase" id="txt_min_purchase" maxlength="100" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT; ?>" value="<?php print(RemoveQuote($rs_list->fields("minimumpurchaserequired"))) ?>">
                                </div>
				<?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                <input type="hidden" name="hdn_pkid" value="<?php print($int_pkid)?>">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a name="help"></a>
    <?php include "../../includes/help_for_edit.php"; ?>
</div>
<?php include "../../includes/include_files_admin.php"; ?>
<script language="JavaScript" src="../../js/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="./item_promocode_edit.js" type="text/javascript"></script>
</body>
</html>
