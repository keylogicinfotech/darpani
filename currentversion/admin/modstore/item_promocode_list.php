<?php
/*
Module Name:- modstore
File Name  :- item_promocode_list.php
Create Date:- 21-JAN-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
# Select Query to display list
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_PROMOCODE.$STR_DB_TABLE_NAME_ORDER_BY_PROMOCODE." ";
$rs_list = GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------------
# get Query String Data
$str_title = "";
$str_mode = "";
$str_desc = "";
$str_visible = "YES";
$int_min_purchase = 0.00;

if (isset($_GET['minpur'])) { $int_min_purchase = trim($_GET['minpur']); }
if (isset($_GET["title"])) { $str_title = trim($_GET["title"]); }
if (isset($_GET["mode"])) { $str_mode = trim($_GET["mode"]); }
if (isset($_GET["visible"])) { $str_visible = trim($_GET["visible"]); }
if (isset($_GET["desc"])) { $str_desc = trim($_GET["desc"]); }
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
$str_title = "";
$str_mode = "";
if(isset($_GET['tit'])) { $str_title = trim($_GET['tit']); }
if(isset($_GET['mode'])) { $str_mode = trim($_GET['mode']); }

#Get message type.
if(isset($_GET['type'])) 
{ 
    switch(trim($_GET['type'])) 
    { 
        case("S"): $str_type = "S"; break; 
        case("E"): $str_type = "E"; break; 
        case("W"): $str_type = "W"; break; 
    }
}
#Get message text.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("F") : $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S") : $str_message = $STR_MSG_ACTION_ADD; break;
        case("D") : $str_message = $STR_MSG_ACTION_DELETE; break;
        case("DU") : $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
        case("U") : $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O") : $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("IE") : $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("V") : $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
        #Local
        case("N") : $str_message = $STR_MSG_ACTION_DISCOUNT_POSITIVE; break;
        case("E") : $str_message = $STR_MSG_ACTION_DISCOUNT_BLANK; break;
    }
}	
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE_PROMOCODE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-6 col-sm-12 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
		<a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
            </div>
	</div>
	<div class="col-md-6 col-sm-12 col-xs-12" align="right" ><h3> <?php print($STR_TITLE_PAGE_PROMOCODE); ?> </h3></div>
    </div><hr> 
    <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
    <div class="row padding-10">
    	<div class="col-md-12">
            <div class="panel-group" id="accordion">
            	<div class="panel panel-default">
                    <div class="panel-heading">
                    	<h4 class="panel-title">
                            <span>
                            <a class="accordion-toggle collapsed link" data-toggle="collapse" title="<?php print($STR_TITLE_ADD); ?>" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>
                            </span>
                            <?php print($STR_LINK_HELP); ?>                                                          </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
			<div class="panel-body">
                            <form name="frm_add"  action="item_promocode_add_p.php" method="post" onSubmit="return frm_add_validate();" enctype="multipart/form-data">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
				<div class="form-group">
                                    <label>Title</label><span class="text-help-form"> *</span><?php /*?><span class="text-help"> (<?php print($STR_PASSWORD)?>)</span><?php */?>
                                    <input type="text" name="txt_name" id="txt_name" maxlength="100" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>">
				</div>
                                <div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description</label><span class="text-help-form"> </span>
                                            <textarea name="ta_desc" id="ta_desc" rows="20" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>" ><?php print(RemoveQuote($str_desc)); ?></textarea>
                                        </div>
                                    </div>
                                </div>
				<div class="row padding-10">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Discount Percentage</label><span class="text-help-form"> *</span><?php /*?><span class="text-help"> (<?php print($STR_PASSWORD)?>)</span><?php */?>
                                            <input type="text" name="txt_percent" id="txt_percent" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PERCENTAGE; ?>">
					</div>
                                    </div>
                                    <div class="col-md-2 align-middle" align="center"><h4 class=""><b>OR</b></h4></div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Discount Amount</label><span class="text-help-form"> *</span><?php /*?><span class="text-help"> (<?php print($STR_PASSWORD)?>)</span><?php */?>
                                            <input type="text" name="txt_amount" id="txt_amount" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT_PRICE; ?>">
					</div>
                                    </div>
				</div>
                                <div class="form-group">
                                    <label>Minimum Purchase Required To Use Promo Code</label><span class="text-help-form"> </span><?php /*?><span class="text-help"> (<?php print($STR_PASSWORD)?>)</span><?php */?>
                                    <input type="text" name="txt_min_purchase" id="txt_min_purchase" maxlength="100" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_INT; ?>" value="<?php  print($int_min_purchase);?>">
                                </div>
				<?php print DisplayFormButton("ADD", 0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                        </div>
                    </div>
		</div>
            </div>
	</div>
    </div>
    <div class="table-responsive">
	<form name="frm_list" action="item_promocode_order_p.php" method="post" onSubmit="return frm_list_check_displayorder();">
            <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th width="4%">Sr. #</th>
                    <?php /* ?><th width="12%">Created By</th><?php */ ?>
                    <th width="">Details</th>
                    <th width="6%">Min. Purchase value</th>
                    <th width="4%">Discount</th>
                    <th width="6%">Display Order</th>
                    <th width="9%">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php if($rs_list->EOF()==true)  {  ?><tr><td colspan="7" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr><?php }
		else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
		<tr>
                    <td align="center"><?php print($int_cnt); ?><input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>"></td>
                    <?php /* ?><td align="center" class="text-primary">
                        <?php
                        if($rs_list->fields("modelpkid") == 0)
                        {
                            print "Admin";
                        }
                        else
                        {
                            $sel_mdl_details = "";
                            $sel_mdl_details = "SELECT shortenurlkey, shortenurlvalue FROM t_model WHERE modelpkid = ".$rs_list->Fields("modelpkid");
                            $rs_mdl_details = GetRecordSet($sel_mdl_details);
                            if($rs_mdl_details->Count() > 0)
                            {
                                print(DisplayWebSiteURL($rs_mdl_details->fields('shortenurlvalue'),$rs_mdl_details->fields("shortenurlkey"),"_blank",$STR_HREF_TITLE_PROFILE,""));
                            }
                        }
                        ?>
                        
                        
                    </td><?php */ ?>
                    <td class="align-middle"><h4 class="nopadding"><b><?php print $rs_list->fields("title");?></b></h4>
                        <p><?php print $rs_list->fields("description");?></p></td>
                    <td align="right" class=""><?php print $rs_list->fields("minimumpurchaserequired");?></td>
                    <td align="center">
                        <?php 
                        if($rs_list->fields("discountpercentage") != "" && $rs_list->fields("discountpercentage") != 0)
                            { print(" ".MyHtmlEncode($rs_list->fields("discountpercentage"))."&nbsp;%"); } ?>
                        <?php 
                        if($rs_list->fields("discountamount") != "" && $rs_list->fields("discountamount") != 0)
                        { print("$&nbsp;".MyHtmlEncode($rs_list->fields("discountamount")).""); }	
                        ?>				
                    </td>
                    <td align="center">
                        <input type="text" name="txt_displayorder<?php print($int_cnt);?>" maxlength="5" value="<?php print($rs_list->fields("displayorder"));?>" class="form-control input-sm text-center">
                    </td>
                    <?php $str_image="";
                        if(strtoupper($rs_list->fields("visible")) == "YES")
                        {   
                            $str_image = $STR_LINK_ICON_PATH_VISIBLE;
                            $str_class = "btn btn-warning btn-xs";
                            $str_title = $STR_HOVER_VISIBLE;
                        }
                        else
                        {   
                            $str_image = $STR_LINK_ICON_PATH_INVISIBLE;
                            $str_class = "btn btn-default active btn-xs";
                            $str_title = $STR_HOVER_INVISIBLE;
                        } ?>
                    <td align="center">
                        <a class="<?php print($str_class); ?>" href="item_promocode_visible_p.php?pkid=<?php print($rs_list->fields("pkid"));?>" title="<?php print($str_title); ?>"><?php print($str_image);?></a>
			<a class="btn btn-success btn-xs" href="item_promocode_edit.php?pkid=<?php print($rs_list->fields("pkid"));?>" title="<?php print($STR_HOVER_EDIT);?>"><?php print $STR_LINK_ICON_PATH_EDIT; ?></a>
			<a class="btn btn-danger btn-xs" href="item_promocode_del_p.php?pkid=<?php print($rs_list->fields("pkid"));?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE);?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></i></a>
                    </td>
		</tr>
		<?php $int_cnt++; $rs_list->MoveNext(); } ?>
		<tr>
                    <td></td><?php /* ?><td></td><?php */ ?><td></td><td></td><td></td>
                    <td align="center"><input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>"><?php print DisplayFormButton("SAVE",0); ?></td>
                    <td></td>
		</tr>
		<?php }  ?>
                </tbody>
            </table>
	</form>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
<?php include "../../includes/include_files_admin.php"; ?>
<script language="JavaScript" src="./item_promocode_list.js" type="text/javascript"></script>
</body></html>
