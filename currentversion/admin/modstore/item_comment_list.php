<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------
#Include files
//session_start();
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------

//select query to display list and get details from tr_mdl_turnonoff   table
# SELECT Filter Criteria
if(isset($_GET["flg"]) && trim($_GET["flg"])!="" )
{ $str_redirect_page_flag=trim($_GET["flg"]); }

$int_cat_pkid=0;
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ $int_cat_pkid=trim($_GET["catid"]); }

$str_mname="";
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_mname=trim($_GET["key"]); }

# get data for paging
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]) && trim($_GET["PagePosition"])>0)
{ $int_page = $_GET["PagePosition"]; }
else
{ $int_page=1; }
//print_r($_GET); exit;
// $str_mname = "";

$str_filter=""; 
$str_filter="&catid=".$int_cat_pkid."&key=".$str_mname."&PagePosition=".$int_page;

$int_masterpkid = 0;
if(isset($_GET["masterpkid"]))
{ $int_masterpkid=trim($_GET["masterpkid"]); }

$str_mastername="";
if(isset($_GET["mname"])) { $str_mastername=trim($_GET["mname"]); }

$int_mempkid = 0;
if(isset($_GET["memid"]))
{ $int_mempkid=trim($_GET["memid"]); }

$str_membername="";
if(isset($_GET["memname"])) { $str_membername=trim($_GET["memname"]); }

$str_approved_blog="";
if(isset($_GET["cbo_approve"])) 
{ $str_approved_blog = trim($_GET["cbo_approve"]); }


$str_where_approve = "";
if($str_approved_blog == "YES") 
{
    $str_where_approve = "AND approved = 'YES'";
}
elseif($str_approved_blog == "NO")  {
    $str_where_approve = "AND approved = 'NO'";
}
$int_pkid = 0;
if(isset($_GET["pkid"])) 
{ $int_pkid=trim($_GET["pkid"]); }
//print $int_pkid;exit;
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_BLOG. " WHERE userpkid=".$int_masterpkid.$str_where_approve." ORDER BY submitdatetime DESC, displayorder DESC";
//print $str_query_select; //exit;
$rs_list = GetRecordSet($str_query_select);

//print $int_mdlpkid; exit;

/*if($int_pkid <=0 || !is_numeric($int_pkid) || $int_pkid == "")
{ CloseConnection(); Redirect("item_comment_list.php?pkid=".$rs_list->fields("pkid")." &masterpkid=".$int_masterpkid.$str_filter."&type=E&msg=F"); exit(); } */

$str_created_by = "";
$str_created_by_url = "";

//print_r($_GET);

$sel_blog_created_by = "";
$sel_blog_created_by = "SELECT * FROM " .$STR_DB_TABLE_NAME_BLOG. " WHERE masterpkid=".$int_pkid." ".$str_where_approve." AND blogtype='B'"; 
//print $sel_blog_created_by ;exit;

//print $sel_blog_created_by;
$rs_created_by = GetRecordSet($sel_blog_created_by);
    
$str_where = "";
$str_where = "";
//print "<Br/><br/>MODEL: ".$int_mdlpkid."<br/>";
//print "MEMBER: ".$int_mempkid."<br/>";
if($rs_created_by->eof() != true)
{
    if($int_mdlpkid != 0)
    {
        $sel_model_details = "";
        $sel_model_details = "SELECT shortenurlkey, shortenurlvalue FROM t_user WHERE pkid = ".$int_mdlpkid;
        $rs_list_model_details = GetRecordSet($sel_model_details);
        $str_created_by = $rs_list_model_details->Fields("shortenurlkey");
        $str_created_by_url = $rs_list_model_details->Fields("shortenurlvalue");
        //$str_where = " AND userpkid != ".$int_mdlpkid;
       
    }
    else if($int_mempkid != 0)
    {
        $sel_model_details = "";
        $sel_model_details = "SELECT firstname, lastname, shortenurlkey, loginid FROM t_freemember WHERE pkid = ".$int_mempkid;
        $rs_list_model_details = GetRecordSet($sel_model_details);
        $str_created_by = $rs_list_model_details->Fields("shortenurlkey");
        $str_created_by_url = "";//$rs_list_model_details->Fields("shortenurlvalue");
        //$str_where = " AND memberpkid != ".$int_mempkid;
    }
}
//print $str_where;





/*if(!$rs_list->EOF())
{
	$str_mastername=$rs_list->fields("modelname");
	$int_mdlpkid=$rs_list->fields("userpkid"); // $int_mdlpkid stands for modelmdlpkid
}
else
{
	$str_mastername="";
	$int_mdlpkid=0; // $int_mdlpkid stands for modelmdlpkid
}*/
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
    $str_type="";
    $str_message="";
    $str_title="";
    $str_mode="";
    if(isset($_GET['tit']))
    {
        $str_title=trim($_GET['tit']);
    }
    if(isset($_GET['mode']))
    {
        $str_mode=trim($_GET['mode']);
    }
#Get message type.
    if(isset($_GET['type']))
    {
        switch(trim($_GET['type']))
        {
            case("S"): $str_type = "S"; break;
            case("E"): $str_type = "E"; break;
            case("W"): $str_type = "W"; break;
        }
    }
#Get message text.
    if(isset($_GET['msg']))
    {
        switch(trim($_GET['msg']))
        {
            case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
            case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
            case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
            case("DM"): $str_message = $STR_MSG_ACTION_DELETE; break;
            case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
            case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
            case("IE"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
            case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
            case("URL"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;
            case("SLVI"): $str_message = $STR_MSG_ACTION_WEBSITE_LOGO_MODE_UPDATED; break;
            case("DANC"): $str_message = $STR_MSG_DISPLAY_NEW_MODE_UPDATED; break;
            case("DAFC"): $str_message = $STR_MSG_DISPLAY_FEATURED_MODE_UPDATED; break;
            case("DAHC"): $str_message = $STR_MSG_DISPLAY_HOT_MODE_UPDATED; break;
            case("PBMC"): $str_message = $STR_MSG_ACTION_PAY_BUTTON_MODE_UPDATED; break;
        }
    }	
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_USER_APPROVED_BLOGS_COMMENTS);?><?php if($str_mastername != "") { print " [".$str_mastername."]"; } ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php /* ?><link href="../includes/bootstrap.min.css" rel="stylesheet">
<link href="../includes/bootstrap-theme.min.css" rel="stylesheet">
<link href="../includes/font-awesome.min.css" rel="stylesheet">
<link href="../includes/admin.css" rel="stylesheet">  <?php */ ?>
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
	<?php include("../../includes/adminheader.php"); ?>
	<div class="row">
		<div class="col-md-6 col-sm-12 col-xs-12 button_space">
                    <div class="btn-group" role="group" aria-label="...">
                        <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;Back</a>
                        <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_HREF_TITLE_LINK);?> <?php print($STR_TITLE_MODEL); ?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_MODEL); ?></a><?php */?>
                        <a href="./item_list.php?masterpkid=<?php print $int_masterpkid; ?><?php print($str_filter); ?>" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_USER_APPROVED_BLOGS); ?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_USER_APPROVED_BLOGS); ?><?php if($str_mastername != "") { print " [".$str_mastername."]"; }  ?></a>
                    </div>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12" align="right" ><h3><?php print($STR_TITLE_USER_APPROVED_BLOGS_COMMENTS);?><?php if($str_mastername != "") { print " [".$str_mastername."]"; } ?></h3></div>
	</div><hr> 
	<?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
	<div class="row">
    	<div class="col-lg-12">
        	<div class="panel-group" id="accordion"> 
            	<div class="panel panel-default">
                	<div class="panel-heading">
                    	<h4 class="panel-title">
                            <div class="row">
                                <div class="col-md-12  padding-10"> 
                                    <?php /* ?><div class="col-md-6 col-xs-8"><a class="accordion-toggle collapsed link" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><i class="glyphicon  glyphicon-plus " ></i>&nbsp;&nbsp;<?php print($STR_TITLE_FORM_ADD); ?> </a></div><?php */ ?>
                                    <div class="col-md-12 col-xs-12" align="right"><?php print($STR_LINK_HELP); ?></div>
                                </div>
                            </div>
                        </h4>
                    </div>
                    
                </div>
            </div>
        </div>
	</div>
        <div class="row">
            <div class="col-md-12" align="left">
                <div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<div class="row"><div class="col-md-12  padding-10"><div class="col-md-12 col-sm-12 col-xs-12"><strong><i class="fa fa-filter"></i>&nbsp;&nbsp;Filter Criteria</strong></div></div></div>
			</h4>
		</div>
		<div class="panel-body">
			<div class="col-md-12" align="center">
				
				<form name="frm_filter" method="get" action="item_comment_list.php">
				<?php /* ?><label>Display </label><?php */ ?> 
                                <label>Select&nbsp;&nbsp;</label>   
                                    <label>
                                        <select name="cbo_approve" class="form-control input-sm">
                                            <option value=""> -- ALL -- </option>
                                            <option <?php print(CheckSelected("YES",$str_approved_blog)); ?> value="YES">APPROVED</option>
                                            <option <?php print(CheckSelected("NO",$str_approved_blog)); ?> value="NO">NOT APPROVED</option>                                            </select>
                                        <input type="hidden" id="pkid" name="pkid" value="<?php print $int_pkid;?>" />
                                        <input type="hidden" id="masterpkid" name="masterpkid" value="<?php print $int_masterpkid;?>" />
                                        <!--<input type="hidden" id="mname" name="mname" value="<?php // print $str_mastername;?>" />-->
                                        
                                </label>
                                <label>&nbsp;&nbsp;Comments&nbsp;&nbsp;</label>
                                <label>&nbsp;<button type="submit" name="btn_submit" class="btn btn-warning btn-sm" title="Click to view list"><strong>View</strong></button></label>
				</form>
			</div>
                    
		</div>
	</div>
            </div>
        </div>
        <?php if($str_created_by != "") { ?>
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-primary">Created By: <a href="<?php print $str_created_by_url; ?>"><?php print $str_created_by; ?></a></h3>
            </div>
        </div>
        <?php } ?>
	<div class="table-responsive">
	<form name="frm_list" method="POST" action="item_del_multiple_p.php" onSubmit="return frm_check_delete_multiple();">
		<table class="table table-striped table-bordered">
        	<thead>
                <tr>
                    <th width="4%">Sr. #</th>
                    <th width="10%">Date</th>
                    <?php /* ?><th width="10%">Model</th><?php */ ?>
                    <th width="18%">Commented By</th>
                    <th width="">Blog Details</th>
                    <th width="4%">Approved</th>
                    <th width="10%">Action</th>
                    
		</tr>
		</thead>
                    <tbody>
			<?php if($rs_list->EOF()==true)  {  ?><tr><td colspan="7" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr><?php }
                        else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
                        <tr>
                           <?php // print "userpkid:" .$rs_list->Fields("userpkid");exit; ?>
                            <td align="center"><?php print($int_cnt); ?><input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->Fields("userpkid"));?>"></td>
                            <td align="center" class="text-primary"><i><?php print(date("d-M-Y h:i:s", strtotime($rs_list->Fields("submitdatetime")))); ?></i></td>
                            <?php /* ?><td  align="center">
                                <?php 
                                $str_query_select="SELECT photofilename,photofilenamelarge FROM t_model WHERE userpkid=".$rs_list->fields("userpkid");
                                $rs_list_model=GetRecordSet($str_query_select);
                                if(!$rs_list_model->EOF())
                                {
                                ?>
                                <a href="<?php print($UPLOAD_IMG_FILE_PATH.$rs_list->fields("userpkid")."/".$rs_list_model->fields("photofilenamelarge"));?>" rel="thumbnail"><img class="img-responsive" width="100" title="Click to view actual size image" src="<?php print($UPLOAD_IMG_FILE_PATH.$rs_list->fields("userpkid")."/".$rs_list_model->fields("photofilename"));?>" border="0" alt="Model Image" align="top"></a><p><a href="<?php print($UPLOAD_IMG_FILE_PATH.$rs_list->fields("userpkid")."/".$rs_list_model->fields("photofilenamelarge"));?>" rel="thumbnail" class=""><?php print $rs_list->fields("modelname"); ?></a><p/>
				<?php } ?>
                            </td><?php */ ?>
                            <td align="center">
                                <?php
//                                print "userpkid:" .$rs_list->Fields("userpkid");exit;
                                if($rs_list->Fields("userpkid") != 0)
                                {
                                    $str_query_select="SELECT shortenurlvalue, shortenurlkey FROM t_user WHERE pkid=".$rs_list->Fields("userpkid");
                                    //print $str_query_select;exit;
                                    $rs_list_model=GetRecordSet($str_query_select);
                                    $str_target = "";
                                    $str_target = "_blank";
                                    if(!$rs_list_model->EOF())
                                    { 
                                    ?>
                                <a title="<?php print $STR_TITLE_BRAG_PROFILE; ?>" href="<?php print($rs_list_model->fields('shortenurlkey')); ?><?php //print("".DisplayWebSiteURL($rs_list_model->fields('shortenurlvalue'),StuffCharacter($rs_list_model->fields('shortenurlvalue'),40," "),$str_target,"Click to view this profile","").""); ?> "><?php print($rs_list_model->fields('shortenurlkey')); ?></a> <b class="text-help">(User)</b>
                                <?php } 
                                ?>
                                    
                                <?php
                                }?>
                            </td>
                            <td style="vertical-align:">
                                
                                <?php if($rs_list->fields("largeimagefilename") != "" ) { ?>
                                <a href="#" data-toggle="modal" data-target=".comment-<?php print($rs_list->fields("pkid")); ?>" rel="thumbnail"><img width="100%<?php //print($INT_COMMENT_PHOTO_THUMB_WIDTH);?>" src="<?php print($UPLOAD_BLOG_IMG_FILE_PATH.$rs_list->fields("largeimagefilename"));?>" border="0" alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" align="top"></a>
                                <div class="modal fade comment-<?php print($rs_list->fields("pkid")); ?> " align="center" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                <img width="100%<?php //print($INT_COMMENT_PHOTO_THUMB_WIDTH);?>" src="<?php print($UPLOAD_BLOG_IMG_FILE_PATH.$rs_list->fields("largeimagefilename"));?>" border="0" alt="<?php print $rs_list->Fields("title"); ?>" title="<?php print $rs_list->Fields("title"); ?>" align="top">
                                            </div>
                                        </div>
                                    </div>
                                </div>
				<?php  }
                                else { //print(MakeBlankTab(150,100,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); 
                                    
                                } ?>
                                <p><h4 class="nopadding text-primary"><b><?php print $rs_list->Fields("title"); ?><?php /*if($rs_list->Fields("displayasnew") == "YES") { ?>&nbsp;<span class="NewText">NEW</span><?php } */ ?></b></h4></p>
                                <?php if($rs_list->Fields("description") != "") {?>
                                <p align="justify"  class="">
                                    <?php print $rs_list->Fields("description"); ?>
                                </p>
                                <?php } ?>
                                <?php   if($rs_list->fields("url") != "")
                                        {
                                            if($rs_list->fields("url") != "") 
                                            { $str_img_alt = $rs_list->fields("urltitle"); }
                                            else
                                            { $str_img_alt = $rs_list->fields("url"); } 
                                        } ?>
                                <?php if($rs_list->fields("openinnewwindow") == 'YES') { $str_new_window="<b>NEW</b> window"; }
                                                else { $str_new_window="<b>CURRENT</b> window"; } ?>
                                                <?php //print $rs_list->fields("url")."----" .$rs_list->fields("openurlinnewwindow"); ?>
                                
                                <?php if($rs_list->fields("url") != "") { ?>
                                    
                                    <p><br/><i class="fa fa-globe"></i>&nbsp;<?php print(DisplayWebsiteURL($rs_list->fields("url"),$rs_list->fields("urltitle"),$rs_list->fields("openinnewwindow"), $str_img_alt, 'link', '', ''));?><span class="text-help"> [ will open in <?php print $str_new_window; ?> ]</span></p><?php } ?>
                                
                                <?php if($rs_list->Fields("location") != "") { ?>
                                    <span><i class="fa fa-map-marker"></i>&nbsp;<?php print $rs_list->Fields("location"); ?></span>
                                <?php } ?>
                                <?php if($rs_list->Fields("ipaddress") != "") { ?>
                                    <span><i class="fa fa-desktop"></i>&nbsp;<?php print $rs_list->Fields("ipaddress"); ?></span>
                                <?php } ?>
                                
                                <?php /*?>
                                 <p><h4 class=" nopadding">
                                  
                                <?php
                                    if(!$rs_list->EOF()) 
                                    { 
                                        $rating_value=$rs_list->fields("ratingvalue"); 
                                        //$rating_value=1.5;
                                    }
                                    else 
                                    { $rating_value=0; }

                                    for($i=1;$i<=5;$i++)
                                    {
                                        if($rating_value>=1) { ?><i class="fa fa-star"></i>&nbsp;<?php $rating_value=$rating_value-1; }
                                        else if($rating_value>=0.5) { ?><i class="fa fa-star-half-o"></i>&nbsp;<?php $rating_value=$rating_value-1; }
                                        else if ($rating_value<0.5 && $rating_value>0) { ?><i class="fa fa-star-o"></i>&nbsp;<?php $rating_value=$rating_value-1; }
                                        else if($rating_value<=0) { ?><i class="fa fa-star-o"></i>&nbsp;<?php }
                                    }
                                    ?>
                                </h4>
                                  </p>
                                 <?php */ ?>
                                
                                
                            </td>
                            <?php 
                                $str_class_approve="";
                                $str_click_title="";
                                if($rs_list->fields("approved")=="YES") {	
                                    $str_class_approve="alert-success";
                                    $str_click_title="Click to not approve"; }
                                else {
                                    $str_class_approve="alert-danger";
                                    $str_click_title="Click to approve"; }
                                ?>
                            <td class="<?php print $str_class_approve; ?>" align="center">
                                <a href="item_comment_approve_p.php?masterpkid=<?php print($int_masterpkid);?>&pkid=<?php print($rs_list->fields("pkid"));?><?php print($str_filter);?>" title="<?php print $str_click_title; ?>" class="alert-link"><?php print $rs_list->fields("approved"); ?></a>
                            </td>
                            <?php $str_image="";  $str_class="";  $str_title="";
                            if(strtoupper($rs_list->fields("visible"))=="YES") { $str_image="<i class='glyphicon glyphicon-eye-open'></i>";
                                $str_class="btn btn-warning btn-xs"; $str_title=$STR_HOVER_VISIBLE; }
                            else { $str_image="<i class='glyphicon glyphicon-eye-close'></i>";
                                $str_class="btn btn-default active btn-xs"; $str_title=$STR_HOVER_INVISIBLE; } ?>
                            <td align="center">
                                <a class="<?php print($str_class); ?>" href="item_comment_visible_p.php?masterpkid=<?php print $int_masterpkid; ?>&pkid=<?php print($rs_list->fields("pkid"));?><?php print($str_filter);?>" title="<?php print($str_title); ?>" tabindex="11"><?php print($str_image);?></a>
				<a class="btn btn-success btn-xs" href="item_comment_edit.php?masterpkid=<?php print($int_masterpkid)?>&pkid=<?php print($rs_list->fields("pkid"));?><?php print($str_filter);?>" title="<?php print($STR_HOVER_EDIT);?>" tabindex="12"><i class="glyphicon glyphicon-pencil"></i></a>
                                <a class="btn btn-danger btn-xs" href="item_comment_del_p.php?masterpkid=<?php print($int_masterpkid)?>&pkid=<?php print($rs_list->fields("pkid"));?><?php print($str_filter);?>" onClick="return frm_list_confirmdelete();" title="<?php print($STR_HOVER_DELETE);?>" tabindex="13"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                            
                            <input type="hidden" name="txt_displayorder<?php print($int_cnt);?>" size="5" maxlength="5" value="<?php print($rs_list->fields("displayorder"));?>" class="clsTextBox">
                            <input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>">
                                        
                        </tr>
		<?php $int_cnt++; $rs_list->MoveNext(); } ?>
                        <input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>" >
			<?php /* ?><input type="hidden" name="userpkid" value="<?php print($int_model_pkid);?>" ><?php */ ?>
			<input type="hidden" name="PagePosition" value="<?php print($int_page);?>" >
			<input type="hidden" name="catid" value="<?php print($int_cat_pkid);?>" >
			<input type="hidden" name="key" value="<?php print($str_mname);?>" >
			<input type="hidden" name="masterpkid" value="<?php print($int_mdlpkid);?>" >
			<input type="hidden" name="mname" value="<?php print($str_mastername);?>" >
                        <input type="hidden" name="memid" value="<?php print($int_mempkid);?>" >
			<input type="hidden" name="memname" value="<?php print($str_membername);?>" >
                <?php }  ?>
       	</tbody>
	</table>
	</form>
    </div> 
	<?php include "../../includes/help_for_list.php"; ?>
</div>
    <?php include "../../includes/include_files_admin.php"; ?>
<?php /* ?><script language="JavaScript" src="../includes/functions.js" type="text/javascript"></script>
<script src="../includes/jquery.min.js"></script>
<script src="../includes/bootstrap.min.js"></script>  <?php */ ?>
<script language="JavaScript" src="./item_comment_list.js" type="text/javascript"></script>
</body>
</html>
