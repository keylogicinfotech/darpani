<?php
/*
Module Name:- modstore
File Name  :- purchase_list.php
Create Date:- 02-MAR-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_datetimeyear.php";
include "./item_app_specific.php";
#------------------------------------------------------------------------------------------------
//$int_purchasepkid=1;
//if(isset($_GET['ppkid']))
//{ $int_purchasepkid=trim($_GET['ppkid']); }

$str_purchasedatetime="";
if(isset($_GET['purdt']))
{ $str_purchasedatetime=trim($_GET['purdt']); }

$int_userpkid=0;
if(isset($_GET['upkid']))
{ $int_userpkid=trim($_GET['upkid']); }

$int_countrypkid=0;
if(isset($_GET['cpkid']))
{ $int_countrypkid=trim($_GET['cpkid']); }

// If user will not be registered then userpkid will be 0
if($str_purchasedatetime=="" || /*$int_userpkid==0 ||*/ $int_countrypkid==0)
{ 
    echo  "<script type='text/javascript'>";
    echo "window.close();";
    echo "</script>";
}
    
$str_filtercriteria=" WHERE purchasedatetime='".$str_purchasedatetime."' AND userpkid=".$int_userpkid;

#------------------------------------------------------------------------------------------------
$str_query_select = "SELECT * FROM t_store_purchase ";
$str_query_select .=" ".$str_filtercriteria. ""; 
$str_query_select .= " ORDER BY purchasedatetime DESC";
//print $str_query_select; //exit;
$rs_list=GetRecordSet($str_query_select);

if($rs_list->EOF())
{ 
    echo  "<script type='text/javascript'>";
    echo "window.close();";
    echo "</script>";
}

$str_query_select = "SELECT count(*) AS totalrecords FROM t_store_purchase ";
$str_query_select .=" ".$str_filtercriteria. ""; 
$str_query_select .= " GROUP BY purchasedatetime ";
$rs_list_count = GetRecordSet($str_query_select); 
#------------------------------------------------------------------------------------------------
?><!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : Order Print</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body style="padding-top: 0px;"> 
<div class="container">
<?php /* ?><?php include($STR_ADMIN_HEADER_PATH); ?><?php */ ?>
    
    <div class="row padding-10">
        <div class="col-md-12" align="right">
            <button class="btn btn-primary hidden-print" onclick="myFunction()"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print</button>
        </div>
    </div>
    <br/>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <td width="50%" style="border: #000 0px;">
                                <img  src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/images/logo.png" width="150px" height="auto"  class="logo">
                            </td>
                            <td width="50%" align="right" style="border: #000 0px;">
                                <?php /* ?><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;<?php print($STR_COMPANY_MOBILE_NUMBER); ?><br/><?php */ ?>
                                <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;<?php print($STR_COMPANY_EMAIL_ADDRESS); ?><br/>
                                <i class="fa fa-globe" aria-hidden="true"></i>&nbsp;<?php print($STR_SITENAME_WITH_PROTOCOL); ?><br/>
                            </td>

                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    
    
    
    
    
    
    

   
    
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="2">INVOICE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="50%">
                                <?php print($STR_SITE_TITLE); ?><br/>
                                <?php print($STR_COMPANY_POSTAL_ADDRESS01); ?><br/>
                                <?php print($STR_COMPANY_POSTAL_ADDRESS02); ?><br/>
                                <?php print($STR_COMPANY_WORKING_DAYS_HOURS); ?><br/>
                            </td>
                            <td width="50%" align="right">
                                <p><b>Order Date : </b><?php print(date("d-M-Y h:i:s A",strtotime($rs_list->fields("purchasedatetime"))));?></p>
                                <p><b>Order ID : </b><?php print $rs_list->fields("subscriptionid"); ?></p>
                                <p><b>Payment Method : </b><?php print $rs_list->fields("paymentoptiontitle"); ?></p>
                                <?php if($rs_list->fields("bankname") != "") { ?>
                                    <p><b>Payment Details : </b><?php print $rs_list->fields("bankname")." | ".$rs_list->fields("paymentmode"); ?></p>
                                <?php } ?>
                            </td>

                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <?php /* ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Customer Address</th>
                            <th>Shipping Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="50%" >
                                <b><?php print $rs_list->fields("firstname"); ?></b>
                                <b><?php print $rs_list->fields("lastname"); ?></b><br/>
                                <?php print $rs_list->fields("address"); ?><br/>
                                <?php print $rs_list->fields("city"); ?> -
                                <?php print $rs_list->fields("zipcode"); ?>,
                                <?php print $rs_list->fields("state"); ?>,
                                <?php print $rs_list->fields("country"); ?><br/>
                                <?php print "Contact : ".$rs_list->fields("phoneno"); ?>
                            </td>
                            <td width="50%" >
                                Darpani<br/>VTM Market, Surat Textile Market,<br/>Ring Road, Surat, Gujarat, India, Pin-395003<br/>8980061160<br/>admin@darpani.com
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php */ ?>


    <div class="row padding-10">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Shipping Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="100%" >
                                <b><?php print $rs_list->fields("firstname"); ?></b>
                                <b><?php print $rs_list->fields("lastname"); ?></b><br/>
                                <?php print $rs_list->fields("address"); ?><br/>
                                <?php print $rs_list->fields("city"); ?> -
                                <?php print $rs_list->fields("zipcode"); ?>,
                                <?php print $rs_list->fields("state"); ?>,
                                <?php print $rs_list->fields("country"); ?><br/>
                                <?php print "Contact : ".$rs_list->fields("phoneno"); ?><br/>
                                <?php print "Email : ".$rs_list->fields("emailid"); ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="table-responsive">
                <form name="frm_list" action="./action_page.php" method="post" onSubmit="javascript: validate_submit_fomrm();"   >
                    <table class="table table-striped table-bordered ">
                        <thead>
                            <?php $str_query_select = "";
                                $str_query_select = "SELECT currency_symbol FROM ".$STR_DB_TABLE_NAME_COUNTRY." WHERE pkid=".$int_countrypkid; 
                                $rs_list_currency_symbol = GetRecordSet($str_query_select);
                            ?>
                            <tr>
                                <th width="4%">Sr. #</th>
                                <th width="">Details</th>
                                <th width="8%">Quantity<br><span class="text-help"></span></th>	
                                <th width="10%">Unit Price<br/><span class="text-help">(in <?php print $rs_list_currency_symbol->Fields("currency_symbol"); ?>)</span></th>
                                <th width="10%">Tailoring Charge<br/><span class="text-help">(in <?php print $rs_list_currency_symbol->Fields("currency_symbol"); ?>)</span></th>
                                <?php /* ?><th width="10%">Shipping Charge<br/><span class="text-help">(in <?php print $rs_list_currency_symbol->Fields("currency_symbol"); ?>)</span></th><?php */ ?>
                                <th width="10%">Total Price<br/><span class="text-help">(in <?php print $rs_list_currency_symbol->Fields("currency_symbol"); ?>)</span></th>	
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($rs_list->EOF()==true)  {  ?>
                                <tr><td colspan="7" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                            <?php }
                            else {
                                $int_total_amount=0;
                                $int_total_shipping_charge=0;
                                $int_total_promo_code=0;
                                $int_total_wholesaler_amount = 0;
                                $int_total_giftcard_amount = 0;
                                $int_total_refund_amount = 0;
                                $int_total_gross_amount = 0;
                                $int_cnt=1;
                                
                                while(!$rs_list->EOF()==true) { ?> 
                                <tr>
                                    <td align="center" class="text-align"><?php print($int_cnt);?></td>
                                    <td align="left" style="vertical-align:text-top">
                                        <?php 
                                        $int_shippingval = 0;
                                        $int_weight = 0;
                                        $int_shipping_total_value = 0;
                                        $int_prod_cnt=1;
                                        $int_actual_amount = 0;
                                        $int_wholesaler_discount_percentages = 0;
                                        $int_giftcard_amount = 0;
                                        $int_gross_amount = 0;
                                        ?>

                                        <?php 
                                        //print "<span class=''>".$int_prod_cnt.".</span>  ";
                                        //print "<b><span class='text-help'>Item Info:</span></b><br/>"; 
                                        if($rs_list->fields("producttitle")!="") { print("<span class='text-help'></span> <span class=''><b>".$rs_list->fields("producttitle"))."</b></span>";}
                                        //if($rs_list->fields("cattitle")!="") { print(" | <span class='text-help'></span> <span class=''>".$rs_list->fields("cattitle"))."</span>";}
                                        //if($rs_list->fields("subcattitle")!="") { print(" | <span class='text-help'></span> <span class=''>".$rs_list->fields("subcattitle"))."</span>";}
                                        if($rs_list->fields("itemcode")!="") { print("<br/><span class='text-help'>Item Code:</span> <span class=''>".$rs_list->fields("itemcode"))."";}
                                        if($rs_list->fields("color")!="") { print(" | <span class='text-help'>Color:</span> <span class=''>".$rs_list->fields("color"))."";}
                                        if($rs_list->fields("size")!="") { print(" | <span class='text-help'>Size:</span> <span class=''>".$rs_list->fields("size"))."";}
                                        //if($rs_list->fields("type")!="") { print(" | <span class='text-help'>Type / Model:</span> <span class=''>".$rs_list->fields("type"))."";}
                                        
                                        
                                        
                                        $int_total = 0;
                                        $int_total = ($rs_list->fields("price") + $rs_list->fields("tailoringprice")) *  $rs_list->fields("quantity");
                                        //if($int_total > 0) { print(" | <span class='text-help'>Total:</span> <span class='  '>".GetPriceValue($int_total))."</span>";}
                                        
                                        if($rs_list->fields("tailoringoptiontitle")!="") { print("<br/><span class='text-help'>Tailoring Option :</span> <span class=''>".$rs_list->fields("tailoringoptiontitle"))."</span>";}
                                        if($rs_list->fields("tailoringmeasurements")!="") { print(" | <span class='text-help'>Measurements :</span> <span class=''>".$rs_list->fields("tailoringmeasurements"))."</span> ";}
                                        
                                        $int_shippingval = $rs_list->fields("shippingvalue");
                                        $int_weight = $rs_list->fields("weight");
                                        $int_shipping_total_value = $int_shippingval * $int_weight * $rs_list->fields("quantity");

                                        $int_actual_amount = $int_actual_amount + $int_total;
                                        $int_promocodepkid = $rs_list->fields("promocodepkid");
                                        $int_wholesaler_discount_percentages = $rs_list->fields("wholesalerdiscount");
                                        $int_giftcard_amount = $rs_list->fields("giftcard");
                                        ?>
                                    </td>
                                    
                                    <td align="center">
                                        <?php if($rs_list->fields("quantity")!="") { print("<span class='text-help'></span><span class=''>".$rs_list->fields("quantity"))."</span> ";} ?>
                                    </td>
                                    <td align="right">
                                        <?php if($rs_list->fields("price")!="") { print("<span class='text-help'></span> <span class=''>".GetPriceValue($rs_list->fields("price")))."&nbsp;</span>";} ?>
                                    </td>
                                    <td align="right">
                                        <?php if($rs_list->fields("tailoringprice")!="") { print("<span class='text-help'></span> <span class=''>".GetPriceValue($rs_list->fields("tailoringprice")))."&nbsp;</span>";} ?>
                                    </td>
                                    <?php /* ?><td align="right">
                                        <?php if($int_shipping_total_value!="") { print("<span class='text-help'></span> <span class=''>".GetPriceValue($int_shipping_total_value)."</span>");} ?><?php */ ?>
                                    </td>
                                    <td align="right">
                                        <?php print("<span class=''><b>".number_format($int_actual_amount,2,".",",")."&nbsp;</b></span><br/>"); ?>
                                    </td>
                                </tr>
                                
                                <?php /* ?>
                                <?php 
                                $int_total_amount = $int_total_amount + $int_actual_amount;
                                if($int_cnt >= $rs_list_count->Fields("totalrecords")) { ?>
                                <tr>
                                    <td colspan="5" align="right"><b>Gross Total</b></td>
                                    <td align="right"><b><?php print(number_format($int_total_amount, 2)); ?></b></td>
                                </tr>
                                <?php } ?>
                                <?php */ ?>
                                
                                <?php if($int_promocodepkid > 0) {
                                    $sel_qry_promocode = "";
                                    $sel_qry_promocode = "SELECT * FROM t_store_promocode WHERE pkid = ".$int_promocodepkid;
                                    //print $sel_qry_promocode;
                                    $rs_promocode_details = GetRecordSet($sel_qry_promocode);
                                ?>
                                
                                <?php /* ?>
                                    <tr>
                                        <td colspan="5" align="right"><b>- Promo Code Discount</b>
                                        [<?php
                                            if($rs_promocode_details->Fields("title") != "") {

                                                print "".$rs_promocode_details->Fields("title");
                                            }
                                            if($rs_promocode_details->Fields("discountamount") != "" && $rs_promocode_details->Fields("discountamount") > 0) {

                                                print " - ".$rs_promocode_details->Fields("discountamount");
                                            }
                                            if($rs_promocode_details->Fields("discountpercentage") != "" && $rs_promocode_details->Fields("discountpercentage") > 0) {

                                                print " - ".$rs_promocode_details->Fields("discountpercentage")."%";
                                            }
                                            ?>]
                                        
                                        </td>
                                        <td align="right"><b><?php print($rs_list->fields("finaldiscountedamount"));  ?></b></td>
                                    </tr>
                                
                                <?php */ ?>
                                
                                    <?php } ?>
                                    <?php 
                                    $int_wholesaler_discount_amount = 0;
                                    $int_wholesaler_discount_amount = ($int_actual_amount * $int_wholesaler_discount_percentages) / 100; ?>
                                    <?php if($int_wholesaler_discount_percentages > 0) { ?>
                                    <tr>
                                        <td colspan="5" align="right"><b>- Wholesaler Discount</b></td>
                                        <td align="right"><b><?php print(number_format($int_wholesaler_discount_amount, 2)); ?></b></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($int_giftcard_amount > 0) { ?>
                                    <tr>
                                        <td colspan="5" align="right"><b>- Gift Card Amount</b></td>
                                        <td align="right"><b><?php print(number_format($int_giftcard_amount, 2)); ?></b></td>
                                    </tr>
                                    <?php } ?>
                                    
                                    <?php 
                                    $int_refunded_amount = 0;
                                    $str_shipping_status = "";
                                    
                                    if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION4 && $rs_list->fields("paymentoptionpkid") == 2) {
                                        if($rs_list->fields("giftcard") > 0) {
                                            //$int_refunded_amount = $rs_list->fields("giftcard");
					    $int_refunded_amount = 0;


                                            $str_shipping_status = $STR_CBO_OPTION4;
                                        } 
                                        else { 
                                            $int_refunded_amount = 0;
                                            $str_shipping_status = $STR_CBO_OPTION4;
                                        }
                                    } 
                                    else if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION4 && $rs_list->fields("paymentoptionpkid") <> 2) {
                                        if($rs_list->fields("giftcard") > 0) {
                                            $int_refunded_amount = (($int_actual_amount - $rs_list->fields("giftcard")) * $INT_REFUND_PERCENTAGE) / 100;
                                            $str_shipping_status = $STR_CBO_OPTION4;
                                            
                                        }
                                        else {
                                            $int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
                                            $str_shipping_status = $STR_CBO_OPTION4;
                                        } 
                                    }
				    else if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION5 && $rs_list->fields("paymentoptionpkid") == 2) {
                                        if($rs_list->fields("giftcard") > 0) {
                                            //$int_refunded_amount = $rs_list->fields("giftcard");
                                            $int_refunded_amount = (($int_actual_amount - $rs_list->fields("giftcard")) * $INT_REFUND_PERCENTAGE) / 100;
                                            $str_shipping_status = $STR_CBO_OPTION5;
                                        } 
                                        else { 
                                            $int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
                                            $str_shipping_status = $STR_CBO_OPTION5;
                                        }
                                    }  
                                    else if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION5 && $rs_list->fields("paymentoptionpkid") <> 2) {
                                         if($rs_list->fields("giftcard") > 0) {
                                            $int_refunded_amount = (($int_actual_amount - $rs_list->fields("giftcard")) * $INT_REFUND_PERCENTAGE) / 100;
                                            $str_shipping_status = $STR_CBO_OPTION5;
                                            
                                        }
                                        else {
                                            $int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
                                            $str_shipping_status = $STR_CBO_OPTION5;
                                        } 
                                        //$int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
                                        //$str_shipping_status = $STR_CBO_OPTION5;
                                        
                                    }
				    else {
                                         if($rs_list->fields("giftcard") > 0) {
                                            //$int_refunded_amount = (($int_actual_amount - $rs_list->fields("giftcard")) * $INT_REFUND_PERCENTAGE) / 100;
                                            //$int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
					    $int_refunded_amount = 0;
                                            //$str_shipping_status = $STR_CBO_OPTION5;
                                            
                                        }
                                        else {
                                            //$int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
					    $int_refunded_amount = 0;
                                            //$str_shipping_status = $STR_CBO_OPTION5;
                                        } 
                                        //$int_refunded_amount = ($int_actual_amount * $INT_REFUND_PERCENTAGE) / 100;
                                        //$str_shipping_status = $STR_CBO_OPTION5;
                                        
                                    }
				    ?>
                                    <?php /* ?><?php 
                                    if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION4 || $rs_list->fields("shippingstatus") == $STR_CBO_OPTION5) { ?>
                                    <tr>
                                        <td colspan="5" align="right"><label class="label label-danger"><?php print $str_shipping_status; ?></label> <b>- Refunded Amount</b></td>
                                        <td align="right"><b><?php print(number_format($int_refunded_amount, 2)); ?></b></td>
                                    </tr>
                                    <?php } ?><?php */ ?>
                                    
                                    <?php /* ?><?php if($int_shipping_total_value > 0) { ?>
                                    <tr>
                                        <td colspan="5" align="right"><b>+ Shipping Charge</b></td>
                                        <td align="right"><b><?php print(number_format($int_shipping_total_value, 2)); ?></b></td>
                                    </tr>
                                    <?php } ?><?php */ ?>
                                
                                    
                                    <?php
                                    // If order is cancelled and payment mode is COD
                                    if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION4  && $rs_list->fields("paymentoptionpkid") == 1) {
                                        $int_gross_amount = 0;
                                    }    
                                    // If order is cancelled and payment mode is not COD
                                    else if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION4 && $rs_list->fields("paymentoptionpkid") <> 1) {
                                        $int_gross_amount = $int_actual_amount + $int_shipping_total_value - $rs_list->fields("finaldiscountedamount") - $int_wholesaler_discount_amount - $int_giftcard_amount - $int_refunded_amount;
                                    } 
				    else if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION5  && $rs_list->fields("paymentoptionpkid") == 1) {
					$int_gross_amount = $int_actual_amount + $int_shipping_total_value - $rs_list->fields("finaldiscountedamount") - $int_wholesaler_discount_amount - $int_giftcard_amount - $int_refunded_amount;
                                    }    
                                    // If order is cancelled and payment mode is not COD
                                    else if($rs_list->fields("shippingstatus") == $STR_CBO_OPTION5 && $rs_list->fields("paymentoptionpkid") <> 1) {
                                        $int_gross_amount = $int_actual_amount + $int_shipping_total_value - $rs_list->fields("finaldiscountedamount") - $int_wholesaler_discount_amount - $int_giftcard_amount - $int_refunded_amount;
                                    } 
				    else {
                                        $int_gross_amount = $int_actual_amount + $int_shipping_total_value - $rs_list->fields("finaldiscountedamount") - $int_wholesaler_discount_amount - $int_giftcard_amount - $int_refunded_amount;
                                    } 
                                    ?>
                                    
                                <?php
                                    $int_total_amount = $int_total_amount + $int_actual_amount;
                                    $int_total_shipping_charge = $int_total_shipping_charge + $int_shipping_total_value;
                                    $int_total_promo_code = $int_total_promo_code + $rs_list->fields("finaldiscountedamount");
                                    $int_total_wholesaler_amount = $int_total_wholesaler_amount + $int_wholesaler_discount_amount;
                                    $int_total_giftcard_amount = $int_total_giftcard_amount + $int_giftcard_amount;
                                    $int_total_refund_amount = $int_total_refund_amount + $int_refunded_amount;
                                    $int_total_gross_amount = $int_total_gross_amount + $int_gross_amount;
                                    $rs_list->movenext();
                                    $int_cnt=$int_cnt+1;
                                    }
                                    ?>
                                    
                                    
                                    <tr>
                                        <td colspan="5" align="right"><input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>"><strong> Gross Total</strong></td>
                                        <td align="right"><?php print("<span class=''><b>".number_format($int_total_amount,2,".",",")."&nbsp;</b></span><br/>"); ?></td>
                                    </tr>
                                    <?php if($int_total_promo_code > 0) { ?>
                                    <tr>
                                        <td colspan="5" align="right"><b>- Promo Code Discount</b>
                                        
                                        [<?php
                                            if($rs_promocode_details->Fields("title") != "") {

                                                print "".$rs_promocode_details->Fields("title");
                                            }
                                            if($rs_promocode_details->Fields("discountamount") != "" && $rs_promocode_details->Fields("discountamount") > 0) {

                                                print " - ".$rs_promocode_details->Fields("discountamount");
                                            }
                                            if($rs_promocode_details->Fields("discountpercentage") != "" && $rs_promocode_details->Fields("discountpercentage") > 0) {

                                                print " - ".$rs_promocode_details->Fields("discountpercentage")."%";
                                            }
                                            ?>]
                                        
                                        
                                        </td>
                                        <td align="right"><b><?php print("<span class=''>".number_format($int_total_promo_code,2,".",",")."&nbsp;</span>"); ?></b></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($int_total_wholesaler_amount > 0) { ?>
                                    <tr>
                                        <td colspan="5" align="right"><b>- Wholesaler Discount</b></td>
                                        <td align="right"><b><?php print("<span class=''>".number_format($int_total_wholesaler_amount,2,".",",")."&nbsp;</span>"); ?></b></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($int_total_giftcard_amount > 0) { ?>
                                    <tr>
                                        <td colspan="5" align="right"><b>- Gift Card Discount</b></td>
                                        <td align="right"><b><?php print("<span class=''>".number_format($int_total_giftcard_amount,2,".",",")."&nbsp;</span>"); ?></b></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($int_total_refund_amount > 0) { ?>
				    <tr>
                                        <td colspan="5" align="right"><b>- Refund Amount</b></td>
                                        <td align="right"><b><?php print("<span class=''>".number_format($int_total_refund_amount,2,".",",")."&nbsp;</span>"); ?></b></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td colspan="5" align="right"><b>+ Shipping Charge</b></td>
                                        <td align="right"><b><?php print("<span class=''>".number_format($int_total_shipping_charge,2,".",",")."&nbsp;</span>"); ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" align="right"><b>Total</b></td>
                                        <td align="right"><b><?php print(number_format($int_total_gross_amount, 2)); ?>&nbsp;</b></td>
                                    </tr>
                            <?php }  ?>					
                        </tbody>
                    </table>
                </form>	
            </div>
        </div>
    </div>
</div>
<?php include "../../includes/include_files_admin.php"; ?>
<script>
function myFunction() 
{
    	window.print();
}
    </script>
</body></html>
