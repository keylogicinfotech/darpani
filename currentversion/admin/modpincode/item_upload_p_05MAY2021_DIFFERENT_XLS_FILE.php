<?php
/*
File Name  :- item_add_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include_once('../../PHPExcel-1.8/Classes/PHPExcel/IOFactory.php');


include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_file_system.php";
//include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_FILES); exit;
if(isset($_FILES['file_img']['tmp_name']) && $_FILES['file_img']['tmp_name'] != "")
{
    
    $uploadFilePath = $UPLOAD_IMG_PATH.basename($_FILES['file_img']['name']);

    if(file_exists($uploadFilePath))
    {
        DeleteFile($uploadFilePath);
    }
    
    move_uploaded_file($_FILES['file_img']['tmp_name'], $uploadFilePath);



    $inputFileName = $uploadFilePath;
    
    /*check point*/

    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);

    $data = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

    //print the result
    //print Count($data);exit;
    for($i=1;  $i<Count($data); $i++):
        $str_query_insert = "";
        $str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME." (product, pincode, city, state, region, prepaid, cod, reversepickup, pickup) VALUES('".$data[$i]['B']."', '".$data[$i]['C']."', '".$data[$i]['D']."', '".$data[$i]['E']."', '".$data[$i]['F']."', '".$data[$i]['G']."', '".$data[$i]['H']."', '".$data[$i]['I']."', '".$data[$i]['J']."' )";
        ExecuteQuery($str_query_insert);
        //print $str_query_insert."<br/>";
    endfor;
}
else
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}

//exit;
#----------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=S&type=S");
exit();
?>
