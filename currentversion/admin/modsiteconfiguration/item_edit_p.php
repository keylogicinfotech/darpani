<?php 
/*	
File Name  :- item_edit_p.php
Create Date:- JAN-2019
Intially Create By :-0014
Update History:
*/
#----------------------------------------------------------------------------------------------------------	
#include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";	
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_xml.php";	
$int_cnt=0;
#-------------------------------------------------------------------------------------------
#check value passed are proper or not
if(isset($_POST["hdn_count"]))
{
        $int_cnt = trim($_POST["hdn_count"]);
}
#--------------------------------------------------------------------------
#to check whether other dynamic fields are passed properly or not.
for($i=1;$i<=$int_cnt;$i++)
{
        if(isset($_POST["txt_display".$i]))
        {
                if($_POST["txt_display".$i] == "" || !is_numeric($_POST["txt_display".$i]) || $_POST["txt_display".$i] < 0)
                {
                        CloseConnection();
                        Redirect("item_edit.php?type=E&msg=F");
                        exit();
                }
        }
        else
        {
                CloseConnection();
                Redirect("item_edit.php?type=E&msg=F");
                exit();
        }	
}
for($i=1;$i<=$int_cnt;$i++)
{
        if(isset($_POST["txt_settingvalue".$i]))
        {
                if($_POST["txt_settingvalue".$i] == "")
                {
                        CloseConnection();
                        Redirect("item_edit.php?type=E&msg=F");
                        exit();							
                }
        }
        else
        {
                CloseConnection();
                Redirect("item_edit.php?type=E&msg=F");
                exit();			
        }
}
#----------------------------------------------------------------------------------------------------------------------------------------
#update records in table
for($i=1;$i<=$int_cnt;$i++)
{
        $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME. "  SET settingvalue='" . trim($_POST["txt_settingvalue".$i]) . "',displayorder=" . trim($_POST["txt_display".$i]) . " WHERE pkid=" . trim($_POST["hdn_pkid".$i]);
        ExecuteQuery($str_query_update);
}
#----------------------------------------------------------------------------------------------------------------------------------------	
#Writing data of table into XML file
#creating recordset.
$str_query_select = "";
$str_query_select = "SELECT settingkey,settingvalue FROM ".$STR_DB_TABLE_NAME. " WHERE visible='YES' ".$STR_DB_TABLE_NAME_ORDER_BY. " ";
$rs_list = GetRecordSet($str_query_select);

#storing recordset data into array.
if(!$rs_list->eof())
{
        $arr=array();
        $arr1=array();
        $i=0;
        while(!$rs_list->eof())
        {
                $arr[$i]=$rs_list->fields("settingkey");
                $arr1[$i]=$rs_list->fields("settingvalue");
                $i=$i+1;
                $rs_list->MoveNext();
        }		
        writeXmlFile($XML_FILE_PATH,$XML_ROOT_TAG,$arr,$arr1);
}
#----------------------------------------------------------------------------------------------------------------------------------------	
CloseConnection();
Redirect("item_edit.php?type=S&msg=S");
exit();	
?>