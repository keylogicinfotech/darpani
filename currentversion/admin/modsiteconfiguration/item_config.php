<?php
$STR_TITLE_PAGE = "Site Configuration";
$UPLOAD_IMG_PATH = "../../mdm//";
$INT_IMG_WIDTH = 0;
$INT_IMG_HEIGHT = 0;

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "siteconfiguration"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY displayorder DESC"; 

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "";

global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/siteconfiguration.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";	

$STR_MSG_DEFAULT_EMAIL = "Whenever any email sent from site then this email address will be used";
$STR_MSG_CONTACT_EMAIL="Contact us inquiry notification will be sent to this email ID";
?>
