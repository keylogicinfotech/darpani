<?php 
/*	
File Name  :- item_edit.php
Create Date:- Jan-2019
Intially Create By :-0014
Update History:
*/
#----------------------------------------------------------------------------------------------------------	
#include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME. " WHERE visible='YES' ".$STR_DB_TABLE_NAME_ORDER_BY. " ";
//print $str_query_select;
$rs_list = GetRecordSet($str_query_select);
#----------------------------------------------------------------------------------------------------------------------------------
# Initialization of variables used for message display. 
$str_type = "";
$str_msg = "";
# Get message type.
    if(isset($_GET["type"]))
    {
        switch(trim($_GET["type"]))
        {
            case("S"): $str_type = "S"; break;
            case("E"): $str_type = "E"; break;
            case("W"): $str_type = "W"; break;
        }
    }
# Get message text.
    if(isset($_GET["msg"]))
    {
        switch(trim($_GET["msg"]))
        {
            case("S"): $str_msg = $STR_MSG_ACTION_EDIT;break;
            case("F"): $str_msg = $STR_MSG_ACTION_INFO_MISSING; break;
        }
    } ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_SITE_CONFIG);?> : <?php print($STR_TITLE_EDIT); ?> </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<!-- Page Content -->
<a name="ptop"></a>
<div class="container">
   <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-md-12 button_space">
                <?php /*?><div class="btn-group" role="group" aria-label="...">
                        <a href="#help"  class="btn btn-info " title="Click to view help"><i class="fa fa-info-sign  "></i>&nbsp; Help</a> 
                        <a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Back to State List"><i class="fa fa-chevron-left"></i>&nbsp;Back</a>
                </div><?php */?>
        </div>
        <div class="col-md-9 col-sm-6 col-md-12" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_SITE_CONFIG); ?></h3></div>
    </div><hr>
    <?php if($str_type != "" && $str_msg != "") { print(DisplayMessage(0,$str_msg,$str_type)); } ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <i class="fa fa-edit "></i>&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>	
                </div>
            </div>
			
            <div class="table-responsive">
                <form name="frm_edit" action="item_edit_p.php" method="post" onSubmit="return frm_edit_validate();" role="form">
                    <table class="table table-striped table-bordered ">
                        <thead>
                            <tr>
                                <th><?php print($STR_TABLE_COLUMN_NAME_SETTING_CAPTION); ?></th>
                                <th width="50%"><?php print($STR_TABLE_COLUMN_NAME_SETTING_VALUE); ?></th>
                                <th width="6%"><?php print($STR_TABLE_COLUMN_NAME_ACTION); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($rs_list->eof()==true){?><tr><td colspan="3" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr><?php }?>
                                <?php $int_cnt = 1; while(!$rs_list->eof()) { ?>	
                            <tr>
                                <td class="">
                                    <h4 class="nopadding"><?php print(MyHtmlEncode($rs_list->fields("settingcaption")))?><span class="text-help-form">*</span></h4>
                                    <?php if($rs_list->fields("comments") != ""){?><span class="text-help-form">(<?php print(MyHtmlEncode($rs_list->fields("comments")))?>)</span>

                                    <?php }?></td>
                                <td><input type="text" name="txt_settingvalue<?php print($int_cnt)?>" value="<?php print(MyHtmlEncode($rs_list->fields("settingvalue")))?>" class="form-control input-sm " ><input name="hdn_pkid<?php print($int_cnt)?>" type="hidden" value="<?php print($rs_list->fields("pkid"))?>" ></td>
                                <td align="center"><input name="txt_display<?php print($int_cnt)?>" type="text" value="<?php print($rs_list->fields("displayorder"))?>" maxlength="4" class="form-control input-sm text-center" ></td>
                            </tr>
                                                <?php $int_cnt = $int_cnt + 1; $rs_list->movenext(); } ?>
                            <tr>
                                <td colspan="3" align="right">
                                    <input name="hdn_count" type="hidden" value="<?php print($int_cnt-1)?>">  
                                    <?php print DisplayFormButton("EDIT",0); ?>&nbsp;<?php print DisplayFormButton("RESET",0); ?>
                                </td>
                            </tr>
                        </tbody>	
                    </table>
                </form>
            </div>
        </div>
    </div>
    <?php include "../../includes/help_for_edit.php"; ?>		
    <!-- /.container -->											
</div>
<script language="JavaScript" src="./item_edit.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>  
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>