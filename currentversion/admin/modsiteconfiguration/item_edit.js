function frm_edit_validate()
{
    var cnt,i;
    cnt = document.frm_edit.hdn_count.value;
    for(i=1;i<=cnt;i++)
    {
        with(document.frm_edit)
        {
            if(eval("txt_display" + i).value == "")
            {
                alert("Please enter display order.");
                eval("txt_display" + i).select();
                eval("txt_display" + i).focus();
                return false;
            }
            if(isNaN(eval("txt_display" + i).value))
            {
                alert("Please enter numeric value for display order.");
                eval("txt_display" + i).select();
                eval("txt_display" + i).focus();
                return false;
            }
            if(eval("txt_display" + i).value < 0)
            {
                alert("Please enter positive integer for display order.");
                eval("txt_display" + i).select();
                eval("txt_display" + i).focus();
                return false;
            }
        }
    }
    for(i=1;i<=cnt;i++)
    {
        with(document.frm_edit)
        {
            if(eval("txt_settingvalue" + i).value == "")
            {
                alert("Please enter text for setting value.");
                eval("txt_settingvalue" + i).select();
                eval("txt_settingvalue" + i).focus();
                return false;
            }
        }
    }
    return true;
}	
