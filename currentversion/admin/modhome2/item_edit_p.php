<?php 
/*
File Name  :- item_edit.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "./item_app_specific.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_file_system.php";
#------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_POST);exit;
//print_r($_FILES);exit;
$str_desc = "";
$str_header = "";
$int_pkid = 0;
$str_hovertext = "";
$str_newsupdates = "";
$str_tweetupdates = "";
$str_url = "";
$str_titleurl = "";
$str_open = "YES";
$str_button_caption = "";
$str_display_button = "NON";
$str_title = "";
if(isset($_POST['txt_title']))
{ 
    $str_title = trim($_POST['txt_title']); 
    
}
if(isset($_POST['ta_desc']))
{
    $str_desc = trim($_POST['ta_desc']);
}

if(isset($_POST['txt_url']))
{
    $str_url=trim($_POST['txt_url']);
}//print $str_url; exit;
if(isset($_POST['txt_urltitle']))
{
    $str_titleurl=trim($_POST['txt_urltitle']);
}//print $str_titleurl; exit;
if(isset($_POST['txt_hover']))
{
    $str_hovertext=trim($_POST['txt_hover']);
}

if(isset($_POST['hdn_pkid']))
{
    $int_pkid = trim($_POST['hdn_pkid']);
}
$str_existing_image = "";
if(isset($_POST['hdn_existing_image']))
{
    $str_existing_image = trim($_POST['hdn_existing_image']);
}	

$str_image = "";
if(isset($_FILES['fileimage']))
{
    $str_image = trim($_FILES['fileimage']['name']);
}

//print $str_existing_image." - - - " .$str_image; exit;
if(isset($_POST['cbo_window']))
{
    $str_open = trim($_POST['cbo_window']);
}//print $str_open; exit;
if(isset($_POST['ta_newsupdates']))
{
    $str_newsupdates = trim($_POST['ta_newsupdates']);
}
if(isset($_POST['ta_tweetupdates']))
{
    $str_tweetupdates = trim($_POST['ta_tweetupdates']);
}	
/*	$str_image_visible="YES";
        if (isset($_POST["cbo_image_visible"]))
        {
                $str_image_visible = trim($_POST["cbo_image_visible"]);
        }*/
	
$str_button_caption = "";
if(isset($_POST['txt_caption']))
{
    $str_button_caption = trim($_POST['txt_caption']);
}

$str_display_button="NON";

if (isset($_POST["cbo_display_button"]))
{
    $str_display_button = trim($_POST["cbo_display_button"]);
}

$int_displayorder=0;
if (isset($_POST["txt_displayorder"]))
{
    $int_displayorder = trim($_POST["txt_displayorder"]);
}
#------------------------------------------------------------------------------------------------------------
#check all validation
//if($str_title=="")
//{
//	CloseConnection();
//	Redirect("cm_home_content.php?msg=F&type=E");
//	exit();
//}

/*if($str_image1_visible=="NO" && $str_image2_visible=="NO" && $str_image3_visible=="NO" && $str_image4_visible=="NO" && $str_image5_visible=="NO")
{
		CloseConnection();
		Redirect("cm_home_content.php?msg=IINV&type=E");
		exit();
}*/

if($str_image != "" )
{
    if(ValidateImageExtension($_FILES['fileimage']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
        CloseConnection();
        Redirect("item_edit.php?msg=I&type=E");
        exit();
    }
}
/*if(($str_desc =="") || ($str_desc =="<br>"))
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid));
    exit();
}*/
/*else
{
	CloseConnection();
	Redirect("cm_home_content.php?msg=IDE&type=E&pkid=".urlencode($int_pkid));
	exit();
}*/
/*if( ($str_title) == "" && ($str_desc) =="")
{
	CloseConnection();
	Redirect("cm_home_content_edit.php?msg=IDE&type=E&pkid=".urlencode($int_pkid));
	exit();
}*/
if($str_url != "")
{
    if(!validateURL02($str_url))
    {
        CloseConnection();
	Redirect("item_edit.php?msg=LINK&type=E&pkid=".urlencode($int_pkid));
	exit();
    }
}

#----------------------------------------------------------------------------------------------------
# Duplication Check
$str_query_select = "";
$str_query_select = "SELECT title FROM " .$STR_DB_TABLE_NAME. " WHERE title= '" . ReplaceQuote($str_title) . "' AND title = '' AND pkid!=".$int_pkid;
//print $str_query_dup;
$rs_list_check_duplicate = GetRecordSet($str_query_select);
/*if(!$rs_list_check_duplicate->eof()){
    CloseConnection();
    Redirect("item_edit.php?msg=DU&type=E&pkid=".urlencode($int_pkid));
    exit();
}*/
#---------------------------------------------------------------------------------------------------------	
# Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
//print $str_select_select;exit;
$rs_list = GetRecordset($str_query_select);

$str_large_file_name = "";
$str_large_path = "";
$str_large_file_name = trim($rs_list->fields("imagefilename"));

//print $str_large_file_name1."<BR>".$str_large_file_name2."<BR>".$str_large_file_name3."<BR>".$str_large_file_name4; 
//print $str_image1."<BR>".$str_image2."<BR>".$str_image3."<BR>".$str_image4; 
//exit;

if($str_image!="")
{
    #delete old image

    if($str_large_file_name != "")
    {
        DeleteFile($UPLOAD_IMG_PATH.trim($str_large_file_name));
    }

    $int_max_pkid = GetMaxValue($STR_DB_TABLE_NAME,"pkid");	
    #upload new image
    $str_large_file_name = GetUniqueFileName()."_home_content.".strtolower(getextension($str_image));
    $str_large_path = trim($UPLOAD_IMG_PATH.$str_large_file_name);
    UploadFile($_FILES['fileimage']['tmp_name'],$str_large_path);
    CompressImage($str_large_path, $str_large_path, 60);
    //ResizeImage($str_large_path,$INT_HOME_CONTENT_WIDTH);
}	

//print ($str_large_path); print($INT_HOME_CONTENT_WIDTH);exit;
#----------------------------------------------------------------------------------------------------------------------------------------
# Update Query
$int_max_do = 0;
$int_max_do = GetMaxValue($STR_DB_TABLE_NAME,"displayorder");

$str_update_query = "";
$str_update_query = "UPDATE ".$STR_DB_TABLE_NAME." set title='".ReplaceQuote($str_title)."',";
//$str_update_query .= "header='".ReplaceQuote($str_header)."',";
$str_update_query .= "description='".ReplaceQuote($str_desc)."',";
$str_update_query .= "hovertext='".ReplaceQuote($str_hovertext)."',";
//$str_update_query .= "newsupdates='".ReplaceQuote($str_newsupdates)."',";
//$str_update_query .= "tweetupdates='".ReplaceQuote($str_tweetupdates)."',";
$str_update_query .= "imagefilename='".ReplaceQuote($str_large_file_name)."',";
//$str_update_query .= "image_visible='".ReplaceQuote($str_image_visible)."',";
$str_update_query .= "photourl='".ReplaceQuote($str_url)."',";
$str_update_query .= "urltitle='".ReplaceQuote($str_titleurl)."',";
$str_update_query .= "caption='".ReplaceQuote($str_button_caption)."',";
$str_update_query .= "displaybutton='".ReplaceQuote($str_display_button)."',";
$str_update_query .= "openurlinnewwindow='".ReplaceQuote($str_open)."' ";
//$str_update_query .= "displayorder=".$int_max_do." "; //has to change this
$str_update_query .= " WHERE pkid=".$int_pkid;

//print $str_update_query; exit;
ExecuteQuery($str_update_query);

#----------------------------------------------------------------------------------------------------------------------------------------	
# Writing data of table into XML file	
WriteXml();
#----------------------------------------------------------------------------------------------------------------------------------------
# Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_list.php?msg=E&type=S");
exit();
?>
