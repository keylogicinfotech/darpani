<?php 
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
#----------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME." ".$STR_DB_TABLE_NAME_ORDER_BY." ";
//print $str_query_select;exit; 
//$str_query_select="SELECT * FROM cm_home_content";
$rs_list = GetRecordSet($str_query_select); 

$str_query_select = "";
$str_query_select = "SELECT count(*) totalrec FROM " .$STR_DB_TABLE_NAME. "";
$rs_list_count = GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------
# Initialization of variables used for message display.   
$str_visible_value = "";
if(isset($_GET["mode"]))
{
    if($_GET["mode"] == "YES") { $str_visible_value = "Visible"; }
    elseif($_GET["mode"] == "NO") { $str_visible_value = "Invisible"; }
}

$str_open="";
if(isset($_GET["window"])) 	{ $str_open = trim($_GET["window"]); }

$str_button_caption = "";
if(isset($_GET["captionbut"]))  { $str_button_caption = trim($_GET["captionbut"]); 	}	
	
    $str_display_button = "NON";
    if(isset($_GET["displaybut"]))  { $str_display_button= trim($_GET["displaybut"]); }
#	Get message type.
    $str_type = "";
    $str_message = "";	
    if(isset($_GET["type"]))
    {
        switch(trim($_GET["type"]))
        {
            case("S"): $str_type = "S"; break;
            case("E"): $str_type = "E"; break;
            case("W"): $str_type = "W"; break;
        }
    }
#Display message.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_visible_value; break;	
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;	
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("E"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("LINK"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;
        case("IINV"): $str_message = "All data can't be INVISIBLE, Please keep atleast one image visible."; break;
        //case("IDE"): $str_message = "Please select image or add description.";	break;
        case("IDE"): $str_message = "Please select image.";	break;
        case("DBC"): $str_message = "Please select button type and enter valid caption for selected button.";	break;
        case("SOH"): $str_message = $STR_MSG_ACTION_DISPLAY_ON_HOME;	break;
        case("SOT"): $str_message = $STR_MSG_ACTION_DISPLAY_ON_TOP;	break;
        case("NV"): $str_message = "Error... You can not display more than 3 records on top of home page.";	break;
        case("DU"): $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<a name="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?><a href="./cm_home_image.php" class="btn btn-default" title="<?php print($STR_HREF_TITLE_LINK);?> <?php print($STR_TITLE_HOME_IMAGE); ?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_HOME_IMAGE);?></a><?php */?>
	<?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Back to State List"><i class="fa fa-chevron-left"></i>&nbsp;Back</a><?php */?>
            </div>
        </div>
        <div class="col-md-9 col-sm-6 col-xs-12" align="right" >
            <h3><?php print($STR_TITLE_PAGE); ?></h3>
        </div>
    </div><hr>
    <?php if($str_type != "" && $str_message != "") { print(DisplayMessage("99%",$str_message,$str_type)); } ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <span>
                                <a class="accordion-toggle collapsed " title="<?php print($STR_HOVER_ADD); ?>" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>
                            </span>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    <form name="frm_add" action="item_add_p.php" method="post" onSubmit="return frm_add_validate();" enctype="multipart/form-data" role="form">
                        <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                            <?php /* ?>    <div class="form-group">
                                    <label>Title</label><span class="text-help-form"> </span>
                                    <input id="txt_title" name="txt_title" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" type="text" >
                                </div>  <?php */ ?>
                                <div class="form-group">
                                    <label>Description</label><span class="text-help-form">*</span>
                                    <textarea name="ta_desc" id="ta_desc" rows="10" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>" ></textarea>
                                    <span class="text-help-form"><?php //print($STR_MAX_255_MSG);?></span>
                                </div> 
                                
                              <?php /* ?>  
                               * <div class="form-group">
                                    <label>URL</label>
                                    <input class="form-control input-sm" name="txt_url" id="txt_url"  maxlength="512" value=""  placeholder="<?php print $STR_MSG_URL_FORMAT; ?>"  >         
                                </div><div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Upload Image</label><span class="text-help-form">&nbsp;(<?php print($STR_MSG_IMG_FILE_TYPE); ?>&nbsp;|&nbsp;<?php print($STR_MSG_IMG_UPLOAD); ?>)</span>&nbsp;
                                            <input type="file" name="fileimage" >
                                            <input name="hdn_existing_image" type="hidden" id="hdn_existing_image" value="<?php print($rs_list->fields("imagefilename")); ?>">
                                        </div>
                                    </div>
                                    
                                </div><?php */ ?>
                    <?php /* ?>            <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>URL Title</label>
                                            <input class="form-control input-sm" name="txt_urltitle" id="txt_urltitle"  maxlength="512" value=""  placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>">      
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>URL</label>
                                            <input class="form-control input-sm" name="txt_url" id="txt_url"  maxlength="512" value=""  placeholder="<?php print $STR_MSG_URL_FORMAT; ?>"  >         
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Open URL In New Window </label>&nbsp;<span class="text-help-form">(<?php print($STR_MSG_OPEN_IN_NEW_WINDOW); ?>)</span>
                                            <select name="cbo_window" class="form-control input-sm" >
                                                <option value="YES" <?php print(CheckSelected("YES",$str_open)); ?>>YES</option>
                                                <option value="NO" <?php print(CheckSelected("NO",$str_open));?>>NO</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Hover Text</label>
                                            <input class="form-control input-sm" name="txt_hover" id="txt_hover"  maxlength="512" value=""  placeholder="<?php print $STR_PLACEHOLDER_HOVER_TEXT; ?>"  >                                       
                                       </div>
                                    </div>
                                </div> <?php */ ?>
                                <input type="hidden" name="fileimage" value="" >
                                <input type="hidden" name="txt_title" value="" >
                                <input type="hidden" name="txt_urltitle" value="" >
                               <input type="hidden" name="txt_url" value="" >
                                <input type="hidden" name="cbo_window" value="" >
                               <input type="hidden" name="txt_hover" value="" >
                                <div class="row padding-10">
                                    <div class="col-md-12">
                                          <div class="form-group">
                                                <label>Visible</label>&nbsp;<span class="text-help-form">(<?php print($STR_MSG_VISIBLE)?>)</span><br/>
                                                <select name="cbo_visible" class="form-control input-sm" >
                                                <option value="YES">YES</option><option value="NO">NO</option></select>
                                            </div>                                          

                                    </div>  
                                </div>
                                <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                <input type="hidden" name="hdn_pkid" value="<?php print($rs_list->fields("pkid")); ?>" >
                                <input type="hidden" name="txt_header" class="" maxlength="100" value="">                            
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-10">
        <div class="col-md-12">
            <form name="frm_list" action="item_order_p.php" method="post" onSubmit="return frm_list_check_displayorder();">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                            <?php /* ?>    <th width="20%"><?php print $STR_TABLE_COLUMN_NAME_IMAGE; ?></th> <?php */ ?>
                                <th ><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                                <th width="2%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_ORDER; ?></th>
                                <th width="5%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>
                            </tr>
                        </thead>
                        <tbody>		
                        <?php if($rs_list->EOF()==true)
                        { ?>
                            <tr>
                                <td colspan="7" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td>
                            </tr>
                    <?php } 
                    else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
                            <tr>
                                <td align="center"><?php print($int_cnt)?></td>
                        <?php /*?>        <td align="left" class="align-top">
                                <?php if($rs_list->fields("imagefilename")!="") { ?>
                                    <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list->fields("pkid")); ?>" rel="thumbnail"><img src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>"  class="img-responsive" border="0" align="absmiddle" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image"></a>
                                    <div class="modal fade f-pop-up-<?php print($rs_list->fields("pkid")); ?> " align="center"  role="dialog" aria-labelledby="" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                    <img src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>" class="img-responsive" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image">
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                <?php  }  else {  print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); } ?> 
                                </td>  <?php */ ?>
                                <td class="align-top">
                                    <?php if($rs_list->fields("title") != "")  {?>
                                    <h4>
                                        <b><?php print(RemoveQuote($rs_list->fields("title"))); ?></b>
                                    </h4><?php } ?>
                                    <?php if($rs_list->fields("description") != "")  {?>
                                    <p align="justify" ><?php print(RemoveQuote($rs_list->fields("description")));?></p><?php } ?>
                                    <?php $str_target = ""; ?>
                                    
                      <?php /* ?>               <?php// if($rs_list->fields("photourl") != "") {
                                   if($rs_list->fields("openurlinnewwindow") == 'YES') {  $str_target = "_blank"; $str_new_window="<b>NEW</b> window"; }
                                        else { $str_target = "_self"; $str_new_window="<b>CURRENT</b> window"; } ?>
                                        <?php if($rs_list->fields("photourl") != "") {
                                            if($rs_list->fields("urltitle") != "") { ?>
                                            <i class="fa fa-globe" aria-hidden="true"></i>
                                            <?php   print(DisplayWebSiteURL($rs_list->fields("photourl"),$rs_list->fields("urltitle"),$rs_list->fields("openurlinnewwindow")," ",$rs_list->fields("hovertext"),$rs_list->fields("urltitle")));
                                                   ?>  
                                    <span class="text-help">
                                        [ will open in <?php print $str_new_window; ?> ]
                                    </span>       
                                                    
                                    <?php } else{ ?>
                                     <i class="fa fa-globe" aria-hidden="true"></i>
                                    <?php  print(DisplayWebSiteURL($rs_list->fields("photourl"),$rs_list->fields("photourl")," "," ",$rs_list->fields("hovertext"),$rs_list->fields("photourl"))); ?>
                                    <span class="text-help">
                                        [ will open in <?php print $str_new_window; ?> ]
                                    </span> 
                                    <?php  }
?>
                                  <?php }   <?php */ ?>
                                  
                                </td>
                            <?php /*?><?php $str_check=""; if(strtoupper(trim($rs_list->fields("displayontop")))=="YES") { $str_check=" checked"; } ?>
                            <td align="center">
                                    <input type="checkbox" name="chk_displayontop<?php print($int_cnt)?>" class="" <?php print($str_check); ?>>
                            </td>
                            <?php $str_checked=""; if(strtoupper(trim($rs_list->fields("displayonhome")))=="YES") { $str_checked=" checked"; } ?>
                            <td align="center">
                                    <input type="checkbox" name="chk_displayonhome<?php print($int_cnt)?>" class="" <?php print($str_checked); ?>>
                            </td><?php */?>
                                <td align="center">
                                    <input type="text" name="txt_displayorder<?php print($int_cnt);?>" size="5" maxlength="5" value="<?php print($rs_list->fields("displayorder"));?>" class="form-control input-sm text-center">
                                    <input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>">
                                </td>
                                    <?php $str_image="";  $str_class="";  $str_title="";
                                if(strtoupper($rs_list->fields("visible"))=="YES") { $str_image=$STR_LINK_ICON_PATH_VISIBLE;
                                    $str_class="btn btn-warning btn-xs"; $str_title=$STR_HOVER_VISIBLE; }
                                else { $str_image=$STR_LINK_ICON_PATH_INVISIBLE;
                                    $str_class="btn btn-default active btn-xs"; $str_title=$STR_HOVER_INVISIBLE; } 
                            ?>
                                <td align="center" >
                                    <p><a href="item_visible_p.php?pkid=<?php print($rs_list->fields("pkid"))?>" class="<?php print($str_class);?>" title="<?php print($str_title); ?>"><?php print($str_image);?></a></p>
                                    <p><a href="item_edit.php?pkid=<?php print($rs_list->fields("pkid"))?>" class="btn btn-success btn-xs " title="<?php print($STR_HOVER_EDIT)?>"><?php print $STR_LINK_ICON_PATH_EDIT; ?></a></p>
                                    <a href="item_del_p.php?pkid=<?php print($rs_list->fields("pkid"))?>" onClick="return confirm_delete() ;" class="btn btn-danger btn-xs" title="<?php print($STR_HOVER_DELETE)?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>
                                </td>
                            </tr>
                            <?php $int_cnt++; $rs_list->MoveNext(); } ?>
                        <input type="hidden" name="hdn_counter" value="<?php print($int_cnt)?>">
                        <tr class="<?php print($int_cnt%2); ?>">
                            <td></td>
                            
                            <td></td>
                            <?php /*?><td><button class="btn btn-success" name="btn" title="Click to change 'Display on Top' setting" onClick="return displayontop_click();">Change</button></td>
                                 *  <td><button class="btn btn-success" name="shop_btn" title="Click to change 'Display on Home' setting" onClick="return displayonhome_click();">Change</button></td><?php */?>
                            <td align="center" valign="middle"><?php print DisplayFormButton("SAVE",8); ?></td>
                            <td align="center"></td>
                        </tr>
                        <?php } ?>	
                    </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
    <!-- /.container -->											
</div>
<!-- Bootstrap Core CSS -->
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
    <script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
</body>
</html>