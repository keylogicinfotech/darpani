/*
	Module Name:- Login
	File Name  :- login.js
	Create Date:- 01-02-2006
	Intially Create By :- 0022Honey
	Update History:
*/
function validate_frm_login()
{
	with(document.frm_login)
		{
			if(isEmpty(txt_adminid.value))	
				{
					alert("Please enter the admin id");
					txt_adminid.select();
					txt_adminid.focus();
					return false;
				}
			if(isEmpty(pas_admin_password.value))	
				{
					alert("Please enter the password ");
					pas_admin_password.select();
					pas_admin_password.focus();
					return false;
				}
		}

return true;
}