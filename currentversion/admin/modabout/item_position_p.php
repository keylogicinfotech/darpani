<?php
/*
	Module Name:- admmodule
	File Name  :- adm_module_position_p.php
	Create Date:- 18-APR-2006
	Intially Create By :- 0025
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	//include "./validate_adm_login.php";
	include "../../includes/configuration.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
        include "./item_config.php";
        include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#get Query String Data
$int_pkid="";
$str_desc="";
if(isset($_GET["pkid"])==true)
{
	$int_pkid=trim($_GET["pkid"]);
}//print $int_pkid; exit;	
if(isset($_POST["ta_desc"]))
{
	$str_desc = trim($_POST["ta_desc"]);
}
//print $str_desc; exit;	
if(isset($_GET["reqpos"])==true)
{
	$req_pos=trim($_GET["reqpos"]);
}//print $req_pos; exit;		
//if($int_pkid <=0 || is_numeric($int_pkid) == false)
//{
//	CloseConnection();
//	Redirect("item_list.php?msg=F&type=E&#ptop");
//	exit();
//}
#----------------------------------------------------------------------------------------------------
#select query to get product details from t_module table
$str_query_select="SELECT position,description,imagefilename FROM t_about WHERE pkid=". $int_pkid;
//print $str_query_select; exit;
//print "hello"; exit;

$rs_position=GetRecordSet($str_query_select);
//print $str_query_select; exit;
//print "hello"; exit;

if($rs_position->eof()==true)
{
	CloseConnection();
	Redirect("item_list.php?msg=F&type=E&#ptop");
	exit();
}//print "hello"; exit;
$str_existing_mode="";
$str_title="";
$str_existing_mode=$rs_position->fields("position");
//print $str_existing_mode; exit;
$str_title=trim($rs_position->fields("imagefilename"));
//print $str_title; exit;
$msg="P";
if($rs_position->fields("pkid")==0)
{
	$msg="P";
}
else
{
	$msg="SV";
}
#----------------------------------------------------------------------------------------------------
#change position mode
$str_mode_title="";
$str_req_mode="";
if($int_pkid!="" || $req_pos!="")
{
	if(strtoupper($str_existing_mode)=="LEFT")
	{
		$str_req_mode=$req_pos;
		$str_mode_title=$req_pos;
	}
//	else if(strtoupper($str_existing_mode)=="MIDDLE")
//	{
//		$str_req_mode=$req_pos;
//		$str_mode_title=$req_pos;
//	}
	else if(strtoupper($str_existing_mode)=="RIGHT")
	{
		$str_req_mode=$req_pos;
		$str_mode_title=$req_pos;
	}
}
#----------------------------------------------------------------------------------------------------
#update status in t_module table
$str_query_update="UPDATE t_about SET position='" . ReplaceQuote($str_req_mode) . "' WHERE pkid=".$int_pkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------
#Write to xml file
WriteXml();
#-----------------------------------------------------------------------------------------------------
#Close connection and redirect to prod_cat_list.php page	
CloseConnection();
Redirect("item_list.php?type=S&msg=".$msg."&title=".urlencode(RemoveQuote($str_title))."&po_mode=". urlencode(RemoveQuote($str_mode_title)) ."&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------
?>