<?php
/*
File Name  :-item_cms_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_xml.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid="";
$str_desc="";
$str_visible="";

if(isset($_POST['ta_desc']))
{
    $str_desc=trim($_POST['ta_desc']);
}//print $str_desc; exit;
if(isset($_POST['hdn_pkid']))
{
    $int_pkid=trim($_POST['hdn_pkid']);
}//print $int_pkid; exit;
if(isset($_POST['cbo_visible']))
{
    $str_visible=trim($_POST['cbo_visible']);
}//print $str_visible; exit;
#----------------------------------------------------------------------------------------------------
#Select query to get details from table
$str_query_select="";
$str_query_select="SELECT * FROM ".$STR_DB_TABLE_NAME_CMS."";
//$str_query_select="select description,itemkey from ".$STR_DB_TABLE_NAME_CMS." where visible=".$str_visible;
//print $str_query_select; exit;
$rs_list=GetRecordset($str_query_select);
if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_cms.php?msg=F&type=E");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Update query to set values in table
$str_query_update="UPDATE ".$STR_DB_TABLE_NAME_CMS." SET description='".ReplaceQuote($str_desc)."' ,";
$str_query_update=$str_query_update."visible='".ReplaceQuote($str_visible)."' where pkid=".$int_pkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#Creating recordset for XML
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_CMS." ";
//print $str_query_select; exit;
$rs_list_xml = GetRecordSet($str_query_select);
//print $rs_list_xml->fields("description");exit;
//print $rs_list_xml->fields("visible");exit;
#Storing recordset data into array.
if(!$rs_list_xml->eof())
{
    $arr=array();
    $arr1=array();
    $arr2=array();
    $i=0;
    while(!$rs_list_xml->eof())
    {
        $arr[$i]=$rs_list_xml->fields("itemkey")."_description";
        $arr1[$i]=$rs_list_xml->fields("description");
        $i=$i+1;
        
        $arr[$i]=$rs_list_xml->fields("itemkey")."_visible";
        $arr1[$i]=$rs_list_xml->fields("visible");
        $i=$i+1;
        
        $rs_list_xml->MoveNext();
    }		
    #Writing data of table into XML file
    writeXmlFile($XML_FILE_PATH_CMS,$XML_ROOT_TAG_CMS,$arr,$arr1,$arr2);
}
#----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_cms.php?msg=E&type=S");
exit();
#----------------------------------------------------------------------------------------------------
?>
