<?php
/*
File Name  :- item_edit_p.php
Create Date:- 18-JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_file_system.php";
include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Initializing varialbles
$int_pkid="";
$str_sub="";
$str_desc="";
$str_image="";

#Get values of all passed GET / POST variables
if(isset($_POST['hdn_pkid']))
{
    $int_pkid=trim($_POST['hdn_pkid']);
}
if($int_pkid=="" || $int_pkid<0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
if(isset($_POST['txt_title']))
{
    $str_sub=trim($_POST['txt_title']);
}
if(isset($_POST['ta_desc']) )
{
    $str_desc=trim($_POST['ta_desc']);
}
if(isset($_FILES['fileimage']))
{
    $str_image=trim($_FILES['fileimage']['name']);
}
#----------------------------------------------------------------------------------------------------
#Check all validation
if($str_sub=="" )
{
    //CloseConnection();
    //Redirect("item_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid));
    //exit();
}
if($str_desc=="" || $str_desc == "<br>")
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid));
    exit();
}
if($str_image!="" )
{
    #Here we cannot use directly name of image file as first argument. Instead we have to specify $_FILES['filename']['tmp_name'].
    if(ValidateImageExtension($_FILES['fileimage']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
        CloseConnection();
        Redirect("item_edit.php?msg=I&type=E&pkid=".urlencode($int_pkid));
        exit();
    }
/* if(CheckFileExtension($str_image,$STR_IMG_FILE_TYPE_VALIDATION)==0 )
    {
            CloseConnection();
            Redirect("item_edit.php?msg=I&type=E&pkid=".urlencode($int_pkid));
            exit();
    }*/
}

#---------------------------------------------------------------------------------------------------------	
# Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
//print $str_select_select;exit;
$rs_list = GetRecordset($str_query_select);

$str_large_file_name = "";
$str_large_path = "";
$str_large_file_name = ($rs_list->fields("imagefilename"));

//print $str_large_file_name1."<BR>".$str_large_file_name2."<BR>".$str_large_file_name3."<BR>".$str_large_file_name4; 
//print $str_image1."<BR>".$str_image2."<BR>".$str_image3."<BR>".$str_image4; 
//exit;

if($str_image!="")
{
    #delete old image

    if($str_large_file_name != "")
    {
        DeleteFile($UPLOAD_IMG_PATH.trim($str_large_file_name));
    }

    $int_max_pkid = GetMaxValue($STR_DB_TABLE_NAME,"pkid");	
    #upload new image
    $str_large_file_name = GetUniqueFileName()."_t.".strtolower(getextension($str_image));
    $str_large_path = trim($UPLOAD_IMG_PATH.$str_large_file_name);
    UploadFile($_FILES['fileimage']['tmp_name'],$str_large_path);
    //ResizeImage($str_large_path,$INT_HOME_CONTENT_WIDTH);
}	

//print ($str_large_path); print($INT_HOME_CONTENT_WIDTH);exit;
#----------------------------------------------------------------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------------------------	
/*$str_query_select="";
$str_query_select="SELECT imagefilename FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
$rs_list=GetRecordset($str_query_select);


$str_thumb_file_name="";
$str_thumb_path = "";
if($rs_list->eof())
{
    $str_thumb_file_name = $rs_list->fields("imagefilename");
}


    #Delete old image
    if($str_thumb_file_name!="")
    {
        DeleteFile($UPLOAD_IMG_PATH.trim($str_thumb_file_name));
    }
    #Upload new image
    $str_thumb_file_name=GetUniqueFileName()."_t.".getextension($str_image);
//    print "img:" .$str_thumb_file_name; exit;
    $str_thumb_path = trim($UPLOAD_IMG_PATH."/".$str_thumb_file_name);
//    print $str_thumb_path;exit;
    UploadFile($_FILES['fileimage']['tmp_name'],$str_thumb_path); */
    //ResizeImage($str_thumb_path,$INT_IMG_THUMB_WIDTH);	
#----------------------------------------------------------------------------------------------------
#
#
#Update query in table
$str_query_update="UPDATE ".$STR_DB_TABLE_NAME." SET title='".ReplaceQuote($str_sub)."',";
$str_query_update.="description='".ReplaceQuote($str_desc)."',";
$str_query_update.="imagefilename='".ReplaceQuote($str_large_file_name)."'";
$str_query_update.=" WHERE pkid=". $int_pkid;
//print $str_query_update;exit; 
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=U&type=S&tit=".urlencode(RemoveQuote($str_sub)));
exit();
#----------------------------------------------------------------------------------------------------
?>
