/*
	Module Name:- modprivacy
	File Name  :- privacy_edit.js
	Create Date:- 11-FEB-2006
	Intially Create By :- 0022Honey
	Update History:
*/
function frm_privacy_edit_validateform()
{
	with(document.frm_privacy_edit)
	{
  		/*if(trim(txt_title.value) == "")
		{
			alert("Please enter subject.");
			txt_title.select();
			txt_title.focus();
			return false;
		}
		if(trim(ta_desc.value) == "")
		{
			alert("Please enter description.");
			ta_desc.select();
			ta_desc.focus();
			return false;
		}*/
		if(trim(fileimage.value) != "")
		{
			if(checkExt(trim(fileimage.value))==false)
			{
				fileimage.select();
				fileimage.focus();
				return false;
			}
		}
	}
	return true;
}

function Confirm_Deletion()
{
	if(confirm("Are you sure you want to delete this photograph permanently."))
	{
		if(confirm("Confirm Deletion:Click 'OK' to delete this photograph or 'Cancel' to deletion."))
		{
			return true;
		}
	}
	return false;
}

function show_details(url)
{
	window.open(url,'PrivacyPolicy','left=50,top=20,scrollbars=yes,width=570,height=520,resizable=yes');
	return false;
}
