<?php 
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------

?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include("../../includes/adminheader.php"); ?>
    <div class="row padding-10">
        <div class="col-md-12 button_space">
           
        </div>
        
    </div>
    <hr> 
    
    
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/font-awesome.min.css" rel="stylesheet">
<link href="../../css/admin.css" rel="stylesheet">

<script language="JavaScript" src="../../js/functions.js" type="text/javascript"></script>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
<script language="JavaScript" src="./item_add.js" type="text/javascript"></script>
<script type="text/javascript" src="../../js/richetxteditor.js"></script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() 
  {
    new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_desc');
        //new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
});
        // convert all text areas to rich text editor on that page
    bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
        // convert text area with id area2 to rich text editor with full panel.
    bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true}).panelInstance('ta_desc');
        // new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>
<script>
$( "#btn_Add" ).click(function() {
    //if($("input#txt_title").val()=="" ){ alert('Please enter subject');return false;}
    //if($('div .nicEdit-main').text()=="" ){ alert('Please enter description');return false;}
});
$( "#btn_reset" ).click(function() {
    location.reload();
});
</script>
</body></html>
