<?php
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------
#select query to get all the details from table	
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME." ".$STR_DB_TABLE_NAME_ORDER_BY." ";
//print $str_query_select;
$rs_list=GetRecordset($str_query_select);
#------------------------------------------------------------------------------------------
#getting query string variables	
$str_sub="";
$str_desc="";
$str_visible="";
$str_pos="";
if(isset($_GET['question'])) { $str_sub=trim($_GET['question']); }
if(isset($_GET['desc'])) { $str_desc=trim($_GET['desc']); }
if(isset($_GET["visible"])) { $str_visible=trim($_GET["visible"]); }
if (isset($_GET["position"])){ $str_pos = trim($_GET["position"]);  }
if (isset($_GET["po_mode"])){$str_pos = trim($_GET["po_mode"]);}
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type="";
$str_message="";
$str_title="";
$str_mode="";
if(isset($_GET['tit'])) { $str_title=trim($_GET['tit']); }
if(isset($_GET['mode'])) { $str_mode=trim($_GET['mode']); }
#Get message type.
if(isset($_GET['type']))
{
    switch(trim($_GET['type']))
    {
        case("S"): $str_type = "S"; break; case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Get message text.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
        case("P"): $str_message = "Module changed to '".$str_pos."' successfully."; break;
    }
}	
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-6 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="fa fa-chevron-left "></i>&nbsp;Back</a><?php */?>
                <a href="./item_cms.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)">
                    <i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)
                </a>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12" align="right" >
            <h3><?php print($STR_TITLE_PAGE);?> </h3>
        </div>
    </div>
    <hr> 
    <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <span>
                                <a class="accordion-toggle collapsed" data-toggle="collapse" title="<?php print $STR_HOVER_ADD; ?>" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>
                            </span>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
                        <div class="panel-body">
                            <form name="frm_add" action="./item_add_p.php" method="post" onSubmit="return frm_add_validateform()" enctype="multipart/form-data">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="form-group">
                                    <label>Title</label><span class="text-help-form"> </span>
                                    <input type="text" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" name="txt_title"  id="txt_title" class="form-control input-sm" maxlength="255" value="<?php print(RemoveQuote($str_sub));?>">
                                </div>
                                <div class="form-group">
                                    <label>Description</label><span class="text-help-form"> *</span>
                                    
                                    <textarea name="txt_desc" id="ta_desc" rows="20" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>" ><?php print(RemoveQuote($str_desc)); ?></textarea>
                                    
                                  <?php /* ?>  <textarea name="ta_desc"  rows="20" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>"><?php print(RemoveQuote($str_desc));?></textarea>  <?php */ ?>
                                </div>
                                <div class="form-group">
                                    <label>Upload Image</label>
                                    <span class="text-help-form"> (<?php print($STR_MSG_IMG_FILE_TYPE);?> | <?php //print($STR_IMG_MSG);?><?php print($STR_MSG_IMG_UPLOAD);?>)</span>
                                    <input type="file" name="fileimage" maxlength="255" class="" >
                                </div>
                                <div class="form-group">
                                    <label>Visible</label><span class="text-help-form"> *</span>
                                    <span class="text-help-form"> (<?php print($STR_MSG_VISIBLE);?>)</span>
                                    <select name="cbo_visible" class="form-control input-sm"><option value="YES" <?php print(CheckSelected("YES",$str_visible));?>>YES</option><option value="NO" <?php print(CheckSelected("NO",$str_visible)); ?>>NO</option></select>
                                </div>
				<?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <form name="frm_list" action="./item_order_p.php" method="post" onSubmit="return frm_list_check_displayorder();">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_POSITION; ?></th>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_ORDER; ?></th>
                        <th width="5%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php $int_cnt=1; 
                          $mod_pos=""; 
                          $pkid=0; 
                          $flag=0; 
                    while(!$rs_list->EOF()==true) {  ?>
                     <?php
                        if($pkid != $rs_list->fields("pkid") && $flag==0)
                        {
                            //$modparentpkid=$rs_list->fields("modparentid"); 
                            $mod_pos=$rs_list->fields("position");
                        }
                        else{}?>
                    <tr>
                        <td align="center" class="text-center"><?php print($int_cnt); ?></td>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr> 
                                    <td align="left" valign="top">
                                        <table align="<?php print($mod_pos);?>" cellpadding="1" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" align="left">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td valign="top" align="left">
                                                          
                                    <?php if($rs_list->fields("imagefilename")!="") { ?>
                                        <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list->fields("pkid")); ?>" rel="thumbnail"><img src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>"  class="img-responsive" border="0" align="absmiddle" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image"></a>
                                        <div class="modal fade f-pop-up-<?php print($rs_list->fields("pkid")); ?> " align="center" role="dialog" aria-labelledby="" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                        <img src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>" class="img-responsive img-rounded" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image">
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div>
                                <?php   }  ?>
                                                                    <?php /* ?>       <?php if($rs_list->fields("imagefilename")!="") { ?><img class="img-responsive img-thumbnail"  width="<?php print($INT_IMG_THUMB_WIDTH); ?>" border="0" src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image"><?php } 
                                                                    // else { print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); } ?><?php */ ?>
                                                                </td>
                                                                <td width="5"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table>
                                            <tr>
                                                <td align="justify">
                                                    <h4><b class="" ><?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("title")))); ?></b></h4>
                                                </td>
                                            </tr>
                                        </table>
                                        <div align="justify">
                                            <?php print(RemoveQuote($rs_list->fields("description"))); ?>
                                        </div>
                                    </td>
                                </tr>
                            </table>								
                        </td>
                        
                    
                        
                <?php
     /*           if ($rs_list->fields("position")=="LEFT")
                {  
                    $str_btn_visible = "SetInvisible";
//                    $str_btn_class = "btn btn-primary btn-xs";
                    $str_class_left = "btn btn-primary btn-xs";
                    $str_title_left = "Click to make position 'LEFT'";
                    $str_title_for_left = "ONLINE";

		    $str_image_offline = "<i class=aria-hidden='true'></i>";
                    $str_class_right = "btn btn-primary btn-xs";
//                    $str_title_offline = "Currently you are online, click to be offline.";
                    $str_title_right = "Click to make position 'RIGHT'";
                }
                else
                {
                     $str_btn_visible = "SetInvisible";
//                    $str_btn_class = "<i class=aria-hidden='true'></i>";
                    $str_class_left = "btn btn-primary btn-xs";
                    $str_title_left = "Click to make position 'RIGHT'";
                    $str_title_for_view_online = "Click to be online";

		    $str_image_offline = "<i class='fa fa-circle' aria-hidden='true'></i>";
                    $str_class_right = "btn btn-primary btn-xs disable";
//                    $str_title_offline = "Currently you are online, click to be offline.";
                    $str_title_for_right = "OFFLINE";
                } */ ?>
                        
       <?php /* ?>         <?php if(strtoupper($rs_list->fields("position"))=="LEFT")
                { ?>
			<a href="" title="<?php print $str_title_left; ?>" class="<?php print $str_class_left; ?>"><?php print $str_btn_visible; ?>&nbsp;<b><?php print "LEFT"; ?></b></a>
		<?php } else { ?>
			<a href="" title="<?php print $str_title_left; ?>" class="<?php print $str_class_left; ?>"><?php print $str_btn_visible; ?>&nbsp;<b><?php print "RIGHT"; ?></b></a>
		<?php } ?>      <?php */ ?>
                        
                        
                       <?php if($mod_pos=="LEFT")
                        {	$str_class="SetVisibleLink";
                                $str_td_class="class='Setvisible'";
                                $str_tip="Click to make position 'LEFT'";
                                
                        }

                        elseif($mod_pos=="RIGHT")
                        {	$str_class="SetInvisibleLink";
                                $str_td_class="class='SetInvisible'";
                                $str_tip="Click to make position 'RIGHT'";
                        }
                        else { $str_td_class=""; }
                            $str_tip_left="Click to make position 'LEFT'";
                            $str_tip_right="Click to make position 'RIGHT'";
                        ?>
							
                        <td align="center" valign="middle" >
                            
                        <?php
                        
                            if ($rs_list->fields("position")=="LEFT") { 
                           $display_left="display:none";
                           $display_right="";
                           $display_middle="";
                           $space_left_side="";
                           $space_middle_side="";
//                            $str_td_leftclass="class='SetInvisible'";
                           $str_td_rightclass="display:none";
                            } 
                            if ($rs_list->fields("position") == "RIGHT") {
                           $display_left="";
                           $display_right="display:none";
                           $display_middle="";
                           $space_left_side="&nbsp;&nbsp;";
                           $space_middle_side="";
                           $str_td_leftclass="display:none";
//                           $str_td_rightclass="class='SetInvisible'";
                            }					
						 //if($modparentpkid==0) { ?>
                            <p><a href="item_position_p.php?pkid=<?php print($rs_list->fields("pkid"));?>&reqpos=LEFT" name="pos_left"  style=" <?php print($display_left); ?>" class="  <?php print($str_class);?>" title="<?php print($str_tip_left);?>"><?php //print($mod_pos);?><b>LEFT</b></a></p>
                            <p><a class="btn btn-primary btn-xs disabled" style=" <?php print($display_right); ?>"><b><?php print "LEFT"; ?></b></a></p> 
                           
                            <a href="item_position_p.php?pkid=<?php print($rs_list->fields("pkid"));?>&reqpos=RIGHT" name="pos_right" style=" <?php print($display_right); ?>" class="  <?php print($str_class);?>" title="<?php print($str_tip_right);?>"><?php //print($mod_pos);?><b>RIGHT</b></a>
                                <p><a class="btn btn-primary btn-xs disabled" style=" <?php print($display_left); ?>"><b><?php print "RIGHT"; ?></b></a></p>
                        </td>
                        
                        
                        <td align="center">
                            <input type="text" name="txt_displayorder<?php print($int_cnt);?>" maxlength="5" value="<?php print($rs_list->fields("displayorder"));?>" class="form-control input-sm text-center" >
                            <input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>">
                            <input type="hidden" name="hdn_parentpkid<?php print($int_cnt);?>" value="<?php print($modparentpkid);?>">
                        </td>
                        <?php $str_image="";
                        if(strtoupper($rs_list->fields("visible"))=="YES")
                        {   $str_image=$STR_LINK_ICON_PATH_VISIBLE;
                            $str_class="btn btn-warning btn-xs";
                            $str_title=$STR_HOVER_VISIBLE;
                        }
                        else
                        {   $str_image=$STR_LINK_ICON_PATH_INVISIBLE;
                            $str_class="btn btn-default active btn-xs";
                            $str_title=$STR_HOVER_INVISIBLE;
                        }
                        ?>
                        <td align="center" class="text-center">
                            <p><a class="<?php print($str_class); ?>" href="./item_visible_p.php?pkid=<?php print($rs_list->fields("pkid"));?>"  title="<?php print($str_title); ?>"><?php print($str_image);?></a></p>
                            <p><a class="btn btn-success btn-xs" href="./item_edit.php?pkid=<?php print($rs_list->fields("pkid"));?>" title="<?php print($STR_HOVER_EDIT);?>"><?php print $STR_LINK_ICON_PATH_EDIT; ?></a></p>
                            <a class="btn btn-danger btn-xs" href="./item_del_p.php?pkid=<?php print($rs_list->fields("pkid"));?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE);?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>	
                        </td>
                    </tr>
                    <?php $int_cnt++; $rs_list->MoveNext(); } ?>
                    <tr>
                        <td></td><td></td><td></td>
                        <td><input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>"><?php print DisplayFormButton("SAVE",0); ?></td>
                        <td></td>
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
<script language="JavaScript" src="./item_add.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>
