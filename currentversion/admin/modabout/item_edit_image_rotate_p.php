<?php
/*
Module Name:- modBiography
File Name  :- bio_del_img_p.php
Create Date:- 11-FEB-2006
Intially Create By :- 0023
Update History:
*/
ob_start();
error_reporting(0);
#----------------------------------------------------------------------------------------------------
#Include Files
include "../includes/validatesession.php";
include "../includes/configuration.php";
include "item_config.php";
include "../includes/lib_data_access.php";
include "../includes/lib_common.php";	
include "../includes/lib_file_system.php";
include "../includes/lib_image.php";
include "../includes/lib_xml.php";
include "item_app_specific.php";
include "../includes/lib_file_upload.php";
#----------------------------------------------------------------------------------------------------
# filter variables
$int_page="";
$int_cat_pkid="";
$str_mname="";
//header("Refresh:0");
#get filter data
if(isset($_POST["catid"]) && trim($_POST["catid"])!="" )
{
	$int_cat_pkid=trim($_POST["catid"]);
}//print $int_cat_pkid; exit;
if(isset($_POST["key"]) && trim($_POST["key"])!="" )
{
	$str_mname=trim($_POST["key"]);
}
#get paging data
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"])!="" && is_numeric($_POST["PagePosition"])==true && trim($_POST["PagePosition"])>0)
{
	$int_page= $_POST["PagePosition"];
}
else
{
	$int_page=4;
}
$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_mname."&PagePosition=".$int_page;
		
//print $str_filter; exit;
#get data
$int_pkid="";
if (isset($_GET["pkid"]))
	$int_pkid=trim($_GET["pkid"]);
//print $int_pkid; exit;	

if($int_pkid=="" || $int_pkid<0 || is_numeric($int_pkid)==false)
{
	CloseConnection();
	Redirect("item_detail_list.php?msg=F&type=E".$str_filter."&#ptop");
	exit();
}
$int_id="";
if (isset($_GET["id"]))
{
	$int_id=trim($_GET["id"]);
}//print $int_id; exit;	
if($int_id=="" || $int_id<0 || is_numeric($int_id)==false)
{
	CloseConnection();
	Redirect("item_detail_list.php?msg=F&type=E&pkid=".$int_tpkid."&#ptop");
	exit();
}
//$str_phototype = "";
//if (isset($_GET["phototype"])) 
//{
//	$str_phototype=trim($_GET["phototype"]);
//}
//print $str_phototype;exit();
$int_degree = 90;

//print $str_option_category;exit();
#----------------------------------------------------------------------------------------------------------------------------------------------------------------	
#select query to get image detail from table.
//if($str_phototype == "rotate_photo4")
//{
	$str_query_select="SELECT thumbimagefilename, largeimagefilename FROM tr_photoalbum_detail WHERE photopkid=".$int_id." AND photoalbumpkid = ".$int_pkid;
        
//         print $str_query_select; exit;
	$rs_del=GetRecordSet($str_query_select);
//	print "$rs_del"; exit;
	if($rs_del->eof()==true)
	{ 
            CloseConnection();
            //print "error"; exit;
            Redirect("item_detail_list.php?msg=F&type=E&#ptop");
            exit();
        }
	$str_dir="";
	$str_dir = $UPLOAD_PHOTO_PATH;
        //print $str_dir; exit;
	if(!file_exists($str_dir))
	{
            CloseConnection();
            Redirect("item_detail_list.php?msg=F&type=E&pkid=".$int_pkid.$str_filter."&#ptop");
            exit();
	}
	$str_large_file="";
	$str_thumb_file="";
	$str_large_file = $str_dir.$int_pkid."/".trim($rs_del->Fields("largeimagefilename"));
	$str_thumb_file = $str_dir.$int_pkid."/".trim($rs_del->Fields("thumbimagefilename"));
        
        $str_large_filename_new = "";
        $str_thumb_filename_new = "";
                
        //print "LARGE: ".$str_large_file;
        //print "<br/>THUMB: ".$str_thumb_file; exit;
	if($rs_del->Fields("thumbimagefilename") != "")
	{
            $str_thumb_filename_new = GetUniqueFileName()."_photo_t.".getextension($rs_del->Fields("thumbimagefilename"));
            $str_thumb_file_new = $str_dir.$int_pkid."/".$str_thumb_filename_new;
            
            if(strtoupper(getextension($rs_del->Fields("thumbimagefilename"))) == "JPEG" || strtoupper(getextension($rs_del->Fields("thumbimagefilename"))) == "JPG")
            {
                
                //print strtoupper(getextension($rs_del->Fields("largeimagefilename")));exit();
		if(file_exists($str_thumb_file))
		{
//                    print "jpeg thumb";exit;
                        $source_t = imagecreatefromjpeg($str_thumb_file);
                        $rotate_t = imagerotate($source_t,$int_degree,0);
                        imagejpeg($rotate_t, $str_thumb_file_new);
                        DeleteFile($str_thumb_file);
//                        header("Location: item_detail_list.php");
                        imagejpeg($rotate_t, $str_thumb_file_new);
		} 
            }
            
            else if(strtoupper(getextension($rs_del->Fields("thumbimagefilename"))) == "GIF")
            {
                //print strtoupper(getextension($rs_del->Fields("largeimagefilename")));exit();
		if(file_exists($str_thumb_file))
		{
                        $source_t = imagecreatefromgif($str_thumb_file);
                        $rotate_t = imagerotate($source_t,$int_degree,0);
                        imagegif($rotate_t, $str_thumb_file_new);
                        DeleteFile($str_thumb_file);
                        imagegif($rotate_t, $str_thumb_file_new);
		}
            }
            else if(strtoupper(getextension($rs_del->Fields("thumbimagefilename"))) == "PNG")
            {
                //print strtoupper(getextension($rs_del->Fields("largeimagefilename")));exit();
           // print getextension($rs_del->Fields("largeimagefilename"));exit();
		if(file_exists($str_thumb_file))
		{
                        $source_t = imagecreatefrompng($str_thumb_file);
                        $rotate_t = imagerotate($source_t,$int_degree,0);
                        imagepng($rotate_t, $str_thumb_file_new);
                        DeleteFile($str_thumb_file);
                        imagepng($rotate_t, $str_thumb_file_new);
		}
            }
//            else if(strtoupper(getextension($rs_del->Fields("largeimagefilename"))) == "JPEG")
//            {
//           // print getextension($rs_del->Fields("largeimagefilename"));exit();
//		if(file_exists($str_large_file))
//		{
//                        $source = imagecreatefrompng($str_large_file);
//                        $rotate = imagerotate($source,$int_degree, 0);
//                        DeleteFile($str_large_file);
//                        imagepng($rotate, $str_large_file);
//		}
//		if(file_exists($str_thumb_file))
//		{
//                        $source = imagecreatefrompng($str_thumb_file);
//                        $rotate = imagerotate($source,$int_degree,0);
//                        DeleteFile($str_thumb_file);
//                        imagepng($rotate, $str_thumb_file);
//		}
//            }
            
            
            
           
	}
        
        
        if($rs_del->Fields("largeimagefilename") != "")
	{
            $str_large_filename_new = GetUniqueFileName()."_photo_l.".getextension($rs_del->Fields("largeimagefilename"));
            $str_large_file_new = $str_dir.$int_pkid."/".$str_large_filename_new;
            
            if(strtoupper(getextension($rs_del->Fields("largeimagefilename"))) == "JPEG" || strtoupper(getextension($rs_del->Fields("largeimagefilename"))) == "JPG")
            {
                
            //print strtoupper(getextension($rs_del->Fields("largeimagefilename")));exit();
		if(file_exists($str_large_file))
		{
                        $source = imagecreatefromjpeg($str_large_file);
                        $rotate = imagerotate($source,$int_degree,0);
                        imagejpeg($rotate, $str_large_file_new);
                        DeleteFile($str_large_file);
//                        header("Location: item_detail_list.php");
                        imagejpeg($rotate, $str_large_file_new);
		}
		
            }
            
            else if(strtoupper(getextension($rs_del->Fields("largeimagefilename"))) == "GIF")
            {
                //print strtoupper(getextension($rs_del->Fields("largeimagefilename")));exit();
		if(file_exists($str_large_file))
		{
//                    print "gif";exit;
                        $source = imagecreatefromgif($str_large_file);
                        $rotate = imagerotate($source,$int_degree, 0);
                        imagegif($rotate, $str_large_file_new);
                        DeleteFile($str_large_file);
                        imagegif($rotate, $str_large_file_new);
		}
		
            }
            else if(strtoupper(getextension($rs_del->Fields("largeimagefilename"))) == "PNG")
            {
                //print strtoupper(getextension($rs_del->Fields("largeimagefilename")));exit();
           // print getextension($rs_del->Fields("largeimagefilename"));exit();
		if(file_exists($str_large_file))
		{
//                    print "png";exit;
                        $source = imagecreatefrompng($str_large_file);
                        $rotate = imagerotate($source,$int_degree, 0);
                        imagepng($rotate, $str_large_file_new);
                        DeleteFile($str_large_file);
                        imagepng($rotate, $str_large_file_new);
		}
		
            }
//            else if(strtoupper(getextension($rs_del->Fields("largeimagefilename"))) == "JPEG")
//            {
//           // print getextension($rs_del->Fields("largeimagefilename"));exit();
//		if(file_exists($str_large_file))
//		{
//                        $source = imagecreatefrompng($str_large_file);
//                        $rotate = imagerotate($source,$int_degree, 0);
//                        DeleteFile($str_large_file);
//                        imagepng($rotate, $str_large_file);
//		}
//		if(file_exists($str_thumb_file))
//		{
//                        $source = imagecreatefrompng($str_thumb_file);
//                        $rotate = imagerotate($source,$int_degree,0);
//                        DeleteFile($str_thumb_file);
//                        imagepng($rotate, $str_thumb_file);
//		}
//            }
           
	}
        
        # Update Query
        $str_query_update = "";
        $str_query_update = "UPDATE tr_photoalbum_detail SET thumbimagefilename = '".$str_thumb_filename_new."', largeimagefilename = '".$str_large_filename_new."' WHERE photopkid = ".$int_id." AND photoalbumpkid = ".$int_pkid;
        //print $str_query_update; exit;
        ExecuteQuery($str_query_update);
//} 
//
//else if($str_phototype == "rotate_photo2")
//{
//	$str_query_select="SELECT photofilename2, photofilenamelarge2 FROM t_model WHERE modelpkid=".$int_pkid;
//	$rs_del=GetRecordSet($str_query_select);
//	
//	if($rs_del->eof()==true)
//	{
//		CloseConnection();
//		Redirect("item_list.php?msg=F&type=E&#ptop");
//		exit();
//        }
//	$str_dir="";
//	$str_dir=$UPLOAD_IMG_FILE_PATH;
//	if(!file_exists($str_dir))
//	{
//		CloseConnection();
//		Redirect("item_edit.php?msg=F&type=E&pkid=".$int_pkid.$str_filter."&#ptop");
//		exit();
//	}
//	$str_large_file="";
//	$str_thumb_file="";
//	$str_large_file=$str_dir.$int_pkid."/".trim($rs_del->Fields("photofilenamelarge2"));
//	$str_thumb_file=$str_dir.$int_pkid."/".trim($rs_del->Fields("photofilename2"));
//	if($rs_del->Fields("photofilenamelarge2")!="")
//	{
//            if(strtoupper(getextension($rs_del->Fields("photofilenamelarge2"))) == "JPG")
//            {
////            print getextension($rs_del->Fields("photofilenamelarge"));exit();
//		if(file_exists($str_large_file))
//		{
//                        $source = imagecreatefromjpeg($str_large_file);
//                        $rotate = imagerotate($source,$int_degree, 0);
//                        DeleteFile($str_large_file);
//                        imagejpeg($rotate, $str_large_file);
//		}
//		if(file_exists($str_thumb_file))
//		{
//                        $source = imagecreatefromjpeg($str_thumb_file);
//                        $rotate = imagerotate($source,$int_degree,0);
//                        DeleteFile($str_thumb_file);
//                        imagejpeg($rotate, $str_thumb_file);
//		}
//            }
//            else if(strtoupper(getextension($rs_del->Fields("photofilenamelarge2"))) == "GIF")
//            {
////            print $int_degree;exit();
//		if(file_exists($str_large_file))
//		{
//                        $source = imagecreatefromgif($str_large_file);
//                        $rotate = imagerotate($source,$int_degree, 0);
//                        DeleteFile($str_large_file);
//                        imagegif($rotate, $str_large_file);
//		}
//		if(file_exists($str_thumb_file))
//		{
//                        $source = imagecreatefromgif($str_thumb_file);
//                        $rotate = imagerotate($source,$int_degree,0);
//                        DeleteFile($str_thumb_file);
//                        imagegif($rotate, $str_thumb_file);
//		}
//            }
//            else if(strtoupper(getextension($rs_del->Fields("photofilenamelarge2"))) == "PNG")
//            {
////            print getextension($rs_del->Fields("photofilenamelarge"));exit();
//		if(file_exists($str_large_file))
//		{
//                        $source = imagecreatefrompng($str_large_file);
//                        $rotate = imagerotate($source,$int_degree, 0);
//                        DeleteFile($str_large_file);
//                        imagepng($rotate, $str_large_file);
//		}
//		if(file_exists($str_thumb_file))
//		{
//                        $source = imagecreatefrompng($str_thumb_file);
//                        $rotate = imagerotate($source,$int_degree,0);
//                        DeleteFile($str_thumb_file);
//                        imagepng($rotate, $str_thumb_file);
//		}
//            }
//           
//	}
//
//
//}
//else if($str_phototype == "rotate_photo3")
//{
//	$str_query_select="SELECT photofilename3, photofilenamelarge3 FROM t_model WHERE modelpkid=".$int_pkid;
//	$rs_del=GetRecordSet($str_query_select);
//	
//	if($rs_del->eof()==true)
//	{
//		CloseConnection();
//		Redirect("item_list.php?msg=F&type=E&#ptop");
//		exit();
//        }
//	$str_dir="";
//	$str_dir=$UPLOAD_IMG_FILE_PATH;
//	if(!file_exists($str_dir))
//	{
//		CloseConnection();
//		Redirect("item_edit.php?msg=F&type=E&pkid=".$int_pkid.$str_filter."&#ptop");
//		exit();
//	}
//	$str_large_file="";
//	$str_thumb_file="";
//	$str_large_file=$str_dir.$int_pkid."/".trim($rs_del->Fields("photofilenamelarge3"));
//	$str_thumb_file=$str_dir.$int_pkid."/".trim($rs_del->Fields("photofilename3"));
//	if($rs_del->Fields("photofilenamelarge3")!="")
//	{
//            if(strtoupper(getextension($rs_del->Fields("photofilenamelarge3"))) == "JPG")
//            {
////            print getextension($rs_del->Fields("photofilenamelarge"));exit();
//		if(file_exists($str_large_file))
//		{
//                        $source = imagecreatefromjpeg($str_large_file);
//                        $rotate = imagerotate($source,$int_degree, 0);
//                        DeleteFile($str_large_file);
//                        imagejpeg($rotate, $str_large_file);
//		}
//		if(file_exists($str_thumb_file))
//		{
//                        $source = imagecreatefromjpeg($str_thumb_file);
//                        $rotate = imagerotate($source,$int_degree,0);
//                        DeleteFile($str_thumb_file);
//                        imagejpeg($rotate, $str_thumb_file);
//		}
//            }
//            else if(strtoupper(getextension($rs_del->Fields("photofilenamelarge3"))) == "GIF")
//            {
////            print $int_degree;exit();
//		if(file_exists($str_large_file))
//		{
//                        $source = imagecreatefromgif($str_large_file);
//                        $rotate = imagerotate($source,$int_degree, 0);
//                        DeleteFile($str_large_file);
//                        imagegif($rotate, $str_large_file);
//		}
//		if(file_exists($str_thumb_file))
//		{
//                        $source = imagecreatefromgif($str_thumb_file);
//                        $rotate = imagerotate($source,$int_degree,0);
//                        DeleteFile($str_thumb_file);
//                        imagegif($rotate, $str_thumb_file);
//		}
//            }
//            else if(strtoupper(getextension($rs_del->Fields("photofilenamelarge3"))) == "PNG")
//            {
////            print getextension($rs_del->Fields("photofilenamelarge"));exit();
//		if(file_exists($str_large_file))
//		{
//                        $source = imagecreatefrompng($str_large_file);
//                        $rotate = imagerotate($source,$int_degree, 0);
//                        DeleteFile($str_large_file);
//                        imagepng($rotate, $str_large_file);
//		}
//		if(file_exists($str_thumb_file))
//		{
//                        $source = imagecreatefrompng($str_thumb_file);
//                        $rotate = imagerotate($source,$int_degree,0);
//                        DeleteFile($str_thumb_file);
//                        imagepng($rotate, $str_thumb_file);
//		}
//            }
//           
//	}
//
//
//}
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file
Synchronize($int_pkid);
WriteXml();
WriteXmlSub($int_pkid);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to page	
CloseConnection();
//print "exit";exit;
Redirect("item_detail_list.php?type=S&msg=DI&ref=1&pkid=".$int_pkid.$str_filter."#rotate_id");
exit();
#----------------------------------------------------------------------------------------------------------------------------------------------------------------	
?>
