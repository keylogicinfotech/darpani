<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
include "../../includes/lib_file_system.php";
//include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_catpkid = 0;
if(isset($_GET['catid']))
{
    $int_catpkid = $_GET['catid'];
}

if($int_catpkid <= 0 || !is_numeric($int_catpkid) || $int_catpkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F");
    exit();
}

//print $int_catpkid; exit;
$int_masterpkid = 0;
if(isset($_GET['subcatid']))
{
    $int_masterpkid=$_GET['subcatid'];
}

if($int_masterpkid <= 0 || !is_numeric($int_masterpkid) || $int_masterpkid == "")
{
    CloseConnection();
    Redirect("item_subcat_list.php?type=E&msg=F&catid=".$int_catpkid);
    exit();
}

$str_filter = "";
$str_filter = "&catid=".$int_catpkid."&subcatid=".$int_masterpkid."&#ptop";

$int_pkid = 0;
if(isset($_GET['pkid']))
{
    $int_pkid = $_GET['pkid'];
}
//print $int_pkid; exit;
if($int_pkid <= 0 || !is_numeric($int_pkid) || $int_pkid == "")
{
    CloseConnection();
    Redirect("item_subcat_photo_list.php?type=E&msg=F".$str_filter);
    exit();
}

#----------------------------------------------------------------------------------------------------
#Select query to get the details from table
$str_query_select = "";
$str_query_select = "SELECT thumbphotofilename,largephotofilename FROM ".$STR_DB_TABLE_NAME_SUBCAT_PHOTO." WHERE pkid=".$int_pkid;
$rs_list = GetRecordset($str_query_select);
//print $rs_list->Count(); exit;
if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_subcat_photo_list.php?type=E&msg=F".$str_filter);
    exit();
}

$str_thumb_file_name = "";
$str_large_file_name = "";

$str_thumb_file_name = $rs_list->Fields("thumbphotofilename");
$str_large_file_name = $rs_list->Fields("largephotofilename");
#-----------------------------------------------------------------------------------------------------
$str_dir = "";
$str_dir = $UPLOAD_IMG_PATH;
//print $str_dir; exit;
if(trim($str_thumb_file_name) != "")
{
    DeleteFile($UPLOAD_IMG_PATH.trim($str_thumb_file_name));
}
if(trim($str_large_file_name) != "")
{
    DeleteFile($UPLOAD_IMG_PATH.trim($str_large_file_name));
}
#-----------------------------------------------------------------------------------------------------
# Delete Query 
$str_query_delete = "";
$str_query_delete = "DELETE FROM ".$STR_DB_TABLE_NAME_SUBCAT_PHOTO." WHERE pkid=".$int_pkid;
//print $str_query_delete;exit;
ExecuteQuery($str_query_delete);
#-----------------------------------------------------------------------------------------------------
# Write to xml file
//WriteXml();
#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_subcat_photo_list.php?type=S&msg=D".$str_filter);
exit();
#----------------------------------------------------------------------------------------------------- 
?>
