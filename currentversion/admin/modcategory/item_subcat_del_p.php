<?php
/*
Module Name:- modstore
File Name  :- item_cat_del_p.php
Create Date:- 04-FEB-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$str_filter = "";
$int_masterpkid = 0;
if(isset($_GET['masterpkid']))
{
    $int_masterpkid = $_GET['masterpkid'];
}
//print $int_masterpkid; exit;
if($int_masterpkid <= 0 || !is_numeric($int_masterpkid) || $int_masterpkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F".$str_filter);
    exit();
}

$int_pkid = 0;
if(isset($_GET['pkid']))
{
    $int_pkid = $_GET['pkid'];
}
if($int_pkid <= 0 || !is_numeric($int_pkid) || $int_pkid == "")
{
    CloseConnection();
    Redirect("item_subcat_list.php?type=E&msg=F&catid=".$int_masterpkid."".$str_filter);
    exit();
}


#----------------------------------------------------------------------------------------------------
    #check wheather child record exists
    $str_query_select="";
    $str_query_select="SELECT pkid FROM ".$STR_DB_TABLE_NAME_STORE." WHERE subcatpkid=".$int_pkid;
    //print $str_query_select; //exit;
    $rs_list_check=GetRecordSet($str_query_select);

    if(!$rs_list_check->eof())
    {
        CloseConnection();
        Redirect("item_subcat_list.php?type=E&msg=F&catid=".$int_masterpkid."".$str_filter);
        exit();
    }

    #select query 
    $str_query_select = "";
    $str_query_select = "SELECT subcattitle FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid=". $int_pkid;
    //print $str_query_select; exit;
    $rs_list = GetRecordSet($str_query_select);

    if($rs_list->eof())
    {
        CloseConnection();
        Redirect("item_subcat_list.php?type=E&msg=F&catid=".$int_masterpkid."".$str_filter);
        exit();
    }

    # Delete Query
    $str_query_delete="";
    $str_query_delete="DELETE FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid=" .$int_pkid;
    //print $str_query_delete; exit;
    ExecuteQuery($str_query_delete);
    #----------------------------------------------------------------------------------------------------
    #Close connection and redirect to item_cat_list.php page	
    CloseConnection();
    Redirect("item_subcat_list.php?type=S&msg=D&catid=".$int_masterpkid."&#ptop");
    exit();
    #------------------------------------------------------------------------------------------------------------

?>