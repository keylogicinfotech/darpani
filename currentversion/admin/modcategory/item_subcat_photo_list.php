<?php
/*
    Module Name:- Modstore
    File Name  :- item_photo_list.php
    Create Date:- 08-Sep-2006
    Intially Create By :- 0014
    Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
    include "../../includes/validatesession.php";
    include "../../includes/configuration.php";
    include "item_config.php";
    include "../../includes/lib_data_access.php";
    include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
#initialize variables	
$str_filter = ""; 
$str_filter = "&#ptop";

#----------------------------------------------------------------------------------------------------
# get filter data
/*$int_userpkid = "";
if(isset($_GET["uid"]) && trim($_GET["uid"])!="" )
{
    $int_userpkid = trim($_GET["uid"]);
}*/
        //print $int_mid; exit;    
#-----------------------------------------------------------------------------------------------------
#getting query string data
$str_filter = "";

$int_catpkid = "";
if(isset($_GET['catid']))
{
    $int_catpkid = trim($_GET['catid']);
}
if($int_catpkid == "" || !is_numeric($int_catpkid) || $int_catpkid < 0)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}

$str_filter = "&catid=".$int_catpkid."&#ptop";
//print $str_filter; exit;

$int_masterpkid = "";
if(isset($_GET['subcatid']))
{
    $int_masterpkid = trim($_GET['subcatid']);
}
if($int_masterpkid == "" || !is_numeric($int_masterpkid) || $int_masterpkid < 0)
{
    CloseConnection();
    Redirect("item_subcat_list.php?msg=F&type=E".$str_filter);
    exit();
}

#select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_SUBCAT_PHOTO." WHERE masterpkid=".$int_masterpkid." AND catpkid=".$int_catpkid." ORDER BY displayorder DESC";
//print $str_query_select; exit;
$rs_list = GetRecordset($str_query_select);
	
$str_query_select = "";
$str_query_select = "SELECT subcattitle FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid=".$int_masterpkid;	
$rs_list_photo = GetRecordset($str_query_select);
if($rs_list_photo->EOF()==true)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}
#--------------------------------------------------------------------------------------------------------------
#initializing variables
    $str_visible = "";
    $str_ptitle = "";
    $str_purl = "";
    $str_preview = "YES";
    $str_access = "NO";
#getting query string datas	
    if(isset($_GET["visible"]))
    { 
        $str_visible=trim($_GET["visible"]);
    }
    if(isset($_GET["ptitle"]))
    { 
        $str_ptitle=trim($_GET["ptitle"]);
    }
    if(isset($_GET["purl"]))
    { 
        $str_purl=trim($_GET["purl"]);
    }
    if (isset($_GET["preview"]))
    {
        $str_preview = trim($_GET["preview"]);
    }
    if (isset($_GET["access"]))
    {
        $str_access = trim($_GET["access"]);
    }
    
    
#------------------------------------------------------------------------------------------

#   Initialization of variables used for message display.   
    $str_type = "";
    $str_message = "";
    $str_mode = "";
    
    if($str_mode=="YES") { $str_md="Visible"; }  else { $str_md="Invisible"; }
    
    if(isset($_GET['mode']))
    {
        $str_mode=trim($_GET['mode']);
    }
# Get message type.
    if(isset($_GET['type']))
    {
        switch(trim($_GET['type']))
	{
            case("S"): $str_type = "S"; break;
            case("E"): $str_type = "E"; break;
            case("W"): $str_type = "W"; break;
	}
    }
# Get message text.
    if($str_mode=="YES") { $str_md="VISIBLE"; }  else { $str_md="INVISIBLE"; }
    if(isset($_GET['msg']))
    {
        switch(trim($_GET['msg']))
	{
            case("F"):$str_message =  $STR_MSG_ACTION_INFO_MISSING; break;
            case("S"):$str_message = $STR_MSG_ACTION_ADD; break;
            case("D"):$str_message = $STR_MSG_ACTION_DELETE; break;
            case("U"):$str_message = $STR_MSG_ACTION_EDIT; break;
            case("O"):$str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
            case("SF"):$str_message = $STR_MSG_ACTION_SET_AS_FRONT; break;
            case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_md; break;
            case("I"): 	$str_message =$STR_MSG_ACTION_INVALID_FILE_EXT; break; 
            case("W"):$str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;	
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_PAGE_SUBCAT_PHOTO_LIST);?> [<?php print $rs_list_photo->Fields("subcattitle"); ?>]</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> 
<a name="ptop" id="ptop"></a>
<div class="container content_bg">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <a href="./item_subcat_list.php?<?php print $str_filter; ?>" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE_SUBCAT); ?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_SUBCAT); ?></a>
            </div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE_SUBCAT_PHOTO_LIST);?> [<?php print $rs_list_photo->Fields("subcattitle"); ?>]</h3></div>
    </div><hr> 
    <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <b><a class="accordion-toggle collapsed" data-toggle="collapse" title="<?php print($STR_HOVER_ADD); ?>" data-parent="#accordion"  href="#collapseOne" aria-expanded="false"><?php print($STR_TITLE_ADD); ?></a></b>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
                        <div class="panel-body">
                            <form id="frm_add" name="frm_add" action="item_subcat_photo_add_p.php" method="POST" onSubmit="return frm_add_validate()" enctype="multipart/form-data">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Upload Photo</label><span class="text-help-form"> *</span>
                                            <span class="text-help-form">(<?php print($STR_MSG_IMG_FILE_TYPE);?> <?php //print($STR_IMG_UPLOAD_MSG);?>) </span>
                                            <input type="file" name="fileimage" maxlength="255" size="70">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Visible</label><span class="text-help-form"> *</span>
                                            <span class="text-help-form">(<?php print($STR_MSG_VISIBLE)?>)</span>
                                            <select name="cbo_visible" class="form-control input-sm">
                                                <option value="YES">YES</option>
                                                <option value="NO">NO</option>
                                            </select>
                                        </div>
                                    </div>                                            
                                </div>
                                <input type="hidden" name="cbo_preview" value="YES">
                                <input type="hidden" name="cbo_access" value="YES">
                                <input type="hidden" name="hdn_masterpkid" value="<?php print($int_masterpkid); ?>">
                                <input type="hidden" name="hdn_catpkid" value="<?php print($int_catpkid); ?>">

                                <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <form name="frm_list" action="./item_subcat_photo_order_p.php" method="POST" onSubmit="return frm_list_check_displayorder();">
                    <table class="table table-striped table-bordered ">
                    <thead align="center">
                        <tr>
                            
                            <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                            <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                            <th width="6%"><?php print $STR_TABLE_COLUMN_NAME_SET_AS_FRONT; ?></th>
                            <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_ORDER; ?></th>
                            <th width="5%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>                        
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($rs_list->EOF()==true)  {  ?>
                           <tr><td colspan="8" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                        <?php } else { 
                            $int_cnt = 1; 
                            while(!$rs_list->EOF()==true) { ?>
                            <tr>
                                <td style="vertical-align: middle;" align="center"><?php print($int_cnt);?></td>
                                <td align="left">
                                    <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list->fields("pkid")); ?>">
                                    <?php /*?><a href="#" onClick="return show_details('mdl_ps_photo_large_image.php?pkid=<?php print($int_pkid);?>&tpkid=<?php print($rs_list->fields("pkid"));?>')"><?php */?>
                                    <img  border="0" src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("thumbphotofilename"));?>" title="Click to view actual size image" alt="Image" class="img-responsive"></a>
                                    <div class="modal fade f-pop-up-<?php print($rs_list->fields("pkid")); ?> " align="center" role="dialog" aria-labelledby="" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                    <img src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("largephotofilename"));?>" class="img-responsive" alt="Image" title="Image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>	
                                </td>
                                <?php
                                $str_class = "";
                                if(strtoupper($rs_list->fields("setasfront")) == "YES") { $str_class="alert-success"; }
                                else { $str_class = "alert-danger"; } ?>

                                <td class="<?php print $str_class; ?>" align="center">
                                   <?php if(strtoupper($rs_list->fields("setasfront"))=="NO") {?>
                                        <a href="item_subcat_photo_setasfront_p.php?pkid=<?php print($rs_list->fields("pkid"))?>&catid=<?php print($rs_list->fields("catpkid"))?>&subcatid=<?php print($rs_list->fields("masterpkid"))?>" title="Click to set as default" class="<?php //print($str_class); ?> alert-link"><?php print($rs_list->fields("setasfront")) ?></a>
                                   <?php } else { print($rs_list->fields("setasfront")); } ?>
                                </td>
                                <td style="vertical-align: middle;" align="center">
                                   <input type="text" name="txt_displayorder<?php print($int_cnt);?>" size="5" maxlength="5" value="<?php print($rs_list->fields("displayorder"));?>" class="form-control input-sm text-align" style="text-align:center;">
                                   <input type="hidden"  name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>">
                                </td>
                                <td align="center" style="vertical-align: middle;" >
                                <?php
                                    $str_image="";
                                    if(strtoupper($rs_list->fields("visible"))=="YES")
                                    {   $str_image = $STR_LINK_ICON_PATH_VISIBLE;
                                        $str_class = "btn btn-warning btn-xs";
                                        $str_title = $STR_HOVER_VISIBLE;
                                    }
                                    else
                                    {   $str_image = $STR_LINK_ICON_PATH_INVISIBLE;
                                        $str_class = "btn btn-default active btn-xs";
                                        $str_title = $STR_HOVER_INVISIBLE;
                                    } ?>
                                    <p><a class="<?php print($str_class); ?>" href="item_subcat_photo_visible_p.php?pkid=<?php print($rs_list->fields("pkid"))?>&catid=<?php print($rs_list->fields("catpkid"))?>&subcatid=<?php print($rs_list->fields("masterpkid"))?><?php //print($str_filter);?>" title="<?php print($str_title);?>"><?php print($str_image);?></a></p>
                                    <p><a class="btn btn-success btn-xs" href="item_subcat_photo_edit.php?pkid=<?php print($rs_list->fields("pkid"))?>&catid=<?php print($rs_list->fields("catpkid"))?>&subcatid=<?php print($rs_list->fields("masterpkid"))?><?php //print($str_filter);?>" title="<?php print($STR_HOVER_EDIT)?>"><?php print $STR_LINK_ICON_PATH_EDIT; ?></a></p>
                                    <p><a class="btn btn-danger btn-xs" href="item_subcat_photo_del_p.php?pkid=<?php print($rs_list->fields("pkid"))?>&catid=<?php print($rs_list->fields("catpkid"))?>&subcatid=<?php print($rs_list->fields("masterpkid"))?><?php //print($str_filter);?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE)?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a></p>
                                </td>
                            </tr>
                            <?php $rs_list->movenext(); 
                            $int_cnt = $int_cnt +1;  
                            } ?>
                            <tr>
                                <td>
                                    <input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>">
                                </td>
                                <td><input type="hidden" id="hdn_masterpkid" name="hdn_masterpkid" value="<?php print($int_masterpkid); ?>">
                                    <input type="hidden" id="hdn_catpkid" name="hdn_catpkid" value="<?php print($int_catpkid); ?>">
                                    <?php /* ?><input type="hdn_userpkid" id="" name="hdn_userpkid" value="<?php print($int_userpkid); ?>"><?php */ ?></td>
                                <td></td>
                                <td><?php print DisplayFormButton("SAVE",0); ?></td>
                                <td></td>
                                <?php /*?><td class="text-align"><input type="button" class="btn btn-success" name="shop_btn" value="Change" title="Click to change 'Display on Home' setting" onClick="return displayonhome_click();"></td><?php */?>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
<?php include "../../includes/include_files_admin.php"; ?>
<script language="JavaScript" src="./item_subcat_photo_list.js" type="text/javascript"></script>
</body></html>