/*
	Module Name:- modpromogallery
	File Name  :- prg_photo_add.js
	Create Date:- 22-FEB-2006
	Intially Create By :- 0022
	Update History:
*/

function frm_photo_add_validateform()
{
	with(document.frm_photo_add)
	{
  		if(isEmpty(fileimage.value))
		{
			alert("Please select photograph.");
			fileimage.select();
			fileimage.focus();
			return false;
		}

//                if(document.getElementById("uploadimages").files.length < 1)
//		{
//		   alert("Please select image");
//		   return false;
//		}
		if(trim(fileimage.value) != "")
		{
			if(checkExt(trim(fileimage.value))==false)
			{
				fileimage.select();
				fileimage.focus();
				return false;
			}
		}
		if(trim(txt_purl.value)!="")
		{
			if(isValidUrl(trim(txt_purl.value))==false)
			{
				txt_purl.focus();	
				txt_purl.select();	
				return false;
			}
		}
		if(cbo_visible.value == "")
		{
			alert("Please set value for visible.");
			cbo_visible.focus();			
			return false;
		}
	}
	return true;
}


