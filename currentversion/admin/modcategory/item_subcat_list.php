<?php
/*
Module Name:- modstore
File Name  :- item_cat_list.php
Create Date:- 04-FEB-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
$str_filter = "";
$str_filter = "&#ptop";

$str_seo_title = "";
$str_seo_keyword = "";
$str_seo_desc = "";
$str_new = "";


$int_pkid = 0;
if(isset($_GET["catid"]))
{ 
    $int_pkid = $_GET["catid"]; 
}
if($int_pkid == "" || $int_pkid <= 0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}

$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME." WHERE catpkid=".$int_pkid.$STR_DB_TABLE_NAME_ORDER_BY;
$rs_list_cat = GetRecordSet($str_query_select);


# Select Query
$str_query_select = "";
/*$str_query_select = "SELECT a.catpkid,a.title cattitle,a.visible catvisible,a.displayorder catorder,";
$str_query_select .= "b.subcatpkid,b.subcattitle subtitle,b.visible subvisible,b.displayorder suborder";
$str_query_select .= " FROM ".$STR_DB_TABLE_NAME_CAT." a";
$str_query_select .= " LEFT JOIN ".$STR_DB_TABLE_NAME_SUBCAT." b";
$str_query_select .= " ON b.catpkid=a.catpkid";
$str_query_select .= " ORDER BY a.displayorder DESC,a.title,a.catpkid,";
$str_query_select .= "b.displayorder DESC,b.subcattitle,b.subcatpkid";*/
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE catpkid=".$int_pkid.$STR_DB_TABLE_NAME_ORDER_BY_SUBCAT;
//print $str_query_select;exit;
$rs_list = GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------------
# get Query String Data
$str_title = "";
$str_mode = "";
$str_cattitle = "";                                                                                                                                                                                                                                                                                       
$str_cattype = "";
$str_visible = "YES";

if (isset($_GET["title"]))
{
    $str_title = trim($_GET["title"]);
}
if (isset($_GET["mode"]))
{
    $str_mode = trim($_GET["mode"]);
}
if (isset($_GET["visible"]))
{
    $str_visible = trim($_GET["visible"]);
}
if (isset($_GET["cattitle"]))
{
    $str_cattitle = trim($_GET["cattitle"]);
}
if (isset($_GET["cattype"]))
{
    $str_cattype = trim($_GET["cattype"]);
}
#------------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING;	break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("SS"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("SD"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("SU"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
        case("SV"): $str_message = " '" . MyHtmlEncode(MakeStringShort(RemoveQuote($str_title),30)) . "' changed to '". $str_mode . "' successfully.";	break;
        case("DU"): $str_message = $STR_MSG_ACTION_TITLE_ALREADY_EXIST_CAT; break;
        case("SDU"): $str_message = $STR_MSG_ACTION_TITLE_ALREADY_EXIST_SUBCAT;break;
        case("DN"); $str_message = $STR_MSG_ACTION_DISPLAY_AS_NEW; break;
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE_SUBCAT);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../css/admin.css" rel="stylesheet">
</head>
<body> <a name="ptop" id="ptop"></a>
    <div class="container">
	<?php include($STR_ADMIN_HEADER_PATH); ?>
	<div class="row padding-10">
            <div class="col-md-6 col-sm-6 col-xs-12 button_space">
                <div class="btn-group" role="group" aria-label="...">
                    <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
                    </div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE_SUBCAT);?> [<?php print RemoveQuote($rs_list_cat->Fields("title")); ?>]</h3></div>
	</div><hr> 
	<?php if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
	<div class="row padding-10">
            <div class="col-md-12">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <h4 class="panel-title">
                            <span>
                                <a class="accordion-toggle collapsed " data-toggle="collapse" data-parent="#accordion" title="<?php print($STR_TITLE_ADD); ?>" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>
                            </span>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" >
                            <div class="panel-body">
                                <form name="frm_add" action="item_subcat_add_p.php" method="POST" onSubmit="return frm_add_validate();">
                                    <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Title</label><span class="text-help-form"> *</span> 
                                                <input type="text" id="txt_title"  name="txt_title"  value="<?php print(MyHtmlEncode($str_cattitle)); ?>" maxlength="255" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>">
                                            </div>
                                        
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Description</label><span class="text-help-form"> </span> 
                                                <textarea cols="100" rows="5" name="ta_desc" id="ta_desc" class="form-control input-sm" tabindex="2" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>"><?php //print($str_description);?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Display As New</label><span class="text-help-form"> *</span>
                                                <span class="text-help-form"> (<?php print($STR_MSG_NEW);?>)</span>
                                                <select id="cbo_new" name="cbo_new" class="form-control input-sm">
                                                    <option value="NO" <?php print(CheckSelected("NO",$str_new)); ?>>NO</option>
                                                    <option value="YES" <?php print(CheckSelected("YES",$str_new));?>>YES</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Visible</label><span class="text-help-form"> *</span>
                                                <span class="text-help-form"> (<?php print($STR_MSG_VISIBLE);?>)</span>
                                                <select id="cbo_visible" name="cbo_visible" class="form-control input-sm">
                                                    <option value="YES" <?php print(CheckSelected("YES",$str_visible));?>>YES</option>
                                                    <option value="NO" <?php print(CheckSelected("NO",$str_visible)); ?>>NO</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>SEO Title</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_seo_title" id="txt_seo_title" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_SEO_TITLE; ?>"  maxlength="255" size="60" value="<?php print $str_seo_title; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>SEO Keywords</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_seo_keywords" id="txt_seo_keywords" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_SEO_KEYWORDS; ?>"  maxlength="255" size="60" value="<?php print $str_seo_keyword; ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>SEO Description</label><span class="text-help-form"> </span>
                                                <textarea name="ta_seo_desc" id="ta_seo_desc" rows="5" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>" ><?php print(RemoveQuote($str_seo_desc)); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="masterpkid" name="masterpkid" value="<?php print $int_pkid; ?>" />
                                    <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</div>	
	<div class="table-responsive">
	<form name="frm_list" action="item_subcat_order_p.php" method="POST" onSubmit="return frm_list_check_displayorder();">
            <table class="table table-striped table-bordered ">
        	<thead>
                <tr>
                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                    <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                    <th width="7%"><?php print $STR_TABLE_COLUMN_NAME_MANAGE_PHOTOS; ?></th>
                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_AS_NEW; ?><?php if(!$rs_list->eof()) {?><br/><input type="checkbox" onClick="return checkallnew();" id="chk_allnew" name="chk_allnew" ><?php } ?></th>
                    <th width="6%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_ORDER; ?></th>
                    <th width="9%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>
		</tr>
            </thead>
            <tbody>
            <?php if($rs_list->EOF()==true)  {  ?>
		<tr><td colspan="8" align="center" class="alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
		<?php } else { ?>
                <?php 
                $int_cnt = 1;
                while($rs_list->eof()==false) 
                { ?>
                
                <tr <?php //print $str_class_collapse?>>
                    <td  align="center" >
                        <?php print($int_cnt);?><?php //print($cattype);?>
                        <input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->Fields("subcatpkid"));?>">
                    </td>
                    <td valign="middle">  
                        <label><h4 class="nopadding"><b><?php print($rs_list->Fields("subcattitle"));?></b></h4></label>
                        <?php if(($rs_list->Fields("description") != "" && $rs_list->Fields("description")!= "<br>") || $rs_list->fields("seotitle") != "" || $rs_list->fields("seokeyword") != "" || $rs_list->fields("seodescription") != "") { ?>    
                        <div class="" id="accordion">
                            <p align="right" class="nopadding text-help"><i><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#moredetails<?php print $int_cnt; ?>"><?php print $STR_MSG_CLICK_TO_VIEW_MORE_DETAILS; ?></a></i></p>
                            <div id="moredetails<?php print $int_cnt;?>" class="panel-collapse collapse" align="justify">
                                <?php if($rs_list->Fields("description") != "" && $rs_list->Fields("description")!= "<br>") { ?>
                                    <p align="justify"><span class="text-help">Description:</span>&nbsp;<?php print RemoveQuote($rs_list->Fields("description")); ?></p>
                                <?php } ?>
                                <?php if($rs_list->fields("seotitle") != "") { ?>    
                                    <p align="justify"><span class="text-help">SEO Title:</span>&nbsp;<?php print(RemoveQuote($rs_list->fields("seotitle"))); ?></p>
                                    <?php } ?>
                                    <?php if($rs_list->fields("seokeyword") != "") { ?>    
                                    <p align="justify"><span class="text-help">SEO Keywords:</span>&nbsp;<?php print(RemoveQuote($rs_list->fields("seokeyword"))); ?></p>
                                    <?php } ?>
                                    <?php if($rs_list->fields("seodescription") != "") { ?>    
                                    <p align="justify"><span class="text-help">SEO Description:</span>&nbsp;<?php print(RemoveQuote($rs_list->fields("seodescription"))); ?></p>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                    </td>
                    <td align="center" > 
                        <label><h4 class="nopadding"><a href="item_subcat_photo_list.php?catid=<?php print $rs_list->Fields("catpkid"); ?>&subcatid=<?php print $rs_list->Fields("subcatpkid"); ?>"><i class="fa fa-image  align-bottom" aria-hidden="true"></i></a></h4></label>
                        <?php
                        $str_query_select = "";
                        $str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_SUBCAT_PHOTO." WHERE masterpkid=".$rs_list->Fields("subcatpkid")." AND catpkid=".$rs_list->Fields("catpkid");
                        $rs_list_photo_count = GetRecordSet($str_query_select); 
                        ?>
                        <span class="text-help">(<?php $rs_list_total = GetRecordSet($str_query_select); $int_total_items = $rs_list_total->count(); print $int_total_items;?>)</span>
                    </td>
			<td class="text-center">
                                            <?php
                                            if(trim(strtoupper($rs_list->fields("displayasnew"))) == "YES") 
                                            { 
                                                /*## START - It will change displayasnew field to 'NO' after mentioned day in configuration
                                                
                                                    $str_current_date = "";
                                                    $str_created_date = "";
                                                    $int_time_diff_in_days = 0.00;
                                                    $str_time_diff = "";

                                                    $str_current_date = strtotime(date("Y-m-d H:i:s"));
                                                    $str_created_date = strtotime($rs_list->Fields("createdatetime"));


                                                    $str_time_diff = $str_current_date - $str_created_date;

                                                    $int_time_diff_in_days = $str_time_diff / (60 * 60 *24);
                                                    if($int_time_diff_in_days > $INT_DAYS_DISPLAY_AS_NEW)
                                                    {
                                                        $str_query_update = "";
                                                        $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME." SET displayasnew='NO' WHERE pkid=".$rs_list->Fields("pkid");

                                                        ExecuteQuery($str_query_update);

                                                    }
                                                ## END - It will change displayasnew field to 'NO' after mentioned day in configuration*/
                                            ?>
                                                <?php print $STR_ICON_PATH_NEW; ?>
                                            <?php 
                                            } ?><br/>
                                            <?php 
                                            $str_checked_new = "";
                                            if(strtoupper(trim($rs_list->fields("displayasnew"))) == "YES") {
                                                $str_checked_new = " checked"; }?>  
                                            <input type="checkbox" name="chk_new<?php print($int_cnt);?>" <?php print($str_checked_new); ?>>
                                        </td>
                    <td  align="center">
                        <input type="text" name="txt_displayorder<?php print($int_cnt);?>" maxlength="5" value="<?php print($rs_list->Fields("displayorder"));?>" class="form-control input-sm text-center" />
                    </td>
                    <?php 
                        $str_image = "";
                        if(strtoupper($rs_list->Fields("visible")) == "YES")
                        {   
                            $str_image = $STR_LINK_ICON_PATH_VISIBLE;
                            $str_class = "btn btn-warning btn-xs";
                            $str_title = $STR_HOVER_VISIBLE;
                        }
                        else
                        {   
                            $str_image = $STR_LINK_ICON_PATH_INVISIBLE;
                            $str_class = "btn btn-default active btn-xs";
                            $str_title = $STR_HOVER_INVISIBLE;
                        } ?>
                    <td align="center">
                        <a class="<?php print($str_class); ?>" href="item_subcat_visible_p.php?masterpkid=<?php print $int_pkid; ?>&pkid=<?php print($rs_list->Fields("subcatpkid"));?>" title="<?php print($STR_HOVER_VISIBLE);?>"><?php print($str_image);?></a>
                        <a class="btn btn-success btn-xs" href="item_subcat_edit.php?masterpkid=<?php print $int_pkid; ?>&pkid=<?php print($rs_list->Fields("subcatpkid"));?>" title="<?php print($STR_HOVER_EDIT);?>"><?php print $STR_LINK_ICON_PATH_EDIT; ?></a>
                        <a class="btn btn-danger btn-xs" href="./item_subcat_del_p.php?masterpkid=<?php print $int_pkid; ?>&pkid=<?php print($rs_list->Fields("subcatpkid"));?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE);?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>
                    </td>
                </tr>
                <?php  $int_cnt++; 
                $rs_list->MoveNext();
                } ?>
                <tr> 
                    <td>
                        <input type="hidden" name="hdn_counter" id="hdn_counter" value="<?php print($int_cnt);?>">
                        <input type="hidden" name="hdn_masterpkid" id="hdn_masterpkid" value="<?php print($int_pkid);?>">
                    </td>
                    
                    <td></td>
                    <td></td>
		    <td class="text-center"><button type="button" class="btn btn-success btn-sm" name="new_btn" title="<?php print($STR_HOVER_DISPLAY_AS_NEW); ?>" onClick="return new_click();"><b>Change</b></button></td>
                    <td  align="center" class="text-align"><?php print DisplayFormButton("SAVE",0); ?></td>
                    <td></td>
                </tr>
            <?php } ?>
            </tbody>
	</table> 
    </form>
    </div>	
    <?php include "../../includes/help_for_list.php"; ?>
</div>
 
<?php include "../../includes/include_files_admin.php"; ?>   
    <?php include($STR_ADMIN_FOOTER_PATH); ?>   
<script language="JavaScript" src="item_subcat_list.js" type="text/javascript"></script>
</body></html>
