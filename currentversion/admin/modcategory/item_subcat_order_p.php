<?php
/*
Module Name:- modstore
File Name  :- item_cat_order_p.php
Create Date:- 04-FEB-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
# Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_masterpkid = 0;
if (isset($_POST["hdn_masterpkid"]))
{
    $int_masterpkid = trim($_POST["hdn_masterpkid"]);
}	
if($int_masterpkid == "" || $int_masterpkid < 0 || is_numeric($int_masterpkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
//print $int_masterpkid; exit;

$int_cnt = 0;
if (isset($_POST["hdn_counter"]))
{
    $int_cnt = trim($_POST["hdn_counter"]);
}	
if($int_cnt == "" || $int_cnt < 0 || is_numeric($int_cnt) == false)
{
    CloseConnection();
    Redirect("item_subcat_list.php?msg=F&type=E&catid=".$int_masterpkid."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
# Update Query
for($i=1;$i<$int_cnt;$i++)
{
    if(trim($_POST["txt_displayorder". $i]) != "" && trim($_POST["txt_displayorder". $i]) >=0 && is_numeric(trim($_POST["txt_displayorder". $i]))==true  && trim($_POST["hdn_pkid". $i])!="" && trim($_POST["hdn_pkid". $i])>0 && is_numeric(trim($_POST["hdn_pkid". $i]))==true)
    {
        $str_query_update="";
        $str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_SUBCAT." SET displayorder='" . trim($_POST["txt_displayorder" . $i]) . "' WHERE subcatpkid=" . trim($_POST["hdn_pkid" . $i]);
        //print $str_query_update."<br/>";
        ExecuteQuery($str_query_update);
    }
}
#----------------------------------------------------------------------------------------------------
# Close connection and redirect to it's relevant location	
CloseConnection();
Redirect("item_subcat_list.php?type=S&msg=O&catid=".$int_masterpkid."&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------
?>