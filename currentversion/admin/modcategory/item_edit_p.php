<?php
/*
Module Name:- modPstore
File Name  :- cat_edit_p.php
Create Date:- 20-MARCH-2006
Intially Create By :- 0023
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid = 0;
if (isset($_POST["hdn_pkid"]))
{	
    $int_pkid = trim($_POST["hdn_pkid"]);
}		
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}

$str_title = "";
$str_desc = "";
$str_desc2 = "";
$str_new = "";
$str_seo_title = "";
$str_seo_keyword = "";
$str_seo_desc = "";
$str_type = 0;

if (isset($_POST["txt_title"]))
{
    $str_title = trim($_POST["txt_title"]);
}
if(isset($_POST["ta_desc"]))
{
    $str_desc = trim($_POST["ta_desc"]);
}
if(isset($_POST["ta_desc2"]))
{
    $str_desc2 = trim($_POST["ta_desc2"]);
}

if (isset($_POST["cbo_new"]))
{
    $str_new = trim($_POST["cbo_new"]);
}
if(isset($_POST["txt_seo_title"]) && trim($_POST["txt_seo_title"])!="")
{
    $str_seo_title = trim($_POST["txt_seo_title"]);
}
if(isset($_POST["txt_seo_keywords"]) && trim($_POST["txt_seo_keywords"])!="")
{
    $str_seo_keywords = trim($_POST["txt_seo_keywords"]);
}
if(isset($_POST["ta_seo_desc"]))
{
    $str_seo_desc = trim($_POST["ta_seo_desc"]);
}
#----------------------------------------------------------------------------------------------------
#Validation Check

if($str_title == "")
{
    CloseConnection();
    Redirect("item_edit.php?msg=F&type=E&catflag=".$str_cat_flag."&pkid=". $int_pkid."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
# Duplication Check
$str_query_select = "";
$str_query_select = "SELECT title FROM ".$STR_DB_TABLE_NAME." WHERE catpkid!=".$int_pkid." AND title= '" . ReplaceQuote($str_title) . "'";
$rs_list_check_duplicate = GetRecordSet($str_query_select);

if(!$rs_list_check_duplicate->eof())
{
    CloseConnection();
    Redirect("item_edit.php?msg=DU&type=E&pkid=". $int_pkid."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
# Update Query
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME." SET title='" .  ReplaceQuote($str_title) . "', ";
$str_query_update .= " description = '" . ReplaceQuote($str_desc) . "',";
$str_query_update .= " description2 = '" . ReplaceQuote($str_desc2) . "',";
$str_query_update .= " displayasnew = '" . ReplaceQuote($str_new) . "',";
$str_query_update .= " lastupdatedate = '" . date("Y-m-d") . "',";
$str_query_update .= " seotitle = '" . ReplaceQuote($str_seo_title) . "',";
$str_query_update .= " seokeyword = '" . ReplaceQuote($str_seo_keywords) . "',";
$str_query_update .= " seodescription = '" . ReplaceQuote($str_seo_desc) . "'";
$str_query_update .= " WHERE catpkid=". $int_pkid;
  //print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to cat_list.php page	
CloseConnection();
Redirect("item_list.php?type=S&msg=U&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------


?>