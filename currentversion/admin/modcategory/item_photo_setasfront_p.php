<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
//include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_masterpkid = 0;
if(isset($_GET['masterpkid']))
{
    $int_masterpkid=$_GET['masterpkid'];
}

if($int_masterpkid <= 0 || !is_numeric($int_masterpkid) || $int_masterpkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F");
    exit();
}

$str_filter = "";
$str_filter = "&catid=".$int_masterpkid."&#ptop";


$int_pkid = 0;
if(isset($_GET['pkid']))
{
    $int_pkid=$_GET['pkid'];
}
if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F".$str_filter);
    exit();
}

#----------------------------------------------------------------------------------------------------
#Select query to get the details from table
$str_query_select="";
$str_query_select="SELECT setasfront FROM ".$STR_DB_TABLE_NAME_PHOTO." WHERE pkid=".$int_pkid;
$rs_list = GetRecordset($str_query_select);
if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_photo_list.php?type=E&msg=F".$str_filter);
    exit();
}
$str_setasfront = $rs_list->Fields("setasfront");
if(strtoupper($str_setasfront)=='YES')
{
    $str_setasfront = "NO";
    //$str_setasfront_title = "Invisible";
}
else if(strtoupper($str_setasfront) == 'NO')
{
    $str_setasfront = "YES";
    //$str_setasfront_title ="Visible";
}
#---------------------------------------------------------------------------------------------------------
#Update Query
$str_query_update = "";    
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_PHOTO." SET setasfront='" .ReplaceQuote($str_setasfront). "', visible='YES' WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);

#Update query.	
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_PHOTO." SET setasfront='NO' WHERE pkid!=".$int_pkid." AND masterpkid=".$int_masterpkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_photo_list.php?type=S&msg=SF".$str_filter);
exit();
#-----------------------------------------------------------------------------------------------------
?>
