<?php 
/*
Create Date:- FEB-2019
Intially Create By :- 0015
Update History:
*/
#----------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/http_to_https.php";	
include "../../includes/lib_xml.php";
include "./item_config.php";
//print_r($_SESSION);exit;
#----------------------------------------------------------------------
//print_r($_GET); exit;
$int_catpkid = 0;
if(isset($_GET['catid']))
{
    $int_catpkid = $_GET['catid'];
}

if($int_catpkid <= 0 || !is_numeric($int_catpkid) || $int_catpkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F");
    exit();
}

//print $int_catpkid; exit;
$int_masterpkid = 0;
if(isset($_GET['subcatid']))
{
    $int_masterpkid=$_GET['subcatid'];
}

if($int_masterpkid <= 0 || !is_numeric($int_masterpkid) || $int_masterpkid == "")
{
    CloseConnection();
    Redirect("item_subcat_list.php?type=E&msg=F&catid=".$int_catpkid);
    exit();
}
//print_r($_GET); exit;
//print $int_masterpkid; exit;
$str_filter = "";
$str_filter = "&catid=".$int_catpkid."&subcatid=".$int_masterpkid."&#ptop";


//print $int_masterpkid; exit;
$int_pkid = 0;
if(isset($_GET["pkid"]) && trim($_GET["pkid"])!="" )
{
    $int_pkid=trim($_GET["pkid"]);
}

if($int_pkid=="" || $int_pkid==0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_subcat_photo_list.php?msg=F&type=E".$str_filter);
    exit();
}


$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME_SUBCAT_PHOTO." WHERE pkid=" . $int_pkid;
//print $str_query_select; exit;
$rs_edit = GetRecordset($str_query_select);
if ($rs_edit->count()==0)
{
    CloseConnection();
    Redirect("item_photo_list.php?msg=F&type=E&masterpkid=".$int_masterpkid."&#ptop");
    exit();
}	
#------------------------------------------------------------------------------------------------
# Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
# Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
	case("E"): $str_type = "E"; break;
	case("W"): $str_type = "W"; break;
    }
}
#	Get message text.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"):$str_message =$STR_MSG_ACTION_INFO_MISSING;break;				
        case("DT"):$str_message =  $STR_MSG_ACTION_TITLE_ALREADY_EXIST;break;
        case("IU"):$str_message =  $STR_MSG_ACTION_INVALID_URL_FORMAT;break;
    }
} 
#----------------------------------------------------------------------
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_PAGE_SUBCAT_PHOTO_LIST) ;?> : <?php print($STR_TITLE_EDIT); ?></title>    
</head>
<body>
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="container center-bg">
        <div class="row padding-10">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="row padding-10">
                    <div class="col-md-3 col-sm-6 col-xs-12 "><br/>
                        <div class="btn-group" role="group" aria-label="...">
                            <a href="./item_subcat_photo_list.php?pkid=<?php print $int_masterpkid.$str_filter; ?>" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE_SUBCAT_PHOTO_LIST);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_SUBCAT_PHOTO_LIST);?></a>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-6 col-xs-12 " align="right" >
                        <h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE_SUBCAT_PHOTO_LIST);?></h3>
                    </div>
                </div><hr/>
                <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); } ?>
                <div class="row padding-10">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <div class="row padding-10">
                                            <div class="col-md-6 col-sm-6 col-xs-8"><b><i class="fa fa-edit"></i>&nbsp;<?php print($STR_TITLE_EDIT); ?> </b></div>
                                            <div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>                                                        </div>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="false" <?php /* ?> style="height: 0px;"<?php */ ?>>
                                    <div class="panel-body">
                                        <form id="frm_edit" name="frm_edit" method="POST" action="item_subcat_photo_edit_p.php" onSubmit="return frm_edit_validate();" enctype="multipart/form-data">
                                            <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                            <div class="row padding-10">
                                                <div class="col-md-6">
                                                    <?php if($rs_edit->fields("thumbphotofilename")!="") { ?>
                                                    <div class="form-group"><label>Existing Photo</label><br/>
                                                        <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($int_pkid); ?>"><img border="0" alt="Image" class="img-responsive" src="<?php print($UPLOAD_IMG_PATH.$rs_edit->fields("thumbphotofilename"));?>"></a>
                                                        <div class="modal fade f-pop-up-<?php print($int_pkid); ?> " align="center" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                                            <div class="modal-dialog modal-lg">
                                                                <div class="modal-content">
                                                                    <div class="modal-body">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                                        <img src="<?php print($UPLOAD_IMG_PATH.$rs_edit->fields("largephotofilename"));?>" class="img-responsive img-rounded" alt="Image" title="Image">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>		
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"><label>Upload New Photo</label>
                                                    <span class="text-help-form">(<?php print($STR_MSG_IMG_FILE_TYPE);?>)</span>
                                                        <input type="file" name="fileimage" maxlength="255">
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" id="hdn_pkid" name="hdn_pkid" value="<?php print $rs_edit->Fields("pkid"); ?>" />
                                            <input type="hidden" id="hdn_masterpkid" name="hdn_masterpkid" value="<?php print $rs_edit->Fields("masterpkid"); ?>" />
                                            <input type="hidden" id="hdn_masterpkid" name="hdn_catpkid" value="<?php print $rs_edit->Fields("catpkid"); ?>" />
                                            <?php print DisplayFormButton("EDIT", 0); ?><?php print DisplayFormButton("RESET", 0); ?>                                                           </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                        
                </div> 
                <?php include "../../includes/help_for_edit.php"; ?>
            </div>
        </div>
    </div>        
    <?php include($STR_ADMIN_FOOTER_PATH); ?>
    <?php include "../../includes/include_files_admin.php"; ?>
    <script language="JavaScript" src="./item_subcat_photo_edit.js"></script>    
</body>
</html>
