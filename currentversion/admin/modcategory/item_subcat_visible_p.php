<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
//include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$str_filter = "";
$int_masterpkid = 0;
if(isset($_GET['masterpkid']))
{
    $int_masterpkid = $_GET['masterpkid'];
}
if($int_masterpkid <= 0 || !is_numeric($int_masterpkid) || $int_masterpkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F".$str_filter);
    exit();
}
$int_pkid = 0;
if(isset($_GET['pkid']))
{
    $int_pkid = $_GET['pkid'];
}
if($int_pkid <= 0 || !is_numeric($int_pkid) || $int_pkid == "")
{
    CloseConnection();
    Redirect("item_subcat_list.php?type=E&msg=F&catid=".$int_masterpkid."".$str_filter);
    exit();
}
#----------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT subcattitle,visible FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid=".$int_pkid;
$rs_list = GetRecordset($str_query_select);

$str_title = "";
$str_title = $rs_list->Fields("subcattitle");

if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_subcat_list.php?type=E&msg=F&catid=".$int_masterpkid."".$str_filter);
    exit();
}

$str_visible = "";
$str_visible = $rs_list->Fields("visible");

if(strtoupper($str_visible) == 'YES')
{
    $str_visible = "NO";
    $str_visible_title = "Invisible";
}
else if(strtoupper($str_visible) == 'NO')
{
    $str_visible = "YES";
    $str_visible_title = "Visible";
}
#-----------------------------------------------------------------------------------------------------
# Update query
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_SUBCAT." SET visible='" .ReplaceQuote($str_visible). "' WHERE subcatpkid=".$int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------
#Write to xml file
//WriteXml();
#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_subcat_list.php?type=S&msg=V&catid=".$int_masterpkid."&mode=".urlencode(RemoveQuote($str_visible)).$str_filter);
exit();
#-----------------------------------------------------------------------------------------------------
?>
