<?php
/*
Module Name:- modstore
File Name  :- item_cat_del_p.php
Create Date:- 04-FEB-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_pkid = 0;
if(isset($_GET["pkid"]) == true)
{
    $int_pkid = trim($_GET["pkid"]);
}	
if($int_pkid <= 0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}


#----------------------------------------------------------------------------------------------------
    #check wheather child record exists
    $str_query_select="";
    $str_query_select="SELECT subcatpkid FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE catpkid=".$int_pkid;
    //print $str_query_select;exit;
    $rs_list_check=GetRecordSet($str_query_select);

    if(!$rs_list_check->eof())
    {
        CloseConnection();
        Redirect("item_list.php?msg=F&type=E&#ptop");
        exit();
    }

    #select query to get details from t_ppd_cat table
    $str_query_select = "";
    $str_query_select = "SELECT title FROM ".$STR_DB_TABLE_NAME." WHERE catpkid=". $int_pkid;
    $rs_list = GetRecordSet($str_query_select);

    if($rs_list->eof())
    {
        CloseConnection();
        Redirect("item_list.php?msg=F&type=E&#ptop");
        exit();
    }

    # Delete Query
    $str_query_delete="";
    $str_query_delete="DELETE FROM ".$STR_DB_TABLE_NAME." WHERE catpkid=" .$int_pkid;
    //print $str_query_delete; exit;
    ExecuteQuery($str_query_delete);
    #----------------------------------------------------------------------------------------------------
    #Close connection and redirect to item_cat_list.php page	
    CloseConnection();
    Redirect("item_list.php?type=S&msg=D&#ptop");
    exit();
    #------------------------------------------------------------------------------------------------------------

?>