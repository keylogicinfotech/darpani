<?php
/*
Module Name:- Manage photoset
File Name  :- mdl_ps_add_p.php
Create Date:- 8-Sep-2006
Intially Create By :- 0014
*/
#---------------------------------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_image.php";
include "../../includes/lib_xml.php";
include "../../includes/lib_email.php";
//print_r($_POST); exit;
#----------------------------------------------------------------------------------------------------
#Initializing varialbles
$str_redirect_page_flag = "";
if(isset($_GET["flg"]) && trim($_POST["flg"])!="" )
{ $str_redirect_page_flag=trim($_POST["flg"]); }

$int_cat_pkid = 0;
if(isset($_POST["catid"]) && trim($_POST["catid"])!="" )
{ $int_cat_pkid=trim($_POST["catid"]); }

$str_key = "";
if(isset($_POST["key"]) && trim($_POST["key"])!="" )
{ $str_key=trim($_POST["key"]); }

# POST data for paging
$int_page = 0;
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"])!="" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"])>0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page=1; }
//print $int_page; exit;

$int_userpkid = 0;
if(isset($_GET['uid']) && $_GET['uid']!= "")
{
    $int_userpkid=$_GET['uid'];
}


$int_pkid = 0;

$int_masterpkid = 0;
if(isset($_POST['hdn_masterpkid']))
{
    $int_masterpkid = trim($_POST['hdn_masterpkid']);
}
if($int_masterpkid == "" || $int_masterpkid<0 || !is_numeric($int_masterpkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}


$str_filter = "";
//$str_filter = "&catid=".$int_cat_pkid."&key=".$str_key."&PagePosition=".$int_page."&userpkid=".$int_userpkid."&pkid=".$int_masterpkid;
$str_filter = "&catid=".$int_cat_pkid."&key=".$str_key."&PagePosition=".$int_page;
//print $str_filter; exit


#Get values of all passed GET / POST variables
if(isset($_POST['hdn_pkid']))
{
    $int_pkid=trim($_POST['hdn_pkid']);
}
if($int_pkid=="" || $int_pkid<0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_tailoringservice_list.php?msg=F&type=E".$str_filter);
    exit();
}




$int_service_option = 0;
if(isset($_POST['cbo_type']))
{
    $int_service_option = trim($_POST['cbo_type']);
}

$int_price = 0;
if($int_service_option > 0)
{
    $str_query_select = "";
    $str_query_select = "SELECT price FROM ".$STR_DB_TABLE_NAME_TAILORING_SERVICE." WHERE visible='YES' AND pkid=".$int_service_option;
    $rs_list_tailorinig_option_price = GetRecordSet($str_query_select);
    
    $int_price = $rs_list_tailorinig_option_price->Fields("price");
}

/*if(isset($_POST['txt_price']))
{
    $int_price = trim($_POST['txt_price']);
}*/
#------------------------------------------------------------------------------------------------------------
$str_query_select = "";
$str_query_select = "SELECT pkid FROM ".$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE." WHERE masterpkid=".$int_service_option." AND catpkid=".$int_masterpkid." AND pkid != ".$int_pkid;
//print $str_query_select; exit;
$rs_list_check_duplicate = GetRecordset($str_query_select);
if (!$rs_list_check_duplicate->EOF())
{
    CloseConnection();
    Redirect("item_tailoringservice_edit.php?msg=DU&type=E&pkid=".$int_pkid."&masterpkid=".$int_masterpkid.$str_filter);
    exit();
}
#------------------------------------------------------------------------------------------------------------
# Update Query
$str_query_update  = "";
$str_query_update  = "UPDATE ".$STR_DB_TR_TABLE_NAME_TAILORING_SERVICE." SET masterpkid=".$int_service_option.",";
$str_query_update .= "price=".$int_price." ";
$str_query_update .= " WHERE pkid=".$int_pkid." AND catpkid=".$int_masterpkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#write to xml file
//WriteXml();
#----------------------------------------------------------------------------------------------------
CloseConnection();
//print "item_tailoringservice_list.php?msg=U&type=S".$str_filter."";exit;
Redirect("item_tailoringservice_list.php?msg=U&type=S&pkid=".$int_masterpkid.$str_filter);
exit();
#----------------------------------------------------------------------------------------------------
?>
