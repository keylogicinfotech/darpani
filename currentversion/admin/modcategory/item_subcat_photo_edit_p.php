<?php
/*
Module Name:- Manage photoset
File Name  :- mdl_ps_add_p.php
Create Date:- 8-Sep-2006
Intially Create By :- 0014
*/
#---------------------------------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_image.php";
include "../../includes/lib_xml.php";
include "../../includes/lib_email.php";
//print_r($_POST); exit;
#----------------------------------------------------------------------------------------------------
#Initializing varialbles
$int_pkid = 0;
$int_catpkid = 0;
$int_masterpkid = 0;
$str_image = "";

//print_r($_POST); exit;
if(isset($_POST['hdn_catpkid']))
{
    $int_catpkid = trim($_POST['hdn_catpkid']);
}
if($int_catpkid == "" || $int_catpkid<0 || !is_numeric($int_catpkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}

if(isset($_POST['hdn_masterpkid']))
{
    $int_masterpkid = trim($_POST['hdn_masterpkid']);
}
if($int_masterpkid == "" || $int_masterpkid<0 || !is_numeric($int_masterpkid))
{
    CloseConnection();
    Redirect("item_subcat_list.php?msg=F&type=E&catid=".$int_catpkid);
    exit();
}

$str_query_select = "";
$str_query_select = "SELECT subcattitle FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid=".$int_masterpkid." AND catpkid=".$int_catpkid;
//print $str_query_select; exit;
$rs_list = GetRecordSet($str_query_select);

$str_subcattitle = "";
if($rs_list->Count() > 0)
{
    $str_subcattitle = str_replace(" ", "_", str_replace("-", "_",$rs_list->Fields("subcattitle")));
}
//print $str_subcattitle; exit;

$str_filter = "";
$str_filter = "&catid=".$int_catpkid."&subcatid=".$int_masterpkid;

#Get values of all passed GET / POST variables
if(isset($_POST['hdn_pkid']))
{
    $int_pkid = trim($_POST['hdn_pkid']);
}
if($int_pkid == "" || $int_pkid < 0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_subcat_photo_list.php?msg=F&type=E".$str_filter);
    exit();
}
//print_r($_FILES); exit;

if(isset($_FILES['fileimage']))
{
    $str_image = trim($_FILES['fileimage']['name']);
}
#------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------
#check all validation
if($str_image != "" )
{
    if(ValidateImageExtension($_FILES['fileimage']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
        CloseConnection();
        Redirect("item_subcat_photo_edit.php?msg=I&type=E&pkid=".$int_pkid.$str_filter);
        exit();
    }
}	


#----------------------------------------------------------------------------------------------------
#upload image

$str_large_file_name = "";
$str_thumb_file_name = "";
$str_large_path = "";
$str_thumb_path = "";

$str_query_select = "";
$str_query_select = "SELECT thumbphotofilename,largephotofilename FROM ".$STR_DB_TABLE_NAME_SUBCAT_PHOTO." WHERE pkid=".$int_pkid." AND masterpkid=".$int_masterpkid." AND catpkid=".$int_catpkid;
//print $str_query_select; exit;
$rs_list = GetRecordset($str_query_select);

if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_subcat_photo_edit.php?msg=F&type=E&pkid=".$int_pkid.$str_filter);
    exit();
}

$str_large_file_name = $rs_list->fields("largephotofilename");
$str_thumb_file_name = $rs_list->fields("thumbphotofilename");

//print $str_image;exit;
if($str_image != "")
{
    #delete old image
    //print $UPLOAD_IMG_PATH.$int_masterpkid."/"; exit;
    if($str_large_file_name!="")
    {
        DeleteFile($UPLOAD_IMG_PATH.trim($str_large_file_name));
    }
    if($str_thumb_file_name!="")
    {
        DeleteFile($UPLOAD_IMG_PATH.trim($str_thumb_file_name));
    }
    #upload new image

    $str_large_file_name=GetUniqueFileName().$int_masterpkid.$str_subcattitle."_l.".getextension($str_image);
    $str_thumb_file_name=GetUniqueFileName().$int_masterpkid.$str_subcattitle."_t.".getextension($str_image);


    
    $str_large_path = trim($UPLOAD_IMG_PATH.$str_large_file_name);
    $str_thumb_path = trim($UPLOAD_IMG_PATH.$str_thumb_file_name);

    UploadFile($_FILES['fileimage']['tmp_name'],$str_large_path);
    CompressImage($str_large_path, $str_large_path, 60);
    
    ## START - Code to put watermark on image
        /*$str_image_to_print_watermark = imagecreatefromstring(file_get_contents($str_main_path));
        $str_image_watermark = imagecreatefromstring(file_get_contents("./../../images/logo_150x29.png"));

        imagecopy($str_image_to_print_watermark, $str_image_watermark, 0, 0, 0, 0, 150 , 29);
        imagejpeg($str_image_to_print_watermark, $str_main_path);
        imagedestroy($str_image_to_print_watermark);*/
    ## END - Code to put watermark on image
    
    //ResizeImage($str_main_path,$INT_PHOTO_MAIN_WIDTH);
    SaveAsThumbImage($str_large_path,$str_large_path,$INT_IMG_WIDTH_LARGE);
    SaveAsThumbImage($str_large_path,$str_thumb_path,$INT_IMG_WIDTH_THUMB);
}

#----------------------------------------------------------------------------------------------------
# Update Query
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_SUBCAT_PHOTO." SET thumbphotofilename='".ReplaceQuote($str_thumb_file_name)."',";
//$str_query_update = $str_query_update."photographerurl='".ReplaceQuote($str_purl)."',";
//$str_query_update = $str_query_update."photographertitle='".ReplaceQuote($str_ptitle)."',";
$str_query_update = $str_query_update."largephotofilename='".ReplaceQuote($str_large_file_name)."'";
//$str_query_update = $str_query_update."colorpkid=".$int_colorpkid.",";
//$str_query_update = $str_query_update."color='".ReplaceQuote($str_color)."',";
//$str_query_update = $str_query_update."previewtoall='".ReplaceQuote($str_preview)."',";
//$str_query_update = $str_query_update."accesstoall='".ReplaceQuote($str_access)."' ";
$str_query_update = $str_query_update." WHERE pkid=".$int_pkid." AND masterpkid=".$int_masterpkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#write to xml file
//WriteXml();
#----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_subcat_photo_list.php?msg=U&type=S".$str_filter);
exit();
#----------------------------------------------------------------------------------------------------
?>
