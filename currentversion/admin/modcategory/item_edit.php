<?php
/*
Module Name:- modstore
File Name  :- item_cat_edit.php
Create Date:- 4-MARCH-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
#get Query String Data
$int_pkid = 0;
if(isset($_GET["pkid"])==true)
{
    $int_pkid = trim($_GET["pkid"]);
}	
if($int_pkid <= 0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}

$str_title = "";
if (isset($_GET["title"]))
{
    $str_title = trim($_GET["title"]);
}
#----------------------------------------------------------------------------------------------------
#select record from table
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE catpkid=". $int_pkid;
$rs_edit=GetRecordSet($str_query_select);

if($rs_edit->eof())
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
#------------------------------------------------------------------------------------------
#   Initialization of variables used for message display.   
    $str_type = "";
    $str_message = "";
#	Get message type.
    if(isset($_GET["type"]))
    {
        switch(trim($_GET["type"]))
        {
            case("S"): $str_type = "S"; break;
            case("E"): $str_type = "E"; break;
            case("W"): $str_type = "W";  break;
        }
    }
#	Get message text.
    if(isset($_GET["msg"]))
    {
        switch(trim($_GET["msg"]))
        {
            case("F"): $str_message =$STR_MSG_ACTION_INFO_MISSING; break;				
            case("DU"): $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE;
            case("SDU"): $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?>  : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_EDIT); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
    <div class="container content_bg">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
        <div class="row padding-10">
            <div class="col-md-3 col-sm-6 col-xs-12 button_space">
                <div class="btn-group" role="group" aria-label="...">
                    <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE);?></a>
		</div>
            </div>
            <div class="col-md-9 col-sm-6 col-xs-12 " align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE);?></h3></div>
	</div><hr> 
	<?php if($str_type != "" && $str_message != "")	{ print(DisplayMessage(0,$str_message,$str_type)); }	?>
	<div class="row padding-10">
            <div class="col-md-12">
        	<div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                	<div class="panel-heading">
                            <h4 class="panel-title">
                                <b><i class="fa fa-edit"></i>&nbsp;<?php print($STR_TITLE_EDIT); ?></b>
                                <?php print($STR_LINK_HELP); ?>												
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse">
                            <div class="panel-body">
                                <form name="frm_edit" action="./item_edit_p.php" method="post" onSubmit="return frm_edit_validate()">
                                    <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group"><label>Title</label><span class="text-help-form"> *</span>
                                                <input type="text" name="txt_title" value="<?php print(MyHtmlEncode($rs_edit->fields("title"))); ?>" maxlength="255" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Description</label><span class="text-help-form"> </span> 
                                                <textarea cols="100" rows="5" name="ta_desc" id="ta_desc" class="form-control input-sm"  placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>"><?php print(MyHtmlEncode($rs_edit->fields("description"))); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Tailoring Service Description</label><span class="text-help-form"> </span> 
                                                <textarea cols="100" rows="5" name="ta_desc2" id="ta_desc2" class="form-control input-sm"  placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>"><?php print(MyHtmlEncode($rs_edit->fields("description2"))); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Display As New</label><span class="text-help-form"> *</span>
                                                <span class="text-help-form"> (<?php print($STR_MSG_NEW);?>)</span>
                                                <select id="cbo_new" name="cbo_new" class="form-control input-sm">
                                                    <option value="NO" <?php print(CheckSelected("NO",$rs_edit->fields("displayasnew"))); ?>>NO</option>
                                                    <option value="YES" <?php print(CheckSelected("YES",$rs_edit->fields("displayasnew")));?>>YES</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>SEO Title</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_seo_title" id="txt_seo_title" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_SEO_TITLE; ?>"  maxlength="255" size="60" value="<?php print $rs_edit->fields("seotitle"); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>SEO Keywords</label><span class="text-help-form"> </span>
                                                <input type="text" name="txt_seo_keywords" id="txt_seo_keywords" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_SEO_KEYWORDS; ?>"  maxlength="255" size="60" value="<?php print $rs_edit->fields("seokeyword"); ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>SEO Description</label><span class="text-help-form"> </span>
                                                <textarea name="ta_seo_desc" id="ta_seo_desc" rows="5" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>" ><?php print(RemoveQuote($rs_edit->fields("seodescription"))); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                    <input type="hidden" name="hdn_pkid" value="<?php print($int_pkid);?>">
                                    <input type="hidden" name="hdn_catflag" value="<?php print($str_cat_flag); ?>">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</div>
    <a name="help"></a>
    <?php include "../../includes/help_for_edit.php"; ?>
</div>

<?php include "../../includes/include_files_admin.php"; ?>      
    <?php include($STR_ADMIN_FOOTER_PATH); ?>   
    <script type="text/javascript" src="../../js/richetxteditor.js"></script>
    <script type="text/javascript">
            bkLib.onDomLoaded(function() {
                new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_desc');
                  new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_desc2');
                          //new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
            });
                    // convert all text areas to rich text editor on that page
            bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
                    // convert text area with id area2 to rich text editor with full panel.
            bkLib.onDomLoaded(function() {
            new nicEditor({fullPanel : true}).panelInstance('ta_desc');     
            new nicEditor({fullPanel : true}).panelInstance('ta_desc2');
                            // new nicEditor({fullPanel : true}).panelInstance('ta_remark');
            });
    </script>
<script language="JavaScript" src="./item_edit.js" type="text/javascript"></script>

</body></html>