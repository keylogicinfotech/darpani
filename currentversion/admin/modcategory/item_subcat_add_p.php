<?php
/*
Module Name:- modstore
File Name  :- item_cat_add_p.php
Create Date:- 04-FEB-2019
Intially Create By :- 015
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
//include "./item_cat_app_specific.php";	
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_masterpkid = 0;
if(isset($_POST["masterpkid"]))
{
    $int_masterpkid = trim($_POST["masterpkid"]);
}
if($int_masterpkid <=0 || is_numeric($int_masterpkid) == false)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}
//print $int_masterpkid;exit;
$str_title = "";
$str_desc = "";
$str_type = "";
$str_new = "YES";
$str_visible = "YES";
$str_seo_title = "";
$str_seo_keywords = "";
$str_seo_desc = "";

if (isset($_POST["txt_title"]))
{
    $str_title = trim($_POST["txt_title"]);
}
if(isset($_POST["ta_desc"]))
{
    $str_desc = trim($_POST["ta_desc"]);
}

if (isset($_POST["cbo_visible"]))
{
    $str_visible = trim($_POST["cbo_visible"]);
}
if (isset($_POST["cbo_new"]))
{
    $str_new = trim($_POST["cbo_new"]);
}

if(isset($_POST["txt_seo_title"]) && trim($_POST["txt_seo_title"])!="")
{
    $str_seo_title = trim($_POST["txt_seo_title"]);
}
if(isset($_POST["txt_seo_keywords"]) && trim($_POST["txt_seo_keywords"])!="")
{
    $str_seo_keywords = trim($_POST["txt_seo_keywords"]);
}
if(isset($_POST["ta_seo_desc"]))
{
    $str_seo_desc = trim($_POST["ta_seo_desc"]);
}
//print $str_type;exit;
#----------------------------------------------------------------------------------------------------
#Redirect URL

$str_redirect = "";
$str_redirect .= "&visible=". urlencode(RemoveQuote($str_visible));
$str_redirect .= "&seotitle=". urlencode(RemoveQuote($str_seo_title));
$str_redirect .= "&seokeyword=". urlencode(RemoveQuote($str_seo_keywords));
$str_redirect .= "&seodescription=". urlencode(RemoveQuote($str_seo_desc));
$str_redirect .= "&title=". urlencode(RemoveQuote($str_title))."&desc=".urlencode(RemoveQuote($str_desc));

#----------------------------------------------------------------------------------------------------
#To check required parameters are passed properly or not

if($str_title == "" || $str_new == "" || $str_visible == "")
{
    CloseConnection();
    Redirect("item_subcat_list.php?msg=F&type=E". $str_redirect."&catid=".$int_masterpkid."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Duplication Check
$str_query_select = "";
$str_query_select = "SELECT subcattitle from ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcattitle= '" . ReplaceQuote($str_title) . "'";
//        print "dup" .$str_query_select;exit; 
$rs_list_check_duplicate = GetRecordSet($str_query_select);

if(!$rs_list_check_duplicate->eof())
{
    CloseConnection();
    Redirect("item_subcat_list.php?msg=DU&type=E&". $str_redirect."&catid=".$int_masterpkid."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Insert Record 

$int_max=0;
$int_max=GetMaxValue($STR_DB_TABLE_NAME_SUBCAT,"displayorder");	

$str_createdate="";
$str_createdate=  date("Y-m-d");

$str_query_insert = "";
$str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_SUBCAT." (catpkid, subcattitle, description, seotitle, seokeyword, seodescription, visible, displayasnew,displayorder)";
$str_query_insert .= " VALUES(".$int_masterpkid.", '" . ReplaceQuote($str_title) . "',";
$str_query_insert .= "'" . ReplaceQuote($str_desc) . "',";
$str_query_insert .= "'" . ReplaceQuote($str_seo_title) . "',";
$str_query_insert .= "'" . ReplaceQuote($str_seo_keywords) . "',";
$str_query_insert .= "'" . ReplaceQuote($str_seo_desc) . "',";
$str_query_insert .= "'" . ReplaceQuote($str_visible) . "','" . ReplaceQuote($str_new) . "',".$int_max.")";
//print $str_query_insert;exit;
ExecuteQuery($str_query_insert);

#----------------------------------------------------------------------------------------------------
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_subcat_list.php?type=S&msg=S&catid=".$int_masterpkid."&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------

?>
