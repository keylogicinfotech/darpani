
function frm_list_check_displayorder()
{
    with(document.frm_list)
    {
        cnt=hdn_counter.value;
        for(i=1;i<cnt;i++)
        {
            if(trim(eval("txt_displayorder" + i).value) =="")
            {
                alert("Please enter display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }

            if(eval("txt_displayorder" + i).value <0)
            {
                alert("Please enter positive integer for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }

            if(isNaN(eval("txt_displayorder" + i).value))
            {
                alert("Please enter numeric value for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }
        }
    }
    return true;
}

function confirm_delete()
{
    if(confirm("Are you sure you want to delete this details?"))
    {
        if(confirm("Confirm Deletion:Click 'Ok' to delete this details or 'Cancel' to deletion."))
        {
            return true;
        }
    }
    return false;
}


function frm_add_validate()
{
    with(document.frm_add)
    {
        if(trim(txt_title.value) == "")
        {
                alert("Please enter title.");
                txt_title.select();
                txt_title.focus();			
                return false;
        }

//		if(cbo_type.value == "")
//		{
//			alert("Please select category / sub category type.");
//			cbo_type.focus();			
//			return false;
//		}

        if(cbo_visible.value == "")
        {
                alert("Please set value for visible.");			
                cbo_visible.focus();			
                return false;
        }
    }
    return true;
}
