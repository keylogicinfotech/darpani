<?php
/*
Module Name:- modPstore
File Name  :- cat_edit_p.php
Create Date:- 20-MARCH-2006
Intially Create By :- 0023
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_masterpkid = 0;
if(isset($_POST['hdn_masterpkid']))
{
    $int_masterpkid = $_POST['hdn_masterpkid'];
}
//print $int_masterpkid; exit;
if($int_masterpkid <= 0 || !is_numeric($int_masterpkid) || $int_masterpkid == "")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F".$str_filter);
    exit();
}



$int_pkid = 0;
if (isset($_POST["hdn_pkid"]))
{	
    $int_pkid = trim($_POST["hdn_pkid"]);
}		
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
    CloseConnection();
    Redirect("item_subcat_list.php?msg=F&type=E&masterpkid=".$int_masterpkid."&#ptop");
    exit();
}

$str_title = "";
$str_desc = "";
$str_new = "";
$str_seo_title = "";
$str_seo_keyword = "";
$str_seo_desc = "";
$str_type = 0;

if (isset($_POST["txt_title"]))
{
    $str_title = trim($_POST["txt_title"]);
}
if(isset($_POST["ta_desc"]))
{
    $str_desc = trim($_POST["ta_desc"]);
}

if (isset($_POST["cbo_new"]))
{
    $str_new = trim($_POST["cbo_new"]);
}
if(isset($_POST["txt_seo_title"]) && trim($_POST["txt_seo_title"])!="")
{
    $str_seo_title = trim($_POST["txt_seo_title"]);
}
if(isset($_POST["txt_seo_keywords"]) && trim($_POST["txt_seo_keywords"])!="")
{
    $str_seo_keyword = trim($_POST["txt_seo_keywords"]);
}
if(isset($_POST["ta_seo_desc"]))
{
    $str_seo_desc = trim($_POST["ta_seo_desc"]);
}
#----------------------------------------------------------------------------------------------------
#Validation Check

if($str_title == "")
{
    CloseConnection();
    Redirect("item_subcat_edit.php?msg=F&type=E&masterpkid=".$int_masterpkid."&pkid=". $int_pkid."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
# Duplication Check
$str_query_select = "";
$str_query_select = "SELECT subcattitle FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid!=".$int_pkid." AND subcattitle= '" . ReplaceQuote($str_title) . "'";
$rs_list_check_duplicate = GetRecordSet($str_query_select);

if(!$rs_list_check_duplicate->eof())
{
    CloseConnection();
    Redirect("item_subcat_edit.php?msg=DU&type=E&masterpkid=".$int_masterpkid."&pkid=". $int_pkid."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
# Update Query
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME_SUBCAT." SET subcattitle='" .  ReplaceQuote($str_title) . "', ";
$str_query_update .= " description = '" . ReplaceQuote($str_desc) . "',";
$str_query_update .= " displayasnew = '" . ReplaceQuote($str_new) . "',";
$str_query_update .= " seotitle = '" . ReplaceQuote($str_seo_title) . "',";
$str_query_update .= " seokeyword = '" . ReplaceQuote($str_seo_keyword) . "',";
$str_query_update .= " seodescription = '" . ReplaceQuote($str_seo_desc) . "'";
$str_query_update .= " WHERE subcatpkid=". $int_pkid;
//print $str_query_update; exit;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to cat_list.php page	
CloseConnection();
Redirect("item_subcat_list.php?type=S&msg=U&catid=".$int_masterpkid."&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------


?>