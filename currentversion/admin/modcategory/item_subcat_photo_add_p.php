<?php
/*
File Name  :- item_order_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
include "../../includes/lib_file_system.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables

$int_catpkid = 0;
if(isset($_POST["hdn_catpkid"]))
{
    $int_catpkid = trim($_POST["hdn_catpkid"]);
}	
if($int_catpkid == "" || $int_catpkid < 0 || !is_numeric($int_catpkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#ptop");
    exit();
}


$int_masterpkid = 0;
if(isset($_POST["hdn_masterpkid"]))
{
    $int_masterpkid = trim($_POST["hdn_masterpkid"]);
}	
if($int_masterpkid == "" || $int_masterpkid < 0 || !is_numeric($int_masterpkid))
{
    CloseConnection();
    Redirect("item_subcat_list.php?msg=F&type=E&catid=".$int_catpkid."&#ptop");
    exit();
}

$str_query_select = "";
$str_query_select = "SELECT subcattitle FROM ".$STR_DB_TABLE_NAME_SUBCAT." WHERE subcatpkid=".$int_masterpkid;
$rs_list = GetRecordSet($str_query_select);

$str_subcattitle = "";
if($rs_list->Count() > 0)
{
    
    $str_subcattitle = str_replace(" ", "_", str_replace("-", "_",$rs_list->Fields("subcattitle")));
}
//print $str_subcattitle;exit;

$str_visible = "";
if(isset($_POST['cbo_visible']))
{
    $str_visible = trim($_POST['cbo_visible']);
}

$str_image = "";
if(isset($_FILES['fileimage']))
{
    $str_image = trim($_FILES['fileimage']['name']);
}

$str_preview = "";
if (isset($_POST["cbo_preview"]))
{
    $str_preview = trim($_POST["cbo_preview"]);
}

$str_access = "";
if (isset($_POST["cbo_access"]))
{
    $str_access = trim($_POST["cbo_access"]);
}
$str_ptitle = "";
$str_purl = "";
$int_colorpkid = 0;
$str_color = "";
#----------------------------------------------------------------------------------------------------
$str_redirect = "";
$str_redirect.= "&visible=".RemoveQuote(urlencode($str_visible))."&pkid=".urlencode($int_masterpkid);
$str_redirect.= "&preview=".$str_preview."&access=".$str_access."&#ptop";
#----------------------------------------------------------------------------------------------------
# check all validation
if($str_image == "" || $str_visible == "")
{
    CloseConnection();
    //Redirect("item_photo_list.php?msg=F&type=E".$str_redirect);
    Redirect("item_subcat_photo_list.php?msg=F&type=E&catid=".$int_catpkid."subcatid=".$int_masterpkid. "&#ptop");
}
if($str_image != "")
{
    if(ValidateImageExtension($_FILES['fileimage']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
        CloseConnection();
        Redirect("item_subcat_photo_list.php?msg=F&type=E&catid=".$int_catpkid."subcatid=".$int_masterpkid. "&#ptop");
        exit();
    }
}	
#---------------------------------------------------------------------------------------------------- 
$str_dir = $UPLOAD_IMG_PATH;
//print $str_dir; exit;
CreateDirectory($str_dir);

if(!file_exists($str_dir))
{
    //print "Error"; exit;
    CloseConnection();
    Redirect("item_subcat_photo_list.php?msg=F&type=E&catid=".$int_catpkid."subcatid=".$int_masterpkid. "&#ptop");
    exit();
}
#upload image
$str_large_file_name="";
$str_thumb_file_name="";
$str_main_path="";
$str_large_path="";
$str_thumb_path="";

if($str_image!="")
{	
    
    $str_large_file_name=GetUniqueFileName().$int_masterpkid.$str_subcattitle."_l.".getextension($str_image);
    $str_thumb_file_name=GetUniqueFileName().$int_masterpkid.$str_subcattitle."_t.".getextension($str_image);

    
    $str_large_path = trim($str_dir.$str_large_file_name);
    $str_thumb_path = trim($str_dir.$str_thumb_file_name);

    UploadFile($_FILES['fileimage']['tmp_name'],$str_large_path);
    
    if(strtoupper(getextension($str_image))=="JPG" || strtoupper(getextension($str_image))=="JPEG")
    {    
        CompressImage($str_large_path, $str_large_path, 60);
    }
    /*## START - Code to put watermark on image
        $str_image_to_print_watermark = imagecreatefromstring(file_get_contents($str_main_path));
        $str_image_watermark = imagecreatefromstring(file_get_contents("./../../images/logo_150x29.png"));

        imagecopy($str_image_to_print_watermark, $str_image_watermark, 0, 0, 0, 0, 150 , 29);
        imagejpeg($str_image_to_print_watermark, $str_main_path);
        imagedestroy($str_image_to_print_watermark);
    ## END - Code to put watermark on image

    //ResizeImage($str_main_path,$INT_PHOTOSET_PHOTO_MAIN_WIDTH);
     * 
     */
    SaveAsThumbImage($str_large_path,$str_large_path,$INT_IMG_WIDTH_LARGE);
    SaveAsThumbImage($str_large_path,$str_thumb_path,$INT_IMG_WIDTH_THUMB);
}	
#----------------------------------------------------------------------------------------------------
$str_photo_front = "NO";
$str_query_select = "SELECT pkid FROM ".$STR_DB_TABLE_NAME_SUBCAT_PHOTO." WHERE setasfront='YES' AND masterpkid=".$int_masterpkid;
$rs_list = GetRecordSet($str_query_select);
if($rs_list->EOF() == true)
{
    $str_photo_front="YES";
}
#----------------------------------------------------------------------------------------------------
#select query to find maximum display order
$int_max = "";
$int_max = GetMaxValue($STR_DB_TABLE_NAME_SUBCAT_PHOTO,"displayorder");
$int_photono = 0;
$int_photono = GetMaxValue($STR_DB_TABLE_NAME_SUBCAT_PHOTO,"photosrno");

# Insert Query
$str_query_insert = "";
$str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME_SUBCAT_PHOTO."(masterpkid, catpkid,photosrno,thumbphotofilename,largephotofilename,";
$str_query_insert .= "visible,setasfront,displayorder)";
$str_query_insert .= " VALUES (".$int_masterpkid.", ".$int_catpkid.",".$int_photono.",";
$str_query_insert .= "'" . ReplaceQuote($str_thumb_file_name) . "',";
$str_query_insert .= "'" . ReplaceQuote($str_large_file_name) . "',";
$str_query_insert .= "'" . ReplaceQuote($str_visible) . "','" . ReplaceQuote($str_photo_front) . "',".$int_max.")";
//print $str_query_insert;exit;
ExecuteQuery($str_query_insert);	

# Update Query
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME." SET lastupdatedate='".date("Y-m-d")."' WHERE catpkid=".$int_masterpkid;
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------
#write to xml file
//WriteXml();
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to listing page	
CloseConnection();
Redirect("item_subcat_photo_list.php?type=S&msg=S&catid=".$int_catpkid."&subcatid=".$int_masterpkid.$str_redirect."&#ptop");
exit();
#----------------------------------------------------------------------------------------------------
?>
