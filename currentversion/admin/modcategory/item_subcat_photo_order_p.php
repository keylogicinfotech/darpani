<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
//include "item_app_specific.php";
//print_r($_POST);exit;
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_catpkid = 0;
if(isset($_POST['hdn_catpkid']))
{
    $int_catpkid = trim($_POST['hdn_catpkid']);
}
if($int_catpkid == "" || $int_catpkid<0 || !is_numeric($int_catpkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}

$int_masterpkid = 0;
if(isset($_POST['hdn_masterpkid']))
{
    $int_masterpkid = $_POST['hdn_masterpkid'];
}
if($int_masterpkid <= 0 || !is_numeric($int_masterpkid) || $int_masterpkid == "")
{
    CloseConnection();
    Redirect("item_subcat_list.php?type=E&msg=F&catid=".$int_catpkid);
    exit();
}

$str_filter = "";
$str_filter = "&catid=".$int_catpkid."&subcatid=".$int_masterpkid;
//print $int_masterpkid;exit;
//exit;

$int_cnt = "";
if (isset($_POST["hdn_counter"]))
{
    $int_cnt=trim($_POST["hdn_counter"]);
}	
if($int_cnt=="" || $int_cnt<0 || !is_numeric($int_cnt))
{
    CloseConnection();
    Redirect("item_subcat_photo_list.php?msg=F&type=E".$str_filter."&#ptop");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Update display order field in table
for($i=1;$i<$int_cnt;$i++)
{
        if(trim($_POST["txt_displayorder".$i]) != "" && trim($_POST["txt_displayorder".$i]) >=0 && is_numeric(trim($_POST["txt_displayorder".$i]))==true  && trim($_POST["hdn_pkid".$i])!="" && trim($_POST["hdn_pkid".$i])>0 && is_numeric(trim($_POST["hdn_pkid".$i]))==true)
        {
            $str_query_update="UPDATE ".$STR_DB_TABLE_NAME_SUBCAT_PHOTO." SET displayorder='".trim($_POST["txt_displayorder".$i])."' WHERE pkid=".trim($_POST["hdn_pkid".$i]);
            ExecuteQuery($str_query_update);
        }
}



#-----------------------------------------------------------------------------------------------------
# Write to xml file
//WriteXml();
#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_subcat_photo_list.php?type=S&msg=O".$str_filter);
exit();
#----------------------------------------------------------------------------------------------------- ?>
