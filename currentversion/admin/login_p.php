<?php
/*
	Module Name:- Login
	File Name  :- login_p.php
	Create Date:- 09-12-2016
	Intially Create By :-0022
	Update History:
*/	
#---------------------------------------------------------------------------------------------	
	session_start();
	include "../includes/configuration.php";
	include "../includes/lib_common.php";
	include "../includes/lib_data_access.php";	
#---------------------------------------------------------------------------------------------	
#	to get values from login page.	
	//print_r($_POST); exit;
	
	if(isset($_POST["txt_adminid"])) 
	{ 
		// create object $conn for using method of Connection() class that is MySqliRealEscapeString()... 
		$conn = new Connection();
		$str_adminid = $conn->MysqliRealEscapeString(trim($_POST["txt_adminid"]));
	 }
	if(isset($_POST["pas_admin_password"]))
	{ $str_password = trim($_POST["pas_admin_password"]); }
	
	// For retrieve the Secrect Code Value from session and Input form.
	$str_unique_code="";
	$str_unique_text="";
	if(isset($_SESSION["image_secret_code"]))
	{ $str_unique_code=trim($_SESSION["image_secret_code"]); }
	if(isset($_POST["txt_code"]) && trim($_POST["txt_code"])!="")
	{ $str_unique_text=trim($_POST["txt_code"]); }
	

#---------------------------------------------------------------------------------------------	
	#	to check whether both values are passed properly or not.
	if($str_adminid == "" || $str_password == ""/* || strlen($str_password)< 8*/)
	{
		CloseConnection();
		Redirect("login.php?msg=F&type=E");
		exit();
	}
	
	# To hide Secret code and Security Question on local server 
	/* if($APP_RUN_MODE == "SERVER") {
		#	Check whether secret code is valid or not
		if($str_unique_code=="" || $str_unique_text=="" )
		{
			CloseConnection();
			unset($_SESSION['image_secret_code']);
			Redirect("./login.php?msg=IS&type=E");
			exit();
		}
		if($str_unique_code!=$str_unique_text)
		{
			CloseConnection();
			unset($_SESSION['image_secret_code']);
			Redirect("./login.php?msg=IS&type=E");
			exit();
		}
		$str_spamfilterq="";
		$str_spamfiltera="";
		if(isset($_POST['txt_spamfilterq']))
		{
			$str_spamfilterq=trim($_POST['txt_spamfilterq']);
		}
		if(isset($_POST['hdn_spamfiltera']))
		{
			$str_spamfiltera=trim($_POST['hdn_spamfiltera']);
		}
		
		if ($str_spamfilterq != $str_spamfiltera)
		{
			CloseConnection();
			Redirect("./login.php?msg=SU&type=E");
			exit();
		}
	} */

#---------------------------------------------------------------------------------------------		
#	select query from t_siteadmin to get the siteadminusername contain admin value.
	//$str_query_select = "select * from t_siteadmin where siteadminusername='" . $str_adminid . "' and siteadminpassword='" . $str_password . "'";
	$str_query_select = "SELECT * FROM t_siteadmin WHERE BINARY username='".$str_adminid."'";
	//print $str_query_select;exit;
	$rs_admin = GetRecordSet($str_query_select);
	
	if(strlen($str_password) < 8)
	{
		CloseConnection();
		Redirect("login.php?msg=SP&type=E");
		exit();
	}
	if(strlen($str_password) > 20)
	{
		CloseConnection();
		Redirect("login.php?msg=LP&type=E");
		exit();
	}
#	to check whether recordset is empty or not
	if($rs_admin->eof() == true) //Record Not Found For Given UserName And Password
	{  
		CloseConnection();
		Redirect('login.php?msg=IP&type=E'); //Redirect Response Back To Default Page
		exit();
	}
	
	
#	record is found but admin login is restricted then redirect the page.
	if($rs_admin->fields("allowlogin") == "NO")
	{
	  CloseConnection();
	  redirect('login.php?msg=CP&type=E'); //Redirect Response Back To Default Page
	  exit();
	}
	
//	print strtotime(date("Y-m-d H:i:s")) - strtotime($rscheck->fields("lastlogindatetime"));
//	exit;
	
	#	If duration between last login time and current login time is less then 15 seconds then do not allow to login for security purpose.
	/*if(strtotime(date("Y-m-d H:i:s")) - strtotime($rs_admin->fields("lastlogindatetime")) < 15) 
	{
		CloseConnection();
		Redirect('login.php?msg=RUH&type=E'); //Redirect Response Back To Default Page
		exit();
	}*/
#---------------------------------------------------------------------------------------------		
	$enc_text = $rs_admin->fields("password");
	
	$plain_text = md5_decrypt($enc_text, $STR_ENCRYPTION_KEY);
        //print $plain_text." - ".$str_password; exit;
	if($str_password!=$plain_text)
	{
		CloseConnection();
		Redirect('login.php?msg=IP&type=E'); //Redirect Response Back To Default Page
		exit();
	}
#---------------------------------------------------------------------------------------------		
	//Record Found For Given UserName And Password

    $_SESSION['adminname'] = $str_adminid; //Create Session Variable For Session Validation
	$_SESSION['adminpkid']=$rs_admin->fields("pkid");
	if($rs_admin->fields("issuperadmin") == "YES")
	{
		$_SESSION['superadmin'] = 'YES';
	}
	else
	{
		$_SESSION['superadmin'] = 'NO';
	}
#---------------------------------------------------------------------------------------------
//Update LastLoginDateTime in t_siteadmin table.	
	$str_query_update="UPDATE t_siteadmin SET lastlogindatetime='".date("Y-m-d H:i:s")."' WHERE pkid=".$rs_admin->fields("pkid");
	//print $str_query_update; exit;
	ExecuteQuery($str_query_update);
#---------------------------------------------------------------------------------------------			
//exit;
	//close the connection
	CloseConnection();
	redirect('admin_home.php'); //Redirect Response To Admin Home Page
	exit();	
?>
