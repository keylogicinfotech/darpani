<?php 
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------
#select query to get all the details from table	
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME." ".$STR_DB_TABLE_NAME_ORDER_BY." ";
//print $str_query_select;
$rs_list=GetRecordset($str_query_select);
#------------------------------------------------------------------------------------------
#getting query string variables	
$str_sub="";
$str_desc="";
$str_visible="";
$str_pos="";
if(isset($_GET['question'])) { $str_sub=trim($_GET['question']); }
if(isset($_GET['desc'])) { $str_desc=trim($_GET['desc']); }
if(isset($_GET["visible"])) { $str_visible=trim($_GET["visible"]); }
if (isset($_GET["position"])){ $str_pos = trim($_GET["position"]);  }
if (isset($_GET["po_mode"])){$str_pos = trim($_GET["po_mode"]);}
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type="";
$str_message="";
$str_title="";
$str_mode="";
if(isset($_GET['tit'])) { $str_title=trim($_GET['tit']); }
if(isset($_GET['mode'])) { $str_mode=trim($_GET['mode']); }
#Get message type.
if(isset($_GET['type']))
{
    switch(trim($_GET['type']))
    {
        case("S"): $str_type = "S"; break; case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Get message text.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;        
    }
}	
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-6 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="fa fa-chevron-left "></i>&nbsp;Back</a>
                <a href="./item_cms.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)">
                    <i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)
                </a><?php */?>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12" align="right" >
            <h3><?php print($STR_TITLE_PAGE);?> </h3>
        </div>
    </div>
    <hr> 
    <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <span>
                                <a class="accordion-toggle collapsed" data-toggle="collapse" title="<?php print $STR_HOVER_ADD; ?>" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>
                            </span>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
                        <div class="panel-body">
                            <form name="frm_add" action="./item_add_p.php" method="post" onSubmit="return frm_add_validateform()" enctype="multipart/form-data">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="form-group">
                                    <label>Title</label><span class="text-help-form"> *</span>
                                    <input type="text" placeholder="<?php print $STR_PLACEHOLDER_TITLE; ?>" name="txt_title"  id="txt_title" class="form-control input-sm" maxlength="255" value="<?php print(RemoveQuote($str_sub));?>">
                                </div>
                                <?php /* ?><div class="form-group">
                                    <label>Description</label><span class="text-help-form"> </span>
                                    <textarea name="txt_desc" id="ta_desc" rows="20" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC ?>" ><?php print(RemoveQuote($str_desc)); ?></textarea>
                                </div><?php */ ?>
                                <div class="form-group">
                                    <label>Visible</label><span class="text-help-form"> *</span>
                                    <span class="text-help-form"> (<?php print($STR_MSG_VISIBLE);?>)</span>
                                    <select name="cbo_visible" class="form-control input-sm"><option value="YES" <?php print(CheckSelected("YES",$str_visible));?>>YES</option><option value="NO" <?php print(CheckSelected("NO",$str_visible)); ?>>NO</option></select>
                                </div>
				<?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <form name="frm_list" action="./item_order_p.php" method="post" onSubmit="return frm_list_check_displayorder();">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                        <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                        <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_DISPLAY_ORDER; ?></th>
                        <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($rs_list->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="5" align="center" class="alert alert-danger">
                            <?php print($STR_MSG_NO_DATA_AVAILABLE); ?>
                        </td>
                    </tr>
                <?php } else { ?>
                    <?php 
                    $int_cnt=1; 
                    while(!$rs_list->EOF()==true) {  ?>
                     <tr>
                        <td align="center" class="text-center"><?php print($int_cnt); ?></td>
                        <td align="leflt" >
                            <h4><b><?php print $rs_list->fields("title"); ?></b></h4>
                            <?php if($rs_list->fields("description") != ""  && $rs_list->fields("description") != "<br>") { ?>
                            <p><?php print $rs_list->fields("description"); ?></p>
                            <?php } ?>
                        </td>
                        <td align="center">
                            <input type="text" name="txt_displayorder<?php print($int_cnt);?>" maxlength="5" value="<?php print($rs_list->fields("displayorder"));?>" class="form-control input-sm text-center" >
                            <input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>">
                        </td>
                        <?php $str_image="";
                        if(strtoupper($rs_list->fields("visible"))=="YES")
                        {   $str_image=$STR_LINK_ICON_PATH_VISIBLE;
                            $str_class="btn btn-warning btn-xs";
                            $str_title=$STR_HOVER_VISIBLE;
                        }
                        else
                        {   $str_image=$STR_LINK_ICON_PATH_INVISIBLE;
                            $str_class="btn btn-default active btn-xs";
                            $str_title=$STR_HOVER_INVISIBLE;
                        }
                        ?>
                        <td align="center" class="text-center">
                            <a class="<?php print($str_class); ?>" href="./item_visible_p.php?pkid=<?php print($rs_list->fields("pkid"));?>"  title="<?php print($str_title); ?>"><?php print($str_image);?></a>
                            <a class="btn btn-success btn-xs" href="./item_edit.php?pkid=<?php print($rs_list->fields("pkid"));?>" title="<?php print($STR_HOVER_EDIT);?>"><?php print $STR_LINK_ICON_PATH_EDIT; ?></a>
                            <a class="btn btn-danger btn-xs" href="./item_del_p.php?pkid=<?php print($rs_list->fields("pkid"));?>" onClick="return confirm_delete();" title="<?php print($STR_HOVER_DELETE);?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>	
                        </td>
                    </tr>
                    <?php $int_cnt++; $rs_list->MoveNext(); } ?>
                    <tr>
                        <td></td><td></td>
                        <td><input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>"><?php print DisplayFormButton("SAVE",0); ?></td>
                        <td></td>
                    </tr>
                <?php }  ?>
                </tbody>
            </table>
        </form>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
<script language="JavaScript" src="./item_add.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>
