function frm_edit_validate()
{
    with(document.frm_edit)
    {
        /* if(trim(fileimage.value) == "")
        {
            alert("Please Select Image");
            fileimage.select();
            fileimage.focus();
            return false;
        }*/
        if(trim(txt_title.value) == "")
        {
            alert("Please enter title.");
            txt_title.select();
            txt_title.focus();
            return false;
        }
     /*   if(trim(hdn_existing_image.value) == "")
        {
            alert("Please Select Image");
            hdn_existing_image.select();
            hdn_existing_image.focus();
            return false;
        } */
     /*    if(trim(fileimage.value) != "")
        {
            if(checkExt(trim(fileimage.value))==false)
            {
                fileimage.select();
                fileimage.focus();
                return false;
            }
        }  */

     /*   if(trim(txt_url.value) != "")
        {
            if(!validateURL_v02(trim(txt_url.value)))	
            {
                alert("Please enter valid url.");
                txt_url.select();
                txt_url.focus();
                return false;					
            }
        } */
        /*if(trim(cbo_display_button.value) != "NON" && trim(txt_caption) == "")
        {
            alert("Please enter valid caption for selected button.");
            return false;
        }*/
    }
    return true;
}

function confirm_delete()
{
    if(confirm("Are you sure you want to delete this image?"))
    {
        if(confirm("Confirm Deletion: Click 'OK' to delete this image or click 'Cancel' to cancel deletion."))
        {
            return true;
        }
    }
    return false;
}

// JavaScript Document