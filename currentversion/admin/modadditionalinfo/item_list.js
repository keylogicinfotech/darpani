function frm_add_validate()
{
    with(document.frm_add)
    {
      /* if(trim(fileimage.value) == "")
        {
            alert("Please Select Image");
            fileimage.select();
            fileimage.focus();
            return false;
        } */
       if(trim(txt_title.value) == "")
        {
            alert("Please enter title.");
            txt_title.select();
            txt_title.focus();
            return false;
        }

       /*  if(trim(fileimage.value) != "")
        {
            if(checkExt(trim(fileimage.value))==false)
            {
                fileimage.select();
                fileimage.focus();
                return false;
            }
        }

        if(trim(txt_url.value) != "")
        {
            if(!validateURL_v02(trim(txt_url.value)))	
            {
                alert("Please enter valid url.");
                txt_url.select();
                txt_url.focus();
                return false;					
            }
        } */
        /*if(trim(cbo_display_button.value) != "NON" && trim(txt_caption) == "")
        {
            alert("Please enter valid caption for selected button.");
            return false;
        }*/
}
    return true;
}

function displayontop_click()
{
    with(document.frm_list)
    {
        action="item_displayontop_set_p.php";
        method="post";
        submit();
    }
}

function displayonhome_click()
{
    with(document.frm_list)
    {
        action="item_displayonhome_set_p.php";
        method="post";
        submit();
    }
}
function frm_list_check_displayorder()
{
with(document.frm_list)
{
    cnt=hdn_counter.value;
    for(i=1;i<cnt;i++)
    {
        if(isEmpty(eval("txt_displayorder" + i).value))
        {
            alert("Please enter display order.");
            eval("txt_displayorder" + i).focus();
            return false;
        }
        if(isNaN(eval("txt_displayorder" + i).value))
        {
            alert("Please enter numeric value for display order.");
            eval("txt_displayorder" + i).select();
            eval("txt_displayorder" + i).focus();
            return false;
        }
        if(eval("txt_displayorder" + i).value<0)
        {
            alert("Please enter positive integer for display order.");
            eval("txt_displayorder" + i).select();
            eval("txt_displayorder" + i).focus();
            return false;
        }
    }
		
}
return true;
}

function confirm_delete()
{
    if(confirm("Are you sure you want to delete this details."))
    {
        if(confirm("Confirm Deletion:Click 'OK' to delete this details or 'Cancel' to deletion."))
        {
                        return true;
        }
    }
    return false;
}

// JavaScript Document