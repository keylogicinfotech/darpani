<?php 
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "./item_app_specific.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_file_system.php";
#----------------------------------------------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_POST);exit;
//print_r($_FILES);exit;
$str_desc = "";
$str_header = "";
$int_pkid = "";
$str_hoverltext = "";
$str_newsupdates = "";
$str_tweetupdates = "";
$str_image = "";
$str_url = "";
$str_urltitle = "";
$str_button_caption = "";
$str_display_button = "NON";
$str_open = "YES";
$str_title = "";

if(isset($_POST['txt_title']))
{ $str_title = trim($_POST['txt_title']); }

if(isset($_POST['ta_desc']))
{
    $str_desc = trim($_POST['ta_desc']);
}
if(isset($_POST['txt_hover']))
{
    $str_hoverltext = trim($_POST['txt_hover']);
}
if(isset($_POST['ta_newsupdates']))
{
    $str_newsupdates = trim($_POST['ta_newsupdates']);
}
if(isset($_POST['ta_tweetupdates']))
{
        $str_tweetupdates = trim($_POST['ta_tweetupdates']);
}
if(isset($_POST['hdn_pkid']))
{
    $int_pkid = trim($_POST['hdn_pkid']);
}
if(isset($_FILES['fileimage']))
{
    $str_image = trim($_FILES['fileimage']['name']);
}
if(isset($_POST['txt_url']))
{
    $str_url = trim($_POST['txt_url']);
}
if(isset($_POST['txt_urltitle']))
{
    $str_titleurl = trim($_POST['txt_urltitle']);
}
$str_button_caption = "";
if(isset($_POST['txt_caption']))
{
    $str_button_caption = trim($_POST['txt_caption']);
}

$str_display_button = "NON";
if (isset($_POST["cbo_display_button"]))
{
    $str_display_button = trim($_POST["cbo_display_button"]);
}

if(isset($_POST['cbo_window']))
{
    $str_open = trim($_POST['cbo_window']);
}

$str_image_visible = "YES";
if (isset($_POST["cbo_visible"]))
{
    $str_image_visible = trim($_POST["cbo_visible"]);
}
	
$int_displayorder = 0;
if (isset($_POST["txt_displayorder"]))
{
    $int_displayorder = trim($_POST["txt_displayorder"]);
}
	//print_r($_POST); exit;

#----------------------------------------------------------------------------------------------------
# Duplication Check
if($str_title != "")
{
    $str_query_select = "";
    $str_query_select = "SELECT title FROM " .$STR_DB_TABLE_NAME. " WHERE title = '" . ReplaceQuote($str_title) . "' AND title != '' ";
    //print $str_query_dup;

    $rs_list_check_duplicate = GetRecordSet($str_query_select);
    if(!$rs_list_check_duplicate->eof()){
        CloseConnection();
        Redirect("item_list.php?msg=DU&type=E&#ptop");
        exit();
    }
}
#------------------------------------------------------------------------------------------------------------
#check all validation
if($str_title=="")
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}

/*if($str_image1_visible=="NO" && $str_image2_visible=="NO" && $str_image3_visible=="NO" && $str_image4_visible=="NO" && $str_image5_visible=="NO")
{
    CloseConnection();
    Redirect("item_list.php?msg=IINV&type=E");
    exit();
}*/
#------------------------------------------------------------------------------------------------------------
#Prepare query string to pass variables back
$str_redirect = "";
$str_redirect = "&title=".urlencode(RemoveQuote($str_title))."&desc=".urlencode(RemoveQuote($str_desc))."";
$str_redirect = $str_redirect."&window=".urlencode(RemoveQuote($str_open))."&url=".urlencode(RemoveQuote($str_url))."";
$str_redirect = $str_redirect."&captionbut=".urlencode(RemoveQuote($str_button_caption))."&displaybut=".urlencode(RemoveQuote($str_display_button));
#-----------------------------------------------------------------------------------------------------------------------------
if($str_image!="" )
{
    if(ValidateImageExtension($_FILES['fileimage']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
        CloseConnection();
        Redirect("item_list.php?msg=I&type=E");
        exit();
    }
}
/*else
{
    CloseConnection();
    Redirect("item_list.php?msg=IDE&type=E&pkid=".urlencode($int_pkid));
    exit();
}*/

/*if($str_desc =="" || $str_desc == "<br>")
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&pkid=".urlencode($int_pkid));
    exit();
}*/

if($str_url != "")
{
    if(!validateURL02($str_url))
    {
        CloseConnection();
        Redirect("item_list.php?msg=LINK&type=E&pkid=".urlencode($int_pkid));
        exit();
    }
}
#---------------------------------------------------------------------------------------------------------	
#upload image
$str_large_file_name="";
$str_large_path="";
if($str_image!="")
{
    $str_large_file_name = GetUniqueFileName()."_home_content.".strtolower(getextension($str_image));
    $str_large_path = trim($UPLOAD_IMG_PATH.$str_large_file_name);
    UploadFile($_FILES['fileimage']['tmp_name'],$str_large_path);
    //ResizeImage($str_large_path,$INT_HOME_CONTENT_WIDTH);
}	

//print ($str_large_path); exit;
#----------------------------------------------------------------------------------------------------------------------------------------
# Insert Query
$int_max_do=GetMaxValue($STR_DB_TABLE_NAME,"displayorder");

$str_query_insert = "INSERT INTO " .$STR_DB_TABLE_NAME. " SET title='".ReplaceQuote($str_title)."',";
//$str_query_insert .= "header='".ReplaceQuote($str_header)."',";
$str_query_insert .= "description='".ReplaceQuote($str_desc)."',";
$str_query_insert .= "hovertext='".ReplaceQuote($str_hoverltext)."',";
//$str_query_insert .= "newsupdates='".ReplaceQuote($str_newsupdates)."',";
//$str_query_insert .= "tweetupdates='".ReplaceQuote($str_tweetupdates)."',";
$str_query_insert .= "imagefilename='".ReplaceQuote($str_large_file_name)."',";
$str_query_insert .= "visible='".ReplaceQuote($str_image_visible)."',";
$str_query_insert .= "photourl='".ReplaceQuote($str_url)."',";
$str_query_insert .= "urltitle='".ReplaceQuote($str_titleurl)."',";
$str_query_insert .= "openurlinnewwindow='".ReplaceQuote($str_open)."',";
$str_query_insert .= "caption='".ReplaceQuote($str_button_caption)."',";
$str_query_insert .= "displaybutton='".ReplaceQuote($str_display_button)."',";
$str_query_insert .= "displayorder=".$int_max_do." ";

//print $str_query_insert; exit;
ExecuteQuery($str_query_insert);
#----------------------------------------------------------------------------------------------------------------------------------------	
#Writing data of table into XML file
WriteXml();
#----------------------------------------------------------------------------------------------------------------------------------------
#Close connection and redirect to it's relevant location
CloseConnection();
Redirect("item_list.php?msg=S&type=S");
exit();
?>