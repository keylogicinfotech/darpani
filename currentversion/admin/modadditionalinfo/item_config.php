<?php
$STR_TITLE_PAGE = "Additional Information List";

$UPLOAD_IMG_PATH="../../mdm/storeadditionalinfo/";
$INT_IMG_WIDTH = 550;
$INT_IMG_HEIGHT = 750;

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "t_store_additional_info"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY displayorder DESC "; 

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "cms_additional_info";

global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/additional.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/additional_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";	

$STR_MSG_LINK_TEXT = "You should change the text 'write your text here' in following code with your own text when you put this code anywhere.";
$STR_MSG_SELECT_CODE = "NOTE:  Click on <u>Select Code</u> text, then Copy/Paste it either on this or any other website to display to site visitors.";
?>
