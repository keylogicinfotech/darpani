<?php 
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "./item_app_specific.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------
#Get passed value and check whether all values are passed properly or not.			
$int_pkid = 0;
$str_mode = "";

if(isset($_GET["pkid"]))
{
    $int_pkid = trim($_GET["pkid"]);
}
if(!is_numeric($int_pkid) || $int_pkid == "" || $int_pkid < 0 )
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E&#potp");
    exit();
}
#---------------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT title,visible FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".trim($int_pkid);
//print $str_query_select;exit;
$rs_list = GetRecordSet($str_query_select);
if($rs_list->EOF()==true)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}	

$str_title = $rs_list->Fields("title");
$str_mode = $rs_list->Fields("visible");
	
#---------------------------------------------------------------------------------------------------------
#Set the value for variables
if(strtoupper($str_mode)=="YES")
{
    $str_set_text = "NO";
}
else
{
    $str_set_text = "YES";
}
#---------------------------------------------------------------------------------------------------------
# Update Query
$str_query_update = "";
$str_query_update = "UPDATE " .$STR_DB_TABLE_NAME. " SET visible='" . $str_set_text . "' WHERE pkid=" . $int_pkid;
//print $str_query_update;exit;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------
#write xml file
WriteXML();	
#---------------------------------------------------------------------------------------------------------
# Close connection and redirect to its relevant location
CloseConnection();
Redirect("item_list.php?msg=V&type=S&mode=".urlencode(RemoveQuote($str_set_text))."&#ptop");
exit();
?>