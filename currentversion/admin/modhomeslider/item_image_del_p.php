<?php
/*
	Module Name:- Master Modules - Manage Home Image
	File Name  :- cm_home_content_image_del_p.php
	Create Date:- 21-OCT-2016
	Initially Create By :- 015
	Update History:-
*/
#-----------------------------------------------------------------------------------------------------------------------------------------------------
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
	include "../includes/lib_file_system.php";
	include "./cm_home_content_app_specific.php";
	
	$int_pkid="";
	$str_title="";
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#	To check whether all values are passed properly or not.	
	if(isset($_GET["pkid"]))
	{
		$int_pkid=trim($_GET["pkid"]);
	}
	if($int_pkid=="")
	{
		CloseConnection();
		Redirect("cm_home_content_edit.php?msg=F&type=E");
		exit();
	}
	if (!is_numeric($int_pkid))
	{
		CloseConnection();
		Redirect("cm_home_content_edit.php?msg=F&type=E");
		exit();
	}
	
	$str_select_query="";
	$str_select_query="SELECT photofilename FROM cm_home_content WHERE pkid=".$int_pkid;
	//print $str_select_query; 
	$rs_visible=GetRecordset($str_select_query);
	$str_large_image=$rs_visible->Fields("photofilename");
#-------------------------------------------------------------------------------------------------------------------	
	#delete the images
	
	//print "<br/><br/>".$UPLOAD_HOME_IMAGE_PATH.trim($str_large_image);
	//exit;
	if(trim($str_large_image)!="")
	{
		DeleteFile($UPLOAD_HOME_IMAGE_PATH.trim($str_large_image));
	}
	
#---------------------------------------------------------------------------------------------------------------------
#	Delete query to delete record from cm_home_image_table.
$str_query_update = "";
$str_query_update="UPDATE cm_home_content SET photofilename='' WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------
#write xml file
WriteXML();			
#-----------------------------------------------------------------------------------------------------------------------------------------------------
	CloseConnection();
	Redirect("cm_home_content_edit.php?msg=DEL&type=S&pkid=".urlencode($int_pkid)."&tit=".urlencode(RemoveQuote($str_highlight)));
	exit();
?>