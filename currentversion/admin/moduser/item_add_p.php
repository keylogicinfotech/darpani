<?php
/*
File Name  :- item_add_p.php
Create Date:- MARCH2019
Intially Create By :- 0013
Update History:
*/
#---------------------------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_system.php";
include "./item_app_specific.php"; 
#--------------------------------------------------------------------------------------------------------------------
//print_r($_POST);exit;
# filter variables
$int_page="";
$int_cat_pkid="";
$str_name_key="";

if(isset($_POST["catid"]) && trim($_POST["catid"])!="" )
{
    $int_cat_pkid=trim($_POST["catid"]);
}
if(isset($_POST["key"]) && trim($_POST["key"])!="" )
{
    $str_name_key=trim($_POST["key"]);
}
#get paging data
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"])!="" && is_numeric($_POST["PagePosition"])==true && trim($_POST["PagePosition"])>0)
{
    $int_page= $_POST["PagePosition"];
}
else
{
    $int_page=1;
}
#----------------------------------------------------------------------------------------------------------------------
# get form data
$int_cat_id=0;
$str_loginid="";
if(isset($_POST["txt_loginid"]))
{
    $str_loginid=trim($_POST["txt_loginid"]);
}
$str_password="";
if(isset($_POST["txt_password"]))
{
    $str_password=trim($_POST["txt_password"]);
}
$str_name = "";
if(isset($_POST["txt_name"]))
{
    $str_name=trim($_POST["txt_name"]);
}
$str_email_id="";
if(isset($_POST["txt_email"]) && trim($_POST["txt_email"])!="")
{
    $str_email_id=trim($_POST["txt_email"]);
}
$str_address = "";
if(isset($_POST["ta_add"]))
{
    $str_address=trim($_POST["ta_add"]);
}
$str_usertype="";
if(isset($_POST["cbo_user"]))   
{
    $str_usertype=trim($_POST["cbo_user"]);
}
$int_referredby=0;
if(isset($_POST["cbo_referredby"]))   
{
    $int_referredby=trim($_POST["cbo_referredby"]);
}
$str_businessname = "";
if(isset($_POST["txt_businessname"]))
{
    $str_businessname=trim($_POST["txt_businessname"]);
}
$str_country="";
if(isset($_POST["cbo_country"]))   
{
    $str_country=trim($_POST["cbo_country"]);
}
$str_state="";
if(isset($_POST["cbo_state"]))   
{
    $str_state=trim($_POST["cbo_state"]);
}
$str_city="";
if(isset($_POST["txt_city"]))   
{
    $str_city=trim($_POST["txt_city"]);
}

$int_phoneno=0;
if(isset($_POST["txt_phoneno"]))
{
    $int_phoneno=trim($_POST["txt_phoneno"]);
}
$int_zipcode = 0;
if(isset($_POST["txt_zipcode"]))
{
    $int_zipcode=trim($_POST["txt_zipcode"]);
}
$int_mobno=0;
if(isset($_POST["txt_mobno"]))
{
    $int_mobno=trim($_POST["txt_mobno"]);
}

if(isset($_FILES['fileimage']))
{
    $str_image = trim($_FILES['fileimage']['name']);
}
#-----------------------------------------------------------------------------------------------------------------------
$str_filter  = "&PagePosition=".urlencode(RemoveQuote($int_page))."&key=".urlencode(RemoveQuote($str_name_key))."&catid=".urlencode(RemoveQuote($int_cat_pkid));
$querystring = $str_filter ."&loginid=".urlencode(RemoveQuote($str_loginid))."&password=".urlencode(RemoveQuote($str_password));
//----------------------------------------------------------------------------------------------------------------------
if($str_loginid=="" || $str_password=="" /*|| $str_model_name=="" || $str_email_id==""*/ /* || $int_cat_id==""   || $int_country_pkid==""*/)
{
    CloseConnection();
    Redirect("./item_list.php?msg=F&type=E&".$querystring."&#ptop"); 
    exit();
}

//if(is_valid_userid($str_loginid)==false)
if(validateEmail($str_loginid)==false)
{
    //CloseConnection();
    //Redirect("./item_list.php?msg=IUI&type=E&".$querystring."&#ptop"); 
    //Redirect("./item_list.php?msg=IEM&type=E&".$querystring."&#ptop"); 
    //exit();
}
//validate userpassword
if(is_valid_userpassword($str_password)==false)
{
    //CloseConnection();
    //Redirect("./item_list.php?msg=IUP&type=E&".$querystring."&#ptop"); 
    //exit();
}

//validate emailid
//if($str_email_id !=)
if(validateEmail($str_email_id)==false)
{
	//CloseConnection();
	//Redirect("./item_list.php?msg=IEM&type=E&".$querystring."&#ptop"); 
	//exit();
}

#-----------------------------------------------------------------------------------------------------------------------------
/*if($str_image!="" )
{
    if(ValidateImageExtension($_FILES['fileimage']['tmp_name'],$STR_IMG_FILE_TYPE_VALIDATION)==0)
    {
        CloseConnection();
        Redirect("item_list.php?msg=I&type=E");
        exit();
    }
}
else
{
    CloseConnection();
    Redirect("item_list.php?msg=IDE&type=E&pkid=".urlencode($int_pkid));
    exit();
}*/
#-------------------------------------------------------------------------------------------
//Check Duplicate details.
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE loginid='".ReplaceQuote($str_loginid)."'";
$rs_list_check_duplicate=GetRecordset($str_query_select);
if(!$rs_list_check_duplicate->eof())
{
    CloseConnection();
    Redirect("item_list.php?msg=DP&type=E&".$querystring."&login_id=".urlencode(RemoveQuote($str_loginid))."&#ptop");
    exit();
}
 #---------------------------------------------------------------------------------------------------------	
#upload image
$str_large_file_name="";
$str_large_path="";
if($str_image!="")
{
    $str_large_file_name = GetUniqueFileName()."_t.".strtolower(getextension($str_image));
    $str_large_path = trim($UPLOAD_IMG_PATH.$str_large_file_name);
    UploadFile($_FILES['fileimage']['tmp_name'],$str_large_path);
    //ResizeImage($str_large_path,$INT_HOME_CONTENT_WIDTH);
}	

//print ($str_large_path); exit;
//$str_query_insert .= "imagefilename='".ReplaceQuote($str_large_file_name)."',";
#----------------------------------------------------------------------------------------------------------------
$str_submitdate=date("Y-m-d H:i:s");
$str_approvedate=date("Y-m-d H:i:s");
$str_query_insert ="INSERT INTO " .$STR_DB_TABLE_NAME. " (loginid,password,name,emailid,address,phoneno,mobileno,isusertype,businessname,";
$str_query_insert.="countrypkid,statepkid,city,zipcode,imagefilename,registrationdatetime,visible,approved,allowlogin,referredbypkid)";
$str_query_insert.=" VALUES('".ReplaceQuote($str_loginid)."','".ReplaceQuote($str_password)."','".ReplaceQuote($str_name)."','".ReplaceQuote($str_email_id)."',";
$str_query_insert.="'".ReplaceQuote($str_address)."','".ReplaceQuote($int_phoneno)."','".ReplaceQuote($int_mobno)."','".ReplaceQuote($str_usertype)."','".ReplaceQuote($str_businessname)."','".ReplaceQuote($str_country)."','".ReplaceQuote($str_state)."','".ReplaceQuote($str_city)."','".ReplaceQuote($int_zipcode)."','".ReplaceQuote($str_large_file_name)."',";
$str_query_insert.="'".$str_submitdate."','YES',";
$str_query_insert.="'YES','YES',".$int_referredby.")";
//print $str_query_insert;exit;
ExecuteQuery($str_query_insert);
#-----------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("./item_list.php?msg=S&type=S".$str_filter); 
exit();				
#-----------------------------------------------------------------------------------------------------------------
?>
