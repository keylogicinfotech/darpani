<?php
/*
File Name  :- item_list.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_system.php";
include "./item_app_specific.php";
//print_r($_FILES); exit;

#-----------------------------------------------------------------------------------------------------------------------------
# filter variables
$int_page="";
$int_cat_pkid="";
$str_name_key="";

#get filter data
if(isset($_POST["catid"]) && trim($_POST["catid"])!="" )
{
    $int_cat_pkid=trim($_POST["catid"]);
}
if(isset($_POST["key"]) && trim($_POST["key"])!="" )
{
    $str_name_key=trim($_POST["key"]);
}
#get paging data
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"])!="" && is_numeric($_POST["PagePosition"])==true && trim($_POST["PagePosition"])>0)
{
    $int_page= $_POST["PagePosition"];
}
else
{
    $int_page=1;
}
$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page;

$int_pkid=0;
if(isset($_POST['pkid']))
{
    $int_pkid=trim($_POST['pkid']);
}
if($int_pkid=="" || $int_pkid==0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}

#-------------------------------------------------------------------------------------------------------------------------
#  initialize variables
$str_loginid="";
$str_password="";

if(isset($_POST["txt_loginid"]))
{ $str_loginid=trim($_POST["txt_loginid"]); }
if(isset($_POST["txt_password"]))
{ $str_password=trim($_POST["txt_password"]); }

$str_name = "";
if(isset($_POST["txt_name"]))
{
    $str_name=trim($_POST["txt_name"]);
}
$str_email_id="";
if(isset($_POST["txt_email"]) && trim($_POST["txt_email"])!="")
{
    $str_email_id=trim($_POST["txt_email"]);
}
$str_address = "";
if(isset($_POST["ta_add"]))
{
    $str_address=trim($_POST["ta_add"]);
}
$str_country="";
if(isset($_POST["cbo_country"]))   
{
    $str_country=trim($_POST["cbo_country"]);
}
$str_state="";
if(isset($_POST["cbo_state"]))   
{
    $str_state=trim($_POST["cbo_state"]);
}
$str_city="";
if(isset($_POST["txt_city"]))   
{
    $str_city=trim($_POST["txt_city"]);
}
$str_usertype="";
if(isset($_POST["cbo_user"]))   
{
    $str_usertype=trim($_POST["cbo_user"]);
}
$int_referredby=0;
if(isset($_POST["cbo_referredby"]))   
{
    $int_referredby=trim($_POST["cbo_referredby"]);
}
$str_businessname = "";
if(isset($_POST["txt_businessname"]))
{
    $str_businessname=trim($_POST["txt_businessname"]);
}
$int_mobno=0;
if(isset($_POST["txt_mobno"]))
{
    $int_mobno=trim($_POST["txt_mobno"]);
}
$int_phoneno=0;
if(isset($_POST["txt_phoneno"]))
{
    $int_phoneno=trim($_POST["txt_phoneno"]);
}
$int_zipcode = 0;
if(isset($_POST["txt_zipcode"]))
{
    $int_zipcode=trim($_POST["txt_zipcode"]);
}
if(isset($_FILES['fileimage']))
{
    $str_image = trim($_FILES['fileimage']['name']);
}
#--------------------------------------------------------------------------------------------------------------------------
# query string datda to forward
$querystring = $str_filter . "&pkid=".$int_pkid;
//PRINT $querystring; EXIT;   
#--------------------------------------------------------------------------------------------------------------------------
# check for null data
if($str_loginid=="" || $str_password=="" || $str_usertype=="")
{
    CloseConnection();
    Redirect("./item_edit.php?msg=F&type=E".$querystring."&#ptop"); 
    exit();
}

if(is_valid_userid($str_loginid)==false)
{
    CloseConnection();
    Redirect("./item_edit.php?msg=IUI&type=E&".$querystring."&#ptop"); 
    exit();
}

//validate userpassword
if(is_valid_userpassword($str_password)==false)
{ 
    //print "Kem CHHO"; exit;
    //CloseConnection();
    //Redirect("./item_edit.php?msg=IUP&type=E&".$querystring."&#ptop"); 
    //exit();
}
	

#-----------------------------------------------------------------------------------------------------------------
//Check Duplicate Seller Login Id.
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE loginid='".ReplaceQuote($str_loginid)."' AND pkid!=".$int_pkid;
//print($str_query_select);
//exit();
$rs_check_duplicate=GetRecordset($str_query_select);
if(!$rs_check_duplicate->eof())
{
    CloseConnection();
    Redirect("item_edit.php?msg=DP&type=E&login_id=".urlencode(RemoveQuote($str_loginid)).$querystring."&#ptop");
}
#---------------------------------------------------------------------------------------------------------	
#----------------------------------------------------------------------------------------------------	
$str_query_select="";
$str_query_select="SELECT imagefilename FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
$rs_list=GetRecordset($str_query_select);
if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_edit.php?msg=I&type=E");
    exit();
}

$str_thumb_file_name="";
$str_thumb_path="";
$str_thumb_file_name=$rs_list->fields("imagefilename");
if($str_image!="")
{
    #Delete old image
    if($str_thumb_file_name!="")
    {
        DeleteFile($UPLOAD_IMG_PATH.trim($str_thumb_file_name));
    }
    #Upload new image
    $str_thumb_file_name=GetUniqueFileName()."_t.".getextension($str_image);
    $str_thumb_path = trim($UPLOAD_IMG_PATH."/".$str_thumb_file_name);
    UploadFile($_FILES['fileimage']['tmp_name'],$str_thumb_path);
    //ResizeImage($str_thumb_path,$INT_IMG_THUMB_WIDTH);
}	
//---------------------------------------------------------------------------------------------------------------------
//update query
$str_query_update="UPDATE " .$STR_DB_TABLE_NAME. " SET loginid='".ReplaceQuote($str_loginid)."',";
$str_query_update.="password='".ReplaceQuote($str_password)."',";
$str_query_update.="name='".ReplaceQuote($str_name)."',";
$str_query_update.="businessname='".ReplaceQuote($str_businessname)."',";
$str_query_update.="emailid='".ReplaceQuote($str_email_id)."',";
$str_query_update.="address='".ReplaceQuote($str_address)."',";
$str_query_update.="isusertype='".ReplaceQuote($str_usertype)."',";
$str_query_update.="referredbypkid='".ReplaceQuote($int_referredby)."',";
$str_query_update.="mobileno='".ReplaceQuote($int_mobno)."',";
$str_query_update.="countrypkid=".ReplaceQuote($str_country).",";
$str_query_update.="statepkid=".ReplaceQuote($str_state).",";
$str_query_update.="city='".ReplaceQuote($str_city)."',";
$str_query_update.="zipcode=".ReplaceQuote($int_zipcode).",";
$str_query_update.="phoneno='".ReplaceQuote($int_phoneno)."',";
$str_query_update.="imagefilename='".ReplaceQuote($str_thumb_file_name)."'";
$str_query_update.=" WHERE pkid=". $int_pkid;	

//print($str_query_update); exit();
ExecuteQuery($str_query_update);

#write to xml file
//Model_WriteXml($int_model_pkid);	

#-----------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("./item_list.php?msg=U&type=S".$querystring."&#ptop"); 
exit();
#-----------------------------------------------------------------------------------------------------------------
?>
