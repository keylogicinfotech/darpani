<?php
/*
File Name  :- item_order_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_config.php";
include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#initialize variables
$int_cat_pkid = "";
if(isset($_POST["catid"]) && trim($_POST["catid"]) != "" )
{ $int_cat_pkid = trim($_POST["catid"]); }

$str_name_key = "";
if(isset($_POST["key"]) && trim($_POST["key"]) != "" )
{ $str_name_key = trim($_POST["key"]); }

# get data for paging
$int_page = "";
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"]) != "" && is_numeric($_POST["PagePosition"]) && trim($_POST["PagePosition"]) > 0)
{ $int_page = $_POST["PagePosition"]; }
else
{ $int_page = 1; }

$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page;
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
$int_cnt="";
if (isset($_POST["hdn_counter"]))
{
        $int_cnt=trim($_POST["hdn_counter"]);
}	
if($int_cnt=="" || $int_cnt<0 || !is_numeric($int_cnt))
{
        CloseConnection();
        Redirect("item_list.php?msg=F&type=E&#ptop");
        exit();
}
#----------------------------------------------------------------------------------------------------
#Update data in table
for($i=1;$i<$int_cnt;$i++)
{
	if(trim($_POST["txt_commission".$i]) != "" && trim($_POST["txt_commission".$i]) >=0 && is_numeric(trim($_POST["txt_commission".$i]))==true  && trim($_POST["hdn_pkid".$i])!="" && trim($_POST["hdn_pkid".$i])>0 && is_numeric(trim($_POST["hdn_pkid".$i]))==true)
        {
            $str_query_update="UPDATE ".$STR_DB_TABLE_NAME." SET commission=".trim($_POST["txt_commission".$i])." WHERE isusertype='".$STR_TITLE_DROPDOWN_OPTION03."' AND pkid=".trim($_POST["hdn_pkid".$i]);
            ExecuteQuery($str_query_update);
        }
}
#----------------------------------------------------------------------------------------------------
#write to xml file
//WriteXml();
#----------------------------------------------------------------------------------------------------
#Close connection and redirect to listing page	
CloseConnection();
Redirect("item_list.php?type=S&msg=CMS".$str_filter);
exit();
#----------------------------------------------------------------------------------------------------
?>
