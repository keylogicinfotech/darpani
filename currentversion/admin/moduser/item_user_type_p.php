<?php
/*
File Name  :- item_list.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_app_specific.php";
#get the data
#-----------------------------------------------------------------------------------------------------------------------
# filter variables
$int_page="";
$int_cat_pkid="";
$str_name_key="";

#---------------------------------------------------------------------------------------------------------------------------------------------------------------
#get filter data
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{
    $int_cat_pkid=trim($_GET["catid"]);
}
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{
    $str_name_key=trim($_GET["key"]);
}
#get paging data
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"])==true && trim($_GET["PagePosition"])>0)
{
    $int_page= $_GET["PagePosition"];
}
else
{
    $int_page=1;
}
$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page;

$int_pkid=0;
if(isset($_GET['pkid']))
{
    $int_pkid=$_GET['pkid'];
}
if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F".$str_filter);
    exit();
}
#select query to get the details of pkid from t_model table
$str_select_query="";
$str_select_query="SELECT isusertype FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
$rs_list_wholesaler=GetRecordset($str_select_query);
//$str_model_name=$rs_list_wholesaler->Fields("modelname");

//	$rs_list_wholesaler
//	$str_wholesaler
if($rs_list_wholesaler->eof())
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F".$str_filter);
    exit();
}
	
$str_wholesaler=$rs_list_wholesaler->Fields("isusertype");
if(strtoupper($str_wholesaler)=='WHOLESALER')
{
//    print("r1");exit;
    $str_wholesaler="REGULAR";
    $str_wholesaler_title="REGULAR";
}
else if(strtoupper($str_wholesaler)=='REGULAR')
{
//    print("w1");exit;
    $str_wholesaler="WHOLESALER";
    $str_wholesaler_title="WHOLESALER";
}
#-----------------------------------------------------------------------------------------------------------------------------
#update query to change the mode
$str_select_update="";
$str_select_update="UPDATE " .$STR_DB_TABLE_NAME. " SET isusertype='" .ReplaceQuote($str_wholesaler). "', discount=0.00 WHERE pkid=".$int_pkid;
//print $str_select_update; exit;
ExecuteQuery($str_select_update);
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file
####WriteXml();
#----------------------------------------------------------------------------------------------------------------------------	
CloseConnection();
Redirect("./item_list.php?msg=US&type=S&mode=".urlencode(RemoveQuote($str_wholesaler_title)).$str_filter); 
exit();
	
?>
