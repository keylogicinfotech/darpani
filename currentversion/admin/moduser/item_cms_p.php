<?php
/*
File Name  :- item_cms_p.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_xml.php";
#----------------------------------------------------------------------------------------------------------------------------------------
$int_pkid="";
$str_bdesc="";
$str_ldesc="";
$str_rdesc="";
$str_usage_terms="";
$str_posting_terms="";
$str_visible="";
$str_visible1="";
$str_visible2="";
if(isset($_POST['ta_bdesc']))
{
    $str_bdesc=trim($_POST['ta_bdesc']);
}
if(isset($_POST['ta_ldesc']))
{
    $str_ldesc=trim($_POST['ta_ldesc']);
}
if(isset($_POST['ta_rdesc']))
{
    $str_rdesc=trim($_POST['ta_rdesc']);
}
if(isset($_POST['hdn_pkid']))
{
    $int_pkid=trim($_POST['hdn_pkid']);
}
//print $int_pkid;exit;
if(isset($_POST["ta_usage_terms"]))
{
$str_usage_terms=trim($_POST["ta_usage_terms"]);
}
if(isset($_POST["ta_posting_terms"]))
{
    $str_posting_terms=trim($_POST["ta_posting_terms"]);
}
if(isset($_POST['cbo_visible']))
{
    $str_visible=trim($_POST['cbo_visible']);
}
if(isset($_POST['cbo_visible1']))
{
    $str_visible1=trim($_POST['cbo_visible1']);
}
if(isset($_POST['cbo_visible2']))
{
    $str_visible2=trim($_POST['cbo_visible2']);
}
$str_photoid_desc="";
$str_autosign_desc="";
if(isset($_POST["ta_photoid_desc"]))
{
    $str_photoid_desc=trim($_POST["ta_photoid_desc"]);
}
if(isset($_POST["ta_autosign_desc"]))
{
    $str_autosign_desc=trim($_POST["ta_autosign_desc"]);
}
        
        
//if( $str_bdesc=="" )
//{
//	CloseConnection();
//	Redirect("./item_list.php?msg=F&type=E"); 
//	exit();
//}
#---------------------------------------------------------------------------------------------------------------------------------------	
#select query to get details from cm_guest_book table
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_CMS. " ";
//print $str_query_select;exit;
$rs_list=GetRecordset($str_query_select);

if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_cms.php?msg=F&type=E");
    exit();
}
        //print $int_pkid; exit;
#----------------------------------------------------------------------------------------------------------------------------------------
#update query to set values in table
#----------------------------------------------------------------------------------------------------------------------------------------
#update query to set values in cm_guest_book table
$str_query_update="";
$str_query_update="UPDATE " .$STR_DB_TABLE_NAME_CMS. " SET description='".ReplaceQuote($str_ldesc)."',";
//$str_query_update.="description='".ReplaceQuote($str_bdesc)."',";
$str_query_update.="description2='".ReplaceQuote($str_rdesc)."',";
$str_query_update.="description3='".ReplaceQuote($str_usage_terms)."',";
$str_query_update.="description4='".ReplaceQuote($str_posting_terms)."', ";
$str_query_update.="description5='".ReplaceQuote($str_photoid_desc)."', ";
$str_query_update.="description6='".ReplaceQuote($str_autosign_desc)."', ";
$str_query_update.="visible='".ReplaceQuote($str_visible)."', ";
$str_query_update.="visible1='".ReplaceQuote($str_visible1)."', ";
$str_query_update=$str_query_update."visible2='".ReplaceQuote($str_visible2)."' WHERE pkid=".$int_pkid;
//print($str_query_update); exit();
ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------------------------------------------	
#Writing data of table into XML file
#creating recordset.
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME_CMS. "";
$rs_list = GetRecordSet($str_query_select);

#storing recordset data into array.
if(!$rs_list->eof())
{
    $arr=array();
    $arr1=array();
    $i=0;
    while(!$rs_list->eof())
    {
        $arr[$i]=$rs_list->fields("itemkey")."_buttondesc";
        $arr1[$i]=$rs_list->fields("buttondesc");
        $i=$i+1;

        $arr[$i]=$rs_list->fields("itemkey")."_login";
        $arr1[$i]=$rs_list->fields("description");
        $i=$i+1;

        $arr[$i]=$rs_list->fields("itemkey")."_registration";
        $arr1[$i]=$rs_list->fields("description2");
        $i=$i+1;

        $arr[$i]=$rs_list->fields("itemkey")."_usageterms";
        $arr1[$i]=$rs_list->fields("description3");
        $i=$i+1;

        $arr[$i]=$rs_list->fields("itemkey")."_postingterms";
        $arr1[$i]=$rs_list->fields("description4");
        $i=$i+1;

        $arr[$i]=$rs_list->fields("itemkey")."_photoid";
        $arr1[$i]=$rs_list->fields("description5");
        $i=$i+1;

        $arr[$i]=$rs_list->fields("itemkey")."_autosignature";
        $arr1[$i]=$rs_list->fields("description6");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_visible";
        $arr1[$i]=$rs_list->fields("visible");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_visible1";
        $arr1[$i]=$rs_list->fields("visible1");
        $i=$i+1;
        
        $arr[$i]=$rs_list->fields("itemkey")."_visible2";
        $arr1[$i]=$rs_list->fields("visible2");
        $i=$i+1;
        
        
        
        $rs_list->MoveNext();
    }		
    #writting data to the file
    writeXmlFile($XML_FILE_PATH_CMS,$XML_ROOT_TAG_CMS,$arr,$arr1);
    }
#----------------------------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_cms.php?msg=E&type=S");
exit();
?>