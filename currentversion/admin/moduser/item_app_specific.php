<?php
#write to xml file
include "../../includes/lib_xml.php";
include "item_config.php";

function WriteXml()
{
    global $XML_SURGEONS_FILE_PATH;
    global $XML_SURGEONS_ROOT_TAG;
    global $STR_DB_TABLE_NAME;

    $arr1=array();
    $arr2=array();
    $arr3=array();
    $arr4=array();
    $select_query="";
    $select_query="SELECT * FROM t_surgeons WHERE visible='YES' ORDER BY displayorder DESC";
    $rs_list=GetRecordset($select_query);
    $i=0;
    while(!$rs_list->eof())
    {
        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="fullname";
        $arr4[$i]=$rs_list->fields("fullname");
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="clinic";
        $arr4[$i]=$rs_list->fields("address");
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="email";
        $arr4[$i]=$rs_list->fields("emailid");
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="phone";
        $arr4[$i]=$rs_list->fields("phone");
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="url";
        $arr4[$i]=$rs_list->fields("url");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="countrypkid";
        $arr4[$i]=$rs_list->fields("countrypkid");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="statepkid";
        $arr4[$i]=$rs_list->fields("statepkid");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="city";
        $arr4[$i]=$rs_list->fields("city");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="openurlinnewwindow";
        $arr4[$i]=$rs_list->fields("openurlinnewwindow");
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="visible";
        $arr4[$i]=$rs_list->fields("visible");
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="submitdatetime";
        $arr4[$i]=$rs_list->fields("submitdatetime");
        $i=$i+1;

        $rs_list->MoveNext();		
    }
    writeXmlFile($XML_SURGEONS_FILE_PATH,$XML_SURGEONS_ROOT_TAG,$arr1,$arr2,$arr3,$arr4);
}

## To draw procedure progress chart 
/*function GetComparisonChart($width,$color,$totalcount)
{   
    */
	/*$str="<table width='$width' border='0' cellspacing='0' cellpadding='0'>";
	$str=$str."<tr>";
	$str=$str."<td valign='baseline' width='".$width."' height='5' style='color: #FFF' bgcolor='". $color ."' class='progress-bar-info progress-bar-striped' align='center'><b>".$totalcount."</b></td>";
	$str=$str."</tr>";
	$str=$str."</table>";
	return $str;
        print $width;*/
        
        
        //print $temp;
     /*   $str="<div class='progress' style='margin-bottom:0;'>";
        $str.= "<div class='progress-bar progress-bar-success progress-bar-striped' role='progressbar' aria-valuenow='' aria-valuemin='0' aria-valuemax='100' style='width:$width%;'><b>$".$totalcount."</b>
            </div>";
        $str.= "</div>"; */
    
        /*$str = "<div class='progress' style='vertical-align:middle;'>";
        $str = $str."&nbsp;<div class='progress-bar progress-bar-success' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width:'20%'>$ ".$totalcount."";
          
        $str = $str."</div>";
        $str = $str."</div>";*/
/*        
      return $str;
}
*/
## To draw vote chart 
function GetComparisonChartView($width,$color,$totalcount)
{
    $str="<table width='$width' border='0' cellspacing='0' cellpadding='0'>";
    $str=$str."<tr>";
    $str=$str."<td valign='baseline' width='".$width."' height='5' style='color: #FFF' bgcolor='". $color ."' class='progress-bar-info progress-bar-striped' align='center'><b>".$totalcount."</b></td>";
    $str=$str."</tr>";
    $str=$str."</table>";
    return $str;        
}

#-------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------
function Schedule_Synchronize($mdlpkid)
{
    $int_mdlkid=$mdlpkid;
    if($int_mdlkid<0 || !is_numeric($int_mdlkid) || $int_mdlkid=="")
    {
        exit();
    }
    #select query to get details of no of statistic from tr_model_resume table
    $str_select="";
    $str_select="SELECT count(schedulesrno) total FROM tr_model_schedule where modelpkid=".$int_mdlkid;
    $rs_select=GetRecordset($str_select);
    $int_schedule_count=0;
    if(!$rs_select->eof())
    {
        $int_schedule_count=$rs_select->fields("total");
    }
    $updatedate="";
    $updatedate=date('Y-m-d');
    #update query to update the no. of resume
    $str_query_update = "";
    $str_query_update = "UPDATE " .$STR_DB_TABLE_NAME. " ";
    $str_query_update .= "SET noofschedule =".$int_schedule_count .", ";
    $str_query_update .= "lastupdatedate='". ReplaceQuote($updatedate) ."' ";
    $str_query_update .= "WHERE modelpkid=".$int_mdlkid;
    ExecuteQuery($str_query_update);	
}

#---------------------------------------------------------------------------------------------------------------------------------
function Comment_Synchronize($mdlpkid)
{
    $int_mdlkid=$mdlpkid;
    //print $int_mdlkid; exit;
    if($int_mdlkid<0 || !is_numeric($int_mdlkid) || $int_mdlkid=="")
    {
        exit();
    }
    #select query to get details of no of statistic from tr_modelsearch_comment table
    $str_select="";
    $str_select="SELECT count(pkid) total FROM tr_blog where modelpkid=".$int_mdlkid." AND blogtype='B'";
    $rs_select=GetRecordset($str_select);
    $int_comment_count=0;
    if(!$rs_select->eof())
    {
        $int_comment_count=$rs_select->fields("total");
    }
    $updatedate=""; 
    $updatedate=date('Y-m-d');
    #update query to update the no. of comment
    $str_query_update = "";
    $str_query_update = "UPDATE " .$STR_DB_TABLE_NAME. " ";
    $str_query_update .= "SET noofcomment =".$int_comment_count .", ";
    $str_query_update .= "lastupdatedate='". ReplaceQuote($updatedate) ."' ";
    $str_query_update .= "WHERE modelpkid=".$int_mdlkid;
    ExecuteQuery($str_query_update);	
}

#---------------------------------------------------------------------------------------------------------------------------------
function Comment_WriteXml($int_modelpkid)
{
    $int_mpkid=$int_modelpkid;
    //print $int_mpkid; exit;
    if($int_mpkid<0 || !is_numeric($int_mpkid) || $int_mpkid=="")
    {
            exit();
    }

    global $XML_COMMON_PATH;
    global $XML_COMMENT_ROOT_TAG;
    $str_xml_file_path=$XML_COMMON_PATH.$int_mpkid."/xmlcontentfiles/comment.xml";
    //print $str_xml_file_path;exit;

    $arr1=array();
    $arr2=array();
    $arr3=array();
    $arr4=array();
    $select_query="";
    $select_query="SELECT * FROM tr_blog WHERE visible='YES' AND approved='YES' and modelpkid=".$int_mpkid." ORDER BY displayorder";
    $rs_list=GetRecordset($select_query);
    $i=0;
    while(!$rs_list->eof())
    {
        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="blogpkid";
        $arr4[$i]=$rs_list->fields("blogpkid");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="modelpkid";
        $arr4[$i]=$rs_list->fields("modelpkid");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="memberpkid";
        $arr4[$i]=$rs_list->fields("memberpkid");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="personname";
        $arr4[$i]=$rs_list->fields("personname");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="designation";
        $arr4[$i]=$rs_list->fields("designation");  
        $i=$i+1;
		
        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="emailid";
        $arr4[$i]=$rs_list->fields("emailid");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="location";
        $arr4[$i]=$rs_list->fields("location");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="title";
        $arr4[$i]=$rs_list->fields("title");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="description";
        $arr4[$i]=$rs_list->fields("description");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="imagefilename";
        $arr4[$i]=$rs_list->fields("imagefilename");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="urltitle";
        $arr4[$i]=$rs_list->fields("urltitle");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="url";
        $arr4[$i]=$rs_list->fields("url");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="openinnewwindow";
        $arr4[$i]=$rs_list->fields("openinnewwindow");  
        $i=$i+1;

        $arr1[$i]="P_".$rs_list->fields("pkid");
        $arr2[$i]="";
        $arr3[$i]="displayasnew";
        $arr4[$i]=$rs_list->fields("displayasnew");  
        $i=$i+1;

        $rs_list->MoveNext();
    }
    writeXmlFile($str_xml_file_path,$XML_COMMENT_ROOT_TAG,$arr1,$arr2,$arr3,$arr4);
}
?>
