<?php
/*
File Name  :- item_cms.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------
# Select query
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME_CMS. " ";
$rs_list = GetRecordSet($str_query_select); 
#----------------------------------------------------------------------------------------------------------------------------------------------------
# filter variables
$int_page="";
$int_cat_pkid="";
$str_key="";
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#get filter data
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{
    $int_cat_pkid=trim($_GET["catid"]);
}
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{
    $str_key=trim($_GET["key"]);
}
#get paging data
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"])==true && trim($_GET["PagePosition"])>0)
{
    $int_page= $_GET["PagePosition"];
}
else
{
    $int_page=1;
}
$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_key."&PagePosition=".$int_page;
#--------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Diplay message.
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("E"): $str_message = $STR_MSG_ACTION_EDIT; break;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?>  : <?php print($STR_TITLE_PAGE); ?>  (<?php print($STR_TITLE_MANAGE_CONTENT); ?>): <?php print($STR_TITLE_EDIT); ?></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body onLoad="myFunction()">
<!-- Page Content -->
<a name="ptop"></a>
<div class="container">
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-6 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Back to State List"><i class="fa fa-chevron-left"></i>&nbsp;Back</a><?php */?>
                <a href="./item_list.php?<?php print($str_filter); ?>" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
           <?php /* ?>     <a href="./item_features.php?<?php print($str_filter); ?>" class="btn btn-default" title="<?php print($STR_TITLE_MODEL_FEATURES);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_MODEL_FEATURES);?></a>   <?php */ ?>
            </div>
        </div>
        <div class="col-md-6" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)</h3></div>
        </div>
        <hr>
        <?php if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); } ?>
        <div class="row padding-10">
            <div class="col-md-12">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <i class="fa  fa-edit "></i>&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b>
                                <?php print($STR_LINK_HELP); ?>
                            </h4>
                        </div>
                        <div id="collapse01" class="panel-collapse collapse in">
                            <form name="frm_edit" action="item_cms_p.php"  method="post">
                            <div class="panel-body">
                                
                                    <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>

                              <?php /* ?>      <div class="form-group">
                                        <label>Profile Page Button Description </label>
                                        <input type="text" id="ta_bdesc" name="ta_bdesc" value="<?php print(RemoveQuote($rs_list->fields("buttondesc"))); ?>" rows="10" cols="90" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>">
                                    </div> <?php */ ?>

                                    <div class="form-group">
                                        <label>Login Description </label>
                                        <textarea id="ta_ldesc" name="ta_ldesc" rows="10" cols="90" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>"><?php print(RemoveQuote($rs_list->fields("description"))); ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Visible</label><span class="text-help-form"> * (<?php print($STR_MSG_VISIBLE);?>) </span>
                                        <select name="cbo_visible" class="form-control input-sm" >
                                        <option value="YES" <?php print(CheckSelected("YES",strtoupper($rs_list->fields("visible"))));?>>YES</option>
                                        <option value="NO" <?php print(CheckSelected("NO",strtoupper($rs_list->fields("visible"))));?>>NO</option>
                                        </select>
                                    </div><br/>
                                    <div class="form-group">
                                        <label>Registration Description </label>
                                        <textarea id="ta_rdesc" name="ta_rdesc" rows="10" cols="90" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>"><?php print(RemoveQuote($rs_list->fields("description2"))); ?></textarea>
                                    </div> 
                                    <div class="form-group">
                                        <label>Visible</label><span class="text-help-form"> * (<?php print($STR_MSG_VISIBLE);?>) </span>
                                        <select name="cbo_visible1" class="form-control input-sm" >
                                        <option value="YES" <?php print(CheckSelected("YES",strtoupper($rs_list->fields("visible1"))));?>>YES</option>
                                        <option value="NO" <?php print(CheckSelected("NO",strtoupper($rs_list->fields("visible1"))));?>>NO</option>
                                        </select>
                                    </div><br/>
                                   <div class="form-group">
                                        <label>Home Description</label>
                                        <textarea id="ta_usage_terms" name="ta_usage_terms" rows="10" cols="90" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>"><?php print(RemoveQuote($rs_list->fields("description3"))); ?></textarea>
                                    </div>
                                    <div class="form-group">
                                    <label>Visible</label><span class="text-help-form"> * (<?php print($STR_MSG_VISIBLE);?>) </span>
                                    <select name="cbo_visible2" class="form-control input-sm" >
                                    <option value="YES" <?php print(CheckSelected("YES",strtoupper($rs_list->fields("visible2"))));?>>YES</option>
                                    <option value="NO" <?php print(CheckSelected("NO",strtoupper($rs_list->fields("visible2"))));?>>NO</option>
                                  </select>
                                    </div><br/>
                               <?php /* ?>      <div class="form-group">
                                        <label>Photo Id Verification Description</label>
                                        <textarea id="ta_photoid_desc" name="ta_photoid_desc" rows="10" cols="90" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>"><?php print(RemoveQuote($rs_list->fields("photoidverificationdesc"))); ?></textarea>
                                    </div>
                            <?php /* ?>         <div class="form-group">
                                        <label>Auto Signature Description</label>
                                        <textarea id="ta_autosign_desc" name="ta_autosign_desc" rows="10" cols="90" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_DESC; ?>"><?php print(RemoveQuote($rs_list->fields("autosignaturedesc"))); ?></textarea>
                                    </div> <?php */ ?>
                                    
                            
                                    <input type="hidden" name="hdn_pkid" value="<?php print($rs_list->fields("pkid")); ?>" >
                                    <?php print DisplayFormButton("EDIT",0); ?>&nbsp;<?php print DisplayFormButton("RESET",0); ?>
                                     </div>
                                 </form>
                               
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php include "../../includes/help_for_edit.php"; ?>
        </div>
    
    <!-- /.container -->											

<?php include "../../includes/include_files_admin.php"; ?>
    <?php include($STR_ADMIN_FOOTER_PATH); ?>
<!--<script language="JavaScript" src="./item_cms.js" type="text/javascript"></script>-->
<script language="javascript" type="text/javascript">
function myFunction() {
    ta_desc.focus();
}
</script>
<script type="text/javascript" src="../../js/richetxteditor.js"></script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_ldesc');
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_rdesc');
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_usage_terms');
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_autosign_desc');
        
  });
	// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
	// convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_ldesc');
             new nicEditor({fullPanel : true}).panelInstance('ta_rdesc');
             new nicEditor({fullPanel : true}).panelInstance('ta_usage_terms');
             new nicEditor({fullPanel : true}).panelInstance('ta_autosign_desc');
        		// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>
</body></html>
