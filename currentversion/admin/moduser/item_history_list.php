<?php
/*
File Name  :- item_list.php
Create Date:- MARCH2019
Intially Create By :- 0013
Update History:
*/
#--------------------------------------------------------------------------------------
# include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#--------------------------------------------------------------------------------------
//$STR_TITLE_HISTORY = "IP Address History";
#initialize variables
$int_page="";
$int_cat_pkid="";
$str_name_key="";
#---------------------------------------------------------------------------------------
# get filter data
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{
    $int_cat_pkid=trim($_GET["catid"]);
}
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{
    $str_name_key=trim($_GET["key"]);
}

if(isset($_GET["loginid"]) && trim($_GET["loginid"])!="" )
{
    $str_id=trim($_GET["loginid"]);
}
if(isset($_GET["pkid"]) && trim($_GET["pkid"])!="" )
{
    $int_pkid=trim($_GET["pkid"]);
}

#   get paging data
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"])==true && trim($_GET["PagePosition"])>0)
{
    $int_page= $_GET["PagePosition"];
}
else
{
    $int_page=1;
}
$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page;
#------------------------------------------------------------------------------------------------
#get paging/filter parameters
/*$page=1;
if (isset($_GET["PagePosition"])) { $page = trim($_GET["PagePosition"]); }
$key="";
if (isset($_GET["key"])) { $key = trim($_GET["key"]); }

$int_cat_id="0"; // To display all models
if (isset($_GET["catid"])) { $int_cat_id = trim($_GET["catid"]); }
*/
$str_query_filter="";
if($str_name_key != "") {
    $str_query_filter=" WHERE UPPER(loginid) like '".$str_name_key."%'"; 
}

#------------------------------------------------------------------------------------------
$int_count=0;
$str_query_select="";
# Select Query to get total no of record
$str_query_select="";
$str_query_select="SELECT COUNT(DISTINCT pkid) as total_record FROM " .$STR_DB_TABLE_NAME_HISTORY.$str_query_filter;
$rs_list_total=GetRecordSet($str_query_select);
$int_total_record = $rs_list_total->fields("total_record");

$int_record_per_page=$INT_RECORD_PER_PAGE;				
$int_total_page=ceil($int_total_record / $int_record_per_page); 
if ($int_page > $int_total_page)
{ $int_page=$int_total_page; }
if($int_page < 1)
{ $int_page=1; }
$int_limit_start=($int_page -1)* $int_record_per_page;	


$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME. " a";
$str_query_select .=" ".$str_query_filter; 
$str_query_select .=" ORDER BY pkid DESC";
//$str_query_select .= " LIMIT ".$int_limit_start.",".$int_record_per_page;
$rs_list_member = GetRecordSet($str_query_select);

//$str_id = "";
//$str_id = $rs_list_member->fields("loginid");
#------------------------------------------------------------------------------------------
$str_query_filter_id="";
if($str_id != "") {
    $str_query_filter_id=" WHERE loginid like '".$str_id."%'"; 
}
//print "filter:" .$str_id;
$str_select_query="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_HISTORY. $str_query_filter_id .$STR_DB_TABLE_NAME_HISTORY_ORDER_BY;
//print $str_query_select; 
 $rs_list = GetRecordSet($str_query_select);      
//print $str_query_select;
if($rs_list->Count()>0)
{ $srno=($int_page -1)* $int_record_per_page +1; }


#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.    
$str_type="";
$str_message="";
$str_title="";
$str_mode="";
if(isset($_GET['tit']))
{
    $str_title=trim($_GET['tit']);
}
if(isset($_GET['mode']))
{
    $str_mode=trim($_GET['mode']);
}
#Get message type.
if(isset($_GET['type']))
{
    switch(trim($_GET['type']))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Initialization of variables used for message display.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("IE"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
        case("URL"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;
        case("IEM"): $str_message = $STR_MSG_ACTION_INVALID_EMAIL; break;
        case("AL"): $str_message = $STR_MSG_ACTION_LOGIN; break;
        case("AP"): $str_message = $STR_MSG_ACTION_APPROVE; break;
//        case("ISUK"): $str_message = $STR_MSG_ACTION_INVALID_SHORTENURLKEY; break;
        case("IUI"): $str_message = $STR_MSG_ACTION_INVALID_LOGINID; break;
        case("IUP"): $str_message = $STR_MSG_ACTION_INVALID_PASSWORD; break;
        case("DPID"): $str_message = $STR_MSG_ACTION_INVALID_PROFILEID; break;
    }
}	
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_HISTORY);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include("../../includes/adminheader.php"); ?>
        <div class="row padding-10">
            <div class="col-md-3 button_space">
		<div class="btn-group" role="group" aria-label="...">
                     <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?> ">
                    <i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?>
                </a>
		</div>
            </div>
            <div class="col-md-9" align="right" ><h3><?php print($STR_TITLE_HISTORY); ?></h3></div>
	</div>
	<hr>
	<?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
	<div class="row padding-10">
    	<div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                           <i class="" ></i>&nbsp;<b><?php // print($STR_TITLE_EDIT); ?></b> 
                          <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    
                    </div>  
                </div>
            </div>
	</div>
     
<!--    <div class="row padding-10">
    </div>-->
    <div class="table-responsive">
	<form name="frm_list" method="post">
            <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th width="4%">Sr. #</th>
                <th width="10%">Registration Date & Time</th>
                <th width="20%">Subscription Id</th>
                <th width="20%">Ipaddress</th>
                <!--<th width="">Details</th>-->
                <!--<th width="8%">Allow Login</th>-->
                <th width="4%">Action</th>
            </tr>
            </thead>
                <tbody>
                    <?php // if($rs_list-fields("loginid") == $str_id) { ?>
                    <?php if($rs_list->EOF()==true) {  ?><tr><td colspan="7" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr><?php }
                            else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
                            
                            <tr>
                                <td align="center"><?php print($srno)?></td>
                                <td align="center" class="align-middle">
                                    <span class="text-help"><i class="text-help"><?php print(date("d-M-Y",strtotime($rs_list->fields("registrationdatetime"))));?></i></span>
                                </td>
                                    <td class="align-middle text-center">
                                        <p><?php print($rs_list->fields("loginid"));?></i></p>
                                    </td>
                                    <td class="align-middle text-center">
                                        <p><?php print($rs_list->fields("ipaddress"));?></i></p>
                                    </td>
                           
                                    <td align="center" class="align-middle">
                                       
                                    <?php /* ?>    <a class="btn btn-success btn-xs" href="item_edit.php?pkid=<?php print($rs_list->fields("pkid"));?>&PagePosition=<?php print($int_page);?>&key=<?php print($str_name_key);?>&catid=<?php print($int_cat_pkid);?>" title="<?php print($STR_HOVER_EDIT);?>" ><i class="fa fa-pencil"></i></a>  <?php */ ?>
                                        <a class="btn btn-danger btn-xs" href="item_history_del_p.php?pkid=<?php print($rs_list->fields("pkid"));?>&PagePosition=<?php print($int_page);?>&key=<?php print($str_name_key);?>&catid=<?php print($int_cat_pkid);?>" onClick="return frm_list_confirmdelete();" title="<?php print($STR_HOVER_DELETE);?>" ><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php $int_cnt++; $srno++; $rs_list->MoveNext(); } ?>
                                
                                <?php if($rs_list->count()>0) {
                                    $margine=5; ?>
                                    <tr>
                                        <td colspan="7" class="text-center nopadding">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination">
                                                    <?php print(PagingWithMargine($int_total_record,$margine,$int_page,"item_history_list.php",$int_record_per_page,"","key=".$str_name_key."&catid=".$int_cat_pkid."&#ptop"));?>
                                                </ul>
                                            </nav>

                                        </td>
                                    </tr>
                                    <?php } ?>
                            <?php }  ?>
                            </tbody>
                        </table>
                    </form>
                </div>
        <script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
	<?php include "../../includes/help_for_list.php"; ?>
    
        </div>
<?php include "../../includes/include_files_admin.php"; ?>
    
<!--<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>-->
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 100}).panelInstance('ta_desc');
		//new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
  });
		// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
		// convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_desc');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>


<script>
/*$( "#btn_Add" ).click(function() {
if($('div .nicEdit-main').text()=="" && $("input#txt_title").val()=="" ){ alert('Please enter title or description');return false;}
});
$( "#btn_reset" ).click(function() {
		location.reload();
});*/
</script>
</body>
</html>
