<?php
/*
File Name  :- item_add_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
//ini_set('memory_limit', '512M');
include_once('../../PHPExcel-1.8.1/Classes/PHPExcel/IOFactory.php');


include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";

include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
// include "../../includes/lib_image.php";
include "../../includes/lib_file_upload.php";
// include "../../includes/lib_file_system.php";
//include "item_app_specific.php";
#----------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables
//print_r($_FILES); exit;
$objPHPExcel    =   new PHPExcel();
$str_query_select = "";
$str_query_select .= "SELECT * FROM  ".$STR_DB_TABLE_NAME. " st ";
  //$str_query_select .= "LEFT JOIN ".$STR_DB_TABLE_NAME_CAT." d ON a.catpkid=d.catpkid AND d.visible='YES' ";


$str_query_select .= "GROUP BY  st.pkid ASC LIMIT 500";


$rs_list = GetRecordSet($str_query_select);


  
$objPHPExcel->setActiveSheetIndex(0);
 
$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'pkid ');
$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'masterpkid');
$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'loginid');
$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'password');
$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'subscriptionid');
$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'description');
$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'name');
$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'businessname'); 
$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'lastname'); 
$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'shortenurlkey'); 
$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'shortenurlvalue'); 
$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'manualamount'); 
$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'imagefilename'); 
$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'imagefilenamelarge'); 
$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'photofilenamenew'); 
$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'photofilenameapproved'); 
$objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'emailid'); 
$objPHPExcel->getActiveSheet()->SetCellValue('R1', 'address'); 
$objPHPExcel->getActiveSheet()->SetCellValue('S1', 'country'); 
$objPHPExcel->getActiveSheet()->SetCellValue('T1', 'countrypkid'); 
$objPHPExcel->getActiveSheet()->SetCellValue('W1', 'state'); 
$objPHPExcel->getActiveSheet()->SetCellValue('U1', 'statepkid'); 
$objPHPExcel->getActiveSheet()->SetCellValue('V1', 'city'); 
$objPHPExcel->getActiveSheet()->SetCellValue('Y1', 'zipcode'); 
$objPHPExcel->getActiveSheet()->SetCellValue('Z1', 'phoneno'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AA1', 'mobileno'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AB1', 'registrationdatetime'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AC1', 'allowlogin'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AD1', 'approved'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AE1', 'visible'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AF1', 'online'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AG1', 'lastlogindatetime'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AH1', 'ipaddress'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AI1', 'noofclick'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AJ1', 'totalfavoritemdl'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AK1', 'setpublic'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AL1', 'featured'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AM1', 'openinnewwindow'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AN1', 'keeppaystatsvisible'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AO1', 'isccbillmember'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AP1', 'getnotification'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AQ1', 'isusertype'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AR1', 'referredbypkid'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AS1', 'discount'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AT1', 'commission'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AU1', 'giftcard'); 
$objPHPExcel->getActiveSheet()->SetCellValue('AV1', 'minimumadvance'); 
$objPHPExcel->getActiveSheet()->getStyle("A1:AV1")->getFont()->setBold(true);
 
$rowCount   =   2;


 
while($rs_list->eof() != true)
{
    
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, mb_strtoupper($rs_list->Fields("pkid"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, mb_strtoupper($rs_list->Fields("masterpkid"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, mb_strtoupper($rs_list->Fields("loginid"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, mb_strtoupper($rs_list->Fields("password"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, mb_strtoupper($rs_list->Fields("subscriptionid"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, mb_strtoupper($rs_list->Fields("description"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, mb_strtoupper($rs_list->Fields("name"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, mb_strtoupper($rs_list->Fields("businessname"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, mb_strtoupper($rs_list->Fields("lastname"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, mb_strtoupper($rs_list->Fields("shortenurlkey"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, mb_strtoupper($rs_list->Fields("shortenurlvalue"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, mb_strtoupper($rs_list->Fields("manualamount"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, mb_strtoupper($rs_list->Fields("imagefilename"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, mb_strtoupper($rs_list->Fields("imagefilenamelarge"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, mb_strtoupper($rs_list->Fields("photofilenamenew"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, mb_strtoupper($rs_list->Fields("photofilenameapproved"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, mb_strtoupper($rs_list->Fields("emailid"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount, mb_strtoupper($rs_list->Fields("address"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, mb_strtoupper($rs_list->Fields("country"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount, mb_strtoupper($rs_list->Fields("countrypkid"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount, mb_strtoupper($rs_list->Fields("state"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('V'.$rowCount, mb_strtoupper($rs_list->Fields("statepkid"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('W'.$rowCount, mb_strtoupper($rs_list->Fields("city"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('X'.$rowCount, mb_strtoupper($rs_list->Fields("zipcode"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('Y'.$rowCount, mb_strtoupper($rs_list->Fields("phoneno"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('Z'.$rowCount, mb_strtoupper($rs_list->Fields("mobileno"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AA'.$rowCount, mb_strtoupper($rs_list->Fields("registrationdatetime"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AB'.$rowCount, mb_strtoupper($rs_list->Fields("allowlogin"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AC'.$rowCount, mb_strtoupper($rs_list->Fields("approved"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AD'.$rowCount, mb_strtoupper($rs_list->Fields("visible"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AE'.$rowCount, mb_strtoupper($rs_list->Fields("online"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AF'.$rowCount, mb_strtoupper($rs_list->Fields("lastlogindatetime"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AG'.$rowCount, mb_strtoupper($rs_list->Fields("ipaddress"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AH'.$rowCount, mb_strtoupper($rs_list->Fields("noofclick"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AI'.$rowCount, mb_strtoupper($rs_list->Fields("totalfavoritemdl"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AJ'.$rowCount, mb_strtoupper($rs_list->Fields("activationkey"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AK'.$rowCount, mb_strtoupper($rs_list->Fields("setpublic"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AL'.$rowCount, mb_strtoupper($rs_list->Fields("featured"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AM'.$rowCount, mb_strtoupper($rs_list->Fields("openinnewwindow"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AN'.$rowCount, mb_strtoupper($rs_list->Fields("keeppaystatsvisible"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AO'.$rowCount, mb_strtoupper($rs_list->Fields("isccbillmember"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AP'.$rowCount, mb_strtoupper($rs_list->Fields("getnotification"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AQ'.$rowCount, mb_strtoupper($rs_list->Fields("isusertype"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AR'.$rowCount, mb_strtoupper($rs_list->Fields("referredbypkid"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AS'.$rowCount, mb_strtoupper($rs_list->Fields("discount"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AT'.$rowCount, mb_strtoupper($rs_list->Fields("commission"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AU'.$rowCount, mb_strtoupper($rs_list->Fields("giftcard"),'UTF-8'));
    $objPHPExcel->getActiveSheet()->SetCellValue('AV'.$rowCount, mb_strtoupper($rs_list->Fields("minimumadvance"),'UTF-8'));
    $rowCount++;
 $rs_list->MoveNext();
 

 }
 
 
$objWriter  =   new PHPExcel_Writer_Excel2007($objPHPExcel);
 
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");;
header("Content-Disposition: attachment;filename=test.xlsx");
header("Content-Transfer-Encoding: binary");
header('Content-Type: application/openxmlformats-officedocument.spreadsheetml.sheet');

// header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="customer_detail.xlsx"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');  
$objWriter->save('php://output');


 
   
                        
 
exit;
#----------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=S&type=S");
exit();
?>
