<?php
/*
File Name  :- item_list.php
Create Date:- MARCH2019
Intially Create By :- 0013
Update History:
*/
#--------------------------------------------------------------------------------------
# include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_datetimeyear.php";
#--------------------------------------------------------------------------------------
#initialize variables
$int_page="";
$int_cat_pkid="";
$str_name_key="";
#---------------------------------------------------------------------------------------
# get filter data
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{ $int_cat_pkid=trim($_GET["catid"]); }

if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{ $str_name_key = trim($_GET["key"]); }

#   get paging data
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"])==true && trim($_GET["PagePosition"])>0)
{ $int_page= $_GET["PagePosition"]; }
else
{ $int_page=1; }

$str_filters="";
$str_filters="&catid=".$int_cat_pkid."&key=".$str_name_key."&PagePosition=".$int_page;
#------------------------------------------------------------------------------------------------
$int_field_type = 0;
$int_field=0;
$page=1;
$int_count=0;
$int_srno=1;

if(isset($_GET["str_filter_type"]))
{
    $int_field_type = trim($_GET["str_filter_type"]);
}
if(isset($_GET["str_filter"]))
{
    $int_field = trim($_GET["str_filter"]);
}
#----------------------------------------------------------------------------------------------------
# SELECT Filter Criteria
$str_filter_type="";
if($int_field_type == "" || !is_numeric($int_field_type) || $int_field_type<=0)
    {
    $int_field_type = 0;
    $str_filter_type=" ";
    }
else
    {
        $str_fieldname="";
        if($int_field_type==1)
        {
            $str_fieldname=$STR_TITLE_DROPDOWN_OPTION01;
        }
        elseif($int_field_type==2)
        {
            $str_fieldname=$STR_TITLE_DROPDOWN_OPTION02;
        }
	elseif($int_field_type==3)
        {
            $str_fieldname=$STR_TITLE_DROPDOWN_OPTION03;
        }
        else
        {
            $str_fieldname="";
        }
        if($str_fieldname!="")
        {
            $str_filter_type=" WHERE isusertype LIKE '".$str_fieldname."%'";
        }
//        elseif($str_fieldname != "" )
    }
#----------------------------------------------------------------------------------------------------
# SELECT Filter Criteria
$str_filter="";
if($int_field == "" || !is_numeric($int_field) || $int_field<=0)
    {
    $int_field = 0;
    $str_filter="";
    }
else
    {
        $str_fieldname="";
        if($int_field==1)
        {
                $str_fieldname="name";
        }
        elseif($int_field==2)
        {
                $str_fieldname="loginid";
        }
        elseif($int_field==3)
        {
                $str_fieldname="emailid";
        }
        else
        {
                $str_fieldname="";
        }
        if($str_fieldname!="" && $str_name_key!="" && $int_field_type!="")
        {
                $str_filter=" AND ".$str_fieldname." LIKE '".$str_name_key."%'";
        }
        elseif($str_fieldname!="" && $str_name_key!="" &&$int_field_type == "")
        {
                $str_filter=" WHERE ".$str_fieldname." LIKE '".$str_name_key."%'";
        }
//        elseif($str_fieldname != "" )
    }
#------------------------------------------------------------------------------------------
$int_count=0;
$str_query_select="";
# Select Query to get total no of record
$str_query_select="";
$str_query_select="SELECT COUNT(DISTINCT pkid) as total_record FROM " .$STR_DB_TABLE_NAME.$str_filter_type .$str_filter; 
$rs_list_total=GetRecordSet($str_query_select);
$int_total_record = $rs_list_total->fields("total_record");

$int_record_per_page=$INT_RECORD_PER_PAGE;				
$int_total_page=ceil($int_total_record / $int_record_per_page); 
if ($int_page > $int_total_page)
{ $int_page=$int_total_page; }
if($int_page < 1)
{ $int_page=1; }
$int_limit_start=($int_page -1)* $int_record_per_page;	
#------------------------------------------------------------------------------------------
/*$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME. " a";
$str_query_select .=" ". $str_filter_type .$str_filter; 
$str_query_select .=" ORDER BY pkid DESC";
$str_query_select .= " LIMIT ".$int_limit_start.",".$int_record_per_page;
//print $str_query_select;
$rs_list = GetRecordSet($str_query_select);
//print $int_record_per_page;
if($rs_list->Count()>0)
{ $srno=($int_page -1)* $int_record_per_page +1; } */
##------------------------------------------------------------------------------------------
$str_query_select = "";
$str_query_select = "SELECT a.name as personname,a.*,b.title,c.title as state_name FROM " .$STR_DB_TABLE_NAME. " a";
$str_query_select .= " LEFT JOIN t_country b";
$str_query_select .= " ON a.countrypkid=b.pkid and b.visible='YES'";
$str_query_select .= " LEFT JOIN t_state c";
$str_query_select .= " ON a.statepkid=c.pkid and b.visible='YES'";
$str_query_select .=" ".$str_filter_type." ".$str_filter; 
$str_query_select .=" ORDER BY pkid DESC";
$str_query_select .= " LIMIT ".$int_limit_start.",".$int_record_per_page;
//print $str_query_select;
$rs_list = GetRecordSet($str_query_select);
//print $rs_list->Fields("state_name");
if($rs_list->Count()>0)
{ $srno=($int_page -1)* $int_record_per_page +1; }


$int_state_pkid="";
$int_state_pkid=trim(MyHtmlEncode($rs_list->fields("pkid")));

$str_state=trim(MyHtmlEncode($rs_list->fields("state")));

$int_country_pkid = "";
//$int_country_pkid=trim(MyHtmlEncode($rs_list->fields("countrypkid")));
#------------------------------------------------------------------------------------------
#
#
#
# for ipaddress
$int_pkid=0;
if(isset($_GET["pkid"]) && trim($_GET["pkid"])!="" )
{
    $int_pkid=trim($_GET["pkid"]);
}
$str_id="";
if(isset($_GET["loginid"]) && trim($_GET["loginid"])!="" )
{
    $str_id=trim($_GET["loginid"]);
}
$str_query_filter_id="";
$str_super = "";
if(isset($_GET["super"])) { $str_super=trim($_GET["super"]); }
/*if($str_name_key != "") 
    {    $str_query_filter_id=" WHERE loginid like '".$rs_list->Fields("loginid")."%'"; 
}*/
//}
//print "filter:" .$str_id;
 #------------------------------------------------------------------------------------------

#Initialization of variables used for message display.    
$str_type="";
$str_message="";
$str_title="";
$str_mode="";
if(isset($_GET['tit']))
{
    $str_title=trim($_GET['tit']);
}
if(isset($_GET['mode']))
{
    $str_mode=trim($_GET['mode']);
}
#Get message type.
if(isset($_GET['type']))
{
    switch(trim($_GET['type']))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Initialization of variables used for message display.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("IE"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
        case("URL"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;
        case("IEM"): $str_message = $STR_MSG_ACTION_INVALID_EMAIL_FORMAT; break;
        case("AL"): $str_message = $STR_MSG_ACTION_LOGIN; break;
         case("US"): $str_message = $STR_MSG_ACTION_USER_TYPE; break;
        case("AP"): $str_message = $STR_MSG_ACTION_APPROVE; break;
        case("DP"): $str_message = $STR_MSG_ACTION_DUPLICATE_LOGINID; break;
//        case("ISUK"): $str_message = $STR_MSG_ACTION_INVALID_SHORTENURLKEY; break;
        case("IUI"): $str_message = $STR_MSG_ACTION_INVALID_LOGINID; break;
        case("IUP"): $str_message = $STR_MSG_ACTION_INVALID_PASSWORD; break;
        case("DPID"): $str_message = $STR_MSG_ACTION_INVALID_PROFILEID; break;
        case("AMT"): $str_message = $STR_MSG_ACTION_AMOUNT; break;
        case("CMS"): $str_message = $STR_MSG_ACTION_COMMISSION; break;

    }
}	
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>

<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
        <div class="row padding-10">
            <div class="col-md-3 button_space">
		<div class="btn-group" role="group" aria-label="...">
                     <a href="./item_cms.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)">
                    <i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)
                </a>
		</div>
            </div>
            <div class="col-md-9" align="right" ><h3><?php print($STR_TITLE_PAGE); ?></h3></div>
	</div>
	<hr>
	<?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
        
        <div class="row padding-10">
            <div class="col-md-12" align="right">
                <a href="./item_download_excel_p.php" class="" title="" ><i class="fa fa-download"></i>&nbsp;Download Excel File</a>
            </div>
        </div>
        
	<div class="row padding-10">
    	<div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <div class="row padding-10">
                                <div class="col-md-6 col-xs-8 col-sm-6"><a class="accordion-toggle collapsed " title="<?php print $STR_HOVER_ADD; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b> </a></div>
                                <div class="col-md-6 col-xs-4 col-sm-6" align="right"><?php print($STR_LINK_HELP); ?></div>
                            </div> 
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <form name="frm_add" action="item_add_p.php?$str_image" method="post" onSubmit="return frm_add_validateform();" enctype="multipart/form-data">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Enter Email ID as Login ID</label><span class="text-help-form"> * <?php // print "(".$STR_MSG_LOGINID_FORMAT.")"; ?></span>
                                            <input id="txt_loginid" name="txt_loginid"  class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?>" type="text"  value="<?php if(isset($_GET["loginid"])){ print(RemoveQuote(trim($_GET["loginid"])));}?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Password</label><span class="text-help-form"> * (<?php print($STR_MSG_PASSWORD_FORMAT);?>)</span>
                                            <input id="txt_password" name="txt_password" size="90" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_PASSWORD; ?>" type="text"  value="<?php if(isset($_GET["password"])){ print(RemoveQuote(trim($_GET["password"])));}?>">
                                        </div>
                                    </div>
                                </div>
				<div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>User Type</label>
                                            <span class="text-help-form"> *</span>
                                            <span class="text-help-form"></span>
                                            
                                            <select name="cbo_user" class="form-control input-sm" >
                                                <option value="<?php print($STR_TITLE_DROPDOWN_OPTION01); ?>" <?php print(CheckSelected($STR_TITLE_DROPDOWN_OPTION01,$str_super)); ?>><?php print($STR_TITLE_DROPDOWN_OPTION01); ?></option>
						<option value="<?php print($STR_TITLE_DROPDOWN_OPTION02); ?>" <?php print(CheckSelected($STR_TITLE_DROPDOWN_OPTION02,$str_super)); ?>><?php print($STR_TITLE_DROPDOWN_OPTION02); ?></option>
						<option value="<?php print($STR_TITLE_DROPDOWN_OPTION03); ?>" <?php print(CheckSelected($STR_TITLE_DROPDOWN_OPTION03,$str_super)); ?>><?php print($STR_TITLE_DROPDOWN_OPTION03); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Referred By</label><span class="text-help"></span>
                                            <select name="cbo_referredby"  id="cbo_referredby" class="form-control input-sm">
                                                    <option value="0">-- SELECT REFERRED BY --</option>
                                                    <?php 
                                                    $str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE isusertype='".$STR_TITLE_DROPDOWN_OPTION03."' ORDER BY name";
                                                    $rs_list_referredby=GetRecordSet($str_query_select);

                                                    if(!$rs_list_referredby->EOF()) {
							while(!$rs_list_referredby->EOF()==true)
                                                    	{?>
                                                        	<option value="<?php print($rs_list_referredby->fields("pkid"))?>" <?php //print(CheckSelected($rs_list_referredby->fields("pkid"),$int_country_pkid)); ?>><?php print($rs_list_referredby->fields("name")); ?> (<?php print($rs_list_referredby->fields("loginid")); ?>)</option>
                                                        	<?php
                                                        	$rs_list_referredby->MoveNext();
                                                    	}
						    }
                                                    ?>				
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Person Name</label><span class="text-help-form"> </span>
                                            <input id="txt_name" name="txt_name" size="90" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_FULLNAME; ?>" type="text" value="<?php if(isset($_GET["name"])){ print(RemoveQuote(trim($_GET["name"])));}?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Business Name</label><span class="text-help-form"> </span>
                                            <input id="txt_businessname" name="txt_businessname" size="90" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_FULLNAME; ?>" type="text" value="<?php if(isset($_GET["businessname"])){ print(RemoveQuote(trim($_GET["businessname"])));}?>">
                                        </div>
                                        
                                    </div>
                                </div>
                                
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="control-group form-group">
                                            <label>Select Country</label><span class="text-help-form"> * </span>
                                                <select name="cbo_country"  id="cbo_country" class="form-control input-sm" onChange="get_state();">
                                                    <option value="0">-- Select Country --</option>
                                                    <?php 
                                                    $str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_COUNTRY. " ORDER BY title";
                                                    $rs_list_country=GetRecordSet($str_query_select);

                                                    while(!$rs_list_country->EOF()==true)
                                                    {?>
                                                        <option value="<?php print($rs_list_country->fields("pkid"))?>" <?php print(CheckSelected($rs_list_country->fields("pkid"),$int_country_pkid)); ?>><?php print($rs_list_country->fields("title")); ?></option>
                                                        <?php
                                                        $rs_list_country->MoveNext();
                                                    }
                                                    ?>				
                                                </select>	
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="controls" id="before_get_state">
                                            <label>Select State</label><span class='text-help-form'> * </span>
                                            <select class='form-control input-sm' name='cbo_state' id='cbo_state'>
                                                <option value='0'>-- SELECT STATE --</option>
                                            </select><br/>
                                        </div>
                                        <div id="get_state"></div>
                                    </div>
                                </div>
                                
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="control-group form-group">
                                            <div class="controls"><label>City</label><span class="text-help-form"> * </span>
<input type="text" name="txt_city" id="txt_city" class="form-control input-sm"  placeholder="<?php print $STR_PLACEHOLDER_CITY; ?>"  size="40" maxlength="100">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group form-group">
                                            <div class="controls"><label>Zip / Postal Code</label><span class="text-help-form"> * </span><input type="text" name="txt_zipcode" id="txt_zipcode" class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_ZIP; ?>" size="40" maxlength="100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Address</label><span class="text-help-form"></span>
                                            <input id="ta_add" name="ta_add" size="90" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_ADDRESS; ?>" type="text" value="<?php if(isset($_GET["address"])){ print(RemoveQuote(trim($_GET["address"])));}?>">
                                            
                                            <!--<textarea name="ta_add" id="ta_desc" cols="100" rows="20" class="form-control input-sm" placeholder="<?php // print $STR_PLACEHOLDER_ADDRESS; ?>" ></textarea>-->
                                            <span class="text-help-form"><?php //print($STR_MAX_255_MSG);?></span>
                                        </div> 
                                    </div>
                                </div>
                                <div class="row padding-10">
					<div class="col-md-4">
                                        <div class="form-group">
                                            <label>Email ID</label><span class="text-help"><?php //print $STR_MSG_ACTION_INVALID_LOGINID; ?></span>
                                            <input id="txt_email" name="txt_email" size="90" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?>" type="text" value="<?php if(isset($_GET["emailid"])){ print(RemoveQuote(trim($_GET["emailid"])));}?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Mobile Number</label><span class="text-help"><?php //print $STR_MSG_ACTION_INVALID_LOGINID; ?></span>
                                            <input id="txt_mobno" name="txt_mobno" size="90" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_PHONENUMBER; ?>" type="text" value="<?php if(isset($_GET["mobileno"])){ print(($_GET["mobileno"]));}?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Phone Number</label><span class="text-help-form"> </span>
                                            <input id="txt_phoneno" name="txt_phoneno" size="90" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_PHONENUMBER; ?>" type="text" value="<?php if(isset($_GET["phoneno"])){ print(($_GET["phoneno"]));}?>">
                                        </div>
                                    </div>
                                </div>
                                
                                <?php /* ?><div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Upload Image</label>&nbsp;
                                            <span class="text-help-form"> (<?php print($STR_MSG_IMG_FILE_TYPE);?> &nbsp;| <?php print $STR_MSG_IMG_UPLOAD; ?>) </span>
                                                <input type="file" name="fileimage" size="90"  >
                                           
                                            <input name="hdn_existing_image" type="hidden" id="hdn_existing_image" value="<?php print($rs_list->fields("imagefilename")); ?>">
                                        </div>
                                    </div>
                                </div><?php */ ?>
                                	<input type="hidden" name="catid" id="catid" value="<?php print($int_cat_pkid);?>">
                                    	<input type="hidden" name="key" id="key" value="<?php print($str_name_key);?>">
                                    	<input type="hidden" name="PagePosition" id="PagePosition" value="<?php print($int_page);?>">
                                        <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
	</div>
        <div class="row">
            <div class="col-md-12" align="left">
                <div class="panel panel-default">
		<div class="panel-heading">
                    <h4 class="panel-title">
                        <strong><i class="fa fa-filter"></i>&nbsp;&nbsp;Filter Criteria</strong>
                    </h4>
		</div>
		
                <div class="panel-body">
                    <div class="col-md-12 text-center" align="center">
                        <form name="frm_filter" method="get" action="item_list.php?<?php print $str_filters; ?>">
                            <label>&nbsp;Select&nbsp;</label>
                            <label>
                                <select name="str_filter_type" class="form-control input-sm">
                                    <option value="0" <?php print(CheckSelected("0",$int_field_type));?>><?php print($STR_TITLE_DROPDOWN); ?></option>
                                    <option value="1" <?php print(CheckSelected("1",$int_field_type));?>><?php print($STR_TITLE_DROPDOWN_OPTION01); ?></option>
                                    <option value="2" <?php print(CheckSelected("2",$int_field_type));?>><?php print($STR_TITLE_DROPDOWN_OPTION02); ?></option>
                                    <option value="3" <?php print(CheckSelected("3",$int_field_type));?>><?php print($STR_TITLE_DROPDOWN_OPTION03); ?></option>
                                </select>
                            </label>
                            <label>&nbsp; Where &nbsp;</label>
                            <label>
                                <select name="str_filter" class="form-control input-sm">
                                    <option value="0" <?php print(CheckSelected("0",$int_field));?>>-- USER --</option>
                                    <option value="1" <?php print(CheckSelected("1",$int_field));?>>Name</option>
                                    <option value="2" <?php print(CheckSelected("2",$int_field));?>>LoginID</option>
                                    <option value="3" <?php print(CheckSelected("3",$int_field));?>>EmailID</option>
                                </select>
                            </label>
                            <label>&nbsp;Start With&nbsp;</label>
                            <label>
                                <select name="key" class="form-control input-sm">
                                    <option value=""<?php print(CheckSelected("",$str_name_key));?>>-- ALL ALPHABET --</option>
                                    <?php 
                                        for($i=65;$i<91;$i++)
                                        {
                                          /*  if($str_name_key != chr($i))
                                            { $select_key=""; }
                                            else
                                            { $select_key="selected"; }
                                           */ ?> 
                                            <option value="<?php print(chr($i));?>" <?php print(CheckSelected(chr($i),$str_name_key));?>><?php print(chr($i));?></option>
                                  <?php } ?>        	
                                </select>			
                            </label>
                            <label>&nbsp;<?php print DisplayFormButton('VIEW', 0)?>
                           
                            </label>
                        </form>
                       
                    </div>
		</div>
            </div>
        </div>
    </div>
    
<!--    <div class="row padding-10">
    </div>-->
    <div class="table-responsive">
	<form name="frm_list" action="item_order_p.php?<?php print $str_filters; ?>" method="post" onSubmit="return frm_list_check_displayorder();">
            <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_DATE; ?></th>
                <?php /* ?><th width="16%"><?php print $STR_TABLE_COLUMN_NAME_LOGIN_DETAILS; ?></th><?php */ ?>
            <?php /* ?>    <th width="14%"><?php print $STR_TABLE_COLUMN_NAME_LOGIN_ID; ?></th> 
                <th width="14%"><?php print $STR_TABLE_COLUMN_NAME_SUBSCRIPTON_ID; ?></th><?php */ ?>
                <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                 <th width="8%"><?php print $STR_TABLE_COLUMN_NAME_MINIMUM_ADVANCE_AMOUNT; ?></th>
		<th width="8%"><?php print $STR_TABLE_COLUMN_NAME_DISCOUNT; ?></th>
                 <th width="8%"><?php print $STR_TABLE_COLUMN_NAME_COMMISSION; ?></th>
                <th width="8%"><?php print $STR_TABLE_COLUMN_NAME_USER_TYPE; ?></th>
            <?php /* ?>    <th width="15%"><?php print $STR_TABLE_COLUMN_NAME_IP_ADDRESS_HISTORY; ?></th> <?php */ ?>
                <th width="5%"><?php print $STR_TABLE_COLUMN_NAME_ALLOW_LOGIN; ?></th>
                <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>
            </tr>
            </thead>
                    <?php if($rs_list->EOF()==true)  {  ?><tr>
            
                <tbody>
                    <td colspan="9" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr><?php }
                            else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
                            <tr>
                                
                                <td align="center"><?php print($srno)?></td>
                                <td  class="align-middle text-center">
                                <?php /* ?><span class="text-help"><?php print $STR_TITLE_DATE_REGISTERED; ?></span><br/><i class="text-help"><?php print(DDMMMYYYYHHIISSFormat($rs_list->fields("registrationdatetime"))); ?></i><?php */ ?>
				<span class="text-help"><?php print $STR_TITLE_DATE_REGISTERED; ?></span><br/><i class="text-help"><?php print(DDMMMYYYYFormat($rs_list->fields("registrationdatetime"))); ?></i>

                                <?php /* ?><?php if($rs_list->fields("lastlogindatetime")!="1970-01-01 00:00:01" && $rs_list->fields("lastlogindatetime") != "") { ?><hr/><span class="text-help"><?php print $STR_TITLE_DATE_LAST_LOGIN; ?></span><br/><i class="text-help"><?php print(DDMMMYYYYHHIISSFormat($rs_list->fields("lastlogindatetime")));?><?php } ?></i><?php */ ?>
                                <?php if($rs_list->fields("lastlogindatetime")!="1970-01-01 00:00:01" && $rs_list->fields("lastlogindatetime") != "") { ?><hr/><span class="text-help"><?php print $STR_TITLE_DATE_LAST_LOGIN; ?></span><br/><i class="text-help"><?php print(DDMMMYYYYFormat($rs_list->fields("lastlogindatetime")));?><?php } ?></i>
				</td>
                                <?php /* ?><td class="align-middle text-left">
                                   <p>
                                   <?php print("<i class='fa fa-user'></i>&nbsp;".$rs_list->fields("loginid")."</b><br/>"."<i class='fa fa-key'></i>&nbsp;".$rs_list->fields("password"));?>
                                   </p>
                                </td><?php */ ?>
                                    
                                <?php /* ?>    <td align="center" class="align-middle">
                                        <p><?php print($rs_list->Fields("loginid")); ?></p>
                                    </td> 
                                    <td align="center" class="align-middle">
                                        <p><?php print($rs_list->Fields("subscriptionid")); ?></p>
                                    </td><?php */ ?>
                                <td align="left" class="align-top">
                                    <?php if($rs_list->fields("imagefilename")!="") { ?>
                                                                    <a href="#" data-toggle="modal" data-target=".f-pop-up-<?php print($rs_list->fields("pkid")); ?>" rel="thumbnail"><img src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>"  class="img-responsive" border="0" align="absmiddle" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image"></a>
                                    <div class="modal fade f-pop-up-<?php print($rs_list->fields("pkid")); ?> " align="center" role="dialog" aria-labelledby="" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br><br>
                                                    <img src="<?php print($UPLOAD_IMG_PATH.$rs_list->fields("imagefilename"));?>" class="img-responsive" title="<?php print $STR_TITLE_PAGE; ?> image" alt="<?php print $STR_TITLE_PAGE; ?> image">
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                      <?php }
                                      ?>
                                          <?php // else {  print(MakeBlankTab(0,0,$STR_IMG_NOT_UPLOADED,"alert alert-danger")); } ?>

					<p><?php print("<i class='fa fa-user'></i>&nbsp;".$rs_list->fields("loginid")."</b><br/>"."<i class='fa fa-key'></i>&nbsp;".$rs_list->fields("password"));?></p>

                                        <p>
                                      <?php if(($rs_list->Fields("name") != "")){ ?><i class='fa fa-user'></i>&nbsp;<?php print("".$rs_list->Fields("name")); ?>&nbsp;&nbsp;
                                    <?php } ?>

                                        <?php if($rs_list->Fields("mobileno") != ""){ ?><i class='fa fa-mobile-phone'></i>&nbsp;<?php print("".$rs_list->Fields("mobileno")); ?>&nbsp;&nbsp;<?php } ?>
                                        <?php if($rs_list->Fields("phoneno") != ""){ ?><i class='fa fa-phone'></i>&nbsp;<?php print("".$rs_list->Fields("phoneno")); ?>&nbsp;&nbsp;<?php } ?>
                                        <?php if($rs_list->Fields("emailid") != ""){ ?><i class="fa fa-envelope"></i>&nbsp;<?php print($rs_list->Fields("emailid")); ?>&nbsp;&nbsp;<?php } ?>
                                        <?php if($rs_list->Fields("businessname") != ""){ ?><i class="fa fa-building"></i>&nbsp;<?php print($rs_list->Fields("businessname")); ?>&nbsp;&nbsp;<?php } ?>
                                        
                                        </p>
                                        <p>
                                        <?php if($rs_list->Fields("address") != ""){ ?><i class='fa fa-map-marker'></i>&nbsp;<?php print("".$rs_list->Fields("address")); ?><?php } ?>
                                        <?php if($rs_list->Fields("city") != ""){ ?><?php print(", ".$rs_list->Fields("city")); ?><?php } ?>
                                        <?php if($rs_list->Fields("zipcode") != ""){ ?><?php print(", ".$rs_list->Fields("zipcode")); ?><?php } ?>
                                        <?php if($rs_list->Fields("state_name") != ""){ ?><?php print(", ".$rs_list->Fields("state_name")); ?><?php } ?>
                                        <?php if($rs_list->Fields("title") != ""){ ?><?php print(", ".$rs_list->Fields("title")."."); ?>&nbsp;<?php } ?> 
                                        </p>
					<p>
                                        <?php if($rs_list->Fields("referredbypkid") > 0) { 
						$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE isusertype='".$STR_TITLE_DROPDOWN_OPTION03."' AND pkid=". $rs_list->Fields("referredbypkid");
//print $str_query_select; 
						$rs_list_referredby = GetRecordSet($str_query_select);
						if($rs_list->Fields("address") != ""){ ?><hr/><i class='fa fa-paperclip'></i>&nbsp;<?php print("".$rs_list_referredby->Fields("name")); ?><?php print(", ".$rs_list_referredby->Fields("loginid")); ?><?php print(", ".$rs_list_referredby->Fields("emailid")); ?><?php }
					} ?>
                                        <?php if($rs_list->fields("isusertype") == $STR_TITLE_DROPDOWN_OPTION01) { ?><hr/><i class='fa fa-money'></i>&nbsp;Minimum advanced payment required is <b><?php print($rs_list->Fields("minimumadvance")." %"); ?></b><?php } ?>
                                        </p>
                                </td>
				<td align="center"><input type="text" name="txt_minadvanceamt<?php print($int_cnt);?>"  value="<?php print($rs_list->fields("minimumadvance"));?>" class="form-control input-sm text-center" ><input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>"></td>
                                <td align="center"><input type="text" name="txt_price<?php print($int_cnt);?>"  value="<?php print($rs_list->fields("discount"));?>" class="form-control input-sm text-center" ><input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>"></td>
				<td align="center"><input type="text" name="txt_commission<?php print($int_cnt);?>"  value="<?php print($rs_list->fields("commission"));?>" class="form-control input-sm text-center" ><input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("pkid"));?>"></td>

<?php 
                                    $str_class="alert-danger";
                                    if(trim(strtoupper($rs_list->fields("isusertype")))==$STR_TITLE_DROPDOWN_OPTION01)
                                    {
                                        $str_class="alert-info";
                                    }
                                    if(trim(strtoupper($rs_list->fields("isusertype")))==$STR_TITLE_DROPDOWN_OPTION02)
                                    {
                                        $str_class="alert-warning";
                                    }			  
			  
                                    ?>
                                <td align="center" class="<?php print $str_class; ?>">
                                    <?php 
                                        $str_user_type="";
                                        $str_class="";
                                        $str_click_title="";
                                        if($rs_list->fields("isusertype") == $STR_TITLE_DROPDOWN_OPTION01) {	
                                            $str_user_type = $STR_TITLE_DROPDOWN_OPTION01;
                                            $str_class_user_type = "alert-info alert-link";
                                            $str_click_title="Click To Make This User ".$STR_TITLE_DROPDOWN_OPTION02; }
                                        else if($rs_list->fields("isusertype")==$STR_TITLE_DROPDOWN_OPTION02) {	
                                            $str_user_type=$STR_TITLE_DROPDOWN_OPTION02;
                                            $str_class_user_type = "alert-warning alert-link";
                                            $str_click_title="Click To Make This User ".$STR_TITLE_DROPDOWN_OPTION01; }
                                        else {
                                            $str_user_type=$STR_TITLE_DROPDOWN_OPTION03;
                                            $str_class_user_type = "alert-danger alert-link";
                                            $str_click_title=""; }
                                    ?>
					<?php if($str_user_type == $STR_TITLE_DROPDOWN_OPTION02 || $str_user_type == $STR_TITLE_DROPDOWN_OPTION01) { ?>
					<a href="item_user_type_p.php?pkid=<?php print($rs_list->fields("pkid")).$str_filters ;?>" title="<?php print($str_click_title);?>"  class="<?php  print $str_class_user_type; ?>"><?php print($str_user_type);?></a>
					<?php } else { ?>
					<span class="<?php print $str_class_user_type; ?>"><?php print($str_user_type);?></span>
					<?php } ?>
                                </td>
                                    
                                    <?php 
                                        $str_class="alert-success";
                                        if(trim(strtoupper($rs_list->fields("allowlogin")))=="NO")
                                        {
                                            $str_class="alert-danger";
                                        }			  
                                        ?>
                                    <td align="center" class="<?php print $str_class; ?>">
                                        <?php 
                                        $ste_allow_login="";
                                        $str_class="";
                                        $str_click_title="";
                                        if($rs_list->fields("allowlogin")=="YES") {	
                                            $ste_allow_login="YES";
                                            $str_class_allow_login="alert-success alert-link";
                                            $str_click_title="Click to not allow login"; }
                                        else {
                                            $ste_allow_login="NO";
                                            $str_class_allow_login="alert-danger alert-link";
                                            $str_click_title="Click to allow login"; }
                                        ?>
                                        <?php if(strtoupper($rs_list->fields("isusertype"))==$STR_TITLE_DROPDOWN_OPTION03) { ?>
                                        <span class="<?php print $str_class_allow_login; ?>"><?php print($ste_allow_login);?></span>
                                        
                                        <?php } else { ?>
                                            <a href="item_allowlogin_p.php?pkid=<?php print($rs_list->fields("pkid")).$str_filters ;?>" title="<?php print($str_click_title);?>"  class="<?php print $str_class_allow_login; ?>"><?php print($ste_allow_login);?></a><?php //} ?>
                                        <?php } ?>
                                    </td>
                            <?php   $str_image="";  $str_class="";  $str_title="";
                                    if(strtoupper($rs_list->fields("visible"))=="YES") 
                                    { $str_image=$STR_LINK_ICON_PATH_VISIBLE;
                                           $str_class="btn btn-warning btn-xs"; $str_title=$STR_HOVER_VISIBLE; }
                                    else { $str_image=$STR_LINK_ICON_PATH_INVISIBLE;
                                            $str_class="btn btn-default active btn-xs"; $str_title=$STR_HOVER_INVISIBLE; } ?>
                                    
                                    <td align="center" class="align-center">
                                        <p><a class="btn btn-success btn-xs" href="item_edit.php?pkid=<?php print($rs_list->fields("pkid"));?>&PagePosition=<?php print($int_page);?>&key=<?php print($str_name_key);?>&catid=<?php print($int_cat_pkid);?>" title="<?php print($STR_HOVER_EDIT);?>" ><i class="fa fa-pencil"></i></a></p>
                                       
                                        <a class="btn btn-danger btn-xs" href="item_del_p.php?pkid=<?php print($rs_list->fields("pkid")).$str_filters ?>" onClick="return frm_list_confirmdelete();" title="<?php print($STR_HOVER_DELETE);?>" ><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>
                                    </td>
                                </tr>
                                <?php $int_cnt++; $srno++; $rs_list->MoveNext(); } ?>
                                <tr>
                        <td></td><td></td><td></td>
			<td class="text-center"><button type="button" class="btn btn-success btn-sm" name="btn_edit_minadvanceamt" title="<?php print($STR_HOVER_EDIT); ?>" onClick="return click_edit_minadvanceamt();"><b>Change</b></button></td>
                        <td class="text-center"><button type="button" class="btn btn-success btn-sm" name="btn_edit_price" title="<?php print($STR_HOVER_EDIT); ?>" onClick="return click_edit_price();"><b>Change</b></button></td>
			<td class="text-center"><button type="button" class="btn btn-success btn-sm" name="btn_edit_commission" title="<?php print($STR_HOVER_EDIT); ?>" onClick="return click_edit_commission();"><b>Change</b></button></td>
                        <td>
				<input type="hidden" name="catid" id="catid" value="<?php print($int_cat_pkid);?>">
                                <input type="hidden" name="key" id="key" value="<?php print($str_name_key);?>">
                                <input type="hidden" name="PagePosition" id="PagePosition" value="<?php print($int_page);?>">
                                <input type="hidden" name="hdn_counter" id="hdn_counter" value="<?php print($int_cnt);?>">
				<?php // print DisplayFormButton("SAVE",0); ?>
			</td>
                        <td></td><td></td>
                    </tr>
                                <?php if($rs_list->count()>0) {
                                    $margine=5; ?>
                                    <tr>
                                        <td colspan="9" class="text-center nopadding">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination">
                                                    <?php print(PagingWithMargine($int_total_record,$margine,$int_page,"item_list.php",$int_record_per_page,"","key=".$str_name_key."&catid=".$int_cat_pkid."&#ptop"));?>
                                                </ul>
                                            </nav>

                                        </td>
                                    </tr>
                                    <?php } ?>
                                <?php }  ?>
                            </tbody>
                        </table>
                    </form>
                </div>
        <script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
	<?php include "../../includes/help_for_list.php"; ?>
    
        </div>
<?php include "../../includes/include_files_admin.php"; ?>
    <?php include($STR_ADMIN_FOOTER_PATH); ?>
    
<!--<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>-->
<script type="text/javascript">
    
function get_state() { // Call to ajax function
    var pkid = $('#cbo_country').val();
//    alert(catpkid);
    var dataString = "country_pkid="+pkid;
    $.ajax({
        type: "POST",
        url: "item_sel_state_p.php", // Name of the php files
        data: dataString,
        success: function(html)
        {
            $("#before_get_state").hide();
            $("#get_state").html(html);
        }
    });
}
</script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 100}).panelInstance('ta_desc');
		//new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
  });
		// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
		// convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_desc');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>


<script>
/*$( "#btn_Add" ).click(function() {
if($('div .nicEdit-main').text()=="" && $("input#txt_title").val()=="" ){ alert('Please enter title or description');return false;}
});
$( "#btn_reset" ).click(function() {
		location.reload();
});*/
</script>
</body>
</html>
