<?php
/*
	Module Name:- modsiteadmin
	File Name  :- sa_add_p.php
	Create Date:- 12-DEC-2016
	Intially Create By :- 015
	Update History:
*/
#--------------------------------------------------------------------------------------------------------------------------------------
#Include Files
	include "../../includes/validatesession.php";
	include "../../includes/configuration.php";
	include "./sa_config.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_datetimeyear.php";
	include "../../includes/lib_common.php";
#--------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------
	$str_super="";

#	To check whether values are passed properly or not.	
	if(isset($_POST["txt_admin_name"]))
	{
		$str_admin_name = trim($_POST["txt_admin_name"]);
	}
	if(isset($_POST["pas_password"]))
	{
		$str_password = trim($_POST["pas_password"]);
	}
	if(isset($_POST["txt_first_name"]))
	{
		$str_first_name = trim($_POST["txt_first_name"]);
	}
/*	if(isset($_POST["txt_last_name"]))
	{
		$str_last_name = trim($_POST["txt_last_name"]);
	}*/
	if(isset($_POST["ta_address"]))
	{
		$str_address = trim($_POST["ta_address"]);
	}
	/*if(isset($_POST["txt_city"]))
	{
		$str_city = trim($_POST["txt_city"]);
	}
	if(isset($_POST["txt_zip"]))
	{
		$str_zip = trim($_POST["txt_zip"]);
	}
	if(isset($_POST["txt_state"]))
	{
		$str_state = trim($_POST["txt_state"]);
	}*/
	if(isset($_POST["txt_email"]))
	{
		$str_email = trim($_POST["txt_email"]);
	}
	/*if(isset($_POST["txt_home_phone"]))
	{
		$str_home_phone = trim($_POST["txt_home_phone"]);
	}
	if(isset($_POST["txt_business_phone"]))
	{
		$str_business_phone = trim($_POST["txt_business_phone"]);
	}
	if(isset($_POST["txt_fax"]))
	{
		$str_fax = trim($_POST["txt_fax"]);
	}
	if(isset($_POST["txt_url"]))
	{
		$str_url = trim($_POST["txt_url"]);
	}*/
	if(isset($_POST["cbo_super"]))
	{
		$str_super = trim($_POST["cbo_super"]);
	}
#-------------------------------------------------------------------------------------------------------------------------------
	$str_redirect="&stradminname=".urlencode(RemoveQuote($str_admin_name))."&strfirstname=".urlencode(RemoveQuote($str_first_name))."";
	$str_redirect=$str_redirect."&straddress=".urlencode(RemoveQuote($str_address))."";
	//$str_redirect=$str_redirect."&strcity=".urlencode(RemoveQuote($str_city))."&strzip=".urlencode(RemoveQuote($str_zip))."";
	$str_redirect=$str_redirect."&stremail=".urlencode(RemoveQuote($str_email))."";
	//$str_redirect=$str_redirect."&strhomephone=".urlencode(RemoveQuote($str_home_phone))."";
	//$str_redirect=$str_redirect."&strbusinessphone=".urlencode(RemoveQuote($str_business_phone))."&strfax=".urlencode(RemoveQuote($str_fax))."";
	$str_redirect=$str_redirect."&admin=".urlencode(RemoveQuote($str_admin_name))."&super=".urlencode(RemoveQuote($str_super));
#-------------------------------------------------------------------------------------------------------------------------------
# To check required parameters are passed properly or not
	
	if($str_admin_name == "" || $str_password == "" || $str_first_name == "" || $str_email == "" || $str_super=="")
	{
		CloseConnection();
		Redirect("./sa_list.php?type=E&msg=F".$str_redirect."&#ptop");
		exit();
	}
	if(strstr($str_admin_name," ")!=false || strstr($str_password," ")!=false)
	{
		CloseConnection();
		Redirect("./sa_list.php?type=E&msg=N".$str_redirect."&#ptop");
		exit();
	}
	
	if (strlen($str_password) < 6)
	{	
		CloseConnection();
		Redirect("./sa_list.php?type=E&msg=P".$str_redirect."&#ptop");
		exit();		
	}
	if($str_address != "")
	{
		if(strlen(RemoveQuote($str_address)) > 255)
		{
			CloseConnection();
			Redirect("./sa_list.php?type=E&msg=F".$str_redirect."&#ptop");
			exit();
		}
	}	
	if(!validateEmail($str_email))
	{
		CloseConnection();
		Redirect("./sa_list.php?type=E&msg=EMAIL".$str_redirect."&#ptop");
		exit();
	}
#-------------------------------------------------------------------------------------------------------------------------------	
#	select query from t_siteadmin to check whether particular admin already exists or not.
	$str_query_select = "select count(*) as NoOfRecords from t_siteadmin where siteadminusername='" . ReplaceQuote($str_admin_name) . "'";
	$rs_duplicate_check = GetRecordset($str_query_select);
	if($rs_duplicate_check->fields("NoOfRecords") > 0)
	{
		CloseConnection();
		Redirect("./sa_list.php?type=E&msg=DU".$str_redirect."&#ptop");
		exit();
	}
#-------------------------------------------------------------------------------------------------------------------------------	
#	insert query into t_siteadmin table to insert record.
	/*$str_query_insert = "insert into t_siteadmin(siteadminusername,siteadminpassword,allowsiteadminlogin,registrationdate,";
	$str_query_insert .= "siteadminfirstname,siteadminlastname,siteadminaddress,siteadmincity,siteadminstate,siteadminzipcode,";
	$str_query_insert .= "siteadminemailid,siteadminhomephone,siteadminbusinessphone,siteadminfax,siteadminurl,issuperadmin)";
	$str_query_insert .= " values('" . ReplaceQuote($str_admin_name) . "','" . md5_encrypt(ReplaceQuote($str_password), $STR_ENCRYPTION_KEY ) . "','YES','";
	$str_query_insert .= ReplaceQuote(YYYYMMDDFormat(date('Y-m-d'))) . "','" . ReplaceQuote($str_first_name) . "','";
	$str_query_insert .= ReplaceQuote($str_last_name) . "','" . ReplaceQuote($str_address) . "','" . ReplaceQuote($str_city) . "','";
	$str_query_insert .= ReplaceQuote($str_state) . "','" . ReplaceQuote($str_zip) . "','" . ReplaceQuote($str_email) . "','";
	$str_query_insert .= ReplaceQuote($str_home_phone) . "','" . ReplaceQuote($str_business_phone) . "','";
	$str_query_insert .= ReplaceQuote($str_fax) . "','" . ReplaceQuote($str_url) . "','" . ReplaceQuote($str_super) . "')";*/
	
	$str_query_insert = "insert into t_siteadmin(siteadminusername,siteadminpassword,allowsiteadminlogin,registrationdate,";
	$str_query_insert .= "siteadminfirstname,siteadminaddress,siteadminemailid,issuperadmin)";
	//$str_query_insert .= "
	$str_query_insert .= " values ('" . ReplaceQuote($str_admin_name) . "','" . md5_encrypt(ReplaceQuote($str_password), $STR_ENCRYPTION_KEY ) . "','YES','";
	$str_query_insert .= ReplaceQuote(YYYYMMDDFormat(date('Y-m-d'))) . "','" . ReplaceQuote($str_first_name) . "','";
	$str_query_insert .= ReplaceQuote($str_address) . "','" . ReplaceQuote($str_email) . "','" . ReplaceQuote($str_super) . "')";
	
	//print ($str_query_insert); exit;
	
	ExecuteQuery($str_query_insert);
#-------------------------------------------------------------------------------------------------------------------------------
#Close connection and redirect to siteadmin_add.php page	
	CloseConnection();
	Redirect("./sa_list.php?type=S&msg=S&admin=".urlencode(RemoveQuote($str_admin_name))."&#ptop");
	exit();
?>