<?php
/*
File Name  :- item_module_set_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#------------------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
#get post data
$int_pkid="";
if(isset($_POST["hdn_pkid"])==true)
{
	$int_pkid=trim($_POST["hdn_pkid"]);
}
	
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
	CloseConnection();
	Redirect("item_module_list.php?msg=F&type=E&#ptop");
	exit();
}
$int_cnt="";
if (isset($_POST["hdn_counter"]))
{
	$int_cnt = trim($_POST["hdn_counter"]);
}	
if($int_cnt =="")
{
	CloseConnection();
	Redirect("item_module_list.php?msg=F&type=E&pkid=".$int_pkid."&#ptop");
	exit();
}
#------------------------------------------------------------------------------------------------
#update table.
$str_query_delete="";	
$str_query_delete="DELETE FROM ".$STR_DB_TABLE_NAME_SITE_MODULE. " WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_delete);
for ($i=1;$i<$int_cnt;$i++)
{
	if (isset($_POST["chk_show" . $i]) && isset($_POST["hdn_pkid" . $i]))
	{
		if (($_POST["chk_show" . $i])=="on")
		{
			$str_query="";	
			$str_query="INSERT INTO ".$STR_DB_TABLE_NAME_SITE_MODULE. "(modulepkid,pkid) VALUES (".$_POST["hdn_pkid" . $i].",".$int_pkid.")";
                        //print $str_query."<br/>";
			ExecuteQuery($str_query);
		}
	}
	
}
#------------------------------------------------------------------------------------------------
#Close connection and redirect to listing page	
CloseConnection();
Redirect("item_module_list.php?type=S&msg=S&pkid=".$int_pkid."&#ptop");
exit();
#------------------------------------------------------------------------------------------------
?>