<?php
/*
	Module Name:- modsiteadmin
	File Name  :- sa_profile.php
	Create Date:- 01-02-2006
	Intially Create By :- 0022
	Update History:
*/
#------------------------------------------------------------------------------------------------
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
#----------------------------------------------------------------------------------------------	
#initializing variables
$str_admin_name="";
$str_password="";
$str_first_name ="";
$str_last_name="";
$str_address="";
$str_city="";
$str_zip="";
$str_state=""; 
$str_email=""; 
$str_home_phone=""; 
$str_business_phone="";
$str_fax="";
$str_url=""; 

#	Get all the values and check whether values are passed properly or not.
	if(isset($_POST["hdn_admin_name"]))
	{
		$str_admin_name = trim($_POST["hdn_admin_name"]);
	}
	if(isset($_POST["txt_first_name"]))
	{
		$str_first_name = trim($_POST["txt_first_name"]);
	}
	if(isset($_POST["txt_last_name"]))
	{
		$str_last_name = trim($_POST["txt_last_name"]);
	}
	if(isset($_POST["ta_address"]))
	{
		$str_address = trim($_POST["ta_address"]);
	}
	if(isset($_POST["txt_city"]))
	{
		$str_city = trim($_POST["txt_city"]);
	}
	if(isset($_POST["txt_zip"]))
	{
		$str_zip = trim($_POST["txt_zip"]);
	}
	if(isset($_POST["txt_state"]))
	{
		$str_state = trim($_POST["txt_state"]);
	}
	if(isset($_POST["txt_email"]))
	{
		$str_email = trim($_POST["txt_email"]);
	}
	if(isset($_POST["txt_home_phone"]))
	{
		$str_home_phone = trim($_POST["txt_home_phone"]);
	}
	if(isset($_POST["txt_business_phone"]))
	{
		$str_business_phone = trim($_POST["txt_business_phone"]);
	}
	if(isset($_POST["txt_fax"]))
	{
		$str_fax = trim($_POST["txt_fax"]);
	}
	if(isset($_POST["txt_url"]))
	{
		$str_url = trim($_POST["txt_url"]);
	}
#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
#check validation	
	if( $str_admin_name == "" || $str_first_name == "" || $str_last_name == "" || $str_address == "" || $str_city == "" || $str_zip == "" || $str_state == "" || $str_email == "")
	{
		CloseConnection();
		Redirect("./sa_profile.php?type=E&msg=F");
		exit();
	}
	if($str_address != "")
	{
		if(strlen(RemoveQuote($str_address)) > 255)
		{
			CloseConnection();
			Redirect("./sa_profile.php?type=E&msg=F");
			exit();			
		}
	}
	if($str_email!="")
	{
		if(!validateEmail($str_email))
		{
			CloseConnection();
			Redirect("./sa_profile.php?msg=EMAIL&type=E");
			exit();
		}
	}
	if($str_url!="")
	{
		if(!validateURL($str_url))
		{
			CloseConnection();
			Redirect("./sa_profile.php?msg=LINK&type=E");
			exit();
		}
	}
#----------------------------------------------------------------------------------------------------------------------------------
#	Update query to update t_siteadmin.
	$str_query_update  = "update t_siteadmin set siteadminfirstname='" . ReplaceQuote($str_first_name) . "'";
	$str_query_update .= ",siteadminlastname='" . ReplaceQuote($str_last_name) . "'";
	$str_query_update .= ",siteadminaddress='" . ReplaceQuote($str_address) . "'";
	$str_query_update .= ",siteadmincity='" . ReplaceQuote($str_city) . "'";
	$str_query_update .= ",siteadminstate='" . ReplaceQuote($str_state) . "'";
	$str_query_update .= ",siteadminzipcode='" . $str_zip . "',siteadminemailid='" . ReplaceQuote($str_email) . "'";
	$str_query_update .= ",siteadminhomephone='" . ReplaceQuote($str_home_phone) . "'";
	$str_query_update .= ",siteadminbusinessphone='" . ReplaceQuote($str_business_phone) . "'";
	$str_query_update .= ",siteadminfax='" . ReplaceQuote($str_fax) . "'";
	$str_query_update .= ",siteadminurl='" . ReplaceQuote($str_url) . "' where siteadminusername='".ReplaceQuote($str_admin_name)."'";

	ExecuteQuery($str_query_update);
#----------------------------------------------------------------------------------------------------------------------------------
#	Clsoe connection and redirect to siteadmin_my_profile.php
	
	CloseConnection();
	Redirect("./sa_profile.php?type=S&msg=S");
	exit();
?>