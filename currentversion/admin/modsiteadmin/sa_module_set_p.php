<?php
/*
	Module Name:- modsiteadmin
	File Name  :- sa_module_set_p.php
	Create Date:- 19-APR-2006
	Intially Create By :- 0025
	Update History:
*/
#------------------------------------------------------------------------------------------------------------
#Include Files
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "sa_config.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
#get post data
$int_pkid="";
if(isset($_POST["hdn_siteadmin_id"])==true)
{
	$int_pkid=trim($_POST["hdn_siteadmin_id"]);
}
	
if($int_pkid <=0 || is_numeric($int_pkid) == false)
{
	CloseConnection();
	Redirect("sa_module_list.php?msg=F&type=E&#ptop");
	exit();
}

$int_cnt="";

if (isset($_POST["hdn_counter"]))
{
	$int_cnt = trim($_POST["hdn_counter"]);
}	
if($int_cnt =="")
{
	CloseConnection();
	Redirect("sa_module_list.php?msg=F&type=E&id=".$int_pkid."&#ptop");
	exit();
}
#------------------------------------------------------------------------------------------------
#update tr_site_module table.

$str_query="";	
$str_query="delete from tr_site_module where siteadminpkid=".$int_pkid;
ExecuteQuery($str_query);

for ($i=1;$i<$int_cnt;$i++)
{
	if (isset($_POST["chk_show" . $i]) && isset($_POST["hdn_pkid" . $i]))
	{
		if (($_POST["chk_show" . $i])=="on")
		{
			$str_query="";	
			$str_query="insert into tr_site_module(modulepkid,siteadminpkid) values(".$_POST["hdn_pkid" . $i].",".$int_pkid.")";
			ExecuteQuery($str_query);
		}
	}
	
}
#------------------------------------------------------------------------------------------------
#Close connection and redirect to sa_module_list page	
	CloseConnection();
	Redirect("sa_module_list.php?type=S&msg=S&id=".$int_pkid."&#ptop");
	exit();
#------------------------------------------------------------------------------------------------
?>