/*
	Module Name:- modsiteadmin
	File Name  :- sa_list.js
	Create Date:- 01-02-2006
	Intially Create By :- 0022
	Update History:
*/
function show_details(Url)
{
		window.open(Url,'siteadmin','left=50,top=20,scrollbars=yes,width=570,height=490,resizable=yes');
		return false;
}
function delete_confirm()
{
	if(confirm("Are you sure you want to delete this admin details?"))
	{
		if(confirm("Confirm Deletion: Click 'OK' to delete admin details or click 'Cancel' to cancel deletion."))
		{
			return true;
		}
	}
	return false;	
}
