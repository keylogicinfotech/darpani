<?php
/*
File Name  :- item_change_pwd_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#---------------------------------------------------------------------------------------------------------------
#To get values which are passed from previous page.
$str_old_password="";
$str_new_password="";
$str_confirm_password="";
if(isset($_POST["pas_old_password"]))
{
        $str_old_password = trim($_POST["pas_old_password"]);
}
if(isset($_POST["pas_new_password"]))
{
        $str_new_password = trim($_POST["pas_new_password"]);
}
if(isset($_POST["pas_confirm_password"]))
{
        $str_confirm_password = trim($_POST["pas_confirm_password"]);
}
#-------------------------------------------------------------------------------------------------------------------------
#To check whether values are passed properly or not.
if($str_old_password == "" || $str_new_password == "")
{
        CloseConnection();
        Redirect("./item_change_pwd.php?type=E&msg=F");
        exit();
}
if(isValidPassword($str_new_password)==false)
{
        CloseConnection();
        Redirect("./item_change_pwd.php?type=E&msg=FP");
        exit();		
}
if($str_confirm_password != $str_new_password)
{
        CloseConnection();
        Redirect("./item_change_pwd.php?type=E&msg=FU");
        exit();
}
#--------------------------------------------------------------------------------------------------------------------------	
#Select query to get records from table to check whether old password is correct or not
$str_query_select = "SELECT password FROM ".$STR_DB_TABLE_NAME. " WHERE username='" . trim($_SESSION['adminname']) . "'";
$rs_list_check_password = GetRecordSet($str_query_select);

if(  md5_decrypt($rs_list_check_password->fields("password"), $STR_ENCRYPTION_KEY) != $str_old_password)
{
        CloseConnection();
        Redirect("./item_change_pwd.php?type=E&msg=P");
        exit();
}
#--------------------------------------------------------------------------------------------------------------------------		
#Update query to update table
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME. " SET password='" . md5_encrypt(ReplaceQuote($str_new_password), $STR_ENCRYPTION_KEY) . "' WHERE username='" . trim(ReplaceQuote($_SESSION['adminname'])) . "'";
ExecuteQuery($str_query_update);
Redirect("./item_change_pwd.php?type=S&msg=S");
exit();
#--------------------------------------------------------------------------------------------------------------------------		
?>