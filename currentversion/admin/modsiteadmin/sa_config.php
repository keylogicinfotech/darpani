<?php 
$STR_UNIQUE_ADMIN="Use only letters, numbers and underscore ( _ ). Admin ID can't be updated once created.";
$STR_PASSWORD="Use only letters and numbers. Password length must be minimum 6 characters.";
$STR_UNIQUE_PASSWORD="New password and existing password must be same.";
$STR_ADMIN_MSG="Select YES if you want to display as super admin.";
$STR_ACCESS_MSG="Check the checkbox to give access permission for that particular module. You can give access permission to multiple modules at a time.";
$STR_POSITION_MSG="Position indicates position of that module on admin home page.<br/>i.e. If position is set to \"LEFT\" that module will be displayed on the left side on admin home page.";
?>