/*
	Module Name:- modsiteadmin
	File Name  :- sa_profile.js
	Create Date:- 01-02-2006
	Intially Create By :- 0022
	Update History:
*/
function frm_my_profile_validation()
{
	with(document.frm_my_profile)
	{
		if(txt_first_name.value=="")
		{
			alert("Please enter first name.");
			txt_first_name.select();
			txt_first_name.focus();
			return false;
		}
		if(txt_last_name.value=="")
		{
			alert("Please enter last name.");
			txt_last_name.select();
			txt_last_name.focus();
			return false;
		}
		if(ta_address.value=="")
		{
			alert("Please enter address.");
			ta_address.select();
			ta_address.focus();
			return false;
		}
		if(ta_address.value.length > 255)
		{
			alert("Address length should be less than 256 characters.");
			ta_address.select();
			ta_address.focus();
			return false;
		}
		if(txt_city.value=="")
		{
			alert("Please enter city name.");
			txt_city.select();
			txt_city.focus();
			return false;
		}
		if(txt_zip.value=="")
		{
			alert("Please enter zip code.");
			txt_zip.select();
			txt_zip.focus();
			return false;
		}
		if(txt_state.value=="")
		{
			alert("Please enter state name.");
			txt_state.select();
			txt_state.focus();
			return false;
		}
		if(txt_email.value=="")
		{
			alert("Please enter email address.");
			txt_email.select();
			txt_email.focus();
			return false;
		}
		if(!sValidateMailAddress(txt_email.value))
		{
			alert("Please enter valid email address. Format: emailid@domainname.com");
			txt_email.select();
			txt_email.focus();
			return false;
		}
		if(txt_url.value != "")
		{
			if(!isValidUrl(txt_url.value))
			{
				txt_url.select();
				txt_url.focus();
				return false;
			}
		}		
	}
	return true;
}
