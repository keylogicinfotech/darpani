function frm_add_validateform()
{
    with(document.frm_add)
    {
        if(isEmpty(txt_name.value))
        {
            alert("Please enter Admin ID.");
            txt_name.select();
            txt_name.focus();
            return false;
        }
        if(is_valid_userid(trim(txt_name.value)) == false)
        {
            txt_name.select();
            txt_name.focus();
            return false;
        }
        if(isEmpty(pas_password.value))
        {
            alert("Please enter password.");
            pas_password.select();
            pas_password.focus();
            return false;
        }

        if(isValidPassword(trim(pas_password.value))==false)
        {
            return false;
        }
        if (checkSpace(pas_password.value))
        {
            alert("Please do not enter blank space in Password.");
            pas_password.select();
            pas_password.focus();
            return false;
        }
        if(pas_password.value.length < 6)
        {
            alert("Please enter at least 6 characters in password.");
            pas_password.select();
            pas_password.focus();
            return false;
        }
        /*if(txt_first_name.value=="")
        {
            alert("Please enter full name.");
            txt_first_name.select();
            txt_first_name.focus();
            return false;
        }*/

        if(trim(txt_email.value)!="")
        {
            if(!sValidateMailAddress(txt_email.value))
            {
                alert("Please enter valid email address. Format: email@domain.com");
                txt_email.select();
                txt_email.focus();
                return false;
            }
        }
        /*else
        {
            alert("Please enter email address.");
            txt_email.focus();
            return false;
        }*/


        /*if(txt_last_name.value=="")
        {
            alert("Please enter last name.");
            txt_last_name.select();
            txt_last_name.focus();
            return false;
        }
        if(ta_address.value=="")
        {
            alert("Please enter address.");
            ta_address.select();
            ta_address.focus();
            return false;
        }*/
//		if(ta_address.value.length > 255)
//		{
//			alert("Address length should be less than 256 characters.");
//			ta_address.select();
//			ta_address.focus();
//			return false;
//		}
//		
        if(txt_city.value=="")
        {
            alert("Please enter city name.");
            txt_city.select();
            txt_city.focus();
            return false;
        }
        if(txt_zip.value=="")
        {
            alert("Please enter zip code.");
            txt_zip.select();
            txt_zip.focus();
            return false;
        }
        if(txt_state.value=="")
        {
            alert("Please enter state name.");
            txt_state.select();
            txt_state.focus();
            return false;
        }

        if(txt_url.value != "")
        {
            if(!isValidUrl(txt_url.value))
            {
                txt_url.select();
                txt_url.focus();
                return false;
            }
        }
    }
    return true;
}

function show_details(Url)
{
    window.open(Url,'siteadmin','left=50,top=20,scrollbars=yes,width=570,height=490,resizable=yes');
    return false;
}
function delete_confirm()
{
    if(confirm("Are you sure you want to delete this details?"))
    {
        if(confirm("Confirm Deletion: Click 'OK' to delete this details or click 'Cancel' to cancel deletion."))
	{
            return true;
	}
    }
	return false;	
}
