<?php
/*
File Name  :- item_module_list.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
#get pkid from previous page.
$int_pkid="";
if(isset($_GET["pkid"]))
{
        $int_pkid = $_GET["pkid"];
}
#to check whether values are passed properly or not
if($int_pkid == "" || !is_numeric($int_pkid) || $int_pkid<=0)
{
    CloseConnection();
    Redirect("./item_list.php?type=E&msg=F");
    exit();
}
#----------------------------------------------------------------------------------------------------
#Select Query to display list
$str_query_select = "SELECT a.modulepkid modid,a.moduleparentpkid modparentid,a.moduletitle modname,a.position modpos,";
$str_query_select .= "b.modulepkid subid,b.moduleparentpkid subparentid,b.moduletitle subname";
$str_query_select .= " FROM ".$STR_DB_TABLE_NAME_MODULE. " a LEFT JOIN ".$STR_DB_TABLE_NAME_MODULE. " b";
$str_query_select .= " ON a.modulepkid=b.moduleparentpkid" ;
$str_query_select .= " WHERE a.moduleparentpkid=0 AND a.visible='YES' AND b.visible='YES'";
$str_query_select .= " ORDER BY a.position,a.moduleparentpkid,a.displayorder ,a.modulepkid,b.displayorder ,b.moduletitle";
//print $modparentpkid; exit;
# Select Query to get modulepkid.
$rs_list=GetRecordSet($str_query_select);
$str_query = "SELECT modulepkid" ;
$str_query .= " FROM ".$STR_DB_TABLE_NAME_SITE_MODULE. " WHERE pkid=".$int_pkid;
$rs_check=GetRecordSet($str_query);
$arr_check="";
for($i=0;$i<$rs_check->count();$i++)
{
	$arr_check[$i]=$rs_check->fields("modulepkid");
	$rs_check->MoveNext();
}	

#select query to get site admin name.
$str_query="SELECT username FROM ".$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
$rs_adminname=GetRecordset($str_query);
#------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
        switch(trim($_GET["type"]))
        {
                case("S"):
                        $str_type = "S";
                        break;
                case("E"):
                        $str_type = "E";
                        break;
                case("W"):
                        $str_type = "W";
                        break;
        }
}
#Get message text.
if(isset($_GET["msg"]))
{
        switch(trim($_GET["msg"]))
        {
                case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING;break;
                case("S"): $str_message = "Module set successfully."; break;
        }
}
?>

<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
    <div class="container">
        <?php include($STR_ADMIN_HEADER_PATH); ?>
            <div class="row padding-10">
                <div class="col-md-6 col-sm-6 col-xs-12 button_space">
                    <div class="btn-group" role="group" aria-label="...">
                        <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;Back</a><?php */?>
                        <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?>" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp; <?php print($STR_TITLE_PAGE);?></a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12" align="right" >
                    <h3><?php print($STR_TITLE_PAGE);?> [<?php print(Myhtmlencode($rs_adminname->fields("username")));?>]</h3>
                </div>
            </div><hr> 
	<?php
	if($str_type != "" && $str_message != "")
		{
			print(DisplayMessage(0,$str_message,$str_type));
		}
	?>
	<div class="row padding-10">
            <div class="col-md-12">
        	<div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <div class="row padding-10">
                                        <div class="col-md-6 col-sm-6 col-xs-8"> 
                                            <?php /*?><a class="accordion-toggle collapsed link" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><i class="glyphicon  glyphicon-plus "></i>&nbsp;&nbsp;<?php print($STR_TITLE_FORM_ADD); ?> </a><?php */?>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>                                                       </div>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
	</div>
	<div class="table-responsive">
            <form name="frm_list" action="item_module_set_p.php" method="post">
                <table class="table table-striped table-bordered">
        	<thead>
                    <tr>
                	<th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                        <th width="86%"><?php print $STR_TABLE_COLUMN_NAME_TITLE; ?></th>
                        <th width="6%"><?php print $STR_TABLE_COLUMN_NAME_POSITION; ?></th>
                        <th width="5%"><?php print $STR_TABLE_COLUMN_NAME_GRANT_PERMISSION; ?>
                            <?php if(!$rs_list->eof()) {?><br><input type="checkbox" onClick="return checksuball();" name="chk_suball"><?php } ?>
			</th>
                    </tr>
		</thead>
		<tbody>
                    <?php if($rs_list->EOF()==true)  {  ?>
                    <tr>
                        <td colspan="6" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td>
                    </tr>
                    <?php }
                        else {
                                $int_cnt=1;
                                $int_mod_cnt=0;
                                $int_sub_cnt=0;
                                $pkid=0;
                                $cnt="";
                                $title="";
                                $modpkid="";
                                $flag=0;
                                $mod_pos="";
                                while(!$rs_list->EOF()==true) { ?>
                                 <?php if($pkid != $rs_list->fields("modid") && $flag==0) 
                                 {
                                        $int_sub_cnt = 0;
                                        $flag=1;
                                        $pkid = $rs_list->fields("modid");
                                        $int_mod_cnt = $int_mod_cnt + 1;
                                        $cnt="<strong>".$int_mod_cnt."</strong>"; 
                                        $title= "<strong>".MyHtmlEncode($rs_list->fields("modname"))."</strong>"; 
                                        $modpkid=$rs_list->fields("modid");
                                        $modparentpkid=$rs_list->fields("modparentid");
                                        $highlightclass="class='alert-warning'";
                                        $position="<strong>".$rs_list->fields("modpos")."</strong>";
                                        
                                       // print "HELLO";
                                 }
                                 else
                                 {	
                                        $flag=0;
                                        $int_sub_cnt = $int_sub_cnt + 1;
                                        $cnt=$int_sub_cnt; 
                                        $title="&nbsp;&nbsp;&nbsp;&nbsp;".MyHtmlEncode($rs_list->fields("subname")); 
                                        $modpkid=$rs_list->fields("subid");
                                        $modparentpkid=$rs_list->fields("subparentid");
                                        $highlightclass="";
                                        $position="";	
                                        //print "HI";
                                 }
                     ?>
                     <?php if(trim($title)!="") { ?>
                    <tr>
                        <td align="center" <?php print($highlightclass);?>>
                            <?php print($cnt);?>
                            <input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($modpkid);?>">
                            <input type="hidden" name="hdn_parentpkid<?php print($int_cnt);?>" value="<?php print($modparentpkid);?>">
                            <input type="hidden" name="current_cnt<?php print($int_cnt);?>" value="<?php print($int_cnt);?>">
                        </td>
                        <td <?php print($highlightclass);?>>
                            <table width="100%" class="" border="0" cellspacing="0" cellpadding="0" >
                               <tr> 
                                <td align="left" valign="top">
                                     <?php print($title); ?>
                                </td>
                              </tr>
                            </table>
                        </td>
                        <td <?php print($highlightclass);?> align="center"> <?php print($position); ?></td>
                            <?php
                                            $str_checked="";
                                            //print($arr_check[0]);
                                            if(count($arr_check)>0 && $arr_check!="")
                                            {
                                                    for($i=0;$i<count($arr_check);$i++)
                                                    {
                                                       // print $arr_check[$i]." - ".$modpkid."<br/>";
                                                            if($arr_check[$i]==$modpkid)
                                                            {
                                                                    $str_checked="checked";
                                                                    break;
                                                            }
                                                            else
                                                            {
                                                                    $str_checked="";
                                                            }				
                                                    }	
                                            }	
                                            if($highlightclass=="")
                                            {
                                                    $str_chkbox_class="TDRowColor".($int_cnt%2);
                                            }	
                                            else
                                            {
                                                    $str_chkbox_class="HighlightChkbox";
                                            }
                            ?>
                            <td align="center" <?php print($highlightclass);?>>
                                <?php print $str_checked; ?>
                                <input type="checkbox" class="<?php print($str_chkbox_class)?>" name="chk_show<?php print($int_cnt);?>"  onClick="return check_sub(this,current_cnt<?php print($int_cnt);?>);" <?php print($str_checked);?>> 
                            </td>	
                    </tr>
                                                    <?php
                                            $int_cnt++;
                                            }
                                            if($flag!=1)
                                            {	
                                                    $rs_list->MoveNext();
                                            }

                            }
                    ?>
                    <tr>
                        <input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>">
			<input type="hidden" name="hdn_pkid" value="<?php print($int_pkid);?>">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="center">
                           <?php print DisplayFormButton("SAVE",0); ?>
                           <?php /*?> <p align="center"><?php print DisplayFormButton("RESET",0); ?></p><?php */ ?>
			</td>
                    </tr>
                    
		<?php
			}
		?>
                    </tbody>
                </table> 
            </form>
        </div>
        <a name="help"></a>
        <div class="breadcrumb">
            <h4><b><?php print($STR_MSG_HELP_HEADING); ?></b></h4>
            <?php /*?><p class="text-help"><?php print($STR_MSG_HELP_DISPLAY_ORDER); ?></p>
            <p class="text-help"><?php print($STR_MSG_HELP_VISIBLE_INVISIBLE); ?></p>
            <p class="text-help"><?php print($STR_MSG_HELP_INVISIBLE_VISIBLE); ?></p>
            <p class="text-help"><?php print($STR_MSG_HELP_UPDATE); ?></p><?php */?>
            <p class="text-help"><?php print($STR_MSG_ACCESS);?></p>
            <p class="text-help"><?php print($STR_MSG_POSITION);?></p>
        </div>
    </div>
<script language="JavaScript" src="./item_module_list.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>  
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>