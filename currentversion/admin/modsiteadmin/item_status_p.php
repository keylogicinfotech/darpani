<?php
/*
File Name  :- item_status_p.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_common.php";
include "./item_config.php";
if (strtoupper($_SESSION['superadmin'])=="NO")
{ 
        CloseConnection();
        Redirect("../admin_home.php");
        exit();
}
#To check whether values are passed properly or not
$int_pkid="";
if(isset($_GET["pkid"]))
{
        $int_pkid = trim($_GET["pkid"]);
}
if($int_pkid == "" || $int_pkid<=0 || !is_numeric($int_pkid))
{
        CloseConnection();
        Redirect("./item_list.php?type=E&msg=F");
        exit();
}
$str_query_select="";
$str_query_select="SELECT allowlogin,issuperadmin,username FROM ".$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
$rs_list_status=GetRecordset($str_query_select);
if($rs_list_status->eof())
{
        CloseConnection();
        Redirect("./item_list.php?type=E&msg=F");
        exit();
}
$str_allowlogin_value=$rs_list_status->fields("allowlogin");
$str_superuser=strtoupper($rs_list_status->fields("issuperadmin"));
if($str_superuser=='YES')
{
        CloseConnection();
        Redirect("./item_list.php");
        exit();
}
#To change passed value
if($str_allowlogin_value == "YES")
{
        $str_change_value = "NO";
}
else
{
        $str_change_value = "YES";
}
#Update query in table to update record in database
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME. " SET allowlogin='" . $str_change_value . "' WHERE pkid=" .$int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------------------------	
CloseConnection();
Redirect("./item_list.php?type=S&msg=A&varchange=".$str_change_value);
exit();
?>

