<?php
/*
	File Name  :- item_add.php
	Create Date:- 01-02-2006
	Intially Create By :- 0022
	Update History:
*/
#--------------------------------------------------------------------------------------------------------------------------------------
#Include Files
	include "../../includes/validatesession.php";
	include "../../includes/configuration.php";
	include "./item_config.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_datetimeyear.php";
	include "../../includes/lib_common.php";
	
	if (strtoupper($_SESSION['superadmin'])=="NO")
	{ 
		CloseConnection();
		Redirect("../admin_home.php");
		exit();
	}
	
#initializing vriables
	$str_name="";
	$str_fname="";
	$str_lname="";
	$str_addlen="";
	$str_city="";
	$str_zip="";
    $str_state="";
	$str_email="";
	$str_phone="";
    $str_bphone="";
	$str_fax="";
	$str_url="";
	$str_super="";
	$str_add="";
	#getting query string variables	
	if(isset($_GET["stradminname"]))
	{ 
		$str_name=trim(MyHtmlEncode(RemoveQuote($_GET["stradminname"]))); 
	}
	if(isset($_GET["strfirstname"]))
	{ 
		$str_fname=trim(MyHtmlEncode(RemoveQuote($_GET["strfirstname"]))); 
	}
	if(isset($_GET["strlastname"]))
	{
	  $str_lname=trim(MyHtmlEncode(RemoveQuote($_GET["strlastname"])));
	}
	if(isset($_GET["straddress"]))
	{ 
		$str_add=trim(MyHtmlEncode(RemoveQuote($_GET["straddress"])));
		$str_addlen=trim(strlen($_GET["straddress"]));
	} 
	else 
	{
		$str_addlen=0;
	}
	if(isset($_GET["strcity"]))
	{ 
		$str_city=trim(MyHtmlEncode(RemoveQuote($_GET["strcity"]))); 
	}
	if(isset($_GET["strzip"]))
	{
		 $str_zip=trim(RemoveQuote($_GET["strzip"])); 
	}  
	if(isset($_GET["strstate"]))
	{ 
		$str_state=trim(MyHtmlEncode(RemoveQuote($_GET["strstate"]))); 
	}   
	if(isset($_GET["stremail"]))
	{ 
		$str_email=trim(RemoveQuote($_GET["stremail"])); 
	}  
	if(isset($_GET["strhomephone"]))
	{ 
		$str_phone=trim(RemoveQuote($_GET["strhomephone"])); 
	}   
	if(isset($_GET["strbusinessphone"]))
	{ 
		  $str_bphone=trim(RemoveQuote($_GET["strbusinessphone"])); 
	} 
	if(isset($_GET["strfax"]))
	{ 
		$str_fax=trim(RemoveQuote($_GET["strfax"])); 
	}
	if(isset($_GET["strurl"]))
	{ 
		$str_url=trim(RemoveQuote($_GET["strurl"])); 
	} 
	if(isset($_GET["super"]))
	{  
		$str_super=trim($_GET["super"]);
	}  
?>
<html>
<head>
<script language="JavaScript" src="../includes/functions.js" type="text/javascript"></script>
<script language="JavaScript" src="./item_add.js" type="text/javascript"></script>
<link href="../includes/admin.css" rel="stylesheet" type="text/css">
<title>:: Add Site Admin ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<table width="1008" border="0" cellpadding="0" cellspacing="0" align="center">
 <tr> 
  <td >
  <?php include "../../includes/adminheader.php" ?>
  </td>
 </tr>
</table>
<table width="1008" align="center" cellpadding="0" cellspacing="0">
 <tr> 
  <td background="../images/tablebg.gif" height="390" valign="top" align="center">
	<table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
	   <tr><td height="20"></td></tr>
        <tr> 
      <td align="center" height="25" class="PageHeader">Add Site Admin</td>
     </tr>
	 <tr> 
     <td height="2" align="right" class="TableHeader10ptBold"></td>
   </tr>
	</table>
    <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
     <tr> 
      <td width="805" height="30"  class="DescriptionText10ptNormal"><a href="../admin_home.php" title="Go to admin main menu" class="NavigationLink">Main Menu</a>&nbsp;&gt;&gt;&nbsp;<a href="./item_list.php" title="Go to Site admin list" class="NavigationLink">Site Admin List</a>&nbsp;&gt;&gt;&nbsp;Add Site Admin</td>
      <td width="145" align="right" class="DescriptionText10ptNormal">&lt;&lt;&nbsp;<a href="./item_list.php" class="NavigationLink" title="Back to Site admin list">Back</a></td>
	 </tr>
	</table>
	 <?php
#------------------------------------------------------------------------------------------
#	Initialization of variables used for message display.   
   	$str_type = "";
	$str_message = "";
#	Get message type.
   		if(isset($_GET["type"]))
		{
			switch(trim($_GET["type"]))
			{
				case("S"):
					$str_type = "S";
					break;
				case("E"):
					$str_type = "E";
					break;
				case("W"):
					$str_type = "W";
					break;
			}
		}

		if(isset($_GET["admin"]))
		{
			$str_admin_name = trim(RemoveQuote($_GET["admin"]));
		}
		else
		{
			$str_admin_name = "";
		}
#	Get message text.
   		if(isset($_GET["msg"]))
		{
			switch($_GET["msg"])
			{
				case("S"):
					$str_message = "Admin '" . $str_admin_name ."' details added successfully.";
					break;
				case("F"):
					$str_message = "Some information(s) missing. Please try again.";
					break;
				case("A"):
					$str_message = "Admin '" . $str_admin_name . "' already exists. Please try again with another name.";
					break;
				case("N"):
					$str_message = "Please do not enter blank space in admin name/password.";
					break;
				case("P"):
					$str_message = "Please enter at least 6 characters for password.";
					break;	
				case("EMAIL"):
					$str_message = "Invalid email format.";
					break;	
				case("LINK"):
					$str_message = "Invalid url format.";
					break;	
			}
		}
#	Diplay message.
		if($str_type != "" && $str_message != "")
		{
			print(DisplayMessage(0,$str_message,$str_type));
		}
	?>
	
    <a name="ptop"></a>
     <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center" class="tbl">
      <tr>
       <td>
	     <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr class="TableHeader10ptBold">
             <td width="50%" height="20">Enter  Site Admin Details </td>
             <td align="right"><a href="#help" class="NavigationLink">Help</a></td>
	       </tr>
         </table> 
  	  </td>
     </tr>
	 <tr>
      <td>
	   <table width="100%" align="center" cellpadding="3" cellspacing="5">
	     <form name="frm_add" action="./item_add_p.php" method="post">
            <tr> 
              <td width="175" height="10"></td>
		      <td width="833"></td>
            </tr>
            <tr> 
              <td align="right" valign="top" class="FieldCaptionText10ptNormal">Admin Name:<span class="HelpText">*</span></td>
              <td align="left" valign="top"><input type="text" name="txt_admin_name" value="<?php print($str_name); ?>" tabindex="1" size="30" maxlength="20" class="clsTextBox">
			   <span class="HelpText"><?php print($STR_UNIQUE_ADMIN);?></span></td>
            </tr>
            <tr> 
              <td align="right" valign="top" class="FieldCaptionText10ptNormal">Password:<span class="HelpText">*</span></td>
              <td align="left" valign="top"><input type="text" name="pas_password" size="30" maxlength="20" tabindex="2"  class="clsTextBox">
			   <span class="HelpText"><?php print($STR_PASSWORD)?></span></td>
            </tr>
            <tr> 
               <td align="right" valign="top" class="FieldCaptionText10ptNormal">First Name:<span class="HelpText">*</span></td>
               <td align="left" valign="top"><input type="text" name="txt_first_name" value="<?php print($str_fname);?>" size="50" maxlength="50" tabindex="3"  class="clsTextBox"> 
                <span class="HelpText"><?php print($STR_MAX_50_MSG);?></span></td>
            </tr>
            <tr> 
               <td align="right" valign="top" class="FieldCaptionText10ptNormal">Last Name:<span class="HelpText">*</span></td>
               <td align="left" valign="top"><input type="text" name="txt_last_name" size="50" value="<?php print($str_lname);?>" maxlength="50" tabindex="4"  class="clsTextBox"> 
                <span class="HelpText"><?php print($STR_MAX_50_MSG);?></span></td>
             </tr>
             <tr> 
               <td align="right" valign="top" class="FieldCaptionText10ptNormal">Address:<span class="HelpText">*</span></td>
               <td rowspan="2" align="left" valign="bottom"><textarea name="ta_address" onKeyUp="javascript:return getLength('document.frm_add.txt_address_len','document.frm_add.ta_address');" tabindex="5" cols="90" rows="15" wrap="virtual" class="clsTextArea"><?php print($str_add); ?></textarea>
                 <br>
               <span class="HelpText"></span></td></tr>
             <tr>
               <td align="right" valign="bottom" class="FieldCaptionText10ptNormal"> 
                    </td>
             </tr>
             
             
             <tr> 
               <td align="right" valign="top" class="FieldCaptionText10ptNormal">Total Characters:<span class="HelpText">*</span></td>
               <td align="left" valign="top"><input name="txt_address_len" type="text" id="txt_address_len"  value=<?php print($str_addlen); ?> size="3" readonly="true"  class="clsTextBox"> 
                <span class="HelpText"><?php print($STR_MAX_255_MSG)?></span></td>
             </tr>
             
             
             
             <tr> 
               <td align="right" valign="top" class="FieldCaptionText10ptNormal">City:<span class="HelpText">*</span></td>
               <td align="left" valign="top"><input type="text" name="txt_city" value="<?php print($str_city); ?>" size="50" maxlength="50" tabindex="6"  class="clsTextBox">               </td>
             </tr>
               <td align="right" valign="top" class="FieldCaptionText10ptNormal">Zipcode:<span class="HelpText">*</span></td>
               <td align="left" valign="top"><input type="text" name="txt_zip" value="<?php print($str_zip); ?>" size="18" maxlength="20" tabindex="7"  class="clsTextBox">               </td>
             </tr>
             <tr> 
               <td align="right" valign="top" class="FieldCaptionText10ptNormal">State:<span class="HelpText">*</span></td>
               <td align="left" valign="top"><input type="text" name="txt_state" value="<?php print($str_state); ?>" size="50" maxlength="50" tabindex="8"  class="clsTextBox">               </td>
             </tr>
             <tr> 
                <td align="right" valign="top" class="FieldCaptionText10ptNormal">Email Id:<span class="HelpText">*</span></td>
                <td align="left" valign="top"><input type="text" name="txt_email" value="<?php print($str_email);?>" size="50" maxlength="100" tabindex="9"  class="clsTextBox"> 
                  <span class="HelpText"><?php print($STR_EMAIL_MSG)?></span></td>
             </tr>
             <tr> 
                <td align="right" valign="top" class="FieldCaptionText10ptNormal">Home Phone:</td>
                <td align="left" valign="top"><input type="text" name="txt_home_phone" value="<?php print($str_phone); ?>" size="30" maxlength="25" tabindex="10"  class="clsTextBox">                </td>
             </tr>
             <tr> 
                <td align="right" valign="top" class="FieldCaptionText10ptNormal">Business Phone:</td>
                <td align="left" valign="top"><input type="text" name="txt_business_phone" value="<?php print($str_bphone); ?>" size="30" maxlength="25" tabindex="11"  class="clsTextBox">                </td>
             </tr>
             <tr> 
                <td align="right" valign="top" class="FieldCaptionText10ptNormal">Fax:</td>
                <td align="left" valign="top"><input type="text" name="txt_fax" value="<?php print($str_fax); ?>" size="30" maxlength="25" tabindex="12"  class="clsTextBox">                </td>
             </tr>
             <tr> 
                <td align="right" valign="top" class="FieldCaptionText10ptNormal">URL:</td>
                <td align="left" valign="top"><input type="text" name="txt_url" value="<?php print($str_url); ?>" size="50" maxlength="255" tabindex="13"  class="clsTextBox"> 
                   <span class="HelpText"><?php print($STR_URL_MSG)?></span>                </td>
             </tr>
			 <tr> 
		       <td align="right" valign="top" class="FieldCaptionText10ptNormal">Is Super Admin:<span class="HelpText">*</span></td>
		       <td align="left" valign="top"><select name="cbo_super" class="clsDropDown">
					  <option value="NO" <?php print(CheckSelected("NO",$str_super)); ?>>NO</option>
					  <option value="YES" <?php print(CheckSelected("YES",$str_super)); ?>>YES</option>
				      </select>  <span class="HelpText"><?php print($STR_ADMIN_MSG)?></span>			   </td>
		     </tr>
			 
			 <tr> 
                <td ></td>
                <td align="left"><input name="btn_submit" type="submit" title="Click to add Admin" value="Add Admin" onClick="return frm_add_validation();" tabindex="14" class="ButtonStyle">&nbsp;<input name="btn_reset" type="reset" value="Reset" title="Click to reset Form" tabindex="15" class="ButtonStyle">                </td>
             </tr>
           
		   </form>
       </table>
	  </td>
     </tr>
   </table>
<a name="help"></a>
      <table width="99%" border="0" cellpadding="2" cellspacing="0" align="center" class="HelpBox">
        <tr > 
          <td height="20" > <table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr class="TableHeader10ptBold"> 
                <td >Help Section </td>
                <td align="right"><a href="#ptop" class="NavigationLink">Top</a></td>
              </tr>
            </table></td>
        </tr>
	   <tr valign="top" >
		<td>
		 <table width="100%" border="0" cellpadding="2" cellspacing="0">
          <tr> 
           <td width="20" valign="baseline" class="HelpText">&#8226;</td>
		        <td valign="baseline" class="HelpText"><?php print($STR_MANDATORY);?></td>
		  </tr>
	     </table>
		</td>
	   </tr>
      </table>
    </td>
  </tr>
</table>
	<?php include "../../includes/adminfooter.php" ?>
</body>
</html>
   