<?php
/*
File Name  :- item_edit_p.php
Create Date:- Jan-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#-------------------------------------------------------------------------------------------	
#initializing vriables
$int_pkid = "";
$str_name = "";
$str_password = "";
$str_first_name = "";
$str_first_name = "";
$str_last_name = "";
$str_address = "";
$str_city = "";
$str_zip = "";
$str_state ="";
$str_email= "";
$str_home_phone = "";
$str_business_phone = "";
$str_fax = ""; 
$str_url = "";
$str_super = "";
$str_allowlogin = "";
#To check whether values are passed properly or not.
if(isset($_POST["hdn_pkid"]))
{
        $int_pkid = trim($_POST["hdn_pkid"]);
}
if(isset($_POST["hdn_name"]))
{
        $str_name = trim($_POST["hdn_name"]);
}
if(isset($_POST["txt_password"]))
{
        $str_password = trim($_POST["txt_password"]);
}
if(isset($_POST["txt_first_name"]))
{
        $str_first_name = trim($_POST["txt_first_name"]);
}
if(isset($_POST["txt_last_name"]))
{
        $str_last_name = trim($_POST["txt_last_name"]);
}
if(isset($_POST["ta_address"]))
{
        $str_address = trim($_POST["ta_address"]);
}
if(isset($_POST["txt_city"]))
{
        $str_city = trim($_POST["txt_city"]);
}
if(isset($_POST["txt_zip"]))
{
        $str_zip = trim($_POST["txt_zip"]);
}
if(isset($_POST["txt_state"]))
{
        $str_state = trim($_POST["txt_state"]);
}
if(isset($_POST["txt_email"]))
{
        $str_email = trim($_POST["txt_email"]);
}
if(isset($_POST["txt_home_phone"]))
{
        $str_home_phone = trim($_POST["txt_home_phone"]);
}
if(isset($_POST["txt_business_phone"]))
{
        $str_business_phone = trim($_POST["txt_business_phone"]);
}
if(isset($_POST["txt_fax"]))
{
        $str_fax = trim($_POST["txt_fax"]);
}
if(isset($_POST["txt_url"]))
{
        $str_url = trim($_POST["txt_url"]);
}
if(isset($_POST["cbo_super"]))
{
        $str_super = trim($_POST["cbo_super"]);
}
if(isset($_POST['hdn_alwlgn']))
{
        $str_allowlogin=trim($_POST['hdn_alwlgn']);
}
if($str_super=='YES')
{
        $str_allowlogin='YES';
}
#---------------------------------------------------------------------------------------------------------------------------------------------------]
#check validation
if( $int_pkid == "" || $str_name == "" || $str_password == "" || /*$str_first_name == "" || $str_email == "" ||*/ $str_super=="")
{
        CloseConnection();
        Redirect("./item_edit.php?type=E&msg=F&pkid=".urlencode($int_pkid));
        exit();
}
//	if($str_address != "")
//	{
//		if(strlen(RemoveQuote($str_address)) > 255)
//		{
//			CloseConnection();
//			Redirect("./item_edit.php?type=E&msg=F&pkid=".urlencode($int_pkid));
//			exit();			
//		}
//	}
if(!validateEmail($str_email))
{
        CloseConnection();
        Redirect("./item_edit.php?type=E&msg=EMAIL&pkid=".urlencode($int_pkid));
        exit();
}
/*if($str_url!="")
{
        if(!validateURL($str_url))
        {
                CloseConnection();
                Redirect("./item_edit.php?type=E&msg=LINK&pkid=".urlencode($int_pkid));
                exit();
        }
}*/
#-------------------------------------------------------------------------------------------	
#Update query in table.
$str_query_update = "";
$str_query_update = "UPDATE ".$STR_DB_TABLE_NAME. " SET password='" . md5_encrypt(ReplaceQuote($str_password), $STR_ENCRYPTION_KEY );
$str_query_update .= "',firstname='" . ReplaceQuote($str_first_name) . "',allowlogin='".ReplaceQuote($str_allowlogin)."'";
$str_query_update .= ",address='" . ReplaceQuote($str_address);
///$str_query_update .= "',siteadmincity='" . ReplaceQuote($str_city) . "',siteadminstate='" . ReplaceQuote($str_state);
$str_query_update .= "',emailid='" . ReplaceQuote($str_email);
//$str_query_update .= "',siteadminhomephone='" . ReplaceQuote($str_home_phone);
//$str_query_update .= "',siteadminbusinessphone='" . ReplaceQuote($str_business_phone) . "',siteadminfax='" . ReplaceQuote($str_fax);
$str_query_update .= "',issuperadmin='"  .ReplaceQuote($str_super) ."' WHERE pkid=" . $int_pkid ;
ExecuteQuery($str_query_update);
#--------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("./item_list.php?type=S&msg=E&admin=".$str_name."&#ptop");
exit();
?>