<?php
/*
	Module Name:- modsiteadmin
	File Name  :- sa_change_pwd_p.php
	Create Date:- 01-02-2006
	Intially Create By :- 0022
	Update History:
*/
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_datetimeyear.php";
	include "../includes/lib_common.php";
#---------------------------------------------------------------------------------------------------------------
#	To get values which are passed from previous page.
	$str_old_password="";
	$str_new_password="";
	$str_confirm_password="";
	if(isset($_POST["pas_old_password"]))
	{
		$str_old_password = trim($_POST["pas_old_password"]);
	}
	if(isset($_POST["pas_new_password"]))
	{
		$str_new_password = trim($_POST["pas_new_password"]);
	}
	if(isset($_POST["pas_confirm_password"]))
	{
		$str_confirm_password = trim($_POST["pas_confirm_password"]);
	}
#-------------------------------------------------------------------------------------------------------------------------
#	To check whether values are passed properly or not.
	if($str_old_password == "" || $str_new_password == "")
	{
		CloseConnection();
		Redirect("./sa_change_pwd.php?type=E&msg=F");
		exit();
	}
	if(isValidPassword($str_new_password)==false)
	{
		CloseConnection();
		Redirect("./sa_change_pwd.php?type=E&msg=FP");
		exit();		
	}
	if($str_confirm_password != $str_new_password)
	{
		CloseConnection();
		Redirect("./sa_change_pwd.php?type=E&msg=FU");
		exit();
	}
#--------------------------------------------------------------------------------------------------------------------------	
#	Select query to get records from t_siteadmin to check whether old password is correct or not
	$str_query_select = "select siteadminpassword from t_siteadmin where siteadminusername='" . trim($_SESSION['adminname']) . "'";
	$rs_check_password = GetRecordSet($str_query_select);
	
	if(  md5_decrypt($rs_check_password->fields("siteadminpassword"), $STR_ENCRYPTION_KEY) != $str_old_password)
	{
		CloseConnection();
		Redirect("./sa_change_pwd.php?type=E&msg=P");
		exit();
	}
#--------------------------------------------------------------------------------------------------------------------------		
#	Update query to update t_siteadmin.
	$str_query_update = "update t_siteadmin set siteadminpassword='" . md5_encrypt(ReplaceQuote($str_new_password), $STR_ENCRYPTION_KEY) . "' where siteadminusername='" . trim(ReplaceQuote($_SESSION['adminname'])) . "'";
	ExecuteQuery($str_query_update);
	Redirect("./sa_change_pwd.php?type=S&msg=S");
	exit();
#--------------------------------------------------------------------------------------------------------------------------		
?>