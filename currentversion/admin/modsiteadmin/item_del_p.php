<?php
/*
File Name  :- item_add.php
Create Date:- Jan-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#--------------------------------------------------------------------------------
#Get / Post variables
if (strtoupper($_SESSION['superadmin'])=="NO")
{ 
    CloseConnection();
    Redirect("../admin_home.php");
    exit();
}

$int_pkid="";
if(isset($_GET["pkid"]))
{
    $int_pkid = trim($_GET["pkid"]);
}
if($int_pkid == "" || $int_pkid<=0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("./item_list.php?type=E&msg=F");
    exit();
}
# Select Query
$str_query_select = "";
$str_query_select = "SELECT username,issuperadmin FROM ".$STR_DB_TABLE_NAME. " WHERE pkid=" . $int_pkid;
$rs_list = GetRecordset($str_query_select);

$str_user_name = $rs_list->fields("username");	
$str_super = $rs_list->fields("issuperadmin");	
if(strtoupper($str_super)=='YES')
{
    CloseConnection();
    Redirect("item_list.php");
    exit();
}
# Delete Query
$str_query_delete = "DELETE FROM ".$STR_DB_TABLE_NAME_SITE_MODULE. " WHERE pkid=" . $int_pkid;
ExecuteQuery($str_query_delete);

# Delete Query
$str_query_delete = "DELETE FROM ".$STR_DB_TABLE_NAME. " WHERE pkid=" . $int_pkid;
ExecuteQuery($str_query_delete);

#---------------------------------------------------------------------------------------------------------------	
CloseConnection();
Redirect("./item_list.php?type=S&msg=D&admin=".RemoveQuote($str_user_name)."&#ptop");
exit();	
?>
