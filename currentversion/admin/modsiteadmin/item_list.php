<?php
/*
File Name  :- item_list.php
Create Date:- 01-02-2019
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_common.php";

/*if (strtoupper($_SESSION['superadmin'])=="NO")
{ 
        CloseConnection();
        Redirect("../admin_home.php");
        exit();
}*/
#------------------------------------------------------------------------------------------
# Initialization of variables used for message display.   
$str_name = "";
$str_fname = "";
$str_lname = "";
$str_addlen = "";
$str_city = "";
$str_zip = "";
$str_state = "";
$str_email = "";
$str_phone = "";
$str_bphone = "";
$str_fax = "";
$str_url = "";
$str_super = "";
$str_add = "";
#getting query string variables	
if(isset($_GET["stradminname"])) { $str_name = trim(MyHtmlEncode(RemoveQuote($_GET["stradminname"]))); }
if(isset($_GET["strfirstname"])) { $str_fname = trim(MyHtmlEncode(RemoveQuote($_GET["strfirstname"]))); }
if(isset($_GET["strlastname"])) { $str_lname = trim(MyHtmlEncode(RemoveQuote($_GET["strlastname"]))); }
if(isset($_GET["straddress"]))
{ 
    $str_add = trim(MyHtmlEncode(RemoveQuote($_GET["straddress"])));
    $str_addlen = trim(strlen($_GET["straddress"]));
} 
else { $str_addlen=0; }

if(isset($_GET["strcity"])) { $str_city=trim(MyHtmlEncode(RemoveQuote($_GET["strcity"])));  }
if(isset($_GET["strzip"])) { $str_zip=trim(RemoveQuote($_GET["strzip"])); }  
if(isset($_GET["strstate"])) { $str_state=trim(MyHtmlEncode(RemoveQuote($_GET["strstate"]))); }   
if(isset($_GET["stremail"])) { $str_email=trim(RemoveQuote($_GET["stremail"])); }  
if(isset($_GET["strhomephone"])) { $str_phone=trim(RemoveQuote($_GET["strhomephone"])); }   
if(isset($_GET["strbusinessphone"])) { $str_bphone=trim(RemoveQuote($_GET["strbusinessphone"])); } 
if(isset($_GET["strfax"])) { $str_fax=trim(RemoveQuote($_GET["strfax"])); }
if(isset($_GET["strurl"])) { $str_url=trim(RemoveQuote($_GET["strurl"])); } 
if(isset($_GET["super"])) { $str_super=trim($_GET["super"]); }

#Select query to get records from table.
#-------------------------------------------------------------------------------------------------------------	
$str_query_flag = "";
if (strtoupper($_SESSION['superadmin'])=="NO")
	{ $str_query_flag = "WHERE issuperadmin='NO' AND pkid=".$_SESSION['adminpkid']." AND username='".$_SESSION['adminname']."'"; }
else if(strtoupper($_SESSION['superadmin'])=="YES") { $str_query_flag = ""; }

$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME. " ".$str_query_flag." ".$STR_DB_TABLE_NAME_ORDER_BY." ";
$rs_list = GetRecordSet($str_query_select);
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
$str_varchar="";

if(isset($_GET['varchange'])) { $str_varchar=trim($_GET['varchange']); }
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
if (isset($_GET["admin"]))
{
    $str_name = trim(MyHtmlEncode(RemoveQuote($_GET["admin"])));
}
else
{
    $str_name = "";
}
#Get message text.
if(isset($_GET["msg"]))
{
    switch($_GET["msg"])
    {
        case("A"): $str_message = "Allow site admin login mode changed to '" . $str_varchar . "' successfully."; break;
        case("DU"): $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
        case("E"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;		
        case("N"): $str_message = $STR_MSG_ACTION_INVALID_USERID; break;
        case("P"): $str_message = $STR_MSG_ACTION_INVALID_PASSWORD; break;
        case("EMAIL"): $str_message = $STR_MSG_ACTION_INVALID_EMAIL; break;			
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-12 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE);?></h3></div>
    </div><hr> 
    <?php if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <span>
                                <a class="accordion-toggle collapsed " data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b></a>
                            </span>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <form name="frm_add" action="./item_add_p.php" method="post" onSubmit="return frm_add_validateform();">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Admin ID</label><span class="text-help-form"> *</span>
                                                    <input type="text" placeholder="<?php print($STR_MSG_LOGINID_FORMAT);?>" id="txt_name"  name="txt_name"  maxlength="20" class="form-control input-sm">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Password</label><span class="text-help-form"> *</span>
                                                    <input type="text" name="pas_password" placeholder="<?php print($STR_MSG_PASSWORD_FORMAT)?>" maxlength="20" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Name</label><span class="text-help-form"> </span>
                                                    <input type="text" name="txt_first_name" placeholder="<?php print($STR_PLACEHOLDER_FULLNAME)?>" value="<?php print($str_fname);?>" maxlength="50" class="form-control input-sm"> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email ID</label><span class="text-help-form"> </span>
                                                    <input type="text" placeholder="<?php print($STR_PLACEHOLDER_EMAIL)?>" name="txt_email" value="<?php print($str_email);?>" maxlength="100" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Address</label>
                                            <textarea name="ta_address" onKeyUp="javascript:return getLength('document.frm_add.txt_address_len','document.frm_add.ta_address');" rows="10" wrap="virtual" class="form-control input-sm" placeholder="Enter address here"><?php print($str_add); ?></textarea>
                                    </div>
                                    <?php /* ?><div class="form-group">
                                        <label><input name="txt_address_len" type="text" id="txt_address_len"  value="<?php print($str_addlen); ?>" readonly="true"  class="form-control input-sm text-center"></label>&nbsp;&nbsp;total characters entered
                                    </div><?php */ ?>
                                    <div class="form-group">
                                        <label>Is Super Admin</label>
                                            <span class="text-help-form"> *</span>
                                            <span class="text-help-form"> (<?php print($STR_MSG_ADMIN)?>)</span>
                                            <select name="cbo_super" class="form-control input-sm" >
                                                <option value="NO" <?php print(CheckSelected("NO",$str_super)); ?>>NO</option>
                                                <option value="YES" <?php print(CheckSelected("YES",$str_super)); ?>>YES</option>
                                            </select>
                                    </div>
                                <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
    	<table class="table table-striped table-bordered ">
            <thead>
                <tr class="text-center">
                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                    <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_DATE; ?></th>
                    <th width="10%"><?php print $STR_TABLE_COLUMN_NAME_LOGIN_ID; ?></th>
                    <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                    <?php /* ?><th width="22%">Email ID</th><?php */ ?>
                    <th width="6%"><?php print $STR_TABLE_COLUMN_NAME_IS_SUPER_ADMIN; ?></th>
                    <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_MANAGE_MODULE; ?></th>
                    <th width="7%"><?php print $STR_TABLE_COLUMN_NAME_ALLOW_SITE_ADMIN_LOGIN; ?></th>
                    <th width="6%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if($rs_list->EOF()==true)  {  ?>
                <tr><td colspan="9" align="center" class="alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                <?php } else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
                <tr>
                    <td align="center"><?php print($int_cnt)?></td>
                    <td align="center" class="text-help"><i><?php print(DDMMMYYYYFormat($rs_list->fields("registrationdate"))); ?></i></td>
                    <td align="center" class=""><b><?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("username")))); ?></b></td>
                    <td align="left">
                        <h4><b><?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("firstname")))); ?></b></h4>
                            <?php if($rs_list->fields("emailid") != "") { ?>
                            <p><i class="fa fa-envelope"></i>&nbsp;<?php print(DisplayEmail($rs_list->fields("emailid"),stuffcharacter($rs_list->fields("emailid"),30,"<br>"),"link"));?></p>
                            <?php } ?>
                            <p align="justify"><?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("address")))); ?></p>
                    </td>
                            <?php /* ?><td align="center"><?php print(DisplayEmail($rs_list->fields("emailid"),stuffcharacter($rs_list->fields("emailid"),30,"<br>"),"link"));?></td><?php */ ?>
                            <?php if($rs_list->fields("issuperadmin") == "YES") {
                            $str_adm="YES"; $str_adm_class="alert-success"; }
                            else { $str_adm="NO"; $str_adm_class="alert-danger"; } ?>
                    <td align="center" class="<?php print $str_adm_class; ?>"><?php print($str_adm); ?></td>
                    <td align="center">
                    <?php # 
                    if($rs_list->fields("issuperadmin") =="NO" && $_SESSION['superadmin'] == "YES") { ?><h4 class="nopadding"><a href="item_module_list.php?pkid=<?php print($rs_list->fields("pkid"))?>"><?php /*?><img src="../images/manage_page_ban.gif" border="0" title="Click to manage modules."><?php */?><i class="fa fa-bars"></i></a></h4><?php }?>
                    </td>
                    <?php if($rs_list->fields("allowlogin") == "YES") {
                            $str_visible = "";
                            $str_class="SetVisible";
                            $str_bg_class="alert-success";
                            $str_change = "NO";
                        } else {
                            $str_visible = "";
                            $str_class="SetInVisible";
                            $str_bg_class="alert-danger";
                            $str_change = "YES";
                        } ?>
                    <td align="center" class="<?php print($str_bg_class); ?>">
                        <?php if($rs_list->fields("issuperadmin") == "YES") { 
                        print($rs_list->fields("allowlogin")); }
                        else if($rs_list->fields("issuperadmin") == "NO" && $_SESSION['superadmin'] == "YES") {?>
                        <a href="item_status_p.php?pkid=<?php print($rs_list->fields("pkid"))?>" title="Click to change allow login mode to '<?php print($str_change) ?>'" class="<?php print($str_visible) ?> alert-link">
                        <?php print($rs_list->fields("allowlogin"))?></a>
                        <?php } else if($rs_list->fields("issuperadmin") == "NO" && $_SESSION['superadmin'] == "NO") { print($rs_list->fields("allowlogin")); } ?>
                    </td>
                    <td align="center">
                        <a class="btn btn-success btn-xs" href="./item_edit.php?pkid=<?php print($rs_list->fields("pkid"));?>" title="<?php print($STR_HOVER_EDIT);?>">
                            <?php print $STR_LINK_ICON_PATH_EDIT; ?>
                            </a>
                            <?php  if($rs_list->fields("issuperadmin") =="NO" && $_SESSION['superadmin'] == "NO") 
                            { ?>
                        <button class="btn btn-default btn-xs disabled">
                            <?php print $STR_LINK_ICON_PATH_DELETE; ?>
                        </button><?php }
                            else if($rs_list->fields("issuperadmin") == "YES" && $_SESSION['superadmin'] == "YES") {?>
                        <button class="btn btn-default btn-xs disabled">
                            <?php print $STR_LINK_ICON_PATH_DELETE; ?>
                        </button><?php }
                        else if($rs_list->fields("issuperadmin") == "NO" && $_SESSION['superadmin'] == "YES")  {  ?>
                        <a class="btn btn-danger btn-xs" href="item_del_p.php?pkid=<?php print($rs_list->fields("pkid"))?>" title="<?php print($STR_HOVER_DELETE); ?>" onClick="return delete_confirm();">
                            <?php print $STR_LINK_ICON_PATH_DELETE; ?>
                        </a>
                    <?php } ?>	
                    </td>
                </tr>
                <?php $int_cnt++; $rs_list->MoveNext(); } ?>
                <?php }  ?>
            </tbody>
	</table>
    </div>
<?php include "../../includes/help_for_list.php"; ?>
</div>
<?php include "../../includes/include_files_admin.php"; ?>  
<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>  
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>