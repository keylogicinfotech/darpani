<?php
/*
	Module Name:- modsiteadmin
	File Name  :- sa_status_p.php
	Create Date:- 01-02-2006
	Intially Create By :- 0022
	Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
	include "../includes/validatesession.php";
	include "../includes/configuration.php";
	include "../includes/lib_data_access.php";
	include "../includes/lib_datetimeyear.php";
	include "../includes/lib_common.php";

	if (strtoupper($_SESSION['superadmin'])=="NO")
	{ 
		CloseConnection();
		Redirect("../admin_home.php");
		exit();
	}

#	To check whether values are passed properly or not
	
	$int_pkid="";
	
	if(isset($_GET["id"]))
	{
		$int_pkid = trim($_GET["id"]);
	}
	if($int_pkid == "" || $int_pkid<=0 || !is_numeric($int_pkid))
	{
		CloseConnection();
		Redirect("./sa_list.php?type=E&msg=F");
		exit();
	}
	$str_select="";
	$str_select="select allowsiteadminlogin,issuperadmin,siteadminusername from t_siteadmin where siteadminpkid=".$int_pkid;
	$rs_status=GetRecordset($str_select);
	if($rs_status->eof())
	{
		CloseConnection();
		Redirect("./sa_list.php?type=E&msg=F");
		exit();
	}
	$str_allowlogin_value=$rs_status->fields("allowsiteadminlogin");
	$str_superuser=strtoupper($rs_status->fields("issuperadmin"));
	if($str_superuser=='YES')
	{
		CloseConnection();
		Redirect("./sa_list.php");
		exit();
	}
#	To change passed value
	if($str_allowlogin_value == "YES")
	{
		$str_change_value = "NO";
	}
	else
	{
		$str_change_value = "YES";
	}
	
#	Update query in t_siteadmin to update record in database
	$str_query_update = "update t_siteadmin set allowsiteadminlogin='" . $str_change_value . "' where siteadminpkid=" .$int_pkid;
	ExecuteQuery($str_query_update);
	
#-----------------------------------------------------------------------------------------------------------------------------	
	CloseConnection();
	Redirect("./sa_list.php?type=S&msg=A&varchange=".$str_change_value);
	exit();
?>

