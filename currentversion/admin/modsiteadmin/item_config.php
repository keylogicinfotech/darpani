<?php 
$STR_TITLE_PAGE = "Site Admin List";

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "t_siteadmin"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY registrationdate DESC"; 

global $STR_DB_TABLE_NAME_MODULE;
global $STR_DB_TABLE_NAME_SITE_MODULE;
$STR_DB_TABLE_NAME_MODULE = "t_module"; 
$STR_DB_TABLE_NAME_SITE_MODULE = "tr_site_module"; 

$STR_MSG_ADMIN = "Select YES if want to make this a super admin.";
$STR_MSG_ACCESS = "Check the checkbox to give access permission for that particular module. You can give access permission to multiple modules at a time.";
$STR_MSG_POSITION = "Position indicates position of that module on admin home page.<br/>i.e. If position is set to \"LEFT\" that module will be displayed on the left side on admin home page.";
?>