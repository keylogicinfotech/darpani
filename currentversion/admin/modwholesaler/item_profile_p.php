<?php
/*
File Name  :- item_profile.php
Create Date:- JAN-2019
Intially Create By :- 0014
Update History:
*/
#------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";       
#----------------------------------------------------------------------------------------------	
#initializing variables
$str_name="";
$str_password="";
$str_first_name ="";
$str_last_name="";
$str_address="";
$str_city="";
$str_zip="";
$str_state=""; 
$str_email=""; 
$str_home_phone=""; 
$str_business_phone="";
$str_fax="";
$str_url=""; 

#Get all the values and check whether values are passed properly or not.
if(isset($_POST["hdn_name"]))
{
        $str_name = trim($_POST["hdn_name"]);
}
if(isset($_POST["txt_first_name"]))
{
        $str_first_name = trim($_POST["txt_first_name"]);
}
if(isset($_POST["txt_last_name"]))
{
        $str_last_name = trim($_POST["txt_last_name"]);
}
if(isset($_POST["ta_address"]))
{
        $str_address = trim($_POST["ta_address"]);
}
if(isset($_POST["txt_city"]))
{
        $str_city = trim($_POST["txt_city"]);
}
if(isset($_POST["txt_zip"]))
{
        $str_zip = trim($_POST["txt_zip"]);
}
if(isset($_POST["txt_state"]))
{
        $str_state = trim($_POST["txt_state"]);
}
if(isset($_POST["txt_email"]))
{
        $str_email = trim($_POST["txt_email"]);
}
if(isset($_POST["txt_home_phone"]))
{
        $str_home_phone = trim($_POST["txt_home_phone"]);
}
if(isset($_POST["txt_business_phone"]))
{
        $str_business_phone = trim($_POST["txt_business_phone"]);
}
if(isset($_POST["txt_fax"]))
{
        $str_fax = trim($_POST["txt_fax"]);
}
if(isset($_POST["txt_url"]))
{
        $str_url = trim($_POST["txt_url"]);
}
#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
#check validation	
if( $str_name == "" || $str_first_name == "" || $str_last_name == "" || $str_address == "" || $str_city == "" || $str_zip == "" || $str_state == "" || $str_email == "")
{
        CloseConnection();
        Redirect("./item_profile.php?type=E&msg=F");
        exit();
}
if($str_address != "")
{
        if(strlen(RemoveQuote($str_address)) > 255)
        {
                CloseConnection();
                Redirect("./item_profile.php?type=E&msg=F");
                exit();			
        }
}
if($str_email!="")
{
        if(!validateEmail($str_email))
        {
                CloseConnection();
                Redirect("./item_profile.php?msg=EMAIL&type=E");
                exit();
        }
}
if($str_url!="")
{
        if(!validateURL($str_url))
        {
                CloseConnection();
                Redirect("./item_profile.php?msg=LINK&type=E");
                exit();
        }
}
#-------------------------------------------------------------------------------------------------------------------------------
#Update query to update table.
$str_query_update  = "UPDATE ".$STR_DB_TABLE_NAME. " SET firstname='" . ReplaceQuote($str_first_name) . "'";
$str_query_update .= ",lastname='" . ReplaceQuote($str_last_name) . "'";
$str_query_update .= ",address='" . ReplaceQuote($str_address) . "'";
$str_query_update .= ",city='" . ReplaceQuote($str_city) . "'";
$str_query_update .= ",state='" . ReplaceQuote($str_state) . "'";
$str_query_update .= ",zipcode='" . $str_zip . "',emailid='" . ReplaceQuote($str_email) . "'";
$str_query_update .= ",homephone='" . ReplaceQuote($str_home_phone) . "'";
$str_query_update .= ",businessphone='" . ReplaceQuote($str_business_phone) . "'";
$str_query_update .= ",fax='" . ReplaceQuote($str_fax) . "'";
$str_query_update .= ",url='" . ReplaceQuote($str_url) . "' WHERE username='".ReplaceQuote($str_name)."'";
ExecuteQuery($str_query_update);
#-------------------------------------------------------------------------------------------------------------------------------
#Clsoe connection and redirect to profile page
CloseConnection();
Redirect("./item_profile.php?type=S&msg=S");
exit();
?>