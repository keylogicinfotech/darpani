<?php
/*
File Name  :- item_edit.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_common.php";
/* if (strtoupper($_SESSION['superadmin'])=="NO")
{ 
    CloseConnection();
    Redirect("../admin_home.php");
    exit();
}*/
#----------------------------------------------------------------------------------------------------
# Get / Post Variables
$int_pkid = "";
if(isset($_GET["pkid"])) { $int_pkid = $_GET["pkid"]; }
# to check whether values are passed properly or not
if($int_pkid == "" || !is_numeric($int_pkid) || $int_pkid<=0)
{
    CloseConnection();
    Redirect("./item_list.php?type=E&msg=F");
    exit();
}
#----------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME. " WHERE pkid=" . $int_pkid;
$rs_list = GetRecordSet($str_query_select);	

$str_type = "";
$str_message = "";
#Get message type.
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
# Get message text.
if(isset($_GET["msg"]))
{
  switch($_GET["msg"])
    {
        case("F"): 	$str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("EMAIL"):	$str_message = $STR_MSG_ACTION_INVALID_EMAIL_FORMAT; break;	
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?>  : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_EDIT); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
            </div>
        </div>
        <div class="col-md-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE); ?></h3></div>
    </div><hr> 
    <?php #Diplay message.
    if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); }?>
    <div class="row padding-10">
    	<div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <i class="fa  fa-edit "></i>&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b> </a>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse">
                        <div class="panel-body">
                           <form name="frm_edit" action="item_edit_p.php" method="post" onSubmit="return frm_edit_validation()">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Wholesaler ID</label><br>
                                                    <span class="text-primary"><strong><?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("username")))); ?></strong></span>
                                                    <input name="hdn_name" type="hidden" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("username"))))?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Password</label><span class="text-help-form"> *</span>
                                                    <input type="text" placeholder="<?php print($STR_MSG_PASSWORD_FORMAT);?>" name="txt_password" value="<?php print(md5_decrypt(RemoveQuote($rs_list->fields("password")), $STR_ENCRYPTION_KEY))?>"  maxlength="20" class="form-control input-sm">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Name</label><span class="text-help-form"> </span>
                                                    <input type="text" name="txt_first_name" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("firstname")))) ?>" placeholder="<?php print($STR_PLACEHOLDER_FULLNAME);?>" maxlength="50" class="form-control input-sm"> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email ID</label><span class="text-help-form"> </span>
                                                    <input type="text" name="txt_email" value="<?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("emailid")))) ?>" placeholder="<?php print($STR_PLACEHOLDER_EMAIL)?>" maxlength="100" class="form-control input-sm" > 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Address</label>
                                            <textarea name="ta_address" id="ta_address" placeholder="<?php print($STR_PLACEHOLDER_ADDRESS);?>" onKeyUp="javascript:return getLength('document.frm_edit.txtaddresslen','document.frm_edit.ta_address');" rows="10" wrap="virtual" class="form-control input-sm"><?php print(MyHtmlEncode(RemoveQuote($rs_list->fields("address")))) ?></textarea>
                                    </div>
                                    <?php /* ?><div class="form-group">
                                        <label><input name="txtaddresslen" type="text" id="txtaddresslen"  value="<?php print(strlen($rs_list->fields("address"))); ?>" readonly="true"  class="text-center form-control input-group-sm"></label>&nbsp;&nbsp;total characters entered
                                    </div><?php */ ?>
                                    <div class="form-group">
                                        <label>Is Wholesaler Id</label><span class="text-help-form"> *</span>
                                            <span class="text-help-form">(<?php print($STR_MSG_WHOLESALER)?>)</span>
                                            <select name="cbo_super" class="form-control input-sm" >
                                                <option value="NO" <?php print(CheckSelected("NO",strtoupper($rs_list->fields("issuperadmin")))); ?>>NO</option>
                                                <option value="YES" <?php print(CheckSelected("YES",strtoupper($rs_list->fields("issuperadmin")))); ?>>YES</option>
                                            </select>
                                    </div>
                                <input name="hdn_pkid" type="hidden" value="<?php print($int_pkid) ?>"><input type="hidden" name="hdn_alwlgn" value="<?php print($rs_list->fields("allowlogin"));?>">
     <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <?php include "../../includes/help_for_edit.php"; ?>
</div>
<script language="JavaScript" src="./item_edit.js" type="text/javascript"></script>    
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>
