<?php
/*
File Name  :- item_add_p.php
Create Date:- Jan-2019
Intially Create By :- 014
Update History:
*/
#--------------------------------------------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_datetimeyear.php";
include "../../includes/lib_common.php";
#--------------------------------------------------------------------------------------------------------------------------------------
# Get / Post variables    
$str_super="";

#To check whether values are passed properly or not.	
if(isset($_POST["txt_name"]))
{
        $str_name = trim($_POST["txt_name"]);
}
if(isset($_POST["pas_password"]))
{
        $str_password = trim($_POST["pas_password"]);
}
if(isset($_POST["txt_first_name"]))
{
        $str_first_name = trim($_POST["txt_first_name"]);
}
/*	if(isset($_POST["txt_last_name"]))
{
        $str_last_name = trim($_POST["txt_last_name"]);
}*/
if(isset($_POST["ta_address"]))
{
    $str_address = trim($_POST["ta_address"]);
}//print $str_address; exit;
/*if(isset($_POST["txt_city"]))
{
        $str_city = trim($_POST["txt_city"]);
}
if(isset($_POST["txt_zip"]))
{
        $str_zip = trim($_POST["txt_zip"]);
}
if(isset($_POST["txt_state"]))
{
        $str_state = trim($_POST["txt_state"]);
}*/
if(isset($_POST["txt_email"]))
{
    $str_email = trim($_POST["txt_email"]);
}
/*if(isset($_POST["txt_home_phone"]))
{
        $str_home_phone = trim($_POST["txt_home_phone"]);
}
if(isset($_POST["txt_business_phone"]))
{
        $str_business_phone = trim($_POST["txt_business_phone"]);
}
if(isset($_POST["txt_fax"]))
{
        $str_fax = trim($_POST["txt_fax"]);
}
if(isset($_POST["txt_url"]))
{
        $str_url = trim($_POST["txt_url"]);
}*/
if(isset($_POST["cbo_super"]))
{
    $str_super = trim($_POST["cbo_super"]);
}
#-------------------------------------------------------------------------------------------------------------------------------
$str_redirect="&stradminname=".urlencode(RemoveQuote($str_name))."&strfirstname=".urlencode(RemoveQuote($str_first_name))."";
$str_redirect=$str_redirect."&straddress=".urlencode(RemoveQuote($str_address))."";
//$str_redirect=$str_redirect."&strcity=".urlencode(RemoveQuote($str_city))."&strzip=".urlencode(RemoveQuote($str_zip))."";
$str_redirect=$str_redirect."&stremail=".urlencode(RemoveQuote($str_email))."";
//$str_redirect=$str_redirect."&strhomephone=".urlencode(RemoveQuote($str_home_phone))."";
//$str_redirect=$str_redirect."&strbusinessphone=".urlencode(RemoveQuote($str_business_phone))."&strfax=".urlencode(RemoveQuote($str_fax))."";
$str_redirect=$str_redirect."&admin=".urlencode(RemoveQuote($str_name))."&super=".urlencode(RemoveQuote($str_super));
#-------------------------------------------------------------------------------------------------------------------------------
# To check required parameters are passed properly or not

if($str_name == "" || $str_password == "" || /*$str_first_name == "" || $str_email == "" ||*/ $str_super=="")
{
    CloseConnection();
    Redirect("./item_list.php?type=E&msg=F".$str_redirect."&#ptop");
    exit();
}
if(strstr($str_name," ")!=false || strstr($str_password," ")!=false)
{
    CloseConnection();
    Redirect("./item_list.php?type=E&msg=N".$str_redirect."&#ptop");
    exit();
}

if (strlen($str_password) < 6)
{	
    CloseConnection();
    Redirect("./item_list.php?type=E&msg=P".$str_redirect."&#ptop");
    exit();		
}
//	if($str_address != "")
//	{
//		if(strlen(RemoveQuote($str_address)))
//		{
//			CloseConnection();
//			Redirect("./item_list.php?type=E&msg=F".$str_redirect."&#ptop");
//			exit();
//		}
//	}	
if(!validateEmail($str_email))
{
    CloseConnection();
    Redirect("./item_list.php?type=E&msg=EMAIL".$str_redirect."&#ptop");
    exit();
}
#-------------------------------------------------------------------------------------------------------------------------------	
#select query from table to check whether particular admin already exists or not.
$str_query_select = "SELECT count(*) as NoOfRecords FROM ".$STR_DB_TABLE_NAME. " WHERE username='" . ReplaceQuote($str_name) . "'";
$rs_list_duplicate_check = GetRecordset($str_query_select);
if($rs_list_duplicate_check->fields("NoOfRecords") > 0)
{
    CloseConnection();
    Redirect("./item_list.php?type=E&msg=DU".$str_redirect."&#ptop");
    exit();
}
#-------------------------------------------------------------------------------------------------------------------------------#insert query into  table to insert record.
/*$str_query_insert = "insert into t_siteadmin(username,password,allowlogin,registrationdate,";
$str_query_insert .= "firstname,lastname,address,siteadmincity,siteadminstate,siteadminzipcode,";
$str_query_insert .= "siteadminemailid,siteadminhomephone,siteadminbusinessphone,siteadminfax,siteadminurl,issuperadmin)";
$str_query_insert .= " values('" . ReplaceQuote($str_name) . "','" . md5_encrypt(ReplaceQuote($str_password), $STR_ENCRYPTION_KEY ) . "','YES','";
$str_query_insert .= ReplaceQuote(YYYYMMDDFormat(date('Y-m-d'))) . "','" . ReplaceQuote($str_first_name) . "','";
$str_query_insert .= ReplaceQuote($str_last_name) . "','" . ReplaceQuote($str_address) . "','" . ReplaceQuote($str_city) . "','";
$str_query_insert .= ReplaceQuote($str_state) . "','" . ReplaceQuote($str_zip) . "','" . ReplaceQuote($str_email) . "','";
$str_query_insert .= ReplaceQuote($str_home_phone) . "','" . ReplaceQuote($str_business_phone) . "','";
$str_query_insert .= ReplaceQuote($str_fax) . "','" . ReplaceQuote($str_url) . "','" . ReplaceQuote($str_super) . "')";*/

$str_query_insert = "INSERT INTO ".$STR_DB_TABLE_NAME. "(username,password,allowlogin,registrationdate,";
$str_query_insert .= "firstname,address,emailid,issuperadmin)";
$str_query_insert .= " values ('" . ReplaceQuote($str_name) . "','" . md5_encrypt(ReplaceQuote($str_password), $STR_ENCRYPTION_KEY ) . "','YES','";
$str_query_insert .= ReplaceQuote(YYYYMMDDFormat(date('Y-m-d'))) . "','" . ReplaceQuote($str_first_name) . "','";
$str_query_insert .= ReplaceQuote($str_address) . "','" . ReplaceQuote($str_email) . "','" . ReplaceQuote($str_super) . "')";
ExecuteQuery($str_query_insert);
#-------------------------------------------------------------------------------------------------------------------------------
#Close connection and redirect to add page	
CloseConnection();
Redirect("./item_list.php?type=S&msg=S&admin=".urlencode(RemoveQuote($str_name))."&#ptop");
exit();
?>