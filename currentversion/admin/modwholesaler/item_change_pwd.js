function frm_change_password_validation()
{
	with(document.frm_change_password)
		{
			if(isEmpty(pas_old_password.value))	
				{
					alert("Please enter the old password. ");
					pas_old_password.select();
					pas_old_password.focus();
					return false;
				}

			if(isEmpty(pas_new_password.value))	
				{
					alert("Please enter the new password.");
					pas_new_password.select();
					pas_new_password.focus();
					return false;
				}

			if(isEmpty(pas_confirm_password.value))	
				{
					alert("Please enter the confirmed password.");
					pas_confirm_password.select();
					pas_confirm_password.focus();
					return false;
				}
			
			if(hdn_old_pwd.value!=pas_old_password.value)
			{
				 alert("Please enter valid old password.");	
				 pas_old_password.select();
				 pas_old_password.focus();
				 return false;
			}
			if(pas_new_password.value != pas_confirm_password.value)
			{
				alert("New password and confirm password should be same.");
				pas_confirm_password.select;
				pas_confirm_password.focus();
				return false;
			}	
		}
return true;
}
