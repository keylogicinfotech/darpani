<?php
/*
File Name  :- item_del_p.php
Create Date:- 18-JAN-2019
Intially Create By :- 0014
Update History:
*/
#-----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";
include "item_app_specific.php";
#-----------------------------------------------------------------------------------------------------	
#Get values of all passed GET / POST variables
$int_pkid=0;
if(isset($_GET['pkid']))
{
    $int_pkid=$_GET['pkid'];
}
if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
{
    CloseConnection();
    Redirect("item_list.php?type=E&msg=F");
    exit();
}
#-----------------------------------------------------------------------------------------------------	
#Delete Query
$str_query_update="";
$str_query_update="DELETE FROM ".$STR_DB_TABLE_NAME." WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#-----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?type=S&msg=D&tit=".urlencode(RemoveQuote($str_title)));
exit();
#-----------------------------------------------------------------------------------------------------
?>	
	
