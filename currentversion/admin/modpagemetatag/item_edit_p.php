<?php
/*
File Name  :- item_edit_p.php
Create Date:- Jan-2019
Intially Create By- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_app_specific.php";
include "item_config.php";
#initializing variables
$int_pkid="";
$str_title="";
$str_keyword="";
$str_desc="";
$str_sub="";
$str_index="";
$str_follow="";
$str_archive="";
$str_visible="";
$str_name="";
$int_revisit="";
$str_expires="";
$str_cache="";
#getting post datas	
if(isset($_POST['hdn_pkid']))
{
        $int_pkid=trim($_POST['hdn_pkid']);
}
if(($int_pkid==trim("")) || ($int_pkid<0) || (!is_numeric($int_pkid)))
{
        CloseConnection();
        Redirect("item_list.php?msg=F&type=E&#ptop");
        exit();
}

if(isset($_POST['hdn_page']))
{
        $str_name=trim($_POST['hdn_page']);
}
if(isset($_POST['txt_title']))
{
        $str_title=trim($_POST['txt_title']);
}
if(isset($_POST['ta_keyword']))
{
        $str_keyword=trim($_POST['ta_keyword']);
}
if(isset($_POST['ta_desc']))
{
        $str_desc=trim($_POST['ta_desc']);
}
if(isset($_POST['ta_subject']))
{
        $str_sub=trim($_POST['ta_subject']);
}
if(isset($_POST['txt_revisit']))
{
        $int_revisit=trim($_POST['txt_revisit']);
}	
if(isset($_POST['cbo_index']))
{
        $str_index=trim($_POST['cbo_index']);
}
if(isset($_POST['cbo_follow']))
{
        $str_follow=trim($_POST['cbo_follow']);
}
if(isset($_POST['cbo_archive']))
{
        $str_archive=trim($_POST['cbo_archive']);
}
if(isset($_POST['cbo_cache']))
{
        $str_cache=trim($_POST['cbo_cache']);
}
if(isset($_POST['txt_expires']))
{
        $str_expires=trim($_POST['txt_expires']);
}
if(isset($_POST['cbo_visible']))
{
        $str_visible=trim($_POST['cbo_visible']);
}
	
#------------------------------------------------------------------------------------------------------------
#check validation
if(($str_index==trim("")) || ($str_follow==trim("")) || ($str_archive==trim("")) || (strlen($str_sub)>255))
{
	CloseConnection();
	Redirect("item_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid)."&#ptop");
	exit();
}	

if($int_revisit!="")
{
	if (($int_revisit < 0) || (!is_numeric($int_revisit)))
	{
		CloseConnection();
		Redirect("item_edit.php?msg=F&type=E&pkid=".urlencode($int_pkid)."&#ptop");
		exit();
	}
}
#---------------------------------------------------------------------------------------------------------------------
#update query in table
$str_query_update="";
$str_query_update="UPDATE ".$STR_DB_TABLE_NAME. " SET titletag='".ReplaceQuote($str_title)."',";
$str_query_update.="keywordtag='".ReplaceQuote($str_keyword)."',";
$str_query_update.="descriptiontag='".ReplaceQuote($str_desc)."',";
$str_query_update.="subjecttag='".ReplaceQuote($str_sub)."',";
$str_query_update.="revisitaftertag='".$int_revisit."',";
$str_query_update.="indextag='".ReplaceQuote($str_index)."',";
$str_query_update.="followtag='".ReplaceQuote($str_follow)."',";
$str_query_update.="archivetag='".ReplaceQuote($str_archive)."',";
$str_query_update.="pragmatag='".ReplaceQuote($str_cache)."',";
$str_query_update.="expirestag='".ReplaceQuote($str_expires)."',";
$str_query_update.="visible='".ReplaceQuote($str_visible)."'";
$str_query_update.=" WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#-----------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_list.php?msg=U&type=S&tit=".urlencode(RemoveQuote($str_name))."&#ptop");
exit();	

?>
