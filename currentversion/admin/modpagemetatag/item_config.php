<?php

$STR_TITLE_PAGE = "SEO Metatag List";
$STR_TITLE_PAGE_HEADING = "SEO Management - Metatag Page List";
$UPLOAD_IMG_PATH = "../../mdm//";
$INT_IMG_WIDTH = 0;
$INT_IMG_HEIGHT = 0;

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = "t_page_metatag"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY displayorder DESC,title";

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "";

global $XML_FILE_PATH;
global $XML_ROOT_TAG;
$XML_FILE_PATH = "../../mdm/xmlmodulefiles/page_metatag.xml";
$XML_ROOT_TAG = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";	

$STR_MSG_TITLE="Page titles are one of the most important factors for search engine friendly web page. The title should contain all keyword phrases that are related to page content.";	
$STR_MSG_KEYWORD="Keyword contain the most relevant words and most important phrases which are synonyms and alternates of page title. Keywords are useful to search page through search engine.";	
$STR_MSG_DESCRIPTION="Description  defines ideas & general content of the page. This description is presented to user by search engine along with the document's title as the result of a search.";
//$STR_CACHE="<b>Note:</b> <b>No-cache (read new)</b> option tells the <u>Browser</u> not to store a copy of your page in your hard drive. If you update the page frequently and want users to see the newest version at all times, then use <b>No-Cache (read new)</b> option.";
//$STR_EXPIRES="<b>Note:</b> If the page is requested after entered date, the <u>Browser</u> should load a new copy from the server, instead of using the copy in its cache. You can enter Date or No. of days<br>Date Format: <b>Sunday, 01-May-05 10:10:35 GMT</b> which means the page will expire on this date. Date must be in GMT.<br>OR <b>+30</b> indicates that the page expires in 30 days from the time it is read by Search Engine.<br>If 0 is set it is interpreted as \"Now\". \"Never\" & \"Immediately\" can also be set.";
//$STR_SUBJECT="<b>Note:</b> The Subject is synopsis of what the page is about. Subject idicates the theme of the website used by <u>Search Engine</u>.";
//$STR_REVISIT="<b>Note:</b> The Revisit-After defines how often the <u>Search Engine</u> should come to your website for re-indexing.";		
//$STR_INDEX="<b>Note:</b> If Index is set to YES then that page will be indexed by <u>Search Engine</u>, which means you are allowing the <u>Search Engine</u> to include your page within their search directory, but if set to NO then that page will not be indexed, that is your page will not appear in their search directory.";
//$STR_FOLLOW="<b>Note:</b> If Follow is set to YES then the links on that page will be followed/explored by <u>Search Engine</u>, but if set to NO then the links on that page cannot be followed/explored by <u>Search Engine</u>.";
//$STR_ARCHIVE="<b>Note:</b> If Archive is set to YES then the page will be cached by <u>Search Engine</u>, but if set to NO then the page will not be cached by <u>Search Engine</u>.";		
?>
