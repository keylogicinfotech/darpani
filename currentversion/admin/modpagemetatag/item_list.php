<?php
/*
File Name  :- item_list.php
Create Date:- Jan-2019
Intially Create By- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "item_app_specific.php";
include "item_config.php";
#----------------------------------------------------------------------------------------------------
#Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME. " WHERE visible='YES' ".$STR_DB_TABLE_NAME_ORDER_BY. " ";
$rs_list=GetRecordset($str_query_select);
#------------------------------------------------------------------------------------------
# Initialization of GET / POST variables.   
$str_type = "";
$str_message = "";
$str_title = "";
$str_mode = "";
if(isset($_GET['tit']))
{
    $str_title = trim(RemoveQuote($_GET['tit']));
}
if(isset($_GET['mode']))
{
    $str_mode = trim($_GET['mode']);
} 
#Get message type.
if(isset($_GET['type']))
{
    switch(trim($_GET['type']))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Get message text.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT ." - ". MyHtmlEncode(MakeStringShort($str_title,30)) ; break;
        //case("V"):$str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode; break;	
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
    }
} ?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_SEO);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
    <div class="container">
        <?php include($STR_ADMIN_HEADER_PATH); ?>
	<div class="row padding-10">
            <div class="col-md-3 col-sm-6 col-md-12 button_space">
            <?php /*?><div class="btn-group" role="group" aria-label="...">
				<a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="fa fa-chevron-left "></i>&nbsp;Back</a>
			</div><?php */?>
            </div>
            <div class="col-md-9 col-sm-6 col-md-12" align="right" ><h3><?php print($STR_TITLE_SEO);?></h3></div>
	</div><hr> 
	<div class="row padding-10">
            <div class="col-md-12">
        	<div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <div class="row padding-10">
                                    <div class="col-md-12" align="right"><?php print($STR_LINK_HELP); ?></div>
                                </div>
                            </h4>
			</div>
                    </div>
                </div>
            </div>
	</div>
	<?php if($str_type != "" && $str_message != "") { print(DisplayMessage(0,$str_message,$str_type)); } ?>
	<div class="table-responsive">
    	<table class="table table-striped table-bordered ">
			<form name="frm_list" method="post" action="item_order_p.php" onSubmit="return frm_list_check_displayorder();">
				<thead>
					<tr>
                                            
                                             <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                                             <th width="20%"><?php print $STR_TABLE_COLUMN_NAME_PAGES; ?></th>
                                            <th width=""><?Php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                                            <th width="5%"><?Php print $STR_TABLE_COLUMN_NAME_DISPLAY_ORDER; ?></th>
                                            <th width="4%"><?Php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>
						
					</tr>
				</thead>
				<tbody>
					<?php if($rs_list->EOF()==true)  {  ?>
						<tr><td colspan="5" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
							<?php } else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
					<tr class="<?php print($int_cnt%2); ?>">
						<td align="center"><?php print($int_cnt)?></td>
						<td align="left">
						<?php /*?><p><b><a href="#a<?php print($int_cnt)?>" id="a_title<?php print($int_cnt)?>" onClick="return viewdetail(<?php print($int_cnt)?>);" title="Click to view details of this page" class="link">				
							<?php print(MyHtmlEncode($rs_list->fields("title")));?></a></b></p><?php */?>
                                                    <h4><b><?php print(MyHtmlEncode($rs_list->fields("title")));?></b></h4>
							</td>
							<td>
							<div align="justify" id="div_com<?php print($int_cnt)?>">
								<?php if($rs_list->Fields("titletag")!="") { ?><span><b>Title: </b></span><?php print($rs_list->Fields("titletag"));?> <br><?php } ?>
								<?php if($rs_list->Fields("keywordtag")!="") { ?><span><b>Keywords: </b></span><?php print($rs_list->Fields("keywordtag"));?> <br><?php } ?>
								<?php if($rs_list->Fields("descriptiontag")!="") { ?><span><b>Description: </b></span><?php print($rs_list->Fields("descriptiontag"));?> <br><?php } ?>
                                                                
								<?php if($rs_list->Fields("subjecttag")!="") { ?><span><b>Subject: </b></span><?php print($rs_list->Fields("subjecttag"));?> <?php } ?>
							</div> 
						</td>
						<td align="center">
							<input type="text" name="txt_displayorder<?php print($int_cnt);?>" maxlength="4" value="<?php print($rs_list->fields("displayorder"));?>" class="form-control input-sm text-center" >
							<input type="hidden" name="hdn_pkid<?php print($int_cnt)?>" value="<?php print($rs_list->fields("pkid"))?>"> 
						</td>
						<?php $str_image="";
							if(strtoupper($rs_list->fields("visible"))=="YES") { $str_image = "<img src='../images/visible.gif' border='0' title=\"Click to set 'Invisible'\">"; }
							else { $str_image = "<img src='../images/invisible.gif' border='0' title=\"Click to set 'Visible'\">"; } ?>
						<td align="center"><a class="btn btn-success btn-xs" href="item_edit.php?pkid=<?php print($rs_list->fields("pkid"))?>" title="Click to edit details" ><?php print $STR_LINK_ICON_PATH_EDIT; ?></a></td>
					</tr>	
						<?php $rs_list->movenext(); $int_cnt=$int_cnt+1; } ?>	
					<tr class="<?php print($int_cnt%2); ?>">
						<td><input type="hidden" name="hdn_counter" value="<?php print($int_cnt)?>"></td><td></td><td></td>
						<td align="center" valign="middle"><?php print DisplayFormButton("SAVE",0); ?></td>
					</tr>
					<?php } ?>	
				</tbody>	
			</form>
		</table>
    </div>
	<?php include "../../includes/help_for_list.php"; ?>
    <!-- /.container -->
</div>
<script language="JavaScript" src="./item_list.js"type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>