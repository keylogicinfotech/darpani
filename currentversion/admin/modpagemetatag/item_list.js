function frm_list_check_displayorder()
{
    with(document.frm_list)
    {
        cnt=hdn_counter.value;
        for(i=1;i<cnt;i++)
        {
            if(trim(eval("txt_displayorder" + i).value) =="")
            {
                alert("Please enter display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }

            if(eval("txt_displayorder" + i).value <0)
            {
                alert("Please enter positive integer for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }

            if(isNaN(eval("txt_displayorder" + i).value))
            {
                alert("Please enter numeric value for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }
        }
    }
    return true;
}


function viewdetail(divno)
{		
	if(document.getElementById("div_com"+ divno).style.display=="block")
	{
		document.getElementById("div_com"+ divno).style.display="none";
		document.getElementById("a_title"+ divno).title="Click to view details";
	}
	else
	{
		document.getElementById("div_com"+ divno).style.display="block";
		document.getElementById("a_title"+ divno).title="Click to hide details";
	}	
	return true;
}

function viewdetaillist()
{
	var counter,i;
	with(document.frm_page_list)
	{
		counter=hdn_counter.value;		
		if(document.getElementById("div_all").innerHTML=="View Details")
		{
			document.getElementById("div_all").innerHTML="Hide Details";
			document.getElementById("div_all").title="Click to hide details";
			for (i=1;i<counter;i++)
			{
				document.getElementById("div_com"+ i).style.display="block";
			}
		}
		else
		{
			document.getElementById("div_all").innerHTML="View Details";
			document.getElementById("div_all").title="Click to view details";
			for (i=1;i<counter;i++)
			{
				document.getElementById("div_com"+ i).style.display="none";
			}
		}
	}
	return true;
}