<?php
#write to xml file
include "../../includes/lib_xml.php";
include "item_config.php";

function WriteXml()
{
        global $STR_DB_TABLE_NAME;
        global $STR_DB_TABLE_NAME_ORDER_BY;
	global $XML_FILE_PATH;
	global $XML_ROOT_TAG;
	
	$str_query_select = "";
	$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME. " WHERE visible='YES' ".$STR_DB_TABLE_NAME_ORDER_BY. ",pkid ";
	$rs_list = GetRecordSet($str_query_select);
	
	#storing recordset data into array.
		$arr = array();
		$arr1 = array();
		$i = 0;
		$flag = 0;
		while(!$rs_list->eof())
		{
                    $flag=1;
                    $pagekey="";
                    $pagekey=$rs_list->fields("pagekey");
                    $tagvalue="";
                    if($rs_list->Fields("pragmatag")!="")
                    {
                            $tagvalue="<meta http-equiv=\"pragma\" content=\"".$rs_list->Fields("pragmatag")."\">|";
                    }
                    else
                    {
                            $tagvalue="";
                    }
                    if($rs_list->Fields("expirestag")!="")
                    {
                            $tagvalue.="<meta http-equiv=\"expires\" content=\"".str_replace("\"","",$rs_list->Fields("expirestag"))."\">|";
                    }
                    else
                    {
                            $tagvalue.="";
                    }
                    if($rs_list->Fields("keywordtag")!="")
                    {
                            $tagvalue.="<meta name=\"keyword\" content=\"".str_replace("\"","",$rs_list->Fields("keywordtag"))."\">|";
                    }
                    else
                    {
                            $tagvalue.="";
                    }
                    if($rs_list->Fields("descriptiontag")!="")
                    {
                            $tagvalue.="<meta name=\"description\" content=\"".str_replace("\"","",$rs_list->Fields("descriptiontag"))."\">|";
                    }
                    else
                    {
                            $tagvalue.="";
                    }
                    if($rs_list->Fields("subjecttag")!="")
                    {
                            $tagvalue.="<meta name=\"subject\" content=\"".str_replace("\"","",$rs_list->Fields("subjecttag"))."\">|";
                    }
                    else
                    {
                            $tagvalue.="";
                    }
                    if($rs_list->Fields("revisitaftertag")!="")
                    {
                            $tagvalue.="<meta name=\"revisit-after\" content=\"".$rs_list->Fields("revisitaftertag")." Days" ."\">|";
                    }
                    else
                    {
                            $tagvalue.="";
                    }
                    $str_index_tag="";
                    $str_follow_tag="";
                    $str_archive_tag="";
                    if($rs_list->Fields("indextag")=="YES")
                    {
                            $str_index_tag="Index";
                    }
                    else
                    {
                            $str_index_tag="Noindex";
                    }
                    if($rs_list->Fields("followtag")=="YES")
                    {
                            $str_follow_tag="Follow";
                    }
                    else
                    {
                            $str_follow_tag="Nofollow";
                    }
                    if($rs_list->Fields("archivetag")=="YES")
                    {
                            $str_archive_tag="Archive";
                    }
                    else
                    {
                            $str_archive_tag="Noarchive";
                    }
                    $tagvalue.="<meta name=\"robot\" content=\"".$str_index_tag.", ".$str_follow_tag.", ".$str_archive_tag."\">";
                    if($flag==1)
                    {
                        $arr[$i]=$pagekey;
                        $arr1[$i]=$tagvalue;
                        $i=$i+1;
                        $rs_list->MoveNext();	
                    }
		}
		writeXmlFile($XML_FILE_PATH,$XML_ROOT_TAG,$arr,$arr1);
}

?>