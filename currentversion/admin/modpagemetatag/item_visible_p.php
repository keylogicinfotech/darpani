<?php
/*
File Name  :- item_visible_p.php
Create Date:- Jan-2019
Intially Create By- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_app_specific.php";
	
#get the data
$int_pkid=0;
if(isset($_GET['pkid']))
{
        $int_pkid=$_GET['pkid'];
}
if($int_pkid<=0 || !is_numeric($int_pkid) || $int_pkid=="")
{
        CloseConnection();
        Redirect("item_list.php?type=E&msg=F");
        exit();
}
#select query to get the details of pkid from table
$str_query_select="";
$str_query_select="SELECT title,visible FROM ".$STR_DB_TABLE_NAME. " WHERE pkid=".$int_pkid;
$rs_list_visible=GetRecordset($str_query_select);
$str_title=$rs_list_visible->Fields("title");
if($rs_list_visible->eof())
{
        CloseConnection();
        Redirect("item_list.php?type=E&msg=F");
        exit();
}
$str_visible=$rs_list_visible->Fields("visible");
if(strtoupper($str_visible)=='YES')
{
        $str_visible="NO";
        $str_visible_title="Invisible";
}
else if(strtoupper($str_visible)=='NO')
{
        $str_visible="YES";
        $str_visible_title="Visible";
}
#-----------------------------------------------------------------------------------------------------------------------------
#update query to change the mode
$str_query_update="";
$str_query_update="UPDATE ".$STR_DB_TABLE_NAME. " SET visible='" .ReplaceQuote($str_visible). "' WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_update);
	
#-----------------------------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#----------------------------------------------------------------------------------------------------------------------------	
CloseConnection();
Redirect("item_list.php?type=S&msg=V&mode=".urlencode(RemoveQuote($str_visible_title))."&tit=".urlencode(RemoveQuote($str_title)));
exit();
	
?>