<?php
/*
File Name  :- item_edit.php
Create Date:- Jan-2019
Intially Create By- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "item_app_specific.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#----------------------------------------------------------------------------------------------------
# Values using GET/POST variables
$int_pkid = "";
if(isset($_GET['pkid'])) { $int_pkid = trim($_GET['pkid']); }
if($int_pkid == "" || $int_pkid <= 0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
#----------------------------------------------------------------------------------------------------
# Select Query
$str_page = "";

$str_query_select = "";
$str_query_select = "SELECT * FROM ".$STR_DB_TABLE_NAME. "  WHERE pkid=".$int_pkid;
//print $str_query_select;
$rs_list_edit = GetRecordset($str_query_select);
$str_page = $rs_list_edit->Fields("title");

if($rs_list_edit->eof())
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
$str_title = "";
if(isset($_GET['tit']))
{
    $str_title=trim(RemoveQuote($_GET['tit']));
}
# Get message type.
if(isset($_GET['type']))
{
    switch(trim($_GET['type']))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
# Get message text.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("U"): $str_message = $STR_MSG_ACTION_UPDATE ." - ". MyHtmlEncode(MakeStringShort($str_title,30)) ; break;
    }
}
# Diplay message.
if($str_type != "" && $str_message != "")
{
    print(DisplayMessage(0,$str_message,$str_type));
} ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_EDIT); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<a name="ptop"></a>
<div class="container">
<?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-md-12 button_space">
            <a href="./item_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?>"><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
        </div>
        <div class="col-md-9 col-sm-6 col-md-12" align="right" ><h3><?php print($STR_TITLE_EDIT); ?>: <?php print($STR_TITLE_PAGE); ?></h3></div>
    </div><hr>
	<?php if($str_type != "" && $str_message != "") { print(DisplayMessage("99%",$str_message,$str_type)); } ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <i class="fa fa-edit "></i>&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b>
                            <?php print($STR_LINK_HELP); ?>	
                        </h4>
                    </div>
                    <div id="collapse01" class="panel-collapse">
                        <div class="panel-body">
                            <form name="frm_edit" action="item_edit_p.php" method="post" onSubmit="return frm_edit_validateform()" enctype="multipart/form-data" role="form">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="form-group">
                                    <label>Page Name</label><br/>
                                    <h4><b class="text-primary"><?php print (MyHtmlEncode(RemoveQuote($rs_list_edit->Fields("title"))));?></b></h4>
                                </div>
                                <div class="form-group">
                                    <label>Page Title <span class="text-help-form"> *</span></label>&nbsp;<span class="text-help-form">(<?php print($STR_MSG_TITLE); ?>)</span>
                                    <input type="text" name="txt_title"  class="form-control input-sm" maxlength="255" value="<?php print(RemoveQuote($rs_list_edit->fields("titletag"))); ?>" placeholder="<?php print($STR_PLACEHOLDER_TITLE);?>">
                                </div>
                                <div class="form-group">
                                    <label>Keywords </label>&nbsp;<span class="text-help-form">(<?php print($STR_MSG_KEYWORD); ?>)</span>
                            
                                   <textarea class="form-control input-sm" name="ta_keyword" id="ta_keyword"  type="text" placeholder="<?php print($STR_PLACEHOLDER_SEO);?>"><?php print(RemoveQuote($rs_list_edit->fields("keywordtag"))); ?></textarea>  
                                    
                                </div>
                                <div class="form-group">
                                    <label>Description</label>&nbsp;<span class="text-help-form">(<?php print($STR_MSG_DESCRIPTION); ?>)</span>
                                    <textarea class="form-control input-sm" name="ta_desc" id="ta_desc" onKeyUp="javascript:return getLength('document.frm_edit.txtdesclen','document.frm_edit.ta_desc');" type="text" placeholder="<?php print($STR_PLACEHOLDER_DESC);?>"><?php print(RemoveQuote($rs_list_edit->fields("descriptiontag"))); ?></textarea>
                                </div>
                                
                          <?php /* ?>      <div class="form-group">
                                    <label><input name="txtdesclen"  id="txtdesclen"  value="<?php print(strlen($rs_list_edit->fields("descriptiontag"))); ?>" readonly="true"  type="text" class="form-control input-group-sm text-center" ></label>&nbsp;&nbsp;total characters entered
                                </div>  <?php */ ?>
                                <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                <input type="hidden" name="hdn_pkid" value="<?php print($int_pkid);?>"> 
                                <input type="hidden" name="hdn_page" value="<?php print($str_page);?>">
                                <input type="hidden" name="cbo_cache" value="No-Cache">
                                <input type="hidden" name="txt_expires" value="0">
                                <input type="hidden" name="ta_subject" value="<?php print($rs_list_edit->fields("subjecttag")); ?>">
                                <input type="hidden" name="txt_revisit" value="0">
                                <input type="hidden" name="cbo_index" value="YES">
                                <input type="hidden" name="cbo_follow" value="YES">
                                <input type="hidden" name="cbo_archive" value="NO">
                                <input type="hidden" name="cbo_visible" value="YES">		
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "../../includes/help_for_edit.php"; ?>		
    <!-- /.container -->											
</div>
<?php include "../../includes/include_files_admin.php"; ?>
<script language="JavaScript" src="./item_edit.js" type="text/javascript"></script>
<?php /* ?><script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_keyword');
		//new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
  });
		// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
		// convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_keyword');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script><?php */ ?>

<?php include($STR_ADMIN_FOOTER_PATH); ?>
<script type="text/javascript" src="../../js/richetxteditor.js"></script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_desc');
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_keyword');
		//new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
  });
		// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
		// convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_desc');
             new nicEditor({fullPanel : true}).panelInstance('ta_keyword');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>
</body></html>