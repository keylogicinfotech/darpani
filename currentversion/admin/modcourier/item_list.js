function frm_list_check_displayorder()
{
    with(document.frm_list)
    {
        cnt=hdn_counter.value;
        for(i=1;i<cnt;i++)
        {
            if(trim(eval("txt_displayorder" + i).value) =="")
            {
                alert("Please enter display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }

            if(eval("txt_displayorder" + i).value <0)
            {
                alert("Please enter positive integer for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }

            if(isNaN(eval("txt_displayorder" + i).value))
            {
                alert("Please enter numeric value for display order.");
                eval("txt_displayorder" + i).select();
                eval("txt_displayorder" + i).focus();
                return false;
            }
        }
    }
    return true;
}

function frm_list_confirmdelete()
{
    if(confirm("Are you sure you want to delete this details?"))
    {
        if(confirm("Confirm Deletion:Click 'Ok' to delete this details or 'Cancel' to deletion."))
        {
            return true;
        }
    }
    return false;
}

function show_details(Url)
{
    window.open(Url,'About','left=50,top=20,scrollbars=yes,resizable=yes,width=570,height=490');
    return false;
}

function frm_add_validateform()
{
    with(document.frm_add)
    {
        if(trim(txt_name.value) == "")
        {
            alert("Please enter name.");
            txt_name.select();
            txt_name.focus();
            return false;
        }
        
       
        if(trim(txt_phoneno.value) == "")
        {
            alert("Please enter Phone Number.");
            txt_phoneno.select();
            txt_phoneno.focus();
            return false;
        }
        if(trim(cbo_country.value) == "")
        {
            alert("Please Select Country.");
            cbo_country.select();
            cbo_country.focus();
            return false;
        }
   /*     if(trim(cbo_state.value) == "")
        {
                alert("Please Select State.");
                cbo_state.select();
                cbo_state.focus();			
                return false;
        } */

       
        if(txt_city.value=="")
        {
            
                alert("Please enter City.");
                txt_city.select();
                txt_city.focus();
                return false;					
            
        }
        
         if(trim(txt_zipcode.value) == "")
        {
                alert("Please Enter Zipcode.");
                txt_zipcode.select();
                txt_zipcode.focus();			
                return false;
        }

       
        if(txt_address.value=="")
        {
            
                alert("Please enter address.");
                txt_address.select();
                txt_address.focus();
                return false;					
           
        }

        /*if(trim(txt_city.value)!="")
        {
                if(!isValidUrl(trim(txt_city.value)))
                {
                        txt_city.select();
                        txt_city.focus();
                        return false;			
                }
        }

        if(cbo_visible.value == "")
        {
                alert("Please set value for visible.");
                cbo_visible.focus();			
                return false;
        }*/

    }
    return true;
}
