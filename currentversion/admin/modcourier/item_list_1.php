<?php
/*
File Name  :- item_list.php
Create Date:- MARCH2019
Intially Create By :- 0013
Update History:
*/
#--------------------------------------------------------------------------------------
# include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_datetimeyear.php";
#--------------------------------------------------------------------------------------
#initialize variables
$int_page="";
$int_cat_pkid="";
$str_key="";
#---------------------------------------------------------------------------------------
# get filter data
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{
    $int_cat_pkid=trim($_GET["catid"]);
}
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{
    $str_key=trim($_GET["key"]);
}
#   get paging data
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"])==true && trim($_GET["PagePosition"])>0)
{
    $int_page= $_GET["PagePosition"];
}
else
{
    $int_page=1;
}
$str_filters="";
$str_filters="&catid=".$int_cat_pkid."&key=".$str_key."&PagePosition=".$int_page;

#------------------------------------------------------------------------------------------------

$int_field=0;
$page=1;
$int_count=0;
$int_srno=1;
$str_key = "";
if(isset($_GET["str_filter"]))
{
    $int_field = trim($_GET["str_filter"]);
}

if(isset($_GET["key"]))
{
	$str_key = trim($_GET["key"]);
}
#----------------------------------------------------------------------------------------------------
# SELECT Filter Criteria
$str_filter="";
if($int_field == "" || !is_numeric($int_field) || $int_field<=0)
    {
    $int_field = 0;
    $str_filter="";
    }
else
    {
        $str_fieldname="";
        if($int_field==1)
        {
                $str_fieldname="name";
        }
        elseif($int_field==2)
        {
                $str_fieldname="loginid";
        }
        elseif($int_field==3)
        {
                $str_fieldname="email";
        }
        else
        {
                $str_fieldname="";
        }
        if($str_fieldname!="" && $str_key!="")
        {
                $str_filter=" WHERE ".$str_fieldname." LIKE '".$str_key."%'";
        }
//        elseif($str_fieldname != "" )
    }
	
#----------------------------------------------------------------------------------------------------

#get paging/filter parameters
/*$page=1;
if (isset($_GET["PagePosition"])) { $page = trim($_GET["PagePosition"]); }
$key="";
if (isset($_GET["key"])) { $key = trim($_GET["key"]); }

$int_cat_id="0"; // To display all models
if (isset($_GET["catid"])) { $int_cat_id = trim($_GET["catid"]); }
*/
$str_query_filter="";
/*if($str_key != "") {
    $str_query_filter=" WHERE UPPER(loginid) like '".$str_key."%'"; 
}*/
#------------------------------------------------------------------------------------------
$int_count=0;
$str_query_select="";
# Select Query to get total no of record
$str_query_select="";
$str_query_select="SELECT COUNT(DISTINCT pkid) as total_record FROM " .$STR_DB_TABLE_NAME.$str_query_filter;
$rs_list_total=GetRecordSet($str_query_select);
$int_total_record = $rs_list_total->fields("total_record");

$int_record_per_page=$INT_RECORD_PER_PAGE;				
$int_total_page=ceil($int_total_record / $int_record_per_page); 
if ($int_page > $int_total_page)
{ $int_page=$int_total_page; }
if($int_page < 1)
{ $int_page=1; }
$int_limit_start=($int_page -1)* $int_record_per_page;	
#------------------------------------------------------------------------------------------
$str_query_select = "";
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME. " a";
$str_query_select .=" ".$str_filter; 
$str_query_select .=" ORDER BY pkid DESC";
$str_query_select .= " LIMIT ".$int_limit_start.",".$int_record_per_page;
//print $str_query_select;
$rs_list = GetRecordSet($str_query_select);
//print $int_record_per_page;
if($rs_list->Count()>0)
{ $srno=($int_page -1)* $int_record_per_page +1; }
#------------------------------------------------------------------------------------------
# for ipaddress
$int_pkid=0;
if(isset($_GET["pkid"]) && trim($_GET["pkid"])!="" )
{
    $int_pkid=trim($_GET["pkid"]);
}
$str_id="";
if(isset($_GET["loginid"]) && trim($_GET["loginid"])!="" )
{
    $str_id=trim($_GET["loginid"]);
}
$str_query_filter_id="";

/*if($str_key != "") 
    {    $str_query_filter_id=" WHERE loginid like '".$rs_list->Fields("loginid")."%'"; 
}*/
//}
//print "filter:" .$str_id;
 #------------------------------------------------------------------------------------------

#Initialization of variables used for message display.    
$str_type="";
$str_message="";
$str_title="";
$str_mode="";
if(isset($_GET['tit']))
{
    $str_title=trim($_GET['tit']);
}
if(isset($_GET['mode']))
{
    $str_mode=trim($_GET['mode']);
}
#Get message type.
if(isset($_GET['type']))
{
    switch(trim($_GET['type']))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Initialization of variables used for message display.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("IE"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
        case("URL"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;
        case("IEM"): $str_message = $STR_MSG_ACTION_INVALID_EMAIL_FORMAT; break;
        case("AL"): $str_message = $STR_MSG_ACTION_LOGIN; break;
        case("AP"): $str_message = $STR_MSG_ACTION_APPROVE; break;
//        case("ISUK"): $str_message = $STR_MSG_ACTION_INVALID_SHORTENURLKEY; break;
        case("IUI"): $str_message = $STR_MSG_ACTION_INVALID_LOGINID; break;
        case("IUP"): $str_message = $STR_MSG_ACTION_INVALID_PASSWORD; break;
        case("DPID"): $str_message = $STR_MSG_ACTION_INVALID_PROFILEID; break;
    }
}	
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include("../../includes/adminheader.php"); ?>
        <div class="row padding-10">
            <div class="col-md-3 button_space">
		<div class="btn-group" role="group" aria-label="...">
                     <a href="./item_cms.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)">
                    <i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)
                </a>
		</div>
            </div>
            <div class="col-md-9" align="right" ><h3><?php print($STR_TITLE_PAGE); ?></h3></div>
	</div>
	<hr>
	<?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
	<div class="row padding-10">
    	<div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <div class="row padding-10">
                                <div class="col-md-6 col-xs-8 col-sm-6"><a class="accordion-toggle collapsed " title="<?php print $STR_HOVER_ADD; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><b><?php print($STR_TITLE_ADD); ?></b> </a></div>
                                <div class="col-md-6 col-xs-4 col-sm-6" align="right"><?php print($STR_LINK_HELP); ?></div>
                            </div> 
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <form name="frm_add" action="item_add_p.php?$str_image" method="post" onSubmit="return frm_add_validateform();" enctype="multipart/form-data">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Login ID</label><span class="text-help-form"> * (<?php print $STR_MSG_LOGINID_FORMAT; ?>)</span>
                                            <input id="txt_loginid" name="txt_loginid"  class="form-control input-sm"  placeholder="<?php print $STR_PLACEHOLDER_LOGINID; ?>" type="text"  value="<?php if(isset($_GET["loginid"])){ print(RemoveQuote(trim($_GET["loginid"])));}?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Password</label><span class="text-help-form"> * (<?php print($STR_MSG_PASSWORD_FORMAT);?>)</span>
                                            <input id="txt_password" name="txt_password" size="90" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_PASSWORD; ?>" type="text"  value="<?php if(isset($_GET["password"])){ print(RemoveQuote(trim($_GET["password"])));}?>">
                                        </div>
                                    </div>
                                </div>
                                 <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email ID</label><span class="text-help"><?php //print $STR_MSG_ACTION_INVALID_LOGINID; ?></span>
                                            <input id="txt_email" name="txt_email" size="90" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_EMAIL; ?>" type="text" value="<?php if(isset($_GET["email"])){ print(RemoveQuote(trim($_GET["email"])));}?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name</label><span class="text-help-form"> </span>
                                            <input id="txt_name" name="txt_name" size="90" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_FULLNAME; ?>" type="text" value="<?php if(isset($_GET["name"])){ print(RemoveQuote(trim($_GET["name"])));}?>">
                                        </div>
                                    </div>
                                </div>
                                <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                <input type="hidden" name="key" value="<?php print($str_key);?>">
                            </form>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
	</div>
        <div class="row">
            <div class="col-md-12" align="left">
                <div class="panel panel-default">
		<div class="panel-heading">
                    <h4 class="panel-title">
                        <strong><i class="fa fa-filter"></i>&nbsp;&nbsp;Filter Criteria</strong>
                    </h4>
		</div>
		
                <div class="panel-body">
                    <div class="col-md-12 text-center" align="center">
                        <form name="frm_filter" method="get" action="item_list.php?<?php print $str_filters; ?>">
                            <label>&nbsp;Select&nbsp;</label>
                            <label>
                                <select name="str_filter" class="form-control input-sm">
                                    <option value="0" <?php print(CheckSelected("0",$int_field));?>>ALL</option>
                                    <option value="1" <?php print(CheckSelected("1",$int_field));?>>Name</option>
                                    <option value="2" <?php print(CheckSelected("2",$int_field));?>>LoginID</option>
                                    <option value="3" <?php print(CheckSelected("3",$int_field));?>>EmailID</option>
                                </select>
                            </label>
                            <label>&nbsp;Start With&nbsp;</label>
                            <label>
                                <select name="key" class="form-control input-sm">
                                    <option value=""<?php print(CheckSelected("",$str_key));?>>All Alphabets</option>
                                    <?php 
                                        for($i=65;$i<91;$i++)
                                        {
                                          /*  if($str_key != chr($i))
                                            { $select_key=""; }
                                            else
                                            { $select_key="selected"; }
                                           */ ?> 
                                            <option value="<?php print(chr($i));?>" <?php print(CheckSelected(chr($i),$str_key));?>><?php print(chr($i));?></option>
                                  <?php } ?>        	
                                </select>			
                            </label>
                            <label>&nbsp;<?php print DisplayFormButton('VIEW', 0)?>
                            </label>
                        </form>
                    </div>
		</div>
            </div>
        </div>
    </div>
<!--    <div class="row padding-10">
    </div>-->
    <div class="table-responsive">
	<form name="frm_list" action="item_order_p.php?<?php print $str_filters; ?>" method="post" onSubmit="return frm_list_check_displayorder();">
            <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_SR_NO; ?></th>
                <th width="18%"><?php print $STR_TABLE_COLUMN_NAME_DATE; ?></th>
                <th width="18%"><?php print $STR_TABLE_COLUMN_NAME_SUBSCRIPTON_ID; ?></th>
                <th width=""><?php print $STR_TABLE_COLUMN_NAME_DETAILS; ?></th>
                <th width="16%"><?php print $STR_TABLE_COLUMN_NAME_LOGIN_DETAILS; ?></th>
                <th width="16%"><?php print $STR_TABLE_COLUMN_NAME_IP_ADDRESS_HISTORY; ?></th>
                <th width="5%"><?php print $STR_TABLE_COLUMN_NAME_ALLOW_LOGIN; ?></th>
                <th width="4%"><?php print $STR_TABLE_COLUMN_NAME_ACTION; ?></th>
            </tr>
            </thead>
                <tbody>
                    <?php if($rs_list->EOF()==true)  {  ?><tr><td colspan="7" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr><?php }
                            else { $int_cnt=1; while(!$rs_list->EOF()==true) { ?>
                            <tr>
                                <td align="center"><?php print($srno)?></td>
                                <td align="center" class="align-middle">
                                    <span class="text-help"><?php print $STR_TITLE_DATE_REGISTERED; ?><br/> </span><i class="text-help"><?php print(DDMMMYYYYHHIISSFormat($rs_list->fields("registrationdatetime"))); ?></i><hr/>
                                        
                                        <?php if($rs_list->fields("lastlogindatetime")!="1970-01-01 00:00:01" && $rs_list->fields("lastlogindatetime") != "") { ?><span class="text-help"><?php print $STR_TITLE_DATE_LAST_LOGIN; ?> <br/></span><i class="text-help"><?php print(DDMMMYYYYHHIISSFormat($rs_list->fields("lastlogindatetime")));?><?php } ?></i>
                                    </td>
                                    <td align="center" class="align-middle">
                                        <p><?php print($rs_list->Fields("subscriptionid")); ?></p>
                                    </td>
                                    <td align="left" class="align-top">
                                        <?php if(($rs_list->Fields("name") != "")){ ?><p><i class='fa fa-user'></i>&nbsp;<?php print("".$rs_list->Fields("name")); ?></p><?php } ?>
                                        <?php if(($rs_list->Fields("email") != "")){ ?><p><i class="fa fa-envelope"></i>&nbsp;<?php print($rs_list->Fields("email")); ?></p>
                                        <?php } ?>
                                    </td>
                                    <td align="center" class="align-middle">
                                        <p>
                                        <?php print("<i class='fa fa-user'></i>&nbsp;".$rs_list->fields("loginid")."</b><br/>"."&nbsp;<i class='fa fa-key'></i>&nbsp;".$rs_list->fields("password"));?>
                                        </p>
                                    </td>
                                    
                                    <td align="center" class="align-middle">
                                        <?php 
                                      /*  $str_select_query="";
                                        $str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_HISTORY." WHERE masterpkid=" .$rs_list->Fields("pkid");
                                        $rs_list_ip = GetRecordSet($str_query_select); */
                                        ?>
                                    
                                        <?php 
                                        $str_query_filter_id=" WHERE loginid like '".$rs_list->Fields("loginid")."%'"; 
                                        
                                        $str_select_query="";
                                        $str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_HISTORY. $str_query_filter_id .$STR_DB_TABLE_NAME_HISTORY_ORDER_BY;
//print $str_query_select; 
                                        $rs_list_ipaddress = GetRecordSet($str_query_select); 

                                         while(!$rs_list_ipaddress->EOF()==true) { 
 
                                        if(($rs_list->Fields("loginid")) == $rs_list_ipaddress->Fields("loginid")){
                                  ?> <?php print $rs_list_ipaddress->Fields("ipaddress"); ?>
                                 <?php /* ?>     <a class="btn btn-danger btn-xs" href="item_history_del_p.php?pkid=<?php print($rs_list_ipaddress->fields("pkid")).$str_filters; ?>" onClick="return frm_list_confirmdelete();" title="<?php print($STR_HOVER_DELETE);?>" ><?php print $STR_LINK_ICON_PATH_DELETE; ?></a> <?php */ ?>
                                    <?php    }
                                    $rs_list_ipaddress->MoveNext(); }?>
                                         
                                       
                                   <?php /* ?>     <a href="./item_history_list.php?pkid=<?php print($rs_list->fields("pkid"));?>&loginid=<?php print($rs_list->fields("loginid"));?>&PagePosition=<?php print($int_page);?>&key=<?php print($str_key);?>&catid=<?php print($int_cat_pkid);?>" <i class="fa fa-history fa-2x"></i></a> <?php */ ?>
                                        
                                    </td>
                                    <?php 
                                        $str_class="alert-success";
                                        if(trim(strtoupper($rs_list->fields("allowlogin")))=="NO")
                                        {
                                            $str_class="alert-danger";
                                        }			  
                                        ?>
                                    <td align="center" class="<?php print $str_class; ?>">
                                        <?php 
                                            $ste_allow_login="";
                                            $str_class="";
                                            $str_click_title="";
                                            if($rs_list->fields("allowlogin")=="YES") {	
                                                $ste_allow_login="YES";
                                                $str_class_allow_login="alert-success alert-link";
                                                $str_click_title="Click to not allow login"; }
                                            else {
                                                $ste_allow_login="NO";
                                                $str_class_allow_login="alert-danger alert-link";
                                                $str_click_title="Click to allow login"; }
                                        ?>
                                        <a href="item_allowlogin_p.php?pkid=<?php print($rs_list->fields("pkid")).$str_filters ;?>" title="<?php print($str_click_title);?>"  class="<?php print $str_class_allow_login; ?>"><?php print($ste_allow_login);?></a><?php //} ?>
                                        
                                    </td>
                            <?php   $str_image="";  $str_class="";  $str_title="";
                                    if(strtoupper($rs_list->fields("visible"))=="YES") 
                                    { $str_image=$STR_LINK_ICON_PATH_VISIBLE;
                                           $str_class="btn btn-warning btn-xs"; $str_title=$STR_HOVER_VISIBLE; }
                                    else { $str_image=$STR_LINK_ICON_PATH_INVISIBLE;
                                            $str_class="btn btn-default active btn-xs"; $str_title=$STR_HOVER_INVISIBLE; } ?>
                                    <td align="center" class="align-middle">
                                       
                                    <?php /* ?>    <a class="btn btn-success btn-xs" href="item_edit.php?pkid=<?php print($rs_list->fields("pkid"));?>&PagePosition=<?php print($int_page);?>&key=<?php print($str_key);?>&catid=<?php print($int_cat_pkid);?>" title="<?php print($STR_HOVER_EDIT);?>" ><i class="fa fa-pencil"></i></a>  <?php */ ?>
                                        <a class="btn btn-danger btn-xs" href="item_del_p.php?pkid=<?php print($rs_list->fields("pkid")).$str_filters ?>" onClick="return frm_list_confirmdelete();" title="<?php print($STR_HOVER_DELETE);?>" ><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>
                                    </td>
                                </tr>
                                <?php $int_cnt++; $srno++; $rs_list->MoveNext(); } ?>
                                
                                <?php if($rs_list->count()>0) {
                                    $margine=5; ?>
                                    <tr>
                                        <td colspan="8" class="text-center nopadding">
                                            <nav aria-label="Page navigation">
                                                <ul class="pagination">
                                                    <?php print(PagingWithMargine($int_total_record,$margine,$int_page,"item_list.php",$int_record_per_page,"","key=".$str_key."&catid=".$int_cat_pkid."&#ptop"));?>
                                                </ul>
                                            </nav>

                                        </td>
                                    </tr>
                                    <?php } ?>
                                <?php }  ?>
                            </tbody>
                        </table>
                    </form>
                </div>
        <script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
	<?php include "../../includes/help_for_list.php"; ?>
    
        </div>
<?php include "../../includes/include_files_admin.php"; ?>
    <?php include($STR_ADMIN_FOOTER_PATH); ?>
    
<!--<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>-->
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 100}).panelInstance('ta_desc');
		//new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
  });
		// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
		// convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_desc');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>


<script>
/*$( "#btn_Add" ).click(function() {
if($('div .nicEdit-main').text()=="" && $("input#txt_title").val()=="" ){ alert('Please enter title or description');return false;}
});
$( "#btn_reset" ).click(function() {
		location.reload();
});*/
</script>
</body>
</html>
