function frm_edit_validateform()
{
    with(document.frm_edit)
    {
    if(trim(txt_name.value) == "")
        {
            alert("Please enter name.");
            txt_name.select();
            txt_name.focus();
            return false;
        }
       
        if(trim(txt_phoneno.value) == "")
        {
            alert("Please enter Phone Number.");
            txt_phoneno.select();
            txt_phoneno.focus();
            return false;
        }
        if(trim(cbo_country.value) == "")
        {
            alert("Please Select Country.");
            cbo_country.select();
            cbo_country.focus();
            return false;
        }
   /*     if(trim(cbo_state.value) == "")
        {
                alert("Please Select State.");
                cbo_state.select();
                cbo_state.focus();			
                return false;
        } */

       
    if(txt_city.value=="")
    {
        alert("Please enter City.");
        txt_city.select();
        txt_city.focus();
        return false;					
    }

     if(trim(txt_zipcode.value) == "")
    {
        alert("Please Enter Zipcode.");
        txt_zipcode.select();
        txt_zipcode.focus();			
        return false;
    }

    if(txt_address.value=="")
    {

        alert("Please enter address.");
        txt_address.select();
        txt_address.focus();
        return false;					
    }
}
return true;
}

function confirm_delete()
{
    if(confirm("Are you sure you want to delete this image permanently.?"))
    {
        if(confirm("Confirm Deletion:Click 'OK' to delete this image or 'Cancel' to deletion."))
        {
            return true;
        }
    }
    return false;
}
