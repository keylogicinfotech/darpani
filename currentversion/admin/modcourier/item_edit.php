<?php
/*
File Name  :- item_edit.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_common.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_datetimeyear.php";
#----------------------------------------------------------------------------------------------------
#initialize variables
$int_page="";
$int_cat_pkid="";
$str_master_key="";
$str_before_img="";
$str_after_img="";

#-----------------------------------------------------------------------------------------------------------------------------------------------------
#get filter data
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{
    $int_cat_pkid=trim($_GET["catid"]);
}
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{
    $str_master_key=trim($_GET["key"]);
}
if(isset($_GET["file_before_image"]) && trim($_GET["file_before_image"])!="" )
{
    $str_before_img=trim($_GET["file_before_image"]);
}
if(isset($_GET["file_after_image"]) && trim($_GET["file_after_image"])!="" )
{
    $str_after_img=trim($_GET["file_after_image"]);
}
#get paging data
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"])==true && trim($_GET["PagePosition"])>0)
{
    $int_page= $_GET["PagePosition"];
}
else
{
    $int_page=1;
}
$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_master_key."&PagePosition=".$int_page;
#--------------------------------------------------------------------------------------------------------------------------
#get Query String Data
$int_pkid="";
if(isset($_GET["pkid"])==true)
{
    $int_pkid=trim($_GET["pkid"]);
}	
#----------------------------------------------------------------------------------------------------
# get Query String Data
$str_query_select = "SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE pkid=". $int_pkid;
//print $str_query_select;
$rs_list = GetRecordSet($str_query_select);
if($rs_list->eof())
{
    CloseConnection();
    Redirect("item_list.php?".$str_query_string."&type=E&msg=F");
    exit();
}
#------------------------------------------------------------------------------------------
# Initialization of variables used for message display.   
$str_type="";
$str_message="";
$str_title="";
$str_mode="";
if(isset($_GET['tit']))
{
    $str_title=trim($_GET['tit']);
}
if(isset($_GET['mode']))
{
    $str_mode=trim($_GET['mode']);
}
#Get message type.
if(isset($_GET['type']))
{
    switch(trim($_GET['type']))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
$str_super = "";
#Get message text.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
        case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
        case("IE"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode."."; break;
        case("URL"): $str_message = $STR_MSG_ACTION_INVALID_URL_FORMAT; break;
        case("I"): $str_message = $STR_MSG_ACTION_INVALID_EMAIL_FORMAT; break;
        case("AL"): $str_message = $STR_MSG_ACTION_LOGIN; break;
        case("IUPRO"): $str_message = $STR_MSG_ACTION_INVALID_SHORTENURLKEY; break;
        case("IUI"): $str_message = $STR_MSG_ACTION_INVALID_LOGINID; break;
        case("DPRO"): $str_message = $STR_MSG_ACTION_INVALID_PROFILEID; break;
        case("IUP"): $str_message = $STR_MSG_ACTION_INVALID_PASSWORD; break;
        case("NIA"): $str_message = $STR_MSG_ACTION_NEW_IMAGE_APPROVED; break;
        case("DP"): $str_message = $STR_MSG_ACTION_DUPLICATE_LOGINID; break;
    }
}	
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?> : <?php print($STR_TITLE_EDIT); ?> </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/font-awesome.min.css" rel="stylesheet">
<link href="../../css/admin.css" rel="stylesheet">
</head>
<body onload="<?php if($rs_list->fields("countrypkid") != 0) { ?>return get_state2(<?php print $rs_list->fields("countrypkid"); ?>);<?php } ?>">
<!--<body>-->
    <a name="ptop" id="ptop"></a>;
<div class="container">
	<?php include($STR_ADMIN_HEADER_PATH); ?>
    
	<div class="row padding-10">
            
            <div class="col-md-3 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <a href="./item_list.php?<?php print $str_filter; ?>" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE);?></a>
            </div>
        </div>
            
            <div class="col-md-9 col-sm-12 col-xs-12" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE);?> </h3>
            </div>
	</div><hr> 
	<?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
	<div class="row padding-10">
    	<div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <i class="fa fa-edit" ></i>&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="false" <?php /* ?>style="height: 0px;"<?php */ ?>>
                        <div class="panel-body">
                            <form name="frm_edit" action="item_edit_p.php" method="post" onSubmit="return frm_edit_validateform();" enctype="multipart/form-data">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Person Name</label><span class="text-help-form"> * </span>
                                            <input id="txt_name" name="txt_name"  class="form-control input-sm" placeholder="<?php print $STR_PLACEHOLDER_NAME; ?>" type="text" value="<?php print(RemoveQuote(trim($rs_list->Fields("name"))));?>">
                                        </div>
                                    </div> 
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Phone Number</label><span class="text-help-form"> * </span>
                                            <input id="txt_phoneno" name="txt_phoneno" size="90" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_PHONENUMBER; ?>" type="text" value="<?php print(RemoveQuote(trim($rs_list->Fields("phoneno"))));?>">
                                        </div>
                                    </div>
                                </div>
                                
                               <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="control-group form-group">
                                            <label>Select Country</label><span class="text-help-form"> * </span>
                                                <select name="cbo_country"  id="cbo_country" class="form-control input-sm" onChange="get_state();">
                                                    
                                                    <option  <?php print(CheckSelected("",$rs_list->fields("statepkid")));?> value="0">-- Select Country --</option>
                                                    <?php 
                                                        $str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_COUNTRY. " ORDER BY title DESC";
                                                        $rs_list_country=GetRecordSet($str_query_select);

                                                        while(!$rs_list_country->EOF()==true)
                                                        {?>
                                                            <option value="<?php print($rs_list_country->fields("pkid"))?>" <?php print(CheckSelected($rs_list_country->fields("pkid"),$rs_list->fields("countrypkid"))); ?>><?php print($rs_list_country->fields("title")); ?></option>
                                                            <?php
                                                            $rs_list_country->MoveNext();
                                                        }
                                                    ?>				
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="controls" id="before_get_state">
                                            <label>Select State</label><span class="text-help-form"> * </span>
                                            <select class='form-control input-sm' name='cbo_state' id='cbo_state'>
                                                <option value='0'>-- SELECT STATE --</option>
                                            </select><br/>
                                        </div>
                                        <div id="get_state"></div>
                                    </div>
                                </div>
                                
                                <div class="row padding-10">
                                    <div class="col-md-6">
                                        <div class="control-group form-group">
                                            <div class="controls"><label>City</label><span class="text-help-form"> * </span>
<input type="text" name="txt_city" id="txt_city" class="form-control input-sm"  placeholder="<?php print $STR_PLACEHOLDER_CITY; ?>" value="<?php print(RemoveQuote(trim($rs_list->Fields("city"))));?>" size="40" maxlength="100">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-group form-group">
                                            <div class="controls"><label>Zip / Postal Code</label><span class="text-help-form"> * </span><input type="text" name="txt_zipcode" id="txt_zipcode" class="form-control input-sm" value="<?php print(RemoveQuote(trim($rs_list->Fields("zipcode"))));?>" placeholder="<?php print $STR_PLACEHOLDER_ZIP; ?>" size="40" maxlength="100">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                 <div class="row padding-10">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Address</label><span class="text-help-form"> * </span>
                                            <input id="txt_address" name="txt_address" size="90" class="form-control input-sm" maxlength="512" placeholder="<?php print $STR_PLACEHOLDER_ADDRESS; ?>" type="text" value="<?php print(RemoveQuote(trim($rs_list->Fields("address"))));?>">
                                            <span class="text-help-form"><?php //print($STR_MAX_255_MSG);?></span>
                                        </div> 
                                    </div>
                                </div>
                                
<?php /*   ?>                            <input type="hidden" name="file_before_image" value="<?php print($str_before_img);?>">
                               <input type="hidden" name="file_after_image" value="<?php print($str_after_img);?>"><?php */   ?>
                                <input type="hidden" name="pkid" value="<?php print($int_pkid);?>">
                                <input type="hidden" name="PagePosition" value="<?php print($int_page);?>">
                                <input type="hidden" name="key" value="<?php print($str_master_key);?>">
                                <input type="hidden" name="catid" value="<?php print($int_cat_pkid);?>">
                               <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
        <script language="JavaScript" src="./item_edit.js" type="text/javascript"></script>
</div>
  
<?php // include "../../includes/include_files_admin.php"; ?>
   

<link href="../../css/admin.css" rel="stylesheet">
<script language="JavaScript" src="../../js/functions.js" type="text/javascript"></script>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>   
    <?php include($STR_ADMIN_FOOTER_PATH); ?>
    
<script type="text/javascript">
function get_state2(masterpkid) { // Call to ajax function
    var pkid = masterpkid;
    var dataString = "country_pkid="+pkid+"&state_pkid=<?php print $rs_list->Fields("statepkid"); ?>";
//    alert(dataString);
    $.ajax({
        type: "POST",
        url: "item_get_state_edit_p.php", // Name of the php files
        data: dataString,
        success: function(html)
        {
            $("#before_get_state").hide();
            $("#get_state").html(html);
        }
    });
}

function get_state() { // Call to ajax function
    var pkid = $('#cbo_country').val();
//    alert(catpkid);
    var dataString = "country_pkid="+pkid;
    $.ajax({
        type: "POST",
        url: "item_sel_state_p.php", // Name of the php files
        data: dataString,
        success: function(html)
        {
            $("#before_get_state").hide();
            $("#get_state").html(html);
        }
    });
}



</script>
<script type="text/javascript" src="../../includes/richetxteditor.js"></script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        get_state2(<?php print($rs_list->fields("countrypkid")); ?>);
        new nicEditor({fullPanel : true,maxHeight : 150}).panelInstance('ta_desc');
	new nicEditor({fullPanel : true,maxHeight : 150}).panelInstance('ta_desc2');
  });
		// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
		// convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_desc');
             new nicEditor({fullPanel : true}).panelInstance('ta_desc2');
        });
</script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
      
        new nicEditor({fullPanel : true,maxHeight : 100}).panelInstance('ta_desc');
        new nicEditor({fullPanel : true,maxHeight : 100}).panelInstance('ta_keyword_list');
        new nicEditor({fullPanel : true,maxHeight : 100}).panelInstance('ta_remark');
        new nicEditor({fullPanel : true,maxHeight : 100}).panelInstance('txt_extra');
		//new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
  });
		// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
		// convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_desc');
             new nicEditor({fullPanel : true}).panelInstance('ta_keyword_list');
             new nicEditor({fullPanel : true}).panelInstance('ta_remark');
             new nicEditor({fullPanel : true}).panelInstance('txt_extra');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>
<script>
/*$( "#btn_Add" ).click(function() {
if($('div .nicEdit-main').text()=="" && $("input#txt_title").val()=="" ){ alert('Please enter title or description');return false;}
});
$( "#btn_reset" ).click(function() {
		location.reload();
});*/
</script>
</body>
</html>
