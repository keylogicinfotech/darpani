<?php
/*
File Name  :- item_add_p.php
Create Date:- MARCH2019
Intially Create By :- 0013
Update History:
*/
#---------------------------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_system.php";
include "./item_app_specific.php"; 
#--------------------------------------------------------------------------------------------------------------------
//print_r($_POST);exit;
# filter variables
$int_page="";
$str_key="";

if(isset($_POST["key"]) && trim($_POST["key"])!="" )
{
    $str_key=trim($_POST["key"]);
}
#get paging data
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"])!="" && is_numeric($_POST["PagePosition"])==true && trim($_POST["PagePosition"])>0)
{
    $int_page= $_POST["PagePosition"];
}
else
{
    $int_page=1;
}
#----------------------------------------------------------------------------------------------------------------------
# get form data
$int_cat_id=0;

$str_name = "";
if(isset($_POST["txt_name"]))
{
    $str_name=trim($_POST["txt_name"]);
}

$str_address = "";
if(isset($_POST["txt_address"]))
{
    $str_address=trim($_POST["txt_address"]);
}
$str_country="";
if(isset($_POST["cbo_country"]))   
{
    $str_country=trim($_POST["cbo_country"]);
}
$str_state="";
if(isset($_POST["cbo_state"]))   
{
    $str_state=trim($_POST["cbo_state"]);
}
$str_city="";
if(isset($_POST["txt_city"]))   
{
    $str_city=trim($_POST["txt_city"]);
}

$int_phoneno=0;
if(isset($_POST["txt_phoneno"]))
{
    $int_phoneno=trim($_POST["txt_phoneno"]);
}
$int_zipcode = 0;
if(isset($_POST["txt_zipcode"]))
{
    $int_zipcode=trim($_POST["txt_zipcode"]);
}
#-----------------------------------------------------------------------------------------------------------------------
$str_filter  = "&PagePosition=".urlencode(RemoveQuote($int_page))."&key=".urlencode(RemoveQuote($str_key));
$querystring = $str_filter;
//----------------------------------------------------------------------------------------------------------------------
//Check Duplicate details.
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME."";

$str_max="";
$str_max=GetMaxValue($STR_DB_TABLE_NAME,"displayorder");

#----------------------------------------------------------------------------------------------------------------
$str_submitdate=date("Y-m-d H:i:s");
$str_approvedate=date("Y-m-d H:i:s");
$str_query_insert ="INSERT INTO " .$STR_DB_TABLE_NAME. " (name,address,phoneno,";
$str_query_insert.="countrypkid,statepkid,city,zipcode,registrationdatetime,visible,displayorder)";
$str_query_insert.=" VALUES('".ReplaceQuote($str_name)."',";
$str_query_insert.="'".ReplaceQuote($str_address)."','".ReplaceQuote($int_phoneno)."','".ReplaceQuote($str_country)."','".ReplaceQuote($str_state)."','".ReplaceQuote($str_city)."','".ReplaceQuote($int_zipcode)."',";
$str_query_insert.="'".$str_submitdate."','YES','".$str_max."')";
//print $str_query_insert;exit;
ExecuteQuery($str_query_insert);
#----------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();
#----------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("./item_list.php?msg=S&type=S".$str_filter); 
exit();				
#-----------------------------------------------------------------------------------------------------------------
?>
