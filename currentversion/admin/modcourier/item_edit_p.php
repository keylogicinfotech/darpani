<?php
/*
File Name  :- item_list.php
Create Date:- JAN2019
Intially Create By :- 0013
Update History:
*/
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_upload.php";
include "../../includes/lib_image.php";
include "../../includes/lib_file_system.php";
include "./item_app_specific.php";
//print_r($_FILES); exit;

#-----------------------------------------------------------------------------------------------------------------------------
# filter variables
$int_page="";
$int_cat_pkid="";
$str_mname="";

#get filter data
if(isset($_POST["catid"]) && trim($_POST["catid"])!="" )
{
    $int_cat_pkid=trim($_POST["catid"]);
}
if(isset($_POST["key"]) && trim($_POST["key"])!="" )
{
    $str_mname=trim($_POST["key"]);
}
#get paging data
if(isset($_POST["PagePosition"]) && trim($_POST["PagePosition"])!="" && is_numeric($_POST["PagePosition"])==true && trim($_POST["PagePosition"])>0)
{
    $int_page= $_POST["PagePosition"];
}
else
{
    $int_page=1;
}
$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_mname."&PagePosition=".$int_page;

$int_pkid=0;
if(isset($_POST['pkid']))
{
    $int_pkid=trim($_POST['pkid']);
}
if($int_pkid=="" || $int_pkid==0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}

#-------------------------------------------------------------------------------------------------------------------------
$str_name = "";
if(isset($_POST["txt_name"]))
{
    $str_name=trim($_POST["txt_name"]);
}

$str_address = "";
if(isset($_POST["txt_address"]))
{
    $str_address=trim($_POST["txt_address"]);
}
$str_country="";
if(isset($_POST["cbo_country"]))   
{
    $str_country=trim($_POST["cbo_country"]);
}
$str_state="";
if(isset($_POST["cbo_state"]))   
{
    $str_state=trim($_POST["cbo_state"]);
}
$str_city="";
if(isset($_POST["txt_city"]))   
{
    $str_city=trim($_POST["txt_city"]);
}

$int_phoneno=0;
if(isset($_POST["txt_phoneno"]))
{
    $int_phoneno=trim($_POST["txt_phoneno"]);
}
$int_zipcode = 0;
if(isset($_POST["txt_zipcode"]))
{
    $int_zipcode=trim($_POST["txt_zipcode"]);
}
#-----------------------------------------------------------------------------------------------------------------------# query string datda to forward
$querystring = $str_filter . "&pkid=".$int_pkid;
//PRINT $querystring; EXIT;   
#--------------------------------------------------------------------------------------------------------------------------
# check for null data
/*if($str_loginid=="" || $str_password=="" || $str_usertype=="")
{
    CloseConnection();
    Redirect("./item_edit.php?msg=F&type=E".$querystring."&#ptop"); 
    exit();
}*/

#-----------------------------------------------------------------------------------------------------------------
//Check Duplicate Seller Login Id.
$str_query_select="";
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME. " WHERE pkid!=".$int_pkid;
//print($str_query_select);
//exit();

/*$rs_check_duplicate=GetRecordset($str_query_select);
if(!$rs_check_duplicate->eof())
{
    CloseConnection();
    Redirect("item_edit.php?msg=DP&type=E".$querystring."&#ptop");
}*/

//update query
$str_query_update="UPDATE " .$STR_DB_TABLE_NAME. " SET name='".ReplaceQuote($str_name)."',";
$str_query_update.="phoneno=".ReplaceQuote($int_phoneno).",";
$str_query_update.="countrypkid=".ReplaceQuote($str_country).",";
$str_query_update.="statepkid=".ReplaceQuote($str_state).",";
$str_query_update.="city='".ReplaceQuote($str_city)."',";
$str_query_update.="zipcode=".ReplaceQuote($int_zipcode).",";
$str_query_update.="address='".ReplaceQuote($str_address)."'";
$str_query_update.=" WHERE pkid=". $int_pkid;	

//print($str_query_update); exit();
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------------
#write to xml file
WriteXml();	
#-----------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("./item_list.php?msg=U&type=S".$querystring."&#ptop"); 
exit();
#-----------------------------------------------------------------------------------------------------------------
?>
