<?php
/*
Module Name:- modmodel
File Name  :- model_del_p.php
Create Date:- 25-Apr-2012
Intially Create By :- 0014
Update History:
*/
#----------------------------------------------------------------------------------------------------
#Include Files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_file_system.php";

#----------------------------------------------------------------------------------------------------
#initialize variables
$int_page="";
$int_cat_pkid="";
$str_mname="";
#----------------------------------------------------------------------------------------------------
#get filter data
if(isset($_GET["catid"]) && trim($_GET["catid"])!="" )
{
    $int_cat_pkid=trim($_GET["catid"]);
}
if(isset($_GET["key"]) && trim($_GET["key"])!="" )
{
    $str_mname=trim($_GET["key"]);
}
#get paging data
if(isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"])==true && trim($_GET["PagePosition"])>0)
{
    $int_page= $_GET["PagePosition"];
}
else
{
    $int_page=1;
}
$str_filter="";
$str_filter="&catid=".$int_cat_pkid."&key=".$str_mname."&PagePosition=".$int_page;

#----------------------------------------------------------------------------------------------------
#	Select query to get the record from t_model table

# Get Model pkid from Querystring parameter
$int_pkid=0;
if(isset($_GET['pkid']))
{
    $int_pkid=trim($_GET['pkid']);
}
if($int_pkid=="" || $int_pkid==0 || !is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_history_list.php?msg=F&type=E".$str_filter);
    exit();
}
#----------------------------------------------------------------------------------------------------
#select query to get title from t_model table
$str_query_select="SELECT * FROM " .$STR_DB_TABLE_NAME_HISTORY. " WHERE pkid=". $int_pkid;
$rs_list=GetRecordSet($str_query_select);

if($rs_list->eof()==true)
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E".$str_filter);
    exit();
}

#----------------------------------------------------------------------------------------------------
///remove folder for this seller so model photos,Resume Photos and Sample Gallery's Photos will be deleted.
$str_dir=$UPLOAD_IMG_FILE_PATH.$int_pkid;
RemoveDirectory($str_dir);

///Delete model info 
$str_query_delete="DELETE FROM " .$STR_DB_TABLE_NAME_HISTORY. " WHERE pkid=".$int_pkid;
ExecuteQuery($str_query_delete);

#----------------------------------------------------------------------------------------------------
#Close connection and redirect page	
CloseConnection();
Redirect("item_list.php?type=S&msg=D".$str_filter);
exit();
#------------------------------------------------------------------------------------------------------------
?>