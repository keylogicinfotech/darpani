
function frm_edit_validate()
{
    with(document.frm_edit)
    {
        if(isEmpty(txt_title.value))
        {
            alert("Please enter title.");
            txt_title.focus();
            txt_title.select();
            return false;
        }
    }
}