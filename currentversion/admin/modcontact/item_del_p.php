<?php
/*
Module Name:- Manage Contact Module
File Name  :- item_del_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------------
#Get all records
if (isset($_POST["hdn_counter"]))
{
    $int_counter = $_POST["hdn_counter"];
}
if (isset($_POST["hdn_page"]))
{
    $int_page=trim($_POST["hdn_page"]);
}
$strpkid = "";
for ($i=1; $i<$int_counter; $i++)
{
    if (isset($_POST["chk_delete".$i]))
    {
        if (($_POST["chk_delete". $i])=="on")
        {
            $strpkid=$strpkid . $_POST["hdn_pkid".$i] ."','";
        }
    }
}
$strpkid = substr($strpkid,0,strrpos($strpkid,","));
$str_query_delete = "";
#delete records
$str_query_delete = "DELETE FROM" .$STR_DB_TABLE_NAME. " WHERE uniquerowid in ('".$strpkid.")";
//print $str_query_delete;exit; 
ExecuteQuery($str_query_delete);
CloseConnection();
Redirect("item_list.php?msg=D&type=S&PagePosition=".$int_page."&#ptop");
exit();

?>