<?php
/*
File Name  :- item_setnew_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_config.php";
#------------------------------------------------------------------------------------------------
$int_counter="";
$int_page="";
if (isset($_POST["hdn_counter"]))
{
    $int_counter=$_POST["hdn_counter"];
}
if (isset($_POST["hdn_page"]))
{
    $int_page=trim($_POST["hdn_page"]);
}
#To check required parameters are passed properly or not
if($int_counter <=0 || !is_numeric($int_counter))
{
    CloseConnection();
    Redirect("item_list.php?msg=F&type=E");
    exit();
}
$strpkid = "";
$strnopkid = "";
for ($i=1; $i<$int_counter; $i++)
{
    if (isset($_POST["chk_" . $i]))
    {
        if (($_POST["chk_" . $i])=="on")
        {
            $strpkid=$strpkid . $_POST["hdn_pkid".$i] . "','";
        }
    }
    else
    {
        $strnopkid=$strnopkid . $_POST["hdn_pkid".$i] . "','";
    }
}
$strpkid=substr($strpkid,0,strrpos($strpkid,","));
$strnopkid=substr($strnopkid,0,strrpos($strnopkid,","));
#------------------------------------------------------------------------------------------------
#update table
$str_query_update="";
$str_query_update="UPDATE" .$STR_DB_TABLE_NAME. " set newmail='NO' WHERE uniquerowid in ('" . $strnopkid . ")";
//print $str_query_update;exit;
ExecuteQuery($str_query_update); 

$str_query_update="";
$str_query_update="UPDATE" .$STR_DB_TABLE_NAME. " set newmail='YES' WHERE uniquerowid in ('" . $strpkid . ")";
ExecuteQuery($str_query_update);

#Close connection and redirect to page	
CloseConnection();
Redirect("item_list.php?type=S&msg=DN&PagePosition=".$int_page."&#ptop");
exit();
#------------------------------------------------------------------------------------------------------------
?>