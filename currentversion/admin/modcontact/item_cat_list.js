
function frm_list_check_displayorder()
{
    with(document.frm_list)
    {
        cnt=hdn_counter.value;
        for(i=1;i<cnt;i++)
        {
            if(isEmpty(eval("txt_displayorder_" + i).value))
            {
                alert("Please enter display order.");
                eval("txt_displayorder_" + i).focus();
                return false;
            }
            if(isNaN(eval("txt_displayorder_" + i).value))
            {
                alert("Please enter numeric value for display order.");
                eval("txt_displayorder_" + i).select();
                eval("txt_displayorder_" + i).focus();
                return false;
            }
            if(eval("txt_displayorder_" + i).value<0)
            {
                alert("Please enter positive integer for display order.");
                eval("txt_displayorder_" + i).select();
                eval("txt_displayorder_" + i).focus();
                return false;
            }
        }
    }
}

function frm_add_validate()
{
    with(document.frm_add)
    {
        if(isEmpty(txt_title.value))
        {
            alert("Please enter title.");
            txt_title.focus();
            txt_title.select();
            return false;
        }
    }
}

function confirm_delete()
{
    if(confirm("Are you sure you want to delete this details?"))
    {
        if(confirm("Confirm Deletion: Click 'OK' to delete this details or 'Cancel' to cancel deletion."))
        {
            return true;
        }
        return false;
    }
    return false;
}
