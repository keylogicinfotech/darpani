<?php
/*
File Name  :- item_order_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#-----------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_app_specific.php";
#-----------------------------------------------------------------------------
$int_counter = "";
#Get passed value and check whether all values are passed properly or not.
if (isset($_POST["hdn_counter"]))
{
    $int_counter=trim($_POST["hdn_counter"]);
}
if($int_counter == "")
{
    CloseConnection();		
    Redirect("item_cat_list.php?msg=F&type=E#ptop");
    exit();
}
# Set display orders
for($i=1;$i<$int_counter;$i++)
{
    if ($_POST["hdn_pkid" .$i] !=""  && is_numeric(trim($_POST["hdn_pkid" .$i]))==true  && trim($_POST["hdn_pkid" .$i])>0 && trim($_POST["txt_displayorder_" . $i])!= "" && is_numeric(trim($_POST["txt_displayorder_" . $i]))==true && trim($_POST["txt_displayorder_" . $i])>=0)
    {
        #update query to update display order in t_contact_subject table.
        $str_query_update = "UPDATE" .$STR_DB_TABLE_NAME_CAT. " SET displayorder=" . $_POST["txt_displayorder_" . $i] . " WHERE pkid=" . $_POST["hdn_pkid" . $i];
        ExecuteQuery($str_query_update);		
    }
}
#-----------------------------------------------------------------------------
#write xml file
WriteXML();		
#-----------------------------------------------------------------------------
CloseConnection();
Redirect("item_cat_list.php?msg=O&type=S&#ptop");
exit();
?>