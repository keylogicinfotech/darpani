<?php
$STR_TITLE_PAGE = "Contact Inquiry List";
$STR_TITLE_PAGE_CAT = "Subject List";
$INT_RECORD_PER_PAGE = 100;

global $STR_DB_TABLE_NAME;
global $STR_DB_TABLE_NAME_ORDER_BY;
$STR_DB_TABLE_NAME = " t_contact_inquiry"; 
$STR_DB_TABLE_NAME_ORDER_BY = " ORDER BY submitdatetime DESC, fullname ASC "; 

global $STR_DB_TABLE_NAME_CAT;
global $STR_DB_TABLE_NAME_CAT_ORDER_BY;
$STR_DB_TABLE_NAME_CAT = " t_contact_subject"; 
$STR_DB_TABLE_NAME_CAT_ORDER_BY = " ORDER BY displayorder DESC, title ASC "; 

global $STR_DB_TABLE_NAME_CMS;
$STR_DB_TABLE_NAME_CMS = "cms_contact";

global $XML_FILE_PATH_CAT;
global $XML_ROOT_TAG_CAT;
$XML_FILE_PATH_CAT = "../../mdm/xmlmodulefiles/contact_subject.xml";
$XML_ROOT_TAG_CAT = "ROOT_ITEM";

global $XML_FILE_PATH_CMS;
global $XML_ROOT_TAG_CMS;
$XML_FILE_PATH_CMS = "../../mdm/xmlcontentfiles/contact_cms.xml";
$XML_ROOT_TAG_CMS = "ROOT_ITEM_CMS";	
?>
