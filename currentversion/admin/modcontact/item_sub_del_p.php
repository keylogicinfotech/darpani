<?php
/*
	Module Name:- Master Modules - Manage Contact
	File Name  :- cont_sub_del_p.php
	Create Date:- 10-June-2006
	Intially Create By :- 0022
	Update History:-
*/
#-----------------------------------------------------------------------------------------------------------------------------------------------------
	include "../../includes/validatesession.php";
	include "../../includes/configuration.php";
	include "../../includes/lib_data_access.php";
	include "../../includes/lib_common.php";
	include "./item_app_specific.php";
	
	$int_pkid="";
	$str_title="";
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#	To check whether all values are passed properly or not.	
	if(isset($_GET["pkid"]))
	{
		$int_pkid=trim($_GET["pkid"]);
	}
	if($int_pkid=="")
	{
		CloseConnection();
		Redirect("item_sub_list.php?msg=F&type=E");
		exit();
	}

	if (!is_numeric($int_pkid))
	{
		CloseConnection();
		Redirect("item_sub_list.php?msg=F&type=E");
		exit();
	}
#-----------------------------------------------------------------------------------------------------------------------------------------------------
# Getting skill title to display message on cont_sub_list.php
	$str_query_select="select subjecttitle from t_contact_subject where subjectpkid=".trim($int_pkid);
//        print "1".$str_query_select;
	$rs_check=GetRecordSet($str_query_select);
	
	if($rs_check->EOF()==true)
	{
		CloseConnection();
		Redirect("item_sub_list.php?msg=F&type=E");
		exit();
	}	
	
	$str_title=$rs_check->Fields("subjecttitle");
	
	$str_select="";
	$str_select="select subjectpkid from t_contact_inquiry where subjectpkid=".trim($int_pkid);
//        print "2".$str_select;
	$rs_list=GetRecordset($str_select);
	if(!$rs_list->eof())
	{
		CloseConnection();
		Redirect("item_sub_list.php");
		exit();
	}	
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#	Delete query to delete record from t_contact_subject.
	$str_query_delete="delete from t_contact_subject where subjectpkid=" . $int_pkid;
//        print "3".$str_query_delete;exit;
	ExecuteQuery($str_query_delete);
#-----------------------------------------------------------------------------------------------------------
#write xml file
WriteXML();			

#-----------------------------------------------------------------------------------------------------------------------------------------------------
	CloseConnection();
	Redirect("item_sub_list.php?msg=D&type=S&title=".urlencode(RemoveQuote($str_title))."&#ptop");
	exit();
?>