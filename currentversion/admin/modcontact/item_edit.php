<?php
/*
Module Name:- Master Modules - Manage Contact
File Name  :- item_edit.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------------------------------------------------------------------
#Get values of all passed GET / POST variables

$int_pkid = "";
if(isset($_GET["pkid"])) { $int_pkid=trim($_GET["pkid"]); }
if($int_pkid=="")
{
    CloseConnection();
    Redirect("item_sub_list.php?msg=F&type=E");
    exit();
}
if (!is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_sub_list.php?msg=F&type=E");
    exit();
}
#------------------------------------------------------------------------------------------------------------------------------------------
# Select Query
$str_query_select = "";
$str_query_select = "SELECT * FROM" .$STR_DB_TABLE_NAME_CAT. " WHERE pkid=" . $int_pkid;
$rs_list = GetRecordset($str_query_select);
if ($rs_list->count()==0)
{
    CloseConnection();
    Redirect("item_sub_list.php?msg=F&type=E&#ptop");
    exit();
}	
#------------------------------------------------------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_msg = "";
#get type
if(isset($_GET["type"]))
{
    switch(trim($_GET["type"]))
    {
        case("S"): $str_type = "S"; break; case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#get message
if(isset($_GET["msg"]))
{
    switch(trim($_GET["msg"]))
    {
        case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("U"): $str_message = $STR_MSG_ACTION_UPDATE; break;
        case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_md; break;
        case("IUP"): $str_message = $STR_MSG_ACTION_INVALID_PASSWORD . $str_mode; break;

    }
}

$str_skill_title="";
if(isset($_GET["title"]))
{
    $str_skill_title=RemoveQuote(MyHtmlEncode(MakeStringShort(trim($_GET["title"]),60)));
}

if(isset($_GET["msg"]))
{
    switch (trim($_GET["msg"]))
    {
        case ("F"): $str_msg= $STR_MSG_ACTION_INFO_MISSING; break;
        case ("M"): $str_msg= $STR_MSG_ACTION_DUPLICATE_TITLE; break;				
    }
}		
#------------------------------------------------------------------------------------------------------------------------------------------
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE_CAT);?> : <?php print($STR_TITLE_EDIT); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body> <a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-3 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?><a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="fa fa-chevron-left "></i>&nbsp;Back</a><?php */?>
                <a href="./item_cat_list.php" class="btn btn-default" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE_CAT);?>" ><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_CAT);?></a>
            </div>
        </div>
        <div class="col-md-9 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_EDIT); ?> : <?php print($STR_TITLE_PAGE_CAT);?></h3></div>
	</div>
        <hr> 
  <?php if($str_type != "" && $str_message != "")	{ print(DisplayMessage(0,$str_message,$str_type)); }	?>
            <div class="row padding-10">
                <div class="col-md-12">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <i class="fa  fa-edit "></i>&nbsp;<b><?php print($STR_TITLE_EDIT); ?></b> </a>
                                    <?php print($STR_LINK_HELP); ?>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse">
                        	<div class="panel-body">
                                    <form name="frm_edit" action="item_edit_p.php" method="post" onSubmit="return frm_edit_validate();">
                                        <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                        <div class="form-group">
                                            <label>Title</label><span class="text-help-form" > *</span>
                                            <input name="txt_title" type="text" class="form-control input-sm" id="txt_title" maxlength="255" value="<?php print(RemoveQuote($rs_list->fields("title"))); ?>" placeholder="<?php print ($STR_PLACEHOLDER_TITLE); ?>">
                                        </div>
                                        <?php print DisplayFormButton("EDIT",0); ?><?php print DisplayFormButton("RESET",0); ?>
                                        <input type="hidden" name="hdn_pkid" value="<?php print($int_pkid)?>">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
		</div>
            </div>
            <a name="help"></a>
	<?php include "../../includes/help_for_edit.php"; ?>
</div>
<?php include "../../includes/include_files_admin.php"; ?>
<script language="JavaScript" src="./item_edit.js" type="text/javascript"></script>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>