function displaynew_click()
{
    with(document.frm_list)
    {
        action="item_setnew_p.php";
        method="post";
        submit();
    }
}
function frm_list_check_all_new()
{
    var counter,i;
    with(document.frm_list)
    {
        counter=hdn_counter.value;
        if(chk_new_all.checked==true)
        {
            for(i=1;i<counter;i++)
            {
                eval("chk_" + i).checked=true;
            }
        }
        else
        {
            for (i=1;i<counter;i++)
            {
                eval("chk_" + i).checked=false;
            }
        }
    }
}


function frm_list_check_all_del()
{
    var counter,i;
    with(document.frm_list)
    {
        counter=hdn_counter.value;
        if(chk_del_all.checked==true)
        {
            for(i=1;i<counter;i++)
            {
                eval("chk_delete" + i).checked=true;
            }
        }
        else
        {
            for (i=1;i<counter;i++)
            {
                eval("chk_delete" + i).checked=false;
            }
        }
    }
}

function frm_list_check_new(chk)
{
    with(document.frm_list)
    {
        if (chk.checked==false)
        {
            chk_new_all.checked=false;
        }
        else
        {
            var i,count,flag;
            flag=0;
            count=hdn_counter.value;
            for(i=1;i<count;i++)
            {
                if (eval("chk_" + i).checked==true)
                {
                    flag++;
                }
            }
            if (flag==count-1)
            {
                chk_new_all.checked=true;
            }
        }
    }	
}

function frm_list_check_del(chk)
{
    with(document.frm_list)
    {
        if (chk.checked==false)
        {
            chk_del_all.checked=false;
        }
        else
        {
            var i,count,flag;
            flag=0;
            count=hdn_counter.value;
            for(i=1;i<count;i++)
            {
                if (eval("chk_delete" + i).checked==true)
                {
                    flag++;
                }
            }
            if (flag==count-1)
            {
                chk_del_all.checked=true;
            }
        }
    }	
}
function confirm_delete()
{
    var counter,i,cnt;
    with(document.frm_list)
    {
        counter=hdn_counter.value;
        cnt=0;
        for (i=1;i<counter;i++)
        {
            if (eval("chk_delete" + i).checked==true)
            {
                cnt++;
            }
        }
        if (cnt==0)
        {
            alert("Please select detail(s) which you want to delete.");
            return false;
        }
        else
        {
            if(confirm("Are you sure you want to delete this detail(s)?"))
            {
                if(confirm("Confirm Deletion: Click 'Ok' to delete this detail(s) or click 'Cancel' to cancel deletion."))
                {
                    return true;
                }
            }
            return false;
        }
    }
    return false;
}