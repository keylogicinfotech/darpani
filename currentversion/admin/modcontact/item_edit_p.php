<?php
/*
File Name  :- item_edit_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_app_specific.php";	
#------------------------------------------------------------------------------------------------------------
#Check whether all the values are passed properly or not.
$str_title="";
$str_visible="";
$int_pkid="";
#------------------------------------------------------------------------------------------------------------
if(isset($_POST["hdn_pkid"]))
{
    $int_pkid=trim($_POST["hdn_pkid"]);
}
if ($int_pkid == "")
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E&#ptop");
    exit();
}
if(isset($_POST["txt_title"]))
{
    $str_title = ReplaceQuote(trim($_POST["txt_title"]));
}
if(isset($_POST["cbo_visible"]))
{
    $str_visible = ReplaceQuote(trim($_POST["cbo_visible"]));
}
#-------------------------------------------------------------------------------------------------------------------
#check validation	
if ($str_title == "")
{
        CloseConnection();
        Redirect("item_edit.php?pkid=".$int_pkid."&msg=F&type=E&#ptop");
        exit();
}
#select noofrecord from table
$str_query_select = "SELECT count(*) noofrecords FROM" .$STR_DB_TABLE_NAME_CAT. " WHERE title='" . $str_title . "' and pkid !=" . $int_pkid;
$rs_list = GetRecordset($str_query_select);
if ($rs_list->fields("noofrecords") > 0)
{
    CloseConnection();
    Redirect("item_edit.php?pkid=".$int_pkid."&msg=M&type=E&title=". urlencode(RemoveQuote($str_title)) ."&#ptop");
    exit();
}
#update query to update record in table
$str_query_update = "UPDATE" .$STR_DB_TABLE_NAME_CAT. " SET title='" . $str_title . "',";
$str_query_update.="visible='".$str_visible."' where pkid=" . $int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------
#write xml file
WriteXML();		
#------------------------------------------------------------------------------------------------------------
#Close connection and redirect on model_skill_list.php
CloseConnection();
Redirect("item_cat_list.php?msg=U&type=S&title=".urlencode(RemoveQuote($str_title))."&#ptop");
exit();	
?>