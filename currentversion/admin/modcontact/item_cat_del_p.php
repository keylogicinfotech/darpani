<?php
/*
File Name  :- item_cat_del_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
 */
#-----------------------------------------------------------------------------
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_app_specific.php";
#------------------------------------------------------------------------------
$int_pkid="";
$str_title="";
#To check whether all values are passed properly or not.	
if(isset($_GET["pkid"]))
{
    $int_pkid=trim($_GET["pkid"]);
}
if($int_pkid=="")
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E");
    exit();
}
if (!is_numeric($int_pkid))
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E");
    exit();
}
#-----------------------------------------------------------------------------
#Get subject title from table
$str_query_select="SELECT title FROM" .$STR_DB_TABLE_NAME_CAT. " WHERE pkid=".trim($int_pkid);
//print "1".$str_query_select;
$rs_list=GetRecordSet($str_query_select);
if($rs_list->EOF()==true)
{
        CloseConnection();
        Redirect("item_cat_list.php?msg=F&type=E");
        exit();
}	
$str_title=$rs_list->Fields("title");
$str_select="";
$str_select="SELECT pkid FROM" .$STR_DB_TABLE_NAME. " WHERE pkid=".trim($int_pkid);
//print "2".$str_select;
$rs_list=GetRecordset($str_select);
if(!$rs_list->eof())
{
    CloseConnection();
    Redirect("item_cat_list.php");
    exit();
}	
#-----------------------------------------------------------------------------------------------------------------------------------------------------
#Delete query to delete record from t_contact_subject.
$str_query_delete="DELETE FROM" .$STR_DB_TABLE_NAME_CAT. " WHERE pkid=" . $int_pkid;
//print "3".$str_query_delete;exit;
ExecuteQuery($str_query_delete);
#-----------------------------------------------------------------------------------------------------------
#write xml file
WriteXML();			
#-----------------------------------------------------------------------------------------------------------------------------------------------------
CloseConnection();
Redirect("item_cat_list.php?msg=D&type=S&title=".urlencode(RemoveQuote($str_title))."&#ptop");
exit();
?>