<?php
/*
File Name  :- item_cat_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------
$str_tit="";
if(isset($_GET["tit"]))	{ $str_tit=RemoveQuote(MyHtmlEncode(MakeStringShort(trim($_GET["tit"]),30))); }
/*$str_query_select="SELECT DISTINCT(a.pkid),a.title,a.visible,a.displayorder,b.catpkid AS inquirypkid FROM t_contact_subject a";
$str_query_select.=" LEFT JOIN t_contact_inquiry b ON a.pkid = b.catpkid";
$str_query_select.=" ORDER  BY displayorder ASC, title";
$rs_list=GetRecordset($str_query_select);  */
$str_query_select = "";
$str_query_select = "SELECT DISTINCT(a.pkid),a.title,a.visible,a.displayorder,b.catpkid AS inquirypkid FROM " .$STR_DB_TABLE_NAME_CAT. " a";
$str_query_select.= " LEFT JOIN" .$STR_DB_TABLE_NAME. " b ON a.pkid = b.catpkid";
$str_query_select.= $STR_DB_TABLE_NAME_CAT_ORDER_BY ;
$rs_list = GetRecordset($str_query_select); 
#------------------------------------------------------------------------------------------
$str_visible = "";
if(isset($_GET["visible"])) { $str_visible = trim($_GET["visible"]); }
#Initialization of variables used for message display.   
    $str_type = "";
    $str_message = "";
    $str_title = "";
    $str_mode = "";
    if(isset($_GET['tit'])) { $str_title = trim($_GET['tit']); }
    if(isset($_GET['mode'])) { $str_mode = trim($_GET['mode']); }
#Get message type.
    if(isset($_GET['type']))
    {
        switch(trim($_GET['type']))
        {
            case("S"): $str_type = "S"; break; case("E"): $str_type = "E"; break;
            case("W"): $str_type = "W"; break;
        }
    }
#Get message text.
    if(isset($_GET['msg']))
    {
        switch(trim($_GET['msg']))
        {
            case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
            case("A"): $str_message = $STR_MSG_ACTION_ADD; break;
            case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
            case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
            case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
            case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
            case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode; break;
            case("Z"): $str_message = $STR_MSG_ACTION_INVALID_ZIP_EXT;break;
            case("M"): $str_message = "Entered Subject '" . RemoveQuote(MyHtmlEncode(MakeStringShort($str_title,30))) . "' already exists. Please try with another subject."; break;
        }
    }	
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE_CAT);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body> 
<a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-6 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?>
                <a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="fa fa-chevron-left "></i>&nbsp;Back</a>
                <?php */?>
                <a href="./item_list.php" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?>" class="btn btn-default"><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?></a>
                <a href="item_cms.php" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)" class="btn btn-default"><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)</a>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE_CAT);?> </h3></div>
    </div>
    <hr> 
    <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
    <div class="row padding-10">
        <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <span>
                                <b><a class="accordion-toggle collapsed link" title="<?php print($STR_TITLE_ADD); ?>" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><?php print($STR_TITLE_ADD); ?> </a></b>
                            </span>
                            <?php print($STR_LINK_HELP); ?>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
                    	<div class="panel-body">
                            <form name="frm_add" action="item_add_p.php" method="post" onSubmit="return frm_add_validate();">
                                <div class="text-help-form" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="row padding-10">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Title</label><span class="text-help-form"> *</span>
                                                <input name="txt_title" type="text" class="form-control input-sm" id="txt_title" maxlength="255" value="<?php if(isset($_GET["tit"])) print(RemoveQuote(MyHtmlEncode($_GET["title"])))?>" placeholder="<?php print ($STR_PLACEHOLDER_TITLE); ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Visible</label><span class="text-help-form"> *</span>
                                                <?php /*?><select name="cbo_visible" class="form-control input-sm" >
                                                <option value="YES">YES</option><option value="NO">NO</option></select><?php */?>
                                                <span class="text-help-form"> (<?php print($STR_MSG_VISIBLE);?>)</span>
                                                <select name="cbo_visible" class="form-control input-sm"><option value="YES" <?php print(CheckSelected("YES",$str_visible));?>>YES</option><option value="NO" <?php print(CheckSelected("NO",$str_visible)); ?>>NO</option></select>
                                            </div>
                                        </div>
                                    </div>
                                <?php print DisplayFormButton("ADD",0); ?><?php print DisplayFormButton("RESET",0); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <form name="frm_list" action="item_order_p.php" method="post" onSubmit="return frm_list_check_displayorder();">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th width="4%">Sr. #</th>
                    <th width="">Details</th>
                    <th width="4%">Display Order</th>
                    <th width="9%">Action</th>
                </tr>
                </thead>
                <tbody>
              <?php if($rs_list->EOF()==true)  {  ?><tr><td colspan="4" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
              <?php } else { $int_cnt=1;
                        while(!$rs_list->EOF()==true) { ?>
                <tr>
                    <td align="center"><?php print($int_cnt); ?></td>
                    <td><h4><b><?php print(RemoveQuote($rs_list->Fields("title"))) ?></b></h4></td>
                    <td align="center">
                            <input type="text" class="form-control input-sm text-center" maxlength="4" value="<?php print($rs_list->fields("displayorder"))?>" name="txt_displayorder_<?php print($int_cnt)?>"><input type="hidden" name="hdn_pkid<?php print($int_cnt)?>" value="<?php print($rs_list->fields("pkid"))?>">
                    </td>
                    <?php $str_image="";
                    if(strtoupper($rs_list->fields("visible"))=="YES")
                        {   $str_image=$STR_LINK_ICON_PATH_VISIBLE;
                            $str_class="btn btn-warning btn-xs";
                            $str_title=$STR_HOVER_VISIBLE;
                        }
                        else
                        {   $str_image=$STR_LINK_ICON_PATH_INVISIBLE;
                            $str_class="btn btn-default active btn-xs";
                            $str_title=$STR_HOVER_INVISIBLE;
                        }
                    ?>
                    <td align="center">
                        <a class="<?php print($str_class); ?>" href="item_visible_p.php?pkid=<?php print($rs_list->fields("pkid"))?>" title="<?php print($str_title); ?>"><?php print($str_image);?></a>
                        <a class="btn btn-success btn-xs" href="item_edit.php?pkid=<?php print($rs_list->fields("pkid"))?>" title="<?php print($STR_HOVER_EDIT)?>"><?php print $STR_LINK_ICON_PATH_EDIT; ?></a>
                     <?php if($rs_list->fields("pkid")!=$rs_list->fields("inquirypkid")){?>
                                <a class="btn btn-danger btn-xs" href="item_cat_del_p.php?pkid=<?php print($rs_list->fields("pkid"))?>" onClick="return confirm_delete(<?php // print($flag) ?>);" title="<?php print($STR_HOVER_DELETE)?>"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>	
                      <?php }  else { ?>
                                <a class="btn btn-default active btn-xs disabled"><?php print $STR_LINK_ICON_PATH_DELETE; ?></a>
                               <?php } ?>
                    </td>
                </tr>
		<?php $int_cnt++; $rs_list->MoveNext(); } ?>
		<tr>
                    <td colspan="2"></td>
                    <td align="center"><input type="hidden" name="hdn_counter" value="<?php print($int_cnt)?>"><?php print DisplayFormButton("SAVE",0); ?></td>
		</tr>
            <?php }  ?>
            </tbody>
            </table>
	</form>
    </div>
	<?php include "../../includes/help_for_list.php"; ?>
</div>
<?php include "../../includes/include_files_admin.php"; ?>
<script language="JavaScript" src="./item_cat_list.js" type="text/javascript"></script>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>
