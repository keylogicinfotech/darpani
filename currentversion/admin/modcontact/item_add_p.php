<?php
/*
File Name  :- item_cat_add_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_app_specific.php";	
#------------------------------------------------------------------------------
#Check whether all the values are passed properly or not.
$str_title="";
$str_visible="";
if(isset($_POST["txt_title"]))
{
    $str_title = trim($_POST["txt_title"]);
}
if ($str_title == "")
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E&#ptop");
    exit();
}
if(isset($_POST['cbo_visible']))
{
    $str_visible=trim($_POST['cbo_visible']);
}
#-----------------------------------------------------------------------------
#get noofrecords from table
$str_query_select = "SELECT count(*) noofrecords FROM" .$STR_DB_TABLE_NAME_CAT. " WHERE title='" . ReplaceQuote($str_title) . "'";
$rs_list = GetRecordset($str_query_select);
if ($rs_list->fields("noofrecords") > 0)
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=M&type=E&tit=".urlencode(RemoveQuote($str_title))."&title=".urlencode(RemoveQuote($str_title))."&#ptop");
    exit();
}
#------------------------------------------------------------------------------
#get max value of displayorder
$int_max_do=GetMaxValue($STR_DB_TABLE_NAME_CAT,"displayorder");
# insert query to store record into t_contact_subject.
$str_query_insert = "INSERT INTO" .$STR_DB_TABLE_NAME_CAT. " (title,visible,displayorder) values('" . ReplaceQuote($str_title) ."','".ReplaceQuote($str_visible)."',".$int_max_do.")";
ExecuteQuery($str_query_insert);
#------------------------------------------------------------------------------
#write xml file
WriteXML();			
    CloseConnection();
    Redirect("item_cat_list.php?msg=A&type=S&title=".urlencode(RemoveQuote($str_title))."&#ptop");
    exit();	
?>