<?php
/*
File Name  :- item_visible_p.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#--------------------------------------------------------------------------------------------------------------------------------------------------------
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "./item_app_specific.php";
#---------------------------------------------------------------------------------------------------------
#Get passed value and check whether all values are passed properly or not.			
$int_pkid = "";
$str_mode = "";

if(isset($_GET["pkid"]))
{
    $int_pkid = trim($_GET["pkid"]);
}
if(!is_numeric($int_pkid) || $int_pkid=="" || $int_pkid<0 )
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E&#potp");
    exit();
}
#---------------------------------------------------------------------------------------------------------
# select title from table
$str_query_select="SELECT title,visible FROM" .$STR_DB_TABLE_NAME_CAT. " WHERE pkid=".trim($int_pkid);
$rs_list=GetRecordSet($str_query_select);
if($rs_list->EOF()==true)
{
    CloseConnection();
    Redirect("item_cat_list.php?msg=F&type=E");
    exit();
}	

$str_title = $rs_list->Fields("title");
$str_mode = $rs_list->Fields("visible");
#---------------------------------------------------------------------------------------------------------
#Set the value for variable to be stored in database.
if(strtoupper($str_mode)=="YES")
{
    $str_set_text="NO";
    $str_v_text="Invisible";
}
else
{
    $str_set_text="YES";
    $str_v_text="Visible";
}
#---------------------------------------------------------------------------------------------------------
#Update query to update t_eyecolor.	
$str_query_update="UPDATE" .$STR_DB_TABLE_NAME_CAT. " SET visible='" . $str_set_text . "' WHERE pkid=" . $int_pkid;
ExecuteQuery($str_query_update);
#-----------------------------------------------------------------------------------------------------------
#write xml file
WriteXML();	
#---------------------------------------------------------------------------------------------------------
#Close connection and redirect to list page
CloseConnection();
Redirect("item_cat_list.php?msg=V&type=S&title=".urlencode(RemoveQuote($str_title))."&mode=".urlencode(RemoveQuote($str_v_text))."&#ptop");
exit();
?>