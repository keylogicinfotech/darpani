<?php
/*
File Name  :- item_sub_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
#------------------------------------------------------------------------------
$str_tit="";
if(isset($_GET["tit"]))	{ $str_tit=RemoveQuote(MyHtmlEncode(MakeStringShort(trim($_GET["tit"]),30))); }
#select query to get records of skill title form t_contact_subject.	
/* $str_query_select = "select * from t_contact_subject order by displayorder desc,subjecttitle";
$rs_list = GetRecordset($str_query_select); */
$str_query_select="SELECT DISTINCT(a.subjectpkid),a.subjecttitle,a.visible,a.displayorder,b.subjectpkid AS inquirypkid FROM t_contact_subject a";
$str_query_select.=" LEFT JOIN t_contact_inquiry b ON a.subjectpkid = b.subjectpkid";
$str_query_select.=" ORDER  BY displayorder ASC , subjecttitle";
$rs_list=GetRecordset($str_query_select);  
#------------------------------------------------------------------------------------------
$str_visible="";
if(isset($_GET["visible"])) { $str_visible=trim($_GET["visible"]); }
#Initialization of variables used for message display.   
    $str_type="";
    $str_message="";
    $str_title="";
    $str_mode="";
    if(isset($_GET['tit'])) { $str_title=trim($_GET['tit']); }
    if(isset($_GET['mode'])) { $str_mode=trim($_GET['mode']); }
#Get message type.
    if(isset($_GET['type']))
    {
        switch(trim($_GET['type']))
        {
            case("S"): $str_type = "S"; break; case("E"): $str_type = "E"; break;
            case("W"): $str_type = "W"; break;
        }
    }
#Get message text.
    if(isset($_GET['msg']))
    {
        switch(trim($_GET['msg']))
        {
            case("F"): $str_message = $STR_MSG_ACTION_INFO_MISSING; break;
            case("A"): $str_message = $STR_MSG_ACTION_ADD; break;
            case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
            case("U"): $str_message = $STR_MSG_ACTION_EDIT; break;
            case("O"): $str_message = $STR_MSG_ACTION_DISPLAY_ORDER; break;
            case("I"): $str_message = $STR_MSG_ACTION_INVALID_PHOTO_EXT; break;
            case("V"): $str_message = $STR_MSG_ACTION_VISIBLE_INVISIBLE . $str_mode; break;
            case("Z"): $str_message = $STR_MSG_ACTION_INVALID_ZIP_EXT;break;
            case("M"): $str_message = $STR_MSG_ACTION_DUPLICATE_TITLE; break;
        }
    }	
?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_CONTACT_SUBJECT);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body> 
<a name="ptop" id="ptop"></a>
<div class="container">
    <?php include("../../includes/adminheader.php"); ?>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <?php /*?>
                <a href="<?php print($_SERVER['HTTP_REFERER']); ?>" class="btn btn-default" title="Click here to go back" ><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;Back</a>
                <?php */?>
                <a href="./item_list.php" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_CONTACT); ?>" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left "></i>&nbsp;<?php print($STR_TITLE_CONTACT); ?></a>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_CONTACT_SUBJECT);?> </h3></div>
    </div>
    <hr> 
    <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <div class="row">
                                <div class="col-md-12  padding-10">
                                    <div class="col-md-6 col-sm-6 col-xs-8"><a class="accordion-toggle collapsed link" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;<?php print($STR_TITLE_ADD); ?> </a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-4" align="right"><?php print($STR_LINK_HELP); ?></div>												
                                </div>
                            </div>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    	<div class="panel-body">
                            <form name="frm_add" action="item_add_p.php" method="post" onSubmit="return frm_add_validation();">
                                <div class="HelpText" align="right"><?php print($STR_MSG_MANDATORY); ?></div>
                                    <div class="form-group">
                                        <label>Subject Title</label><span class="HelpText"> *</span>
                                             <input name="txt_title" type="text" class="form-control input-sm" id="txt_title" maxlength="255" tabindex="1" value="<?php if(isset($_GET["tit"])) print(RemoveQuote(MyHtmlEncode($_GET["title"])))?>" placeholder="Enter contact subject here">
                                    </div>
                                    <div class="form-group">
                                        <label>Visible</label><span class="HelpText"> *</span>
                                        <?php /*?><select name="cbo_visible" class="form-control input-sm" tabindex="2">
                                        <option value="YES">YES</option><option value="NO">NO</option></select><?php */?>
                                        <span class="HelpText"> (<?php print($STR_MSG_VISIBLE);?>)</span>
                                        <select tabindex="2" name="cbo_visible" class="form-control input-sm"><option value="YES" <?php print(CheckSelected("YES",$str_visible));?>>YES</option><option value="NO" <?php print(CheckSelected("NO",$str_visible)); ?>>NO</option></select>
                                    </div>
                                        <button type="submit" tabindex="2" class="btn btn-success" title="<?php print($STR_TITLE_HOVER_ADD);?>"  onClick="return frmAdd_Validate(2)" ><span class="glyphicon glyphicon-plus-sign" aria-hidden="true" ></span> Add</button>&nbsp;
                                        <input tabindex="3" name="btn_reset" id="btn_reset" title="<?php print($STR_TITLE_HOVER_RESET);?>" value="Reset" class="btn btn-danger" type="reset">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <form name="frm_list" action="item_order_p.php" method="post" onSubmit="return frm_list_check_displayorder();">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th width="4%">Sr. #</th>
                    <th width="">Details</th>
                    <th width="4%">Display Order</th>
                    <th width="10%">Action</th>
                </tr>
                </thead>
                <tbody>
              <?php if($rs_list->EOF()==true)  {  ?><tr><td colspan="4" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
              <?php } else { $int_cnt=1;
                        while(!$rs_list->EOF()==true) { ?>
                <tr>
                    <td align="center"><?php print($int_cnt); ?></td>
                    <td><?php print(RemoveQuote($rs_list->Fields("subjecttitle"))) ?></td>
                    <td align="center">
                            <input type="text" size="6" class="form-control input-sm" style="text-align:center" maxlength="4" value="<?php print($rs_list->fields("displayorder"))?>" name="txt_displayorder_<?php print($int_cnt)?>"><input type="hidden" name="hdn_pkid<?php print($int_cnt)?>" value="<?php print($rs_list->fields("subjectpkid"))?>">
                    </td>
                    <?php $str_image="";
                    if(strtoupper($rs_list->fields("visible"))=="YES")
                        {   $str_image="<i class='glyphicon glyphicon-eye-open'></i>";
                            $str_class="btn btn-warning btn-xs";
                            $str_title=$STR_TITLE_HOVER_VISIBLE;
                        }
                        else
                        {   $str_image="<i class='glyphicon glyphicon-eye-close'></i>";
                            $str_class="btn btn-default active btn-xs";
                            $str_title=$STR_TITLE_HOVER_INVISIBLE;
                        }
                    ?>
                    <td align="center">
                        <a class="<?php print($str_class); ?>" href="item_visible_p.php?pkid=<?php print($rs_list->fields("subjectpkid"))?>" title="<?php print($str_title); ?>"><?php print($str_image);?></a>
                        <a class="btn btn-success btn-xs" href="item_edit.php?pkid=<?php print($rs_list->fields("subjectpkid"))?>" title="<?php print($STR_TITLE_HOVER_EDIT)?>"><i class="glyphicon glyphicon-pencil"></i></a>
                     <?php if($rs_list->fields("subjectpkid")!=$rs_list->fields("inquirypkid")){?>
                                <a class="btn btn-danger btn-xs" href="item_sub_del_p.php?pkid=<?php print($rs_list->fields("subjectpkid"))?>" onClick="return delete_confirm(<?php // print($flag) ?>);" title="<?php print($STR_TITLE_HOVER_DELETE)?>"><i class="glyphicon glyphicon-trash"></i></a>	
                      <?php }  else { ?>
                                <a class="btn btn-default active btn-xs disabled"><i class='glyphicon glyphicon-trash disabled'></i></a>
                               <?php } ?>
                    </td>
                </tr>
		<?php $int_cnt++; $rs_list->MoveNext(); } ?>
		<tr>
                    <td colspan="2"></td>
                    <td align="center"><input type="hidden" name="hdn_counter" value="<?php print($int_cnt)?>"><button name="btn_submit_save" type="submit" title="<?php print($STR_TITLE_HOVER_DISPLAY_ORDER)?>" class="btn btn-success">Save</button></td>
		</tr>
            <?php }  ?>
            </tbody>
            </table>
	</form>
    </div>
	<?php include "../../includes/help_for_list.php"; ?>
</div>
<?php include "../../includes/include_files_admin.php"; ?>
<script language="JavaScript" src="./item_sub_list.js" type="text/javascript"></script>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>