<?php
/*
File Name  :- item_list.php
Create Date:- JAN-2019
Intially Create By :- 0013
Update History:-
*/
#------------------------------------------------------------------------------
#Include files
include "../../includes/validatesession.php";
include "../../includes/configuration.php";
include "./item_config.php";
include "../../includes/lib_data_access.php";
include "../../includes/lib_common.php";
include "../../includes/lib_datetimeyear.php";
#------------------------------------------------------------------------------
//print "error";exit;
$int_page=1;
if (isset($_GET["PagePosition"]) && trim($_GET["PagePosition"])!="" && is_numeric($_GET["PagePosition"]))
	{ $int_page = trim($_GET["PagePosition"]);	}
        
# Select Query.	
$str_query_select = "SELECT count(*) total_record FROM" .$STR_DB_TABLE_NAME. "" .$STR_DB_TABLE_NAME_ORDER_BY. "";
$rs_list_count = GetRecordset($str_query_select);
//print "first".$rs_list_count->fields("total_record");exit;
$int_total_record = $rs_list_count->fields("total_record");
#------------------------------------------------------------------------------------------------
#setting paging parameter

$int_record_per_page=$INT_RECORD_PER_PAGE;				
$int_total_page=ceil($int_total_record / $int_record_per_page); 
if ($int_page > $int_total_page)
{ $int_page=$int_total_page; }
if($int_page < 1)
{ $int_page=1; }
$int_limit_start=($int_page -1)* $int_record_per_page;	

# Currently Category is not used so this query is used - 08JUL2014
$str_query_select="SELECT a.*,b.title FROM" .$STR_DB_TABLE_NAME. " a LEFT JOIN" .$STR_DB_TABLE_NAME_CAT. " b ON a.catpkid=b.pkid";
$str_query_select .=" ORDER BY submitdatetime DESC,fullname LIMIT ".$int_limit_start.",".$int_record_per_page;
$rs_list=GetRecordset($str_query_select);

if($rs_list->Count()>0)
{ $srno=($int_page -1)* $int_record_per_page +1; }

#---------------------------------------------------------------------------------------
$str_query_select="SELECT a.*,b.title FROM" .$STR_DB_TABLE_NAME. " a LEFT JOIN" .$STR_DB_TABLE_NAME_CAT. " b ON a.catpkid=b.pkid";
$str_query_select .=" ORDER BY submitdatetime DESC,fullname LIMIT ".$int_limit_start.",".$int_record_per_page;
$rs_list=GetRecordset($str_query_select);
//print "second".$rs_list->fields("title");exit;
if($rs_list->Count()>0)
{	
    $int_srno=(($int_page-1)* $int_record_per_page + 1);
}	
#------------------------------------------------------------------------------------------
#Initialization of variables used for message display.   
$str_type = "";
$str_message = "";
# Get message type.
if(isset($_GET['type']))
{
    switch(trim($_GET['type']))
    {
        case("S"): $str_type = "S"; break;
        case("E"): $str_type = "E"; break;
        case("W"): $str_type = "W"; break;
    }
}
#Get message text.
if(isset($_GET['msg']))
{
    switch(trim($_GET['msg']))
    {
        case("S"): $str_message = $STR_MSG_ACTION_ADD; break;
        case("D"): $str_message = $STR_MSG_ACTION_DELETE; break;
        case("DN"); $str_message = $STR_MSG_DISPLAY_AS_NEW; break;					
    }
} ?>
<!DOCTYPE html>
<html>
<head>
<title><?php print($STR_SITE_TITLE);?> : <?php print($STR_TITLE_PAGE);?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body> 
<a name="ptop" id="ptop"></a>
<div class="container">
    <?php include($STR_ADMIN_HEADER_PATH); ?>
    <div class="row padding-10">
        <div class="col-md-6 col-sm-6 col-xs-12 button_space">
            <div class="btn-group" role="group" aria-label="...">
                <a href="item_cat_list.php" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE_CAT); ?>" class="btn btn-default"><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE_CAT); ?></a>
                <a href="item_cms.php" title="<?php print($STR_TITLE_GO_TO);?> <?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)" class="btn btn-default"><i class="fa fa-chevron-left "></i>&nbsp;<?php print($STR_TITLE_PAGE); ?> (<?php print($STR_TITLE_MANAGE_CONTENT); ?>)</a>
            </div>
        </div>
    <div class="col-md-6 col-sm-6 col-xs-12" align="right" ><h3><?php print($STR_TITLE_PAGE);?> </h3></div>
    </div>
    <hr> 
    <?php if($str_type != "" && $str_message != ""){ print(DisplayMessage(0,$str_message,$str_type)); 	} ?>
        <div class="row padding-10">
            <div class="col-md-12">
        	<div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                	<div class="panel-heading">
                            <h4 class="panel-title">
                                <div class="row padding-10">
                                    <div class="col-md-12" align="right"><?php print($STR_LINK_HELP); ?></div>                                                           </div>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
	</div>
	<div class="table-responsive">
            <form name="frm_list" action="item_del_p.php" method="post" onSubmit="return confirm_delete();">
		<table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th width="4%">Sr. #</th>
                                <th width="10%">Date</th>
                                <th width="">Inquiry Details</th>
                                <th width="20%">Contact Details</th>
                                <th width="6%">New Mail<br>
                                    <?php if($rs_list->EOF()==false) {?><input type="checkbox"  class="" onClick="return frm_list_check_all_new();" name="chk_new_all" value="<?php print($int_cnt)?>">
                                    <?php } ?></th>
                                <th width="6%">Delete<br/>
                                <?php if($rs_list->EOF()==false) {?><input type="checkbox" class="" onClick="return frm_list_check_all_del();" name="chk_del_all" value="<?php print($int_cnt)?>">
                                <?php } ?>
                            </th>
                        </tr>	
                    </thead>
                    <tbody>
                  <?php if($rs_list->EOF()==true)  {  ?><tr><td colspan="6" align="center" class="alert alert-danger"><?php print($STR_MSG_NO_DATA_AVAILABLE); ?></td></tr>
                  <?php } else { $int_cnt=1; 
                            while(!$rs_list->EOF()==true) { ?>
                                <tr>
                                    <td align="center"><?php print($int_srno);?></td>
                                    <?php list($date,$time)=explode(" ",$rs_list->fields("submitdatetime"))?>
                                    <td align="center" class="text-help"><?php print(DDMMMYYYYFormat($date)." ".$time);?></td>
                                    <td align="left">
                                  <?php if($rs_list->fields("title")!="") { ?><label><h4 class="nopadding"><strong><?php print(MyHtmlEncode($rs_list->fields("title")));?></strong></h4></label><?php } ?>
                                   
                                        <p align="justify"> <?php print(RemoveQuote($rs_list->fields("description")));?></p>
                                    </td>
                                    <td align="left" class="align-top">
                                   <?php if($rs_list->fields("fullname")!="") { ?><b><i class="fa fa-user"></i>&nbsp;<?php print(MyHtmlEncode($rs_list->fields("fullname")));?></b><br/><?php } ?>						
                                   <?php if($rs_list->fields("emailid")!="") { ?><i class="fa fa-envelope"></i>&nbsp;<?php print(DisplayEmail($rs_list->fields("emailid"),StuffCharacter($rs_list->fields("emailid"),45,"<br>"),"link",""));?>&nbsp;<br/><?php } ?>
                                   <?php if($rs_list->fields("phone")!="") { ?><i class="fa fa-phone"></i>&nbsp;<?php print(MyHtmlEncode($rs_list->fields("phone")));?><br/><?php } ?>
                                   <?php /*?><?php if($rs_list->fields("fax")!="") { ?> <?php print(ReplaceRecordWithCharacter($rs_list->fields("fax"),"<i class='fa fa-phone-alt'></i>"));?>&nbsp;<?php print(MyHtmlEncode($rs_list->fields("fax")));?><br/><?php } ?><?php */?>
                                   <?php if($rs_list->fields("url")!="") { ?><i class="fa fa-globe"></i>&nbsp;<?php print(RemoveQuote($rs_list->fields("url")));?><br/><?php } ?>
                                   <?php if($rs_list->fields("ipaddress")!="") { ?><i class="fa fa-blackboard"></i>&nbsp;<?php print(MyHtmlEncode($rs_list->fields("ipaddress"))); }?>
                                    </td>
                                 <?php  $str_checked=""; if(strtoupper(trim($rs_list->fields("newmail")))=="YES") { $str_checked=" checked"; } ?>  
                                    <td align="center">
                                        <?php if($rs_list->fields("newmail")=="YES") { ?><span class="label label-success">NEW</span><br/><?php } ?>
                                        <input type="checkbox" class="input-sm" name="chk_<?php print($int_cnt);?>" <?php print($str_checked);?> onClick="return frm_list_check_new(this);"/>	
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="<?php print($int_cnt%2); ?>" name="chk_delete<?php print($int_cnt)?>" onClick="return frm_list_check_del(this);"/>
                                        <input type="hidden" name="hdn_pkid<?php print($int_cnt);?>" value="<?php print($rs_list->fields("uniquerowid"));?>">
                                    </td>
				</tr>
                            <?php $int_cnt++; $int_srno++; $rs_list->MoveNext(); 
                            } ?>
                            <tr>
                                <td>
                                    <input type="hidden" name="hdn_counter" value="<?php print($int_cnt);?>">
                                    <input type="hidden" name="hdn_page" value="<?php print($int_page); ?>"/>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                     <?php /* ?>           <?php
echo '<br><Button onclick="document.getElementById(';?>'modal-wrapper2'<?php echo ').style.display=';?>'block'<?php echo '" name="comment" style="width:100px; color: white;background-color: black;border-radius: 10px; padding: 4px;">Show</button>';  <?php */ ?>
                                
                               

                                <!--<td><?php // print DisplayFormButton("CHANGE"); ?></td>-->
                                <td><button type="button" class="btn btn-success btn-sm" name="btn_new_save" value="Change" title="<?php print($STR_HOVER_EDIT);?>" onClick="return displaynew_click();"><b>Change</b></button></td>
                                <td><?php print DisplayFormButton("DELETE",0); ?></td>
                            </tr>
                            <?php if($rs_list->count()>0) {
                            $int_margine=5; ?>
                            <tr>
                                <td colspan="6" class="text-center nopadding">
                                    <nav aria-label="Page navigation">
                                        <ul class="pagination">
                                            <?php print(PagingWithMargine($int_total_record,$int_margine,$int_page,"item_list.php",$int_record_per_page,"",""));?>
                                        </ul>
                                    </nav>

                                </td>
                            </tr>
                        <?php } ?>
                  <?php } ?>					
                </tbody>
            </table>
	</form>
    </div>
    <?php include "../../includes/help_for_list.php"; ?>
</div>
<script language="JavaScript" src="./item_list.js" type="text/javascript"></script>
<?php include "../../includes/include_files_admin.php"; ?>
<?php include($STR_ADMIN_FOOTER_PATH); ?>
</body></html>
