<?php 	
$str_host_url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
//$str_host_url = substr($str_host_url,0,strpos($str_host_url,"/admin")+1)."user/default.php";
$str_host_url = substr($str_host_url,0,strpos($str_host_url,"/admin")+1);	
?>
<link rel="shortcut icon" href="../../favicon.ico">

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                    <?php 
                        $first_part="";
                        $directoryURI = $_SERVER['REQUEST_URI'];
                        $path = parse_url($directoryURI, PHP_URL_PATH);
                        //print $path; exit;
                        $components = explode('/', $path);
                        //print(array_count($components)); exit;
                        //print count($components); 
                        //$first_part = $components[3]; //For LOCAL ?>
                    <a href="<?php print($str_host_url);?>" target="_blank" title="Click to visit website">
                        <?php if(count($components) <= 5) { ?>
                            <img  src="../images/logo.png" width="200px" height="auto"  class="logo">
                        <?php } else { ?>
                            <img src="../../images/logo.png" width="200px" height="auto"  class="logo">
                        <?php } ?>    
                    </a>                    
                    <?php /*?><span class="admin-text">&nbsp;&nbsp;
                        <?php 
                        if(isset($_SESSION["adminname"])) {
                                if ($_SESSION["adminname"] != "") { ?>
                                Welcome, 
                        <span><b><?php print($_SESSION["adminname"]);?></b></span> 
                                <?php if(strtoupper($_SESSION["superadmin"])=="YES"){ ?>(Super Admin) <?php } ?>
                                <?php } ?>
                    <?php } ?>
                    </span><?php */?>                    
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                 <ul class="nav navbar-nav navbar-right">
                    <?php if(count($components) > 5) { ?>
                    <li>
                        <a href="../admin_home.php" title="Go to admin main menu"><i class="fa fa-home fa-2x nopadding"></i></a>
                    </li>
                    <?php } ?>
                    
                    <li>
                        <?php if(count($components) > 5) { ?>
                            <a href="../logout_p.php" title="Logout from admin panel"><i class="fa fa-sign-out fa-2x nopadding"></i></a>
                        <?php } else { ?>
                            <a href="../admin/logout_p.php" title="Logout from admin panel"><i class="fa fa-sign-out fa-2x nopadding"></i></a>
                        <?php } ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                   
                    <li class="admin-text align-middle">
                         
                        <?php 
                        if(isset($_SESSION["adminname"])) {
                                if ($_SESSION["adminname"] != "") { ?>
                                Welcome, 
                        <span><b><?php print($_SESSION["adminname"]);?></b></span> 
                                <?php if(strtoupper($_SESSION["superadmin"])=="YES"){ ?>(Super Admin) <?php } ?>
                                <?php } ?>
                    <?php } ?>&nbsp; &nbsp;
                    </li>
                </ul>
               
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

<?php /*?><div class="row">
	<div class="col-md-12">
		<div class="header">
			<div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12"><a href="<?php print($str_host_url);?>" target="_blank" title="Click to visit website"><img  src="../images/logo.png" class="img-responsive logo" width="350px" height="auto"></a></div>
                <div class="col-md-6 col-sm-6 col-xs-12" align="right"><p align="right">
				<?php 
				if(isset($_SESSION["adminname"])) {
					if ($_SESSION["adminname"] != "") { ?>
      					Welcome, 
            			<span><b><?php print($_SESSION["adminname"]);?></b></span> 
           				<?php if(strtoupper($_SESSION["superadmin"])=="YES"){ ?>(Super Admin) <?php } ?>
					<?php } ?>
					</p>
					<a href="../admin_home.php" title="Go to admin main menu" class="btn btn-warning"><i class="glyphicon  glyphicon-home "></i>&nbsp;Main Menu</a>&nbsp;&nbsp;		
					
					<a href="../logout_p.php" title="Logout from admin panel" class="btn btn-danger" ><i class="glyphicon glyphicon-log-out"></i>&nbsp;Logout</a>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php */?>
