<?php /*?><a name="help"></a>

<div class="row padding-10">
    <div class="col-md-12">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <div class="row padding-10">
                    <div class="col-md-6 col-xs-8"><b><?php print($STR_MSG_HELP_HEADING); ?></b></div>
                    <div class="col-md-6 col-xs-4" align="right"><?php print($STR_LINK_TOP); ?></div>        
		</div>
            </div>
            <div class="panel-body">
                <p class="text-help"><?php print($STR_MSG_MANDATORY); ?></p>
		<p class="text-help"><?php print($STR_MSG_HTML); ?></p>
            </div>
        </div>
    </div>
</div><?php */?>


<a name="help"></a>
<div class="row padding-10">
    <div class="col-md-12">
        <div class="breadcrumb">
            <h4><b><?php print($STR_MSG_HELP_HEADING); ?></b></h4>
            <div class="row padding-10">
                <div class="col-md-12">
                    <p class="text-help"><?php print($STR_MSG_MANDATORY); ?></p>
                    <p class="text-help"><?php print($STR_MSG_HTML); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>


<?php /*?><a name="help"></a>

<div class="row padding-10">
    <div class="col-md-12">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <b><?php print($STR_MSG_HELP_HEADING); ?></b>
            </div>
            <div class="panel-body">
                <p class="text-help"><?php print($STR_MSG_MANDATORY); ?></p>
		<p class="text-help"><?php print($STR_MSG_HTML); ?></p>
            </div>
        </div>
    </div>
</div><?php */?>


<?php /*?><div class="panel panel-warning">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="alert-link" aria-expanded="false"><b><i class="glyphicon glyphicon-hand-right"></i>&nbsp;<?php print($STR_MSG_HELP_HTML_OBJ);?></b></a>
					</h4>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
					<div class="panel-body">
											<div class="container-fluid panel-body">
							<p><?php print($STR_MSG_HELP_HTML_OBJ);?></p><p class="line-hr"></p>
							<div class="row">
									<div class="col-md-6">
											<h4 class="nopadding heading"><b>Text & Headings colors</b></h4>
											<code>&lt;p class="text-success"&gt;your text&lt;/p&gt;</code><br/>
											<b>Use any of below class for different color options:</b><br/>
												<span class="text-primary">text-primary</span>&nbsp;
												<span class="text-success">text-success</span>&nbsp;
												<span class="text-info">text-info</span>&nbsp;
												<span class="text-warning">text-warning</span>&nbsp;
												<span class="text-danger">text-danger</span>&nbsp;
												<p class="line-hr"></p>
										 
											<h4 class="nopadding heading"><b>Text background </b></h4>
											<p><code>&lt;div class="bg-success"&gt;...&lt;/div&gt;</code></p>
											<b>Use any of below class for different color options:</b><br/>
												<span class="bg-primary">bg-primary</span>&nbsp;
												<span class="bg-success">bg-success</span>&nbsp;
												<span class="bg-info">bg-info</span>&nbsp;
												<span class="bg-warning">bg-warning</span>&nbsp;
												<span class="bg-danger">bg-danger</span>
												<p class="line-hr"></p>
										 
											<h4 class="nopadding heading"><b>Link tag</b></h4>
											<code>&lt;a href="#" class="text-primary"&gt;your text&lt;/a&gt;</code><br/>
											<b>Use any of below class for different color options:</b><br/>
												<a href="#" class="text-primary">text-primary</a>&nbsp;
												<a href="#" class="text-success">text-success</a>&nbsp;
												<a href="#" class="text-info">text-info</a>&nbsp;
												<a href="#" class="text-warning">text-warning</a>&nbsp;
												<a href="#" class="text-danger">text-danger</a>
									</div>	
									
									<div class="col-md-6">
											<h4 class="nopadding heading"><b>Labels</b></h4>
											<p><code>&lt;div class="label label-default"&gt;...&lt;/div&gt; </code></p>
											<b>Use any of below class for different color options:</b><br/>
												<span class="label label-primary">label label-primary</span>&nbsp;
												<span class="label label-success">label label-success</span>&nbsp;
												<span class="label label-info">label label-info</span>&nbsp;
												<span class="label label-warning">label label-warning</span>&nbsp;
												<span class="label label-danger">label label-danger</span>
												<p class="line-hr"></p>
									
											<h4 class="nopadding heading"><b>Various type of icons are available at below links. You  can use them as per given example below.</b></h4>
											<br/>
											<p><a href="http://getbootstrap.com/components/" target="_blank">click here to view GLYPHICON icons.</a></p>
											<code>&lt;i class="glyphicon glyphicon-home"&gt;&lt;/i&gt;</code>
											<br/><br/>
											<p><a href="http://fontawesome.io/icons/" target="_blank">click here to view FONT AWESOME icons.</a></p>
											<code>&lt;i class="fa fa-home" aria-hidden="true"&gt;&lt;/i&gt;</code>
									</div>
								</div>
								<p class="line-hr"></p>
								
										
								<h4 class="nopadding heading"><b>Mobile Friendly Table Structure</b></h4>
								<p></p>
									<p><?php print($STR_MSG_HELP_HTML_TABLE);?></p><br/>
									<p><b>&bull; col-md-12 - Consider it as column and 12 indicates total available width.</b></p>
									<p><b>&bull; col-md-6 - Consider it as column and 6 indicates half of total available width.</b></p>
									<p><b>&bull; col-md-1 - Consider it as column and 1 indicates 1 /12 part of total available width.</b><p/>
									<p><b>&bull; You can use various combination of col-md-* as per need but total of number must not exceed 12.</b><p/><br />
									<P><code>&lt;div class="row"&gt;<br/>&nbsp;&nbsp;&lt;div class="col-md-12"&gt;your text here&lt;/div&gt;<br/>&lt;/div&gt;</code></P>
									<P><code>&lt;div class="row"&gt;<br/>&nbsp;&nbsp;&lt;div class="col-md-6"&gt;your text here&lt;/div&gt;<br/>&nbsp;&nbsp;&lt;div class="col-md-6"&gt;your text here&lt;/div&gt;<br/>&lt;/div&gt;</code></P><br/>
								<div class="row">
									<div class="col-lg-12 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-12</div></div></div>
								</div>
					
								<div class="row">
									<div class="col-lg-6 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-6</div></div></div>
									<div class="col-lg-6 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-6</div></div></div>
								</div>
					
								<div class="row">
									<div class="col-lg-4 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-4</div></div></div>
									<div class="col-lg-4 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-4</div></div></div>
									<div class="col-lg-4 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-4</div></div></div>
								</div>
					
								<div class="row">
									<div class="col-lg-3 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-3</div></div></div>
									<div class="col-lg-3 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-3</div></div></div>
									<div class="col-lg-3 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-3</div></div></div>
									<div class="col-lg-3 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-3</div></div></div>
								</div>
					
								<div class="row">
									<div class="col-lg-2 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-2</div></div></div>
									<div class="col-lg-2 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-2</div></div></div>
									<div class="col-lg-2 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-2</div></div></div>
									<div class="col-lg-2 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-2</div></div></div>
									<div class="col-lg-2 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-2</div></div></div>
									<div class="col-lg-2 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-2</div></div></div>
								</div>
					
								<div class="row padding-10">
									<div class="col-lg-1 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-1</div></div></div>
									<div class="col-lg-1 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-1</div></div></div>
									<div class="col-lg-1 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-1</div></div></div>
									<div class="col-lg-1 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-1</div></div></div>
									<div class="col-lg-1 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-1</div></div></div>
									<div class="col-lg-1 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-1</div></div></div>
									<div class="col-lg-1 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-1</div></div></div>
									<div class="col-lg-1 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-1</div></div></div>
									<div class="col-lg-1 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-1</div></div></div>
									<div class="col-lg-1 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-1</div></div></div>
									<div class="col-lg-1 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-1</div></div></div>
									<div class="col-lg-1 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-1</div></div></div>
								</div>
					
					
								<div class="row">
									<div class="col-lg-8 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-8</div></div></div>
									<div class="col-lg-4 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-4</div></div></div>
								</div>
					
								<div class="row">
									<div class="col-lg-3 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-3</div></div></div>
									<div class="col-lg-6 text-center"><div class="panel panel-default"><div class="panel-body">.col-md-6</div></div></div>
									<div class="col-lg-3 text-center"><div class="panel panel-default"><div class="panel-body">col-md-3</div></div></div>
								</div>
							</div>
					</div>
				</div>
			</div><?php */?>
