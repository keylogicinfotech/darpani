<?php

/*
*		Function Name : CompressImage(string,string,[,int])
*		Purpose : To compress image quality in size without reducing image dimensions
*/

	function CompressImage($source, $destination, $quality) 
	{
		$info = getimagesize($source);
		if ($info['mime'] == 'image/jpeg') 
	        	$image = imagecreatefromjpeg($source);
		elseif ($info['mime'] == 'image/gif') 
		        $image = imagecreatefromgif($source);
		elseif ($info['mime'] == 'image/png') 
	        	$image = imagecreatefrompng($source);
		
		imagejpeg($image, $destination, $quality);
		return $destination;
	}
#-------------------------------------------------------------------------------------------------
/*
*		Function Name : GetThumbImage(string,int,[,int])
*		Purpose : To create the ThumbImage from the image given by the image path
*		Arguments : $path : contains the valid path for the image
*                   $width : contains the width of the thumbnail image
*					$height : Optional parameter contains the height of the thumbnail image.
*							  If no parameter is given for the $height then the thumbnail image is converted
*							   as per the aspect ratio.
*		Usage :   Here, we need to call GetThumbImage() function from individual page which contains header of Content - Type = Image.
				  And from that particular page, call function GetThumbImage().
				  GetThumbImage('./images/testImage.jpg',100,65);
*   			  GetThumgImage('./image/testImage.jpg',100); convert the height as per the aspect ratio.	
*/
	function GetThumbImage($path,$width,$height=0) 
	{
		$strextension = strtoupper(GetURLExtension($path));
		if($strextension == "JPEG" || $strextension == "JPG")
		{
			$sourceImage=imagecreatefromjpeg($path);
			if($height==0)
				{
				$height=($width*(imagesy($sourceImage)/imagesx($sourceImage)))+1;
				}
			$thumbImage=imagecreatetruecolor($width,$height);
			imagecopyresampled($thumbImage,$sourceImage,0,0,0,0,$width,$height,imagesx($sourceImage),imagesy($sourceImage));
			imagejpeg($thumbImage,'',100);		
			imagedestroy($sourceImage);
			imagedestroy($thumbImage);
			return;			
		}
		elseif($strextension == "PNG")
		{
			$sourceImage=imagecreatefrompng($path);
			imagealphablending($sourceImage, true); // setting alpha blending on
			imagesavealpha($sourceImage, true); // save alphablending setting (important)
			if($height==0)
				{
				$height=($width*(imagesy($sourceImage)/imagesx($sourceImage)))+1;
				}
			$thumbImage=imagecreatetruecolor($width,$height);
			imagecopyresampled($thumbImage,$sourceImage,0,0,0,0,$width,$height,imagesx($sourceImage),imagesy($sourceImage));
			imagepng($thumbImage,'',100);		
			imagedestroy($sourceImage);
			imagedestroy($thumbImage);
			return;			
		}
		elseif($strextension == "GIF")
		{
			$sourceImage=imagecreatefromgif($path);
			if($height==0)
				{
				$height=($width*(imagesy($sourceImage)/imagesx($sourceImage)))+1;
				}
			$thumbImage=imagecreatetruecolor($width,$height);
			imagecopyresampled($thumbImage,$sourceImage,0,0,0,0,$width,$height,imagesx($sourceImage),imagesy($sourceImage));
			imagegif($thumbImage,'',100);		
			imagedestroy($sourceImage);
			imagedestroy($thumbImage);
			return;			
		}
		else
		{
			return false;
		}
	}	

//   End of function GetThumbImage()
#-------------------------------------------------------------------------------------------------
//	Begining of SaveAsThumbImage function
/*
*		Function name : SaveAsThumbImage($sourcepath,$destinationpath,$width,$height=0)
*		Purpose	:	To save the image from the source image path to destination image path.
*		Argument : $sourcepath - String contains the valid source image path.
				   $destinationpath - String contains the valid destination image path.
				   $width - numeric value which will represent destination image's width
				   $height - numeric value which will represent destination image's height (optional)
*		Usage :	SaveAsThumbImage('./images/source.jpeg','./images/destination.jpeg',100);
*/
	function SaveAsThumbImage($sourcepath,$destinationpath,$width,$height=0)
	{	
		$strextension = strtoupper(GetURLExtension($sourcepath));
		// for image of JPEG format
		if($strextension == "JPEG" || $strextension == "JPG")
		{
			$sourceImage=imagecreatefromjpeg($sourcepath);
			$img_width = imagesx($sourceImage);			//get the source image width.
			if ($img_width < $width)					//compare source image width with passed image width.
			{
				imagejpeg($sourceImage,$destinationpath,90);		
				imagedestroy($sourceImage);
				return;
			}
			else
			{			
				if($height==0)
				{
					$height=($width*(imagesy($sourceImage)/imagesx($sourceImage)))+1;
				}
				$thumbImage=imagecreatetruecolor($width,$height);
				imagecopyresampled($thumbImage,$sourceImage,0,0,0,0,$width,$height,imagesx($sourceImage),imagesy($sourceImage));
				imagejpeg($thumbImage,$destinationpath,90);		
				imagedestroy($sourceImage);
				imagedestroy($thumbImage);
				return;
			}
		}
		// for image of PNG format
		elseif($strextension == "PNG")
		{
			$sourceImage=imagecreatefrompng($sourcepath);
			//imagealphablending($sourceImage, true); // setting alpha blending on
			//imagesavealpha($sourceImage, true); // save alphablending setting (important)
			$img_width = imagesx($sourceImage);		//get the source image width.
			if ($img_width < $width)					//compare source image width with passed image width.
			{
				imagepng($sourceImage,$destinationpath,9);
				imagedestroy($sourceImage);
				return;
			}
			else
			{
				if($height==0)
				{
					$height=($width*(imagesy($sourceImage)/imagesx($sourceImage)))+1;
				}
				$thumbImage=imagecreatetruecolor($width,$height);
				imagecopyresampled($thumbImage,$sourceImage,0,0,0,0,$width,$height,imagesx($sourceImage),imagesy($sourceImage));
				imagepng($thumbImage,$destinationpath,9);
				imagedestroy($sourceImage);
				imagedestroy($thumbImage);
				return;						
			}
		}
		// for image of GIF format
		elseif($strextension == "GIF")
		{
			$sourceImage=imagecreatefromgif($sourcepath);
			$img_width = imagesx($sourceImage);			//get the source image width.
			if ($img_width <= $width)					//compare source image width with passed image width.	
			{
				//imagegif($sourceImage,$destinationpath,90);		
				imagegif($sourceImage,$destinationpath);
				imagedestroy($sourceImage);
				return;
			}
			else
			{			
				if($height==0)
				{
					$height=($width*(imagesy($sourceImage)/imagesx($sourceImage)))+1;
				}
				$thumbImage=imagecreatetruecolor($width,$height);
				imagecopyresampled($thumbImage,$sourceImage,0,0,0,0,$width,$height,imagesx($sourceImage),imagesy($sourceImage));
				//imagegif($thumbImage,$destinationpath,90);	
				imagegif($sourceImage,$destinationpath);
				imagedestroy($sourceImage);
				imagedestroy($thumbImage);
				return;
			}
		}
		else
		{
			return false;
		}
	}	
//	End of SaveAsThumbImageFunction
#-------------------------------------------------------------------------------------------------------
/*
*		Function name : GetImage(String)
*		Purpose	:	To display the image from the path specified by argument ($path)
*		Argument : $path - String contains the valid image path.
*		Usage :	GetImage('./images/test.jpeg');
*/	
	function GetImage($path)
	{
		$sourceImage=imagecreatefromjpeg($path);
		imagejpeg($sourceImage,'',100);		
		imagedestroy($sourceImage);
	}

//  end of function GetImage()


#-------------------------------------------------------------------------------------------------------
#	Folloing function is used to resize the existing image.
#''	Function Name :- ResizeImage
#''	Prototype :- ResizeImage($sourcefilepath,$width,$height=0)
#''	Input Parameter :- 2 Parameters Required. one is optional
#''	Return Value :- boolean value (true or false) this will dependable on function "SaveAsThumbImage()".
#''	Purpose :- It will resize the existing image with same name on passed location.
	function ResizeImage($sourcefilepath,$width,$height=0)
	{
		$filepath=$sourcefilepath;
		$filename=substr($sourcefilepath,strrpos($filepath,"/")+1);
		$destinationtempfile="temp_" . $filename;
		$destinationtempfilepath=substr($sourcefilepath,0,strrpos($filepath,"/")+1).$destinationtempfile;
		SaveAsThumbImage($sourcefilepath,$destinationtempfilepath,$width,$height);
		unlink($sourcefilepath);
		rename($destinationtempfilepath,$sourcefilepath);
		return true;
	}
#----------------------------------------------------------------------------------------------------
#	Following function is used to get img tag of the existing image.
#''	Function Name :- ResizeImageTag
#''	Prototype :- ResizeImageTag($sourcefilepath,$recheight,$recwidth,cofigwidth,$alttext,$title,$flag)
#''				$sourcefilepath=virtual path where image exists,if in case you want image tag to return absolute path 
#''								than pass parameter 1 for $flag else 0.
#''				$recwidth=width of image.
#'' 			$recheight=height of image.
#'' 			$configwidth=width defined in config variable
#'' 			$alttext=alter text for image tag.
#'' 			$title=title for image tag.
#''	Input Parameter :- 7 Parameters Required, 3 are optional
#''	Return Value :- image tag .
#''	Purpose :- It will return image tag of the image.

	function ResizeImageTag($path,$uheight=0,$uwidth=0,$cwidth,$alttext="",$title="",$flag=0)
	{
           
		$configwidth=$cwidth;
                //print $path."<br/>";
		if(file_exists(".".$path))
		{
			$user_height=$uheight;
                        
			$user_width=$uwidth;
			$str_flag=$flag;
			$str_height="";
			$cmp_width=0;
			$str_title="";
			list($width,$height) = getimagesize(".".$path);
			if($title!="")
			{
				$str_title=" title="."\"".$title."\""."";
			}
			if($user_height!=0)
			{
				$str_height=" height="."\"".$user_height."\""."";
			}
			if($user_width!=0)
			{
				$cmp_width = $user_width;
			}
			else
			{
				$cmp_width = $width;
			} 
			if($cmp_width >=$configwidth)
			{
				$str_width=" width="."\"".$configwidth."\""."";
			}
			else
			{
				$str_width=" width="."\"".$cmp_width ."\""."";
			}
			if($str_flag==0)
			{
				$str_path=$path;
                                //print $str_path;exit;
			}
			elseif($str_flag==1)
			{
						$str_host_url ="";
					  	$str_img_src = "";
						$str_host_url = strtolower("http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
						$str_img_src = substr($str_host_url,0,strpos($str_host_url,"user")-1);												
						$str_img_src=$str_img_src."/".substr($path,3);		
                                                //print $str_img_src;exit;
						$str_path=$str_img_src;
			}
                        
			return "<img src="."\"".$str_path."\""." border=0 class='img-responsive' alt="."\"".$alttext."\" ".$str_title.$str_width.$str_height.">";
                        
                        
		}
	}
	
	#----------------------------------------------------------------------------------------------------
#	Following function is used to get img tag of the existing image.
#''	Function Name :- DisplaySWFFile
#''	Prototype :- DisplaySWFFile($src_path,$click_url,$int_width="0",$int_height="0",$click_mode="_blank",$bg_clor="#FFFFFF")
#''				$src_path=virtual path where image exists,if in case you want image tag to return absolute path 
#''								than pass parameter 1 for $flag else 0.
#''				$click_url="Banner Click URL"
#''				$int_width=width of image.
#'' 			$int_height=height of image.
#'' 			$Click_Mode=URKL open in New Windoe or Not
#'' 			$bg_color=SWF File BAckfround Color
#''	Input Parameter :- 6 Parameters Required, 4 are optional
#''	Return Value :- object Tag .
#''	Purpose :- It will return object tag of the SWF file.

	function DisplaySWFFile($src_path,$click_url,$int_width="0",$int_height="0",$click_mode="",$click_tooltip="",$bg_clor="#FFFFFF",$flag=0)
	{	
		if(file_exists($src_path))
		{
			$int_height=$int_height;
			$int_width=$int_width;
			$str_bg_color=$bg_clor;
			$str_click_mode="";
			$str_click_url="";
			$str_src_path="";			
			$str_width="";
			$str_height="";			
			$str_object_tag="";
			$str_click_tooltip=$click_tooltip;
			
			list($width,$height) = getimagesize($src_path);
			if($int_height!=0)
			{
				$str_height=" height='".$int_height."'";
			}
			else
			{
				$str_height=" height='".$height."'";
			}
			if($int_width!=0)
			{
				$str_width=" width='".$int_width."'";
			
			}
			else
			{
				$str_width=" width='".$width."'";
			}		

			if($click_mode!="")
			{
				$str_click_mode="?click_mode=".$click_mode;
			}
			
			$str_click_tooltip="&click_tooltip=".urlencode($str_click_tooltip);
			
			if($click_url!="")
			{
				$str_click_url="&click_url=".urlencode($click_url);
			}
			if($flag==1)
			{
			$str_host_url ="";
			$str_host_url = strtolower("http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
			$str_host_url = substr($str_host_url,0,strpos($str_host_url,"user")-1);												
			$src_path=$str_host_url."/".substr($src_path,3);
			}			
			$str_src_path = $src_path.$str_click_mode.$str_click_tooltip.$str_click_url;
			
			$str_object_tag =  "<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0' ".$str_width.$str_height.">";
			$str_object_tag = $str_object_tag . "<param name='movie' value='".$str_src_path."'>";
			$str_object_tag = $str_object_tag . "<param name='quality' value='high'>";
			$str_object_tag = $str_object_tag . "<param name='bgcolor' value='".$str_bg_color."'>";
			$str_object_tag = $str_object_tag . "<param name='menu' value='false'>";
			$str_object_tag = $str_object_tag . "<embed src='".$str_src_path."' menu='false' quality='high' ".$str_width.$str_height. " bgcolor='".$str_bg_color."' type='application/x-shockwave-flash' pluginspage='http://www.macromedia.com/go/getflashplayer'></embed>";
			$str_object_tag = $str_object_tag . "</object>";			
			return $str_object_tag;
		}
	}

	
#----------------------------------------------------------------------------------------------------
#	Following function is used to get img tag of the existing image.
#''	Function Name :- DisplaySWFFileWithEmbedTag
#''	Prototype :- DisplaySWFFileWithEmbedTag($src_path,$click_url,$int_width="0",$int_height="0",$click_mode="_blank",$bg_clor="#FFFFFF")
#''				$src_path=virtual path where image exists,if in case you want image tag to return absolute path 
#''								than pass parameter 1 for $flag else 0.
#''				$click_url="Banner Click URL"
#''				$int_width=width of image.
#'' 			$int_height=height of image.
#'' 			$Click_Mode=URKL open in New Windoe or Not
#'' 			$bg_color=SWF File BAckfround Color
#''	Input Parameter :- 6 Parameters Required, 4 are optional
#''	Return Value :- object Tag .
#''	Purpose :- It will return only embed tag of the SWF file.

	function DisplaySWFFileWithEmbedTag($src_path,$click_url,$int_width="0",$int_height="0",$click_mode="",$click_tooltip="",$bg_clor="#FFFFFF",$flag=0)
	{		
		if(file_exists($src_path))
		{
			global $SITENAME_WITH_PROTOCOL;
			$int_height=$int_height;
			$int_width=$int_width;
			$str_bg_color=$bg_clor;
			$str_click_mode="";
			$str_click_url="";
			$str_src_path="";			
			$str_width="";
			$str_height="";			
			$str_embed_tag="";
			$str_click_tooltip=$click_tooltip;
			
			list($width,$height) = getimagesize($src_path);
			if($int_height!=0)
			{
				$str_height=" height='".$int_height."'";
			}
			else
			{
				$str_height=" height='".$height."'";
			}
			if($int_width!=0)
			{
				$str_width=" width='".$int_width."'";
			
			}
			else
			{
				$str_width=" width='".$width."'";
			}		

			if($click_mode!="")
			{
				$str_click_mode="?click_mode=".$click_mode;
			}
			
			$str_click_tooltip="&click_tooltip=".urlencode($str_click_tooltip);
			
			if($click_url!="")
			{
				$str_click_url="&click_url=".urlencode($click_url);
			}
			if($flag==1)
			{
				if($_SERVER['HTTP_HOST']=="" || $_SERVER['PHP_SELF']=="")
				{					
					$str_host_url=$SITENAME_WITH_PROTOCOL;
				}
				else
				{
					$str_host_url ="";
					$str_host_url = strtolower("http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
					$str_host_url = substr($str_host_url,0,strpos($str_host_url,"user")-1);																	
				}
				$src_path=$str_host_url."/".substr($src_path,3);
			}			
			$str_src_path = $src_path.$str_click_mode.$str_click_tooltip.$str_click_url;			
			$str_embed_tag = "<embed src='".$str_src_path."' menu='false' quality='high' ".$str_width.$str_height. " bgcolor='".$str_bg_color."' type='application/x-shockwave-flash' pluginspage='http://www.macromedia.com/go/getflashplayer'></embed>";
			return $str_embed_tag;
		}
	}
?>
