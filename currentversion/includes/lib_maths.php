<?php
#-------------------------------------------------------------------------------------------------
#	Function Name :- get_percentage
#	Input Parameter :- 2 Parameters Required.First Parameter that on which u want to get percentage.
#	and second parameter is from which u want to take percentage value
#	Return Value :- numeric
#	Purpose :- To Calculate Percentage

function get_percentage($param1,$param2)
{
	if($param1<>0 && $param2<>0)
	{
		$var_per=round($param2*100/$param1);
		return $var_per;
	}
	else
	{
		return 0;
	}
}
?>
