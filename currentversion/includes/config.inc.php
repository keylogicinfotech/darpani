<?php
    define('AUTHNET_LOGIN', $AUTHORIZE_API_LOGIN_ID);
    define('AUTHNET_TRANSKEY', $AUTHORIZE_TRANSACTION_KEY);

    if (!function_exists('curl_init'))
    {
        throw new Exception('CURL PHP extension not installed');
    }

    if (!function_exists('simplexml_load_file'))
    {
        throw new Exception('SimpleXML PHP extension not installed');
    }

?>