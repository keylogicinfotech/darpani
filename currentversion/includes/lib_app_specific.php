<?php
##------------------------------------------------------------------------------------------------------------
#Used in module modModelSearch
##------------------------------------------------------------------------------------------------------------
##------------------------------------------------------------------------------------------------------------
##------------------------------------------------------------------------------------------------------------
#	To get model age combo.
function GetModelAge($strName,$set=0)
{
	if($set == "")
	{
		$set = "";
	}
	$str= "<select name='" . $strName . "' class='clsDropDown'>";
	$str.= "<option value=''>-- Select Age --</option>";
	for($i=18;$i<=60;$i++)
	{
		$str.= "<option value='".$i."' ".CheckSelected($set,$i).">".$i." - Years</option>";
	}
	$str.= "</select>";	
	return $str;	
}
#-------------------------------------------------------------------------------------------------------------------------------
#	To get mode height in feet and inch.
function GetModelHeight($strFeet,$strInch,$setFeet=0,$setInch=0)
{
	if($setFeet == "")
	{
		$setFeet = "";
	}
	if($setInch == "")
	{
		$setInch = "";
	}
	$str= "<select name='" . $strFeet . "' class='clsDropDown'>";
	$str.= "<option value=''>-- Feet --</option>";
	for($i=4;$i<=6;$i++)
	{
		$str.= "<option value='".$i."' ".CheckSelected($setFeet,$i).">".$i." - Feet</option>";
	}
	$str.= "</select>";
	$str.= "&nbsp;&nbsp;";	
	$str.= "<select name='" . $strInch . "' class='clsDropDown'>";
	$str.= "<option value=''>-- Inch --</option>";
	for($i=0;$i<=11;$i++)
	{
		$str.= "<option value='".$i."' ".CheckSelected($setInch,$i).">".$i." - Inch</option>";
	}
	$str.= "</select>";	
	return $str;	
}
#-------------------------------------------------------------------------------------------------------------
#	To get model weight in lbs.
function GetModelWeight($strWeight,$set=0)
{
	if($set == "")
	{
		$set = "";
	}
	$str= "<select name='" . $strWeight . "' class='clsDropDown'>";
	$str.= "<option value=''>-- Select Weight --</option>";
	for($i=80;$i<=300;$i++)
	{
		$str.= "<option value='".$i."' ".CheckSelected($set,$i).">".$i." - Lbs</option>";
	}
	$str.= "</select>";	
	return $str;	
}
#-------------------------------------------------------------------------------------------------------------
#Used in module -> modPhotogallery, modJournal
#-------------------------------------------------------------------------------------------------------------------------------
/*''Function Name :- DisplayMemberAccess
''	Prototype :-  DisplayMemberAccess($strPreview,$strAccess)
''	Input Parameter :- 2 Parameters Required.
					   $strPreview -> value for preview field.
					   $strAccess -> value for access field.
''	Return Value :- string message
''	Purpose :- This function creates message according to passed values.
*/

function DisplayMemberAccess($strPreview,$strAccess)
{
	$msg="";
	if(strtoupper(trim($strPreview))=="YES" && strtoupper(trim($strAccess))=="YES")
	{
		$msg="Preveiw: ALL, Access: ALL";
	}
	else if(strtoupper(trim($strPreview))=="NO" && strtoupper(trim($strAccess))=="NO")
	{
		$msg="Preveiw: MEMBER, Access: MEMBER";
	}
	else if(strtoupper(trim($strPreview))=="YES" && strtoupper(trim($strAccess))=="NO")
	{
		$msg="Preveiw: ALL, Access: MEMBER";
	}
	else if(strtoupper(trim($strPreview))=="NO" && strtoupper(trim($strAccess))=="YES")
	{
		$msg="Preveiw: MEMBER, Access: MEMBER";	
	}
	return $msg;
}

#-------------------------------------------------------------------------------------------------------------------------------
#	To get model measurements.
function GetModelMeasurement($strMeasurement1,$strMeasurement2,$strMeasurement3,$strMeasurement4,$setMeasurement1=0,$setMeasurement2=0,$setMeasurement3=0,$setMeasurement4=0)
{
	if($setMeasurement1 == "")
	{
		$setMeasurement1 = "";
	}	
	if($setMeasurement2 == "")
	{
		$setMeasurement2 = "";
	}	
	if($setMeasurement3 == "")
	{
		$setMeasurement3 = "";
	}	
	if($setMeasurement4 == "")
	{
		$setMeasurement4 = "";
	}
	$str= "<select name='" . $strMeasurement1 . "' class='clsDropDown'>";
	$str.= "<option value=''>-Bust/Chest Size-</option>";
	for($i=30;$i<=42;$i++)
	{
		$str.= "<option value='".$i."' ".CheckSelected($setMeasurement1,$i).">".$i." - Inch</option>";
	}
	$str.= "</select>";
	$str.= "&nbsp;";
	$str.= "<select name='" . $strMeasurement2 . "' class='clsDropDown'>";
	$str.= "<option value=''>-Cup Size-</option>";
	$str.= "<option value='A'" . ($setMeasurement2 == "A" ? "selected":"") . ">A</option>";
	$str.= "<option value='B'" . ($setMeasurement2 == "B" ? "selected":"") . ">B</option>";
	$str.= "<option value='C'" . ($setMeasurement2 == "C" ? "selected":"") . ">C</option>";
	$str.= "<option value='D'" . ($setMeasurement2 == "D" ? "selected":"") . ">D</option>";
	$str.= "<option value='DD'" . ($setMeasurement2 == "DD" ? "selected":"") . ">DD</option>";
	$str.= "<option value='E'" . ($setMeasurement2 == "E" ? "selected":"") . ">E</option>";
	$str.= "<option value='EE'" . ($setMeasurement2 == "EE" ? "selected":"") . ">EE</option>";
	$str.= "<option value='F'" . ($setMeasurement2 == "F" ? "selected":"") . ">F</option>";
	$str.= "<option value='FF'" . ($setMeasurement2 == "FF" ? "selected":"") . ">FF</option>";
	$str.= "<option value='G'" . ($setMeasurement2 == "G" ? "selected":"") . ">G</option>";
	$str.= "<option value='GG'" . ($setMeasurement2 == "GG" ? "selected":"") . ">GG</option>";
	$str.= "<option value='H'" . ($setMeasurement2 == "H" ? "selected":"") . ">H</option>";
	$str.= "<option value='HH'" . ($setMeasurement2 == "HH" ? "selected":"") . ">HH</option>";
	$str.= "</select>";
	$str.= "&nbsp;-&nbsp;";	
	$str.= "<select name='" . $strMeasurement3 . "' class='clsDropDown'>";
	$str.= "<option value=''>-Waist Size-</option>";
	for($i=22;$i<=36;$i++)
	{
		$str.= "<option value='".$i."' ".CheckSelected($setMeasurement3,$i).">".$i." - Inch</option>";
	}
	$str.= "</select>";
	$str.= "&nbsp;-&nbsp;";
	$str.= "<select name='" . $strMeasurement4 . "' class='clsDropDown'>";
	$str.= "<option value=''>-Hip Size-</option>";
	for($i=30;$i<=44;$i++)
	{
		$str.= "<option value='".$i."' ".CheckSelected($setMeasurement4,$i).">".$i." - Inch</option>";
	}
	$str.= "</select>";				
	return $str;	
}

#-------------------------------------------------------------------------------------------------------------------------------
#	To get Model Online status ( if online then print ONLINE Other wise OFFLINE
function DisplayOnlineStatus($str_login,$dt_date,$int_time_diff=3)
{

	if($str_login=='YES' && (strtotime($dt_date) > strtotime(date("Y-m-d H:i:s",mktime(date("H")-$int_time_diff, date("i"), date("s"), date("m"), date("d"), date("Y")))))) 
	{
		print("<img src='./images/online_icon.gif'>");
		//print("<span class='OnlineText8ptBold'>ONLINE</span>");
	} 
	else
	{
		print("<img src='./images/offline_icon.gif'>");
		//print("<span class='OfflineText8ptBold'>OFFLINE</span>");
	}
}

#-------------------------------------------------------------------------------------------------------------------------------
#	To get Model shoe size combo.
function GetModelShoeSize($strShoe,$set=0)
{
	if($set == "")
	{
		$set = "";
	}
	$str= "<select name='" . $strShoe . "'>";
	$str.= "<option value=''>-- Select Shoe Size --</option>";
	for($i=5;$i<=15;$i++)
	{
		$str.= "<option value='".$i."' ".CheckSelected($set,$i).">".$i." - Inch</option>";
	}
	$str.= "</select>";	
	return $str;	
}
#-------------------------------------------------------------------------------------------------------------------------------
#	To get Model dress size combo.
function GetModelDressSize($strDress,$set=0)
{
	if($set == "")
	{
		$set = "";
	}
	$str= "<select name='" . $strDress . "'>";
	$str.= "<option value=''>-- Select Dress Size --</option>";
	for($i=1;$i<=20;$i++)
	{
		$str.= "<option value='".$i."' ".CheckSelected($set,$i).">".$i." </option>";
	}
	$str.= "</select>";	
	return $str;	
}
#-------------------------------------------------------------------------------------------------------------------------------
#	This function creates a date format to have input for date of birth.
#	Mainly used to display input for date
	function DisplayDateOfBirth($dayvalue,$monthvalue,$yearvalue,$varday=0,$varmonth=0,$varyear=0)
	{
		if($varday == "")
		{
			$varday = 0;
		}
		if($varmonth == "")
		{
			$varmonth = 0;
		}
		if($varyear == "")
		{
			$varyear = 0;
		}	
		
		$Tab="";
		$Tab = '<select name="'. $dayvalue .'" >';
		$Tab = $Tab . "<option value=''>--Day--</option>";
		for($i = 1;$i<=31;$i++)
		{
			$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$varday) . ">" . $i . "</option>";
		}
		$Tab = $Tab . "</select>";
		print $Tab;
		
		$Tab = '&nbsp;&nbsp;<select name="' . $monthvalue .'">';
		$Tab = $Tab . "<option value=''>--Month--</option>";
		$Tab = $Tab . "<option value='1'" . CheckSelected("1",$varmonth) . ">January</option>";
		$Tab = $Tab . "<option value='2'" . CheckSelected("2",$varmonth) . ">February</option>";
		$Tab = $Tab . "<option value='3'" . CheckSelected("3",$varmonth) . ">March</option>";
		$Tab = $Tab . "<option value='4'" . CheckSelected("4",$varmonth) . ">April</option>";
		$Tab = $Tab . "<option value='5'" . CheckSelected("5",$varmonth) . ">May</option>";
		$Tab = $Tab . "<option value='6'" . CheckSelected("6",$varmonth) . ">June</option>";
		$Tab = $Tab . "<option value='7'" . CheckSelected("7",$varmonth) . ">July</option>";
		$Tab = $Tab . "<option value='8'" . CheckSelected("8",$varmonth) . ">August</option>";
		$Tab = $Tab . "<option value='9'" . CheckSelected("9",$varmonth) . ">September</option>";
		$Tab = $Tab . "<option value='10'" . CheckSelected("10",$varmonth) . ">October</option>";
		$Tab = $Tab . "<option value='11'" . CheckSelected("11",$varmonth) . ">November</option>";
		$Tab = $Tab . "<option value='12'" . CheckSelected("12",$varmonth) . ">December</option>";
		$Tab = $Tab . "</select>";
		print $Tab;
		
		$Tab = '&nbsp;&nbsp;<select name="' . $yearvalue .'">';
		$Tab = $Tab . "<option value=''>--Year--</option>";
		$varcurrentyear = date("Y");
		$last_year=$varcurrentyear-70;
		
		for($i = $varcurrentyear;$i>=$last_year ;$i--)
		{
			$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$varyear) . ">" . $i . "</option>";
		}
		$Tab = $Tab . "</select>";
		print($Tab);
	}
#-------------------------------------------------------------------------------------------------------------------------------
function get_poll_chart($str_options,$int_chartwidth,$int_chartheight) 
{ 
	$str="";
	$int_chart_height=$int_chartheight*100/100;
	#$int_option_height=$int_chartheight*30/100;
	$int_total_votes=0;
	$int_max_vote=0;
	$arr_str_option=array();
	$arr_str_vote=array();
	$arr_str_optionlist=array();
	$arr_str_per=array();
	$arr_str_optionlist=explode(",",$str_options);
	$int_chart_width=$int_chartwidth*3/100;
	//$int_chart_width=$int_chartwidth;
	$int_per_height=50;
	$int_td_width=floor($int_chartwidth/((count($arr_str_optionlist)))+6);
	//$int_td_width=60;
	

	if(count($arr_str_optionlist)>1)
	{
		for($cnt=0;$cnt < count($arr_str_optionlist);$cnt++)
		{		
			$arr_str_options=explode("|",$arr_str_optionlist[$cnt]);
			$arr_str_option[$cnt]=$arr_str_options[0];
			$arr_str_vote[$cnt]=$arr_str_options[1];
			$int_total_votes+=$arr_str_options[1];
			if($int_max_vote < $arr_str_options[1])
			{
				$int_max_vote=$arr_str_options[1];
			}
		}
		for($cnt=0;$cnt < count($arr_str_vote);$cnt++)
		{
			if($arr_str_vote[$cnt] > 0)
			{
				$arr_str_per[$cnt]=number_format(($arr_str_vote[$cnt]/$int_total_votes*100),2);
			}
			else
			{
				$arr_str_per[$cnt]=0;
			}
		}
		$str.="<table height='$int_chart_height' width='$int_chart_width' border='0' cellspacing='0' cellpadding='0'>";	
		$str.="<tr><td align='left' valign='bottom'>
			<table border='0' cellspacing='0' cellpadding='0'>
				<tr>
				<td valign='bottom' align='left' width='$int_td_width'>";
		$str.="<table align='left' width='0' border='0' height='$int_chart_height' cellspacing='1' cellpadding='0' bgcolor='#000000'>";
		$str.="<tr><td valign='bottom' align='right'></td></tr></table></td>";
		
		$int_chart_height=$int_chart_height-($int_per_height+10);
		
		for($cnt=0;$cnt < count($arr_str_option);$cnt++)
		{
			$int_height=0;
			$str_color="#6F6F6F"; 		
			if($arr_str_vote[$cnt]==$int_max_vote)
			{
				$str_color="#000000";
			}
			if($arr_str_vote[$cnt]>0)
			{
				$int_height=ceil($arr_str_per[$cnt]*$int_chart_height/100);
			}
			$str.="<td height='$int_chart_height' valign='bottom' align='center' width='$int_td_width'>";
			$str.="<table align='center' border='0' cellspacing='0' cellpadding='0' width='$int_td_width'><tr>";	
			$str.="<td valign='bottom' align='center' class='poll_vote' width='$int_td_width'>";
			$str.="$arr_str_per[$cnt]<br>%";
			$str.="</td></tr>";
			if($arr_str_per[$cnt]>0)
			{
				$str.="<tr><td width='$int_td_width'><table align='center'><tr><td valign='bottom' bgcolor='$str_color' height='$int_height' align='center' width='15'></td></tr></table></td></tr>";
			}
			$str.="</table></td>";
			$str.="<td width='70'></td>";
		}
		$str.="</tr></table></td></tr>";
		$str.="<tr><td valign='top' height='2' width='$int_td_width' align='left' bgcolor='#000000'></td></tr>";	#<hr color='#000000' align='right' size='2'>
		$str.="<tr><td align='center' valign='bottom'><table border='0' cellspacing='0' cellpadding='0'><tr>";				
		$int_cnt=0;
		for($cnt=0;$cnt < count($arr_str_option);$cnt++)
		{
			$int_cnt++;
			$str.="<td class='poll_vote_percent' align='center' width='$int_td_width'>$int_cnt</td></td> ";
		}
		$str.="</tr></table></td></tr></table>";
	}
	return $str;
}

#------------------------------------------------------------------------------------------------------
function GetModelSubcatMaxValue($subcattable,$firstcatidcol,$firstcatid,$secondcatidcol,$secondcatid,$subcatcol)
{	
	$int_max=0;
	$str_query="select max(".$subcatcol.") as maxorder from ".$subcattable." where ".$subcattable.".".$firstcatidcol."=".$firstcatid ." and ". $secondcatidcol ."=". $secondcatid;
	$rs_max=GetRecordset($str_query);
	if($rs_max->EOF()!=true)
	{
		$int_max= $rs_max->fields("maxorder") + 1;
	}
	return($int_max);
}

#------------------------------------------------------------------------------------------------------
/*function Video_WriteXml()
{
	global $UPLOAD_XML_MODULE_PATH;
	global $MOD_VIDEO_ROOT;
	
	$arr1=array();
	$arr2=array();
	$arr3=array();
	$arr4=array();
	
	$str_query_select = "SELECT a.videopkid,a.title,a.description,a.displayonhome,a.createddate,a.lastupdatedate,a.extratext,a.photofilename,a.previewtoall,a.accesstoall,";
	$str_query_select .= "a.displayasnew,a.displayashot,b.videoclippkid,b.cliptitle,b.videofilename,b.noofdownload ";
	$str_query_select .= "FROM t_video a LEFT JOIN tr_video_clip b ON a.videopkid=b.videopkid AND b.visible='YES' ";
	$str_query_select .= "WHERE a.visible='YES' ";
	$str_query_select .= "ORDER BY a.createddate desc,a.displayorder,a.title,b.displayorder,b.cliptitle";	
	$rs_xml_list=GetRecordset($str_query_select);
	
	$i=0;
	while(!$rs_xml_list->eof())
	{
		
		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="videopkid";
		$arr4[$i]=$rs_xml_list->fields("videopkid");  
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="title";
		$arr4[$i]=$rs_xml_list->fields("title");
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="desc";
		$arr4[$i]=$rs_xml_list->fields("description");
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="extra";
		$arr4[$i]=$rs_xml_list->fields("extratext");
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="photo";
		$arr4[$i]=$rs_xml_list->fields("photofilename");
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="createddate";
		$arr4[$i]=$rs_xml_list->fields("createddate");
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="lastdate";
		$arr4[$i]=$rs_xml_list->fields("lastupdatedate");
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="previewtoall";
		$arr4[$i]=$rs_xml_list->fields("previewtoall");
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="accesstoall";
		$arr4[$i]=$rs_xml_list->fields("accesstoall");
		$i=$i+1;		
		
		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="displayonhome";
		$arr4[$i]=$rs_xml_list->fields("displayonhome");
		$i=$i+1;		
		
		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="new";
		$arr4[$i]=$rs_xml_list->fields("displayasnew");
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="hot";
		$arr4[$i]=$rs_xml_list->fields("displayashot");
		$i=$i+1;
		
		$int_old_pkid=$rs_xml_list->fields("videopkid");
		$int_new_pkid=$rs_xml_list->fields("videopkid");
		
		$str_video_clip="";
		while($int_old_pkid==$int_new_pkid&& $rs_xml_list->eof()==false)
		{				
			if($rs_xml_list->fields("cliptitle")!="" && $rs_xml_list->fields("videoclippkid")!=0 && $rs_xml_list->fields("videoclippkid")!="")
			{			
				$str_video_clip.=$rs_xml_list->fields("videoclippkid")."|;|";
				$str_video_clip.=$rs_xml_list->fields("cliptitle")."|;|";
				$str_video_clip.=$rs_xml_list->fields("videofilename")."|;|";
				$str_video_clip.=$rs_xml_list->fields("noofdownload")."||;||";
			}
							
			$rs_xml_list->MoveNext();								
			$int_new_pkid=$rs_xml_list->fields("videopkid");
		}
		
		$arr1[$i]="P_".$int_old_pkid;
		$arr2[$i]="";
		$arr3[$i]="clip";
		$arr4[$i]=$str_video_clip;
		$i=$i+1;
	}
	writeXmlFile($UPLOAD_XML_MODULE_PATH."video.xml",$MOD_VIDEO_ROOT,$arr1,$arr2,$arr3,$arr4);

}
*/
#------------------------------------------------------------------------------------------------------------------------------------
/*
this function is used on modgirl module on pages like girl_profile_click_list.php etc.
this function takes a pipe seperated string of format dayno-dayclick| and returns an array of dayclick.
eg. 1-25|2-57|3-44|.....|31-45| is input string where 1,2,3...31 are day number while 25,57.. are click
on that particular day.
the function will return an array of clicks i.e 25,57... on success.
*/
	function GetDayClick($str_dayclick)
	{
		$arr_ret=array();
		if($str_dayclick=="")
		{
			return $arr_ret;
		}
		$str_dayclick=substr($str_dayclick,1);
		$arr=explode("|",$str_dayclick);	
		for($i=0;$i<count($arr)-1;$i++)
		{
			$arr_ret[$i]=substr($arr[$i],strpos($arr[$i],"-")+1,strlen($arr[$i]));
		}
		return $arr_ret;
	}

#--------------------------------------------------------------------------------------------------------
/*
this function is used to generate mdetail string as per the arguments passed.
- if both argument are blank then string with all dayclick=0 is generated
GetMonthClickDetails("","") will return 1-0|2-0|3-0|....|31-0|

- if first argument is blank then string with the dayclick=1 corresponding to day number passed as 2nd arg.,is generated
GetMonthClickDetails("",3) will return 1-0|2-0|3-1|....|31-0|

- if no argument is blank then string with the dayclick=dayclick+1 corresponding to day number passed as 2nd arg.,is generated
$str_string="1-0|2-100|3-1|....|31-10|
GetMonthClickDetails($str_string,2) will return 1-0|2-101|3-1|....|31-10|
*/
function GetMonthClickDetails($str_detail="",$int_dayno=0)
{
	$str_return="";
	if($str_detail=="" && $int_dayno==0)
	{
		for($i=1;$i<32;$i++)
		{
			$str_return.="|".$i."-0";
		}		
		$str_return.="|";
	}
	elseif($str_detail=="" && $int_dayno!=0)
	{
		for($i=1;$i<32;$i++)
		{
			if($i==$int_dayno)
			{
				$str_return.="|".$i."-1";
			}
			else
			{
				$str_return.="|".$i."-0";
			}	
		}
		$str_return.="|";
	}
	else
	{
		$str_needle="|".$int_dayno."-";
		$int_pos=strpos($str_detail,$str_needle);
		
		if(strlen($int_dayno)==2)
		{
			$int_pos=$int_pos+4;
		}
		else
		{
			$int_pos=$int_pos+3;		
		}	
		$str_sub=substr($str_detail,$int_pos);
		$str_dayclick=substr($str_sub,0,strpos($str_sub,"|"));
		$str_new_dayclick=$str_dayclick+1;
		$str_find="|".$int_dayno."-".$str_dayclick;
		$str_replace="|".$int_dayno."-".$str_new_dayclick;
		$str_return=str_replace($str_find,$str_replace,$str_detail);
	}
	
	return $str_return;
}

#---------------------------------------------------------------------------------------------------------------------------------
	function Resume_Synchronize($mdlpkid)
	{
		$int_mdlkid=$mdlpkid;
		if($int_mdlkid<0 || !is_numeric($int_mdlkid) || $int_mdlkid=="")
		{
			exit();
		}
		#select query to get details of no of statistic from tr_model_resume table
		$str_select="";
		$str_select="SELECT count(resumepkid) total FROM tr_mdl_resume where modelpkid=".$int_mdlkid;
		$rs_select=GetRecordset($str_select);
		$int_resume_count=0;
		if(!$rs_select->eof())
		{
			$int_resume_count=$rs_select->fields("total");
		}
		$updatedate="";
		$updatedate=date('Y/m/d');
		#update query to update the no. of resume
		$str_query_update = "";
		$str_query_update = "update t_model ";
		$str_query_update .= "set noofresume =".$int_resume_count .", ";
		$str_query_update .= "lastupdatedate='". ReplaceQuote($updatedate) ."' ";
		$str_query_update .= "Where modelpkid=".$int_mdlkid;
		ExecuteQuery($str_query_update);	
	}

#---------------------------------------------------------------------------------------------------------------------------------
function Resume_WriteXml($int_modelpkid)
{
	$int_mpkid=$int_modelpkid;
	if($int_mpkid<0 || !is_numeric($int_mpkid) || $int_mpkid=="")
	{
		exit();
	}
	
	global $XML_COMMON_PATH;
	global $XML_RESUME_ROOT_TAG;
	$str_xml_file_path=$XML_COMMON_PATH.$int_mpkid."/xmlcontentfiles/resume.xml";
	
	$arr1=array();
	$arr2=array();
	$arr3=array();
	$arr4=array();
	$select_query="";
	$select_query="SELECT * from tr_mdl_resume where visible='YES' and modelpkid=".$int_mpkid." order by displayorder";
	$rs_list=GetRecordset($select_query);
	$i=0;
	while(!$rs_list->eof())
	{
		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="modelpkid";
		$arr4[$i]=$rs_list->fields("modelpkid");  
		$i=$i+1;

		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="title";
		$arr4[$i]=$rs_list->fields("resumetitle");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="description";
		$arr4[$i]=$rs_list->fields("resumedescription");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="thumbfilename";
		$arr4[$i]=$rs_list->fields("resumethumbfilename");  
		$i=$i+1;

		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="largefilename";
		$arr4[$i]=$rs_list->fields("resumelargefilename");  
		$i=$i+1;

		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="urltitle";
		$arr4[$i]=$rs_list->fields("urltitle");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="url";
		$arr4[$i]=$rs_list->fields("url");  
		$i=$i+1;

		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="openinnewwindow";
		$arr4[$i]=$rs_list->fields("openinnewwindow");  
		$i=$i+1;

		$rs_list->MoveNext();
	}
	writeXmlFile($str_xml_file_path,$XML_RESUME_ROOT_TAG,$arr1,$arr2,$arr3,$arr4);
}

#--------------------------------------------------------------------------------------------------------
	function Statastics_Synchronize($mdlpkid)
	{
		$int_mdlkid=$mdlpkid;
		if($int_mdlkid<0 || !is_numeric($int_mdlkid) || $int_mdlkid=="")
		{
			exit();
		}
		#select query to get details of no of statistic from tr_model_statistics table
		$str_select="";
		$str_select="SELECT count(statisticsrno) total FROM tr_model_statistics where modelpkid=".$int_mdlkid;
		$rs_select=GetRecordset($str_select);
		$int_statistic_count=0;
		if(!$rs_select->eof())
		{
			$int_statistic_count=$rs_select->fields("total");
		}
		
		$updatedate="";
		$updatedate=date('Y/m/d');
		#update query to update the no. of statistic
		$str_query_update = "";
		$str_query_update = "update t_model ";
		$str_query_update .= "set noofstatistics =".$int_statistic_count .", ";
		$str_query_update .= "lastupdatedate='". ReplaceQuote($updatedate) ."' ";
		$str_query_update .= "Where modelpkid=".$int_mdlkid;
		ExecuteQuery($str_query_update);
		
	}

#---------------------------------------------------------------------------------------------------------------------------------
function Statistics_WriteXml($int_modelpkid)
{
	
	$int_mpkid=$int_modelpkid;
	if($int_mpkid<0 || !is_numeric($int_mpkid) || $int_mpkid=="")
	{
		exit();
	}
	
	global $XML_COMMON_PATH;
	global $XML_STATISTICS_ROOT_TAG;
	$str_xml_file_path=$XML_COMMON_PATH.$int_mpkid."/xmlcontentfiles/statistics.xml";
	
	$arr1=array();
	$arr2=array();
	$arr3=array();
	$arr4=array();
	$select_query="";
	
	//$select_query="SELECT * from tr_model_statistics where modelpkid=".$int_mpkid." order by displayorder";
	
	$select_query="SELECT s.statistickey,ms.statisticsrno,ms.statisticvalue,ms.modelpkid FROM t_statistics s LEFT JOIN tr_model_statistics ms ON s.statisticspkid=ms.statisticspkid WHERE ms.modelpkid=".$int_mpkid." ORDER BY s.displayorder";
	
	$rs_list=GetRecordset($select_query);
	$i=0;
	while(!$rs_list->eof())
	{
		$arr1[$i]="P_".$rs_list->fields("statisticsrno");
		$arr2[$i]="";
		$arr3[$i]="modelpkid";
		$arr4[$i]=$rs_list->fields("modelpkid");  
		$i=$i+1;

		
		$arr1[$i]="P_".$rs_list->fields("statisticsrno");
		$arr2[$i]="";
		$arr3[$i]="key";
		$arr4[$i]=$rs_list->fields("statistickey");  
		$i=$i+1;

		$arr1[$i]="P_".$rs_list->fields("statisticsrno");
		$arr2[$i]="";
		$arr3[$i]="value";
		$arr4[$i]=$rs_list->fields("statisticvalue");  

		$i=$i+1;
		$rs_list->MoveNext();
	}
	writeXmlFile($str_xml_file_path,$XML_STATISTICS_ROOT_TAG,$arr1,$arr2,$arr3,$arr4);
}


	function TurnOnOff_Synchronize($mdlpkid)
	{
		$int_mdlkid=$mdlpkid;
		if($int_mdlkid<0 || !is_numeric($int_mdlkid) || $int_mdlkid=="")
		{
			exit();
		}
		#select query to get details of no of statistic from tr_model_turnonoff table
		$str_select="";
		$str_select="SELECT count(resumepkid) total FROM tr_mdl_turnonoff where modelpkid=".$int_mdlkid;
		$rs_select=GetRecordset($str_select);
		$int_turnonoff_count=0;
		if(!$rs_select->eof())
		{
			$int_turnonoff_count=$rs_select->fields("total");
		}
		$updatedate="";
		$updatedate=date('Y/m/d');
		#update query to update the no. of turnonoff
		$str_query_update = "";
		$str_query_update = "update t_model ";
		$str_query_update .= "set noofturnonoff =".$int_turnonoff_count .", ";
		$str_query_update .= "lastupdatedate='". ReplaceQuote($updatedate) ."' ";
		$str_query_update .= "Where modelpkid=".$int_mdlkid;
		ExecuteQuery($str_query_update);	
	}

#---------------------------------------------------------------------------------------------------------------------------------
function TurnOnOff_WriteXml($int_modelpkid)
{
	$int_mpkid=$int_modelpkid;
	if($int_mpkid<0 || !is_numeric($int_mpkid) || $int_mpkid=="")
	{
		exit();
	}
	
	global $XML_COMMON_PATH;
	global $XML_TURNONOFF_ROOT_TAG;
	$str_xml_file_path=$XML_COMMON_PATH.$int_mpkid."/xmlcontentfiles/turnonoff.xml";
	
	$arr1=array();
	$arr2=array();
	$arr3=array();
	$arr4=array();
	$select_query="";
	$select_query="SELECT * from tr_mdl_turnonoff where visible='YES' and modelpkid=".$int_mpkid." order by displayorder";
	$rs_list=GetRecordset($select_query);
	$i=0;
	while(!$rs_list->eof())
	{
		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="modelpkid";
		$arr4[$i]=$rs_list->fields("modelpkid");  
		$i=$i+1;

		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="title";
		$arr4[$i]=$rs_list->fields("resumetitle");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="description";
		$arr4[$i]=$rs_list->fields("resumedescription");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="thumbfilename";
		$arr4[$i]=$rs_list->fields("resumethumbfilename");  
		$i=$i+1;

		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="largefilename";
		$arr4[$i]=$rs_list->fields("resumelargefilename");  
		$i=$i+1;

		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="urltitle";
		$arr4[$i]=$rs_list->fields("urltitle");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="url";
		$arr4[$i]=$rs_list->fields("url");  
		$i=$i+1;

		$arr1[$i]="P_".$rs_list->fields("resumepkid");
		$arr2[$i]="";
		$arr3[$i]="openinnewwindow";
		$arr4[$i]=$rs_list->fields("openinnewwindow");  
		$i=$i+1;

		$rs_list->MoveNext();
	}
	writeXmlFile($str_xml_file_path,$XML_TURNONOFF_ROOT_TAG,$arr1,$arr2,$arr3,$arr4);
}
#--------------------------------------------------------------------------------------------------------------------------------
function DisplayMemberAccess02($strPreview,$strAccess)
{
	$msg="";
	if(strtoupper(trim($strPreview))=="YES" && strtoupper(trim($strAccess))=="YES")
	{
		$msg="<span class='text-help'>Preveiw: </span><br/><span class='text-success'>ALL</span><hr/><span class='text-help'>Access: </span><br/><span class='text-success'>ALL</span>";
	}
	else if(strtoupper(trim($strPreview))=="NO" && strtoupper(trim($strAccess))=="NO")
	{
		$msg="<span class='text-help'>Preveiw: </span><br/><span class='text-danger'>MEMBER</span><hr/><span class='text-help'>Access: </span><br/><span class='text-danger'>MEMBER</span>";
	}
	else if(strtoupper(trim($strPreview))=="YES" && strtoupper(trim($strAccess))=="NO")
	{
		$msg="<span class='text-help'>Preveiw: </span><br/><span class='text-success'>ALL</span><hr/><span class='text-help'>Access: </span><br/><span class='text-danger'>MEMBER</span>";
	}
	else if(strtoupper(trim($strPreview))=="NO" && strtoupper(trim($strAccess))=="YES")
	{
		$msg="<span class='text-help'>Preveiw: </span><br/><span class='text-danger'>MEMBER</span><hr/><span class='text-help'>Access: </span><br/><span class='text-success'>ALL</span>";	
	}
	return $msg;
}
#---------------------------------------------------------------------------------------------------------------------------------
	function Schedule_Synchronize($mdlpkid)
	{
		$int_mdlkid=$mdlpkid;
		if($int_mdlkid<0 || !is_numeric($int_mdlkid) || $int_mdlkid=="")
		{
			exit();
		}
		#select query to get details of no of statistic from tr_model_resume table
		$str_select="";
		$str_select="SELECT count(schedulesrno) total FROM tr_model_schedule where modelpkid=".$int_mdlkid;
		$rs_select=GetRecordset($str_select);
		$int_schedule_count=0;
		if(!$rs_select->eof())
		{
			$int_schedule_count=$rs_select->fields("total");
		}
		$updatedate="";
		$updatedate=date('Y/m/d');
		#update query to update the no. of resume
		$str_query_update = "";
		$str_query_update = "update t_model ";
		$str_query_update .= "set noofschedule =".$int_schedule_count .", ";
		$str_query_update .= "lastupdatedate='". ReplaceQuote($updatedate) ."' ";
		$str_query_update .= "Where modelpkid=".$int_mdlkid;
		ExecuteQuery($str_query_update);	
	}

#---------------------------------------------------------------------------------------------------------------------------------
function PhotoAlbum_Synchronize($pkid,$mpkid)
{
	$int_pkid=$pkid;
	$int_mpkid=$mpkid;
	if($int_mpkid<0 || !is_numeric($int_mpkid) || $int_mpkid=="")
	{
		exit();
	}
	if($int_pkid<0 || !is_numeric($int_pkid) || $int_pkid=="")
	{
		exit();
	}
	#select query to count no. of visible records from  tr_photoalbum_detail table
	$str_select="";
	$str_select="SELECT count(photosrno) total FROM tr_model_photoalbum_detail where visible='YES' and modelpkid=".$int_mpkid." and photoalbumsrno=".$int_pkid;
	$rs_select=GetRecordset($str_select);
	#select query to count no. of invisible records from tr_photoalbum_detail table
	$str_select="";
	$str_select="SELECT count(photosrno) total FROM tr_model_photoalbum_detail where visible='NO' and modelpkid=".$int_mpkid." and photoalbumsrno=".$int_pkid;
	$rs_select1=GetRecordset($str_select);
	
	#select query to get details of no of new record from tr_photoalbum_detail table
	$str_select="";
	$str_select="SELECT count(photosrno) total FROM tr_model_photoalbum_detail where displayasnew='YES' and modelpkid=".$int_mpkid." and visible='YES' and photoalbumsrno=".$int_pkid;
	$rs_select2=GetRecordset($str_select);
	
	$int_visible_rec=0;
	$int_invisible_rec=0;
	$int_new_rec=0;
	
	if(!$rs_select->eof())
	{
		$int_visible_rec=$rs_select->fields("total");
	}
	if(!$rs_select1->eof())
	{
		$int_invisible_rec=$rs_select1->fields("total");
	}
	if(!$rs_select2->eof())
	{
		$int_new_rec=$rs_select2->fields("total");
	}
	
	$updatedate="";
	$updatedate=date('Y/m/d');
	
	#update query to update the no. of visible,no. of invisible,no of new rec and lastupdatedate
	$str_query_update = "";
	$str_query_update = "update tr_model_photoalbum set ";
	$str_query_update .= "noofvisiblephoto=".$int_visible_rec;
	$str_query_update .= ",noofinvisiblephoto=".$int_invisible_rec;
	$str_query_update .= ",noofnewphoto=".$int_new_rec;
	$str_query_update .= ",lastupdatedate='". ReplaceQuote($updatedate) ."' where modelpkid=".$int_mpkid." and photoalbumsrno=".$int_pkid;
	ExecuteQuery($str_query_update);	
}

#---------------------------------------------------------------------------------------------------------------------------------
 function PhotoAlbum_WriteXml($int_modelpkid)
{
	$int_mpkid=$int_modelpkid;
	if($int_mpkid<0 || !is_numeric($int_mpkid) || $int_mpkid=="")
	{
		exit();
	}
	
	global $XML_COMMON_PATH;
	global $XML_PHOTOALBUM_ROOT_TAG;
	$str_xml_file_path=$XML_COMMON_PATH.$int_mpkid."/xmlcontentfiles/photoalbum_main.xml";

	$arr1=array();
	$arr2=array();
	$arr3=array();
	$arr4=array();
	$select_query="";
	$select_query="SELECT a.photoalbumsrno,b.photosrno,b.thumbimagefilename,a.photoalbumtitle,a.accesstoall,a.previewtoall,a.photoalbumdescription,";
	$select_query .= "a.createdate,a.lastupdatedate,a.noofview,a.photographertitle,a.photographerurl,a.displayorder,a.displayasnew,a.visible,a.noofvisiblephoto";
	$select_query .= " FROM tr_model_photoalbum a left join tr_model_photoalbum_detail b on a.photoalbumsrno=b.photoalbumsrno";
	$select_query .= " and a.modelpkid=b.modelpkid and b.setasfront='YES' and b.visible='YES' WHERE a.visible='YES' and noofvisiblephoto > 0 and a.modelpkid=".$int_mpkid;
	$select_query .= " ORDER BY a.createdate desc,a.displayorder,photoalbumtitle,photoalbumsrno";
	$rs_list=GetRecordset($select_query);
	$i=0;
	if(!$rs_list->eof())
	{
		while(!$rs_list->eof())
		{
			$arr1[$i]="P_".$rs_list->fields("photoalbumsrno");
			$arr2[$i]="";
			$arr3[$i]="photosrno";
			$arr4[$i]=$rs_list->fields("photosrno");  
			$i=$i+1;

			$arr1[$i]="P_".$rs_list->fields("photoalbumsrno");
			$arr2[$i]="";
			$arr3[$i]="photoalbumsrno";
			$arr4[$i]=$rs_list->fields("photoalbumsrno");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_list->fields("photoalbumsrno");
			$arr2[$i]="";
			$arr3[$i]="title";
			$arr4[$i]=$rs_list->fields("photoalbumtitle");  
			$i=$i+1;
	
			$arr1[$i]="P_".$rs_list->fields("photoalbumsrno");
			$arr2[$i]="";
			$arr3[$i]="description";
			$arr4[$i]=$rs_list->fields("photoalbumdescription");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_list->fields("photoalbumsrno");
			$arr2[$i]="";
			$arr3[$i]="thumbimagefilename";
			$arr4[$i]=$rs_list->fields("thumbimagefilename");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_list->fields("photoalbumsrno");
			$arr2[$i]="";
			$arr3[$i]="accesstoall";
			$arr4[$i]=$rs_list->fields("accesstoall");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_list->fields("photoalbumsrno");
			$arr2[$i]="";
			$arr3[$i]="previewtoall";
			$arr4[$i]=$rs_list->fields("previewtoall");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_list->fields("photoalbumsrno");
			$arr2[$i]="";
			$arr3[$i]="noofview";
			$arr4[$i]=$rs_list->fields("noofview");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_list->fields("photoalbumsrno");
			$arr2[$i]="";
			$arr3[$i]="photographertitle";
			$arr4[$i]=$rs_list->fields("photographertitle");  
			$i=$i+1;
	
			$arr1[$i]="P_".$rs_list->fields("photoalbumsrno");
			$arr2[$i]="";
			$arr3[$i]="photographerurl";
			$arr4[$i]=$rs_list->fields("photographerurl");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_list->fields("photoalbumsrno");
			$arr2[$i]="";
			$arr3[$i]="displayasnew";
			$arr4[$i]=$rs_list->fields("displayasnew");  
			$i=$i+1;
		
			$arr1[$i]="P_".$rs_list->fields("photoalbumsrno");
			$arr2[$i]="";
			$arr3[$i]="noofvisiblephoto";
			$arr4[$i]=$rs_list->fields("noofvisiblephoto");  
			$i=$i+1;
			
			$rs_list->MoveNext();
		}
	}
		writeXmlFile($str_xml_file_path,$XML_PHOTOALBUM_ROOT_TAG,$arr1,$arr2,$arr3,$arr4);
}

#---------------------------------------------------------------------------------------------------------------------------------
function PhotoAlbum_WriteXmlSub($int_modelpkid,$int_pkid)
{
	$int_mpkid=$int_modelpkid;
	if($int_mpkid<0 || !is_numeric($int_mpkid) || $int_mpkid=="")
	{
		exit();
	}
	
	$int_photo_pkid=$int_pkid;
	global $XML_COMMON_PATH;

	$XML_PHOTOALBUM_SUB_FILE_PATH=$XML_COMMON_PATH.$int_mpkid."/xmlcontentfiles/photoalbum_".$int_pkid.".xml";
	$XML_PHOTOALBUM_SUB_ROOT_TAG="PHOTOALBUM_".$int_pkid."_ROOT";
	
	$arr1=array();
	$arr2=array();
	$arr3=array();
	$arr4=array();
	$select_query="";
	$select_query="SELECT photosrno,photoalbumsrno, phototitle, thumbimagefilename, largeimagefilename, displayasnew,noofview";
	$select_query.=" FROM tr_model_photoalbum_detail";
	$select_query.=" WHERE visible = 'YES' AND photoalbumsrno =".$int_photo_pkid." and modelpkid=".$int_mpkid;
	$select_query.=" ORDER BY displayorder desc,photosrno";	
	$rs_list=GetRecordset($select_query);
	$i=0;
	if(!$rs_list->eof())
	{
		while(!$rs_list->eof())
		{
			$arr1[$i]="P_".$rs_list->fields("photosrno");
			$arr2[$i]="";
			$arr3[$i]="photoalbumsrno";
			$arr4[$i]=$rs_list->fields("photoalbumsrno");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_list->fields("photosrno");
			$arr2[$i]="";
			$arr3[$i]="photosrno";
			$arr4[$i]=$rs_list->fields("photosrno");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_list->fields("photosrno");
			$arr2[$i]="";
			$arr3[$i]="recordno";
			$arr4[$i]=$rs_list->RecordNo();  
			$i=$i+1;
			
			
			$arr1[$i]="P_".$rs_list->fields("photosrno");
			$arr2[$i]="";
			$arr3[$i]="phototitle";
			$arr4[$i]=$rs_list->fields("phototitle");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_list->fields("photosrno");
			$arr2[$i]="";
			$arr3[$i]="thumbimagefilename";
			$arr4[$i]=$rs_list->fields("thumbimagefilename");  
			$i=$i+1;

			$arr1[$i]="P_".$rs_list->fields("photosrno");
			$arr2[$i]="";
			$arr3[$i]="largeimagefilename";
			$arr4[$i]=$rs_list->fields("largeimagefilename");  
			$i=$i+1;

			$arr1[$i]="P_".$rs_list->fields("photosrno");
			$arr2[$i]="";
			$arr3[$i]="displayasnew";
			$arr4[$i]=$rs_list->fields("displayasnew");  
			$i=$i+1;
			
			$arr1[$i]="P_".$rs_list->fields("photosrno");
			$arr2[$i]="";
			$arr3[$i]="noofview";
			$arr4[$i]=$rs_list->fields("noofview");  
			$i=$i+1;
			
			$rs_list->MoveNext();
		}

	}	
		writeXmlFile($XML_PHOTOALBUM_SUB_FILE_PATH,$XML_PHOTOALBUM_SUB_ROOT_TAG,$arr1,$arr2,$arr3,$arr4);
}

#---------------------------------------------------------------------------------------------------------------------------------
	function Video_Synchronize($mdlpkid)
	{
		$int_mdlkid=$mdlpkid;
		if($int_mdlkid<0 || !is_numeric($int_mdlkid) || $int_mdlkid=="")
		{
			exit();
		}
		#select query to get details of no of statistic from tr_model_resume table
		$str_select="";
		$str_select="SELECT count(videopkid) total FROM tr_model_video where modelpkid=".$int_mdlkid;
		$rs_select=GetRecordset($str_select);
		$int_video_count=0;
		if(!$rs_select->eof())
		{
			$int_video_count=$rs_select->fields("total");
		}
		$updatedate="";
		$updatedate=date('Y/m/d');
		#update query to update the no. of resume
		$str_query_update = "";
		$str_query_update = "update t_model ";
		$str_query_update .= "set noofvideo =".$int_video_count .", ";
		$str_query_update .= "lastupdatedate='". ReplaceQuote($updatedate) ."' ";
		$str_query_update .= "Where modelpkid=".$int_mdlkid;
		ExecuteQuery($str_query_update);	
	}

#---------------------------------------------------------------------------------------------------------------------------------
 function Video_WriteXml($int_modelpkid)
{
	$int_mpkid=$int_modelpkid;
	if($int_mpkid<0 || !is_numeric($int_mpkid) || $int_mpkid=="")
	{
		exit();
	}
	
	global $XML_COMMON_PATH;
	global $XML_VIDEO_ROOT_TAG;
	$str_xml_file_path=$XML_COMMON_PATH.$int_mpkid."/xmlcontentfiles/video_".$int_mpkid.".xml";

	//global $XML_VIDEO_FILE_PATH;
	//global $XML_VIDEO_ROOT_TAG;	
	
	$arr1=array();
	$arr2=array();
	$arr3=array();
	$arr4=array();

	$str_query_select = "SELECT a.modelpkid,a.videopkid,a.title,a.description,a.displayonhome,a.createddate,a.lastupdatedate,a.extratext,a.photofilename,a.previewtoall,a.accesstoall,";
	$str_query_select .= "a.displayasnew,a.displayashot,b.videoclippkid,b.cliptitle,b.videofilename,b.noofdownload ";
	$str_query_select .= "FROM tr_model_video a LEFT JOIN tr_model_video_clip b ON a.videopkid=b.videopkid AND b.visible='YES' ";
	$str_query_select .= "WHERE a.visible='YES' and a.modelpkid=".$int_mpkid;
	$str_query_select .= " ORDER BY a.createddate desc,a.displayorder,a.title,b.displayorder,b.cliptitle";	
	
	//print $str_query_select; exit;
	$rs_xml_list=GetRecordset($str_query_select);
	
	$i=0;
	while(!$rs_xml_list->eof())
	{
		
		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="videopkid";
		$arr4[$i]=$rs_xml_list->fields("videopkid");  
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="title";
		$arr4[$i]=$rs_xml_list->fields("title");
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="desc";
		$arr4[$i]=$rs_xml_list->fields("description");
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="extra";
		$arr4[$i]=$rs_xml_list->fields("extratext");
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="photo";
		$arr4[$i]=$rs_xml_list->fields("photofilename");
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="createddate";
		$arr4[$i]=$rs_xml_list->fields("createddate");
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="lastdate";
		$arr4[$i]=$rs_xml_list->fields("lastupdatedate");
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="previewtoall";
		$arr4[$i]=$rs_xml_list->fields("previewtoall");
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="accesstoall";
		$arr4[$i]=$rs_xml_list->fields("accesstoall");
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="displayonhome";
		$arr4[$i]=$rs_xml_list->fields("displayonhome");
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="new";
		$arr4[$i]=$rs_xml_list->fields("displayasnew");
		$i=$i+1;

		$arr1[$i]="P_".$rs_xml_list->fields("videopkid");
		$arr2[$i]="";
		$arr3[$i]="hot";
		$arr4[$i]=$rs_xml_list->fields("displayashot");
		$i=$i+1;
		
		$int_old_pkid=$rs_xml_list->fields("videopkid");
		$int_new_pkid=$rs_xml_list->fields("videopkid");
		
		$str_video_clip="";
		while($int_old_pkid==$int_new_pkid&& $rs_xml_list->eof()==false)
		{				
			if($rs_xml_list->fields("cliptitle")!="" && $rs_xml_list->fields("videoclippkid")!=0 && $rs_xml_list->fields("videoclippkid")!="")
			{			
				$str_video_clip.=$rs_xml_list->fields("videoclippkid")."|;|";
				$str_video_clip.=$rs_xml_list->fields("cliptitle")."|;|";
				$str_video_clip.=$rs_xml_list->fields("videofilename")."|;|";
				$str_video_clip.=$rs_xml_list->fields("noofdownload")."||;||";
			}
							
			$rs_xml_list->MoveNext();								
			$int_new_pkid=$rs_xml_list->fields("videopkid");
		}
		
		$arr1[$i]="P_".$int_old_pkid;
		$arr2[$i]="";
		$arr3[$i]="clip";
		$arr4[$i]=$str_video_clip;
		$i=$i+1;
	}
	writeXmlFile($str_xml_file_path,$XML_VIDEO_ROOT_TAG,$arr1,$arr2,$arr3,$arr4);	
}

#--------------------------------------------------------------------------------------------------------
function Testimonial_WriteXml()
{
	return;
	
	global $XML_TESTIMONIAL_FILE_PATH;
	global $XML_TESTIMONIAL_ROOT_TAG;
	
	$arr1=array();
	$arr2=array();
	$arr3=array();
	$arr4=array();
	$select_query="";
	$select_query="SELECT * from t_testimonial where visible='YES' order by displayorder";
	$rs_list=GetRecordset($select_query);
	$i=0;
	while(!$rs_list->eof())
	{
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="personname";
		$arr4[$i]=$rs_list->fields("personname");  
		$i=$i+1;

		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="designation";
		$arr4[$i]=$rs_list->fields("designation");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="emailid";
		$arr4[$i]=$rs_list->fields("emailid");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="location";
		$arr4[$i]=$rs_list->fields("location");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="description";
		$arr4[$i]=$rs_list->fields("description");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="imagefilename";
		$arr4[$i]=$rs_list->fields("imagefilename");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="urltitle";
		$arr4[$i]=$rs_list->fields("urltitle");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="url";
		$arr4[$i]=$rs_list->fields("url");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="openinnewwindow";
		$arr4[$i]=$rs_list->fields("openinnewwindow");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="displayasnew";
		$arr4[$i]=$rs_list->fields("displayasnew");  
		$i=$i+1;
		
		$rs_list->MoveNext();
	}
	writeXmlFile($XML_TESTIMONIAL_FILE_PATH,$XML_TESTIMONIAL_ROOT_TAG,$arr1,$arr2,$arr3,$arr4);
}

#--------------------------------------------------------------------------------------------------------
# Used to return string of category-subcategory...
function getcategorylist($int_productpkid,$rs_item_list)
{
	$str_category_info="";
	$int_parent="";
	while($int_productpkid==$rs_item_list->Fields("productpkid"))
	{
		if($int_parent!=$rs_item_list->Fields("pcategorypkid"))
		{
			$int_parent=$rs_item_list->Fields("pcategorypkid");
			if($str_category_info=="")
			{
				$str_category_info="<b>".MyHtmlEncode($rs_item_list->Fields("pcategoryname"))."</b> - ".MyHtmlEncode($rs_item_list->Fields("psubcategoryname"));	
			}
			else
			{
				$str_category_info.=" , <b>".MyHtmlEncode($rs_item_list->Fields("pcategoryname"))."</b> - ".MyHtmlEncode($rs_item_list->Fields("psubcategoryname"));	
			}
		}
		else
		{		
			$str_category_info.=" , ".MyHtmlEncode($rs_item_list->Fields("psubcategoryname"));	
		}
		$rs_item_list->MoveNext();
	}
	print($str_category_info);
	return $rs_item_list;
}

#---------------------------------------------------------------------------------------------------------------------------------
	function Product_Comment_Synchronize($productpkid)
	{
		$int_productpkid=$productpkid;
		if($int_productpkid<0 || !is_numeric($int_productpkid) || $int_productpkid=="")
		{
			exit();
		}
		#select query to get details of no of statistic from tr_modelsearch_comment table
		$str_select="";
		$str_select="SELECT count(testimonialpkid) total FROM tr_product_comment WHERE productpkid=".$int_productpkid;
		$rs_select=GetRecordset($str_select);
		$int_comment_count=0;
		if(!$rs_select->eof())
		{
			$int_comment_count=$rs_select->fields("total");
		}
		$updatedate="";
		$updatedate=date('Y/m/d');
		#update query to update the no. of comment
		$str_query_update = "";
		$str_query_update = "UPDATE tr_product ";
		$str_query_update .= "SET noofcomment =".$int_comment_count .", ";
		$str_query_update .= "lastupdatedate='". ReplaceQuote($updatedate) ."' ";
		$str_query_update .= "WHERE productpkid=".$int_productpkid;
		ExecuteQuery($str_query_update);	
	}

#---------------------------------------------------------------------------------------------------------------------------------
	function Comment_Synchronize($mdlpkid)
	{
		$int_mdlkid=$mdlpkid;
		if($int_mdlkid<0 || !is_numeric($int_mdlkid) || $int_mdlkid=="")
		{
			exit();
		}
		#select query to get details of no of statistic from tr_modelsearch_comment table
		$str_select="";
		$str_select="SELECT count(testimonialpkid) total FROM tr_modelsearch_comment where modelpkid=".$int_mdlkid;
		$rs_select=GetRecordset($str_select);
		$int_comment_count=0;
		if(!$rs_select->eof())
		{
			$int_comment_count=$rs_select->fields("total");
		}
		$updatedate="";
		$updatedate=date('Y/m/d');
		#update query to update the no. of comment
		$str_query_update = "";
		$str_query_update = "UPDATE t_model ";
		$str_query_update .= "SET noofcomment =".$int_comment_count .", ";
		$str_query_update .= "lastupdatedate='". ReplaceQuote($updatedate) ."' ";
		$str_query_update .= "WHERE modelpkid=".$int_mdlkid;
		ExecuteQuery($str_query_update);	
	}

#---------------------------------------------------------------------------------------------------------------------------------
function Comment_WriteXml($int_modelpkid)
{
	$int_mpkid=$int_modelpkid;
	if($int_mpkid<0 || !is_numeric($int_mpkid) || $int_mpkid=="")
	{
		exit();
	}
	
	global $XML_COMMON_PATH;
	global $XML_TESTIMONIAL_ROOT_TAG;
	$str_xml_file_path=$XML_COMMON_PATH.$int_mpkid."/xmlcontentfiles/comment.xml";
	
	$arr1=array();
	$arr2=array();
	$arr3=array();
	$arr4=array();
	$select_query="";
	$select_query="SELECT * FROM tr_modelsearch_comment WHERE visible='YES' AND approved='YES' ORDER BY displayorder";
	$rs_list=GetRecordset($select_query);
	$i=0;
	while(!$rs_list->eof())
	{
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="personname";
		$arr4[$i]=$rs_list->fields("personname");  
		$i=$i+1;

		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="designation";
		$arr4[$i]=$rs_list->fields("designation");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="emailid";
		$arr4[$i]=$rs_list->fields("emailid");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="location";
		$arr4[$i]=$rs_list->fields("location");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="description";
		$arr4[$i]=$rs_list->fields("description");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="imagefilename";
		$arr4[$i]=$rs_list->fields("imagefilename");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="urltitle";
		$arr4[$i]=$rs_list->fields("urltitle");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="url";
		$arr4[$i]=$rs_list->fields("url");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="openinnewwindow";
		$arr4[$i]=$rs_list->fields("openinnewwindow");  
		$i=$i+1;
		
		$arr1[$i]="P_".$rs_list->fields("testimonialpkid");
		$arr2[$i]="";
		$arr3[$i]="displayasnew";
		$arr4[$i]=$rs_list->fields("displayasnew");  
		$i=$i+1;
		
		$rs_list->MoveNext();
	}
	writeXmlFile($str_xml_file_path,$XML_TESTIMONIAL_ROOT_TAG,$arr1,$arr2,$arr3,$arr4);
}

#--------------------------------------------------------------------------------------------------------
?>