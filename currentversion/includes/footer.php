<?php
$fp=openXMLfile($STR_XML_FILE_PATH_CMS."footer_cms.xml");
$str_footer_desc=getTagValue("ITEMKEYVALUE_DESCRIPTION",$fp);
$str_footer_banner_code=getTagValue("ITEMKEYVALUE_BANNERCODE",$fp);
//$str_footer_google_analytics_code=getTagValue("ITEMKEYVALUE_TOPBANNERCODE",$fp_keyword);
//$str_footer_noofview=getTagValue("ITEMKEYVALUE_NOOFVIEW",$fp_keyword);
//$str_footer_noofview_visible=getTagValue("ITEMKEYVALUE_NOOFVIEW_VISIBLE",$fp_keyword);
closeXMLfile($fp);

# No. of views from table
//$str_query_select = "";
//$str_query_select = "SELECT noofview FROM cms_footer";
//$rs_list_footer = GetRecordSet($str_query_select);
?>
<footer>
        <div class="footer">
        <div class="container">
            <div class="row padding-10">
                  <div class="col-md-2 col-sm-2 col-xs-6" >
                        <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/" title="" class="footer-link">Home</a></p>
                        <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/about" title="" class="footer-link">About Us</a></p>
                        <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/contact" title="" class="footer-link">Contact Us</a></p>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6" >
                        <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/faq" title="" class="footer-link">FAQ</a></p>
                        <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/privacy" title="" class="footer-link">Privacy</a></p>
                        <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/terms" title="" class="footer-link">Terms</a></p>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-6" >
                        <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_order_status_list.php" title="" class="footer-link">Track Order</a></p>
                        <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_order_status_list.php" title="" class="footer-link">Cancel Order</a></p>
                        <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/return-policy" title="" class="footer-link">Return Policy</a>
                </div>
                

                <div class="col-md-3 col-sm-3 col-xs-12" >
                    
<!--                    <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/coupons" title="" class="footer-link">Darpani Coupons</a></p>
                    <p><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/offers" title="" class="footer-link">Darpani Offers</a></p>-->
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12" >
		    <?php #------------------------------ START - Home Content 3 --------------------------------------------------?>
		    <?php
    		    $arr_xml_list_home_content3="";
		    $arr_xml_list_home_content3=readXml($STR_XML_FILE_PATH_MODULE."homecontent3.xml","ROOT_ITEM");
		    $arr_home_content_cnt3 = (count($arr_xml_list_home_content3));
		    $arr_test1 = array_keys($arr_xml_list_home_content3);
		    if($arr_test1[0]!="ROOT_ITEM") {  ?>
		    
			    <div class="row padding-10">
			       <?php 

				if($arr_test1[0]!="ROOT_ITEM") { 
				    $i=1;
				    $int_arr_lim = 0;?>
				     
				  <?php  foreach($arr_xml_list_home_content3 as $key => $val) {  
				        $int_arr_lim++;

				        if(is_array($val)) { 
				            ?>
				            <?php /* ?><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" align="center"><?php */ ?>
					    <div >
				                    <?php if($arr_xml_list_home_content3[$key]["IMAGEFILENAME"] != "") { ?>
				                        <img src="<?php print($str_img_path_home2.$arr_xml_list_home_content3[$key]["IMAGEFILENAME"]);?>" class="img-responsive" alt="" title=""/>
				                    <?php } ?>
				                    <?php if($arr_xml_list_home_content3[$key]["TITLE"] != "") { ?>
				                        <h3><?php  print($arr_xml_list_home_content3[$key]["TITLE"]);?></h3>
				                    <?php } ?>
				                    <?php if($arr_xml_list_home_content3[$key]["DESCRIPTION"] != "") { ?>
				                        <p align="justify">
				                            <?php  print($arr_xml_list_home_content3[$key]["DESCRIPTION"]);?>
				                        </p>
				                    <?php } ?>
				            </div>
				            
				        <?php  }
				    
				        //if($int_arr_lim % 1 == 0) { print "</div><div class='row padding-10'>"; }
				        if($int_arr_lim % 1 == 0) { print "</div><div class=''>"; }  
				    } ?>
				 </div>
			      <?php  } ?>
		     
		    <?php } ?>
		    <?php #------------------------------ END - Home Content 3 --------------------------------------------------- ?>
                        
                </div>
            </div>
        </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row padding-10">
                    <div class="col-md-12" align="center">
                        <p class=""><?php print $str_footer_desc; ?></p>
			<?php if($str_footer_banner_code !="") { ?><p class=""><?php print $str_footer_banner_code; ?></p><?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/functions.js" type="text/JavaScript"></script>
<script language="JavaScript" src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/js/jqBootstrapValidation.js"></script>
<?php /* ?><script language="JavaScript" src="./js/jquery.min.js"></script>
<script language="JavaScript" src="./js/bootstrap.min.js"></script>
<script language="JavaScript" src="./user/newsletter.js" type="text/JavaScript"></script><?php */ ?>
<script language="JavaScript" type="text/javascript">
function frm_newletter_Validate_Email()
{
	
//    with (document.frm_newletter)
//    {
//        if (email.value=="")
//        {
//            alert("Please enter email address");
//            email.focus();
//            return false;			
//        }
//        if (!sValidateMailAddress(email.value))
//        {
//            alert("Please enter valid email address. Format: emailid@domainname.com");
//            email.select();
//            email.focus();
//            return false;
//        }
//    }
//    return true;
}
</script>

<!--top top bottom arrow-->
<script>
$(document).ready(function(){ 
    $(window).scroll(function(){ 
        if ($(this).scrollTop() > 100) { 
            $('#scroll').fadeIn(); 
        } else { 
            $('#scroll').fadeOut(); 
        } 
    }); 
    $('#scroll').click(function(){ 
        $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }); 
});
</script>
<script type="text/javascript">
        $( document ).ready(function() {
            $('#load_cartitems').load('<?php print($STR_SITENAME_WITH_PROTOCOL);?>/user/product_cart_count_p.php');
            $('#mobile-cart-count').load('<?php print($STR_SITENAME_WITH_PROTOCOL);?>/user/product_cart_count_p.php');
        });
	var auto_refresh = setInterval(
	function ()
	{
            $('#load_cartitems').load('<?php print($STR_SITENAME_WITH_PROTOCOL);?>/user/product_cart_count_p.php').fadeIn("slow");
            $('#mobile-cart-count').load('<?php print($STR_SITENAME_WITH_PROTOCOL);?>/user/product_cart_count_p.php').fadeIn("slow");
	}, 1000); // refresh every 10000 milliseconds
        
    </script>
<!--end top top bottom arrow-->

<?php 
// This will delete old added data into cart
$str_query_delete = "DELETE FROM t_store_sessioncart WHERE sentdate < NOW() - INTERVAL 7 DAY";
ExecuteQuery($str_query_delete);
?>
