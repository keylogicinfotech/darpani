<?php
#-------------------------------------------------------------------------------------------------------
#function to Write XML File
	function writeXmlFile($file_path,$rootnode,$arr1,$arr2,$arr3="",$arr4="")
	{
		$strtagname="";
		$strtagvalue="";
		$arrtagname=array();
		$arrtagvalue=array();
						
		$xml_string="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";
		$xml_string.="<".strtoupper($rootnode).">\r\n";
		
		$i=0;
		while($i < count($arr1))
		{
			if(is_array($arr3)==false && is_array($arr4)==false)
			{
				$strtagname=$arr1[$i];
				$strtagvalue=$arr2[$i];
				$i++;
				$xml_string.="<".strtoupper($strtagname)."><![CDATA[".$strtagvalue."]]></".strtoupper($strtagname).">\r\n";
			}
			else
			{
				if($arr3[$i]=="" && $arr4[$i]=="")
				{
					$strtagname=$arr1[$i];
					$strtagvalue=$arr2[$i];
					$i++;
					$xml_string.="<".strtoupper($strtagname)."><![CDATA[".$strtagvalue."]]></".strtoupper($strtagname).">\r\n";
				}
				else
				{
					$strtagname=$arr1[$i];
					$current_tag=$arr1[$i];
					$previous_tag=$arr1[$i];
					$xml_string.="<".strtoupper($current_tag).">\r\n";
					while($current_tag==$previous_tag && $i < count($arr1))
					{
						$strtagname=$arr3[$i];
						$strtagvalue=$arr4[$i];
						$i++;
						$xml_string.="  <".strtoupper($strtagname)."><![CDATA[".$strtagvalue."]]></".strtoupper($strtagname).">\r\n";
						if($i < count($arr1) )
						{
							$previous_tag=$arr1[$i];
						}
					}
					$xml_string.="</".strtoupper($current_tag).">\r\n";
				}
			}
		}
		
		$xml_string.="</".strtoupper($rootnode).">";
		$file=fopen($file_path,"w")
				or die("Error in creating file!!!");
		fwrite($file,$xml_string);
		closeXmlfile($file);
		return true;
	}
#-------------------------------------------------------------------------------------------------------
#function to open xml file in read mode.
	function openXmlFile($file_path)
	{
		$fp="";
		if(file_exists($file_path))
		{
			$fp = fopen($file_path,"r")
				or die("Error in opening file!!!");
		}
		return($fp);
	}
	
#-------------------------------------------------------------------------------------------------------
#function to close xml file.
	function closeXmlfile($fp)
	{
		if($fp != "")
		{
			fclose($fp);
		}
	}
#-------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------
#function to read value of tag from XML file.
#Return Value:	String which contains value of tag.
#Parameters:	$str_tagname: name of XML TAG whose value will be returned.
#				$fp: File pointer of XML file.

	function getTagValue($str_tagname,$fp)
	{		
		if($fp != "")
		{
		global $tagname,$tagvalue,$finalval;
		$tagname=strtoupper($str_tagname);
	
		$tag = "";	
		$tagvalue="";		
		$finalval="";
		
		// Create an XML parser
		$xml_parser = xml_parser_create("UTF-8");
		
		// Set the functions to handle opening and closing tags 
		xml_set_element_handler($xml_parser, "startElement", "endElement");
		
		// Set the function to handle blocks of character data
		xml_set_character_data_handler($xml_parser, "characterData");
		
		// Read the XML file 4KB at a time

			rewind($fp);
			while ($data = fread($fp, 4096))
				{
				// Parse each 4KB chunk with the XML parser created above 				
				xml_parse($xml_parser,$data, feof($fp));
				}
		
			// Free up memory used by the XML parser
			xml_parser_free($xml_parser);
		
			return(trim($finalval));
		}
	}
	
	#---------------------------------------------
	# Parser functions for reading XML file.
	#		these parser functions will be called by parser during reading of XML file.

	#function clled when parser encounters starting of tag in XML file
	function startElement($parser, $name) 
	{
		global $tag;
		$tag = $name;
	}
	
	#when tag ends encountered.
	function endElement($parser, $name) 
	{	
		global $tag,$finalval,$tagvalue;
		$tag = "";
		if($tagvalue!=="" && $finalval == "")
		{
			$finalval = $tagvalue;
		}
		$tagvalue="";
	}
	
	#to read data between tags.
	function characterData($parser, $data) 
	{
		global $tag, $tagvalue,$tagname;
			if($tag==$tagname) 
			{
				$tagvalue = $tagvalue.$data;
			}
	}	
#-------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------
#Function to read attribute value

	function getAttributeValue($str_tagname,$str_att,$fp)
	{		
		if($fp != "")
		{
		global $tagname1,$attribute,$finalattvalue;
		$tagname1=strtoupper($str_tagname);
		$attribute=strtoupper($str_att);
		$finalattvalue="";
				
		// Create an XML parser
		$xml_parser = xml_parser_create();
		
		// Set the functions to handle opening and closing tags 
		xml_set_element_handler($xml_parser, "startElementAtt", "endElementAtt");
		
		// Set the function to handle blocks of character data
		xml_set_character_data_handler($xml_parser, "attributeData");
		
		// Read the XML file 4KB at a time

			rewind($fp);
			while ($data = fread($fp, 4096))
				{
				// Parse each 4KB chunk with the XML parser created above 				
				xml_parse($xml_parser,$data, feof($fp));
				}
		
			// Free up memory used by the XML parser
			xml_parser_free($xml_parser);
		
			return($finalattvalue);
		}
	}

	#function clled when parser encounters starting of tag in XML file
	function startElementAtt($parser, $name, $attr) 
	{
		global $attribute;
		global $finalattvalue;
		global $attributevalue;
		global $attributename;
		global $tagname1;
		
		if($name==$tagname1)
		{
			if(sizeof($attr))
			{
				if($attribute != "")
				{
					while(list($attributename,$attributevalue)=each($attr))
					{
						if($attribute==$attributename)
						{
								$finalattvalue=$attributevalue;
						}
					}
				}
				else
				{
					$finalattvalue=array();
					$i=0;
					while(list($attributename,$attributevalue)=each($attr))
					{
						$finalattvalue[$i][0]=$attributename;
						$finalattvalue[$i][1]=$attributevalue;
						$i=$i + 1;
					}
				}
			}
		}
	}
	
	#when tag ends encountered.
	function endElementAtt($parser, $name) 
	{		
	}
	
	#to read data between tags.
	function attributeData($parser, $data) 
	{	
	}
#------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------
#Function to read list of tags/subtags' values, and attributes value.
#Return Value:	function will return array(one & two dimensions mixed) of tag/attribute values.
#Parameters:	$filename: name of the XML file from which data will be read.
#				$tagname: name of the XML TAG whose value will be returned.
#				$option(Default 0): set it 1 for reading attribute value. 

function readXml($filename,$tagname,$option=0) 
{
	$tag=trim($tagname);
	//$result="";  // For below PHP 7.1 version
	$result=array(); // For PHP 7.1 version and above
	$att_result="";
		
	// check weather the XML file exists or not
	if(!file_exists($filename) || trim($tagname)=="")
	{
		$result[0]="";
		return($result);
	}
	
    // read the XML file
    $data = implode("", file($filename));
	
	//create xml parser
    $parser = xml_parser_create("UTF-8");
	
	//setting options
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 0);
	
	//getting all xml data into array structure
    xml_parse_into_struct($parser, $data, $values, $tags);
	
	// Free up memory used by the XML parser
    xml_parser_free($parser);

	#-----------------------------------
	#starting parsing array returned by parser 
	$i=0;
	$level=0;
	$level_name= "";
	
	while($i < count($values) && $tag != $values[$i]["tag"]) #till specified tag found in array.
	{
		$i=$i+1;
	}
	while($i < count($values))	#till end of the specified tag.
	{		
		#COMPLETE type
		//print $values[$i]["type"]; exit;
		if($values[$i]["type"]=="complete")
		{
		
			if($level==0) ##if tag has no subtags return value part of main tag.
			{
				if(array_key_exists("value",$values[$i]))
				{
					$result[$values[$i]["tag"]]=$values[$i]["value"];
				}				
				break;
			}
			else if($level==1)#get value of subtag.
			{
				if(array_key_exists("value",$values[$i]))
				{
					$result[$values[$i]["tag"]]=$values[$i]["value"];					
				}
				else
				{
					$result[$values[$i]["tag"]]="";
				}
			}
			else if($level==2)
			{
				if(trim($level_name)!="")
				{
					if(array_key_exists("value",$values[$i]))
					{
						$result[$level_name][$values[$i]["tag"]]=$values[$i]["value"];					
					}
					else
					{
						$result[$level_name][$values[$i]["tag"]]="";
					}				
				}
			}
		}
		
		#OPEN type
		if($values[$i]["type"]=="open")
		{
			if($level==0)
			{
				//print "HELLO"; exit;
				if(array_key_exists("attributes",$values[$i])) #if parent tag has attribute list.
				{
					//print "HELLO"; exit;
					$att_result=array();
					while (list($attkey, $attval) = each($values[$i]["attributes"])) 
					{
						$att_result[$attkey] = $attval;						
					}
				}
			}
			if($option==1) #when option set to 1, attribut values array will be returned.
			{
				if($att_result=="")
				{
					$att_result[0]="";
				}
				return($att_result);
			}
			
			$level=$level+1;
			$level_name= $values[$i]["tag"];			
		}
		
		#CLOSE type
		if($values[$i]["type"]=="close")
		{
			$level=$level-1;
			$level_name= "";	
			if($level==0) #if parent tag is closed, terminate the loop.
			{
				break;
			}
		}
		
		$i=$i+1;
	}
	
	#return attributes value if option set to 1.
	if($option==1)
	{
		if($att_result=="")
		{
			$att_result[0]="";
		}
		return($att_result);
	}
	
	#By default - return tag values
	if($result=="")
	{
		$result[0]="";
	}
	return($result);
}
#------------------------------------------------------------------------------------------------------
?>