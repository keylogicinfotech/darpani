<?php
#-------------------------------------------------------   
/// variables used to Set Running Mode of Application
//$APP_RUN_MODE = "LOCAL"; 
$APP_RUN_MODE = "LOCAL";
#-------------------------------------------------------   
// Script timeout in seconds. i.e. Maximum Seconds script will do processing and after that it will show timeout message.
set_time_limit(150);
#-------------------------------------------------------   
if ($APP_RUN_MODE == "LOCAL") {
    $STR_BACKPROCESS_URL = "./advertise_purchase_payment_local.php";
    $STR_BACKPROCESS_URL_STORE = "./store_purchase_payment_local.php";
    $STR_BACKPROCESS_URL_PPD = "./ppd_purchase_payment_local.php";
    $STR_SITENAME = "localhost/60004darpani/currentversion";
    $STR_SITENAME_WITHOUT_PROTOCOL = "localhost/60004darpani/currentversion";
    $STR_SITENAME_WITH_PROTOCOL = "http://localhost/60004darpani/currentversion";
} else {
    $STR_BACKPROCESS_URL = "https://bill.ccbill.com/jpost/signup.cgi";
    $STR_BACKPROCESS_URL_STORE = "https://bill.ccbill.com/jpost/signup.cgi";
    $STR_BACKPROCESS_URL_PPD = "https://bill.ccbill.com/jpost/signup.cgi";
    $STR_SITENAME = "darpani.com";
    $STR_SITENAME_WITHOUT_PROTOCOL = "darpani.com";
    $STR_SITENAME_WITH_PROTOCOL = "https://darpani.com";
    date_default_timezone_set('Asia/Kolkata');
}
$STR_SITE_URL = "www.darpani.com";
$STR_SITE_TITLE = "Darpani";
$STR_SITE_NAME_WITH_FTP_PROTOCOL = "ftp.darpani.com";



$STR_COMPANY_GST = "245896328569";
$STR_COMPANY_POSTAL_ADDRESS01 = "UG-9, La Citadel, Near Kangaroo Circle, Magob, ";
$STR_COMPANY_POSTAL_ADDRESS02 = "Surat, Gujarat, India, Pin Code-395010, ";
$STR_COMPANY_FAX_NUMBER = "FAX : 0261 1234567";
$STR_COMPANY_PHONE_NUMBER = "Phone : 0261 1234567";
$STR_COMPANY_MOBILE_NUMBER = " 9876543210";
$STR_COMPANY_EMAIL_ADDRESS = " darpanionline@gmail.com";
$STR_COMPANY_WORKING_DAYS_HOURS = "11 am to 6 pm, Mon-Sat, Sunday Closed, <br/>ONLY WhatsApp: 81540 44411 ( <b>No Call Please</b> )";
#-------------------------------------------------------
$STR_ENCRYPTION_KEY = "60004darpani";
#------------------------------------------------------- Payment Gateway
$STR_PAYU_MERCHANT_KEY = "YYI9So8b";
$STR_PAYU_MERCHANT_SALT = "l5d35I98ys";
//$STR_PAYU_BASE_URL = "https://sandboxsecure.payu.in";	// For Sandbox Mode
$STR_PAYU_BASE_URL = "https://secure.payu.in";    // For Production Mode
$STR_PAYU_URL_SUCCESS = "https://darpani.com/user/product_payment_success_payu_p.php";          // User will go to this page after successful payment
$STR_PAYU_URL_FAILURE = "https://darpani.com";          // User will go to this page after fail payment


#------------------------------------------------------- CCBILL Payment Gateway - VIP Members
$STR_CCBILL_MEMBER_CLIENT_ACCOUNT = "000000-0xxx";
$STR_CCBILL_MEMBER_CLIENT_ACCOUNT_NUMBER = "000000";
$STR_CCBILL_MEMBER_CLIENT_SUB_ACCOUNT_NUMBER = "0xxx";
$STR_CCBILL_MEMBER_FORM_NAME_CREDIT_CARD = "211cc";
$STR_CCBILL_MEMBER_CHECK_CURRENCIES = "840";
$STR_CCBILL_MEMBER_FORM_NAME_CHECK = "211ck";
$STR_CCBILL_MEMBER_FORM_NAME_PHONE = "";
$STR_CCBILL_MEMBER_ALLOWEDTYPES = "0000004918:840";
$STR_CCBILL_MEMBER_SUBSCRIPTIONTYPEID = "0000004918:840";
$STR_CCBILL_MEMBER_LANGUAGE = "English";
$STR_CCBILL_MEMBER_EU_DEBIT_CURRENCIES = "978";
$STR_CCBILL_MEMBER_EU_DEBIT_ALLOWEDTYPES = "0000868242:978,0000002160:978,0000000150:978";
$STR_CCBILL_MEMBER_EU_DEBIT_SUBSCRIPTIONTYPEID = "0000868242:978";
$STR_CCBILL_MEMBER_DIRECT_PAY_EU_FORM_NAME = "201dp";
$STR_CCBILL_MEMBER_DIRECT_PAY_EU_CURRENCIES = "978";
$STR_CCBILL_MEMBER_DIRECT_PAY_EU_ALLOWEDTYPES = "0000000150:978";
$STR_CCBILL_MEMBER_DIRECT_PAY_EU_SUBSCRIPTIONTYPEID = "0000000150:978";
$STR_CCBILL_MEMBER_WEBMASTER_CLIENT_ACCOUNT = "000000-0xxx";
#------------------------------------------------------- CCBILL Payment Gateway - Online Store
$STR_CCBILL_STORE_CLIENT_ACCOUNT = "000000-yyyy";
$STR_CCBILL_STORE_CLIENT_ACCOUNT_NUMBER = "000000";
$STR_CCBILL_STORE_CLIENT_SUB_ACCOUNT_NUMBER = "yyyy";
$STR_CCBILL_STORE_FORM_NAME_CREDIT_CARD = "211cc";
$STR_CCBILL_STORE_FORM_NAME = "31t9";
$STR_CCBILL_STORE_OPEN_PRICE = 149.99;
$STR_CCBILL_STORE_SALT_VALUE = "";
$STR_CCBILL_STORE_DOWNLOAD_LIMIT_DAY = 3;
$STR_CCBILL_STORE_CHECK_CURRENCIES = "840";
#------------------------------------------------------- CCBILL Payment Gateway - LIVE Cam
$STR_CCBILL_PHONE_CLIENT_ACCOUNT = "000000-0zzz";
$STR_CCBILL_PHONE_CLIENT_ACCOUNT_NUMBER = "000000";
$STR_CCBILL_PHONE_CLIENT_SUB_ACCOUNT_NUMBER = "0zzz";
$STR_CCBILL_PHONE_FORM_NAME_CREDIT_CARD = "211cc";
$STR_CCBILL_PHONE_FORM_NAME_CHECK = "211ck";
$STR_CCBILL_PHONE_FORM_NAME_PHONE = "";
$STR_CCBILL_PHONE_ALLOWEDTYPES = "0000006984:840,0000016778:840,0000003603:840,0000020629:840,0000002580:840,0000032604:840,0000014099:840,0000004673:840,0000037435:840,0000016760:840,0000516472:840,0001161460:840,0000643917:840,0000667036:840";
$STR_CCBILL_PHONE_SUBSCRIPTIONTYPEID = "0000006984:840";
$STR_PHONE_HOST_NAME = "ModelName";
$STR_PHONE_CAM_FORWARD_PAGE = "videochat_room.php?host_name=" . $STR_PHONE_HOST_NAME . "&mode_request=1";
#------------------------------------------------------- CCBILL Payment Gateway - PayPerCall.com
$STR_PPC_LOGINID = "";
$STR_PPC_LOGINID_FOR_WEB_CALL_BUTTON = "";
$STR_PPC_PASSWORD = "";
$STR_PPC_API_KEY = "";
$STR_PPC_CALL_INBOUND_CHARGE = 0.10;
$STR_PPC_CALL_OUTBOUND_CHARGE = 0.20;
$STR_PPC_CALL_MODEL_SPLIT_PERCENT = 0.70;
$STR_PPC_CALL_ADMIN_SPLIT_PERCENT = 0.30;
#------------------------------------------------------- FORM related messages
$STR_TITLE_GO_TO = "Go To";
$STR_TITLE_PROFILE = "Click to view this profile";
$STR_TITLE_CMS = "CMS";
$STR_TITLE_ADD = "<i class='fa fa-plus'></i>&nbsp;Click To Add New Details";
$STR_TITLE_EDIT = "Edit Details";
$STR_TITLE_FILTER_CRITERIA = "<i class='fa fa-filter'></i>&nbsp;Filter Criteria";
$STR_TITLE_DOWNLOAD = "<i class='fa fa-download'></i>&nbsp;Click To Download Details";
$STR_TITLE_UPLOAD = "<i class='fa fa-upload'></i>&nbsp;Click To Upload Details";
$STR_TITLE_DOWNLOAD_FILE = "<i class='fa fa-download'></i>&nbsp;Click To Download File";
$STR_TITLE_SEND_EMAIL = "<i class='fa fa-paper-plane'></i>&nbsp;Click To Send Email";
$STR_TITLE_DATE_CREATED = "created on ";
$STR_TITLE_DATE_UPDATED = "last updated on ";
$STR_TITLE_DATE_REGISTERED = "Register On ";
$STR_TITLE_DATE_LAST_LOGIN = "Last Login On ";

$STR_HOVER_VIEW = "Click To View Details";
$STR_HOVER_FAVORITE = "Click To Favorite This Detail";
$STR_HOVER_VIEW_URL = "Click To View Website";
$STR_HOVER_RESET = "Click To Reset All Data";
$STR_HOVER_ADD = "Click To Add New Details";
$STR_HOVER_DUPLICATE = "Click To Create Duplicate Of This Item";
$STR_HOVER_EDIT = "Click To Edit This Details";
$STR_HOVER_DELETE = "Click To Delete This Detail";
$STR_HOVER_DOWNLOAD = "Click To Download This Detail";
$STR_HOVER_DELETE_IMAGE = "Click To Delete This Image";
$STR_HOVER_INVISIBLE = "Click To Make This Detail Visible";
$STR_HOVER_VISIBLE = "Click To Make This Detail Invisible";
$STR_HOVER_DISPLAY_ORDER = "Click To Set Display Order";
$STR_HOVER_ENABLE_DISABLE = "Click To Enable / Disable ";
$STR_HOVER_ALLOW_LOGIN_MODE = "Click To Change Allow Login Mode To";
$STR_HOVER_HELP = "Click To View Help";
$STR_HOVER_MANAGE = "Click To Manage";
$STR_HOVER_MANAGE_PAGE = "Click To Manage Pages";
$STR_HOVER_SELECT_CODE = "Click To Select Code";
$STR_HOVER_IMAGE = "Click To View Actual Size Image";
$STR_HOVER_NOTAPPROVED = "Click To Make This Detail Approved";
$STR_HOVER_APPROVED = "Click To Make This Detail Not Approved";
$STR_HOVER_DISPLAY_AS_NEW = "Click To Change 'Display As New' mode";
$STR_HOVER_DISPLAY_AS_FEATURED = "Click To Change 'Display As Featured' mode";
$STR_HOVER_DISPLAY_AS_HOT = "Click To Change 'Display As Hot' mode";
$STR_HOVER_NUMBER_OF_VIEW_EDIT = "Click To Change 'Number of Views'";
//$STR_HOVER_NUMBER_OF_VIEW = "Number of Views";

$STR_PLACEHOLDER_SECRET_CODE = "Secret Code is Case Sensitive, Refresh browser to create new one";
$STR_PLACEHOLDER_PROFILE = "Enter profile ID Here";
$STR_PLACEHOLDER_AFFILIATEID = "Enter Current Affiliate ID Here";
$STR_PLACEHOLDER_LOGINID = "Use Only Letters, Numbers, Underscore ( _ ).";
$STR_PLACEHOLDER_PASSWORD = "Enter Password Here";
$STR_PLACEHOLDER_CONFIRM_PASSWORD = "Enter Confirm Password Here";
$STR_PLACEHOLDER_TITLE = "Enter Title Here";
$STR_PLACEHOLDER_SUB_TITLE = "Enter Sub Title Here";
$STR_PLACEHOLDER_FULLNAME = "Enter Name Here";
$STR_PLACEHOLDER_FIRSTNAME = "Enter Your First Name Here";
$STR_PLACEHOLDER_LASTNAME = "Enter Your Last Name Here";
$STR_PLACEHOLDER_DESC = "Enter Description Here";
$STR_PLACEHOLDER_MESSAGE = "Enter Your Message Here";
$STR_PLACEHOLDER_SUBJECT = "Enter Subject Here";
$STR_PLACEHOLDER_KEYWORD = "Enter SEO KeyWords Here";
$STR_PLACEHOLDER_INT = "Enter Positive Numeric Value Here";
$STR_PLACEHOLDER_INT_PRICE = "Enter Positive Numeric Value Without Dollar Sign Here";
$STR_PLACEHOLDER_INT_PERCENTAGE = "Enter Positive Numeric Value Without Percentage Sign Here";
$STR_PLACEHOLDER_INT_WITH_ZERO = "Enter 0 (zero) OR Positive Numeric Value Here";
$STR_PLACEHOLDER_EMAIL = "Example: emailid@websitename.com";
$STR_PLACEHOLDER_URL = "Example: http://www.websitename.com";
$STR_PLACEHOLDER_PHONENUMBER = "Enter Phone Number Here";
$STR_PLACEHOLDER_ADDRESS = "Enter Address Here";
$STR_PLACEHOLDER_CITY = "Enter City Here";
$STR_PLACEHOLDER_STATE = "Enter State Here";
$STR_PLACEHOLDER_ZIP = "Enter Zip Code Here";
$STR_PLACEHOLDER_COLOR = "Enter Color Here. Separate Multiple Colors By Pipe (|)";
$STR_PLACEHOLDER_COLOR_CODE = "Enter Hex Color Code Here";
$STR_PLACEHOLDER_CMYK_COLOR_CODE = "Enter CMYK Color Code Here";
$STR_PLACEHOLDER_SIZE = "Enter Size Here. Separate Multiple Sizes By Pipe (|)";
$STR_PLACEHOLDER_TYPE = "Enter Type Here. Separate Multiple Types By Pipe (|)";
$STR_PLACEHOLDER_TEXT = "Enter Text Here";
$STR_PLACEHOLDER_NAME = "Enter Name Here";
$STR_PLACEHOLDER_EXTRA_TEXT = "Enter Extra Text Here. e.g. Must Watch!!!, Hot Video, etc.";
$STR_PLACEHOLDER_HOVER_TEXT = "Enter Hover Text Here";
$STR_PLACEHOLDER_SEO_TITLE = "Enter SEO Title";
$STR_PLACEHOLDER_SEO_KEYWORDS = "Enter SEO Keywords";
$STR_PLACEHOLDER_TIME = "Enter Time Here In 24 hour format. example: 9:00 am to 5:00 pm OR 17:00 to 22:00.";
$STR_PLACEHOLDER_INT_INCH = "Enter Value In Inch.";
$STR_MSG_NOT_APPROVED = "NOT <br/>APPROVED";
$STR_MSG_MANDATORY = "Fields marked with * are mandatory.";
$STR_MSG_ADMIN = "(For Super Admin Only)";
$STR_MSG_SECERT_CODE = "Secret Code is Case Sensitive, Refresh browser to create new one";
$STR_MSG_SECERT_QA = "Please type your answer here in 'word'.";
$STR_MSG_VISIBLE = "If 'YES', data will be VISIBLE at user side";
$STR_MSG_OPEN_IN_NEW_WINDOW = "If 'YES', URL will be opened in new window when clicked.";
$STR_MSG_NEW = "If 'YES'. data will be displayed as NEW at user side";
$STR_MSG_HOT = "If 'YES', data will be displayed as HOT at user side";
$STR_MSG_FEATURED = "If 'YES', data will be highlighted at user side";
$STR_MSG_IMG_VISIBLE = "If 'YES', image will be displayed at user side";
$STR_MSG_IMG_UPDATE = "Keep field blank if you do not want to update existing image";
$STR_MSG_IMG_UPLOAD = "Keep field blank if you do not want to upload image";
$STR_MSG_FILE_UPLOAD = "Keep field blank if you do not want to upload file";
$STR_MSG_FILE_UPDATE = "Keep field blank if you do not want to update existing file";
$STR_MSG_IMG_NOT_UPLOADED = "Image Not Uploaded";
$STR_MSG_IMG_FILE_TYPE = "Supported image type: .jpeg, .jpg, .png, .gif";
$STR_MSG_DOC_FILE_TYPE = "Supported document file Type: .pdf, .doc, .docx, .odt";
$STR_MSG_VIDEO_FILE_TYPE = "Supported video type: .wmv, .mpg, .mpeg, .mp4, .ogg, .ogv";
$STR_MSG_ZIP_FILE_TYPE = "Supported video type: .wmv, .mpg, .mpeg";
$STR_MSG_DISPLAY_AS_NEW = "If 'YES', data will be displayed as NEW at user side";
$STR_MSG_DISPLAY_AS_HOT = "If 'YES', data will be displayed as HOT at user side";
$STR_MSG_DISPLAY_AS_FREE = "If 'YES', data will be FREE for site visitors.";
$STR_MSG_URL_FORMAT = "Example : http://www.websitename.com ";
$STR_MSG_EMAIL_FORMAT = "Example: emailid@websitename.com";
$STR_MSG_PHONE_FORMAT = "Example: 111-111-1111";
$STR_MSG_LOGINID_FORMAT = "Use Only Letters, Numbers, Underscore ( _ ).";
$STR_MSG_PASSWORD_FORMAT = "Minimum Length 6 chars with atleast one capital. Use Only Letters, Numbers, Underscore ( _ ).";
$STR_MSG_PASSWORD_BOTH_SAME = "New Password And Existing Password Must Be Same.";
$STR_MSG_PRICE_IN_US = "Enter single price in US$";
$STR_MSG_MEMBER_ALLOW_PREVIEW = "If 'YES', non-members can view only preview of item. If 'NO', non-member can't view even preview of item.";
$STR_MSG_MEMBER_ALLOW_ACCESS = "If 'YES', non-member will have full accesss of item. If 'NO', only members will have full access of item.";
$STR_MSG_CLICK_TO_VIEW_MORE_DETAILS = "Click To View More Details";
$STR_MSG_COLOR = "Separate multiple colors with pipe | separator. e.g. White | Black";
$STR_MSG_SIZE = "Separate multiple sizes with pipe | separator. e.g. S | M | L | XL";
$STR_MSG_TYPE = "Separate multiple types with pipe | separator. e.g. Type1 | Type2";
$STR_MSG_SHIPPING_DOMESTIC = "(in US$) | Enter 0 (zero) if no shipping charge applies.";
$STR_MSG_SHIPPING_INTERNATIONAL = "(in US$) | Enter 0 (zero) if no shipping charge applies.";
#------------------------------------------------------- FORM action messages
$STR_MSG_NO_DATA_AVAILABLE = "Currently no data available to display";
$STR_MSG_ACTION_ADD = "Success... New details added";
$STR_MSG_ACTION_EDIT = "Success... Details updated";
$STR_MSG_ACTION_DUPLICATE = "Success... Details duplicated";
$STR_MSG_ACTION_DELETE = "Success... Selected details deleted";
$STR_MSG_ACTION_DELETE_ALL = "Success... Selected all details deleted";
$STR_MSG_ACTION_DELETE_IMAGE = "Success... Selected image deleted";
$STR_MSG_ACTION_DELETE_FILE = "Success... Selected file deleted";
$STR_MSG_ACTION_DISPLAY_ORDER = "Success... Display order updated";
$STR_MSG_ACTION_NO_OF_VIEW = "Success... No of view updated";
$STR_MSG_ACTION_VISIBLE_INVISIBLE = "Success... Details changed to ";
$STR_MSG_ACTION_LOGIN = "Success... Login mode updated";
$STR_MSG_ACTION_REPLY = "Success... Reply mode updated";
$STR_MSG_ACTION_USER_TYPE = "Success... User Type changed";
$STR_MSG_ACTION_POST_TOPIC = "Success... Post Topic mode updated";
$STR_MSG_ACTION_APPROVE = "Success... Approved mode updated.";
$STR_MSG_ACTION_PURCHASE = "Success... Purchase Mode updated";
$STR_MSG_ACTION_SET_AS_DEFAULT = "Success... Set As Default mode updated";
$STR_MSG_ACTION_SET_AS_FRONT = "Success... Set As Front mode updated";
$STR_MSG_ACTION_DISPLAY_AS_NEW = "Success... Display As New mode updated";
$STR_MSG_ACTION_DISPLAY_AS_FEATURED = "Success... Display As Featured mode updated";
$STR_MSG_ACTION_DISPLAY_ADDITIONAL_INFO = "Success... Display As Additional Information mode updated";
$STR_MSG_ACTION_DISPLAY_AS_HOT = "Success... Display As Hot mode updated";
$STR_MSG_ACTION_IMAGE_ROTATED = "SUCCESS !!! Image rotated.";
$STR_MSG_ACTION_INFO_MISSING = "Error... Some information(s) missing or invalid, please try again";
$STR_MSG_ACTION_INVALID_SCODE = "Error... Invalid secret code. It's case sensitive.";
$STR_MSG_ACTION_INVALID_FILE_EXT = "Error... Invalid file extension";
$STR_MSG_ACTION_INVALID_ZIP_EXT = "Error... Invalid zip extension";
$STR_MSG_ACTION_INVALID_IMAGE_EXT = "Error... Invalid image extension";
$STR_MSG_ACTION_INVALID_EMAIL_FORMAT = "Error... Invalid email format. example: emailid@websitename.com";
$STR_MSG_ACTION_INVALID_URL_FORMAT = "Error... Invalid URL format. example: www.websitename.com";
$STR_MSG_ACTION_INVALID_DATE_FORMAT = "Error... Invalid date format";
$STR_MSG_ACTION_INVALID_PRICE = "Error... Price must be greater than zero (0)";
$STR_MSG_ACTION_INVALID_LOGINID = "Error... Invalid login ID format. Only use alphabets, numbers and underscore(_)";
$STR_MSG_ACTION_INVALID_PASSWORD = "Error... Invalid password format. Only use alphabets and numbers";
$STR_MSG_ACTION_INVALID_SECRET_CODE = "Error... Invalid secret code.";
$STR_MSG_ACTION_ALLOW_LOGIN = "Error... You are not allowed to login.";
$STR_MSG_ACTION_ALLOW_REPLY = "Error... You are not allowed to reply.";
$STR_MSG_ACTION_INVALID_PASSWORD2 = "Error... Invalid password format. Only use alphabets and numbers and One capital character";
$STR_MSG_ACTION_INVALID_LOGINID_PASSWORD = "Error... Invalid login ID / Password";
$STR_MSG_ACTION_INVALID_CONFIRM_PASSWORD = "Error... Confirm Password must be match with above entered password";
$STR_MSG_ACTION_DUPLICATE_TITLE = "Error... Title already exists, please try with another one";
$STR_MSG_ACTION_DUPLICATE_LOGINID = "Error... Login ID already exists, please try with another one";
$STR_MSG_ACTION_SELECT_FILE_TO_UPLOAD = "Error... Please select file to upload";
$STR_MSG_ACTION_LOGIN_RESTRICTED = "Admin panel Login is restricted at present";
$STR_MSG_ACTION_STATUS_CHANGED = "Success... Shipping status updated";
#-------------------------------------------------------  ICON messages
$STR_LINK_ICON_PATH_VISIBLE = "<i class='fa fa-eye'></i>";
$STR_LINK_ICON_PATH_INVISIBLE = "<i class='fa fa-eye-slash'></i>";
$STR_LINK_ICON_PATH_EDIT = "<i class='fa fa-pencil'></i>";
$STR_LINK_ICON_PATH_DUPLICATE = "<i class='fa fa-clone'></i>";
$STR_LINK_ICON_PATH_DELETE = "<i class='fa fa-trash'></i>";
$STR_LINK_ICON_PATH_APPROVED = "<i class='fa fa-check'></i>";
$STR_LINK_ICON_PATH_NOT_APPROVED = "<i class='fa fa-times'></i>";
$STR_LINK_ICON_PATH_IMAGE = "<i class='fa fa-image' title='Click To Manage Images'></i>";
$STR_LINK_ICON_PATH_VIDEO_CLIP = "<i class='fa fa-file-video-o' title='Click To Manage Video Clips'></i>";

$STR_ICON_PATH_NO_OF_VIEW = "<i class='fa fa-eye text-muted' title='Number of Views'></i>";
$STR_ICON_PATH_NO_OF_DOWNLOAD = "<i class='fa fa-download text-muted' title='Number of Downloads'></i>";
$STR_ICON_PATH_NO_OF_SOLD = "<i class='fa fa-money text-muted' title='Number of Sold'></i>";
$STR_ICON_PATH_AVAILABLE = "<span class='alert-success text-success'><i class='fa fa-check-circle-o'></i>&nbsp;In Stock</span>";
$STR_ICON_PATH_NOTAVAILABLE = "<span class='alert-danger text-danger'><i class='fa fa-times-circle-o'></i>&nbsp;Out Of Stock</span>";

$STR_ICON_PATH_NEW = "<label class='label label-success'>NEW</label>";
$STR_ICON_PATH_FREE = "<label class='label label-warning'>FREE</label>";
$STR_ICON_PATH_HOT = "<label class='label label-danger'>TRENDING</label>";
$STR_ICON_PATH_FEATURED = "<label class='label label-info'>FEATURED</label>";
#------------------------------------------------------- File type validation
$STR_IMG_FILE_TYPE_VALIDATION = "jpg,jpeg,png,gif,";
$STR_DOC_FILE_TYPE_VALIDATION = "pdf,doc,docx,odt,";
$STR_VIDEO_FILE_TYPE_VALIDATION = "flv,asf,avi,mov,mp4,mpg,mpeg,ogg,ogv,webm,wmv,mkv,3gp,ram, rm, rmvb, rpm,";
$STR_ZIP_FILE_TYPE_VALIDATION = "zip,rar,tar,";
#------------------------------------------------------- HELP messages
$STR_MSG_HELP_HEADING = "Help Section";
$STR_MSG_HELP_DISPLAY_ORDER = "Column named DISPLAY ORDER is used to manage display order in an ASCENDING order at user side.";
$STR_MSG_HELP_HTML_OBJ = "For mobile friendly website, use below mentioned mobile friendly HTML objects as per given examples.";
$STR_MSG_HELP_HTML_TABLE = "For mobile friendly website, you must NOT use TABLE tags. Instead of that you should use DIV tags as per given example.";
$STR_MSG_HTML = "<b>Basic HTML tags to use in your content.</b><br/>
&bull;  " . htmlentities('<h1>your text here</h1>') . " to " . htmlentities('<h4>your text here</h4>') . " : This tag is used for different size heading where h1 gives largest and h4 gives smallest size font. <br/>
&bull;  Image : " . htmlentities('<img src="path of image" border="0" alt="info about image" class="image-responsive" width="100" height="100">') . "<br/>
&bull; Mail to : " . htmlentities("<a href='mailto:emailid@websitename.com'>your text  here</a>") . "<br/>
&bull; Link : " . htmlentities('<a href="http://www.websitename.com">your text  here</a>') . "<br/>
&bull; Blank space : " . htmlentities('&nbsp;') . " : Multiply this for multiple spaces.<br/>
&bull; New line : " . htmlentities('<br/>') . "<br/>
&bull; Horizontal line : " . htmlentities('<hr>') . "<br/>
&bull; Bold text : " . htmlentities('<b>your text here</b>') . "<br/>
&bull; Italic text : " . htmlentities('<i>your text here</i>') . "<br/>
&bull; Strike text : " . htmlentities('<strike>your text here</strike>') . "<br/>
&bull; Underline text : " . htmlentities('<u>your text here</u>') . "<br/>";
$STR_LINK_HELP = "<a href='#help' title='Click To View Help Section'><span class='pull-right badge'><i class='fa fa-info-circle'></i>&nbsp; HELP</span></a>";
$STR_LINK_TOP = "<a href='#ptop' class='link ' title='Go To Page Top'><span class='pull-right badge'><i class='fa fa-chevron-circle-up'></i>&nbsp;TOP</a></span>";
$STR_MSG_HELP_EDIT_BUTTON = "<a class='btn btn-success btn-xs disabled'><i class='fa fa-pencil'></i></a>&nbsp;Click to UPDATE particular data.";
$STR_MSG_HELP_EDIT_BUTTON_DISABLED = "<a class='btn btn-default btn-xs disabled'><i class='fa fa-pencil'></i></a>&nbsp;indicates disabled UPDATE feature.";
$STR_MSG_HELP_DELETE_BUTTON = "<a class='btn btn-danger btn-xs disabled'><i class='fa fa-trash-o'></i></a>&nbsp;Click to DELETE particular data permanently.";
$STR_MSG_HELP_DELETE_BUTTON_DISABLED = "<a class='btn btn-default  btn-xs disabled'><i class='fa fa-trash-o'></i></a>&nbsp;Indicates disabled DELETE feature.";
$STR_MSG_HELP_VISIBLE_INVISIBLE_BUTTON = "<a class='btn btn-warning btn-xs disabled'><i class='fa fa-eye'></i></a>&nbsp;Indicates VISIBLE data, Click to make it INVISIBLE.";
$STR_MSG_HELP_INVISIBLE_VISIBLE_BUTTON = "<a class='btn btn-default btn-xs disabled'><i class='fa fa-eye-slash'></i></a>&nbsp;Indicates INVISIBLE data, Click to make it VISIBLE.";
$STR_MSG_HELP_ONLINE_OFFLINE_STATUS = "If set ONLINE, Your Online Status will be displayed for Video Chat Room.";
$STR_MSG_HELP_ALLOW_GUEST = "Allows Guests to ONLY preview you without having to create a FREE cam account.";

$STR_MSG_HELP_PRICE = "Do not use $ or any other sign with price.";
$STR_MSG_HELP_TIME = "Do not use minutes or other text with numeric value.";
$STR_MSG_HELP_SIZE_INCH = "Do not use any text when entering value in inches.";
#------------------------------------------------------- XML file path
$STR_XML_FILE_PATH_MODULE = "../mdm/xmlmodulefiles/";
$STR_XML_ROOT_MODULE = "ROOT_ITEM";
$STR_XML_FILE_PATH_CMS = "../mdm/xmlcontentfiles/";
$STR_XML_ROOT_CMS = "ROOT_ITEM_CMS";



#------------------------------------------------------- Module wise variable
//$STR_TITLE_SIGNUP = "Signup";
//$STR_TITLE_SIGNIN = "Signin";
//$STR_TITLE_ABOUT = "About";
$STR_IMG_PATH_ABOUT = "./mdm/about/";
//$STR_TITLE_HOWITWORKS = "How It Works";
$STR_IMG_PATH_HOWITWORKS = "./mdm/howitworks/";
//$STR_TITLE_TERMS = "Term and Conditions";
$STR_IMG_PATH_TERMS = "./mdm/termsconditions/";
//$STR_TITLE_PRIVACY_POLICY = "Privacy";
$STR_IMG_PATH_PRIVACY_POLICY = "./mdm/privacypolicy/";
//$STR_TITLE_REFUND_POLICY = "Refund Policy";
$STR_IMG_PATH_REFUND_POLICY = "./mdm/refundpolicy/";
//$STR_TITLE_FAQ = "FAQ";
$STR_IMG_PATH_FAQ = "./mdm/faq/";

$STR_IMG_PATH_RETURNPOLICY = "./mdm/returnpolicy/";
//$STR_TITLE_LEGAL = "Legal Stuff";
$STR_IMG_PATH_LEGAL = "./mdm/legalstuff/";
$STR_TITLE_MODEL_PAST_ANNOUNCEMENT = "Past Announcements";
$STR_TITLE_MODEL_DASHBOARD = "My Dashboard";
$STR_UPLOAD_NEWS_PATH = "../mdm/news/";
$STR_IMG_PATH_MEASURECHART = "./mdm/measurechart/";
//-----------------contact-------------------------------------------------------------
$STR_TITLE_CONTACT = "Contact Us";
$STR_IMG_PATH_CONTACT = "../mdm/contentmanagement/";
$STR_IMG_PATH_HOME = "./mdm/home/";
$STR_IMG_PATH_HOME_SLIDER = "./mdm/homeslider/";
$STR_TITLE_HOME = "Home";
$STR_TITLE_SITE_CONFIG = "Site Configuration";
$STR_TITLE_INVEST = "Invest List";
$STR_TITLE_WEB_MASTER = "Webmaster List";
$STR_TITLE_WEB_MASTER_HOME = "Webmaster Home";
$STR_TITLE_BUYER_HOME = "Buyer Home";
$INT_IMG_WIDTH_HOME = "120";
$INT_IMG_HEIGHT_HOME = "180";
$INT_WEBMASTER_HOME = 200;
$STR_TITLE_LOGIN = "Log In";

##--------------------------------------INDEX PAGE SECTION HEADING--------------------------------------------- 
$STR_TITLE_HOME_FEATURED = "FEATURED";
$STR_TITLE_HOME_HOT = "TRENDING DEALS";
$STR_TITLE_HOME_NEW = "NEW ARRIVALS";
##--------------------------------------INDEX PAGE SECTION HEADING--------------------------------------------- 


///--------------------------------------Service page--------------------------------------------- 

$STR_TITLE_SERVICE = "Services";
$STR_IMG_PATH_SERVICE = "./mdm/service/";

$STR_TITLE_BUYNOW = "Buy Now";
$STR_TITLE_ADVERTISE_WITH_US = "Advertise With Us";

$STR_TITLE_FREETRIAL = "Free Trial";
$STR_TITLE_WHYUS = "Why Us?";



$STR_TITLE_VISITOR_AREA = "Visitor Area";

$STR_IMG_PATH_CODEOFCONDUCT = "../mdm/codeofconduct/";

$STR_TITLE_BROWSE_CATEGORY = "Browse Reviews by";
$STR_TITLE_SUBMIT_REVIEW = "Submit Review";
$STR_TITLE_BROWSE_SUBCATEGORY = "Browse Reviews";



$STR_FORGOT_PASSWORD = "Forgot Password";
$STR_TITLE_CHANGE_PASSWORD = "Change Password";

$STR_TITLE_PORTFOLIO = "Portfolio";
$STR_IMG_PATH_PORTFOLIO = "./mdm/portfolio/";

//$STR_TITLE_DASHBOARD = "My Dashboard";
//--------------------------------------------------------------------------------------------
$STR_TITLE_AFFILIATE_PROGRAM = "Affiliate Program";
$STR_TITLE_AFFILIATE_TOOL = "Affiliate Tools";
$STR_TITLE_PURCHASE_LOCAL_FORM = "Purchase Local Form";
$STR_TITLE_PERSONAL_DETAILS = "Personal Details";
$STR_TITLE_CREATE_MANAGE_AUTHORIZATION_DOCUMENT = "Create / Manage authorization documents";

$STR_TITLE_CATEGORY = "Category List";

$STR_TITLE_FIND_JOB = "Find A Job";

$STR_BILL = "Buyer Information Verification";
$STR_AGE_IDENTITY_VERIFICATION = "Age & Identity Verification - Upload the two (2) required images below:";
$STR_MEMBER_VERIFICATION = "Member Verification";
$STR_FILTER_CRITERIA = "Filter Criteria";

$STR_TITLE_BROWSE_REVIEW_WITH_CAT = "Browse Reviews";
$STR_TITLE_BROWSE_BY_CATEGORY_WITH_CAT = "Browse Reviews By Categories";

#------------------------------------------------------------------------------------------------------

# variable used for displaying shopping cart details
$STR_TRANS_LIMIT_MESSAGE = "* NOTE: The total purchase amount must not exceed $3000 per single order but you can do multiple transactions if necessary.";
$STR_CART_MESSAGE = "** To remove an Item from your cart, set the Quantity to 0(Zero) and click 'update cart' button.";

# variable used for displaying shopping cart details
$STR_CART_ADDITIONAL_MESSAGE = "*** When changing any information of the shopping cart, click 'update cart' button.";

# variable used for displaying final order message.
$STR_FINAL_ORDER_MESSAGE = "**** Please do NOT Refresh this page, otherwise you may be billed more than once for your purchase.";

# Product Module
#variblae used for subject in sending email
//$STR_USER_SUBJECT="Purchase Order Details From camsitestartup.com";
//$STR_ADMIN_SUBJECT="New Order Placed on camsitestartup.com with Order No:";
///-----------------------------------------------------------------------------------
# this variable are used for Authorize.Net Payment Gateway.
#For Test Account of SANDBOX
//$AUTHORIZE_API_LOGIN_ID="6DX73dZ8sf";
//$AUTHORIZE_TRANSACTION_KEY="3Mb8JEw229y5KzDx";	

#total rivews for one page
//$INT_REVIEW_PER_PAGE = 50;
$STR_PATH_UPLOAD_ADBANNER = "./mdm/adbanner/";  ## with_cat_subcat

# For Live Account
$AUTHORIZE_TRANSACTION_KEY = "";
$INT_AMOUNT_FIX_DEDUCTION_PER_BILL = 0.55;
///-----------------------------------------------------------------------------------
# this variable is used for css store theme
$STR_FLAG_APPLY_STORE_THEME_ON_THIS_PAGE = 'YES';


//$INT_SHOP_CATEGORY_RECORDS_PER_PAGE=50;
//$INT_SHOP_SUBCATEGORY_RECORDS_PER_PAGE=50;
//$INT_SHOP_PRODUCTS_RECORDS_PER_PAGE=50;

#Variable used on SHOP PANEL page
$STR_PATH_UPLOAD_SHOPPANEL = "../mdm/shoppanel/";
#image size
$INT_SHOPPANEL_WIDTH = 175;

//	$INT_SHOP_LIST_PAGE_BANNER_ARRAYINDEX = "8";
//	$INT_SHOP_MAIN_CATEGORY_LIST_PAGE_BANNER_ARRAYINDEX = "9";
//	$INT_SHOP_SUB_CATEGORY_LIST_PAGE_BANNER_ARRAYINDEX = "10";
//	$INT_SHOP_PRODUCT_DETAIL_PAGE_BANNER_ARRAYINDEX = "11";

#variable used to display instructions for Cashier's check
$STR_CASH_MSG = "Cashier's Check Format: <br> <br>Check No : 00012345 <br> Bank       : Bank Of California <br> Branch : California";

#-------------------------------This variable is used to display paypal instruction while resend the email.-----------------------
$STR_COMPANY_PAYPAL_MSG = "Send payment to our Paypal Id with Purchase order no.";
$STR_COMPANY_PAYPAL = "NOTE: Our Company Paypal Id is: ";

#-----------------------------------this variable is used to display moneyorder information---------------------------------------
$STR_MONEYORDER_MSG = "Thank you for your Order.<br> If paying by money order, please make it payable to:<br><br>";
$STR_MONEYORDER_MSG_1 = "<br><br>Thank You.";

$STR_DISPLAY_AS_NEW_PRODUCT = "NOTE: If set YES, product will be displayed as new.";
$STR_WIDTH_MSG = "Note: Width should not be more than 728.";
///-----------------------------------------------------------------------------------   
//$INT_RECORD_PER_PAGE=40; /// Number of zipset listed per page.
///-----------------------------------------------------------------------------------   
/// Photoset DOWNLOAD Limit in Days After Purchase
///-----------------------------------------------------------------------------------   
$STR_XML_COMMON_PATH = "../mdm/model/";
///-----------------------------------------------------------------------------------   
/// XML XMLULE PATH&nbsp;
///-----------------------------------------------------------------------------------   
## For Admin Header & Footer
$STR_ADMIN_HEADER_PATH = "../../includes/header_admin.php";
$STR_ADMIN_FOOTER_PATH = "../../includes/footer_admin.php";

## For User Header & Footer
$STR_USER_HEADER_PATH = "./../includes/header.php";
$STR_USER_HEADER_PATH2 = "./../includes/header2.php";
$STR_USER_FOOTER_PATH = "../includes/footer.php";
$STR_USER_FOOTER_PATH2 = "../includes/footer2.php";

## For Page Banner
$STR_USER_BANNER_PATH = "./banner_page.php";

///------------------------------------VIDEO-----------------------------------------------
$STR_TITLE_VIDEO = "Video";
$STR_TITLE_VIDEO_CLIP = "Video Clip";
global $STR_XML_VIDEO_FILE_PATH;
global $STR_XML_VIDEO_ROOT_TAG;

$STR_XML_VIDEO_FILE_PATH = "../mdm/xmlmodulefiles/video.xml";
$STR_XML_VIDEO_ROOT_TAG = "VIDEO_ROOT";

$STR_UPLOAD_VIDEO_CLIP_PATH = "../mdm/ftpupload/video/";
//$INT_VIDEO_RECORDS_PER_PAGE = 5;
$INT_VIDEO_RECORDS_MARGIN = 5;

$STR_VIDEO_IMG_MSG = "NOTE: Maximum video photo width: 150";
$INT_VIDEO_THUMB_WIDTH = "150";
$INT_VIDEO_THUMB_HEIGHT = "100";

$STR_VIDEO_FILE_MSG = "NOTE: you can upload maximum 10 MB of video file.";
$STR_MORE_MB = "If video file size is more than 10 MB then select file extension for it.";
$STR_EXTENSION = "NOTE: on the basis of selected Extension, we will give you location where you have to upload a file having file name generated by us.";

$INT_VIDEO_LIMIT = 5000;
$INT_CLIP_PER_VIDEO_LIMIT = 1;

$STR_HEADER_VIDEO = "VIDEO";
$STR_VIDEO_PATH_UPLOAD_VIDEO = "./mdm/video/";
$INT_VIDEO_PANEL = 200;


$STR_TITLE_PAGE_BANNER = "Page Banner List";
$STR_TITLE_PAGE_BANNER_HEADER = "Header Page Banner List";
$STR_TITLE_PAGE_BANNER_FOOTER = "Footer Page Banner List";
$STR_TITLE_PAGE_BANNER_PANEL = "Panel Page Banner List";
$INT_HOMEPAGE_BANNER_LIMIT = 0;


$STR_TITLE_MANAGE_PAGE_FOR_BANNER = "Manage Pages";
$STR_PG_BANNER_PATH_UPLOAD = "./mdm/pagebanner/";
$STR_PG_BANNER_PATH_SUB_UPLOAD = "./../mdm/pagebanner/";
$STR_XML_PAGE_BANNER_FILE_NAME = "page_banner.xml";

$STR_TITLE_MANAGE_CONTENT = "CMS";



$STR_UPLOAD_CONTENT_MANANGEMENT_PATH = "./mdm/contentmanagement/";
$STR_UPLOAD_WEBMASTER_PATH = "./mdm/webmaster/";

$STR_IMG_PATH_UPLOAD = "../mdm/model/";
$STR_IMG_PATH_ABSOLUTE_UPLOAD = "mdm/model/";

$STR_USER_PANEL_PATH = "../user/user_panel.php";

///-------------------------------------IMAGE related variable----------------------------------------------   
#image size
$INT_COMMENT_IMG_THUMB_WIDTH = "150";
$INT_COMMENT_IMG_THUMB_HEIGHT = "225";
$INT_COMMENT_IMG_LARGE_WIDTH = "600";

$STR_XML_IMG_MSG = "NOTE: Approx. Size: 1200w x 1500h";
$STR_IMG_MSG = "NOTE: Maximum Photograph Width: 750";

$STR_IMG_PATH_UPLOAD_BUYER = "../mdm/buyer/";
$STR_UPLOAD_IMG_PATH_IMGSET = "../mdm/photoset/";

$STR_DOMAIN_IMG_PATH_UPLOAD = "./mdm/domain/";
$STR_PROD_IMAGE_PATH_UPLOAD = "./mdm/product/";
$STR_PRODUCT_IMAGE_PATH_UPLOAD = "../mdm/product/";
$STR_UPLOAD_PHYSICAL_PRODUCT_IMAGE_PATH = realpath("../mdm/product/");
$STR_PRODUCT_LARGE_IMG_MSG = "Maximum Photograph Width: 600 px";
$INT_PRODUCT_SMALL_IMAGE_WIDTH = 75;
$INT_PRODUCT_THUMB_IMAGE_WIDTH = 200;
$INT_PRODUCT_LARGE_IMAGE_WIDTH = 600;
$INT_MAX_PRODUCT_IMAGES = 0;

$STR_HELP_TEXT_HTML_OBJ = "For mobile friendly website, use below mentioned mobile friendly HTML objects as per given examples.";


$INT_XML_IMG_LARGE_WIDTH = 1200;
$INT_XML_IMG_THUMB_WIDTH = 200;

$STR_XML_IMG_PATH_UPLOAD = "./mdm/model/";
$STR_SHOP_PATH_UPLOAD = "./mdm/contentmanagement/";

$STR_PROMOGALLERY_PATH_UPLOAD = "../mdm/promogallery/";
$STR_PROMO_GALLERY_DEFAULT_URL = "Click to visit the site";
$INT_PROMOGALLERY_IMG_PAGING_LIMIT = 10;
$INT_PROMOGALLERY_IMG_PAGING_LIMIT = 10;

/*$STR_TITLE_BANNER = "Opportunities List";
$INT_BANNER = 728;
$STR_BANNER_PATH_UPLOAD = "../mdm/banner/";*/

$INT_LIMIT = 0;


$STR_XML_MAX_RESUME_ALLOWED = 10;    // maximum number of resume entry allowed for model.


$STR_XML_MAX_STATISTICS_ALLOWED = 50;    // maximum number of entry allowed for model.
$STR_KEY = "e.g Birthdate, Height, Eyes etc.";
$STR_VALUE = "e.g August 19(Leo), 5'7, Brown etc. ";
$STR_IMPORT_STATS_FORMAT = "Format: ModelName [Registration Date][EmailId](visible/invisible)(Number of Statistics)";

$STR_XML_MAX_TURNONOFF_ALLOWED = 50;    // maximum number of resume entry allowed for model.
$INT_TURNONOFF_IMG_LARGE_WIDTH = "500";
$INT_TURNONOFF_IMG_THUMB_WIDTH = "150";

$STR_TURNONOFF_IMG_PATH_UPLOAD = "../mdm/model/";

$STR_XML_PHOTOALBUM_ROOT_TAG = "PHOTOALBUM_ROOT";

$INT_MDL_PHOTOALBUM_LIMIT = 0;
$INT_MDL_IMG_LIMIT = 0;
$INT_XML_IMG_PAGING_LIMIT = 20;

$STR_IMG_PATH_UPLOAD = "../mdm/model/";

$INT_IMG_LARGE_WIDTH = "750";
$INT_IMG_THUMB_WIDTH = "150";
$INT_IMG_THUMB_HEIGHT = "100";



//$STR_LINK_MSG="<br/>NOTE: If anchor tag is added then please also add class for that anchor tag. | e.g ".htmlentities("<a href='http://www.website.com' class='Link10ptNormal'>write your text here</a>");

//$STR_MAILTO_MSG="NOTE: To enter MAIL TO functionality, Please copy following code in above text area.<br/>e.g ".htmlentities("<a href='mailto:emailid@domainname.com' class='Link8ptNormal'>emailid@domainname.com </a>");

$STR_DISPLAY_AS_NEW_CATEGORY = "NOTE: If set NO, data will not be displayed as new.";
$STR_DISPLAY_AS_NEW_PRODUCT = "NOTE: If set NO, data will not be displayed as new.";
$STR_ALLOW_SHOP = "NOTE: Select YES if you want to make this data available for shopping.";

$STR_ZIP_FILE_TYPE = "File Type: .zip, .rar";
$STR_MAX_ZIP_LIMIT = "You can upload zipfile of maximum 10MB.";
$STR_RESUME_URL = "NOTE:If url is not given then even the url title will not be displayed at user side.";

//$STR_DISPLAY_ORDER="Column named \"Display Order\" is used to manage display order in an ascending order at user side.";

$STR_AMOUNT_MSG = "(in $)";
$STR_AMOUNT_CHAR = "$";
$STR_SCROLL_MSG = "e.g. Coming Soon, Special Offer etc.";

$STR_IMG_PATH_UPLOAD_COMMENT = "../mdm/modelsearchcomment/";

$STR_UPLOAD_PATH_TESTIMONIAL = "./mdm/testimonial/";
$INT_THUMB_WIDTH_TESTIMONIAL = "300";
$INT_THUMB_HEIGHT_TESTIMONIAL = "200";
$STR_XML_FILE_PATH_TESTIMONIAL = "../mdm/xmlmodulefiles/modelsearchcomment.xml";


$STR_RIGHT_PANEL_LISTING_TESTIMONIAL = "YES";

$INT_MAX_XML_WRITE_TIME = 600; // Time in Second for rewrite XML file 

$INT_XML_REFRESH_TIME = 600;

$STR_MAIL_TEMPLATE_FILE = "../mdm/mailtemplates/email_template.html";


$STR_UPLOAD_PANEL_PATH_JOINNOW = "../mdm/joinnowpanel/";
$INT_PANEL_WIDTH_JOINNOW = "188";


$STR_FROM_DEFAULT = "P_SITE_EMAIL_ADDRESS";
$STR_FROM_DEFAULT_SUBJECT = "P_SITE_EMAIL_SUBJECT";
$STR_WEBSITE_NAME = "P_SITE_URL";
$STR_PRODUCT_ADD_NOTIFICATION_EMAIL = "p_PRODUCT_ADD_NOTIFICATION_EMAIL";
$STR_CONTACT_US_NOTIFICATION_EMAIL_CONTACT_US = "P_CONTACT_US_NOTIFICATION_EMAIL_FAN_STUFF";
$STR_COMMENT_NOTIFICATION_EMAIL = "P_COMMENT_NOTIFICATION_EMAIL";


$STR_IMG_PATH_WHYUS = "./mdm/whyus/";

$STR_IMG_PATH_PRICING = "./mdm/pricing/";

$STR_XML_IMG_PATH_BECOME = "../mdm/becomemodel/";
$INT_XML_THUMB_WIDTH_BECOME = 75;

$STR_UPLOAD_PATH_ZIPFILE = "../mdm/xonwqdk/";

$STR_TITLE_SUBMIT_NEWS_CONTENTS = "Submit News Contents List";
$STR_TITLE_SUBMIT_NEWS_LIST = "Submit News List";
$STR_UPLOAD_PATH_NEWS = "../mdm/news/";
$INT_NEWS_THUMB_WIDTH = "150";
$INT_NEWS_LARGE_WIDTH = "600";

///-----------------------------------------------------------------------------------   
$STR_UPLOAD_IMG_PATH_GALLERY = "../mdm/photogallery/";



$STR_XML_ROOT_CATEGORY = "CATEGORY_ROOT";
$STR_IMG_PATH_BANNER = "../mdm/banner/";
$INT_BANNER_WIDTH = 468;

$STR_LINK_PATH_UPLOAD = "../mdm/link/";
$INT_LINK_WIDTH = 468;

$STR_UPLOAD_IMG_PATH_POLL = "../mdm/poll/";

$STR_UPLOAD_PATH_PROMOGALLERY = "../mdm/promogallery/";
$STR_DEFAULT_URL_PROMOGALLERY = "Click to visit the site";

$INT_NOOFDAYS_TO_CONSIDER_IMGSET_NEW = 7;
$INT_DOWNLOAD_LIMIT_DAY_IMGSET = 3;
$INT_IMGSET_MAX_DOWNLOAD = 5;

$STR_UPLOAD_PATH_DIGISIGN = "./../mdm/digisign/";
$STR_EXTENSION_DIGISIGN = "jpg";

#upload path for review
$STR_UPLOAD_REVIEW_PATH = "../mdm/review/";
$STR_UPLOAD_REVIEW_PATH_ABSOLUTE = "./mdm/review/";

$STR_IMG_PATH_FEATURED_CAM_GIRL = "./mdm/homefeaturedcamgirl/";
$STR_IMG_PATH_FEATURED_SEX_TOYS = "./mdm/homefeaturedsextoys/";
$STR_IMG_PATH_FEATURED_SEX_TOYS2 = "./mdm/homefeaturedsextoys2/";
$STR_TITLE_VERTICAL_BANNER = "Vertical Banner List";
$STR_IMG_PATH_VERTICAL_BANNER = "./mdm/homeverticalbanner/";
$STR_IMG_PATH_IN_THE_SPOTLIGHT = "./mdm/homeinthespotlight/";


#admin side variable
$STR_TITLE_NEWS_INDUSTRY = "Industry News List";
$STR_TITLE_PRESS = "Press List";
$STR_TITLE_SPONSOR = "Sponsor List";
$STR_TITLE_LIST_REVIEW = "Review List";
$STR_TITLE_MODULE_ADMIN_LOGIN = "Module Admin Login";
$STR_TITLE_ADMIN_LOGIN = "Site Admin Login";
$STR_TITLE_SITE_ADMIN = "Site Admin List";
$STR_TITLE_SEO = "SEO Metatag List";

#MODEL
$STR_TITLE_MODEL = "Recipient"; // Needed after login by model
$STR_TITLE_PHOTO_ALBUM = "Photo Album List";
$STR_TITLE_MANAGE_CONTENT = "CMS";
$INT_PHOTOALBUM_LIMIT = 0;
$STR_IMG_NOT_UPLOADED = "image not uploaded";
$STR_PREVIEW_NOT_AVAILABLE = "Preview<br/>Not<br/>Available";
$STR_ZIP_NOT_UPLOADED = "zip file not uploaded";
$STR_MSG_ACTION_INVALID_PHOTO_EXT = "ERROR !!! Image extension not supported.";
$STR_TITLE_PROMO_GALLERY = "Promo Gallery List";
$STR_TITLE_PROMO_GALLERY_IMAGE = "Promo Gallery Image List";


# Table Headinig
$STR_TABLE_COLUMN_NAME_SR_NO = "Sr. #";
$STR_TABLE_COLUMN_NAME_DATE = "Date";
$STR_TABLE_COLUMN_NAME_CATEGORY = "Category";
$STR_TABLE_COLUMN_NAME_CATEGORY_SUBCATEGORY = "Category / Sub Category";
$STR_TABLE_COLUMN_NAME_TITLE = "Title";
$STR_TABLE_COLUMN_NAME_DETAILS = "Details";
$STR_TABLE_COLUMN_NAME_CONTACT_DETAILS = "Contact Details";
$STR_TABLE_COLUMN_NAME_FILE_DETAILS = "File Details";
$STR_TABLE_COLUMN_NAME_ALLOW_PURCHASE = "Allow Purchase";
$STR_TABLE_COLUMN_NAME_NO_OF_VIEW = "Number Of View";
$STR_TABLE_COLUMN_NAME_SET_AS_FRONT = "Set As Front";
$STR_TABLE_COLUMN_NAME_ACTION = "Action";
$STR_TABLE_COLUMN_NAME_IMAGE = "Image";
$STR_TABLE_COLUMN_NAME_VIDEO = "Video";
$STR_TABLE_COLUMN_NAME_POSITION = "Position";
$STR_TABLE_COLUMN_NAME_LOGIN_DETAILS = "Login Detail";
$STR_TABLE_COLUMN_NAME_SUBSCRIPTON_ID = "Subscription Id";
$STR_TABLE_COLUMN_NAME_LOGIN_ID = "Login Id";
$STR_TABLE_COLUMN_NAME_PAGES = "Pages";
$STR_TABLE_COLUMN_NAME_IS_SUPER_ADMIN = "Is Super Admin";
$STR_TABLE_COLUMN_NAME_IP_ADDRESS_HISTORY = "IP Address History";
$STR_TABLE_COLUMN_NAME_URL = "URL";
$STR_TABLE_COLUMN_NAME_DISPLAY_AT = "Display At";
$STR_TABLE_COLUMN_NAME_VIEW = "View";
$STR_TABLE_COLUMN_NAME_SELECT = "Select";
$STR_TABLE_COLUMN_NAME_DELETE = "Delete";
$STR_TABLE_COLUMN_NAME_UNSUBSCRIBE = "Unsubscribe";
$STR_TABLE_COLUMN_NAME_ALLOW_SITE_ADMIN_LOGIN = "Allow Site Admin Login";
$STR_TABLE_COLUMN_NAME_ALLOW_LOGIN = "Allow Login";
$STR_TABLE_COLUMN_NAME_SEND_MAIL = "Send Mail";
$STR_TABLE_COLUMN_NAME_TOTAL_ITEMS = "Total Items";
$STR_TABLE_COLUMN_NAME_TOTAL_AMOUNT = "Total <br/>Amount<br>(in US$)";
$STR_TABLE_COLUMN_NAME_DISPLAY_AS_NEW = "Display As New";
$STR_TABLE_COLUMN_NAME_DISPLAY_AS_HOT = "Display As Hot";
$STR_TABLE_COLUMN_NAME_DISPLAY_AS_FEATURED = "Display As Featured";
$STR_TABLE_COLUMN_NAME_DISPLAY_ORDER = "Display Order";
$STR_TABLE_COLUMN_NAME_MANAGE_PHOTOS = "Manage Photos";
$STR_TABLE_COLUMN_NAME_MANAGE_OPTIONS = "Manage Options";
$STR_TABLE_COLUMN_NAME_MANAGE_CLIPS = "Manage Clips";
$STR_TABLE_COLUMN_NAME_AVAILABILITY = "Availability";
$STR_TABLE_COLUMN_NAME_MANAGE_MODULE = "Manage Module";
$STR_TABLE_COLUMN_NAME_MANAGE_PAGE = "Manage Pages";
$STR_TABLE_COLUMN_NAME_MANAGE_LINK = "Manage Links";
$STR_TABLE_COLUMN_NAME_OPTION = "Option";
$STR_TABLE_COLUMN_NAME_SETTING_CAPTION = "Setting Caption";
$STR_TABLE_COLUMN_NAME_SETTING_VALUE = "Setting Value";
$STR_TABLE_COLUMN_NAME_GRANT_PERMISSION = "Grant<br/>Permission";
$STR_TABLE_COLUMN_NAME_IS_SUPER_ADMIN = "Is Super Admin?";
$STR_TABLE_COLUMN_NAME_CREATED_BY = "Created By";
$STR_TABLE_COLUMN_NAME_POSTED_BY = "Posted By";
$STR_TABLE_COLUMN_NAME_COMMENT = "Comments";
$STR_TABLE_COLUMN_NAME_REPLY = "Manage Reply";
$STR_TABLE_COLUMN_NAME_APPROVED = "Approved";
$STR_TABLE_COLUMN_NAME_COMMENTED_BY = "Commented By";
$STR_TABLE_COLUMN_NAME_ALLOW_REPLY = "Allow Reply";
$STR_TABLE_COLUMN_NAME_ALLOW_POST_TOPIC = "Allow Post Topic";
$STR_TABLE_COLUMN_NAME_USER_TYPE = "User Type";


$STR_TABLE_COLUMN_NAME_PRICE = "Price";
$STR_TABLE_COLUMN_NAME_SIZE = "Size";
$STR_TABLE_COLUMN_NAME_BUST = "Bust";
$STR_TABLE_COLUMN_NAME_WAIST = "Waist";
$STR_TABLE_COLUMN_NAME_HIP = "Hip";
$STR_TABLE_COLUMN_NAME_COLOR = "Color";
$STR_TABLE_COLUMN_NAME_COLOR_CODE = "Hex Color <br/>Code";
$STR_TABLE_COLUMN_NAME_DISPLAY_ADDITIONAL = "Display Additional Info";

$STR_TABLE_COLUMN_NAME_MANAGE_STATE = "Manage States";

$STR_TABLE_COLUMN_NAME_ORDER_STATUS = "Order Status";
$STR_TABLE_COLUMN_NAME_USER = "User";
$STR_TABLE_COLUMN_NAME_MINIMUM_ADVANCE_AMOUNT = "Min. Advance Amount (In %)";
$STR_TABLE_COLUMN_NAME_DISCOUNT_AMOUNT = "Discount";
$STR_TABLE_COLUMN_NAME_DISCOUNT = "Discount (In %)";
$STR_TABLE_COLUMN_NAME_COMMISSION = "Commission (In <i class='fa fa-inr'></i>)";
$STR_TABLE_COLUMN_NAME_MINIMUM_PURCHASE_AMOUNT = "Min. Purchase value";
$STR_TABLE_COLUMN_NAME_PKID = "pkid";
$STR_TABLE_COLUMN_NAME_CAT_PKID = "Category pkid";
$STR_TABLE_COLUMN_NAME_SUBCAT_PKID = "Sub Category pkid";
$STR_TABLE_COLUMN_NAME_UNFAVORITE = "UnFavorite";
$STR_TABLE_COLUMN_NAME_COURIER_PRICE = "Courier Price<br/>(per kg.)";
