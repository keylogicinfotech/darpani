<?php

/*	This file contains the useful classes and functions for manipulating the database*/

/*
*	Class name : RecordSet
*   Purpose : Purpose of this class is to manipulating the RecordSet
*/
class RecordSet /* start of the recordset class */
{
	var $conn;
	var $result;
	var $RecNo;
	var $RecArray;
	/*
		*  function name : MoveFirst()
		*  purpose : move the cursor at the first record
		*  Return : true on suceess or false on failure
		*/
	function MoveFirst()
	{
		if (mysqli_data_seek($this->result, 0)) {
			$this->RecNo = 0;
			$this->RecArray = mysqli_fetch_array($this->result, MYSQLI_BOTH);
			return true;
		} else
			return false;
	}

	/*
		*  function name : MovePrev()
		*  purpose : move the cursor at the Previous record
		*  Return : true on suceess or false on failure
		*/
	function MovePrev()
	{
		/*if(mysqli_data_seek($this->result,$this->RecordNo))
			{
				$this->RecNo = $this->RecNo -1;
				$this->RecArray=mysqli_fetch_array($this->result, MYSQLI_BOTH);
				return true;
			}
			else
				return false;*/

		if ($this->RecNo != 0) {
			if (mysqli_data_seek($this->result, $this->RecordNo() - 1)) {
				$this->RecNo = $this->RecNo - 1;
				$this->RecArray = mysqli_fetch_array($this->result, MYSQLI_BOTH);
				return true;
			} else
				return false;
		} else
			return false;
	}

	/*
		*  function name : MoveNext()
		*  purpose : move the cursor at the Next record
		*  Return : true on suceess or false on failure
		*/
	function MoveNext()
	{
		if (!$this->EOF()) {
			$this->RecNo = $this->RecNo + 1;
			if ($this->RecArray = mysqli_fetch_array($this->result, MYSQLI_BOTH)) {
				return true;
			} else
				return false;
		} else
			return false;
	}

	/*
		*  function name : MoveLast()
		*  purpose : move the cursor at the Last record
		*  Return : true on suceess or false on failure
		*/
	function MoveLast()
	{
		if ($this->Count() != 0) {
			if (mysqli_data_seek($this->result, mysqli_num_rows($this->result) - 1)) {
				$this->RecNo = mysqli_num_rows($this->result) - 1;
				$this->RecArray = mysqli_fetch_array($this->result, MYSQLI_BOTH);
				return true;
			}
		} else
			return false;
	}
	/*
		*  function name : Move()
		*  Argument : int (move the cursor at the given no)
		*  purpose : move the cursor at the Last record
		*  Return : true on suceess or false on failure
		*/
	function Move($rowNo)
	{
		if (mysqli_data_seek($this->result, $rowNo)) {
			$this->RecNo = $rowNo;
			$this->RecArray = mysqli_fetch_array($this->result, MYSQLI_BOTH);
			return true;
		} else
			return false;
	}

	/*
		*  function name : RecordNo()
		*  purpose : Get the current recordNo
		*  Return : int (current record no)
		*/
	function RecordNo()
	{
		return $this->RecNo;
	}

	/*
		*  function name : EOF()
		*  purpose : Check whether the record is at the EOF()
		*  Return : true if cursor at the end of file or false otherwise
		*/
	function EOF()
	{
		if ($this->RecNo >= mysqli_num_rows($this->result))
			return true;
		else
			return false;
	}

	/*
		*  function name : Count()
		*  purpose : Getting the total No of record in the RecordSet
		*  Return : int (contains the total no of records)
		*/
	function Count()
	{
		return mysqli_num_rows($this->result);
	}

	/*
		*	function name : Fields()
		*	Purpose : To Get the value of the specific fields.
		*	Argument : int - contains the field index. The value of the field index must be between 0 to No of fields - 1
		*				String - contains the name of the field.
		*	Return : Value of the given field.
		*	Usage : to get the value of the field "3" i.e. $rs->Fields(3)
		*			or to get the value of the "UserName" field i.e. $rs->Fields("UserName")
		*/
	function Fields($fieldName)
	{
		return $this->RecArray[$fieldName];
	}

	/*
		*	function name : FieldCount()
		*	Purpose : To Get the No. of Fields in the current recordSet
		*	return : int contains the no of fields.
		*	Usage : i.e. $rs->FieldCount();
		*/
	function FieldCount()
	{
		return mysqli_num_fields($this->result);
	}

	/*
		*	function name : FieldName()
		*	Purpose : To get the name of the field
		*	argument : int - contains the index of the field. index must be between 0 to no of fields -1
		*	Return : Field name if the index is set properly or false if the index is out of bound.
		*	Usage : i.e. FieldName(2);		
		*/
	function FieldName($index)
	{
		if ($index >= $this->FieldCount())
			return false;
		return mysqli_field_name($this->result, $index);
	}
} /* end of the recordset class */

/*
*	Class Name : Connection
*   Purpose : Create connection with the database.  
*   Constructor : Connection()
*               when this constructor is called by creating the object 
*				it creates the connection with the database.
*    Note : New Constuctor is added, if required, if to connect with the other database then the default database
*/
class Connection /* start of the Connection class */
{
	var $Host;
	var $UserName;
	var $Password;
	var $DatabaseName;
	var $conn;

	function __construct()
	{
		//local or server connection settings
		global $APP_RUN_MODE;
		if (isset($APP_RUN_MODE)) {
			if ($APP_RUN_MODE == "LOCAL") {
				//local settings
				$this->Host = "localhost";
				$this->DatabaseName = "60004darpani";
				$this->UserName = "root";
				$this->Password = "";
			} elseif ($APP_RUN_MODE == "SERVER") {
				//server settings
				$this->Host = "localhost";
				$this->DatabaseName = "emailsca_60004darpani";
				$this->UserName = "emailsca_drpUS19";
				$this->Password = "drpPS19";
			}
		}

		$this->conn = mysqli_connect($this->Host, $this->UserName, $this->Password)
			or die("Could not connect: " . mysqli_error($this->conn));
		mysqli_select_db($this->conn, $this->DatabaseName)
			or die("Could not select database: " . mysqli_error($this->conn));
		mysqli_set_charset($this->conn, 'utf8'); // Some chars are converted into ? without this line

	}
	function ExecuteQuery($SqlQuery)
	{
		return mysqli_query($this->conn, $SqlQuery);
	}
	function MysqliRealEscapeString($val)
	{
		return mysqli_real_escape_string($this->conn, $val);
	}
	function GetRecordSet($SqlQuery)
	{
		$rs = new RecordSet();
		$rs->conn = $this->conn;
		$rs->result = mysqli_query($this->conn, $SqlQuery) or die("Invalid query: " . mysqli_error($this->conn));
		$rs->recNo = 0;
		$rs->RecArray = mysqli_fetch_array($rs->result, MYSQLI_BOTH);
		return $rs;
	}
	function CloseConnection()
	{
		mysqli_close($this->conn);
	}
} /* end of the connection class */

/*
*	When this file included in the file the Connection object is crated by the following code;
*/
$conn = new Connection();
/*
*	Function Name : ExecuteQuery()
*   Argument : String conatins the valid insert/delete/update query
*   return : true on successful execution of the query or false on error on executing the query
*   Purpose : Executing the insert/delete/update query
*   Usage :  i.e. ExecuteQuery("Delete * from table")	
*/
function ExecuteQuery($SqlQuery) /* start of the ExecuteQuery function */
{
	global $conn;
	return $conn->ExecuteQuery($SqlQuery);
} /* end of the ExecuteQuery function */

/*
*	Function Name : GetRecordSet()
*   Argument : String conatins the valid select query
*   return : RecordSet object on successfull Execution of the query or false on error on executing the query	
*   Purpose : Executing the select query and getting the RecordSet object
*   Usage : i.e. GetRecordSet("Select * from table")
*/
function GetRecordSet($SqlQuery) /* start of the GetRecordSet function */
{
	global $conn;
	return $conn->GetRecordSet($SqlQuery);
} /* End of the GetRecordSet function */


/*
*	Function Name : CloseConnection()
*   Argument : null
*   Purpose : Closing the current Active connection
*   Usage : CloseConnection()
*   Note : At the end of the file where this file is included this function must call to free the connection
*/

function CloseConnection() /* start of the CloseConnection function */
{
	global $conn;
	$conn->CloseConnection();
} /* end of the CloseConnection function */

$uploadDocPath = "../data/catalog";
