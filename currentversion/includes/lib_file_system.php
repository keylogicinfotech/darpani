<?php

/*
*	This file contains the useful FileSystem functions.
*/

/*
*	Function Name : RemoveDirectory()
*   Argument : String (contains the valid directory path)
*   Return : true if directory successfully deleted or false if error in deleting the directory
*   Purpose : Delete the given directory. This function delete all the files and the directory in
*             the given directory recursivly and finaly delete the given directory.
*   Usage : To remove the "fldImage" directory and all the contains in that directory 
*           i.e.  RemoveDirectory('./catalog/fldImage') (assume that fldImage is in the catalog directory)
*/
function RemoveDirectory($dir) /* start of the RemoveDirectory() function */ 
{
	if (is_dir($dir)) 
	{
            
	    if ($dh = opendir($dir)) 
		{
        	while (($file = readdir($dh)) !== false) 
			{
				if($file != "." && $file != ".." && is_file($dir ."/". $file))
				{
					unlink($dir ."/". $file);
				}
				elseif($file != "." && $file != ".." && is_dir($dir ."/". $file))
				{
					RemoveDirectory($dir ."/". $file);
				}
			}
			closedir($dh);
			rmdir($dir);
			return true;
		}
		else
			return false;
	}
	else
	{
		return false;
	}
} /* end of the RemoveDirectory() function */


/*
*	Function Name : ClearDirectory()
*   Argument : String (contains the valid directory path)
*   Return : true if all the files successfully deleted from the given directory or false if error
*            in deleting the files
*   Purpose : Delete all the files from the given directory.
*   Usage : To remove all the files from the "fldImage" directory
*           i.e.  DeleteFiles('./catalog/fldImage') (assume that fldImage is in the catalog directory)
*/
function ClearDirectory($dir) /* start of the ClearDirectory() function */
{
	if (is_dir($dir)) 
	{
	    if ($dh = opendir($dir)) 
		{
        	while (($file = readdir($dh)) !== false) 
			{
				if($file != "." && $file != ".." && is_file($dir ."/". $file))
				{	
					if(! unlink($dir ."/". $file))
						return false;
				}
			}
			closedir($dh);
			return true;
		}
		else
			return false;
	}
	else
	{
		return false;
	}
} /* end of the ClearDirectory() function */

/*
*	Function Name : File_Size()
*   Argument : String (contains the valid fileName)
*   Return : int (filesize) in case of success or false in case of error
*   Purpose : Get the Sizeof the File
*   Usage : To get the size of the file "tmp.txt"
*           i.e.  File_Size('./catalog/tmp.txt') 
*/
function File_Size($fileName) /* start of the File_Size() function */
{
	if (file_exists($fileName)) 
  	  return filesize($fileName);
	else
		return false;
} /* end of the FileSize() function */

/*
*	Function Name : Dir_Size()
*   Argument : String (contains the valid Directory Name)
*   Return : int (directory size) in case of success or false in case of error
*   Purpose : Get the Sizeof the Directory 
*   Usage : To get the size of the directory "catalog"
*           i.e.  Dir_Size('./catalog') 
*/
function Dir_Size($dir) /* start of the Dir_Size() function */
{
	if (is_dir($dir)) 
	{
	    if ($dh = opendir($dir)) 
		{
			$dirSize=0;
        	while (($file = readdir($dh)) !== false) 
			{
				if($file != "." && $file != ".." && is_file($dir ."/". $file))
				{
					$dirSize+= File_Size($dir ."/". $file);
				}
				elseif($file != "." && $file != ".." && is_dir($dir ."/". $file))
				{
					$dirSize+=Dir_Size($dir ."/". $file); 
				}
			}
			closedir($dh);
			return $dirSize;
		}
		else
			return false;
	}
	else
	{
		return false;
	}
} /* end of the Dir_Size() function */

/*
*	Function Name : CreateDirectory()
*   Argument : String (contains the valid Directory Name)
*   Return : true if directory successfully  crated or false if error in creating the directory
*   Purpose : Create a new Direcory 
*   Usage : To Create a new Directory "catalog"
*           i.e.  CreateDirectory('./catalog') 
*/
function CreateDirectory($dir) /* start of the CreateDirectory function */
{
	if(file_exists($dir))
		return false;
	mkdir($dir);
	return true;
} /* end of the CreateDirectory() function */


/*
*	Function Name : DeleteFile(string)
*	Purpose : To delete the file
*	Argument : $filename - string contains the valid file path
*	return true if file successfully deleted or false if error in deleting the file		
*	Usage : To delete the file test.jpg
*			DeleteFile("./images/test.jpg");
*/
function DeleteFile($filename) // start of the DeleteFile() function
{
	if (file_exists($filename))
		return unlink($filename);
	else
		return false;
} // end of the DeleteFile() function
#---------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------
/*''	Function Name :- WriteTextFile
''	Prototype :- WriteTextFile($strFilePath,$StrTemplateName)
''	Input Parameter :- 2 Parameters Required. 
						First parameter is full path of file.
						Second parameter is String to be inserted in that file.
''	Return Value :- 1 or 0.
					1 if success, 0 if Fail.
''	Purpose :- This function is used if we want to clear text of some file and insert another
				text into that file.
''	Usage :- WriteTextFile("temp.txt","Welcome to this beautiful world.")	*/
#	Following function is used to remove existing text in text file and write passed text in that file.
	function WriteTextFile($strFilePath,$StrTemplateName)
	{
		$handle = fopen($strFilePath,"w+");
		if(fwrite($handle,$StrTemplateName) == true)
		{
			fclose($handle);
			return 1;
		}
		else
		{
			fclose($handle);
			return 0;
		}
	}
#----------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------
/*''	Function Name :- ReadTextFile
''	Prototype :- ReadTextFile($strFilePath)
''	Input Parameter :- 1 Parameters Required. 
						First parameter is full path of file.
''	Return Value :- returns text of particular file.
''	Purpose :- This function is used to read particular file.
''	Usage :- ReadTextFile("temp.txt")	*/
#	Following function is used to read text file.
	function ReadTextFile($strFilePath)
	{
		$handle = fopen($strFilePath,"r");
		$strContent = fread($handle,filesize($strFilePath));
		fclose($handle);
		return $strContent;
	}
#--------------------------------------------------------------------------------------------------------
/*''	Function Name :- AppendTextFile
''	Prototype :- AppendTextFile($strFilePath,$StrTemplateName)
''	Input Parameter :- 2 Parameters Required. 
						First parameter is full path of file.
						Second parameter is String to be inserted in that file.
''	Return Value :- 1 or 0.
					1 if success, 0 if Fail.
''	Purpose :- This function is used if we want to append text to file. 
				
''	Usage :- AppendTextFile("temp.txt","Welcome to this beautiful world.")	
#	Following function DOES NOT remove existing text in text file and APPENDS passed text in that file.
NOTE: If the file does not exists then it is created.
*/

function AppendTextFile($strFilePath,$StrTemplateName)// Put in lib_file_system.php
{
		$handle = fopen($strFilePath,"a+");
		if(fwrite($handle,$StrTemplateName) == true)
		{
			fclose($handle);
			return 1;
		}
		else
		{
			fclose($handle);
			return 0;
		}
}

#----------------------------------------------------------------------------------------------------------
/*''	Function Name :- read_text_file
''	Prototype :- read_text_file($str_file_path)
''	Input Parameter :- 1 Parameters Required. 
						First parameter is full path of file.
''	Return Value :- returns text of particular file.
''	Purpose :- This function is used to read particular file.
''	Usage :- read_text_file("temp.txt")	*/
#	Following function is used to read text file.
	function read_text_file($str_file_path)
	{
		$handle = fopen($str_file_path,"r");
		$str_content = fread($handle,filesize($str_file_path));
		fclose($handle);
		return $str_content;
	}
	
#----------------------------------------------------------------------------------------------------------

?>
