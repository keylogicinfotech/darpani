<?php
    ##------------------------------------------------ CURRENCY ------------------------------------------------
    $_SESSION["default_cntry"] = "";
    if(isset($_SESSION["usr_cntry"]) == "" || $_SESSION["usr_cntry"] == 0) 
    {  
        $str_query_select = "";
        $str_query_select = "SELECT pkid FROM t_site_country WHERE UPPER(title) = 'INDIA'";
        
        $rs_list_default_country = GetRecordSet($str_query_select);
        
        $_SESSION["default_cntry"] = $rs_list_default_country->Fields("pkid");
        $_SESSION["usr_cntry"] = $rs_list_default_country->Fields("pkid");
        //print $_SESSION["usr_cntry"]; exit;
    }
    else 
    {
        $_SESSION["default_cntry"] = $_SESSION["default_cntry"];
        $_SESSION["usr_cntry"] = $_SESSION["usr_cntry"];
        //$_SESSION["usr_cntry"] = $rs_default_cntry->Fields("countrypkid");
    }
    //print_r($_SESSION);
    
    
    $str_currency_symbol = "";
    $int_conversionrate = 1.00;
    $str_currency_shortform = "";
    $int_courier_price_per_kg = "";

    if(isset($_SESSION["usr_cntry"])) 
    {
        $sel_qry_price = "";
        $sel_qry_price = "SELECT conversionrate, currency_title, currency_symbol, currency_shortform, courierpriceperkg FROM t_site_country WHERE pkid = ".$_SESSION["usr_cntry"];
	//print $sel_qry_price."<br/>";
        $rs_list_currency = GetRecordSet($sel_qry_price);
        if($rs_list_currency->Count() >0)
        {
            $str_currency_symbol = $rs_list_currency->Fields("currency_title");            
	    $str_currency_symbol = $rs_list_currency->Fields("currency_symbol");
            $str_currency_shortform = $rs_list_currency->Fields("currency_shortform");
            $int_conversionrate = $rs_list_currency->Fields("conversionrate");
	    $int_courier_price_per_kg = $rs_list_currency->fields("courierpriceperkg");
        }
    } 
    //print $str_currency_shortform."-".$int_conversionrate."<br/>";
    //print_r($_SESSION);
    ?>

<link rel="shortcut icon" href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/favicon.ico">
<header class="header-area header-wrapper">
    <div class="curency-header-div hidden-xs hidden-sm">
        <div class="container">
            <div class="row currency-header">
            </div>
        </div>
    </div>
    <div class="header-top-bar-mirror hidden-xs hidden-sm">
    </div>
    <div class="header-top-bar">
        <div class="container">
            <div class="desktop-header-panel hidden-xs hidden-sm">
                <div class="row padding-10">
                    <div class="col-md-12 col-lg-12 pr-0" align="center">
                        <div class="company-logo">
                            <img src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/images/logo.png" alt="Darpani Logo" title="Darpani Logo" width="150px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mobile-header-panel container-fluid hidden-md hidden-lg">
      <div class="">
          <div class="row padding-10">
              <div class="col-xs-12 col-sm-12 plr-0" align="center">
                <div class="mobile-view-icon">
                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>"><img src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/images/logo.png" alt="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>"></a>
                </div>
              </div>
          </div>
      </div>
    </div>
    <div class="hidden-md hidden-lg mobile-header-panel-mirror" style="height: 51px; z-index: -9999999999999"></div>
</header>
<!--top top bottom arrow-->
<a href="#" id="scroll" style="display: none;"><span></span></a>
<!--end top top bottom arrow-->
