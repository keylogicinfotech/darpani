<?php
/*	Function Name : UploadFile()
*   Argument : $source :- temporary file name of the uploaded file i.e. $_FILES['txtFile']['tmp_name']
*			   $destination :- destination file name where the uploaded file is saved
*   Return : true if file is successfully uploaded or false if error in Uploading the file
*   Purpose : Upload the file on the server
*   Usage : ToUpad the file 
*          i.e.  UploadFile($_FILES['txtFile']['tmp_name'],'./catalog/newImage.jpg')
*          here 'txtFile' is the name of the file in the form collection
*/
			function UploadFile($source,$destination) /* start of the UploadFile function */
			 {       
//			 	print $source."<br/>";
//				print $destination."";
				
				if(move_uploaded_file($source, $destination)) 
						return true; 
					else
						 return false; 
			} /* end of the UploadFile function */
#------------------------------------------------------------------------------------------------------
// ***  function to get unique filename   ***

        function GetUniqueFileName()  
		{
		  $arr=getdate();
   		  $filename= $arr['year'] . substr("0" . $arr['mon'],-2) . substr("0" . $arr['mday'],-2) . substr("0" . $arr['hours'],-2) . substr("0" . $arr['minutes'],-2) . substr("0" . $arr['seconds'],-2) ;
          return($filename);
		}
// ** end function GetUniqueFilename   **		
#-------------------------------------------------------------------------------------------------------
// **  start of function to get the extension of file from the filename

     function GetExtension($fname)
	 {
	    
		$pos=strpos($fname,".");
		$ext=substr($fname,$pos+1);
		return $ext;
     }		
//  *** End of function GetExtension
#-------------------------------------------------------------------------------------------------------
//***	Begining of GetURLExtension function
#	This function is used to get urlextension. because there might be chances that '.' will occure
#	more than 1 time in given string.
	function GetURLExtension($fname)
	{
		$pos= strrpos($fname,".");
		$ext=substr($fname,$pos+1);
		return $ext;		
	}
//***	End of GetURLEXtension function.
#-------------------------------------------------------------------------------------------------------
//** start of function to get filename of file without extension.
#	This function will return filename without extension.
     function GetFileName($fname)
	 {
	    
		$pos=strpos($fname,".");
		$ext=substr($fname,0,$pos);
		return $ext;
     }
//***	End of function GetFileName
#-------------------------------------------------------------------------------------------------------	
/*''Function Name :- CheckFileExtension($thumbfile,$strpermission)
''	Prototype :- CheckFileExtension($thumbfile,$strpermission)
''	Input Parameter :- 2 Parameters Required.
''	Return Value :- boolean (True or false)
''	Purpose :- This function is used to check whether file is selected of valid extensions or not.
''				it will return TRUE if file is selected with proper extension else FALSE.
''	Usage :- CheckFileExtension('temp.jpeg','jpeg,jpg,png,')	*/
	function CheckFileExtension($filename,$strpermission)
	{
		if(strpos($strpermission,",") === false)
		{
			$strpermission = $strpermission . ",";
		}
		$strext = GetExtension($filename);
		$strposition = strpos(strtolower($strpermission),strtolower($strext) . ",");
		if($strposition === false)
		{
			return 0;
		}
		return 1;
	}
#-------------------------------------------------------------------------------------------------------	
/*''Function Name :- CheckURLFileExtension($thumbfile,$strpermission)
''	Prototype :- CheckURLFileExtension($thumbfile,$strpermission)
''	Input Parameter :- 2 Parameters Required.
''	Return Value :- boolean (True or false)
''	Purpose :- This function is used to check whether file is selected of valid extensions or not for whole url.
''				it will return TRUE if file is selected with proper extension else FALSE.
''	Usage :- CheckFileExtension('temp.jpeg','jpeg,jpg,png,')	*/
	function CheckURLFileExtension($filename,$strpermission)
	{
		if(strpos($strpermission,",") === false)
		{
			$strpermission = $strpermission . ",";
		}
		$strext = GetURLExtension($filename);
		$strposition = strpos(strtolower($strpermission),strtolower($strext) . ",");
		if($strposition === false)
		{
			return 0;
		}
		return 1;
	}

#---------------------------------------------------------------------------------------------------
#''Function Name: sizeoffile()
#''Function Arguments: size of file in bytes
#''Function Returns: Function will return string converting bytes in KB Or MB if file size is less than 1 MB,
#''                  then it will return size in KB otherwise in MB.	
	function sizeoffile($bytes)
	{
		$onemb=1024*1024;
		$sizeinkb=number_format($bytes/1024,2,'.','');
		$sizeinmb=number_format($sizeinkb/1024,2,'.','');
		if ($bytes < $onemb)  		#1048576 bytes
		{
			return($sizeinkb . " KB");
		}
		else
		{
			return($sizeinmb . " MB");
		}
	}
#------------------------------------------------------------------------------------------------------
/*	Function Name : DisplayFileIcon($filename,$iconpath)
*   Argument : $filename :- name of the file.
*			   $iconpath :- path where icon image is stored
*	Purpose : This function finds icon image of file type and returns path of that icon.
*   Return : if image found - image file path, otherwise false.
*/

	function DisplayFileIcon($filename,$iconpath)
	{
		$str_extension="";
		$str_extension=GetExtension($filename);
		$str_filepath=$iconpath.$str_extension.".gif";
		if(!file_exists($str_filepath))
			$str_filepath=$iconpath."sys.gif";
		if(!file_exists($str_filepath))
			return(false);
		return($str_filepath);

	}
#------------------------------------------------------------------------------------------------------
/*	Function Name : ValidateImageExtension($str_image,$str_permission,$str_allow="")
*   Argument : $str_image* :- name of the file, passed not directly as filename but as $_FILES['filename']['tmp_name'].
*			   $str_permission* :- extension to be allowed or disallowed, depending upon 3rd parameter.
			   $str_allow :- "YES" if you want to allow the files with extensions passed as 2nd parameter.
			   				 "NO" if you dont want to allow files with the extensions passed as 2nd parameter.
			   IMP. NOTE :- If third parameter is not passed its default value is "YES".
			   				i.e. It would allow the files with extensions passed as 2nd parameter.
''	Return Value :- boolean (True or false)
''	Purpose :- This function is used to check whether file is to be allowd/disallowed for the given extensions.
''				
''	Usage :- To allow file with extensions jpeg,jpg,png you can call fucntion in this format:
				ValidateImageExtension($_FILES['file_large_image']['tmp_name'],'jpeg,jpg,png,','YES') 
										OR
				ValidateImageExtension($_FILES['file_large_image']['tmp_name'],'jpeg,jpg,png,') //coz bydefault third parameter is "YES"
			And if you dont want to allow files with extension jpeg,jpg,png you can call fucntion in this format:
				ValidateImageExtension($_FILES['file_large_image']['tmp_name'],'jpeg,jpg,png,','NO') 
*/

function ValidateImageExtension($str_image,$str_permission,$str_allow="")
{
	$str_ext="";
	if($str_allow=="")
	{
		$str_allow="YES";
	}
	$arr_size=getimagesize($str_image);
	if($arr_size[2]!="")
	{
		switch($arr_size[2])
		{
			case("1"):
				$str_ext="GIF";
				break;
			case("2"):
				$str_ext="JPG";
				break;
			case("3"):
				$str_ext="PNG";
				break;
			case("4"):
				$str_ext="SWF";
				break;
			case("13")://actualy 13 corresponds to SWC extension.
				$str_ext="SWF";
				break;
		}	
		if($str_ext!="")//if file uploaded has one of the above extension then continue else return 0
		{
			$str_flag=true;
			if(strpos($str_permission,",") === false)
			{
				$str_permission = $str_permission . ",";
			}
			$strposition = strpos(strtolower($str_permission),strtolower($str_ext) . ",");
			if($strposition === false)
			{
				$str_flag=false;
			}
	
			if(($str_flag && $str_allow=="YES") || (!$str_flag && $str_allow=="NO"))
			{
				return 1;
			}	
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}	
	}	
	else
	{
		return 0;
	}
}	
#---------------------------------------------------------------------------------------------------
#''Function Name: filesize_formatted()
#''Function Arguments: path of file in string
#''Function Returns: Function will return string that display size of file.	
function filesize_formatted($path)
{
    $size = filesize($path);
    $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $power = $size > 0 ? floor(log($size, 1024)) : 0;
    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
}
?>