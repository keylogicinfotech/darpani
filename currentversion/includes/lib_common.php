<?php header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
/*	
	This file contains similar function like in ASP
*/
/*
*	Function name : Redirect
*   Similar to Asp function : Response.Redirect
*/

function sms($str_customer_ten_digit_number, $msg)
{

	$username = "darpani";
	$password = "darpani";
	$senderid = "DARPNI";
	$url = 'http://182.18.182.46/vendorsms/pushsms.aspx?user=' . $username . '&password=' . $password . '&msisdn=' . $str_customer_ten_digit_number . '&sid=' . $senderid . '&msg="' . $msg . '"&fl=0&gwid=2';
	// $url = 'http://182.18.182.46/vendorsms/pushsms.aspx?user=' . $username . '&password=' . $password . '&msisdn=' . $str_customer_ten_digit_number . '&sid=' . $senderid . '"&msg="' . $msg . '&fl=0&gwid=2';
	// print_r($url);
	// die;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FAILONERROR, true);
	$response_json = curl_exec($ch);
	// var_dump($response_json);
	// print_r($response_json);
	// die;

	// if we put any link or html anchor tag in url then store curl_error in database
	if (curl_errno($ch)) {
		$error_msg = curl_error($ch);
		// $response_json = '{"Error":"Curl_Error"}';
		// var_dump($response_json);

		if (isset($error_msg)) {  // if isset error_msg then store error in database
			$query = "INSERT INTO sms_log(response,status,created_at) VALUES('curl_error','"  . $error_msg .  "',NOW())";
			// echo $query;
			ExecuteQuery($query);
			// die($error_msg);
		}
	}
	curl_close($ch);
	$response = json_decode($response_json, 1);
	// print_r($response);
	// if error in response and api mobile_number,sender_id...etc then store in database

	if (isset($response)) {  // if isset response then store error in database
		if ($response['ErrorCode'] != 000) {
			$query = "INSERT INTO sms_log(response,status,created_at) VALUES('" . $response_json . "','"  . $response['ErrorMessage'] .  "',NOW())";
			ExecuteQuery($query);
		}
	}
}


function Redirect($path) // start of the function
{
	header('Location: ' . $path);
} // end of the function
#--------------------------------------------------------------------------------------------------------
// This function is prepared so that while displaying values on the page if there is null value
//	 of the field value is empty then the row of the page does not remain empty so instead of leaving
//	 them empty display '-' there.

/*''	Function Name :- ifBlank
''	Prototype :- ifBlank(parameter1)
''	Input Parameter :- 1 Parameters Required.
''	Return Value :- string
''	Purpose :- This function takes string as an input. It will find whether string is Null or not.
				if given string is null then it will place SPACE and retrurn string.
''	Usage :- IfBlank(parameter1)	*/

function ifBlank($strstring)
{
	$srcstring = "";
	$srcstring = $strstring;
	if (strlen(trim($srcstring)) == 0 || empty($srcstring))
		$srcstring = " ";
	return $srcstring;
}

//  *** end of function ifBlank
#------------------------------------------------------------------------------------------------------
/*''	Function Name :- validateURL
''	Prototype :- validateURL(parameter1)
''	Input Parameter :- 1 Parameters Required.
''	Return Value :- boolean
''	Purpose :- This function takes string as an input. It will check whether string is valid URL or not.
				if given string is a valid URL then it will retrurn true otherwise false.
''	Usage :- validateURL(parameter1)	*/
/*function validateURL($strstring)
	{
		$strstring = strtolower($strstring); 
		$str_substring = substr($strstring,0,8);
		if($str_substring=="https://" || substr($str_substring,0,7)=="http://")
		{
			return true; 
		}
		return false;
	}*/

#New Function Definations from 03-FEB-2017
/*function validateURL($strstring)
	{ 
            if(preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$strstring))
            
            {
                return true;
            }
            else
            {
                return false;
            }
                        
	}*/

#New Function Definations from 26-SEP-2018
function validateURL($strstring)
{
	if (preg_match('^((https?:[/][/])?(\w+[.])+com|((https?:[/][/])?(\w+[.])+com[/]|[.][/])?\w+([/]\w+)*[-a-z0-9+&@#\/%=~_|]/i', $strstring) === 0) {
		return false;
	} else {
		return true;
	}
}

#For inner pages after login
function validateURL02($strstring)
{
	if (preg_match("@^https?://@", $strstring)) {
		return true;
	} else {
		return false;
	}
}


#------------------------------------------------------------------------------------------------------
/*''	Function Name :- validateEmail
''	Prototype :- validateEmail(parameter1)
''	Input Parameter :- 1 Parameters Required.
''	Return Value :- boolean
''	Purpose :- This function takes string as an input. It will check whether string is valid email or not.
				if given string is a valid email then it will retrurn true otherwise false.
''	Usage :- validateEmail(parameter1)	*/
function validateEmail($strstring)
{
	if (preg_match("/^([-a-zA-Z0-9._]+@[-a-zA-Z0-9.]+(\.[-a-zA-Z0-9]+)+)*$/", $strstring)) {
		return true;
	} else {
		return false;
	}
}
#------------------------------------------------------------------------------------------------------
/*''	Function Name :- validateVedioFileTitle
''	Prototype :- validateVedioFileTitle(parameter1)
''	Input Parameter :- 1 Parameters Required.
''	Return Value :- boolean
''	Purpose :- This function takes string as an input. It will check whether string is valid filename or not.
				if given string is a valid email then it will retrurn true otherwise false.
''	Usage :- validateVedioFileTitle(parameter1)	*/
function validateVedioFileTitle($strstring)
{
	if (preg("^([a-zA-Z0-9 ]*)$", $strstring)) {
		return true;
	} else {
		return false;
	}
}
#------------------------------------------------------------------------------------------------------
#this function is used to display paging on Listing page.	
if (isset($_REQUEST["PagePosition"]))
	$intRecordPositionPageNum = (int)$_REQUEST["PagePosition"];
else
	$intRecordPositionPageNum = 1;

function setCursor(&$rs, $intRecordsPerPage)
{
	global $intRecordPositionPageNum;
	$rs->Move(($intRecordPositionPageNum - 1) * $intRecordsPerPage);
}
function ScrollBar($rs, $intRecordsPerPage, $FileName)
{
	global $intRecordPositionPageNum;
	$intTotalNumHousePages = ceil($rs->Count() / $intRecordsPerPage);
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="50%" align="center" class="BlueScrolling">
							<?php
							if ($intRecordPositionPageNum > 1 || !$rs->EOF())
								print("Page:&nbsp;&nbsp;");
							//If the House page number is higher than page 1 then display a back link    	
							if ($intRecordPositionPageNum > 1)
								print("&lt;&lt;&nbsp;<a class='BlueScrolling' href='" . $FileName . "?PagePosition=" . ($intRecordPositionPageNum - 1) . "' target='_self'>Prev</a>");
							//If there are more pages to display then display links to all the pages
							if ($intRecordPositionPageNum > 1 || !$rs->EOF()) {
								//Display a link for each page in the guestbook     	
								for ($intLinkPageNum = 1; $intLinkPageNum <= $intTotalNumHousePages; $intLinkPageNum++) {
									//If there is more than 7 pages display ... last page and exit the loop
									if ($intLinkPageNum > 15) {
										if ($intTotalNumHousePages == $intRecordPositionPageNum)
											print(" ...<font size='2'><strong>" . $intTotalNumHousePages . "</strong></font>");
										else
											print(" ...<a  class='BlueScrolling' href='" . $FileName . "?PagePosition=" . $intTotalNumHousePages . "' target='_self'>" . $intTotalNumHousePages . "</a>");
										break;
									} else {
										if ($intLinkPageNum == $intRecordPositionPageNum)
											print("&nbsp;&nbsp;<font size='2'><strong>" . $intLinkPageNum . "</strong></font>");
										else
											print("&nbsp;&nbsp;<a class='BlueScrolling'  href='" . $FileName . "?PagePosition=" . $intLinkPageNum . "' target='_self'>" . $intLinkPageNum . "</a>");
									}
								}
							}
							if (!$rs->EOF() && $intRecordPositionPageNum < $intTotalNumHousePages)
								print("&nbsp;&nbsp;<a class='BlueScrolling' href='" . $FileName . "?PagePosition=" . ($intRecordPositionPageNum + 1) . "' target='_self'>Next</a>&nbsp;&gt;&gt;");
							?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<?php
}
#---------------------------------------------------------------------------------------------------------
# Following function is used to insert specific character at certain position of given string.
/*''	Function Name :- StuffCharacter
''	Prototype :- StuffCharacter($strString,$intPosition,$strCharacter)
''	Input Parameter :- 3 Parameters Required. 
						First parameter is source string.
						Second parameter is position in the string.
						Third parameter is character to be inserted.
''	Return Value :- string
''	Purpose :- This function is mainly used to insert specific character at certain position of
				given string.
				e.g. if we want to insert 'A' after 2 characters in given string 'Temp'. then
				call function such as
				StuffCharacter("Temp",2,"A");
				This function will return value as "TeAmp"
''	Usage :- StuffCharacter("Temp",2,"A")	*/
function StuffCharacter($strString, $intPosition, $strCharacter)
{
	$int_pos = $intPosition;
	$int_len = strlen(trim($strString));
	$str_tmp = trim($strString);
	$str_result = "";
	if ($int_len > $int_pos) {
		while (strlen($str_tmp) > 0) {
			$str_tmp = substr($str_tmp, 0, $intPosition);
			$str_result = $str_result . $str_tmp . $strCharacter;
			$str_tmp = substr(trim($strString), $int_pos);

			$int_pos = $int_pos + $intPosition;

			if ($int_pos > $int_len) {
				$str_result = $str_result . $str_tmp;
				$str_tmp = "";
			}
		}
	} else {
		$str_result = trim($strString);
	}
	return $str_result;
}
#-------------------------------------------------------------------------------------------------------
/*''	Function Name :- CheckSelected
''	Prototype :- CheckSelected(parameter1,parameter2)
''	Input Parameter :- 2 Parameters Required.Both will be compared whether they are equal or not
''	Return Value :- string
''	Purpose :- It will check whether passed two parameters are same or not. If they are same then
''				it will return SELECTED else NOTHING.
''	Usage :- CheckSelected(parameter1,parameter2)	*/

// pre-selected country in my profile section of address 
function CheckSelected($parameter1, $parameter2)
{
	$para1 = trim($parameter1);
	$para2 = trim($parameter2);
	if ($para1 == $para2) {
		return "selected";
	} else {
		return "";
	}
}

#-------------------------------------------------------------------------------------------------
/*''	Function Name :- SelectCheckedMultiple
''	Prototype :- SelectCheckedMultiple($parameter1,$parameter2,$separator)
''	Input Parameter :- 3 Parameters Required.Both will be compared whether they are equal or not
''	Return Value :- string
''	Purpose :- It will find second parameter into the first parameter and checks whether passed second parameter is available in first parameter or not. If available, then
''				it will return SELECTED else NOTHING.
''  Note :- Here, first parameter is a list of values separated by $separator.             
''	Usage :- SelectCheckedMultiple($parameter1,$parameter2,$separator)	*/

function SelectCheckedMultiple($parameter1, $parameter2, $separator)
{

	$para1 = trim($parameter1);
	$para2 = trim($parameter2);

	if (substr($para1, strlen($para1) - 1, 1) != $separator) {
		$para1 = trim($para1) . $separator;
	}
	$arr = explode($separator, $para1);

	for ($c = 0; $c < count($arr) - 1; $c++) {
		if (trim($arr[$c]) === trim($para2)) {
			return "selected";
		}
	}
	return "";
}

#-------------------------------------------------------------------------------------------------
/*''	Function Name :- SelectCheckedMultiple
''	Prototype :- SelectCheckedMultiple($parameter1,$parameter2)
''	Input Parameter :- 2 Parameters Required.Both will be compared whether they are equal or not
''	Return Value :- string
''	Purpose :- It will find second parameter into the first parameter and checks whether passed second parameter is available in first parameter or not. If available, then
''				it will return CHECKED else NOTHING.
''  Note :- Here, first parameter is a list of values separated by semicolon.             
''	Usage :- SelectCheckedMultiple($parameter1,$parameter2)	*/

function CheckSelectedMultiple($parameter1, $parameter2)
{

	$para1 = trim($parameter1);
	$para2 = trim($parameter2);

	if (substr($para1, strlen($para1) - 1, 1) != ";") {
		$para1 = trim($para1) . ";";
	}
	$arr = explode(";", $para1);

	for ($c = 0; $c < count($arr) - 1; $c++) {
		if (trim($arr[$c]) === trim($para2)) {
			return "checked";
		}
	}
	return "";
}



#-------------------------------------------------------------------------------------------------
/*''	Function Name :- SelectChecked
''	Prototype :- SelectChecked($parameter1,$parameter2)
''	Input Parameter :- 2 Parameters Required.Both will be compared whether they are equal or not
''	Return Value :- string
''	Purpose :- It will check whether passed two parameters are same or not. If they are same then
''				it will return CHECKED else NOTHING.
''	Usage :- SelectChecked($parameter1,$parameter2)	*/

function SelectChecked($parameter1, $parameter2)
{
	$para1 = trim($parameter1);
	$para2 = trim($parameter2);
	if ($para1 == $para2) {
		return "checked";
	} else {
		return "";
	}
}
#----------------------------------------------------------------------------------------------------
/*''	Function Name :- heightInCntiMeter
''	Prototype :- heightInCntiMeter(parameter1,parameter2)
''	Input Parameter :- 1 Parameters Required.
''	Return Value :- int
''	Purpose :- This function takes 2 int parameters input. 
				parameter1 is height in foot
				parameter2 is height in inch
				if returns centemeter conversion of feet-inch height
''	Usage :- heightInCntiMeter(parameter1,parameter2)	*/
function heightInCntiMeter($int_height_foot, $int_height_inch = 0)
{
	return ceil((($int_height_foot * 12) + $int_height_inch) * 2.53);
}
#----------------------------------------------------------------------------------------------------
/*''	Function Name :- weightLbsToKg
''	Prototype :- weightKgToLbs(parameter1)
''	Input Parameter :- 1 Parameters Required.
''	Return Value :- int
''	Purpose :- This function takes 2 int parameters input. 
				parameter1 is weight in lbs
				if returns LBS conversion of KG weight
''	Usage :- weightKgToLbs($int_weight)	*/
function weightLbsToKg($int_weight)
{
	return ceil($int_weight / 2.3);
}
#----------------------------------------------------------------------------------------------------
/* If the length of the data is greater than the length of the column then
' we can cut the string in to desired length and append '...' at the end of the final string
' Eg. if the original string is 'TeststringTestString' which is of length 20 characters then cut it to 10
' characters and make it 'Teststr...' 
' Parameters : 1) the original string and 2) length we want */
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
function MakeStringShort($sourcestring, $LenOfDestString)
{
	$strsource = trim($sourcestring);
	$lendest = trim($LenOfDestString);
	if (strlen($strsource) > $lendest) {
		$strsource = substr($strsource, 0, $lendest - 3) . "..";
	}
	return $strsource;
}
#---------------------------------------------------------------------------------------------------------------
#	This function creates a table with message passed as a parameter ($txt).
#	Mainly used to create and display run time table.
/*function MakeBlankTab($wd,$ht=0,$txt,$cssclass)
	{
		if($ht == "")
		{
			$ht = ($wd * 5)/4;
		}	
		$tab = "<table height='" . $ht . "' width='" . $wd . "' cellpadding ='0' cellspacing ='0' border='0' class=" . $cssclass .">";
		$tab = $tab . "<tr><td align='center' valign ='middle'>" . $txt . "</td></tr>";
		$tab = $tab . "</table>";
		return $tab;
	}*/
function MakeBlankTab($wd, $ht = 0, $txt, $cssclass)
{
	if ($ht == "") {
		$ht = ($wd * 5) / 4;
	}
	$tab = "<p class='" . $cssclass . "'>" . $txt . "</p>";
	return $tab;
}
#-------------------------------------------------------------------------------------------------------------------
function MakeBlankTabLink($wd, $ht = 0, $txt, $Bclr)
{
	if ($ht == "") {
		$ht = ($wd * 5) / 4;
	}
	$tab = "<table height='" . $ht . "' width='" . $wd . "' cellpadding ='0' cellspacing ='0' border='0' bgcolor=" . $Bclr . ">";
	$tab = $tab . "<tr><td align='center' valign ='middle'>" . $txt . "</td></tr>";
	$tab = $tab . "</table>";
	return $tab;
}
#-------------------------------------------------------------------------------------------------------------------
#	This function creates a table with message passed as a parameter ($txt).
#	Mainly used to create and display run time table.

function ReplaceNullStringWithDash($str, $character)
{
	if (is_null($str))
		return $character;
	elseif ($str == "")
		return $character;
	elseif (strlen($str) == 0)
		return $character;
	else
		return $str;
}
#------------------------------------------------------------------------------------------------------------
#''	Function Name :- PagingWithMargine
#''	Prototype :- PagingWithMargine($int_total_record,$Margin,$intRecordPositionPageNum,$strLink,$intRecordsPerPage,$ApplyClass,$Querystring)
#''	Input Parameter :- 7 Parameters Required.
#''	(1)$int_total_record (Total no of record with out paging) (2)$Margine. it is used for paging size at the display time e.g. Margine=2 it will display 2 previous pages, 2 next pages and one current.
#'' (3)$intRecordsPositionPageNum. it is as same as absolutepage in asp. (4)$strLink- paging link
#'' (5)$intRecorsPerPage. it is used to define how many records display on pages.
#'' (6)$ApplyClass.(7)$QueryString. if querystring does not exist pass null ("") string as a variable.
#''	Return Value :- It will return String.
#''	Purpose :- It is used for paging on particular page.
#''	Usage :- PagingWithMargine($int_total_record,$Margin,$intRecordPositionPageNum,$strLink,$intRecordsPerPage,$ApplyClass,$Querystring)

/* function PagingWithMargine($int_total_record,$Margine,$intRecordPositionPageNum,$strLink,$intRecordsPerPage,$ApplyClass,$QryStr)
  {
			$str="";
			$TotalPage=ceil($int_total_record / $intRecordsPerPage);
			if($intRecordPositionPageNum > $TotalPage)
			{
				$intRecordPositionPageNum=$TotalPage;
			}
			$TotalDisplay = (($Margine * 2) + 1);
			if ($TotalPage <= $TotalDisplay)
			{
				$StartPosition = 1;
				$EndPosition = $TotalPage;
			}
			elseif ($intRecordPositionPageNum <= $Margine)
			{
				$StartPosition = 1;
				$EndPosition = ((2 * $Margine) + 1);
			}
			elseif (($intRecordPositionPageNum > $Margine) && ($intRecordPositionPageNum <= $TotalPage-$Margine))
			{
				$StartPosition = ($intRecordPositionPageNum - $Margine);
				$EndPosition = ($intRecordPositionPageNum + $Margine);
			}
			elseif ($intRecordPositionPageNum > ($TotalPage - $Margine))
			{
				$StartPosition= ($TotalPage - (2 * $Margine));
				$EndPosition= $TotalPage;
			}
			if ($intRecordPositionPageNum != 1)	
			{
				//$str=$str . "<a href='" . $strLink . "?PagePosition=1&" . $QryStr . "' Class='" . $ApplyClass . "' title='Go to First Page'>&lt;&lt; First Page</a> &nbsp;&nbsp;";
				$str=$str . " <li><span><a href='" . $strLink . "?PagePosition=1&" . $QryStr . "' Class=' " . $ApplyClass . "' title='Go to First Page'> <strong><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i>First Page</strong></a></span></li>";
			}
			else
			{
				$str=$str . "<li><span aria-hidden='true'><a class='link'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i> First Page</a></span></li>";
			}
			if ($intRecordPositionPageNum != 1)
			{
				$str=$str . "<li><span><a href='" . $strLink . "?PagePosition=" . ($intRecordPositionPageNum - 1) . "&" . $QryStr . "' Class=' " . $ApplyClass . "' title='Go to Page " . ($intRecordPositionPageNum - 1) . "'><i class='fa fa-chevron-left'></i>&nbsp;<strong>Prev</strong></a></span></li>";
			}
			else
			{
				$str=$str . "<li><span aria-hidden='true'><a class='link'><i class='fa fa-chevron-left'></i> Prev</a></span></li>";
			}
			for($i=$StartPosition;$i<=$EndPosition;$i++)
			{
				if ($intRecordPositionPageNum != $i)
				{
					$str=$str . "<li><span><a href='" . $strLink . "?PagePosition=" . $i . "&" . $QryStr . "' Class=' ". $ApplyClass . "' title='Go to Page " . $i . "'><strong>" . $i . "</strong></a></span></li>";
				}
				else
				{
					$str=$str . "<li><span aria-hidden='true' class='link'><a class='link'>" . $i . "</a></span></li>" . "  ";
				}
			}
			if ($intRecordPositionPageNum != $TotalPage)
			{
				$str=$str . "&nbsp;&nbsp; <li><span aria-hidden='true'><a href='" . $strLink . "?PagePosition=" . ($intRecordPositionPageNum + 1) . "&" . $QryStr . "' Class=' " . $ApplyClass . "' title='Go to Page ". ($intRecordPositionPageNum + 1) ."'><strong>Next</strong> <i class='fa fa-chevron-right'></i></a></span></li>";
			}
			else
			{
				$str=$str . "<li><span aria-hidden='true'><a class='link'>Next&nbsp;<i class='fa fa-chevron-right'></i></a></span></li>";
			}
			if ($intRecordPositionPageNum != $TotalPage)
			{
				$str=$str . "<li><span aria-hidden='true'><a href='" . $strLink . "?PagePosition=" . $TotalPage . "&" . $QryStr . "' Class=' " . $ApplyClass . "' title='Go to Last Page'> <strong>Last Page</strong><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></span></li>";
			}
			else
			{
				$str=$str . "<li><span aria-hidden='true'><a class='link'>Last Page<i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a></span></li>";
			}
			return $str;
		}*/
function PagingWithMargine($int_total_record, $Margine, $intRecordPositionPageNum, $strLink, $intRecordsPerPage, $ApplyClass, $QryStr)
{
	$str = "";
	$TotalPage = ceil($int_total_record / $intRecordsPerPage);
	if ($intRecordPositionPageNum > $TotalPage) {
		$intRecordPositionPageNum = $TotalPage;
	}
	$TotalDisplay = (($Margine * 2) + 1);
	if ($TotalPage <= $TotalDisplay) {
		$StartPosition = 1;
		$EndPosition = $TotalPage;
	} elseif ($intRecordPositionPageNum <= $Margine) {
		$StartPosition = 1;
		$EndPosition = ((2 * $Margine) + 1);
	} elseif (($intRecordPositionPageNum > $Margine) && ($intRecordPositionPageNum <= $TotalPage - $Margine)) {
		$StartPosition = ($intRecordPositionPageNum - $Margine);
		$EndPosition = ($intRecordPositionPageNum + $Margine);
	} elseif ($intRecordPositionPageNum > ($TotalPage - $Margine)) {
		$StartPosition = ($TotalPage - (2 * $Margine));
		$EndPosition = $TotalPage;
	}



	$str = "<nav aria-label='...'>";
	$str .= "<ul class='pagination'>";





	if ($intRecordPositionPageNum != 1) {
		//$str=$str . "<a href='" . $strLink . "?PagePosition=1&" . $QryStr . "' Class='" . $ApplyClass . "' title='Go to First Page'>&lt;&lt; First Page</a> &nbsp;&nbsp;";
		$str = $str . " <li><a href='" . $strLink . "?PagePosition=1&" . $QryStr . "' Class='" . $ApplyClass . "' title='Go to First Page'> <strong><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i>First Page</strong></a></li>";
	} else {
		$str = $str . "<li class='disabled'><span aria-hidden='true'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i> First Page</span></li>";
	}
	if ($intRecordPositionPageNum != 1) {
		$str = $str . "<li><a href='" . $strLink . "?PagePosition=" . ($intRecordPositionPageNum - 1) . "&" . $QryStr . "' Class='" . $ApplyClass . "' title='Go to Page " . ($intRecordPositionPageNum - 1) . "'><i class='fa fa-chevron-left'></i>&nbsp;<strong>Prev</strong></a></li>";
	} else {
		$str = $str . "<li class='disabled'><span aria-hidden='true'><i class='fa fa-chevron-left'></i> Prev</span></li>";
	}
	for ($i = $StartPosition; $i <= $EndPosition; $i++) {
		if ($intRecordPositionPageNum != $i) {
			$str = $str . "<li><a href='" . $strLink . "?PagePosition=" . $i . "&" . $QryStr . "' Class=' " . $ApplyClass . "' title='Go to Page " . $i . "'><strong>" . $i . "</strong></a></li>";
		} else {
			//$str=$str . "<li><span aria-hidden='true' class='link'><a class='link'>" . $i . "</a></span></li>" . "  ";
			$str = $str . "<li class='active'><span aria-hidden='true'>" . $i . "</span></li>" . "  ";
		}
	}
	if ($intRecordPositionPageNum != $TotalPage) {
		$str = $str . "&nbsp;&nbsp; <li><a href='" . $strLink . "?PagePosition=" . ($intRecordPositionPageNum + 1) . "&" . $QryStr . "' Class='" . $ApplyClass . "' title='Go to Page " . ($intRecordPositionPageNum + 1) . "'><strong>Next</strong> <i class='fa fa-chevron-right'></i></a></li>";
	} else {
		$str = $str . "<li class='disabled'><span aria-hidden='true'>Next&nbsp;<i class='fa fa-chevron-right'></i></span></li>";
	}
	if ($intRecordPositionPageNum != $TotalPage) {
		$str = $str . "<li><a href='" . $strLink . "?PagePosition=" . $TotalPage . "&" . $QryStr . "' Class='" . $ApplyClass . "' title='Go to Last Page'> <strong>Last Page</strong><i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></li>";
	} else {
		$str = $str . "<li class='disabled'><span aria-hidden='true'>Last Page<i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></span></li>";
	}

	$str .= "</ul>";
	$str .= "</nav>";

	return $str;
}

##For Post Method
function PagingWithMargineWithPost($int_total_record, $Margine, $intRecordPositionPageNum, $strLink, $intRecordsPerPage, $ApplyClass, $QryStr)
{
	$str = "";
	$TotalPage = ceil($int_total_record / $intRecordsPerPage);
	if ($intRecordPositionPageNum > $TotalPage) {
		$intRecordPositionPageNum = $TotalPage;
	}
	$TotalDisplay = (($Margine * 2) + 1);
	if ($TotalPage <= $TotalDisplay) {
		$StartPosition = 1;
		$EndPosition = $TotalPage;
	} elseif ($intRecordPositionPageNum <= $Margine) {
		$StartPosition = 1;
		$EndPosition = ((2 * $Margine) + 1);
	} elseif (($intRecordPositionPageNum > $Margine) && ($intRecordPositionPageNum <= $TotalPage - $Margine)) {
		$StartPosition = ($intRecordPositionPageNum - $Margine);
		$EndPosition = ($intRecordPositionPageNum + $Margine);
	} elseif ($intRecordPositionPageNum > ($TotalPage - $Margine)) {
		$StartPosition = ($TotalPage - (2 * $Margine));
		$EndPosition = $TotalPage;
	}
	if ($intRecordPositionPageNum != 1) {
		//$str=$str . "<a href='" . $strLink . "?PagePosition=1&" . $QryStr . "' Class='" . $ApplyClass . "' title='Go to First Page'>&lt;&lt; First Page</a> &nbsp;&nbsp;";




		$str = $str . "<label>"
			. "<form name='frm_paging' id='frm_paging' action='" . $strLink . "' method='POST'>"
			. "<input type='hidden' name='PagePosition' id='PagePosition' value='1' />"
			. "<button class='btn btn-pagination border-redius-remove' type='submit' title='Go to First Page'><b><i class='fa fa-angle-double-left'></i>&nbsp;First</b></button>"
			. "</form>"
			//. "<a href='" . $strLink . "?PagePosition=1&" . $QryStr . "' Class='link " . $ApplyClass . "' title='Go to First Page'> <b><i class='glyphicon glyphicon-menu-left'></i><i class='glyphicon glyphicon-menu-left'></i>First Page</b></a>"
			. "</label>";
	} else {
		$str = $str . "<label><button class='btn btn-pagination disabled border-redius-remove'><i class='fa fa-angle-double-left'></i>&nbsp;First</button></label>";
	}
	if ($intRecordPositionPageNum != 1) {
		$str = $str . "<label>"
			. "<form name='frm_paging' id='frm_paging' action='" . $strLink . "' method='POST'>"
			. "<input type='hidden' name='PagePosition' id='PagePosition' value=" . ($intRecordPositionPageNum - 1) . " />"
			. "<button class='btn btn-pagination border-redius-remove' type='submit' title='Go to Page " . ($intRecordPositionPageNum - 1) . "'><b><i class='fa fa-angle-left'></i>&nbsp;<b>Prev</b></button>"
			. "</form>"
			. ""
			//. "<a href='" . $strLink . "?PagePosition=" . ($intRecordPositionPageNum - 1) . "&" . $QryStr . "' Class='link " . $ApplyClass . "' title='Go to Page " . ($intRecordPositionPageNum - 1) . "'><i class='glyphicon glyphicon-menu-left'></i>&nbsp;<strong>Prev</strong></a>"
			. "</label>";
	} else {
		$str = $str . "<label><button class='btn btn-pagination disabled border-redius-remove'><i class='fa fa-angle-left'></i>&nbsp;Prev</button></label>";
	}
	for ($i = $StartPosition; $i <= $EndPosition; $i++) {
		if ($intRecordPositionPageNum != $i) {
			$str = $str . "<label>"
				. "<form name='frm_paging' id='frm_paging' action='" . $strLink . "' method='POST'>"
				. "<input type='hidden' name='PagePosition' id='PagePosition' value=" . $i . " />"
				. "<button class='btn btn-pagination border-redius-remove' type='submit' title='Go to Page " . $i . "'><b>" . $i . "</b></button>"
				. "</form>"
				. ""
				. ""
				. ""
				//. "<a href='" . $strLink . "?PagePosition=" . $i . "&" . $QryStr . "' Class='link ". $ApplyClass . "' title='Go to Page " . $i . "'><strong>" . $i . "</strong></a>"
				. "</label>";
		} else {
			$str = $str . "<label><button class='btn btn-pagination-active disabled border-redius-remove'>" . $i . "</button></label>" . "";
		}
	}
	if ($intRecordPositionPageNum != $TotalPage) {
		$str = $str . "<label>"
			. "<form name='frm_paging' id='frm_paging' action='" . $strLink . "' method='POST'>"
			. "<input type='hidden' name='PagePosition' id='PagePosition' value=" . ($intRecordPositionPageNum + 1) . " />"
			. "<button class='btn btn-pagination border-redius-remove' type='submit' title='Go to Page " . ($intRecordPositionPageNum + 1) . "'><b>Next</b>&nbsp;<i class='fa fa-angle-right'></i></button>"
			. "</form>"
			. ""
			. ""
			. ""
			. ""
			//. "<a href='" . $strLink . "?PagePosition=" . ($intRecordPositionPageNum + 1) . "&" . $QryStr . "' Class='link " . $ApplyClass . "' title='Go to Page ". ($intRecordPositionPageNum + 1) ."'><strong>Next</strong> <i class='glyphicon glyphicon-menu-right'></i></a>"
			. "</label>";
	} else {
		$str = $str . "<label><button class='btn btn-pagination disabled border-redius-remove'>Next&nbsp;<i class='fa fa-angle-right'></i></button></label>";
	}
	if ($intRecordPositionPageNum != $TotalPage) {
		$str = $str . "<label>"
			. "<form name='frm_paging' id='frm_paging' action='" . $strLink . "' method='POST'>"
			. "<input type='hidden' name='PagePosition' id='PagePosition' value=" . $TotalPage . " />"
			. "<button class='btn btn-pagination border-redius-remove' type='submit' title='Go to Last Page'><b>Last</b>&nbsp;<i class='fa fa-angle-double-right'></i></button>"
			. "</form>"
			. ""
			. ""
			. ""
			//. "<a href='" . $strLink . "?PagePosition=" . $TotalPage . "&" . $QryStr . "' Class='link " . $ApplyClass . "' title='Go to Last Page'> <strong>Last Page</strong><i class='glyphicon glyphicon-menu-right'></i><i class='glyphicon glyphicon-menu-right'></i>"
			. "</label>";
	} else {
		$str = $str . "<label><button class='btn btn-pagination disabled border-redius-remove'>Last&nbsp;<i class='fa fa-angle-double-right'></i></button></label>";
	}
	return $str;
}

#----------------------------------------------------------------------------------------------------
/* 
	Function Name :- Display_Page_Banner()
	Prototype :- Display_Page_Banner($str_pagetag,$str_page_structure)
	Input Parameter :- 1. $str_pagetag is actulay tag in xmlfile,corresponding to page where banner is to be dipslayed.
					   2. $str_page_structure is either ROOT or SUB depending upon where the page lies,
					   	  i.e. directly in "user" folder(ROOT) or in some sub folder(e.g ccbill)(SUB) 					
	Return Value :- Returns string with page banner. 																													 
	Usage :- This fuction is used to display page banner.
	NOTE: To call this function include lib_image.php file.
*/
#----------------------------------------------------------------------------------------------------
function Display_Page_Banner($str_pagetag, $str_page_folder_structure)
{
	$str_main = "";
	$str_main_row = "";
	if ($str_pagetag == "" || $str_page_folder_structure == "") {
		return ($str_main_row);
	}
	global $XML_PAGE_BANNER_FILE_NAME;
	global $UPLOAD_XML_PATH;
	global $UPLOAD_XML_PATH_SUB;
	global $UPLOAD_PG_BANNER_PATH;
	global $UPLOAD_PG_BANNER_PATH_SUB;
	global $INT_PAGE_BANNER; // for image width
	if (strtoupper($str_page_folder_structure) == "ROOT") {
		$str_xml_path = $UPLOAD_XML_PATH;
		$str_banner_path = $UPLOAD_PG_BANNER_PATH;
	} elseif (strtoupper($str_page_folder_structure) == "SUB") {
		$str_xml_path = $UPLOAD_XML_PATH_SUB;
		$str_banner_path = $UPLOAD_PG_BANNER_PATH_SUB;
	}
	$fp_banner = openXMLfile($str_xml_path . $XML_PAGE_BANNER_FILE_NAME);
	$str_main = getTagValue($str_pagetag, $fp_banner);
	closeXMLfile($fp_banner);

	if ($str_main != "") {
		$arr_main = explode("|||", $str_main);
		for ($i = 0; $i < count($arr_main); $i++) {
			$arr_sub = explode("||", $arr_main[$i]);

			$str_page_banner_path = $str_banner_path . $arr_sub[0];
			$str_page_banner_url = $arr_sub[1];
			$str_tooltip = "";
			$str_target = "_blank";
			if ($arr_sub[2] == "NO") {
				$str_target = "_self";
			}
			$str_file_ext = "";
			$str_file_ext = $arr_sub[3];

			if (count($arr_sub) == 5) {
				$str_tooltip = "title='" . $arr_sub[4] . "'";
			}
			if (trim(strtoupper($str_file_ext)) != "SWF") {
				// $str_main_row=$str_main_row."<tr><td align='center' valign='top'><a href='".$arr_sub[1]."'". $str_tooltip." target='".$str_target."'><img src='".$str_banner_path.$arr_sub[0]."'".$str_tooltip." align='middle' alt='".$arr_sub[1]."' border='0'></a></td></tr>";
				$str_main_row = $str_main_row . "<div><a href='" . $str_page_banner_url . "'" . $str_tooltip . " target='" . $str_target . "'>" . ResizeImageTag($str_page_banner_path, 0, 0, 200, "", "", 0) . "</a></div>";
				$str_main_row = $str_main_row . "";

				print "<br/>This is path of image : " . "<img src='" . $str_page_banner_path . "'>";
			} else {
				// $str_main_row=$str_main_row."<tr><td align='center' valign='top'><a href='".$arr_sub[1]."'". $str_tooltip." target='".$str_target."'><img src='".$str_banner_path.$arr_sub[0]."'".$str_tooltip." align='middle' alt='".$arr_sub[1]."' border='0'></a></td></tr>";
				$str_bg_color = "#FFFFFF";
				$str_click_url = "";
				$str_src_path = "";
				$str_width = "";
				$str_height = "";
				$str_object_tag = "";

				list($width, $height) = getimagesize($str_page_banner_path);
				$str_height = " height='" . $height . "'";
				$str_width = " width='" . $width . "'";
				$str_click_mode = "?click_mode=" . $str_target;
				$str_click_tooltip = "&click_tooltip=" . urlencode($str_tooltip);
				$str_click_url = "&click_url=" . urlencode($str_page_banner_url);

				$str_src_path = $str_page_banner_path . $str_click_mode . $str_click_tooltip . $str_click_url;

				$str_object_tag =  "<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0' " . $str_width . $str_height . ">";
				$str_object_tag = $str_object_tag . "<param name='movie' value='" . $str_src_path . "'>";
				$str_object_tag = $str_object_tag . "<param name='quality' value='high'>";
				$str_object_tag = $str_object_tag . "<param name='bgcolor' value='" . $str_bg_color . "'>";
				$str_object_tag = $str_object_tag . "<param name='menu' value='false'>";
				$str_object_tag = $str_object_tag . "<embed src='" . $str_src_path . "' menu='false' quality='high' " . $str_width . $str_height . " bgcolor='" . $str_bg_color . "' type='application/x-shockwave-flash' pluginspage='http://www.macromedia.com/go/getflashplayer'></embed>";
				$str_object_tag = $str_object_tag . "</object>";
				$str_main_row = $str_main_row . "<tr><td align='center' valign='top'>" . $str_object_tag . "</td></tr>";
				$str_main_row = $str_main_row . "<tr><td align='center' valign='middle' height='5'></td></tr>";
			}
		}
	}
	return ($str_main_row);
}
// end of function Display_Page_Banner()


function Display_Page_Banner2($str_pagetag, $str_page_folder_structure)
{
	$str_main = "";
	$str_main_row = "";
	if ($str_pagetag == "" || $str_page_folder_structure == "") {
		return ($str_main_row);
	}
	global $XML_PAGE_BANNER_FILE_NAME;
	global $UPLOAD_XML_PATH;
	global $UPLOAD_XML_PATH_SUB;
	global $UPLOAD_PG_BANNER_PATH;
	global $UPLOAD_PG_BANNER_PATH_SUB;
	global $INT_PAGE_BANNER; // for image width
	if (strtoupper($str_page_folder_structure) == "ROOT") {
		$str_xml_path = $UPLOAD_XML_PATH;
		$str_banner_path = $UPLOAD_PG_BANNER_PATH;
	} elseif (strtoupper($str_page_folder_structure) == "SUB") {
		$str_xml_path = $UPLOAD_XML_PATH_SUB;
		$str_banner_path = $UPLOAD_PG_BANNER_PATH_SUB;
	}
	$fp_banner = openXMLfile($str_xml_path . $XML_PAGE_BANNER_FILE_NAME);
	$str_main = getTagValue($str_pagetag, $fp_banner);
	closeXMLfile($fp_banner);

	if ($str_main != "") {
		$arr_main = explode("|||", $str_main);
		$cnt = 1;
		for ($i = 0; $i < count($arr_main); $i++) {
			$arr_sub = explode("||", $arr_main[$i]);

			$str_page_banner_path = $str_banner_path . $arr_sub[0];
			//print $str_page_banner_path."<br/>";
			$str_page_banner_url = $arr_sub[1];
			$str_tooltip = "";
			$str_target = "_blank";
			if ($arr_sub[2] == "NO") {
				$str_target = "_self";
			}
			$str_file_ext = "";
			$str_file_ext = $arr_sub[3];

			if (count($arr_sub) == 5) {
				$str_tooltip = "title='" . $arr_sub[4] . "'";
			}
			if (trim(strtoupper($str_file_ext)) != "SWF") {
				//print "PATH: ".$str_page_banner_path."<br/>";

				//print "<img src='".$str_page_banner_path."' class='img-responsive photo_border_s' border='0'><br/>";


				print "<div class='col-lg-3 col-md-3 col-sm-3 col-xs-12' align='center' ><a href='" . $str_page_banner_url . "'" . $str_tooltip . " target='" . $str_target . "'><img src='" . $str_page_banner_path . "' class='img-responsive border='0' width='100%'></a><br/></div>";
				//print $i;

				if ($cnt % 4 == 0) {
					print "</div><div class='row padding-10'>";
				}
				/*$str_main_row=$str_main_row."<div class='photo_border_s'><a href='".$str_page_banner_url."'". $str_tooltip." target='".$str_target."'>".ResizeImageTag($str_page_banner_path,0,0,$INT_PAGE_BANNER,$str_page_banner_url,"",0)."</a></div>";
				$str_main_row=$str_main_row."<br/>";	*/
			} else {
				// $str_main_row=$str_main_row."<tr><td align='center' valign='top'><a href='".$arr_sub[1]."'". $str_tooltip." target='".$str_target."'><img src='".$str_banner_path.$arr_sub[0]."'".$str_tooltip." align='middle' alt='".$arr_sub[1]."' border='0'></a></td></tr>";
				$str_bg_color = "#FFFFFF";
				$str_click_url = "";
				$str_src_path = "";
				$str_width = "";
				$str_height = "";
				$str_object_tag = "";

				list($width, $height) = getimagesize($str_page_banner_path);
				$str_height = " height='" . $height . "'";
				$str_width = " width='" . $width . "'";
				$str_click_mode = "?click_mode=" . $str_target;
				$str_click_tooltip = "&click_tooltip=" . urlencode($str_tooltip);
				$str_click_url = "&click_url=" . urlencode($str_page_banner_url);

				$str_src_path = $str_page_banner_path . $str_click_mode . $str_click_tooltip . $str_click_url;

				$str_object_tag =  "<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0' " . $str_width . $str_height . ">";
				$str_object_tag = $str_object_tag . "<param name='movie' value='" . $str_src_path . "'>";
				$str_object_tag = $str_object_tag . "<param name='quality' value='high'>";
				$str_object_tag = $str_object_tag . "<param name='bgcolor' value='" . $str_bg_color . "'>";
				$str_object_tag = $str_object_tag . "<param name='menu' value='false'>";
				$str_object_tag = $str_object_tag . "<embed src='" . $str_src_path . "' menu='false' quality='high' " . $str_width . $str_height . " bgcolor='" . $str_bg_color . "' type='application/x-shockwave-flash' pluginspage='http://www.macromedia.com/go/getflashplayer'></embed>";
				$str_object_tag = $str_object_tag . "</object>";
				$str_main_row = $str_main_row . "<div class='photo_border_s'>" . $str_object_tag . "</div>";
				$str_main_row = $str_main_row . "<br/>";
			}
			$cnt++;
		}
	}
	return ($str_main_row);
}

#------------------------------------------------------------------------------------------------------------
#this function is used to display success message or error message or warning message.
#Updated on 06-MAR-2017

function DisplayMessage($tablesize, $strtextmessage, $strmessagetype)
{
	if (strtoupper($strmessagetype) == "S") {
		$strclass = "alert alert-success";
	} elseif (strtoupper($strmessagetype) == "E") {
		$strclass = "alert alert-danger";
	} elseif (strtoupper($strmessagetype) == "W") {
		$strclass = "alert alert-warning";
	}
	$Tab = "<div  class='" . $strclass . "'>" . $strtextmessage . "";
	$Tab = $Tab . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times</button></div>";
	return $Tab;
}

#older version 
/*function DisplayMessage($tablesize,$strtextmessage,$strmessagetype)
{
	if(strtoupper($strmessagetype) == "S")
	{
		$strclass = "SuccessMessage alert-success";
	}
	elseif(strtoupper($strmessagetype) == "E")
	{
		$strclass = "ErrorMessage alert-danger";
	}
	elseif(strtoupper($strmessagetype) == "W")
	{
		$strclass = "WarningMessage alert-warning";
	}
	$Tab = "<table width='" . $tablesize . "' height='20' border='0' cellspacing='1' cellpadding='0' align='center' bgcolor='#cccccc' >";
	$Tab = $Tab . "<tr bgcolor='lightyellow'>";
	$Tab = $Tab . "<td class='" . $strclass . "' align='center' >" . $strtextmessage . "</td>";
	$Tab = $Tab . "</tr></table>";
	return $Tab;
}*/

#------------------------------------------------------------------------------------------------------------
#this function is used to display success message or error message or warning message.
function DisplayMessageSmall($tablesize, $strtextmessage, $strmessagetype)
{
	if (strtoupper($strmessagetype) == "S") {
		$strclass = "SuccessMessageSmall";
	} elseif (strtoupper($strmessagetype) == "E") {
		$strclass = "ErrorMessageSmall";
	} elseif (strtoupper($strmessagetype) == "W") {
		$strclass = "WarningMessageSmall";
	}
	$Tab = "<table width='" . $tablesize . "' height='12' border='0' cellspacing='1' cellpadding='0' align='center'>";
	$Tab = $Tab . "<tr bgcolor='lightyellow'>";
	$Tab = $Tab . "<td class='" . $strclass . "' align='center' >" . $strtextmessage . "</td>";
	$Tab = $Tab . "</tr></table>";
	return $Tab;
}

#------------------------------------------------------------------------------------------------------------------
#''	Function Name :- MyHtmlEncode
#''	Prototype :- MyHtmlEncode($strstring)
#''	Input Parameter :- 1 Parameters Required.
#''	Return Value :- string
#''	Purpose :- It will be compared whether string is empty or not.
#				If string is not empty, then implement html encode or return same string.
#''	Usage :- MyHtmlEncode($strstring)

function MyHtmlEncode($strstring)
{
	$srcstring = "";
	$srcstring = $strstring;
	if (strlen(trim($srcstring)) == 0 || empty($srcstring) || $srcstring == "") {
		return $srcstring;
	} else {
		$srcstring = htmlentities($srcstring, ENT_QUOTES);
		return $srcstring;
	}
}
#-------------------------------------------------------------------------------------------------------------------
/*
    *	Function Name : ReplaceQuote()
*   Argument :1.$var_str :- add slashes before ', " in string if required.
*   Return : Returns final string with replace quots.
*   Purpose :  This function add slashes before ', " if required.
*   Usage :  This function add slashes before ', " if required.
*/
function ReplaceQuote($var_str)
{
	if (!get_magic_quotes_gpc()) {
		$final_str = addslashes($var_str);
	} else {
		$final_str = $var_str;
	}

	return $final_str;
}

#-------------------------------------------------------------------------------------------------------------------
/*
*	Function Name : RemoveQuote()
*   Argument :1.$var_str :- string from which to remove slashes. 
*   Return : Returns final string with remove slashes if required.
*   Purpose : This function remove slashes if required.
*   Usage :  This function remove slashes if required.
*/
function RemoveQuote($var_str)
{
	if (!get_magic_quotes_gpc()) {
		$final_str = $var_str;
		//print $final_str; exit;
	} else {
		$final_str = stripslashes($var_str);
	}
	//print $final_str;exit;
	return $final_str;
}
#-------------------------------------------------------------------------------------------------------------------
/*
*	Function Name : isValidPassword()
*   Argument :1.password string. 
*   Return : Returns true or false
*   Purpose : This function is used to validate password.
*   Usage :  isValidPassword("password")
*/
function isValidPassword($str_password)
{
	$arr = array();
	$arr = $str_password;
	$max = strlen($arr);
	if (strpos($str_password, "&nbsp;") !== false) {
		return false;
	}
	for ($i = 1; $i < $max; $i++) {
		if ((ord($arr[$i]) > 64 && ord($arr[$i]) < 91) || (ord($arr[$i]) > 96 && ord($arr[$i]) < 123) || (ord($arr[$i]) > 47 && ord($arr[$i]) < 58) || (ord($arr[$i]) == 95)) {
			$flag = 1;
		} else {
			return false;
			exit;
		}
	}
	if ($flag == 1) {
		return true;
	}
}

#-------------------------------------------------------------------------------------------------------------------
# This function return the price with '$'.
/*function GetPriceValue($var_price)
{
	if($var_price!="")
	{
		return "$".number_format($var_price,2,".","");
	}
	else
	{
		return $var_price;
	}
}*/
#Without currency symbol        
function GetPriceValue($var_price)
{
	if ($var_price != "") {
		return "" . number_format($var_price, 2, ".", "");
	} else {
		return $var_price;
	}
}
#-------------------------------------------------------------------------------------------------------------------
/*	Function Name : GetMaxValue($tablename,$colname)
*   Argument :1.table name, 2. field name 
*   Return : Returns max value of the field.
*   Purpose : This function is used to find max value in a particular field.
*   Usage :  GetMaxValue("t_tablename","fieldname")
*/
function GetMaxValue($tablename, $colname)
{
	$int_max = 0;
	$str_query = "select max(" . $colname . ") as maxorder from " . $tablename . ";";
	$rs_max = GetRecordSet($str_query);
	if ($rs_max->eof() != true) {
		$int_max = $rs_max->fields("maxorder") + 1;
		if ($int_max < 0) {
			$int_max = 0;
		}
	}
	return ($int_max);
}

#--------------------------------------------------------------------------------------------------------------------------------------
#Function Name : GetSubcatMaxValue($subcattablename,$pkidcolname,$catpkid,$subcatcolname)
#   Argument :1.table name, 2. field name 3.pkid  4.fieldname
#   Return : Returns max value of the field.
#   Purpose : This function is used to find max value in a particular field.
#   Usage :  GetSubcatMaxValue("t_tablename","fieldname","pkidvalue","subcatcolname")
#	first parameter = sub category table name
#	second parameter = category id column name 
#	third parameter = category pkid 
#	fourth parameter = subcategory displayorder coulmn name

function GetSubcatMaxValue($subcattable, $catidcol, $catid, $subcatcol)
{
	$int_max = 0;
	$str_query = "select max(" . $subcatcol . ") as maxorder from " . $subcattable . " where " . $subcattable . "." . $catidcol . "=" . $catid;
	$rs_max = GetRecordset($str_query);
	if ($rs_max->EOF() != true) {
		$int_max = $rs_max->fields("maxorder") + 1;
	}
	return ($int_max);
}
#---------------------------------------------------------------------------------------------------------------------
/*''Function Name :- ReplaceRecordInNewLine($strString1,$strString2)
''	Prototype :- ReplaceRecordInNewLine($strString1,$strString2)
''	Input Parameter :- 2 Parameters Required.
''	Return Value :- final string after concatenating both the parameters.
''	Purpose :- This function will check wheter first parameter exists or not, if exists than return the second paramter else will not return anything. 
''			   This function is mainly used to display each record in new line. 
''			   $strString1 will be the record from database.
''			   $strString will be most probably the HTML newline character <BR>.	
''	Usage :- ReplaceRecordInNewLine(parameter1,parameter2)	*/

function ReplaceRecordWithCharacter($strString1, $strString2)
{
	$para1 = $strString1;
	$para2 = $strString2;

	if ($para1 != "") {
		return $para2;
	} else {
		return "";
	}
}
#-------------------------------------------------------------------------------------------------------------------
/*''Function Name :- DisplayWebSiteURL
''	Prototype :- DisplayWebSiteURL($strhref,$strtext,$strtarget="_blank",$strlinkclass="",$strtitle="",$strtextclass="",$str_defaulttext="Website URL")
''	Input Parameter :- 1 Parameters Required, 5 optional parameters.
					   $strhref -> url address.
					   $strtext -> display text for url link.
					   $strtarget -> for setting target attribute of <a> tag.
					   $strlinkclass -> class name for url link.
					   $strtitle -> for setting tooltip.
					   $strtextclass->class name for text.
					   $str_defaulttext->default text to be used when $strtext is null.
''	Return Value :- URL string
''	Purpose :- This function creates url link if link is passed else only text is returned.
			   if text is not passed and url is passed then parameter $str_defaulttext is used.
			   if in case $str_defaulttext is also not passed then 'Website Url' is taken as text.
*/

function DisplayWebSiteURL($strhref, $strtext, $strtarget = "_blank", $strlinkclass = "", $strtitle = "", $strtextclass = "", $str_defaulttext = "Website URL")
{
	$strurl = "";
	$str_windowtarget = $strtarget;
	$strtarget = "_blank";
	if (strtoupper($str_windowtarget) == "NO") {
		$strtarget = "_self";
	}
	//print $strtarget; 
	if (trim($strlinkclass) != "") {
		$strlinkclass = " class='" . $strlinkclass . "'";
	}

	if (trim($strtitle) != "") {
		$strtitle = " title='" . $strtitle . "'";
	}

	if (trim($strhref) != "" && trim($strtext) != "") {
		$strurl = "<a href='" . $strhref . "'" . $strlinkclass . $strtitle . " target='" . $strtarget . "'>" . $strtext . "</a>";
	} elseif (trim($strhref) != "" && trim($strtext) == "") {
		if (trim($str_defaulttext) == "") {
			$str_defaulttext = "Website URL";
		}
		$strtext = $str_defaulttext;
		$strurl = "<a href='" . $strhref . "'" . $strlinkclass . $strtitle . " target='" . $strtarget . "'>" . $strtext . "</a>";
	}
	if (trim($strhref) == "" && trim($strtext) != "") {
		if (trim($strtextclass) != "") {
			$strurl = "<span class=" . $strtextclass . ">" . $strtext . "</span>";
		} else {
			$strurl = $strtext;
		}
	}

	return $strurl;
}

#-------------------------------------------------------------------------------------------------------------------
/*''Function Name :- DisplayWebSiteURL
''	Prototype :- DisplayWebsiteUrl1($strhref="",$strtext="",$strtarget="YES",$strtooltip="",$linkclass="",$textclass="",$stuffchar="35")
''	Input Parameter :- 1 Parameters Required, 4 optional parameters.
					   $strhref -> url address.
					   $strtext -> display text for url link.
					   $strtarget -> for setting display url in new window or not.
					   $strtooltip -> for setting tooltip.
					   $linkclass -> Link Class Name.
					   $textclass -> Link text Class Name.					   
					   $stuffchar->display no. of characters in one line
''	Return Value :- URL string
''	Purpose :- This function creates url link.	
*/

/*function DisplayWebsiteUrl($strhref="",$strtext="",$strtarget="YES",$strtooltip="",$linkclass="",$textclass="",$stuffchar="35")
{
	#getting all parameters
	if(trim(strtoupper($strtarget))=="YES")
	{
		$strtarget=" target='_blank'";
	}
	else
	{
		$strtarget=" target='_self'";
	}
	
	if(trim($linkclass)!="")
	{
		$linkclass=" class='".$linkclass."'";
	}
	if(trim($textclass)!="")
	{
		$textclass=" class='".$textclass."'";
	}
	if(trim($strtooltip)!="")
	{
		$strtooltip=" title='".$strtooltip."'";
	}
	if(trim($stuffchar)=="")
	{
		$stuffchar="35";
	}	
#checking conditions and returning all parameters	
	if($strhref=="" && $strtext=="")
	{
		return "";
	}
	if($strhref=="" && $strtext!="")
	{
		return "<span" . $textclass .">" . $strtext . "</span>";
	}
	if($strhref!="" && $strtext=="")
	{
		return "<a href='" . $strhref . "'" . $linkclass . $strtooltip . $strtarget . ">" . StuffCharacter($strhref,$stuffchar,"<br>") . "</a>";
	}
	if($strhref!="" && $strtext!="")
	{
		return "<a href='" . $strhref . "'" . $linkclass . $strtooltip . $strtarget . ">" . $strtext . "</a>";
	}
}*/
#-------------------------------------------------------------------------------------------------------------------


#-------------------------------------------------------------------------------------------------------------------
/*''Function Name :- DisplayWebSiteURL
''	Prototype :- DisplayWebSiteURL($strhref,$strtext,$strtarget="_blank",$strlinkclass="",$strtitle="",$strtextclass="",$str_defaulttext="Website URL")
''	Input Parameter :- 1 Parameters Required, 5 optional parameters.
					   $strhref -> url address.
					   $strtext -> display text for url link.
					   $strtarget -> for setting target attribute of <a> tag.
					   $strlinkclass -> class name for url link.
					   $strtitle -> for setting tooltip.
					   $strtextclass->class name for text.
					   $str_defaulttext->default text to be used when $strtext is null.
''	Return Value :- URL string
''	Purpose :- This function creates url link if link is passed else only text is returned.
			   if text is not passed and url is passed then parameter $str_defaulttext is used.
			   if in case $str_defaulttext is also not passed then 'Website Url' is taken as text.
*/

function DisplayWebSiteURL_USER($strhref, $strtext, $strtarget = "_blank", $strlinkclass = "", $strtitle = "", $strtextclass = "", $str_defaulttext = "Website URL")
{
	$strurl = "";
	$str_windowtarget = $strtarget;
	$strtarget = "_blank";
	if (strtoupper($str_windowtarget) == "NO") {
		$strtarget = "_self";
	}

	if (trim($strlinkclass) != "") {
		$strlinkclass = " class='" . $strlinkclass . "'";
	}

	if (trim($strtitle) != "") {
		$strtitle = " title='" . $strtitle . "'";
	}

	if (trim($strhref) != "" && trim($strtext) != "") {
		$strurl = "<a href='" . $strhref . "'" . $strlinkclass . $strtitle . " target='" . $strtarget . "'>" . $strtext . "</a>";
	} elseif (trim($strhref) != "" && trim($strtext) == "") {
		if (trim($str_defaulttext) == "") {
			$str_defaulttext = "Website URL";
		}
		$strtext = $str_defaulttext;
		$strurl = "<a href='" . $strhref . "'" . $strlinkclass . $strtitle . " target='" . $strtarget . "'>" . $strtext . "</a>";
	}
	if (trim($strhref) == "" && trim($strtext) != "") {
		if (trim($strtextclass) != "") {
			$strurl = "<span class=" . $strtextclass . ">" . $strtext . "</span>";
		} else {
			$strurl = $strtext;
		}
	}

	return $strurl;
}



#-------------------------------------------------------------------------------------------------------------------
/*''Function Name :- DisplayEmail
''	Prototype :- DisplayEmail($strhref,$strtext,$strclass="",$strtitle="")
''	Input Parameter :- 1 Parameters Required, 2 optional parameters.
					   $strhref -> email address.
					   $strtext -> display text for email link or if text is empty then stuffcharacter email id can be sent.
					   $strclass -> class name.
					   $strtitle -> for setting tooltip.
''	Return Value :- email string
''	Purpose :- This function creates email link.	
*/

function DisplayEmail($strhref, $strtext, $strclass = "", $strtitle = "")
{
	if (trim($strclass) != "") {
		$strclass = " class='" . $strclass . "'";
	}

	if (trim($strtitle) != "") {
		$strtitle = " title='" . $strtitle . "'";
	}
	$stremail = "";
	$stremail = "<a href=mailto:" . $strhref . "" . $strclass . $strtitle . ">" . $strtext . "</a>";
	return $stremail;
}
#-------------------------------------------------------------------------------------------------------------------

function DisplayFormatedString($strstart = "", $strvalue, $strend = "")
{
	if (trim($strvalue == "")) {
		return trim($strvalue);
	} else {
		return $strstart . $strvalue . $strend;
	}
}
#-------------------------------------------------------------------------------------------------------------------
// Function Name : isvalidbannerSource   
//	Prototype    : isvalidbannerSource($strString) 
// Input Parameter : strString
// Return value : Boolean
// Purpose : It will check both http and banner file extension
// Usage : isvalidbannerSource($strString) 

function isvalidbannerSource($strString)
{
	$txtval = strtolower($strString);

	if (trim(substr($txtval, 0, 7)) != 'http://' && trim(substr($txtval, 0, 8)) != trim("https://")) {
		return false;
	}
	if (strlen($txtval) <= 10) {
		return false;
	}

	if (strtoupper(substr($txtval, strrpos($txtval, ".") + 1, strlen($txtval))) == "JPG") {
		return true;
	}
	if (strtoupper(substr($txtval, strrpos($txtval, ".") + 1, strlen($txtval))) == "JPEG") {
		return true;
	}
	if (strtoupper(substr($txtval, strrpos($txtval, ".") + 1, strlen($txtval))) == "PNG") {
		return true;
	}
	if (strtoupper(substr($txtval, strrpos($txtval, ".") + 1, strlen($txtval))) == "GIF") {
		return true;
	}
	if (strtoupper(substr($txtval, strrpos($txtval, ".") + 1, strlen($txtval))) == "BMP") {
		return true;
	}
	return false;
}
#-------------------------------------------------------------------------------------------------------------------
// Function Name : validPositiveNumber   
//	Prototype    : validPositiveNumber($no) 
// Input Parameter : number
// Return value : Boolean
// Purpose : It will check for positive number
// Usage : validPositiveNumber($no)

function validPositiveNumber($no)
{
	if (trim($no) != "") {
		if (trim($no) < 0 || is_numeric(trim($no)) == false) {
			return false;
		}
		return true;
	} else {
		return false;
	}
}

#------------------------------------------------------------------------------------------------------
function get_image_text($length)
{
	$ret_char = "";
	$ret_string = "";
	for ($i = 0; $i < $length; $i++) {
		mt_srand((float)microtime() * 1000000);
		$ret_char = mt_rand(1, 8); // mt_rand(1,8) indicates below 8 cases

		switch ($ret_char) {
				// 2 to 9 numberic numbers
			case 1:
				$ret_char = mt_rand(50, 57);
				break;
				// Capital letters - A to H 
			case 2:
				$ret_char = mt_rand(65, 72);
				break;
				// Capital letters - J to N
			case 3:
				$ret_char = mt_rand(74, 78);
				break;
				// Capital letters - P to Z
			case 4:
				$ret_char = mt_rand(80, 90);
				break;
				// Small letter - a to h
			case 5:
				$ret_char = mt_rand(97, 104);
				break;
				// Small letter - j to k
			case 6:
				$ret_char = mt_rand(106, 107);
				break;
				// Small letter - m to n
			case 7:
				$ret_char = mt_rand(109, 110);
				break;
				// Small letter - p to z
			case 8:
				$ret_char = mt_rand(112, 122);
				break;
		}
		$ret_string .= chr($ret_char);
	}
	return $ret_string;
}
#-------------------------------------------------------------------------------------------------------------------
/*
*	Function Name : is_valid_userid()
*   Argument :1.password string. 
*   Return : Returns true or false
*   Purpose : This function is used to validate password.
*   Usage :  is_valid_userid("password")
*/
function is_valid_userid($str_userid)
{
	if ($str_userid == "") {
		return false;
	}
	if (strpos($str_userid, "&nbsp;") !== false || strpos($str_userid, " ") !== false) {
		return false;
	}
	if (preg_match("/^([a-zA-Z0-9][a-zA-Z0-9_]*)$/i", $str_userid) === false) {
		return false;
	}
	return true;
}
#------------------------------------------------------------------------------------------------------------------------------------------      
# function to generate random key of a specific length $iv_len
function get_rnd_iv($iv_len)
{
	$iv = '';
	while ($iv_len-- > 0) {
		$iv .= chr(mt_rand() & 0xff);
	}
	return $iv;
}
#------------------------------------------------------------------------------------------------------------------------------------------
# function to encrypt text 
# parameters
# $plain_text -- text to be encrypted
# $password -- key to encrypt text
# $iv_len -- length of randomized key to be generated for encryption ( optional parameter , its default value is 16)
#------------------------------------------------------------------------------------------------------------------------------------------
function md5_encrypt($plain_text, $password, $iv_len = 16)
{
	$plain_text .= "\x13";
	$n = strlen($plain_text);
	if ($n % 16) $plain_text .= str_repeat("\0", 16 - ($n % 16));
	$i = 0;
	$enc_text = get_rnd_iv($iv_len);
	$iv = substr($password ^ $enc_text, 0, 512);
	while ($i < $n) {
		$block = substr($plain_text, $i, 16) ^ pack('H*', md5($iv));
		$enc_text .= $block;
		$iv = substr($block . $iv, 0, 512) ^ $password;
		$i += 16;
	}
	return base64_encode($enc_text);
}
#------------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------	
/*
*	Function Name : GetDomainName()
*   Argument :1.full url string. 
*   Return : Returns domain name
*   Purpose : This function is used to get domainname from url.
*   Usage :  GetDomainName("http://www.domainname.com")
	will return string domainname.com
*/


function GetDomainName($str_dname)
{
	#check whether string contains "//" or not.	
	$int_pos = strpos($str_dname, "//");
	if ($int_pos === false) {
		return $str_dname;
	}
	#get string starting after "//"	till end.
	$str_sub_str = substr($str_dname, ($int_pos + 2));
	#check whether string contains "www" & if yes then get string staring after "www." till end
	if (strtoupper(substr($str_sub_str, 0, 3)) == "WWW") {
		$str_sub_str = substr($str_sub_str, 4);
	}
	#get string till the slash(if found) else till end.
	$int_pos = strpos($str_sub_str, "/");
	if ($int_pos === false) {
		$str_sub_str = substr($str_sub_str, 0);
	} else {
		$str_sub_str = substr($str_sub_str, 0, $int_pos);
	}

	return $str_sub_str;
}

#----------------------------------------------------------------------------------------------------
function ImagePaging($int_total_rec, $int_margin, $int_cnt, $str_link, $str_link_class, $str_querystring = "")
{
	#making image paging string.
	$str_display_block = "";
	$str_display_links = "";
	$int_photopkid = "";
	$arr_return = "";
	if ($int_total_rec == "" || $int_margin == "" || $int_cnt == "") {
		$arr_return = array('', '');
		return ($arr_return);
	}

	$int_block_size = ceil($int_total_rec / $int_margin);
	if ($int_total_rec != $int_margin && $int_margin != 1 && $int_margin < $int_total_rec) {
		for ($i = 1; $i <= $int_block_size; $i++) {
			$str_seperator = "";
			$int_start = ($i - 1) * ($int_margin) + 1;
			$int_end = $int_start + ($int_margin - 1);
			if ($i != $int_block_size) {
				$str_seperator = " | ";
			}
			if ($int_end > $int_total_rec) {
				$int_end = $int_total_rec;
			}
			if ($int_start == $int_end) {
				if ($int_cnt != $int_end) {
					$str_display_block = $str_display_block . "<a href='" . $str_link . $str_querystring . "&recno=" . ($int_end - 1) . "' class='" . $str_link_class . "'>" . $int_end . "</a>";
				} else {
					$str_display_block = $str_display_block . $int_end;
					$arr_start_end = array($int_start, $int_end);
				}
			} else {
				if ($int_start <= $int_cnt && $int_cnt <= $int_end) {
					$str_display_block = $str_display_block . $int_start . "-" . $int_end . $str_seperator;
					$arr_start_end = array($int_start, $int_end);
				} else {

					$str_display_block = $str_display_block . "<a href='" . $str_link . $str_querystring . "&recno=" . ($int_start - 1) . "' class='" . $str_link_class . "'>" . $int_start . "-" . $int_end . "</a>" . $str_seperator;
				}
			}
		} //end of for
		for ($j = $arr_start_end[0]; $j <= $arr_start_end[1]; $j++) {
			$str_sub_seperator = "";
			if ($j != $arr_start_end[1]) {
				$str_sub_seperator = " | ";
			}
			if ($j == ($int_cnt)) {
				$str_display_links = $str_display_links . $j . $str_sub_seperator;
			} else {
				$str_display_links = $str_display_links . "<a href='" . $str_link . $str_querystring . "&recno=" . ($j - 1) . "' class='" . $str_link_class . "'>" . $j . "</a>" . $str_sub_seperator;
			}
		}
	} //end of main if	
	else {
		for ($i = 1; $i <= $int_total_rec; $i++) {
			$str_sub_seperator = "";
			if ($i != $int_total_rec) {
				$str_sub_seperator = " | ";
			}
			if ($i == ($int_cnt)) {
				$str_display_links = $str_display_links . $i . $str_sub_seperator;
			} else {
				$str_display_links = $str_display_links . "<a href='" . $str_link . $str_querystring . "&recno=" . ($i - 1) . "' class='" . $str_link_class . "'>" . $i . "</a>" . $str_sub_seperator;
			}
		}
	} //end of main else
	$arr_return = array($str_display_block, $str_display_links);
	return ($arr_return);
}

#----------------------------------------------------------------------------------------------------

/*
*	Function Name : is_valid_userpassword()
*   Argument :1.password string. 
*   Return : Returns true or false
*   Purpose : This function is used to validate password.
*   Usage :  is_valid_userpassword("password")
	IMPORTANT NOTE: is_valid_userpassword & isValidPassword ARE DIFFERENT.
*/
function is_valid_userpassword($str_userpassword)
{
	if ($str_userpassword == "") {
		return false;
	}
	if (strpos($str_userpassword, "&nbsp;") !== false || strpos($str_userpassword, " ") !== false) {
		return false;
	}
	if (strlen($str_userpassword) < 6) {
		return false;
	}
	if (!preg_match("#[0-9]+#", $str_userpassword)) {
		return false;
	}
	if (!preg_match("#[A-Z]+#", $str_userpassword)) {
		return false;
	}
	if (!preg_match("#[a-z]+#", $str_userpassword)) {
		return false;
	}

	return true;
}
#----------------------------------------------------------------------------------------------------
/* 
	Function Name :- validateHTMLTag()
	Prototype :- validateHTMLTag($arr,$str_desc)
	Input Parameter :- 	1. $arr array of html tags
						2. $str_desc is description as entered from user side page.
	Return Value :- Returns string from which all invalid tags and its values are deleted.					
	Usage :- This fuction is used validating description to restrict certain HTML tags.
*/
#----------------------------------------------------------------------------------------------------
function validateHTMLTag($arr, $str_desc)
{
	$str_result = $str_desc;
	for ($i = 0; $i < count($arr); $i++) {
		do {
			$int_pos1 = strpos(strtoupper($str_desc), strtoupper($arr[$i]));
			$end_tag = "</" . substr($arr[$i], 1) . ">";
			$int_pos2 = strpos(strtoupper($str_desc), strtoupper($end_tag));
			if ($int_pos2 === false) {
				$end_tag = "/>";
				$int_pos2 = strpos(strtoupper($str_desc), strtoupper($end_tag));
			}

			if ($int_pos2 === false) {
				$temp = $int_pos1 + strlen($arr[$i]);
			} else {
				$temp = $int_pos2 + strlen($end_tag);
			}

			if ($int_pos1 !== false) {
				$str_result = substr($str_desc, 0, $int_pos1);
				$str_result .= substr($str_desc, $temp);
				$str_desc = $str_result;
			}
		} while ($int_pos1 !== false);
	}
	return ($str_result);
}

#------------------------------------------------------------------------------------------------------
/*
*	Function Name : GetEncryptId()
*   Argument : $int_id :- value should be Numeric*			   
*   Return value Format : Encrypted Id
*   Purpose : To Encrypt the id 
*   Usage : 
*          e.g,  GetEncryptId(15)
*          		 here 15 is Id to Encrypt
*		OutPut :XXXXXXXXXXXXXXXXX
*		in above out put X = any number between (1 to 9) 
*/
function GetEncryptId($int_id)
{
	$int_rand1 = "";
	$int_rand2 = "";
	$int_result = "";

	for ($i = 1; $i <= 6; $i++)
		$int_rand1 = $int_rand1 . rand(1, 9);

	for ($i = 1; $i <= 9; $i++)
		$int_rand2 = $int_rand2 . rand(1, 9);

	$int_result = $int_rand1 . $int_id . $int_rand2;
	return $int_result;
}

#-------------------------------------------------------------
/*
*	Function Name : GetDecryptId()
*   Argument : $int_id :- value should be Numeric			   
*   Return value Format : Decrypted Id
*   Purpose : To Decrypt the id 
*   Usage : 
*          e.g,  GetEncryptId(XXXXXXXXXXXXXXXXX)
*			   here X = Encrypted Id         
*			OutPut : Decrypted Id		
*/
function GetDecryptId($int_id)
{
	$int_result = substr($int_id, 6, strlen($int_id) - 15);
	return $int_result;
}

#------------------------------------------------------------------------------------------------------------------------------------------
# function to decrypt text 
##################### parameters ###########################################
# $plain_text -- text to be decrypted ( cypher text)
# $password -- key to decrypt text (should be same as when encrypted)
# $iv_len -- length of randomized key to be generated for encryption ( optional parameter , its default value is 16)
#------------------------------------------------------------------------------------------------------------------------------------------
function md5_decrypt($enc_text, $password, $iv_len = 16)
{
	$enc_text = base64_decode($enc_text);
	$n = strlen($enc_text);
	$i = $iv_len;
	$plain_text = '';
	$iv = substr($password ^ substr($enc_text, 0, $iv_len), 0, 512);
	while ($i < $n) {
		$block = substr($enc_text, $i, 16);
		$plain_text .= $block ^ pack('H*', md5($iv));
		$iv = substr($block . $iv, 0, 512) ^ $password;
		$i += 16;
	}
	return preg_replace('/\\x13\\x00*$/', '', $plain_text);
}
#------------------------------------------------------------------------------------------------------------------------------------------

#''	Function Name :- PagingWithMargine
#''	Prototype :- PagingWithMargine($Recordset,$intRecordPositionPageNum,$strLink,$Querystring)
#''	Input Parameter :- 5 Parameters Required.
#''	(1)$Recordset 
#'' (2)$intRecordsPositionPageNum. it is as same as absolutepage in asp. (3)$strLink- paging link
#''	(5)$intRecordsPerPage - records per page to be displayed
#''	Return Value :- It will return String.
#''	Purpose :- It is used for paging on particular page.
#''	Usage :- paging_with_margine_test($int_record_count,$int_current_page_num,$str_link,$qry_str,$int_records_per_page)

function paging_with_margine_difference($int_record_count, $int_current_page_num, $str_link, $qry_str, $int_records_per_page)
{
	// START - Function's Configurations
	# Links that should be displayed before and after pagenumber links
	$int_links = 3;
	# Number which should be deferred between two links 
	$int_diff = 5;
	# Class name pf paging links
	$str_apply_class = "NavigationLink";
	// END - Function's Configurations

	// $int_start_position = "" ; $int_end_position = "" ;
	$flag = 0;
	$page = 0;
	$str = "";
	$int_links_before = "";
	$int_links_after = "";
	$int_current_link_number = "";
	$int_total_before_links = 0;
	$int_total_after_links = 0;
	$int_link_before_init = "";
	$int_link_after_end = "";

	if (isset($_GET["last"]) && trim($_GET["last"]) != "") {
		$flag = trim($_GET["last"]);
	}
	if (isset($_GET["page"]) && trim($_GET["page"]) != "") {
		$page = trim($_GET["page"]);
	}

	if ($page > 1) {
		$page = 1;
	} else if ($page < 0) {
		$page = 0;
	}

	if ($page == 0) {
		$int_current_page_num = $int_current_page_num - 1;
	}

	$int_links_display = $int_links * 2 + 1;
	$int_total_page = ceil($int_record_count / $int_records_per_page);
	$int_total_links = ceil($int_total_page / $int_diff);
	if ($int_current_page_num > $int_total_page) {
		$int_current_page_num = $int_total_page;
	}

	if (($int_current_page_num % $int_diff == 0) && ($page == 1)) {
		$flag = 1;
	} else {
		$flag = 0;
	}

	if ($flag == 1) {
		$int_current_link =  ($int_current_page_num - ($int_current_page_num % $int_diff)) - $int_diff;
	} else {
		$int_current_link =  ($int_current_page_num - ($int_current_page_num % $int_diff));
	}
	$int_current_link_number = ($int_current_link / $int_diff) + 1;

	$int_links_after = $int_total_links - $int_current_link_number;
	$int_links_before = $int_current_link_number - 1;

	$int_links_before_avail = $int_links_before;
	$LinksAfterAvail = $int_links_after;

	if ($int_links_before > $int_links) {
		$int_links_before = $int_links;
	}

	if ($int_links_after > $int_links) {
		$int_links_after = $int_links;
	}

	# LINKS BEFORE
	for ($i = $int_links_before; $i >= 1; $i--) {
		$int_total_before_links = $int_total_before_links + 1;
		$j = $int_current_link - $i * $int_diff;
		if ($j == 0) {
			$j = 1;
			$a = $j;
		} else {
			$a = $j + 1;
		}

		if ($int_total_before_links == 1) {
			$int_link_before_init = $j;
		}
		$b = $a + $int_diff - 1;
		$tooltip = $a . " To " . $b;
		$str = $str . "<a href='" . $str_link . "?page_position=" . $a . "&page=0" . $qry_str . "' Class='" . $str_apply_class . "' title='Go to Page " . $tooltip . "'>" . $j . "</a>&nbsp;&nbsp;";
	}

	$str .= "&nbsp;&gt;&gt;&nbsp;&nbsp;&nbsp;";

	# LINKS BETWEEN
	for ($i = $int_current_link + 1; $i <= ($int_current_link + $int_diff); $i++) {
		if ($i > $int_total_page) {
			break;
		}

		if ($i == ($int_current_link + 1) && $page == 0) {
			$str = $str . "" . $i . "&nbsp;&nbsp;";
			continue;
		}

		if ($page == 1 && $i == $int_current_page_num) {
			$str = $str . "" . $i . "&nbsp;&nbsp;";
		} elseif ($i == ($int_current_link + $int_diff)) {
			$str = $str . "<a href='" . $str_link . "?page_position=" . $i . "&page=1&last=1" . $qry_str . "' Class='" . $str_apply_class . "' title='Go to Page " . $i . "'>" . $i . "</a>&nbsp;";
		} else {
			$str = $str . "<a href='" . $str_link . "?page_position=" . $i . "&page=1" . $qry_str . "' Class='" . $str_apply_class . "' title='Go to Page " . $i . "'>" . $i . "</a>&nbsp;";
		}
	}
	$str .= "&nbsp;&nbsp;&lt;&lt;&nbsp;&nbsp;";
	# LINKS AFTER
	for ($i = 1; $i <= $int_links_after; $i++) {
		$int_total_after_links = $int_total_after_links + 1;
		$j = ($int_current_link + ($i * $int_diff)) . "&nbsp;";
		$d = $j + $int_diff;

		if ($i == $int_links_after) {
			$int_link_after_end = $j;
		}

		$a = $j + 1;
		//$b=$a+4;
		$b = $a + $int_diff - 1;
		$tooltip = $a . " To " . $b;
		$str = $str . "<a href='" . $str_link . "?page_position=" . $a . "&page=0" . $qry_str . "' Class='" . $str_apply_class . "' title='Go to Page " . $tooltip . "'>" . $j . "</a>&nbsp;";
	}

	# ADDITIONAL
	if ($int_total_before_links == $int_links && $int_total_after_links < $int_links) {
		if ($int_total_links > $int_links_display)
			$int_to_add_before = $int_links - $int_total_after_links;
		else
			$int_to_add_before = $int_links_before_avail - $int_links_before;


		$res = $int_to_add_before + $int_links_before;

		//if($LinksBeforeAvail >= $ToAddBefore + $LinksBefore ) 
		if ($int_links_before_avail >= $res) {
			$temp = "";

			for ($i = $int_to_add_before; $i >= 1; $i--) {
				$j = $int_link_before_init - ($i * $int_diff);

				if ($j == 0) {
					$j = 1;
					$a = $j;
				} else {
					$a = $j + 1;
				}
				$b = $a + $int_diff - 1;

				$tooltip = $a . " To " . $b;
				$temp .= "<a href='" . $str_link . "?page_position=" . $a . "&page=0" . $qry_str . "' Class='" . $str_apply_class . "' title='Go to Page " . $tooltip . "'>" . $j . "</a>&nbsp;";
			}
			$str = $temp . $str;
		}
	} elseif ($int_total_after_links == $int_links && $int_total_before_links < $int_links) {
		$int_to_add_after = $int_links - $int_total_before_links;
		if ($int_to_add_after <= $LinksAfterAvail) {
			$temp = "";
			for ($i = 1; $i <= $int_to_add_after; $i++) {
				$j = $int_link_after_end + ($i * $int_diff);
				if ($j == 1) {
					$a = $j;
				} else {
					$a = $j + 1;
				}
				$b = $a + $int_diff - 1;
				if ($j > $int_total_page)
					break;
				$tooltip = $a . " To " . $b;
				$temp .= "<a href='" . $str_link . "?page_position=" . $a . "&" . $qry_str . "' Class='" . $str_apply_class . "' title='Go to Page " . $tooltip . "'>" . $j . "</a>&nbsp;";
			}
			$str .= $temp;
		}
	}
	if ($int_current_page_num == 0 || $int_current_page_num == 1) {
		$tempf  = "&nbsp;&lt;&lt;&nbsp;&nbsp;";
		$tempf .= "First&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	} else {
		$tempf  = "&nbsp;&lt;&lt;&nbsp;&nbsp;";
		$tempf .= "<a href='" . $str_link . "?page_position=" . 1 . "&page=0" . $qry_str . "' Class='" . $str_apply_class . "' title='Go to Page 1'>" . "First" . "</a>&nbsp;";
		$tempf .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	}

	if (($page == 0 && $int_current_page_num + 1 == $int_total_page) || ($page == 1 && $int_current_page_num == $int_total_page)) {
		$templ  = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		$templ .= "Last&nbsp;&nbsp;";
		$templ .= "&gt;&gt;&nbsp;&nbsp;";
	} else {
		$LastPages = $int_total_page % $int_diff;
		if ($LastPages == 0) {
			$templ  = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$templ .= "<a href='" . $str_link . "?page_position=" . $int_total_page . "&page=1&last=1" . $qry_str . "' Class='" . $str_apply_class . "' title='Go to Page " . $int_total_page . "'>" . "Last" . "</a>&nbsp;";
			$templ .= "&nbsp;<b>&gt;&gt;</b>&nbsp;&nbsp;";
		} else {
			$templ  = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$templ .= "<a href='" . $str_link . "?page_position=" . $int_total_page . "&page=1" . $qry_str . "' Class='" . $str_apply_class . "' title='Go to Page " . $int_total_page . "'>" . "Last" . "</a>&nbsp;";
			$templ .= "&nbsp;&gt;&gt;&nbsp;&nbsp;";
		}
	}
	$str = $tempf . $str . $templ;
	return $str;
}

#------------------------------------------------------------------------------------------------------------------------------------------
# END -- functions to encrypt and decrypt text
#------------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------------

/*''	Function Name :- CheckSelected
''	Prototype :- CheckSelected(parameter1,parameter2)
''	Input Parameter :- 2 Parameters Required.Both will be compared whether they are equal or not
''	Return Value :- string
''	Purpose :- It will check whether passed two parameters are same or not. If they are same then
''				it will return SELECTED else NOTHING.
''	Usage :- CheckSelected(parameter1,parameter2)	*/

function CheckSelecteds($parameter1, $parameter2)
{

	$para1 = trim($parameter1);
	$para2 = trim($parameter2);

	if (substr($para1, strlen($para1) - 1, 1) != ";") {
		$para1 = trim($para1) . ";";
		//$para1=substr($para1,strlen($para1)-1,1);
	}
	$arr = split(";", $para1);

	for ($c = 0; $c < count($arr) - 1; $c++) {
		//echo "C=". $arr[$c];
		if (trim($arr[$c]) === trim($para2)) {
			return "checked";
		}
	}
	/*$pos=0;
	while (FALSE !== ($pos = strpos ($para1, $para2, $pos))) {
	   return "checked";
	   $pos += strlen ($substring); 
	} */
	return "";
}
#----------------------------------------------------------------------------------------------------
/* 
	Function Name :- Display_Page_Metatag()
	Prototype :- Display_Page_Metatag($str_page_tag)
	Input Parameter :- 1. $str_page_tag is actulay tag in xmlfile,corresponding to page where metatag is to be dipslayed.
	Usage :- This fuction is used to display page metatag.
*/
#----------------------------------------------------------------------------------------------------
function Display_Page_Metatag($str_page_tag)
{

	global $STR_XML_FILE_PATH_MODULE;
	//print $STR_XML_FILE_PATH_MODULE;
	$fp = openXMLfile($STR_XML_FILE_PATH_MODULE . "page_metatag.xml");
	$metatag_page = getTagValue($str_page_tag, $fp);
	closeXMLfile($fp);
	$arr_metatag = explode("|", $metatag_page);
	//print_r($arr_metatag);
	for ($i = 0; $i < count($arr_metatag); $i++) {
		if ($arr_metatag[$i] != trim("")) {
			print($arr_metatag[$i]);
			print("\n");
		}
	}
}

#--------------------------------------------------------------------------------------------------------------------
# This function is used to generate daraker and lighter shade of any color
function colourBrightness($hex, $percent)
{
	// Work out if hash given
	$hash = '';
	if (stristr($hex, '#')) {
		$hex = str_replace('#', '', $hex);
		$hash = '#';
	}
	/// HEX TO RGB
	$rgb = array(hexdec(substr($hex, 0, 2)), hexdec(substr($hex, 2, 2)), hexdec(substr($hex, 4, 2)));
	//// CALCULATE 
	for ($i = 0; $i < 3; $i++) {
		// See if brighter or darker
		if ($percent > 0) {
			// Lighter
			$rgb[$i] = round($rgb[$i] * $percent) + round(255 * (1 - $percent));
		} else {
			// Darker
			$positivePercent = $percent - ($percent * 2);
			$rgb[$i] = round($rgb[$i] * $positivePercent) + round(0 * (1 - $positivePercent));
		}
		// In case rounding up causes us to go to 256
		if ($rgb[$i] > 255) {
			$rgb[$i] = 255;
		}
	}
	//// RBG to Hex
	$hex = '';
	for ($i = 0; $i < 3; $i++) {
		// Convert the decimal digit to hex
		$hexDigit = dechex($rgb[$i]);
		// Add a leading zero if necessary
		if (strlen($hexDigit) == 1) {
			$hexDigit = "0" . $hexDigit;
		}
		// Append to the hex string
		$hex .= $hexDigit;
	}
	return $hash . $hex;
}
#--------------------------------------------------------------------------------------------------------------------
// This function returns brightness value from 0 to 255. 
// White's value is 255 and Black's value is 0. 
function GetColorBrightnessValue($hex)
{
	// remove # from color code
	$hex = str_replace('#', '', $hex);
	$c_r = hexdec(substr($hex, 0, 2));
	$c_g = hexdec(substr($hex, 2, 2));
	$c_b = hexdec(substr($hex, 4, 2));
	return (($c_r * 299) + ($c_g * 587) + ($c_b * 114)) / 1000;
}
#--------------------------------------------------------------------------------------------------------------------
function GenerateRandomString($length = 6)
{
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}
#--------------------------------------------------------------------------------------------------------------------
function Generate_Random_Password($length = 8)
{
	//$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#$";
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$password = '';
	for ($i = 0; $i < $length; $i++) {
		$password = substr(str_shuffle($chars), 0, $length);
	}
	return $password;
}
#------------------------------------------------------------------------------------------	
// This function is uised get included files (.php, .js, .css) and images and mdm folder in respect of currently open page
function GetCurrentPathRespectToUser()
{
	$str_full_url = strtolower($_SERVER['QUERY_STRING']);
	$str_total_equalto = substr_count($str_full_url, "=");


	if ($str_total_equalto >= 9) // Used when 2 query string variable passed 
	{
		$str_path_respect_to_user_folder = "../../../../../../../../user/"; // 
	}
	if ($str_total_equalto >= 8) // Used when 2 query string variable passed 
	{
		$str_path_respect_to_user_folder = "../../../../../../../user/"; // 
	}
	if ($str_total_equalto >= 7) // Used when 2 query string variable passed 
	{
		$str_path_respect_to_user_folder = "../../../../../../user/"; // 
	}
	if ($str_total_equalto >= 6) // Used when 2 query string variable passed 
	{
		$str_path_respect_to_user_folder = "../../../../../user/"; // 
	}
	if ($str_total_equalto >= 5) // Used when 2 query string variable passed 
	{
		$str_path_respect_to_user_folder = "../../../../user/"; // 
	}
	if ($str_total_equalto >= 4) // Used when 2 query string variable passed 
	{
		$str_path_respect_to_user_folder = "../../../user/"; // 
	} else if ($str_total_equalto >= 3) // Used when 2 query string variable passed 
	{
		$str_path_respect_to_user_folder = "../../user/"; // 
	} else if ($str_total_equalto >= 2) // Used when 2 query string variable passed 
	{
		$str_path_respect_to_user_folder = "../user/"; // 
	} else if ($str_total_equalto == 1) // Used when Only 1 query string variable passed 
	{
		$str_path_respect_to_user_folder = "./user/";
	} else if ($str_total_equalto == 0 || $str_total_equalto == "") // Used when No query string variables passed
	{
		$str_path_respect_to_user_folder = "./user/";
		//$str_path_respect_to_user_folder = "./"; 
	}

	return $str_path_respect_to_user_folder;
}
#------------------------------------------------------------------------------------------	
// This function is uised for relative links in header, foother and other places
function GetRelativePathForLink()
{
	$str_full_url = strtolower($_SERVER['QUERY_STRING']);
	$str_total_equalto = substr_count($str_full_url, "=");

	if ($str_total_equalto >= 2) // Used when 2 query string variable passed 
	{
		$str_relative_path_for_link = "../";
	} else if ($str_total_equalto == 1) // Used when Only 1 query string variable passed 
	{
		$str_relative_path_for_link = "./";
	} else if ($str_total_equalto == 0 || $str_total_equalto == "") // Used when No query string variables passed
	{
		$str_relative_path_for_link = "./";
	}

	return $str_relative_path_for_link;
}
#------------------------------------------------------------------------------------------	
// This function is used to add/remove additional dot from relative links when pages moves between URL rewrite pages and simple pages

function NonURLRewriteLinksPath()
{
	if (strpos($_SERVER['REQUEST_URI'], "mdl_") != false) // Used when No query string variables passed
	{
		$str_dot_for_path_link = "../";
	} else if (strpos($_SERVER['REQUEST_URI'], "buyer_") != false) // Used when No query string variables passed
	{
		$str_dot_for_path_link = "../";
	} else if (strpos($_SERVER['REQUEST_URI'], "mem_") != false) // Used when No query string variables passed
	{
		$str_dot_for_path_link = "../";
	} else if (strpos($_SERVER['REQUEST_URI'], "member_") != false) // Used when No query string variables passed
	{
		$str_dot_for_path_link = "../";
	} else if (strpos($_SERVER['REQUEST_URI'], "freemember_") != false) // Used when No query string variables passed
	{
		$str_dot_for_path_link = "../";
	} else if (strpos($_SERVER['REQUEST_URI'], "_cart") != false) // Used when No query string variables passed
	{
		$str_dot_for_path_link = "../";
	} else if (strpos($_SERVER['REQUEST_URI'], "_checkout") != false) // Used when No query string variables passed
	{
		$str_dot_for_path_link = "../";
	} else {
		$str_dot_for_path_link = "";
	}

	return $str_dot_for_path_link;
}

#------------------------------------------------------------------------------------------	
// This function is used to add/remove additional dot from relative links when pages moves between URL rewrite pages and simple pages

function NonURLRewritePathRespectToUser()
{
	if (strpos($_SERVER['REQUEST_URI'], "buyer_") != false) // For buyer home page 
	{
		$str_dot_for_path_link = "./";
	} else // For URL rewrite pages
	{
		$str_dot_for_path_link = "./user/";
	}

	return $str_dot_for_path_link;
}
#------------------------------------------------------------------------------------------	
// User for Pay Per Call Feature for Model and Member
function PPCcURLAPIAccess($xml_url)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $xml_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Method#1 : this method should not be used for Server Security. 
	//	curl_setopt ($ch, CURLOPT_CAINFO, "C:/PHP/cacert.pem"); // Method#2 : this methoud should be used instead of above method.
	$xml = curl_exec($ch);
	//if(curl_exec($ch) === false) { echo 'Curl error: ' . curl_error($ch); }
	//else { echo 'Operation completed without any errors'; }
	curl_close($ch);

	return $xml;
}
#------------------------------------------------------------------------------------------	
// User for Pay Per Call Feature for Model and Member
function GetPPCIDWithExtension($int_extension_no, $str_loginid_without_ext)
{
	if ($int_extension_no > 0) {
		if ($int_extension_no > 0 && $int_extension_no < 10) {
			$PPC_loginid_with_ext = trim("00" . $int_extension_no . substr($str_loginid_without_ext, 3));
		} else if ($int_extension_no > 9 && $int_extension_no < 99) {
			$PPC_loginid_with_ext = trim("0" . $int_extension_no . substr($str_loginid_without_ext, 3));
		} else if ($int_extension_no > 99 && $int_extension_no < 999) {
			$PPC_loginid_with_ext = trim($int_extension_no . substr($str_loginid_without_ext, 3));
		}
	} else {
		$PPC_loginid_with_ext = $str_loginid_without_ext;
	}

	return $PPC_loginid_with_ext;
}
#----------------------------------------------------------------------------------------
#	This function highlights searched keywords in string
#	$text is string to be searched and $words is an array of keywords to search
function HighlightSearchKeywords($text, $words)
{
	if (!is_array($words)) {
		$words = preg_split('#\\W+#', $words, -1, PREG_SPLIT_NO_EMPTY);
	}
	# Added capture for text before the match.
	$regex = '#\\b(\\w*)(';
	$sep = '';
	foreach ($words as $word) {
		$regex .= $sep . preg_quote($word, '#');
		$sep = '|';
	}
	# Added capture for text after the match.
	$regex .= ')(\\w*)\\b#i';
	# Using \1 \2 \3 at relevant places.
	return preg_replace($regex, '\\1<span class="SearchHighLight">\\2</span>\\3', $text);
}
#----------------------------------------------------------------------------------------
function GetComparisonChart($width, $color, $totalcount)
{
	$str = "<div class='progress' style='margin-bottom:0;'>";
	$str .= "<div class='progress-bar progress-bar-success progress-bar-striped' role='progressbar' aria-valuenow='' aria-valuemin='0' aria-valuemax='100' style='width:$width%;'><b>$" . $totalcount . "</b></div>";
	$str .= "</div>";
	return $str;
}
##---------------------------------------------------------------------------------------
#       this function convert all white space into &nbsp in string
#       $text is string variable.
function ConvertWhitespaceTonbsp($text)
{
	#str_replace() string all " " convert with &nbsp.
	$converted_text = str_replace(" ", '&nbsp;', $text);

	#after convert return text.
	return $converted_text;
}
#----------------------------------------------------------------------------------------
function RemoveSpecialCharsFromString($string)
{
	$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	//$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars. (hyphen is allowed)
	//$string = preg_replace('/[^A-Za-z0-9]/', '', $string); // Removes special chars. (hyphen is also removed)

	//$string = preg_replace('/[^A-Za-z0-9\&\.\/]/', '', $string); // Removes special chars. (& and .(dot) is also allowed)
	$string = preg_replace('/[^A-Za-z0-9\&\.\(\)\/]/', '', $string); // Removes special chars. (& and .(dot) and ( and ) are allowed)
	return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}
#----------------------------------------------------------------------------------------
function RemoveSpecialCharsFromStringExceptWhiteSpace($string)
{
	//$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	//$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars. (hyphen is allowed)
	//$string = preg_replace('/[^A-Za-z0-9]/', '', $string); // Removes special chars. (hyphen is also removed)
	$string = preg_replace('/[^A-Za-z0-9\&\.\/]/', ' ', $string); // Removes special chars. (& and .(dot) is also allowed)
	return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}
##---------------------------------------------------------------------------------------
# this function to print button
# $str_button is string variable to display string.
function DisplayFormButton($text, $int_tabindex) // Value should be ADD/EDIT/RESET/SAVE
{
	$str_button = "";
	if (strtoupper($text) == "SAVE") {
		$str_button = "<button type='submit' id='btn_submit_save' class='btn btn-primary btn-sm' title='Click To Set Display Order' tabindex='" . $int_tabindex . "' ><b>Save</b></button>";
	} else if (strtoupper($text) == "ADD") {
		$str_button = "<button type='submit' id='btn_add' class='btn btn-primary' title='Click To Add New Details' tabindex='" . $int_tabindex . "'><b><i class='fa fa-plus-circle'></i>&nbsp;Add</b></button>&nbsp;";
	} else if (strtoupper($text) == "EDIT") {
		$str_button = "<button type='submit' id='btn_edit' class='btn btn-primary' title='Click To Edit This Details' tabindex='" . $int_tabindex . "'><b><i class='fa fa-pencil-square-o'></i>&nbsp;Edit</b></button>&nbsp;";
	} else if (strtoupper($text) == "DELETE") {
		$str_button = "<button type='submit' id='btn_delete' class='btn btn-default btn-sm' title='Click To Delete This Details' tabindex='" . $int_tabindex . "'><b>Delete</b></button>";
	} else if (strtoupper($text) == "SEND") {
		$str_button = "<button type='submit' class='btn btn-primary' title='Click To Send' tabindex='" . $int_tabindex . "' ><b><i class='fa fa-paper-plane'></i>&nbsp;Send</b></button>&nbsp;";
	} else if (strtoupper($text) == "RESET") {
		$str_button = "<button type='reset' class='btn btn-default' title='Click To Reset All Data' tabindex='" . $int_tabindex . "' ><b><i class='fa fa-refresh'></i>&nbsp;Reset</b></button>";
	} else if (strtoupper($text) == "DOWNLOAD") {
		$str_button = "<button type='submit' class='btn btn-primary' title='Click To Download Details' tabindex='" . $int_tabindex . "' ><b><i class='fa fa-download'></i>&nbsp;Download</b></button>";
	} else if (strtoupper($text) == "VIEW") {
		$str_button = "<button type='submit' class='btn btn-warning btn-sm' title='Click To View Details' tabindex='" . $int_tabindex . "'><i class='fa fa-eye'></i>&nbsp;<b>View</b></button>";
	} else if (strtoupper($text) == "SEARCH") {
		$str_button = "<button type='submit' class='btn btn-warning btn-sm' title='Click To Search Details' tabindex='" . $int_tabindex . "'><i class='fa fa-search'></i>&nbsp;<b>Search</b></button>";
	} else if (strtoupper($text) == "VIEWREPORT") {
		$str_button = "<button type='submit' class='btn btn-warning btn-sm' title='Click To View report Details' tabindex='" . $int_tabindex . "'><i class='fa fa-eye'></i>&nbsp;<b>View Report</b></button>";
	} else if (strtoupper($text) == "SIGNIN") {
		$str_button = "<button type='submit' class='btn btn-primary' title='Click To Log In'" . $int_tabindex . "' ><b><i class='fa fa-sign-in'></i>&nbsp;Login</b></button>";
	} else if (strtoupper($text) == "SIGNUP") {
		$str_button = "<button type='submit' class='btn btn-primary' title='Click To Register'" . $int_tabindex . "' ><b><i class='fa fa-user-plus'></i>&nbsp;Register</b></button>";
	} else if (strtoupper($text) == "GETNEWPASSWORD") {
		$str_button = "<button type='submit' class='btn btn-primary' title='Click To Sign Up'" . $int_tabindex . "' ><b>Click to get new password&nbsp;<i class='fa fa-long-arrow-right'></i></b></button>";
	} else if (strtoupper($text) == "CHANGEPASSWORD") {
		$str_button = "<button type='submit' class='btn btn-primary' title='Click To Change Password'" . $int_tabindex . "' ><b><i class='fa fa-edit'></i>&nbsp;Change Password</b></button>";
	}

	#after convert return text.
	return $str_button;
}
#------------------------------------------------------------------------------------------	
## Function to convert hexadecimal image code to RGB code...
function hex2rgb($hex)
{
	$hex = str_replace("#", "", $hex);

	if (strlen($hex) == 3) {
		$r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
		$g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
		$b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
	} else {
		$r = hexdec(substr($hex, 0, 2));
		$g = hexdec(substr($hex, 2, 2));
		$b = hexdec(substr($hex, 4, 2));
	}
	$rgb = array($r, $g, $b);
	//return implode(",", $rgb); // returns the rgb values separated by commas
	return $rgb; // returns an array with the rgb values
}


?>