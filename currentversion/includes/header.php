<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-162432112-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-162432112-1');
</script>
<!-- Facebook Pixel Code -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '798484083973532');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=798484083973532&ev=PageView&noscript=1" /></noscript>
<!-- End Facebook Pixel Code -->

<?php
##------------------------------------------------ CURRENCY ------------------------------------------------
$_SESSION["default_cntry"] = "";
if (isset($_SESSION["usr_cntry"]) == "" || $_SESSION["usr_cntry"] == 0) {
    $str_query_select = "";
    $str_query_select = "SELECT pkid FROM t_site_country WHERE UPPER(title) = 'INDIA'";

    $rs_list_default_country = GetRecordSet($str_query_select);

    $_SESSION["default_cntry"] = $rs_list_default_country->Fields("pkid");
    $_SESSION["usr_cntry"] = $rs_list_default_country->Fields("pkid");
    //print $_SESSION["usr_cntry"]; exit;
} else {
    $_SESSION["default_cntry"] = $_SESSION["default_cntry"];
    $_SESSION["usr_cntry"] = $_SESSION["usr_cntry"];
    //$_SESSION["usr_cntry"] = $rs_default_cntry->Fields("countrypkid");
}
//print_r($_SESSION);


$str_currency_symbol = "";
$int_conversionrate = 1.00;
$str_currency_shortform = "";
$int_courier_price_per_kg = "";

if (isset($_SESSION["usr_cntry"])) {
    $sel_qry_price = "";
    $sel_qry_price = "SELECT conversionrate, currency_title, currency_symbol, currency_shortform, courierpriceperkg FROM t_site_country WHERE pkid = " . $_SESSION["usr_cntry"];
    $rs_list_currency = GetRecordSet($sel_qry_price);
    if ($rs_list_currency->Count() > 0) {
        $str_currency_symbol = $rs_list_currency->Fields("currency_title");
        $str_currency_symbol = $rs_list_currency->Fields("currency_symbol");
        $str_currency_shortform = $rs_list_currency->Fields("currency_shortform");
        $int_conversionrate = $rs_list_currency->Fields("conversionrate");
        $int_courier_price_per_kg = $rs_list_currency->fields("courierpriceperkg");
    }
} //print $str_currency_shortform."-".$int_conversionrate;
// print_r($_SESSION);
?>

<link rel="shortcut icon" href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/favicon.ico">
<header class="header-area header-wrapper">
    <div class="curency-header-div hidden-xs hidden-sm">
        <div class="container">
            <div class="row currency-header">

                <div class="col-md-2 col-lg-2 currency-panel">
                    <form name="frm_sel_loc" id="frm_sel_loc" method="POST" action="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/set_currency_p.php">
                        <label>
                            <select class="browser-default custom-select input-xs line-height nopadding" name="usr_cntry" id="usr_cntry" onChange="document.forms['frm_sel_loc'].submit();">
                                <option value="0">Select Currency</option>
                                <?php
                                $str_sel_country = "";
                                $str_sel_country = "SELECT * FROM t_site_country WHERE visible='YES' ORDER BY displayorder DESC";
                                $rs_country = GetRecordSet($str_sel_country);
                                while ($rs_country->eof() != true) {
                                    $str_select = "";
                                    //if(isset($_SESSION["default_cntry"]) != $_SESSION["usr_cntry"])
                                    //{
                                    if (($_SESSION["usr_cntry"]) == $rs_country->Fields("pkid") && $_SESSION["default_cntry"] != $rs_country->Fields("pkid")) {
                                        $str_select = "selected";
                                    } else {
                                        $str_select = "";
                                    }                 //} 
                                ?>
                                    <option value="<?php print $rs_country->Fields("pkid"); ?>" <?php print $str_select; ?>><?php print $rs_country->Fields("currency_symbol") . " - " . $rs_country->Fields("currency_shortform") . " " . $rs_country->Fields("currency_title"); ?></option>
                                <?php $rs_country->MoveNext();
                                } ?>
                            </select>
                        </label>
                    </form>
                </div>
                <div class="col-md-3 col-lg-3">
                    <div class="search-box-input-panel">
                        <form name="frm_search" id="frm_search" action="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_listing2_p.php" method="POST">
                            <input type="text" class="serach-products ui-autocomplete-input" id="txt_search" name="txt_search" required data-validation-required-message="Please enter something to search" placeholder="Search Anything" tabindex="1">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col-md-7 col-lg-7 fast-link plr-0">
                    <?php if (isset($_SESSION["userid"]) && $_SESSION["userid"] != "") { ?>

                        <a class="" href="<?php print $STR_SITENAME_WITH_PROTOCOL . "/user/user_logout_p.php?#ptop"; ?>"><span class=""><i class="fa fa-sign-out"></i>&nbsp;Logout</span></a>
                        <a class="verticalLine" href="<?php print $STR_SITENAME_WITH_PROTOCOL . "/user/user_home.php?#ptop"; ?>"><span class=""><i class="fa fa-home"></i>&nbsp;My Account</span></a>
                        <a class="verticalLine"><span class="text-primary"><i class="fa fa-user-circle"></i>&nbsp;<?php if ($_SESSION["username"] != "") {
                                                                                                                        print $_SESSION["username"];
                                                                                                                    } else {
                                                                                                                        print $_SESSION["userid"];
                                                                                                                    }; ?></span></a>
                    <?php } else { ?>
                        <a class="" href="<?php print $STR_SITENAME_WITH_PROTOCOL . "/user-login#Register" ?>"><span class=""><i class="fa fa-user"></i>&nbsp;Register</span></a>
                        <a class="verticalLine" href="<?php print $STR_SITENAME_WITH_PROTOCOL . "/user-login#Login" ?>"><span class=""><i class="fa fa-sign-in"></i>&nbsp;Login</span></a>

                    <?php } ?>
                    <!--                    <a class="verticalLine" href="<?php //print $STR_SITENAME_WITH_PROTOCOL."/contact#ptop"
                                                                            ?>"><i class="fa fa-envelope"></i>&nbsp;Contact</a>-->
                    <a class="verticalLine" href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_order_status_list.php"><i class="fa fa-times-circle-o"></i>&nbsp;Cancel Order</a>
                    <a class="verticalLine" href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/user_order_status_list.php"><i class="fa fa-truck"></i>&nbsp;Track Order</a>
                </div>
            </div>
        </div>
    </div>
    <div class="header-top-bar-mirror hidden-xs hidden-sm">
    </div>
    <div class="header-top-bar">
        <div class="container">
            <div class="desktop-header-panel hidden-xs hidden-sm">
                <div class="row padding-10">
                    <div class="col-md-11 col-lg-11 pr-0">
                        <div class="row padding-10">
                            <div class="col-md-3 col-lg-3 pr-0">
                                <div class="company-logo">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>"><img src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/images/logo.png" alt="Darpani Logo" title="Darpani Logo" width="150px"></a>
                                </div>
                            </div>
                            <div class="col-md-9 col-lg-9 pl-0">
                                <nav id="desktop-menu-items">
                                    <ul class="main-menu f-right">
                                        <?php
                                        $str_query_select = "";
                                        //$str_query_select = "SELECT DISTINCT a.catpkid, a.title, a.displayorder FROM t_store b LEFT JOIN t_store_cat a ON a.catpkid=b.catpkid AND b.visible='YES' AND b.approved='YES' WHERE a.visible='YES' ORDER BY a.displayorder";
                                        $str_query_select  = "SELECT DISTINCT a.catpkid, a.title, a.displayorder ";
                                        $str_query_select .= "FROM t_store b ";
                                        $str_query_select .= "LEFT JOIN tr_store_photo c ON b.pkid=c.masterpkid AND c.visible='YES' AND c.setasfront='YES' ";
                                        $str_query_select .= "LEFT JOIN t_store_cat a ON a.catpkid=b.catpkid AND a.visible='YES' ";
                                        $str_query_select .= "LEFT JOIN t_store_subcat d ON d.subcatpkid=b.subcatpkid AND d.visible='YES' ";

                                        $str_query_select .= "WHERE a.visible='YES' AND b.approved='YES' ORDER BY a.displayorder DESC";

                                        $rs_list_menu_cat = GetRecordSet($str_query_select);
                                        if ($rs_list_menu_cat->Count() > 0) {
                                            while (!$rs_list_menu_cat->EOF()) { ?>
                                                <li>
                                                    <b>&nbsp;<a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_menu_cat->Fields("catpkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>"><span style="  font-size: 16px;"><?php print $rs_list_menu_cat->Fields("title");  ?></span></a>&nbsp;</b>
                                                    <span class="menu-tool-tip"></span>
                                                    <div class="mega-menu-area p-all-30">
                                                        <div class="row padding-10">
                                                            <?php ##-------------------------------------- START : SHOP BY CATEGORY -------------------------------------- 
                                                            $str_query_select = "";
                                                            /*$str_query_select = "SELECT DISTINCT b.subcattitle, a.title, c.title as cattitle, b.subcatpkid, b.subcattitle, b.displayorder FROM t_store a ";
                                                    $str_query_select .= "LEFT JOIN tr_store_photo d ON a.pkid=d.masterpkid AND d.visible='YES' AND d.setasfront='YES' ";
                                                    $str_query_select .= "LEFT JOIN t_store_cat c ON a.catpkid=c.catpkid AND a.visible='YES' ";
                                                    $str_query_select .= "LEFT JOIN t_store_subcat b ON a.subcatpkid=b.subcatpkid AND b.visible='YES' ";
                                                    $str_query_select .= "WHERE a.catpkid=".$rs_list_menu_cat->Fields("catpkid")." AND a.visible='YES' ORDER BY b.displayorder";*/
                                                            $str_query_select  = "SELECT DISTINCT c.subcatpkid, c.subcattitle FROM t_store a ";
                                                            //$str_query_select .= "LEFT JOIN tr_store_photo d ON a.pkid=d.masterpkid AND d.visible='YES' AND d.setasfront='YES' AND d.setasofferimage='' "; 
                                                            $str_query_select .= "LEFT JOIN t_store_cat b ON a.catpkid=b.catpkid AND b.visible='YES' ";
                                                            $str_query_select .= "LEFT JOIN t_store_subcat c ON a.subcatpkid=c.subcatpkid AND c.visible='YES' WHERE a.catpkid=" . $rs_list_menu_cat->Fields("catpkid") . " AND a.approved='YES' AND a.visible='YES' ORDER BY c.subcattitle ASC";
                                                            //print $str_query_select; 
                                                            $rs_list_menu_subcat = GetRecordSet($str_query_select);
                                                            if ($rs_list_menu_subcat->Count() > 0) {
                                                            ?>
                                                                <div class="single-menu col-lg-2 col-md-2" style="border-left: 1px solid #d7d7d7;">
                                                                    <span class="heading-main-header">SHOP BY CATEGORY</span>
                                                                    <hr class="star-primary">
                                                                    <?php
                                                                    while (!$rs_list_menu_subcat->EOF()) { ?>
                                                                        <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list_menu_subcat->Fields("pkid"); 
                                                                                                                                ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_subcat->Fields("subcattitle"))))); ?>" title="<?php print $rs_list_menu_subcat->Fields("subcattitle"); ?>"><?php print $rs_list_menu_subcat->Fields("subcattitle"); ?></a>
                                                                    <?php
                                                                        $rs_list_menu_subcat->MoveNext();
                                                                    }
                                                                    ?>

                                                                </div>
                                                            <?php } ?>
                                                            <?php
                                                            ##-------------------------------------- START : SHOP BY COLOR --------------------------------------
                                                            $str_query_select = "";
                                                            //$str_query_select = "SELECT DISTINCT b.pkid, b.title, b.displayorder FROM tr_store_color AS a LEFT JOIN t_store_color AS b ON a.masterpkid=b.pkid WHERE b.visible='YES' ORDER BY b.displayorder";
                                                            $str_query_select = "SELECT DISTINCT b.title, b.cmyk_code, b.displayorder ";
                                                            $str_query_select .= "FROM tr_store_color a ";
                                                            $str_query_select .= "LEFT JOIN t_store_color b ON b.pkid=a.masterpkid ";
                                                            $str_query_select .= "LEFT JOIN t_store c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                                                            $str_query_select .= "LEFT JOIN t_store_cat d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                                                            $str_query_select .= "LEFT JOIN t_store_subcat e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                                                            $str_query_select .= "WHERE c.catpkid=" . $rs_list_menu_cat->Fields("catpkid") . " ";
                                                            $str_query_select .= "ORDER BY b.displayorder DESC";
                                                            $rs_list_menu_color = GetRecordSet($str_query_select);
                                                            if ($rs_list_menu_color->Count() > 0) { ?>
                                                                <div class="single-menu col-lg-2 col-md-2" style="border-left: 1px solid #d7d7d7;">
                                                                    <span class="heading-main-header">SHOP BY COLOR</span>
                                                                    <hr class="star-primary">
                                                                    <?php
                                                                    while (!$rs_list_menu_color->EOF()) { ?>
                                                                        <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list_menu_subcat->Fields("pkid"); 
                                                                                                                                ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>/color-<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", strtolower($rs_list_menu_color->Fields("title")))))); ?>" title="<?php print $rs_list_menu_color->Fields("title"); ?>"><?php print $rs_list_menu_color->Fields("title"); ?></a>
                                                                    <?php
                                                                        $rs_list_menu_color->MoveNext();
                                                                    }
                                                                    $rs_list_menu_color->MoveFirst(); ?>
                                                                </div>
                                                            <?php }
                                                            ##-------------------------------------- END : SHOP BY COLOR ---------------------------------
                                                            ?>

                                                            <?php
                                                            ##-------------------------------------- START : SHOP BY MATERIAL -------------------------------
                                                            $str_query_select = "";
                                                            $str_query_select = "SELECT DISTINCT b.title, b.displayorder ";
                                                            $str_query_select .= "FROM tr_store_fabric a ";
                                                            $str_query_select .= "LEFT JOIN t_store_fabric b ON b.pkid=a.masterpkid ";
                                                            $str_query_select .= "LEFT JOIN t_store c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                                                            $str_query_select .= "LEFT JOIN t_store_cat d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                                                            $str_query_select .= "LEFT JOIN t_store_subcat e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                                                            $str_query_select .= "WHERE c.catpkid=" . $rs_list_menu_cat->Fields("catpkid") . " ";
                                                            $str_query_select .= "ORDER BY b.displayorder DESC";
                                                            //print $str_query_select; 
                                                            $rs_list_menu_fabric = GetRecordSet($str_query_select);
                                                            //print ($rs_list_menu_fabric->Count());
                                                            if ($rs_list_menu_fabric->Count() > 0) { ?>
                                                                <div class="single-menu col-lg-2 col-md-2" style="border-left: 1px solid #d7d7d7;">
                                                                    <span class="heading-main-header">SHOP BY MATERIAL</span>
                                                                    <hr class="star-primary">
                                                                    <?php
                                                                    while (!$rs_list_menu_fabric->EOF()) { ?>
                                                                        <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list_menu_subcat->Fields("pkid"); 
                                                                                                                                ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>/fabric-<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_fabric->Fields("title"))))); ?>" title="<?php print $rs_list_menu_fabric->Fields("title"); ?>"><?php print $rs_list_menu_fabric->Fields("title"); ?></a>
                                                                    <?php
                                                                        $rs_list_menu_fabric->MoveNext();
                                                                    }
                                                                    $rs_list_menu_fabric->MoveFirst(); ?>
                                                                </div>
                                                            <?php
                                                            }
                                                            ##-------------------------------------- END : SHOP BY MATERIAL------------------------------ 
                                                            ?>

                                                            <?php
                                                            ##-------------------------------------- START : SHOP BY TYPE -------------------------------
                                                            $str_query_select = "";
                                                            $str_query_select = "SELECT DISTINCT b.title, b.displayorder ";
                                                            $str_query_select .= "FROM tr_store_type a ";
                                                            $str_query_select .= "LEFT JOIN t_store_type b ON b.pkid=a.masterpkid ";
                                                            $str_query_select .= "LEFT JOIN t_store c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                                                            $str_query_select .= "LEFT JOIN t_store_cat d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                                                            $str_query_select .= "LEFT JOIN t_store_subcat e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                                                            $str_query_select .= "WHERE c.catpkid=" . $rs_list_menu_cat->Fields("catpkid") . " ";
                                                            $str_query_select .= "ORDER BY b.displayorder DESC";
                                                            //print $str_query_select; 
                                                            $rs_list_menu_type = GetRecordSet($str_query_select);
                                                            //print ($rs_list_menu_fabric->Count());
                                                            if ($rs_list_menu_type->Count() > 0) { ?>
                                                                <div class="single-menu col-lg-2 col-md-2" style="border-left: 1px solid #d7d7d7;">
                                                                    <span class="heading-main-header">SHOP BY TYPE</span>
                                                                    <hr class="star-primary">
                                                                    <?php
                                                                    while (!$rs_list_menu_type->EOF()) { ?>
                                                                        <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list_menu_subcat->Fields("pkid"); 
                                                                                                                                ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>/type-<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_type->Fields("title"))))); ?>" title="<?php print $rs_list_menu_type->Fields("title"); ?>"><?php print $rs_list_menu_type->Fields("title"); ?></a>
                                                                    <?php
                                                                        $rs_list_menu_type->MoveNext();
                                                                    }
                                                                    $rs_list_menu_type->MoveFirst(); ?>
                                                                </div>
                                                            <?php
                                                            }
                                                            ##-------------------------------------- END : SHOP BY TYPE------------------------------ 
                                                            ?>


                                                            <?php
                                                            ##-------------------------------------- START : SHOP BY WORK -------------------------------
                                                            $str_query_select = "";
                                                            $str_query_select = "SELECT DISTINCT b.title, b.displayorder ";
                                                            $str_query_select .= "FROM tr_store_work a ";
                                                            $str_query_select .= "LEFT JOIN t_store_work b ON b.pkid=a.masterpkid ";
                                                            $str_query_select .= "LEFT JOIN t_store c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                                                            $str_query_select .= "LEFT JOIN t_store_cat d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                                                            $str_query_select .= "LEFT JOIN t_store_subcat e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                                                            $str_query_select .= "WHERE c.catpkid=" . $rs_list_menu_cat->Fields("catpkid") . " ";
                                                            $str_query_select .= "ORDER BY b.displayorder DESC";
                                                            //print $str_query_select; 
                                                            $rs_list_menu_work = GetRecordSet($str_query_select);
                                                            //print ($rs_list_menu_fabric->Count());
                                                            if ($rs_list_menu_work->Count() > 0) { ?>
                                                                <div class="single-menu col-lg-2 col-md-2" style="border-left: 1px solid #d7d7d7;">
                                                                    <span class="heading-main-header">SHOP BY WORK</span>
                                                                    <hr class="star-primary">
                                                                    <?php
                                                                    while (!$rs_list_menu_work->EOF()) { ?>
                                                                        <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list_menu_subcat->Fields("pkid"); 
                                                                                                                                ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>/work-<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_work->Fields("title"))))); ?>" title="<?php print $rs_list_menu_work->Fields("title"); ?>"><?php print $rs_list_menu_work->Fields("title"); ?></a>
                                                                    <?php
                                                                        $rs_list_menu_work->MoveNext();
                                                                    }
                                                                    $rs_list_menu_work->MoveFirst(); ?>
                                                                </div>
                                                            <?php
                                                            }
                                                            ##-------------------------------------- END : SHOP BY WORK------------------------------ 
                                                            ?>
                                                            <?php
                                                            ##-------------------------------------- START : SHOP BY PRICE ----------------------------
                                                            $str_query_select = "";
                                                            $str_query_select = "SELECT * FROM t_store WHERE visible='YES' AND approved='YES' AND catpkid=" . $rs_list_menu_cat->Fields("catpkid");
                                                            //print $str_query_select;
                                                            $rs_list_menu_product = GetRecordSet($str_query_select);


                                                            $str_query_select = "";
                                                            $str_query_select = "SELECT pkid, title, fromamount, toamount FROM t_store_pricerange WHERE visible='YES' ORDER BY displayorder DESC";
                                                            $rs_list_menu_price_range = GetRecordSet($str_query_select);

                                                            //print ($rs_list_menu_product->Fields("color"));
                                                            if ($rs_list_menu_price_range->Count() > 0) { ?>

                                                                <div class="single-menu col-lg-2 col-md-2" style="border-left: 1px solid #d7d7d7;">
                                                                    <span class="heading-main-header">SHOP BY PRICE</span>
                                                                    <hr class="star-primary">
                                                                    <?php
                                                                    $arr_old_price = array();

                                                                    while (!$rs_list_menu_product->EOF()) {
                                                                        $int_price = 0;
                                                                        if ($rs_list_menu_product->Fields("ourprice") != "" || $rs_list_menu_product->Fields("ourprice") > 0) {
                                                                            $int_price = $rs_list_menu_product->Fields("ourprice");
                                                                        } else {
                                                                            $int_price = $rs_list_menu_product->Fields("listprice");
                                                                        }
                                                                        //print $int_price;
                                                                        //
                                                                    ?>
                                                                        <?php
                                                                        while (!$rs_list_menu_price_range->EOF()) {
                                                                            //print $int_price."<br/>";
                                                                            if ($int_price >= $rs_list_menu_price_range->Fields("fromamount") && $int_price <= $rs_list_menu_price_range->Fields("toamount") &&  !in_array($rs_list_menu_price_range->Fields("pkid"), $arr_old_price)) {
                                                                                array_push($arr_old_price, $rs_list_menu_price_range->Fields("pkid"))
                                                                        ?>
                                                                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list_menu_subcat->Fields("pkid"); 
                                                                                                                                        ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>/price-<?php print($rs_list_menu_price_range->Fields("pkid")); ?>" title="<?php print $rs_list_menu_price_range->Fields("title"); ?>"><?php
                                                                                                                                                                                                                                                                                                                                                                                                                    $int_from_amt_menu = ceil($rs_list_menu_price_range->Fields("fromamount") / $int_conversionrate);
                                                                                                                                                                                                                                                                                                                                                                                                                    $int_to_amt_menu = ceil($rs_list_menu_price_range->Fields("toamount") / $int_conversionrate);
                                                                                                                                                                                                                                                                                                                                                                                                                    ?><?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_from_amt_menu, 2); ?> - <?php print $str_currency_symbol; ?>&nbsp;<?php print number_format($int_to_amt_menu, 2); ?></a>
                                                                        <?php
                                                                            }
                                                                            $rs_list_menu_price_range->MoveNext();
                                                                        }
                                                                        $rs_list_menu_price_range->MoveFirst(); ?>
                                                                    <?php $rs_list_menu_product->MoveNext();
                                                                    }
                                                                    $rs_list_menu_product->MoveFirst(); ?>
                                                                </div>
                                                            <?php } ##-------------------------------------- END : SHOP BY PRICE ----------------------- 
                                                            ?>

                                                        </div>
                                                    </div>
                                                    <!--filter ends-->
                                                </li>
                                        <?php $rs_list_menu_cat->MoveNext();
                                            }
                                        }
                                        $rs_list_menu_cat->MoveFirst(); ?>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-1 col-lg-1">
                        <div class="row padding-10">
                            <?php /* ?>  
                        <div class="col-md-10 col-lg-10">
                          <div class="search-box-input-panel">
                            <form name="frm_search" id="frm_search" action="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_listing2_p.php" method="POST">
                                <input type="text" class="serach-products ui-autocomplete-input" id="txt_search" name="txt_search" required data-validation-required-message="Please enter something to search" placeholder="Search Anything" tabindex="1">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                          </div>
                          <?php */ ?>
                        </div>
                        <div class="col-md-12 col-lg-12 pl-0" align="right">
                            <!--                          <div class="dropdown-account cursor d-inline">
                             <ul class="my-setting">
                                 <li>
                                     <span><i class="fa fa-user-circle desktop-header-icon-color"></i></span>
                                     <ul>
                                         <li> <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/signin"><i class="fa fa-sign-in"></i>&nbsp;Login</a></li>
                                      <li> <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/signup"><i class="fa fa-user-plus"></i>&nbsp;Register</a></li>
                                     </ul>
                                 </li>
                             </ul>
                          </div>-->
                            <div class="cart-box  d-inline cursor">
                                <div class="cart-icone">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_cart.php"><i class="fa fa-shopping-bag desktop-header-icon-color"></i></a>
                                    <?php
                                    /*$int_total_cart_items = 0;
                                  if(isset($_SESSION['sessionid']) && isset($_SESSION['uniqueid'])) {
                                  $rs_cart_count = "";
                                  $rs_cart_count = "SELECT COUNT(*) AS total_items_in_cart FROM t_store_sessioncart WHERE sessionid='".$_SESSION['sessionid']."' AND uniqueid='".$_SESSION['uniqueid']."'";
                                  
                                    $rs_cart_count=GetRecordSet($rs_cart_count);
                                    
                                    if(!$rs_cart_count->EOF())
                                    {
                                        $int_total_cart_items = $rs_cart_count->Fields("total_items_in_cart");
                                    }
                                  } */
                                    ?>
                                    <span class="text-bold " id="load_cartitems" name="load_cartitems"><?php //print $int_total_cart_items; 
                                                                                                        ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="mobile-header-panel container-fluid hidden-md hidden-lg">
        <div class="">
            <div class="row padding-10">
                <div class="col-xs-2 col-sm-1">
                    <div class="mobile-menu-panel">
                        <div class="mobile-menu-bar">
                            <a href="#" id="showmenu">
                                <i class="fa fa-align-justify"></i>
                            </a>
                        </div>
                        <nav id="menu" class="left">
                            <div class="heading">SHOP FOR
                                <span><i class="fa fa-times fa-lg" aria-hidden="true"></i></span>
                            </div>
                            <ul>
                                <?php
                                if ($rs_list_menu_cat->Count() > 0) {
                                    while (!$rs_list_menu_cat->EOF()) { ?>
                                        <li class="main-category submenu">
                                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php print $rs_list_menu_cat->Fields("catpkid"); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>" title="<?php print $rs_list_menu_cat->Fields("title");  ?>"><span><?php print $rs_list_menu_cat->Fields("title");  ?></span><i class="fa fa-caret-down"></i></a>
                                            <ul>
                                                <?php ##-------------------------------------- START : SHOP BY CATEGORY -------------------------------------- 
                                                $str_query_select = "";
                                                $str_query_select  = "SELECT DISTINCT c.subcatpkid, c.subcattitle FROM t_store a ";
                                                $str_query_select .= "LEFT JOIN t_store_cat b ON a.catpkid=b.catpkid AND b.visible='YES' ";
                                                $str_query_select .= "LEFT JOIN t_store_subcat c ON a.subcatpkid=c.subcatpkid AND c.visible='YES' WHERE a.catpkid=" . $rs_list_menu_cat->Fields("catpkid") . " AND a.approved='YES' AND a.visible='YES' ORDER BY c.subcattitle ASC";
                                                //print $str_query_select; 
                                                $rs_list_menu_subcat = GetRecordSet($str_query_select);
                                                if ($rs_list_menu_subcat->Count() > 0) {
                                                ?>
                                                    <li class="submenu sub-category"><a href="#">SHOP BY CATEGORY<i class="fa fa-caret-down"></i></a>
                                                        <ul>
                                                            <?php
                                                            while (!$rs_list_menu_subcat->EOF()) { ?>
                                                                <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list_menu_subcat->Fields("pkid"); 
                                                                                                                            ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>/<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_subcat->Fields("subcattitle"))))); ?>" title="<?php print $rs_list_menu_subcat->Fields("subcattitle"); ?>"><?php print $rs_list_menu_subcat->Fields("subcattitle"); ?></a></li>
                                                            <?php
                                                                $rs_list_menu_subcat->MoveNext();
                                                            } ?>
                                                        </ul>
                                                    </li>
                                                <?php } ##-------------------------------------- END : SHOP BY CATEGORY -------------------------------------- 
                                                ?>

                                                <?php ##-------------------------------------- START : SHOP BY COLOR -------------------------------------- 
                                                $str_query_select = "";
                                                //$str_query_select = "SELECT DISTINCT b.pkid, b.title, b.displayorder FROM tr_store_color AS a LEFT JOIN t_store_color AS b ON a.masterpkid=b.pkid WHERE b.visible='YES' ORDER BY b.displayorder";
                                                $str_query_select = "SELECT DISTINCT b.title, b.cmyk_code, b.displayorder ";
                                                $str_query_select .= "FROM tr_store_color a ";
                                                $str_query_select .= "LEFT JOIN t_store_color b ON b.pkid=a.masterpkid ";
                                                $str_query_select .= "LEFT JOIN t_store c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                                                $str_query_select .= "LEFT JOIN t_store_cat d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                                                $str_query_select .= "LEFT JOIN t_store_subcat e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                                                $str_query_select .= "WHERE c.catpkid=" . $rs_list_menu_cat->Fields("catpkid") . " ";
                                                $str_query_select .= "ORDER BY b.displayorder DESC";
                                                $rs_list_menu_color = GetRecordSet($str_query_select);
                                                if ($rs_list_menu_color->Count() > 0) { ?>
                                                    <li class="submenu sub-category"><a href="#">SHOP BY COLOR<i class="fa fa-caret-down"></i></a>
                                                        <ul>
                                                            <?php
                                                            while (!$rs_list_menu_color->EOF()) { ?>
                                                                <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list_menu_subcat->Fields("pkid"); 
                                                                                                                            ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>/color-<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", strtolower($rs_list_menu_color->Fields("title")))))); ?>" title="<?php print $rs_list_menu_color->Fields("title"); ?>"><?php print $rs_list_menu_color->Fields("title"); ?></a></li>
                                                            <?php
                                                                $rs_list_menu_color->MoveNext();
                                                            } ?>
                                                        </ul>
                                                    </li>
                                                <?php } ?>

                                                <?php
                                                ##-------------------------------------- START : SHOP BY MATERIAL -------------------------------
                                                $str_query_select = "";
                                                $str_query_select = "SELECT DISTINCT b.title, b.displayorder ";
                                                $str_query_select .= "FROM tr_store_fabric a ";
                                                $str_query_select .= "LEFT JOIN t_store_fabric b ON b.pkid=a.masterpkid ";
                                                $str_query_select .= "LEFT JOIN t_store c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                                                $str_query_select .= "LEFT JOIN t_store_cat d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                                                $str_query_select .= "LEFT JOIN t_store_subcat e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                                                $str_query_select .= "WHERE c.catpkid=" . $rs_list_menu_cat->Fields("catpkid") . " ";
                                                $str_query_select .= "ORDER BY b.displayorder DESC";
                                                //print $str_query_select; 
                                                $rs_list_menu_fabric = GetRecordSet($str_query_select);
                                                //print ($rs_list_menu_fabric->Count());
                                                if ($rs_list_menu_fabric->Count() > 0) { ?>
                                                    <li class="submenu sub-category"><a href="#">SHOP BY MATERIAL<i class="fa fa-caret-down"></i></a>
                                                        <ul>
                                                            <?php
                                                            while (!$rs_list_menu_fabric->EOF()) { ?>
                                                                <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list_menu_subcat->Fields("pkid"); 
                                                                                                                            ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>/fabric-<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", strtolower($rs_list_menu_fabric->Fields("title")))))); ?>" title="<?php print $rs_list_menu_fabric->Fields("title"); ?>"><?php print $rs_list_menu_fabric->Fields("title"); ?></a></li>
                                                            <?php
                                                                $rs_list_menu_fabric->MoveNext();
                                                            } ?>
                                                        </ul>
                                                    </li>

                                                <?php
                                                }
                                                ##-------------------------------------- END : SHOP BY MATERIAL------------------------------ 
                                                ?>

                                                <?php
                                                ##-------------------------------------- START : SHOP BY TYPE -------------------------------
                                                $str_query_select = "";
                                                $str_query_select = "SELECT DISTINCT b.title, b.displayorder ";
                                                $str_query_select .= "FROM tr_store_type a ";
                                                $str_query_select .= "LEFT JOIN t_store_type b ON b.pkid=a.masterpkid ";
                                                $str_query_select .= "LEFT JOIN t_store c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                                                $str_query_select .= "LEFT JOIN t_store_cat d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                                                $str_query_select .= "LEFT JOIN t_store_subcat e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                                                $str_query_select .= "WHERE c.catpkid=" . $rs_list_menu_cat->Fields("catpkid") . " ";
                                                $str_query_select .= "ORDER BY b.displayorder DESC";
                                                //print $str_query_select; 
                                                $rs_list_menu_type = GetRecordSet($str_query_select);
                                                //print ($rs_list_menu_fabric->Count());
                                                if ($rs_list_menu_type->Count() > 0) { ?>
                                                    <li class="submenu sub-category"><a href="#">SHOP BY TYPE<i class="fa fa-caret-down"></i></a>
                                                        <ul>
                                                            <?php
                                                            while (!$rs_list_menu_type->EOF()) { ?>
                                                                <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list_menu_subcat->Fields("pkid"); 
                                                                                                                            ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>/type-<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", strtolower($rs_list_menu_type->Fields("title")))))); ?>" title="<?php print $rs_list_menu_type->Fields("title"); ?>"><?php print $rs_list_menu_type->Fields("title"); ?></a></li>
                                                            <?php
                                                                $rs_list_menu_type->MoveNext();
                                                            } ?>
                                                        </ul>
                                                    </li>
                                                <?php
                                                }
                                                ##-------------------------------------- END : SHOP BY TYPE------------------------------ 
                                                ?>
                                                <?php
                                                ##-------------------------------------- START : SHOP BY WORK -------------------------------
                                                $str_query_select = "";
                                                $str_query_select = "SELECT DISTINCT b.title, b.displayorder ";
                                                $str_query_select .= "FROM tr_store_work a ";
                                                $str_query_select .= "LEFT JOIN t_store_work b ON b.pkid=a.masterpkid ";
                                                $str_query_select .= "LEFT JOIN t_store c ON a.itempkid=c.pkid AND c.visible='YES' AND c.approved='YES' ";
                                                $str_query_select .= "LEFT JOIN t_store_cat d ON d.catpkid=c.catpkid AND d.visible='YES' ";
                                                $str_query_select .= "LEFT JOIN t_store_subcat e ON e.catpkid=d.catpkid AND e.visible='YES' ";
                                                $str_query_select .= "WHERE c.catpkid=" . $rs_list_menu_cat->Fields("catpkid") . " ";
                                                $str_query_select .= "ORDER BY b.displayorder DESC";
                                                //print $str_query_select; 
                                                $rs_list_menu_work = GetRecordSet($str_query_select);
                                                //print ($rs_list_menu_fabric->Count());
                                                if ($rs_list_menu_work->Count() > 0) { ?>
                                                    <li class="submenu sub-category"><a href="#">SHOP BY WORK<i class="fa fa-caret-down"></i></a>
                                                        <ul>
                                                            <?php
                                                            while (!$rs_list_menu_work->EOF()) { ?>
                                                                <li><a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list_menu_subcat->Fields("pkid"); 
                                                                                                                            ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>/work-<?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", strtolower($rs_list_menu_work->Fields("title")))))); ?>" title="<?php print $rs_list_menu_work->Fields("title"); ?>"><?php print $rs_list_menu_work->Fields("title"); ?></a></li>
                                                            <?php
                                                                $rs_list_menu_work->MoveNext();
                                                            } ?>
                                                        </ul>
                                                    </li>

                                                <?php
                                                }
                                                ##-------------------------------------- END : SHOP BY WORK------------------------------ 
                                                ?>
                                                <?php
                                                ##-------------------------------------- START : SHOP BY PRICE ----------------------------
                                                $str_query_select = "";
                                                $str_query_select = "SELECT * FROM t_store WHERE visible='YES' AND approved='YES' AND catpkid=" . $rs_list_menu_cat->Fields("catpkid");
                                                //print $str_query_select;
                                                $rs_list_menu_product = GetRecordSet($str_query_select);


                                                $str_query_select = "";
                                                $str_query_select = "SELECT pkid, title, fromamount, toamount FROM t_store_pricerange WHERE visible='YES' ORDER BY displayorder DESC";
                                                $rs_list_menu_price_range = GetRecordSet($str_query_select);

                                                //print ($rs_list_menu_product->Fields("color"));
                                                if ($rs_list_menu_price_range->Count() > 0) { ?>

                                                    <li class="submenu sub-category"><a href="#">SHOP BY PRICE<i class="fa fa-caret-down"></i></a>
                                                        <ul>
                                                            <?php
                                                            $arr_old_price = array();

                                                            while (!$rs_list_menu_product->EOF()) {
                                                                $int_price = 0;
                                                                if ($rs_list_menu_product->Fields("ourprice") != "" || $rs_list_menu_product->Fields("ourprice") > 0) {
                                                                    $int_price = $rs_list_menu_product->Fields("ourprice");
                                                                } else {
                                                                    $int_price = $rs_list_menu_product->Fields("listprice");
                                                                }
                                                                //print $int_price;
                                                                //
                                                            ?>
                                                                <?php
                                                                while (!$rs_list_menu_price_range->EOF()) {
                                                                    //print $int_price."<br/>";
                                                                    if ($int_price >= $rs_list_menu_price_range->Fields("fromamount") && $int_price <= $rs_list_menu_price_range->Fields("toamount") &&  !in_array($rs_list_menu_price_range->Fields("pkid"), $arr_old_price)) {
                                                                        array_push($arr_old_price, $rs_list_menu_price_range->Fields("pkid"))
                                                                ?>
                                                                        <li>
                                                                            <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/<?php //print $rs_list_menu_subcat->Fields("pkid"); 
                                                                                                                                    ?><?php print(str_replace(" ", "-", str_replace("/", "_slash_", str_replace("&", "_and_", $rs_list_menu_cat->Fields("title"))))); ?>/price-<?php print(str_replace(".", "_", $rs_list_menu_price_range->Fields("title"))); ?>" title="<?php print $rs_list_menu_price_range->Fields("title"); ?>"><?php print $rs_list_menu_price_range->Fields("title"); ?></a>
                                                                        </li>
                                                                <?php
                                                                    }
                                                                    $rs_list_menu_price_range->MoveNext();
                                                                }
                                                                $rs_list_menu_price_range->MoveFirst(); ?>
                                                            <?php $rs_list_menu_product->MoveNext();
                                                            }
                                                            $rs_list_menu_product->MoveFirst(); ?>
                                                        </ul>
                                                    </li>
                                                <?php } ##-------------------------------------- END : SHOP BY PRICE ----------------------- 
                                                ?>



                                            </ul>
                                        </li>
                                <?php $rs_list_menu_cat->MoveNext();
                                    }
                                } ?>
                            </ul>
                            <div class="others-mobile-navigation">
                                <div class="track-navigation">
                                    <div class="row padding-10">
                                        <div class="col-xs-6 col-sm-6 div-separator">
                                            <div class="track-order">
                                                <a class="mobile-menu-track-order-icon" href=""><i class="fa fa-truck fa-2x"></i><br>
                                                    <span class="track-font">Track Order</span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6">
                                            <div class="cancel-order">
                                                <a class="mobile-menu-track-order-icon" href=""><i class="fa fa-times-circle-o fa-2x"></i><br>
                                                    <span class="track-font">Cancel Order</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <?php if (isset($_SESSION["userid"]) && $_SESSION["userid"] != "") { ?>
                                    <div class="account-navigation">
                                        <div class="row padding-10">
                                            <div class="col-xs-12 col-sm-12">
                                                <i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;<?php if ($_SESSION["username"] != "") {
                                                                                                                print $_SESSION["username"];
                                                                                                            } else {
                                                                                                                print $_SESSION["userid"];
                                                                                                            } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="account-navigation">
                                        <div class="row padding-10">
                                            <div class="col-xs-6 col-sm-6 div-separator">
                                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL . "/user/user_home.php?#ptop"; ?>"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;My Account</a>
                                            </div>
                                            <div class="col-xs-6 col-sm-6">
                                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL . "/user/user_logout_p.php?#ptop"; ?>"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a>
                                            </div>
                                        </div>
                                    </div>


                                <?php } else { ?>
                                    <div class="account-navigation">
                                        <div class="row padding-10">
                                            <div class="col-xs-6 col-sm-6 div-separator">
                                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login#Login"><span><i class="fa fa-sign-in" aria-hidden="true"></i></span>&nbsp;Login</a>
                                            </div>
                                            <div class="col-xs-6 col-sm-6">
                                                <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login#Register"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Register</a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="currency-track-order-navigation">
                                    <div class="row padding-10">
                                        <div class="col-xs-12 col-sm-12 currency">
                                            <form name="frm_sel_loc2" id="frm_sel_loc2" method="POST" action="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/set_currency_p.php">
                                                <label>
                                                    <select class="form-control" name="usr_cntry" id="usr_cntry" onChange="document.forms['frm_sel_loc2'].submit();">
                                                        <option value="0">Currency</option>
                                                        <?php
                                                        $str_sel_country = "";
                                                        $str_sel_country = "SELECT * FROM t_site_country WHERE visible='YES' ORDER BY displayorder DESC";
                                                        $rs_country = GetRecordSet($str_sel_country);
                                                        while ($rs_country->eof() != true) {
                                                            $str_select = "";
                                                            //if(isset($_SESSION["default_cntry"]) != $_SESSION["usr_cntry"])
                                                            //{
                                                            if (($_SESSION["usr_cntry"]) == $rs_country->Fields("pkid") && $_SESSION["default_cntry"] != $rs_country->Fields("pkid")) {
                                                                $str_select = "selected";
                                                            } else {
                                                                $str_select = "";
                                                            }                 //} 
                                                        ?>
                                                            <option value="<?php print $rs_country->Fields("pkid"); ?>" <?php print $str_select; ?>><?php print $rs_country->Fields("currency_symbol") . " - " . $rs_country->Fields("currency_title"); ?></option>
                                                        <?php $rs_country->MoveNext();
                                                        } ?>
                                                    </select>
                                                </label>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </nav>
                        <div class="site-overlay"></div>
                    </div>
                </div>
                <div class="col-xs-5 col-sm-8 plr-0">
                    <div class="mobile-view-icon">
                        <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>"><img src="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/images/logo.png" alt="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>"></a>
                    </div>
                </div>

                <div class="col-xs-5 col-sm-3 pl-0">
                    <div class="row padding-10">
                        <div class="col-xs-12 col-sm-12 pl-0 text-right">
                            <div class="show-serach-bar-icon">
                                <i class="fa fa-search white-fa-border" style="color: #535766;"></i>
                            </div>


                            <?php if (isset($_SESSION["userid"]) && $_SESSION["userid"] != "") { ?>
                                <div class="account">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL . "/user/user_home.php?#ptop"; ?>"><i class="fa fa-user-circle white-fa-border" style="color: #535766;"></i></a>
                                </div>
                            <?php } else { ?>
                                <div class="account">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user-login"><i class="fa fa-user-circle white-fa-border" style="color: #535766;"></i></a>
                                </div>
                            <?php } ?>



                            <div class="cart-icone">
                                <div class="hover-i ">
                                    <a href="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_cart.php">
                                        <i style="color: #535766;" class="fa fa-shopping-bag white-fa-border"></i>
                                    </a>
                                    <span id="mobile-cart-count" class="cart_i_count text-bold "><?php //print $int_total_cart_items; 
                                                                                                    ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mobile-search-panel" style="display:none;">
                <div class="row padding-10">
                    <div class="search-box-mobile">
                        <div class="col-xs-12 col-sm-12">
                            <div class="search-box-input-panel">
                                <?php /* ?><form>
                        <input type="text" class="serach-products ui-autocomplete-input" placeholder="Search" autocomplete="off">
                        <input type="hidden" id="es_path" data-esip="">
                        <input type="hidden" id="es_token" data-token="">
                        <button type="submit"><i class="fa fa-search"></i></button>
                      </form><?php */ ?>
                                <form name="frm_search" id="frm_search" action="<?php print $STR_SITENAME_WITH_PROTOCOL; ?>/user/product_listing2_p.php" method="POST">
                                    <input type="text" class="serach-products ui-autocomplete-input" id="txt_search" name="txt_search" required data-validation-required-message="Please enter something to search" placeholder="Search Anything" tabindex="1">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hidden-md hidden-lg mobile-header-panel-mirror" style="height: 51px; z-index: -9999999999999"></div>
</header>
<!--top top bottom arrow-->
<a href="#" id="scroll" style="display: none;"><span></span></a>
<!--end top top bottom arrow-->