<?php
#--------------------------------------------------------------------------------------------------------   
#variables used to Set Running Mode of Application 
$APP_RUN_MODE="LOCAL";	
//$APP_RUN_MODE="SERVER";
#--------------------------------------------------------------------------------------------------------   
	set_time_limit(150);
#--------------------------------------------------------------------------------------------------------   
#variable for entire site.
$STR_SITE_NAME="www.BragCard.com";
$STR_SITE_NAME_WITH_PROTOCOL="http://www.BragCard.com";
$STR_SITENAME_WITHOUT_PROTOCOL = "BragCard.com";
$STR_SITE_TITLE ="BragCard";
#--------------------------------------------------------------------------------------------------------
# Encryption Key
$STR_ENCRYPTION_KEY="50137orlandodrone";
#--------------------------------------------------------------------------------------------------------
$STR_FROM_DEFAULT="P_SITE_EMAIL_ADDRESS";
$STR_WEBSITE_NAME="P_SITE_URL";

$UPLOAD_XML_MODULE_PATH="../../mdm/xmlmodulefiles/";
$UPLOAD_XML_CONTENT_PATH="../../mdm/xmlcontentfiles/";
#--------------------------------------------------------------------------------------------------------
#Path to open domain from admin site domain listing page by click on domain name...
if($APP_RUN_MODE=="LOCAL") { 
	$STR_DOMAIN_FILE_PATH="./../../../currentversion/";
} else {
	$STR_DOMAIN_FILE_PATH="http://www.BragCard.com/";
}
#--------------------------------------------------------------------------------------------------------

#SETTING LIMITS
#--------------------------------------------------------------------------------------------------------

#Module Name:- modPhotogallery
$INT_PHOTOALBUM_LIMIT=0;
$INT_PHOTO_LIMIT=0;

#Module Name:- modSpSeriesAlbum
$INT_SPPHOTOALBUM_LIMIT=0;
$INT_SPPHOTO_LIMIT=0;

#Module Name:- modWallpaper
$INT_WP_LIMIT=0;

#Module Name:- modBanner

#Module Name:- modPageBanner


#Module Name:- modLink
$INT_LINK_CAT_LIMIT=0;
$INT_LINK_LIMIT=0;

#Module Name:- modJournal
$INT_JNL_LIMIT=0;
$INT_JNL_DETAIL_LIMIT=0;

#MODULE NAME:-Portfolio
$INT_PORTFOLIO_LIMIT=0;

#Module Name:-modvideo
$INT_VIDEO_LIMIT=50;
$INT_CLIP_PER_VIDEO_LIMIT=5;
$STR_EXT_MP4=".mp4";




#--------------------------------------------------------------------------------------------------------
#Page Titles
$STR_TITLE_ADMIN_LOGIN = "Site Admin Login";
$STR_TITLE_MODULE_ADMIN_LOGIN = "Module Admin Login";
$STR_TITLE_MODULE_ADMIN_MAIN_MENU = "Module Admin Main Menu";
$STR_TITLE_CHANGE_MODULE_ADMIN_MAIN_MENU_PWD = "Change Module Admin Main Menu Password";
$STR_TITLE_SITE_CONFIG = "Site Configuration";
$STR_TITLE_SITE_ADMIN = "Site Admin List";
$STR_TITLE_SITE_ADMIN_MODULE = "Site Admin Module List";

$STR_TITLE_NEWSLETTER = "Subscriber Category";

$STR_TITLE_LEGAL = "Legal List";
$STR_TITLE_TERMS_CONDITIONS = "Term & Conditions List";
$STR_TITLE_PRIVACY_POLICY = "Privacy List";
$STR_TITLE_FAQ = "FAQ List";
$STR_TITLE_HOME_CONTENT = "Home Content List";
$STR_TITLE_HOME_IMAGE = "Home Image List";
$STR_TITLE_ABOUT = "About";
$STR_TITLE_SERVICES = "Services";
$STR_TITLE_STATISTICS = "	Statistics List";
$STR_TITLE_RESUME = "	Resume List";
$STR_TITLE_PORTFOLIO = " Portfolio List";
$STR_TITLE_WORK_SKILL = "Work & Skill List";
$STR_TITLE_CONTACT = "Contact Inquiry List";
$STR_TITLE_CONTACT_SUBJECT = "Contact Subject List";
//$STR_TITLE_BANNER = "Banner List";

$STR_TITLE_COUNTRY = "Country List";
$STR_TITLE_STATE = "State List";
$STR_TITLE_CITY = "City List";
$STR_TITLE_PAYMENT_PLAN = "Payment Plan List";
$STR_TITLE_PAYMENT_PLAN_CMS= "Payment Plan";
$STR_TITLE_PROMO_GALLERY = "Promo Gallery List";
$STR_TITLE_PROMO_GALLERY_IMAGE = "Promo Gallery Image List";
$STR_TITLE_FOOTER_BANNER = "Footer Text and Banner Code";
$STR_TITLE_MANAGE_CONTENT = "CMS";
$STR_TITLE_NEWS_ANNOUNCEMENT = "News / Announcement List";
$STR_TITLE_ARCHIVED_NEWS_ANNOUNCEMENT = "Archived News / Announcement List";
$STR_TITLE_WEB_MASTER = "Webmaster List";
$STR_TITLE_WEB_MASTER_HOME = "Webmaster Home";

$STR_TITLE_PAYMENT_METHOD = "Payment Method List";
$STR_TITLE_TAX_INFO = "Tax Information List";
$STR_TITLE_EVENT = "Event List";
$STR_TITLE_CATEGORY = "Category List";
$STR_TITLE_DOMAIN = "Domain List";
$STR_TITLE_BUYER = "Customer List";
$STR_TITLE_FREE_MEMBER = "Free Member List";
$STR_TITLE_EDIT_SPLITAMOUNT = "Split Amount & Card Processing Fees Details";
$STR_TITLE_ADMIN_MODULE = "Module Admin List";
$STR_TITLE_WORK_HISTORY = "Work History";
$STR_TITLE_MODULE_LIST = "Admin Module List";
//$STR_TITLE_MEMBER = "Member List";
//$STR_TITLE_WHY_US = "Why Us?";
$STR_TITLE_KNOWLEDGE = "Knowledge List";
$STR_TITLE_GLOSSARY = "Glossary List";
$STR_TITLE_PAYMENT_REQUEST = "Payment Request List";
$STR_TITLE_AUTHORIZATION_DOCUMENT = "Authorization Document List";
$STR_TITLE_BUY_NOW = "Buy Now";
$STR_TITLE_VIDEO_LIST = "Video List";
$STR_TITLE_HOME_PAGE_HEADER = "Home Page Header";
//$STR_TITLE_VIDEO_CLIP_LIST = "Video Clip List";

$STR_TITLE_ADD_BANNER_LIST = "Advertisement Banner List";
$STR_TITLE_ARTICLESANDTOOLS_LIST = "Articles And Tools List";
//$STR_TITLE_BACKGROUND_CHECK = "Background Check";
//$STR_TITLE_NANNY_TAXES = "Nanny Taxes List";
//$STR_TITLE_CPR_FIRST_AID = "CPR/First Aid List";
//$STR_TITLE_INA = "INA List";
//$STR_TITLE_SCREENING_PRACTICES = "Screening Practices List";
//$STR_TITLE_SAMPLE_FORMS = "Sample Form List";
//$STR_TITLE_NANNY_CATEGORY = "Find A Job Category List";
//$STR_TITLE_HOWHEAR_LIST = "How Did You Hear About Us List";
//$STR_TITLE_FIND_A_JOB = "Find A Job List";
//$STR_TITLE_FIND_NANNY = "Find Nanny List";
//$STR_TITLE_FIND_NANNY_CATEGORY = "Find Nanny Category List";
//$STR_TITLE_CONTACT_SERVICES = "Drone Services List";
//$STR_TITLE_CONTACT_SERVICES_CATEGORY = "Drone Services Category List";
$STR_TITLE_FOOTER_BANNER_CODE = "Banner Code For Footer";

$STR_TITLE_HEADER_NAVIGATION = "Header Navigation List";
$STR_TITLE_FOOTER_NAVIGATION = "Footer Navigation List";
$STR_TITLE_FEATURED_CAM_GIRL = "Featured Cam Girl List";
$STR_TITLE_IN_THE_SPOTLIGHT = "In The Spot Light List";
$STR_TITLE_IN_THE_SPOTLIGHT_ALTERNATIVE = "Alternative In The Spot Light List";
$STR_TITLE_HOME_TOP_SECTION = "Top Section List";
$STR_TITLE_HOME_BOTTOM_TOP_SECTION = "Bottom - Top Section";
$STR_TITLE_HOME_BOTTOM_SECTION = "Bottom Section List";
$STR_TITLE_FEATURED_SEX_TOYS = "Featured Sex Toys List";
$STR_TITLE_FEATURED_SEX_TOYS2 = "Featured Sex Toys2 List";
$STR_TITLE_VERTICAL_BANNER = "Vertical Banner List";

$STR_TITLE_INDUSTRY_NEWS = "Industry News List";
$STR_TITLE_PRESS = "Press List";
$STR_TITLE_INVEST = "Invest List";
$STR_TITLE_SPONSOR = "Sponsor List";
$STR_TITLE_SUBMIT_NEWS_CONTENTS = "Submit News Contents List";
$STR_TITLE_SUBMIT_NEWS_LIST = "Submit News List";

$STR_TITLE_CATEGORY_LIST = "Category List";
$STR_TITLE_SUB_CATEGORY_LIST = "Sub Category List";
$STR_TITLE_REVIEW_LIST = "Review List";




    
$STR_TITLE_CCBILL_PAYMENT_OPTION = "CCbill Payment Options List";
$STR_TITLE_MANUAL_TRANS_DETAILS = "Manual Transaction Details";
$STR_MONTHLY_VIEW_COUNT_REPORT="Monthly Unique View Count Report";
$STR_YEARLY_VIEW_COUNT_REPORT="Yearly Unique View Count Report";
$STR_FILTER_CRITERIA="Filter Criteria";

$STR_WEEKLY_SALES_REPORT="Weekly Transaction Report";
$STR_MONTHLY_SALES_REPORT="Monthly Transaction Report";
$STR_YEARLY_SALES_REPORT="Yearly Transaction Report";

$STR_WEEKLY_PAY_REQUEST_REPORT="Weekly Pay Request Report";
$STR_MONTHLY_PAY_REQUEST_REPORT="Monthly Pay Request Report";

$STR_TITLE_FORM_ADD = "Add New Details";
$STR_TITLE_FORM_UPDATE = "Edit Details";
$STR_TITLE_FORM_ENTER_PASSWORD = "Enter Password Details";

$STR_BUTTON_TITLE_FORM_ADD="Click to add details";
$STR_BUTTON_TITLE_FORM_EDIT="Click to save changes";
$STR_BUTTON_TITLE_FORM_ENABLE_DISABLE="Click to enable/disable ";
$STR_BUTTON_TITLE_FORM_RESET="Click to reset form";
$STR_BUTTON_TITLE_HELP="Click to view help";

$STR_HREF_TITLE_LINK="Go to";
$STR_BUTTON_TITLE_FORM_DISPLAY_AS_NEW="Click to change 'Display As New' mode";
$STR_BUTTON_TITLE_FORM_EDIT_ICON="Click to edit details";
$STR_BUTTON_TITLE_FORM_DELETE_ICON="Click to delete details";
$STR_BUTTON_TITLE_FORM_INVISIBLE_ICON="Click to make it visible";
$STR_BUTTON_TITLE_FORM_VISIBLE_ICON="Click to make it invisible";
$STR_BUTTON_TITTLE_FORM_DISPLAY_ORDER="Click to set display order";
$STR_BUTTON_TITLE_FORM_ALLOW_LOGIN_MODE="Click to change allow login mode to";
#--------------------------------------------------------------------------------------------------------
# Error Messages will be displayed on page
$STR_MSG_ACTION_INVALID_SCODE = "ERROR !!! Invalid secret code. It's case sensitive.";
$STR_MSG_ACTION_INFO_MISSING = "ERROR !!! Some information(s) missing, please try again.";
$STR_MSG_ACTION_INVALID_PHOTO_EXT = "ERROR !!! Image extension not supported.";
$STR_MSG_ACTION_INVALID_DOC_EXT = "ERROR !!! Document extension not supported.";
$STR_MSG_ACTION_INVALID_ZIP_EXT = "ERROR !!! Zipfile extension not supported.";
$STR_MSG_ACTION_INVALID_DATE_FORMAT = "ERROR !!! Invalid Date format.";
$STR_MSG_ACTION_INVALID_URL_FORMAT = "ERROR !!! Invalid URL format.";
$STR_MSG_ACTION_INVALID_EMAIL = "ERROR !!! Invalid email format. example: emailid@websitename.com";
$STR_MSG_ACTION_INVALID_USERID = "ERROR !!! Invalid admin ID format. please only use alphabets, numbers and underscore(_).";
$STR_MSG_ACTION_INVALID_PASSWORD = "ERROR !!! Invalid password format. please only use alphabets and numbers.";
$STR_MSG_ACTION_INVALID_FILE_EXTENSION = "ERROR !!! Invalid file extension";
$STR_MSG_ACTION_INVALID_USERNAME_PASSWORD = "ERROR !!! Invalid User Name and / or Password.";
$STR_MSG_ACTION_LOGIN_RESTRICTED = "ERROR !!! Admin panel Login is restricted at present";

$STR_MSG_ACTION_ADD = "SUCCESS !!! Details added.";
$STR_MSG_ACTION_UPDATE = "SUCCESS !!! Details updated.";
$STR_MSG_ACTION_DELETE = "SUCCESS !!! Details deleted.";
$STR_MSG_ACTION_DELETE_IMAGE = "SUCCESS !!! Image deleted.";
$STR_MSG_ACTION_DELETE_FILE = "SUCCESS !!! File deleted.";
$STR_MSG_ACTION_DISPLAY_ORDER = "SUCCESS !!! Display order updated.";
$STR_MSG_ACTION_VISIBLE_INVISIBLE = "SUCCESS !!! Details changed to ";
$STR_MSG_ACTION_LOGIN="SUCCESS !!! Login Mode Changed.";
$STR_MSG_DISPLAY_NEW_MODE_UPDATED = "SUCCESS !!! Display as New mode updated.";

$STR_MSG_NO_DATA_AVAILABLE = "Currently no data available to display";

#--------------------------------------------------------------------------------------------------------
# Help Messages will be displayed on page in Help Section 
$STR_MSG_HELP_HEADING = "Help Section";
$STR_MSG_HELP_DISPLAY_ORDER = "Column named DISPLAY ORDER is used to manage display order in an ASCENDING order at user side.";
$STR_MSG_HELP_VISIBLE_INVISIBLE = "<a class='btn btn-warning btn-xs disabled'><i class='glyphicon glyphicon-eye-open'></i></a>&nbsp;Indicates VISIBLE data, Click to make it INVISIBLE.";
$STR_MSG_HELP_INVISIBLE_VISIBLE = "<a class='btn btn-default active btn-xs disabled'><i class='glyphicon glyphicon-eye-close'></i></a>&nbsp;Indicates INVISIBLE data, Click to make it VISIBLE.";
$STR_MSG_HELP_UPDATE = "<a class='btn btn-success btn-xs disabled'><i class='glyphicon   glyphicon-pencil'></i></a>&nbsp;Click to UPDATE particular data.";
$STR_MSG_HELP_UPDATE_DISABLED = "<a class='btn btn-default active btn-xs disabled'><i class='glyphicon   glyphicon-pencil'></i></a>&nbsp;indicates disabled UPDATE feature.";
$STR_MSG_HELP_DELETE = "<a class='btn btn-danger btn-xs disabled'><i class='glyphicon  glyphicon-remove'></i></a>&nbsp;Click to DELETE particular data permanently.";
$STR_MSG_HELP_DELETE_DISABLED = "<a class='btn btn-default active btn-xs disabled'><i class='glyphicon  glyphicon-remove'></i></a>&nbsp;Indicates disabled DELETE feature.";
#--------------------------------------------------------------------------------------------------------
$STR_LINK_HELP = "<a href='#help'  class='link' title='Click to view help section'><i class='glyphicon glyphicon-info-sign '></i>&nbsp; HELP</a>";
$STR_LINK_TOP = "<a href='#ptop' class='link ' title='Go to top of page'><i class='glyphicon glyphicon-circle-arrow-up'></i>&nbsp;TOP</a>";
#--------------------------------------------------------------------------------------------------------
$STR_HTML_MSG="<b>Basic HTML tags to use in your content.</b><br/>
&bull;  ". htmlentities('<h1>your text here</h1>') . " to " . htmlentities('<h4>your text here</h4>') . " : This tag is used for different size heading where h1 gives largest and h4 gives smallest size font. <br/>
&bull;  Image : ". htmlentities('<img src="path of image" border="0" alt="info about image" class="image-responsive" width="100" height="100">'). "<br/>
&bull; Mail to : ". htmlentities("<a href='mailto:emailid@websitename.com'>your text  here</a>") ."<br/>
&bull; Link : ". htmlentities('<a href="http://www.websitename.com">your text  here</a>') ."<br/>
&bull; Blank space : ". htmlentities('&nbsp;') ." : Multiply this for multiple spaces.<br/>
&bull; New line : ". htmlentities('<br/>') ."<br/>
&bull; Horizontal line : ". htmlentities('<hr>') ."<br/>
&bull; Bold text : ". htmlentities('<b>your text here</b>') ."<br/>
&bull; Italic text : ". htmlentities('<i>your text here</i>') ."<br/>
&bull; Strike text : ". htmlentities('<strike>your text here</strike>') ."<br/>
&bull; Underline text : ". htmlentities('<u>your text here</u>') ."<br/>";

$STR_HELP_TEXT_HTML_OBJ="For mobile friendly website, use below mentioned mobile friendly HTML objects as per given examples.";
$STR_HELP_TEXT_HTML_TABLE="For mobile friendly website, you must NOT use TABLE tags. Instead of that you should use DIV tags as per given example.";

$STR_SECERT_CODE="NOTE: Secret code is case sensitive. Refresh your browser to generate a new code.";
$STR_SECERT_QA="NOTE: Please type your answer here in 'word'.";

$STR_DISPLAY_AS_NEW = "If set NO, data will not be displayed as NEW.";
$STR_DISPLAY_AS_HOT = "If set NO,  data will not be displayed as HOT.";
$STR_VISIBLE="NOTE: if set NO, data will be INVISIBLE at user side.";
$STR_MANDATORY="Fields marked with * are mandatory.";
$STR_URL_MSG="Example : http://www.websitename.com ";
$STR_EMAIL_MSG="Example: emailid@websitename.com";
$STR_EMAIL_FORMAT_MSG ="FORMAT: emailid@domainname.com";
$STR_OPEN_IN_NEW_WINDOW = "Select YES if you want to open URL in new window when clicked.";

$STR_IMG_FILE_TYPE = "Supported file type: .jpeg, .jpg, .png, .gif";
$STR_IMG_FILE_TYPE_VALIDATION = "jpg,jpeg,png,gif,";

$STR_DOC_FILE_TYPE_VALIDATION = "pdf,doc,docx,";
$STR_VIDEO_FILE_TYPE_VALIDATION = "flv,asf,avi,mov,mp4,mpg,mpeg,ogg,ogv,webm,wmv,";

$STR_IMG_FILE_TYPE_DISPLAY="jpg,jpeg,png,gif,";
$STR_DOC_FILE_TYPE_DISPLAY = "pdf,doc,docx,txt";
$STR_ZIP_FILE_TYPE_DISPLAY = "zip,rar,tar,";
$STR_VIDEO_FILE_TYPE_DISPLAY = "flv,asf,avi,mov,mp4,mpg,mpeg,ogg,ogv,webm,wmv,";

$STR_IMG_UPDATE_MSG="Keep field blank if not want to update existing image.";
$STR_IMG_UPLOAD_MSG = "Keep field blank if not want to upload image.";
$STR_IMG_VISIBLE="If set NO, Image will not be displayed at user side.";
$STR_IMG_NOT_UPLOADED = "image not uploaded";

//$STR_LINK_MSG="<br/>NOTE: If anchor tag is added then please also add class for that anchor tag. | e.g ".htmlentities("<a href='http://www.website.com' class='Link10ptNormal'>write your text here</a>");

//$STR_MAILTO_MSG="NOTE: To enter MAIL TO functionality, Please copy following code in above text area.<br/>e.g ".htmlentities("<a href='mailto:emailid@domainname.com' class='Link8ptNormal'>emailid@domainname.com </a>");

$STR_DISPLAY_AS_NEW_CATEGORY = "NOTE: If set NO, data will not be displayed as new.";
$STR_DISPLAY_AS_NEW_PRODUCT = "NOTE: If set NO, data will not be displayed as new.";
$STR_ALLOW_SHOP = "NOTE: Select YES if you want to make this data available for shopping.";

$STR_ZIP_FILE_TYPE = "File Type: .zip, .rar";
$STR_MAX_ZIP_LIMIT="You can upload zipfile of maximum 10MB.";
$STR_ZIP_FILE_TYPE_VALIDATION = ".zip,.rar,";

$STR_COLOR_MSG="e.g F5F26A   i.e. color code in HEX";

$STR_MAX_30_MSG="NOTE: Maximum 30 characters are allowed to be entered.";
$STR_MAX_50_MSG="NOTE: Maximum 50 characters are allowed to be entered.";
$STR_MAX_80_MSG="NOTE: Maximum 80 characters are allowed to be entered.";
$STR_MAX_100_MSG="NOTE: Maximum 100 characters are allowed to be entered.";
$STR_MAX_255_MSG="Maximum 255 characters are allowed to be entered.";
$STR_MAX_512_MSG="NOTE: Maximum 512 characters are allowed to be entered.";
$STR_MAX_1024_MSG="NOTE: Maximum 1024 characters are allowed to be entered.";

$STR_RESUME_URL="NOTE:If url is not given then even the url title will not be displayed at user side.";

//$STR_DISPLAY_ORDER="Column named \"Display Order\" is used to manage display order in an ascending order at user side.";

$STR_AMOUNT_MSG="(in $)";
$STR_AMOUNT_CHAR="$";
$STR_SCROLL_MSG="e.g. Coming Soon, Special Offer etc.";

$STR_PROD_IMG_FILE_TYPE = "File Type: .jpeg, .jpg, .png";
$STR_PROD_PDF_FILE_TYPE = "Supported File Type: .pdf";

$STR_PROD_IMG_FILE_TYPE_VALIDATION = "jpg,jpeg,png,";


//$STR_ONLINE_STATUS="NOTE: If set ONLINE, Your Online Status will be displayed for Video Chat Room.";

//$STR_WEBMASTER_MSG="NOTE: Press 'Enter' key to display bullet at the user side.";

//$STR_SEPRATE_LINE_MSG="NOTE: Press 'Enter' key to display bullet at user side.";

//$STR_FORUM_MSG="NOTE: If set to 'YES', Allow Post Topic functionality will be visible at user side. <br/>If set to 'NO', Allow Post Topic functionality will be restricted at user side and user can only reply for the posted topic.";
//$STR_FORUM_APPROVE="NOTE: If set to \91YES\92, user can submit only below specified number of topics until admin approves user\92s previously posted topic.<br/> If set to \91NO\92, user can post topics without waiting for an approval of admin.";
//$STR_FORUM_NOOFTOPIC="Specify total number of topics which will be allowed to post until admin approves user\92s previously posted topic.<br/>This total number will be considered only if 'Approve Posted Topics' mode is set to \91YES\92.";
//$STR_FORUM_NOTIFY="NOTE: If set to 'YES', email will be sent to below email address to inform the submission of new topic.";
//$STR_FORUM_NOTIFY_EMAIL="NOTE: If 'Notify for Topic Posting' mode is set to 'YES', specify email id to send notification at that address.";

#For Digital Signature
$UPLOAD_DIGISIGN_PATH="./../../mdm/digisign/";

#For Contact Inquiry
$UPLOAD_CONTACT_FILE_PATH="../../mdm/contact/";
#------------------------------------------------------------------------------
?>
