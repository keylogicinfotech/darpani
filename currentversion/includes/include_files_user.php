<?php # File Inclusion ?>
<link href="./../css/bootstrap.min.css" rel="stylesheet">
<!--<link href="./../css/bootstrap.min.css" rel="stylesheet">-->
<link href="./../css/font-awesome.min.css" rel="stylesheet">
<link href="./../css/user.css" rel="stylesheet">

<script language="JavaScript" src="./../js/functions.js" type="text/javascript"></script>
<script src="./../js/jquery.min.js"></script>
<script src="./../js/bootstrap.min.js"></script>
<script language="JavaScript" src="./../js/jqBootstrapValidation.js"></script>
<script type="text/javascript" src="./../js/richetxteditor.js"></script>
<script type="text/javascript">
  bkLib.onDomLoaded(function() {
        new nicEditor({fullPanel : true,maxHeight : 200}).panelInstance('ta_desc');
		//new nicEditor({fullPanel : true,maxHeight : 300}).panelInstance('ta_remark');
  });
		// convert all text areas to rich text editor on that page
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() }); 
		// convert text area with id area2 to rich text editor with full panel.
        bkLib.onDomLoaded(function() {
             new nicEditor({fullPanel : true}).panelInstance('ta_desc');
			// new nicEditor({fullPanel : true}).panelInstance('ta_remark');
        });
</script>