<?php
#--------------------------------------------------------------------------------------
#this function converts seconds in HH:MM:SS format
function SecToHHMMSS($seconds)
{
  $t = round($seconds);
  return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
}
function DDMMMYYYYFormat($strDate)
{
	return date("d-M-Y",strtotime($strDate));		
}
#--------------------------------------------------------------------------------------
# It shows date like: 12-Mar-2019 12:00:00 (In 24 Hrs Format).
function DDMMMYYYYHHIISSFormat($strDate)
{
	return date("d-M-Y H:i:s",strtotime($strDate));		
}
#---------------------------------------------------------------------------------------
function YYYYMMDDFormat($strDate)
{
	return date("Y-m-d",strtotime($strDate));
}
#----------------------------------------------------------------------------------------
function MMDDYYYYFormat($strDate)
{
	return date("m/d/Y",strtotime($strDate));		
}
#-----------------------------------------------------------------------------------------
#	This function creates a date format to have input of date.
#	Mainly used to display input for date
	function DisplayBirthDate($dayvalue,$monthvalue,$yearvalue,$varday=0,$varmonth=0,$varyear=0)
	{
		if($varday == "")
		{
			$varday = 0;
		}
		if($varmonth == "")
		{
			$varmonth = 0;
		}
		if($varyear == "")
		{
			$varyear = 0;
		}	
		
		$Tab="";
		$Tab = '<label><select name="'. $dayvalue .'" class="form-control input-sm">';
		$Tab = $Tab . "<option value='0'>-- Day --</option>";
		for($i = 1;$i<=31;$i++)
		{
			$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$varday) . ">" . $i . "</option>";
		}
		$Tab = $Tab . "</select></label>";
		print $Tab;
		
		$Tab = '&nbsp;&nbsp;<label><select name="' . $monthvalue .'" class="form-control input-sm">';
		$Tab = $Tab . "<option value='0'>-- Month --</option>";
		$Tab = $Tab . "<option value='1'" . CheckSelected("1",$varmonth) . ">January</option>";
		$Tab = $Tab . "<option value='2'" . CheckSelected("2",$varmonth) . ">February</option>";
		$Tab = $Tab . "<option value='3'" . CheckSelected("3",$varmonth) . ">March</option>";
		$Tab = $Tab . "<option value='4'" . CheckSelected("4",$varmonth) . ">April</option>";
		$Tab = $Tab . "<option value='5'" . CheckSelected("5",$varmonth) . ">May</option>";
		$Tab = $Tab . "<option value='6'" . CheckSelected("6",$varmonth) . ">June</option>";
		$Tab = $Tab . "<option value='7'" . CheckSelected("7",$varmonth) . ">July</option>";
		$Tab = $Tab . "<option value='8'" . CheckSelected("8",$varmonth) . ">August</option>";
		$Tab = $Tab . "<option value='9'" . CheckSelected("9",$varmonth) . ">September</option>";
		$Tab = $Tab . "<option value='10'" . CheckSelected("10",$varmonth) . ">October</option>";
		$Tab = $Tab . "<option value='11'" . CheckSelected("11",$varmonth) . ">November</option>";
		$Tab = $Tab . "<option value='12'" . CheckSelected("12",$varmonth) . ">December</option>";
		$Tab = $Tab . "</select></label>";
		print $Tab;
		
		$Tab = '&nbsp;&nbsp;<label><select name="' . $yearvalue .'" class="form-control input-sm">';
		$Tab = $Tab . "<option value='0'>-- Year --</option>";
		$varcurrentyear = date("Y");
		for($i = $varcurrentyear-70 ;$i<=$varcurrentyear - 18;$i++)
		{
			$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$varyear) . ">" . $i . "</option>";
		}
		$Tab = $Tab . "</select></label";
		print($Tab);
	}
#-------------------------------------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------
#	This function creates a date format to have input of date.
#	Mainly used to display input for date
	function DisplayDate($dayvalue,$monthvalue,$yearvalue,$varday=0,$varmonth=0,$varyear=0)
	{
		if($varday == "")
		{
			$varday = 0;
		}
		if($varmonth == "")
		{
			$varmonth = 0;
		}
		if($varyear == "")
		{
			$varyear = 0;
		}	

		$Tab="";
		$Tab = '<label><select id="'. $dayvalue .'" name="'. $dayvalue .'" class="form-control">';
		$Tab = $Tab . "<option value='0'>-- Day --</option>";
		for($i = 1;$i<=31;$i++)
		{
			$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$varday) . ">" . $i . "</option>";
		}
		$Tab = $Tab . "</select></label>";
		print $Tab;
		
		$Tab = '&nbsp;&nbsp;<label><select id="' . $monthvalue .'" name="' . $monthvalue .'" class="form-control">';
		$Tab = $Tab . "<option value='0'>-- Month --</option>";
		$Tab = $Tab . "<option value='1'" . CheckSelected("1",$varmonth) . ">January</option>";
		$Tab = $Tab . "<option value='2'" . CheckSelected("2",$varmonth) . ">February</option>";
		$Tab = $Tab . "<option value='3'" . CheckSelected("3",$varmonth) . ">March</option>";
		$Tab = $Tab . "<option value='4'" . CheckSelected("4",$varmonth) . ">April</option>";
		$Tab = $Tab . "<option value='5'" . CheckSelected("5",$varmonth) . ">May</option>";
		$Tab = $Tab . "<option value='6'" . CheckSelected("6",$varmonth) . ">June</option>";
		$Tab = $Tab . "<option value='7'" . CheckSelected("7",$varmonth) . ">July</option>";
		$Tab = $Tab . "<option value='8'" . CheckSelected("8",$varmonth) . ">August</option>";
		$Tab = $Tab . "<option value='9'" . CheckSelected("9",$varmonth) . ">September</option>";
		$Tab = $Tab . "<option value='10'" . CheckSelected("10",$varmonth) . ">October</option>";
		$Tab = $Tab . "<option value='11'" . CheckSelected("11",$varmonth) . ">November</option>";
		$Tab = $Tab . "<option value='12'" . CheckSelected("12",$varmonth) . ">December</option>";
		$Tab = $Tab . "</select></label>";
		print $Tab;
		
		$Tab = '&nbsp;&nbsp;<label><select name="' . $yearvalue .'" id="' . $yearvalue .'" class="form-control">';
		$Tab = $Tab . "<option value='0'>-- Year --</option>";
		$varcurrentyear = date("Y") - 2;
		for($i = $varcurrentyear; $i<=$varcurrentyear + 10;$i++)
		{
			$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$varyear) . ">" . $i . "</option>";
		}
		$Tab = $Tab . "</select></label>";
		print($Tab);
	}
#-------------------------------------------------------------------------------------------------------------------

#	This function creates a time(HH:MM) to have input of time.
#	Mainly used to display input for time(HH:MM)
function DisplayTime($cbohourvalue,$cbominutevalue,$vartimehh=0,$vartimemm=0)
	{
		if($vartimehh == "")
		{
			$vartimehh = "";
		}
		if($vartimemm == "")
		{
			$vartimemm = "";
		}
		
		$Tab="";
		$Tab = '<select id="'.$cbohourvalue.'" name="'.$cbohourvalue.'" class="form-control">';
		$Tab=$Tab."<option value=' '>-- Hour --</option>";
		
		for($i = 0;$i<=23;$i++)
		{
			$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$vartimehh) . ">" . $i . "</option>";
		}
		$Tab = $Tab . "</select>";		
		
		$Tab = $Tab . '&nbsp;:&nbsp;&nbsp;<select id="'.$cbominutevalue.'" name="'.$cbominutevalue.'" class="form-control">';
		$Tab = $Tab . "<option value=' '>-- Minute --</option>";
		for($i = 0;$i<=59;$i++)
		{
			$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$vartimemm) . ">" . $i . "</option>";
		}
		$Tab = $Tab . "</select>";				
		print($Tab);
	}
#-------------------------------------------------------------------------------------------------------------------
#	This function creates a time(HH:MM:AM/PM) to have input of time.
#	Mainly used to display input for time(HH:MM:AM/PM)
function DisplayTime12($timehhvalue,$timemmvalue,$timeampmvalue,$vartimehh=0,$vartimemm=0,$vartimeampm=0)
	{
		if($vartimehh == "")
		{
			$vartimehh = "";
		}
		if($vartimemm == "")
		{
			$vartimemm = "";
		}
		if($vartimeampm == "")
		{
			$vartimeampm = "";
		}
		
		$Tab="";
		$Tab = "<label><select name='" . $timehhvalue . "' id='" . $timehhvalue . "' class='form-control'><option value=''>-- Hour --</option>";
		for($i = 1;$i<=12;$i++)
		{	
			$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$vartimehh) . ">" . $i . "</option>";
		}
		$Tab = $Tab . "</select></label>&nbsp;&nbsp;<label>";		
		$Tab = $Tab . "<select name='" . $timemmvalue . "' id='" . $timemmvalue . "' class='form-control'>";
		$Tab = $Tab . "<option value=''>-- Minute --</option>";
		for($i = 0;$i<=59;$i++)
		{	
			$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$vartimemm) . ">" . $i . "</option>";
		}
		$Tab = $Tab . "</select></label>&nbsp;&nbsp;<label>";		
		$Tab = $Tab . "<select name='" . $timeampmvalue . "' id='" . $timeampmvalue . "' class='form-control'>";
		$Tab = $Tab . "<option value=''>-- AM/PM --</option>";
		$Tab = $Tab . "<option value='AM'" . CheckSelected('AM',$vartimeampm) . "> AM </option>";
		$Tab = $Tab . "<option value='PM'" . CheckSelected('PM',$vartimeampm) . "> PM </option>";
		$Tab = $Tab . "</select></label>";
		
		return $Tab;
	}	
#-------------------------------------------------------------------------------------------------------------------
#	This function creates a timezone to have input of timezone.
#	Mainly used to display input for timezone
function DisplayTimeZone($cbotimezone,$varzone=0)
	{	
		if($varzone == "")
		{
			$varzone = "";
		}
		$Tz="";
		$Tz = '<select name="'. $cbotimezone .'" id="'. $cbotimezone .'" class="form-control">';
		$Tz = $Tz . "<option value='EST'" . CheckSelected('EST',$varzone) . ">EST(Eastern Standard Time)</option>";
		$Tz = $Tz . "<option value='PST'" . CheckSelected('PST',$varzone) . ">PST(Pacific Standard Time)</option>";
		$Tz = $Tz . "<option value='CST'" . CheckSelected('CST',$varzone) . ">CST(Central  Standard Time)</option>";
		$Tz = $Tz . "<option value='MST'" . CheckSelected('MST',$varzone) . ">MST(Mountain Standard Time)</option>";
		$Tz = $Tz . "</select>";
		
		return $Tz;
	}

#--------------------------------------------------------------------------------------------------------------------
	
	function DisplayMonth($varmonth=0, $monthtitle="Month")
	{
		if($varmonth == "")
		{
			$varmonth = 0;
		}
		$Tab = "";
		$Tab = "<select name='cbo_month' id='cbo_month' class='form-control'><option value='0'>--". $monthtitle ."--</option>";
		for($i = 1;$i<=12;$i++)
		{
			$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$varmonth) . ">" . MonthName($i) . "</option>";
		}
		$Tab = $Tab . "</select>";
		return $Tab;	
	}
	
#--------------------------------------------------------------------------------------------------------------------
	/*
	function DisplayMonth12($varmonth=0)
	{
		if($varmonth == "")
		{
			$varmonth = 0;
		}
		$Tab = "";
		$Tab = "<select name='cbo_month'><option value=''>--Month--</option>";
		for($i = 1;$i<=12;$i++)
		{
			$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$varmonth) . ">" . MonthName($i) . "</option>";
		}
		$Tab = $Tab . "</select>";
		return $Tab;	
	}
	*/
#-------------------------------------------------------------------------------------------------------------------
	function DisplayMonth12($varmonth=0,$varmonthlist=0)
	{
		if($varmonth == "")
		{
			$varmonth = 0;
		}
		if($varmonthlist == "")
		{
			$varmonthlist = 0;
		}
		$Tab = "";
		$arraymonthlist=explode(",",$varmonthlist);
		
		$Tab = "<select name='cbo_month'><option value=''>--Month--</option>";
		for($i = 1;$i<=12;$i++)
		{
			$int_monthcount=0;
			for($j=0;$j<count($arraymonthlist);$j++)
			{
				if($arraymonthlist[$j]==$i)
				{
					$int_monthcount=1;
				}
			}
			if($int_monthcount==0)
			{
				$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$varmonth) . ">" . MonthName($i) . "</option>";
			}
		}
		$Tab = $Tab . "</select>";
		return $Tab;	
	}
#-------------------------------------------------------------------------------------------------------------------
	/*
	function DisplayYear($varyear=0)
	{
		if($varyear == "")
		{
			$varyear = 0;
		}
		$varstartyear = 2005;
		$varcurrentyear = date("Y");
		$Tab = "";
		$Tab = $Tab . "<select name='cbo_year'>";		
		$Tab = $Tab . "<option value=''>--Year--</option>";
		for($i=$varstartyear;$i<=$varcurrentyear+10;$i++)
		{
			$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$varyear) . ">" . $i . "</option>";
		}
		$Tab = $Tab . "</select>";
		return $Tab;		
	}
	*/
#-------------------------------------------------------------------------------------------------------------------
	function DisplayYear($varyear=0,$varyearlist=0)
	{
		if($varyear == "")
		{
			$varyear = 0;
		}
		if($varyearlist == "")
		{
			$varyearlist = 0;
		}

		$varstartyear = date("Y")-1;
		$varcurrentyear = date("Y");
		$Tab = "";
		
		$arrayyearlist=explode(",",$varyearlist);
		
		$Tab = $Tab . "<select name='cbo_year' id='cbo_year' class='form-control'>";		
		$Tab = $Tab . "<option value=''>--Year--</option>";
		for($i=$varstartyear;$i<=$varcurrentyear+10;$i++)
		{			
			$int_yearcount=0;
			for($j=0;$j<count($arrayyearlist);$j++)
			{
				if($arrayyearlist[$j]==$i)
				{
					$int_yearcount=1;
				}
			}
			if($int_yearcount==0)
			{
				$Tab = $Tab . "<option value='" . $i . "'" . CheckSelected($i,$varyear) . ">" . $i . "</option>";
			}
		}
		$Tab = $Tab . "</select>";
		return $Tab;		
	}
#-------------------------------------------------------------------------------------------------------------------
	function MonthName($monthnumber)
	{
		$strmonthname = "";
		switch($monthnumber)
		{
			case(1):
				$strmonthname = "January";
				break;
			case(2):
				$strmonthname = "February";
				break;
			case(3):
				$strmonthname = "March";
				break;
			case(4):
				$strmonthname = "April";
				break;
			case(5):
				$strmonthname = "May";
				break;
			case(6):
				$strmonthname = "June";
				break;
			case(7):
				$strmonthname = "July";
				break;
			case(8):
				$strmonthname = "August";
				break;
			case(9):
				$strmonthname = "September";
				break;
			case(10):
				$strmonthname = "October";
				break;
			case(11):
				$strmonthname = "November";
				break;
			case(12):
				$strmonthname = "December";
				break;
			case(13):
				$strmonthname = "All";
				break;
			default:
				$strmonthname = "Illegal Month";
				break;
		}
		return $strmonthname;
	}
#----------------------------------------------------------------------------------------------------------------------
  /*
*	Function Name : DateDifferenceInDays()
*   Argument :1. $strenddate :- End date.
*			  2. $strstartdate :- Start date.
*   Return : Returns number of days between two dates.
*   Purpose : To display date difference
*   Usage : to display date difference
   i.e.  DateDifferenceInDays("2005/12/31","2005/01/01")
*/
	function DateDifferenceInDays($strenddate,$strstartdate)
	{
		$strendyear = date ("Y",strtotime($strenddate));
		$strstartyear = date ("Y",strtotime($strstartdate));
		$strdatediff = 0;
		$strdisplay = "";
		if($strendyear > $strstartyear)
		{
			$strstartdate4 = $strstartdate;
			$strstartdate5 = getdate(strtotime($strstartdate4));
			$strstartdate6 = mktime(0,0,0,$strstartdate5["mon"],$strstartdate5["mday"],$strstartdate5["year"]);						
			$strtempdatediff = date("z",$strstartdate6);
			if(date("L",$strstartdate6) == 1)
			{
				$strdatediff = $strdatediff + (366-$strtempdatediff);
			}
			else
			{
				$strdatediff = $strdatediff + (365-$strtempdatediff);
			}
			
			for($i=$strstartyear+1;$i<=$strendyear-1;$i++)
			{
				$strtempdate = getdate(strtotime($i."/01/01"));
				$strtempdate1 = mktime(0,0,0,$strtempdate["mon"],$strtempdate["mday"],$strtempdate["year"]);
				if(date("L",$strtempdate1) == 1)
				{
					$strdatediff = $strdatediff + 366;
				}
				else
				{
					$strdatediff = $strdatediff + 365;
				}
			}
			
			$strenddate4 = $strenddate;
			$strenddate5 = getdate(strtotime($strenddate4));
			$strenddate6 = mktime(0,0,0,$strenddate5["mon"],$strenddate5["mday"],$strenddate5["year"]);						
			$strtemp1datediff = date("z",$strenddate6);
			$strdatediff = $strdatediff + $strtemp1datediff;
			if($strdatediff < 7 && $strdatediff > 0)
			{
				$strdisplay = "<span class='RedText'>" . $strdatediff . " days</span>";
			}
			elseif($strdatediff < 30 && $strdatediff > 0)
			{
				$strdisplay = "<span class='BlueText'>" . $strdatediff . " days</span>";
			}
			elseif($strdatediff > 30)
			{
				$strdisplay = "<span class='GreenText'>" . $strdatediff . " days</span>";
			}
			else
			{
				$strdisplay = "<span class='RedText'>Registration Expired</span>";
			}		
			return $strdisplay;
		}
		else
		{
				$strenddate1 = getdate(strtotime($strenddate));
				$strstartdate1 = getdate(strtotime($strstartdate));
				
				$strenddate2 = mktime(0,0,0,$strenddate1["mon"],$strenddate1["mday"],$strenddate1["year"]);
				$strstartdate2 = mktime(0,0,0,$strstartdate1["mon"],$strstartdate1["mday"],$strstartdate1["year"]);
				$strenddate3 = date("z",$strenddate2);
				$strstartdate3 = date("z",$strstartdate2);
				$strdatediff = $strenddate3 - $strstartdate3;
				if($strdatediff < 7 && $strdatediff > 0)
				{
					$strdisplay = "<span class='RedText'>" . $strdatediff . " days</span>";
				}
				elseif($strdatediff < 30 && $strdatediff > 0)
				{
					$strdisplay = "<span class='BlueText'>" . $strdatediff . " days</span>";
				}
				elseif($strdatediff > 30)
				{
					$strdisplay = "<span class='GreenText'>" . $strdatediff . " days</span>";
				}
				else
				{
					$strdisplay = "<span class='RedText'>Registration Expired</span>";
				}			
				return $strdisplay;							
		}
	}
#---------------------------------------------------------------------------------------------------------
/*
*	Function Name : GetTimeInAMPMFormat()
*   Argument : $int_hour :- value must be between (0 to 23)
*			   $int_minute :- value must be between (0 to 59)
*   Return value Format : hh:mm AM/PM	
*   Purpose : Convert 24 hours format time in 12 hours format time 
*   Usage : 
*          e.g,  GetTimeInAMPMFormat(15,30)
*          		 here 15 is hour and 30 is minute
*		OutPut : 03:30 PM									
*/		
			
	function GetTimeInAMPMFormat($int_hour, $int_minute)
	{
		  if($int_hour >=0 && $int_hour < 12 )
		  {		
		  		$str_ampm = "AM";							 										
				$str_hh = trim($int_hour);								
				if(strlen($str_hh)==1)
					$str_hh = "0".$str_hh;
		  }
		  else	
		  {		
		  		$str_ampm = "PM";				
				switch ($int_hour)
				{		
						case(12):
							$str_hh = "12";
							break;
						case(13):
							$str_hh = "01";
							break;
						case(14):
							$str_hh = "02";
							break;
						case(15):
							$str_hh = "03";
							break;
						case(16):
							$str_hh = "04";
							break;
						case(17):
							$str_hh = "05";
							break;
						case(18):
							$str_hh = "06";
							break;
						case(19):
							$str_hh = "07";
							break;
						case(20):
							$str_hh = "08";
							break;
						case(21):
							$str_hh = "09";
							break;
						case(22):
							$str_hh = "10";
							break;
						case(23):
							$str_hh = "11";
							break;	
					}																
			}								 
			$str_mm = trim($int_minute);			
			if(strlen($str_mm)==1)
				$str_mm = "0".$str_mm;
										
		return trim($str_hh . ":" . $str_mm . " " . $str_ampm); 	
	}	

#--------------------------------------------------------------------------------------------
#This function is used to add date format which are older than 1970 ...
#Enter date in 'Y-m-d' format ...
#i.e. AddDays($today,366) returns 2006-09-09 ...where $today is date in 'Y-m-d' format

#If date comes from database then ==> i.e. AddDays($today,366) returns 2006-09-09 ...
#where $today=date('Y-m-d',strtotime($date)); Here value of date is'2005-09-08';
#You can get date in format which you want by catch return value of this function in a variable 
#i.e. $date and then use it as shown below...
#$edate=date("j-n-Y",strtotime($date));
function AddDays($sDate,$day)
{
	$strDate=getdate(strtotime($sDate));
	$newDate=mktime($strDate["hours"]+0,$strDate["minutes"]+0,$strDate["seconds"]+0,$strDate["mon"]+0,$strDate["mday"]+$day,$strDate["year"]+0);
	return date("Y-m-d",$newDate);
}	

#--------------------------------------------------------------------------------------------

//From AddMonths($sDate,$year) Function, you can get date after month specified by the $month.
//If you want only month then you can get it by :
//Month as 01 to 12 by ==> date("m",strtotime(AddMonths(date("Y-m-d"),5)));
//Month as 1 to 12 by ==> date("n",strtotime(AddMonths(date("Y-m-d"),5)));
//Month as Jan to Dec by ==> date("M",strtotime(AddMonths(date("Y-m-d"),5)));
//Month as January to December by ==> date("F",strtotime(AddMonths(date("Y-m-d"),5)));
function AddMonths($sDate,$month)
{
	$strDate=getdate(strtotime($sDate));
	$newDate=mktime($strDate["hours"]+0,$strDate["minutes"]+0,$strDate["seconds"]+0,$strDate["mon"]+$month,$strDate["mday"]+0,$strDate["year"]+0);
	return date("Y-m-d",$newDate);
}	

#--------------------------------------------------------------------------------------------
//From AddYears($sDate,$year) Function, you can get date after year specified by the $year.
//If you want only year then you can get it by :
//date("Y",strtotime(AddYears(date("Y-m-d"),5)));
function AddYears($sDate,$year)
{
	$strDate=getdate(strtotime($sDate));
	$newDate=mktime($strDate["hours"]+0,$strDate["minutes"]+0,$strDate["seconds"]+0,$strDate["mon"]+0,$strDate["mday"]+0,$strDate["year"]+$year);
	return date("Y-m-d",$newDate);
}	

#--------------------------------------------------------------------------------------
/* this function is used to display date using timestamp datatype.
Note #1: here comparison with -1 as well as false is done, 
         becoz in PHP version prior to 5.1.0 'strtotime' returns -1 on failure 
		 while from version 5.1.0 it returns false on failure.
*/
function DateFromTimestamp($strdate,$str_seperator)
{
	if($strdate!="")
	{
		if(strtotime($strdate)==-1 || strtotime($strdate)===false) //see Note #1.
		{
			$str_format_date = "";
			$str_format_date = substr($strdate,6,2).$str_seperator.date("M",strtotime(substr($strdate,0,4)."-". substr($strdate,4,2)."-".substr($strdate,6,2))) . $str_seperator .substr($strdate,0,4);
			$str_format_date .= " - " . substr($strdate,8,2). ":".substr($strdate,10,2).":".substr($strdate,12,2);
		}
		else
		{
			$str_format_date = "";
			$str_format_date=date("d".$str_seperator."M".$str_seperator."Y - H:i:s",strtotime($strdate));
		}	
	}
	else
	{
		$str_format_date="";
	}
	return $str_format_date;
}

#---------------------------------------------------------------------------------------------------------
/*
*	Function Name : TimeIn24HourFormat()
*   Argument : $int_hour :- value must be between (1 to 12)
*			   $int_minute :- value must be between (0 to 59)
			   $int_ampm :- value must be am/pm
*   Return value Format : hh:mm 	
*   Purpose : Convert 12 hours format time in 24 hours format time 
*   Usage : 
*          e.g,  TimeIn24HourFormat(1,45,am)
*          		 here 1 is hour, 45 is minute and am is format
*		OutPut :  13:45								
*/		

function TimeIn24HourFormat($int_hour,$int_minute,$int_ampm)
{
	$str_hour=$int_hour;
	$str_min=$int_minute;
	$str_ampm=$int_ampm;
	if(strtoupper($str_ampm)=='AM')
	{
		if($str_hour==12)
		{
			$str_hour=0;
		}
	}
	if(strtoupper($str_ampm)=='PM')
	{
		if($str_hour==12)
		{
			$str_hour=12;
		}
		else
		{
			$count=$str_hour;
			$str_hour= 12 + $count;
		}
	}
	if(strlen($str_hour)==1)
	{
		$str_hour="0".$str_hour;
	}
	if(strlen($str_min)==1)
	{
		$str_min="0".$str_min;
	}
	return $str_hour.":".$str_min.":"."00";
}
#---------------------------------------------------------------------------------------------------------
/*
*	Function Name : TimeIn24HourFormat()
*   Argument : $int_hour :- value must be between (1 to 12)
*			   $int_minute :- value must be between (0 to 59)
			   $int_ampm :- value must be am/pm
*   Return value Format : hh:mm 	
*   Purpose : Convert 12 hours format time in 24 hours format time 
*   Usage : 
*          e.g,  TimeIn24HourFormat(1,45,am)
*          		 here 1 is hour, 45 is minute and am is format
*		OutPut :  13:45								
*/		

function Compare24HourTime($starttime,$endtime)
{
		list($str_hh,$str_mm,$str_sec)=split(":",$starttime);
		list($str_hh1,$str_mm1,$str_sec)=split(":",$endtime);
		
		if($str_hh1<$str_hh)
		{
			return false;
		}
		if($str_hh==$str_hh1)
		{
			if($str_mm1<$str_mm)
			{
				return false;
			}
		}
		return true;
}

// Convert UNIX TIMESTAMP in PHP Date Time 
function ConvertUNIXTimeStampToDateTime($dt_unix_time_stamp)
{
	$epoch = $dt_unix_time_stamp;
	$dt = new DateTime("@$epoch"); // convert UNIX timestamp to PHP DateTime
	$dt_datetime_format = $dt->format('Y-m-d H:i:s'); // output = 2012-08-15 00:00:00 

	return $dt_datetime_format;
}
#------------------------------------------------------------------------------------------------
?>
